<?php

include_once ("init.php");
include_once( 'sur_adm_permission.php' );
include_once( 'controllers/lgl_case_entry/classcase_entry.php' );

header('Content-Type: text/html; charset=utf-8');  
$case_entry = new case_entry();
$StatusMessage = "";
switch($_GET['action']){
	case '1' :		
					$case_entry->Addcase_entry();							
					break;	
	case '2' :		
					$case_entry->Editcase_entry();					
					break;	
					
	case '3' :		
					$case_entry->store();					
					break;	
	
	case '4':	
	case 'destroy':	
					$case_entry->destroy($_REQUEST['case_id']);	
					$_SESSION['AlertMessage'] = "Record Deleted Sucessfully";	
					header('Location: lgl_case_entry.php');
					break;	
					
	case '5' :		
					$case_entry->update();	
					break;	
	default:		
					$case_entry->index();
					break;
}

//==========================#  End of Getting records in list page===================================================================
?>