<?php if ( !defined( 'SMARTY_DIR' ) ){include_once( 'init.php' );}
include( 'sur_adm_permission.php' );
header('Content-Type: text/html; charset=utf-8');

$tmplaguage = $_SESSION['opt_lang']; 
$t->assign('txtlang',$tmplaguage);

if (isset($_REQUEST['maintype']))
	$reqmainprofile_type = $_REQUEST['maintype'];
else
	$reqmainprofile_type = "category";

$t->assign ( 'reqmainprofile_type', $reqmainprofile_type );

$reqprofile_type = $_REQUEST['subtype'];
$t->assign ( 'reqprofile_type', $reqprofile_type );

$lang_pri = 'english';

if (isset($_POST["txtgreeklanguage"]))
	$lang_sec = $_POST["txtgreeklanguage"];	
else{
	if ($_SESSION["tmp_opt_lang_sec"] <> "")
		$lang_sec = $_SESSION["tmp_opt_lang_sec"];
	
		//$lang_sec = FnGetValue("tbl_lang","language","default_flag=1"); 
}
$_SESSION["tmp_opt_lang_sec"] = $lang_sec;

$t->assign ( 'lang_pri', $lang_pri );
$t->assign ( 'lang_sec', $lang_sec );

$grklang_msg = array();
$grklangtemp = $db->getAll( "SELECT * FROM tbl_lang where language <> '$lang_pri' order by `lang_id` desc");
foreach( $grklangtemp as $index => $grkrow ) {
		$grklang_msg['language'][] = $grkrow['language'];
		$grklang_msg['lang_id'][] = $grkrow['lang_id'];
	}
$t->assign ( 'grklang_msg', $grklang_msg );


$main_category_id = $_REQUEST['txtmain_category_id'];
$t->assign('main_category_id',$main_category_id);
if($main_category_id==""){	$main_category_id=0;}
if($lang<>''){
	$req_retunr_str = heire_cat(0,$main_category_id,$tmplaguage);
	$t->assign('select_option_str',$req_retunr_str);
}

$create_op = $_GET['create_op'];
if ($create_op==3){
	//for primary fields
	$txtCustomeOrder= $_POST['txtCustomeOrder'];
	$txtfield_type	= $_POST['txtfield_type' ];
	$txtTitle       = $_POST['txtTitle'];
	$txtnooffields 	= $_POST['txtnooffields' ];
	$txtcustome_search    = $_POST['txtcustome_search'];
	$txtmain_category_id  = $_POST['txtmain_category_id'];
	$txtlang              = $_POST['txtenglish_other_lang'];
	//for secondary fields
	$txtgrkTitle = $_POST['txtgrkTitle'];
	$txtgrklang  = $_POST['txtgreek_other_lang'];
	
	$t->assign('txtmain_category_id',$txtmain_category_id);
	$t->assign('txtlang',$txtlang);
	$t->assign('txtTitle',$txtTitle);
	$t->assign('txtcustome_search',$txtcustome_search);
	$t->assign('txtfield_type',$txtfield_type);
	$t->assign('txtnooffields',$txtnooffields);
	$t->assign('txtCustomeOrder',$txtCustomeOrder);
	$t->assign('txtgrkTitle',$txtgrkTitle);

	//$t->display('adm_create_signup.htm');
	//exit();
}

$op = $_GET['op'];
if ($op==3){
	//for primary fields
	$txtCustomeOrder= $_POST['txtCustomeOrder'];
	$txtfield_type	= $_POST['txtfield_type' ];
	$txtTitle       = $_POST['txtTitle'];
	$txtnooffields 	= $_POST['txtnooffields' ];
	$txtcustome_search    = $_POST['txtcustome_search'];
	$txtmain_category_id  = $_POST['txtmain_category_id'];
	$txtlang              = $_POST['txtenglish_other_lang'];
	//for secondary fields
	$txtgrkTitle = $_POST['txtgrkTitle'];
	$txtgrklang  = $_POST['txtgreek_other_lang'];
	
	$t->assign('txtmain_category_id',$txtmain_category_id);
	$t->assign('txtlang',$txtlang);
	$t->assign('txtTitle',$txtTitle);
	$t->assign('txtcustome_search',$txtcustome_search);
	$t->assign('txtfield_type',$txtfield_type);
	$t->assign('txtnooffields',$txtnooffields);
	$t->assign('txtCustomeOrder',$txtCustomeOrder);
	$t->assign('txtgrkTitle',$txtgrkTitle);
	$cus_id = $_GET['cus_id'];
	$sqlins = "delete from tbl_profile_cus_fields WHERE main_cus_id = $cus_id";
	$db->query( $sqlins );
	//$sqlins = "delete from tbl_combo_value WHERE cus_id = $main_cus_id and lang = '$main_lang' and type = '$type'";
	$sqlins = "delete from tbl_profile_cus_fields_value WHERE main_cus_id = $cus_id";
	$db->query( $sqlins );
}

if(isset($_GET['update'])){
	if($reqprofile_type <> ''){
		$sql = "SELECT * FROM tbl_profile_cus_fields where cus_type = '$reqmainprofile_type' and lang = '$txtlang'  and sub_cus_type = '$reqprofile_type'";		
	}else{
		$sql = "SELECT * FROM tbl_profile_cus_fields where cus_type = '$reqmainprofile_type' and lang = '$txtlang' ";	
	}
	$sql .=" group by main_cus_id asc";
	$data = array();
	$temp = $db->getAll( $sql );
	$txtlang              = $_POST['txtenglish_other_lang'];
	$txtgrklang           = $_POST['txtgreek_other_lang'];
	foreach ($temp as $index => $row){
		$main_cus_id = $row['main_cus_id'];
		$controlname = $row['type'];
		$getfield_name = $controlname."_".$main_cus_id;

		if(isset($_POST['txttitle'.$getfield_name])){
			$txtTitle    = $_POST['txttitle'.$getfield_name];
			$txtgrkTitle = $_POST['txtgrktitle'.$getfield_name];
			$txtCustomeOrder 	= $_POST['txtorder'.$getfield_name];
			if($txtCustomeOrder == '')
				$txtCustomeOrder = 0;
			$txtgrkCustomeOrder = $_POST['txtgreekorder'.$getfield_name];
			if($txtgrkCustomeOrder == '')
				$txtgrkCustomeOrder = 0;
			$txtCustomeShow = $_POST['txtshow'.$getfield_name];
			$txtCustomeShow_list = $_POST['txtshow_list'.$getfield_name];
			$txtCustomeShow_search = $_POST['txtshow_search'.$getfield_name];
			
			$txtcols=$_POST['txtfield_col_'.$main_cus_id];
			$txtrows=$_POST['txtfield_rows_'.$main_cus_id];		
			
			$sql_tmp="";	
			if($txtcols==""){ 	$txtcols=2;		}
			if($txtrows==""){	$txtrows=2;		}
			
			if($txtCustomeShow == ''){	$txtCustomeShow = 0;}
			$up_sql = "Update tbl_profile_cus_fields SET title = '$txtTitle',order_no = '$txtCustomeOrder',visible = '$txtCustomeShow',approve_for_list = '$txtCustomeShow_list',search = '$txtCustomeShow_search' where main_cus_id = $main_cus_id and lang = '$txtlang'";
			$result = mysql_query($up_sql)	OR die(mysql_error()."Could not executeadasd11");
	
			$up_sql = "Update tbl_profile_cus_fields SET title = '$txtgrkTitle',order_no = '$txtgrkCustomeOrder',visible = '$txtCustomeShow',approve = '$txtCustomeShow_list',search = '$txtCustomeShow_search'  where main_cus_id = $main_cus_id and lang = '$txtgrklang'";
			$result = mysql_query($up_sql)	OR die(mysql_error()."Could not executeadasd11");

			$up_combo_sql = "Update tbl_profile_cus_fields_value SET cols = $txtcols,rows=$txtrows where main_cus_id = $main_cus_id";
			$result = mysql_query($up_combo_sql)	OR die(mysql_error()."Could not execute1".$up_combo_sql);
			
		}
	}
	?><script language="javascript">
	window.location.href="mn_profile_cus_fields.php?&maintype=<?=$reqmainprofile_type?>&txtmain_category_id=<?=$main_category_id?>";
	</script><?
}
//********************************************  Custom  Field table *************************************************************
if(isset($_GET['cus_field_submit'])){
	$txtprofile_type = $_REQUEST['txtprofile_type'];
	$txtfield_type 	 = $_POST['txtfield_type'];
	
	$txtCustomeOrder 	= $_POST['txtCustomeOrder'];
	if($txtCustomeOrder == '')
		$txtCustomeOrder = 0;
	$txtmain_category_id  = $_POST['txtmain_category_id'];
	
	if($txtmain_category_id == ''){		$txtmain_category_id = 0;}
	$txtlang              = $_POST['txtlang'];
	$txtgrklang           = $_POST['txtgrklang'];
	$txtTitle             = $_POST['txtTitle'];
	$txtgrkTitle          = $_POST['txtgrkTitle'];
	$txtcustome_search    = $_POST['txtcustome_search'];
	$_SESSION['addcustomlang'] = $txtlang;
	//=====adding custom field in all category==============
	
	//===========Adding custom field in perticular category=====================
		$ins_sql = "insert into tbl_profile_cus_fields values(NULL,'$txtfield_type','$txtTitle','$txtmain_category_id','$txtlang','0','$txtCustomeOrder','$reqmainprofile_type','1','1','$reqprofile_type','0','0')";
		//
		$result = mysql_query($ins_sql)	OR die(mysql_error(). $ins_sql . "Could not executeadasd11");
		$custome_id = mysql_insert_id();
		$txtmain_cus_id = $custome_id;
		$sql_up = "Update tbl_profile_cus_fields SET main_cus_id = '$txtmain_cus_id' where cus_id = '$custome_id'";
		$result = mysql_query($sql_up)	OR die(mysql_error()."Could not executeadasd11");

		$ins_sql = "insert into tbl_profile_cus_fields values(NULL,'$txtfield_type','$txtgrkTitle','$txtmain_category_id','$txtgrklang','$txtmain_cus_id','$txtCustomeOrder','$reqmainprofile_type','1','1','$reqprofile_type','0','0')";
		$result = mysql_query($ins_sql)	OR die(mysql_error(). $ins_sql . "Could not ");
		
		if(($txtfield_type == 'radio')||($txtfield_type == 'combobox') ||($txtfield_type == 'checkbox')){
			$txtfield_value	= $_POST['txtfield_value' ];
			$txtextra 		= $_POST['txtextra' ];
			$txtnooffields	= $_POST['txtnooffields' ];

			$txtgrkfield_value	= $_POST['txtgrkfield_value' ];
			$txtgrkextra 		= $_POST['txtgrkextra' ];

			for ($j=0; $j<$txtnooffields; $j++) {
				if($txtfield_type == 'radio')		if($j == $selected_id) $selected = 'checked'; 	else $selected ='';
				if($txtfield_type == 'combobox')	if($j == $selected_id) $selected = 'selected'; 	else $selected ='';
				
				$combo_sql = "insert into tbl_profile_cus_fields_value values(NULL,$txtmain_category_id,'$txtTitle','$txtfield_value[$j]','$selected','$txtextra[$j]','$txtmain_cus_id','$txtlang','$txtfield_type','0','0','0')";
				$result = mysql_query($combo_sql)	OR die(mysql_error()."Could not execute1".$combo_sql);
				$main_value_id = mysql_insert_id();
				
				$up_combo_sql = "Update tbl_profile_cus_fields_value SET main_value_id = $main_value_id where value_id = $main_value_id";
				$result = mysql_query($up_combo_sql)	OR die(mysql_error()."Could not execute1".$up_combo_sql);
				
				$combo_sql = "insert into tbl_profile_cus_fields_value values(NULL,$txtmain_category_id,'$txtgrkTitle','$txtgrkfield_value[$j]','$selected','$txtgrkextra[$j]','$txtmain_cus_id','$txtgrklang','$txtfield_type','0','0','$main_value_id')";
				$result = mysql_query($combo_sql)	OR die(mysql_error()."Could not execute1".$combo_sql);
			}			
		}else{
			$txtfield_value= $_POST['txtfield_value' ];
			$txtsize 	   = $_POST['txtsize'];
			$txtextra      = $_POST['txtextra'];

			$txtgrkfield_value = $_POST['txtgrkfield_value' ];
			$txtgrksize 	   = $_POST['txtgrksize'];
			$txtgrkextra       = $_POST['txtgrkextra'];
			
			if($txtsize<>''){ $txtfield_col	= $_POST['txtsize'];
			}else{ if($_POST['txtfield_col' ] != '') $txtfield_col	= $_POST['txtfield_col' ]; 		else $txtfield_col = 10; }
			
			if($txtgrksize<>''){ $txtgrkfield_col	= $_POST['txtgrksize'];
			}else{ if($_POST['txtgrkfield_col' ] != '') $txtgrkfield_col	= $_POST['txtgrkfield_col' ]; 		else $txtgrkfield_col = 10; }
			
			if($_POST['txtfield_rows' ] != '') $txtfield_rows = $_POST['txtfield_rows' ]; 	else $txtfield_rows = 2;
			if($_POST['txtgrkfield_rows' ] != '') $txtgrkfield_rows = $_POST['txtgrkfield_rows' ]; 	else $txtgrkfield_rows = 2;
			
			if(($txtfield_col==0)||($txtfield_col=="")){	$txtfield_col="10";	}
			if(($txtgrkfield_col==0)||($txtgrkfield_col=="")){	$txtgrkfield_col="10";	}
			
			$combo_sql = "insert into tbl_profile_cus_fields_value values(NULL,$txtmain_category_id,'$txtfield_value','$txtfield_value','','','$txtmain_cus_id','$txtlang','$txtfield_type','$txtfield_rows','$txtfield_col','0')";
			$result = mysql_query($combo_sql)	OR die(mysql_error()."Could not execute1".$combo_sql);
			$main_value_id = mysql_insert_id();
			
			$up_combo_sql = "Update tbl_profile_cus_fields_value SET main_value_id = $main_value_id where value_id = $main_value_id";
			$result = mysql_query($up_combo_sql)	OR die(mysql_error()."Could not execute1".$up_combo_sql);

			$combo_sql = "insert into tbl_profile_cus_fields_value values(NULL,$txtmain_category_id,'$txtgrkfield_value','$txtgrkfield_value','','','$txtmain_cus_id','$txtgrklang','$txtfield_type','$txtfield_rows','$txtfield_col','$main_value_id')";
			$result = mysql_query($combo_sql)	OR die(mysql_error()."Could not execute1".$combo_sql);
		}
	if($reqprofile_type <> '')
		$to = "mn_profile_cus_fields.php?maintype=$reqmainprofile_type&subtype=$reqprofile_type";
	else
		$to = "mn_profile_cus_fields.php?maintype=$reqmainprofile_type";	
	header("Location:$to");
	exit();
}
//********************************************  End Of  Custom  Field table *************************************************************

$lang_sec_list = array();
$lang_sec_list = array();
$lang_sec_list_tmp = $db->getAll( "SELECT * FROM tbl_lang where language <> '$lang_pri' order by `lang_id` desc" );
foreach( $lang_sec_list_tmp as $index => $lang_sec_row ) {
		$lang_sec_list['language'][] = $lang_sec_row['language'];
		$lang_sec_list['lang_id'][] = $lang_sec_row['lang_id'];
	}
$t->assign ( 'lang_sec_list', $lang_sec_list );

$greekvalue_data = array();
$chechboxcontrol = array();
$comboboxcontrol = array();
$grkchechboxcontrol = array();
$grkcomboboxcontrol = array();

if($reqprofile_type <> ''){
	$sql = "SELECT * FROM tbl_profile_cus_fields where cus_type = '$reqmainprofile_type' and lang = '$tmplaguage'  and sub_cus_type = '$reqprofile_type' ";
}else{
	$sql = "SELECT * FROM tbl_profile_cus_fields where cus_type = '$reqmainprofile_type' and lang = '$tmplaguage' ";	
}	
if($main_category_id<>0){
	$sql .=" and cat_id='".$main_category_id."'";
}
$sql .=" group by main_cus_id	order by cat_id";

$data = array();
$temp = $db->getAll( $sql );
foreach ($temp as $index => $row){
	$main_cus_id = $row['main_cus_id'];
	$controlname = $row['type'];
	//$row['cat_name']=fngetvalue("tbl_category","name","cat_id='".$row['cat_id']."'");
	
	$row['title'] = FnGetValue("tbl_profile_cus_fields","title","lang = '$lang_pri' and main_cus_id = '$main_cus_id'");  
	$row['grk_title'] = FnGetValue("tbl_profile_cus_fields","title","lang = '$lang_sec' and main_cus_id = '$main_cus_id'");  
	$row['grk_order_value'] = FnGetValue("tbl_profile_cus_fields","order_no","lang = '$lang_sec' and main_cus_id = '$main_cus_id'");  

	if($row['type']=='text'){
		$row['engclos']   = FnGetValue("tbl_profile_cus_fields_value","cols","main_cus_id = '$main_cus_id' and lang = '$lang_pri'"); 	
		$row['grkclos']   = FnGetValue("tbl_profile_cus_fields_value","cols","main_cus_id = '$main_cus_id' and lang = '$lang_sec'"); 	
	}

	if($row['type']=='textarea'){
		$row['engrows']   = FnGetValue("tbl_profile_cus_fields_value","rows","main_cus_id = '$main_cus_id' and lang = '$lang_pri'"); 	
		$row['engclos']   = FnGetValue("tbl_profile_cus_fields_value","cols","main_cus_id = '$main_cus_id' and lang = '$lang_pri'"); 	

		$row['grkrows']   = FnGetValue("tbl_profile_cus_fields_value","rows","main_cus_id = '$main_cus_id' and lang = '$lang_sec'"); 	
		$row['grkclos']   = FnGetValue("tbl_profile_cus_fields_value","cols","main_cus_id = '$main_cus_id' and lang = '$lang_sec'"); 	
	}

	if($row['type']=='combobox'){
		$engsql_combobox = "SELECT * FROM tbl_profile_cus_fields_value where main_cus_id = '$main_cus_id' and lang = '$lang_pri' ";
		$engtemp_combobox[$controlname] = $db->getall($engsql_combobox);

		$grksql_combobox = "SELECT * FROM tbl_profile_cus_fields_value where main_cus_id = '$main_cus_id' and lang = '$lang_sec' ";
		$grktemp_combobox[$controlname] = $db->getall($grksql_combobox);
	}

	if($row['type']=='checkbox'){
		$engsql_checkbox = "SELECT * FROM tbl_profile_cus_fields_value where main_cus_id = '$main_cus_id' and lang = '$lang_pri' ";
		$engtemp_checkbox[$controlname] = $db->getall($engsql_checkbox);

		$grksql_checkbox = "SELECT * FROM tbl_profile_cus_fields_value where main_cus_id = '$main_cus_id' and lang = '$lang_sec' ";
		$grktemp_checkbox[$controlname] = $db->getall($grksql_checkbox);
	}

	if($row['type']=='radio'){
		$engsql_radio = "SELECT * FROM tbl_profile_cus_fields_value where main_cus_id = '$main_cus_id' and lang = '$lang_pri' ";
		$engtemp_radio[$controlname] = $db->getall($engsql_radio);

		$grksql_radio = "SELECT * FROM tbl_profile_cus_fields_value where main_cus_id = '$main_cus_id' and lang = '$lang_sec' ";
		$grktemp_radio[$controlname] = $db->getall($grksql_radio);
	}

	$data[]  = $row;
}
$t->assign ( 'data', $data );
$t->assign ( 'comboboxcontrol', $engtemp_combobox );
$t->assign ( 'chechboxcontrol', $engtemp_checkbox );
$t->assign ( 'grkcomboboxcontrol', $grktemp_combobox );
$t->assign ( 'grkchechboxcontrol', $grktemp_checkbox );
$t->assign ( 'engtemp_radio', $engtemp_radio );
$t->assign ( 'grktemp_radio', $grktemp_radio );

$t->display('mn_profile_cus_fields.htm');
?>