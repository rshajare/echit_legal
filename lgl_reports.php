<?
if ( !defined( 'SMARTY_DIR' ) ) { include_once( 'init.php' );}
include_once( 'updateable_other_functions.php');

$t->assign ('unid',md5(uniqid(rand(), true)));
header('Content-Type: text/html; charset=utf-8');
$ses_lang = $_SESSION['opt_lang'];
if( $ses_lang == '')  
	$ses_lang = 'english';

$comp_arr = array();
$sql = "SELECT company_name,company_id FROM tbl_lgl_company order by `company_id` asc";
$temp = $db->getAll( $sql );
foreach ($temp as $index => $row) {
	$comp_arr['company_id'][] = $row['company_id'];
	$comp_arr['company_name'][] = $row['company_name'];
}
$t->assign('comp_arr', $comp_arr);

$customer_arr = array();
$sql = "SELECT customer_name,customer_id FROM tbl_lgl_customer order by `customer_id` asc";
	$temp = $db->getAll( $sql );
foreach ($temp as $index => $row) {
	$customer_arr['customer_id'][] = $row['customer_id'];
	$customer_arr['customer_name'][] = $row['customer_name'];
}
$t->assign('customer_arr', $customer_arr);

$branch_arr = array();
$sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch order by `branch_id` asc";
	$temp = $db->getAll( $sql );
foreach ($temp as $index => $row) {
	$branch_arr['branch_id'][] = $row['branch_id'];
	$branch_arr['branch_name'][] = $row['branch_name'];
}
$t->assign('branch_arr', $branch_arr);
//print_r($branch_arr);

$op = $_GET['op'];
$t->assign ( 'op', $op );

$Report_draft_id = $_GET['Report_draft_id'];
$t->assign ( 'Report_draft_id', $Report_draft_id );
//echo $Report_draft_id;

$page_file = array();
$page_file['list_page'] = "lgl_reports.php?op=$op";
$t->assign ( 'page_file', $page_file );

		  $reqcompany_id 		= $_REQUEST['txtcompany_id'];
		  $t-> assign('reqcompany_id',$reqcompany_id);

		  $reqmain_branch_id	= $_REQUEST['txtmain_branch_id'];
		  $t-> assign('reqmain_branch_id',$reqmain_branch_id);
		 
		  $req_branch_name		= fnGetValue("tbl_lgl_branch","branch_name","branch_id=$reqmain_branch_id");
		  $t-> assign('req_branch_name',$req_branch_name);
		  

		  $reqcustomer_id = $_REQUEST['txtcustomer_id'];
		  $t-> assign('reqcustomer_id',$reqcustomer_id);
		  
		  $year=date("Y");
		  $t-> assign('year',$year);

		  $date=date("d/m/Y");
		  $t-> assign('date',$date);
	
		  $main_data = array();
		  $main_sql = "SELECT * FROM tbl_lgl_report_draft where report_name='$Report_draft_id'";
				 //echo $main_sql;
		  $main_temp = $db->getAll( $main_sql );
		  foreach ($main_temp as $index => $main_row) {

				  
				  $data = array();
				  $sql = "SELECT * FROM tbl_lgl_customer 
				          LEFT JOIN tbl_lgl_company on tbl_lgl_customer.company_id = tbl_lgl_company.company_id
				          LEFT JOIN tbl_lgl_complainant on tbl_lgl_customer.complainant = tbl_lgl_complainant.complainant_id
				          LEFT JOIN tbl_lgl_advocate on tbl_lgl_customer.advocate = tbl_lgl_advocate.advocate_id
				          LEFT JOIN tbl_lgl_cheque_return on tbl_lgl_customer.customer_id = tbl_lgl_cheque_return.customer_id
						  LEFT JOIN tbl_lgl_court on tbl_lgl_customer.court = tbl_lgl_court.court_id
						  LEFT JOIN tbl_lgl_legal_notice on tbl_lgl_customer.customer_id = tbl_lgl_legal_notice.customer_id
						  where tbl_lgl_customer.company_id='$reqcompany_id' and tbl_lgl_customer.main_branch_id='$reqmain_branch_id' and tbl_lgl_customer.customer_id='$reqcustomer_id'";
				          //echo $sql;
				  $temp = $db->getAll( $sql );
				  foreach ($temp as $index => $row) {
					 	$row['cust_drawn_on']			= fnGetValue("tbl_lgl_banks","Bankname","BankID='".$row["cust_drawn_on"]."'");
					 	$row['drawn_on']				= fnGetValue("tbl_lgl_banks","Bankname","BankID='".$row["drawn_on"]."'");
					 	$row['ch_deposited_at']			= fnGetValue("tbl_lgl_banks","Bankname","BankID='".$row["ch_deposited_at"]."'");
/*					 	$row['drawn_on_branch']			= fnGetValue("tbl_lgl_banks","Bankname","BankID='".$row["ch_deposited_at"]."'");
					 	$row['ch_deposited_at_branch']			= fnGetValue("tbl_lgl_banks","Bankname","BankID='".$row["ch_deposited_at"]."'");
*/						
						
						$row['agreement_date']			= sqldateout($row['agreement_date']);	
					 	$row['commencinf_from']			= sqldateout($row['commencinf_from']);	
					 	
						$row['terminating_on']			= sqldateout($row['terminating_on']);	
					 	$row['auction_date']			= sqldateout($row['auction_date']);	
					 	$row['cheque_date']				= sqldateout($row['cheque_date']);	
					 	$row['cheque_return_date']		= sqldateout($row['cheque_return_date']);	
					 	$row['ch_deposited_on']			= sqldateout($row['ch_deposited_on']);	
					 	$row['ch_received_date']		= sqldateout($row['ch_received_date']);	
					 	$row['cust_cheque_date']		= sqldateout($row['cust_cheque_date']);	
					 	$row['joining_on']				= sqldateout($row['joining_on']);	
					 	$row['auction_date']			= sqldateout($row['auction_date']);	
					 	$row['cust_cheque_date']		= sqldateout($row['cust_cheque_date']);	
					 	$row['send_on']					= sqldateout($row['send_on']);	
					 	$row['ack_received_on']			= sqldateout($row['ack_received_on']);	
					 	$row['legal_notice_date']		= sqldateout($row['legal_notice_date']);	
					 	$row['received_on']				= sqldateout($row['received_on']);	

						$row['textChitValue']			= NumtoWord($row['chit_value']);	
						$row['textpayment_amount'] 		= NumtoWord($row['payment_amount']);
						$row['textfuture_liability'] 	= NumtoWord($row['future_liability']);
						$row['complainant_age']			=(date('Y')-date('Y',strtotime($row['complainant_dob'])));
						$row['textauction_amount'] 		= NumtoWord($row['auction_amount']);
						$row['prize_amount'] 			= (($row['chit_value'])-($row['auction_amount']));
						$row['textprize_amount'] 		= NumtoWord($row['prize_amount']);
						
						$row['liability_amount'] 		= (($row['chit_period'])-($row['installments_cCleared']))*($row['monthly_sub']);
						$row['textliability_amount']	= NumtoWord($row['liability_amount']);
						$row['textpending_amount'] 		= NumtoWord($row['pending_amount']);
						$row['textmonthly_sub'] 		= NumtoWord($row['monthly_sub']);
						$row['textauction_amount'] 		= NumtoWord($row['auction_amount']);
						$row['textpending_amount'] 		= NumtoWord($row['pending_amount']);
						$row['textch_amount'] 			= NumtoWord($row['ch_amount']);

						$time=strtotime(sqldatein($row['commencinf_from']));
						$row['start_date']=date("d",$time);
						$row['start_month']=date("F",$time);
						$row['start_year']=date("Y",$time);
						
						$time=strtotime(sqldatein($row['terminating_on']));
						$row['end_date']=date("d",$time);
						$row['end_month']=date("F",$time);
						$row['end_year']=date("Y",$time);
						
						$garantor_sql="SELECT * FROM tbl_lgl_guarantor where customer_id='".$row['customer_id']."'"; 
						$garantor_temp = $db->getAll( $garantor_sql );
						foreach ($garantor_temp as $index => $garantor_row) {
							$row['gar_arr'][]=$garantor_row;
						}
						$data[] = $row;
					  
				  } 
				  $t->assign('data', $data);
				  
						  
				$draft			= stripslashes($main_row['report_draft']);	
				$main_data[] = $main_row;
		  } 
		  $t->assign('draft', $draft);
		  $t->assign('main_data', $main_data);

	
		$t->display('lgl_reports.htm');	
	


?>