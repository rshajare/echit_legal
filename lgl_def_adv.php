<?php

include_once ("init.php");
include_once( 'sur_adm_permission.php' );
include_once( 'controllers/lgl_def_adv/classdef_adv.php' );

header('Content-Type: text/html; charset=utf-8');  
$def_adv = new def_adv();
$StatusMessage = "";
switch($_GET['action']){
	case '1' :		
					$def_adv->Adddef_adv();							
					break;	
	case '2' :		
					$def_adv->Editdef_adv();					
					break;	
					
	case '3' :		
					$def_adv->store();					
					break;	
	
	case '4':	
	case 'destroy':	
					$def_adv->destroy($_REQUEST['def_adv_id']);	
					$_SESSION['AlertMessage'] = "Record Deleted Sucessfully";	
					header('Location: lgl_def_adv.php');
					break;	
					
	case '5' :		
					$def_adv->update();	
					break;	
	default:		
					$def_adv->index();
					break;
}

//==========================#  End of Getting records in list page===================================================================
?>