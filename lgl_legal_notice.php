<?php
include_once ("init.php");
include_once( 'sur_adm_permission.php' );
include_once( 'controllers/lgl_legal_notice/classlegal_notice.php' );

header('Content-Type: text/html; charset=utf-8');  
$legal_notice = new legal_notice();
$StatusMessage = "";
switch($_GET['action']){
	case '1' :		
					$legal_notice->Addlegal_notice();							
					break;	
	case '2' :		
					$legal_notice->Editlegal_notice();					
					break;	
					
	case '3' :		
					$legal_notice->store();					
					break;	
	
	case '4':	
	case 'destroy':	
					$legal_notice->destroy($_REQUEST['legal_notice_id']);	
					$_SESSION['AlertMessage'] = "Record Deleted Sucessfully";	
					header('Location: lgl_legal_notice.php');
					break;	
					
	case '5' :		
					$legal_notice->update();	
					break;	
	default:		
					$legal_notice->index();
					break;
}

//==========================#  End of Getting records in list page===================================================================
?>