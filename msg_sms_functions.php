<? 

// +----------------------------------------------------------------------------------------------------------------------------+
// |  File Name : nm_sms functions.php
// |  Version : 2.0 
// |  Create-date : 7, May, 2010
// |  Modify-date : 1, November, 2010
// |  Authors : Niteen Borate & Sachin Kashid                        
// | 
// |  Desciption : functions to sent SMS 
// |  How to use : call 

// |  fnSendSMS($Phone_No,$req_Message,$req_Report_ID){
// |  $Phone_No  = to phone/mobile  number
// |  $req_Message = actual sms to sent
// |  $req_Report_ID = Batch number to track group messaging
// |  
// +------------------------------------------------------------------------------------------------------------------------------
//


function SendSMScurl($URL){
	//echo $URL;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_URL, $URL);
	$response = curl_exec ($ch);
	curl_close ($ch);
	return $response;
}

function SendSMScurl_post_New($URL,$FieldString){
	//echo $URL."<BR>".$FileString;
	$ch = curl_init(); 
	if (!$ch){die("Couldn't initialize a cURL handle");}
	$ret = curl_setopt($ch, CURLOPT_URL,$URL);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);          
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $FieldString);
	$ret = curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    
	
	print_r($ch);
	$curlresponse = curl_exec($ch); // execute
//die();	
	if(curl_errno($ch))
	echo 'curl error : '. curl_error($ch);
	
	if (empty($ret)) {
		// some kind of an error happened
		die(curl_error($ch));
		curl_close($ch); // close cURL handler
	}else{
		$info = curl_getinfo($ch);
		curl_close($ch); // close cURL handler
		return $curlresponse;    //echo "Message Sent Successfully" ;       
	}
}

function SendSMScurl_post($url,$FileString){
	extract($_POST) ;
	//$str = urlencode($FileString) ;
	$str = $FileString ;
	$ch = curl_init();
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_POST,1);
	curl_setopt($ch,CURLOPT_POSTFIELDS,$str);
	$response = curl_exec ($ch);
	curl_close ($ch);
	return $response;
}

function fnSendSMS($Phone_No,$req_Message){
//	echo "in fnsendsms";
	
	$on_off_flag  = FnGetValue("tbl_mn_sms_set","on_off_flag","sms_set_id='sms_set'");
	if ($on_off_flag == 1){
		//echo "in fnsendsms";
		$req_SMS_User_Profile 		= FnGetValue("tbl_mn_sms_set","user_id","sms_set_id='sms_set'");
		$req_SMS_User_Password 		= FnGetValue("tbl_mn_sms_set","password","sms_set_id='sms_set'");
		$req_SMS_Sender 			= urlencode(FnGetValue("tbl_mn_sms_set","sender_name","sms_set_id='sms_set'"));
		$req_SMS_api_id 			= urlencode(FnGetValue("tbl_mn_sms_set","api_id","sms_set_id='sms_set'"));
		
		$reqmax_sms_limit 			= FnGetValue("tbl_mn_sms_set","max_sms_limit","sms_set_id='sms_set'");
		if ($reqmax_sms_limit ==0)  $reqmax_sms_limit = MAX_SMS_LIMIT_ONETIME;  
		$req_Message = urlencode($req_Message);
		
	
		if (SMS_API_TYPE=="nagpurtrade"){
				$req_baseurl = "http://208.101.14.59/api/pushsms.php";
				$req_Report_ID = 1;
				$ph_var = "ph";
				$msg_var = "text";
				$req_Url = $req_baseurl . '?usr='.trim($req_SMS_User_Profile).'&pwd='.trim($req_SMS_User_Password).'&rpt='.$req_Report_ID;
				if ($req_SMS_Sender <> '') $req_Url = $req_Url . '&sndr='.$req_SMS_Sender;				
		}elseif (SMS_API_TYPE=="smsgupshup"){
				$req_baseurl = "http://enterprise.smsgupshup.com/GatewayAPI/rest";
				$ph_var = "send_to";
				$msg_var = "msg";
				$req_Url = $req_baseurl . '?method=SendMessage&msg_type=TEXT&userid='.trim($req_SMS_User_Profile).'&auth_scheme=plain&password='.trim($req_SMS_User_Password).'&v=1.1&format=text';
				if ($req_SMS_Sender <> '') $req_Url = $req_Url . '&mask='.$req_SMS_Sender;
		}elseif (SMS_API_TYPE=="9nodes"){
				$req_baseurl = "http://www.9nodes.com/API/sendsms.php";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "to";
				$msg_var = "msg";
				$req_Url = $req_baseurl . '?method=SendMessage&type=1&username='.trim($req_SMS_User_Profile).'&dlr_url='.$our_url.'&password='.trim($req_SMS_User_Password);
				if ($req_SMS_Sender <> '') $req_Url = $req_Url . '&from='.$req_SMS_Sender;
		
		}elseif (SMS_API_TYPE=="rapidsms"){
			//http://trans.rapidsms.in/api/web2sms.php?workingkey=Aa74f7bf59e1296276c24c84e6ae82030&to=919766177773&sender=EKDANT&message=Dear%20Sir%20/%20Madam,%20Auction%20in%20monthly%20chit%20Group%20No.%20EK%2002%20will%20be%20held%20on%2015/10/2012,%20At%2002:00:00%20PM%20Please%20come%20before%2010%20minutes%20Regards%20EKDANTA%20CHIT%20FUND%20PVT.%20LTD%20contact%20no.-%2007172272227
			$req_baseurl = "http://trans.rapidsms.in/api/web2sms.php";
			$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
			$ph_var = "to";
			$msg_var = "message";
			$req_Url = $req_baseurl . '?workingkey='.trim($req_SMS_User_Profile).'&sender='.$req_SMS_Sender;
		}elseif (SMS_API_TYPE=="msandesh"){
		
				//http://isms2api.rapidsms.in/desk2web/SendSMS.aspx?UserName=rapid6&password=rapid123&MobileNo=919273635953&SenderID=rapid&CDMAHeader=rapid&Message=test%20sms&isFlash=false
				$req_baseurl = "http://transapi.msandesh.in/desk2web/SendSMS.aspx";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "MobileNo";
				$msg_var = "Message";
				$req_Url = $req_baseurl . '?isFlash=false&UserName='.trim($req_SMS_User_Profile).'&SenderID='.$req_SMS_Sender.'&CDMAHeader='.$req_SMS_Sender.'&password='.trim($req_SMS_User_Password);
				//if ($req_SMS_Sender <> '') $req_Url = $req_Url . '&from='.$req_SMS_Sender;
		
		}elseif (SMS_API_TYPE=="msandesh_pro"){    
		
				//http://isms2api.rapidsms.in/desk2web/SendSMS.aspx?UserName=rapid6&password=rapid123&MobileNo=919273635953&SenderID=rapid&CDMAHeader=rapid&Message=test%20sms&isFlash=false
				$req_baseurl = "http://api.msandesh.in/desk2web/SendSMS.aspx";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "MobileNo";
				$msg_var = "Message";
				$req_Url = $req_baseurl . '?isFlash=false&UserName='.trim($req_SMS_User_Profile).'&SenderID='.$req_SMS_Sender.'&CDMAHeader='.$req_SMS_Sender.'&password='.trim($req_SMS_User_Password);
				//if ($req_SMS_Sender <> '') $req_Url = $req_Url . '&from='.$req_SMS_Sender;
					
		
		
		}elseif (SMS_API_TYPE=="bulksms-service.com"){
		
				//http://hp.bulksms-service.com/sms/user/urlsms.php?username=HP0332&pass=uday123&senderid=06000&message=Test msg from CAPTO&dest_mobileno=919273635953,919986347300&response=Y
				$req_baseurl = "http://hp.bulksms-service.com/sms/user/urlsms.php";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "dest_mobileno";
				$msg_var = "message";
				$req_Url = $req_baseurl . '?response=y&username='.trim($req_SMS_User_Profile).'&senderid='.$req_SMS_Sender.'&pass='.trim($req_SMS_User_Password);
				//if ($req_SMS_Sender <> '') $req_Url = $req_Url . '&from='.$req_SMS_Sender;	

		
		}elseif (SMS_API_TYPE=="Technopenta"){
		
				//http://8.6.95.141/smpp/sendsms?username=manishkh&password=manish321&to=9273635953&udh=&from=123&text=test
				$req_baseurl = "http://8.6.95.141/smpp/sendsms";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "to";
				$msg_var = "text";
				$req_Url = $req_baseurl . '?username='.trim($req_SMS_User_Profile).'&from='.$req_SMS_Sender.'&password='.trim($req_SMS_User_Password);
				//if ($req_SMS_Sender <> '') $req_Url = $req_Url . '&from='.$req_SMS_Sender;	
		
		
		}elseif (SMS_API_TYPE=="ihante"){
				//http://103.16.101.52:8080/sendsms/bulksms?username=imsp-manishk&password=kfcfpl22&type=0&dlr=1&destination=91NUMBER&source=KFCHIT&message=MESSAGE
			    $req_baseurl = "103.16.101.52/sendsms/bulksms";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "destination";
				$msg_var = "message";
				$FileString   = '?type=0&dlr=1&username='.trim($req_SMS_User_Profile).'&source='.$req_SMS_Sender.'&password='.trim($req_SMS_User_Password);
				$FieldString  = 'type=0&dlr=1&username='.trim($req_SMS_User_Profile).'&source='.$req_SMS_Sender.'&password='.trim($req_SMS_User_Password);
				
				$req_Url = $req_baseurl .$FieldString ;
				//if ($req_SMS_Sender <> '') $req_Url = $req_Url . '&from='.$req_SMS_Sender;		
				
		}elseif (SMS_API_TYPE=="dndopen"){
		
				//http://dndopen.dove-sms.com/SMSAPI.jsp?username=username&password=password&sendername=ABC&mobileno=919999999999&message=Hello 					
				//http://dndopen.dove-sms.com/TransSMS/SMSAPI.jsp?username=saptarushi&password=saptarushi&sendername=SAPTAR&mobileno=919867959301&message=Hello

				$req_baseurl = "dndopen.dove-sms.com/TransSMS/SMSAPI.jsp";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "mobileno";
				$msg_var = "message";
				$req_Url = $req_baseurl . '?isFlash=false&username='.trim($req_SMS_User_Profile).'&sendername='.$req_SMS_Sender.'&password='.trim($req_SMS_User_Password);
				//if ($req_SMS_Sender <> '') $req_Url = $req_Url . '&from='.$req_SMS_Sender;	
		}elseif (SMS_API_TYPE=="smscountry"){
		
				//http://dndopen.dove-sms.com/SMSAPI.jsp?username=username&password=password&sendername=ABC&mobileno=919999999999&message=Hello 					
				$req_baseurl = "http://api.smscountry.com/SMSCwebservice_Bulk.aspx";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "mobilenumber";
				$msg_var = "message";
				$req_Url = $req_baseurl . '?mtype=N&DR=Y&User='.trim($req_SMS_User_Profile).'&sid='.$req_SMS_Sender.'&passwd='.trim($req_SMS_User_Password);
				//if ($req_SMS_Sender <> '') $req_Url = $req_Url . '&from='.$req_SMS_Sender;	
		}elseif (SMS_API_TYPE=="sinfini"){
		
				//http://<SMS_Service_URL>/api/web2sms.php?workingkey=<API_KEY>&to=9900XXXXXX&sender=DEMO&message=THIS+IS+A+TEST+SMS 
				//http://alerts.sinfini.com/api/web2sms.php?workingkey=57164k2ia707ws93895y&sender=<senderid>&to=<mobilenumber1,mobilenumber2,....>&message=<textmessage>
				
				$req_baseurl = "http://alerts.sinfini.com/api/web2sms.php";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "to";
				$msg_var = "message";
				$req_Url = $req_baseurl . '?workingkey='.trim($req_SMS_User_Profile).'&sender='.$req_SMS_Sender;
				//if ($req_SMS_Sender <> '') $req_Url = $req_Url . '&from='.$req_SMS_Sender;
		
		}elseif (SMS_API_TYPE=="swift"){
		
				//http://<SMS_Service_URL>/api/web2sms.php?workingkey=<API_KEY>&to=9900XXXXXX&sender=DEMO&message=THIS+IS+A+TEST+SMS 
				//http://alerts.sinfini.com/api/web2sms.php?workingkey=57164k2ia707ws93895y&sender=<senderid>&to=<mobilenumber1,mobilenumber2,....>&message=<textmessage>
				
				$req_baseurl = "http://swift.antsms.in/api/web2sms.php";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "to";
				$msg_var = "message";
				$req_Url = $req_baseurl . '?workingkey='.trim($req_SMS_User_Profile).'&sender='.$req_SMS_Sender;
				//if ($req_SMS_Sender <> '') $req_Url = $req_Url . '&from='.$req_SMS_Sender;	
		}elseif (SMS_API_TYPE=="whitelist"){
		
				//http://whitelist.smsapi.org/SendSMS.aspx?UserName=xxxx&password=xxxx&MobileNo=xxxx,xxxx,xxxx&SenderID= xxxx&CDMAHeader=xxxx&Message=xxxx
				$req_baseurl = "http://whitelist.smsapi.org/SendSMS.aspx";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "MobileNo";
				$msg_var = "Message";
				$req_Url = $req_baseurl . '?CDMAHeader=false&UserName='.trim($req_SMS_User_Profile).'&SenderID='.$req_SMS_Sender.'&password='.trim($req_SMS_User_Password);
				//if ($req_SMS_Sender <> '') $req_Url = $req_Url . '&from='.$req_SMS_Sender;	
		}elseif (SMS_API_TYPE=="dndopen_new"){

                //http://mainadmin.dove-sms.com/sendsms.jsp?user=saptaru&password=D@ve12&mobiles=9930451731&sms=msgbody&senderid=SAPTAR
				$req_baseurl = "http://mainadmin.dove-sms.com/sendsms.jsp";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "mobiles";
				$msg_var = "sms";
				$req_Url = $req_baseurl . '?senderid=SAPTAR&username='.trim($req_SMS_User_Profile).'&user='.$req_SMS_Sender.'&password='.trim($req_SMS_User_Password);
				//if ($req_SMS_Sender <> '') $req_Url = $req_Url . '&from='.$req_SMS_Sender;	
		
		}elseif (SMS_API_TYPE=="insignsms"){
		         
               // http://web.insignsms.com/api/sendsms?username=ABC&password=XYZ&senderid=TEST&message=hi%20Ravindra&numbers=9870101010,8108191919&dndrefund=1
				$req_baseurl = "web.insignsms.com/api/sendsms";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "numbers";
				$msg_var = "message";
				$req_Url = $req_baseurl . '?senderid=JUPTER&username='.trim($req_SMS_User_Profile).'&user='.$req_SMS_Sender.'&password='.trim($req_SMS_User_Password);
				//if ($req_SMS_Sender <> '') $req_Url = $req_Url . '&from='.$req_SMS_Sender;
		}elseif (SMS_API_TYPE=="rapidsms_pro"){
		         
             // http://promo.rapidsms.in/api/web2sms.php?username=&password=&sender=BULKSMS&message=test&to=99XXXX999
			// http://promo.rapidsms.in/api/web2sms.php?workingkey=<API_KEY>&to=<MOBILENUMBER>&sender=BULKSMS&message=<MESSAGE>
				$req_baseurl = "http://promo.rapidsms.in/api/web2sms.php";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "to";
				$msg_var = "message";
				$req_Url = $req_baseurl . '?sender=BULKSMS&username='.trim($req_SMS_User_Profile).'&user='.$req_SMS_Sender.'&password='.trim($req_SMS_User_Password);			
		
		}elseif (SMS_API_TYPE=="smshorizon"){
			//http://smshorizon.co.in/api/sendsms.php?user=".$user."&apikey=".$apikey."&mobile=".$mobile."&senderid=".$senderid."&message=".$message."&type=".$type.""
				$req_baseurl = "http://smshorizon.co.in/api/sendsms.php";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "mobile";
				$msg_var = "message";
				$req_Url = $req_baseurl . '?user='.trim($req_SMS_User_Profile).'&apikey='.$req_SMS_api_id.'&senderid='.$req_SMS_Sender.'&type=txt';			
		
		}elseif (SMS_API_TYPE=="mobicomm_dove"){
				//http://mobicomm.dove-sms.com/mobicomm//submitsms.jsp?user=saptaru&key=2f3893b934XX&mobile=+919890486956&message=test sms&senderid=abcdef&accusage=1
				$req_baseurl = "http://mobicomm.dove-sms.com/mobicomm//submitsms.jsp";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "mobile";
				$msg_var = "message";
				$req_Url = $req_baseurl . '?user='.trim($req_SMS_User_Profile).'&key='.$req_SMS_api_id.'&senderid='.$req_SMS_Sender.'&accusage=1';			
		}elseif (SMS_API_TYPE=="smsjust"){
				$req_baseurl = "http://www.smsjust.com/sms/user/urlsms.php";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "dest_mobileno";
				$msg_var = "message";
				$req_Url = $req_baseurl . '?username='.trim($req_SMS_User_Profile).'&pass='.trim($req_SMS_User_Password).'&senderid='.$req_SMS_Sender.'&response=Y';			
		}elseif (SMS_API_TYPE=="rmtech"){
				$req_baseurl = "http://www.sms.rmtechnology.in/api/smsapi.aspx";
				$our_url = urlencode(WEBSITE_PATH . "/msg_sms_track.php");
				$ph_var = "to";
				$msg_var = "message";
				$req_Url = $req_baseurl . '?username='.trim($req_SMS_User_Profile).'&password='.trim($req_SMS_User_Password).'&from='.$req_SMS_Sender.'&response=Y';			
		}				
		// if multiple phone		
		if(is_array($SMSPhone_No)){
			$req_Url = $req_Url . '&$msg_var='.$req_Message;
			$req_cnt = count($SMSPhone_No);
			$SMSPhone_No_send = "";
			if ($req_cnt > $reqmax_sms_limit ){ 
				echo "Exceeded  the maximum limit (". MAX_SMS_LIMIT_ONETIME.")! plz contact administrator to increase it";
			}else{
				$msg_cnt = 0; $req_cnt = 500;
				for($i=0;$i<$req_cnt ;$i++){
					if($SMSPhone_No_send != "") $SMSPhone_No_send .= "|";
					$SMSPhone_No_send .= $SMSPhone_No[$i];
					if((($i % 200) == 0)&&($i != 0)){
						$SMSPhone_No_send = "";
						if (TMP_LOCAL == 1 ) {
						}else {
							$FileString = $req_Url.'&'.$ph_var.'='.$SMSPhone_No_send.",9766177773";
						}
						$req_Report_ID++;
					}
				  }
				
				  $FileString = $req_Url.'&'.$ph_var.'='.$SMSPhone_No_send;
				  //echo "<BR>".$FileString;
				  $req_Report_ID++;
				  if (TMP_LOCAL == 1 ) {
						echo  "In Else".$req_Url;  	
				  }else {
					if (SMS_API_TYPE=="ihante"){	
						$FieldString	=$FieldString . '&'.$ph_var.'='.$Phone_No.'&'.$msg_var.'='.$req_Message;									
						SendSMScurl_post_New($req_baseurl,$FieldString) ;     // send actual sms
					}else{
						$tmpResponse= SendSMScurl($req_Url);      // send actual sms				
					}
					//$tmpResponse = SendSMScurl_post($url,$FileString); // By Post method
					return $tmpResponse;
				 }
				 
			} // end of else if total phone is less than limit
			
		
		}else{  // for single phone
			$req_Url = $req_Url . '&'.$ph_var.'='.$Phone_No.'&'.$msg_var.'='.$req_Message;
			if (TMP_LOCAL == 1 ) { 	echo $req_Url;  }		// print on screen if local
			else{ 
				if (SMS_API_TYPE=="ihante"){	
					$FieldString	=$FieldString . '&'.$ph_var.'='.$Phone_No.'&'.$msg_var.'='.$req_Message;				
					SendSMScurl_post_New($req_baseurl,$FieldString) ;     // send actual sms
				}else{
					$tmpResponse= SendSMScurl($req_Url);      // send actual sms				
				}
				return $tmpResponse; //echo $req_Url;  
			}
			
		}  // end of if single phone
		
	} // end of ONN OFF
} // end of function



// function to get Balance
function fnCreditBalance(){
	$on_off_flag  = FnGetValue("tbl_mn_sms_set","on_off_flag","sms_set_id='sms_set'");
	if ($on_off_flag == 1){
		$req_SMS_User_Profile 	= FnGetValue("tbl_mn_sms_set","user_id","sms_set_id='sms_set'");
		$req_SMS_User_Password 	= FnGetValue("tbl_mn_sms_set","password","sms_set_id='sms_set'");
		$req_SMS_Sender 		= urlencode(FnGetValue("tbl_mn_sms_set","sender_name","sms_set_id='sms_set'"));
		//$req_baseurl 			= FnGetValue("tbl_mn_sms_set","api_url","sms_set_id='sms_set'");
		if (SMS_API_TYPE=="nagpurtrade"){
			$req_baseurl 			= "http://208.101.14.59/api/balance.php";
			$req_Url = $req_baseurl . '?usr='.trim($req_SMS_User_Profile).'&pwd='.trim($req_SMS_User_Password);
		}elseif(SMS_API_TYPE=="smsgupshup"){
			$req_baseurl 			= "http://208.101.14.59/api/balance.php";
			$req_Url = $req_baseurl . '?usr='.trim($req_SMS_User_Profile).'&pwd='.trim($req_SMS_User_Password);
		}
		
		if (TMP_LOCAL == 1 ) {
			//echo $req_Url;  	
			//$tmpResponse= SendSMScurl($req_Url); 		// send actual sms
			return $tmpResponse;
		}else {// print on screen if local
			$tmpResponse= SendSMScurl($req_Url); 		// send actual sms
			return $tmpResponse;
		}
	}// onn off flag
	
}// end of function



function send_reminder_to_subcriber($flag){

		$tmplocal = $flag;
		$reqfrm_email  = FnGetValue("tbl_mn_app_setting","field_value","field_name='admin_email_id'");
		$sql_first  = "select * from tbl_news_letter_details where send_date <= CURDATE() and sent_msg = 0";
		//echo $sql_first;
		$temp_first = mysql_query($sql_first) or die(mysql_error().$sql_first);
		$from    = FnGetValue("tbl_mn_app_setting","field_value","field_name='admin_email_id'");
		while($first_row = mysql_fetch_array($temp_first)){
		
		}
		$up_sql1 = "Update tbl_news_letter_details SET sent_msg = '1' where (send_date <= CURDATE() and sent_msg = 0) ";
		$up_res1 = mysql_query(mysql_query($up_sql1)) ;//or die(mysql_error()."<br>".$up_sql1); 
}



//============================================================================================================
// Send SMS To all group members 

function fnSendMSG_Prize_Group($reqtxtGrp_No,$reqtxtAuction_No,$reqtxtTicket_No){

		global $t;
		$main_data = array();
		$main_sql = "select subdirdetails.*,applicantdetails.GroupNo,applicantdetails.Ticketno from subdirdetails,applicantdetails where applicantdetails.SubCodeNo = subdirdetails.SubID 
						and applicantdetails.GroupNo = '$reqtxtGrp_No' ";
		//echo $main_sql;				
		$reqtxtAuction_Date  = sqldateout(fngetvalue("auction","left(AuctionDateTime,10)","GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtAuction_No'"));			
		$reqtxtAuction_time  = SQLDateOut_Time(FnGetValue("auction","AuctionDateTime","GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtAuction_No'"));
		$reqtxtAuction_time  = substr($reqtxtAuction_time,11);
		
		$req_AppID = fngetvalue("applicantdetails","AppID","GroupNo = '$reqtxtGrp_No' and TicketNo = '$reqtxtTicket_No' ");	
		$reqtxtFullName  	 = fngetvalue("subdirdetails,applicantdetails","FullName","applicantdetails.SubCodeNo = subdirdetails.SubID and applicantdetails.AppID=$req_AppID");
	
		$main_tmp = mysql_query($main_sql) or die(mysql_error().$main_sql);
//		foreach($main_tmp as $main_row){
		while($main_row=mysql_fetch_array($main_tmp)){

					

			$req_Report_ID = 1;
			$Phone_No = $main_row["MobNoCorres"];
			$req_Message = FnShowEmail_Lang("dear_sir") ." ". $main_row["FullName"] . " ".
							FnShowEmail_Lang("pr_auction_in_monthly_chit")." ". $reqtxtGrp_No . " ".
							FnShowEmail_Lang("was_held_on") ." ". $reqtxtAuction_Date ." ".FnShowEmail_Lang("at")." ". $reqtxtAuction_time ." ".
							FnShowEmail_Lang("prized_amt") ." ". $reqtxtAuction_Amt ." ".FnShowEmail_Lang("prized_to") ." ". $reqtxtFullName ." ".FnShowEmail_Lang("having_tkt_no")." ".$reqtxtTicket_No." ". FnShowEmail_Lang("dividend_is")." ".$reqlasdevidentt." ". FnShowEmail_Lang("regards");
			fnSendSMS($Phone_No,$req_Message,$req_Report_ID);
			
			$SubID = $main_row["SubID"];
			$reqpersonnel_id = FnGetValue("tbl_sur_sign_up","personnel_id","ref_user_code=$SubID");
			$reqemail    = FnGetValue("tbl_sur_sign_up","email","personnel_id=$reqpersonnel_id");
			$firstname 	 = FnGetValue("tbl_sur_sign_up","first_name","personnel_id=$reqpersonnel_id");
		    $shop_url    = WEBSITE_PATH . "index.php" ; 
			$shop_name   = FnGetValue("tbl_mn_lhf","category_tag","type='lhf' and name='shop_name' and lang='english'");
			
			 //send email 
			 $to = $reqemail;
			 $t->assign('firstname', $firstname);
			 $t->assign('email', $reqemail);
			 $t->assign('prize_entry_msg', $req_Message);
			 $t->assign('shop_url', $shop_url);
			 $t->assign('shop_name', $shop_name);
			 $msg = $t->fetch('emailtemplates/prize_entry.html');
			 if (eregi ("<title>(.*)</title>", $msg, $out)) { $subject = $out[1];  }
			 fnSend_Email_Pear($to,$from,$subject,$msg,$attch_id);
			 // end email
		}
			 
	
}// end of fnSendMSG_Prize_Group



function fnSendMSG_Intimation_Group($reqtxtGrp_No,$reqtxtInst_No,$reqtxtToTicketNo=0,$reqtxtFrmTicketNo=0){

		$t = new osDate_Smarty;
		global $db ;
		$ses_lang=$_SESSION["opt_lang"];
		
	    $main_sql = "select subdirdetails.*,chit_share_perc,applicantdetails.GroupNo,applicantdetails.Ticketno,main_branch_id from subdirdetails,applicantdetails where applicantdetails.SubCodeNo = subdirdetails.SubID 
						and applicantdetails.GroupNo = '$reqtxtGrp_No' ";
		if (($reqtxtToTicketNo<>0) && ($reqtxtFrmTicketNo<>0)){
			$main_sql .= "and (applicantdetails.Ticketno >= '$reqtxtFrmTicketNo' and applicantdetails.Ticketno <= '$reqtxtToTicketNo')";
		}
						
		//echo $main_sql;				
		$main_tmp = mysql_query($main_sql) or die(mysql_error().$main_sql);
//		foreach($main_tmp as $main_row){
		while($main_row=mysql_fetch_array($main_tmp)){
				$sub_sql = "select SubCodeNo,Ticketno,prizeamount from applicantdetails where GroupNo = '$reqtxtGrp_No' and prizeinstno <> '0' order by prizeinstno desc limit 0,1";
				$sub_tmp = $db->getAll($sub_sql);
				foreach($sub_tmp as $sub_row){
					$main_row['current_date']    = date('d/m/Y');
					$main_row['LAST_PRIZED_TKT'] = $sub_row['Ticketno'];
					$main_row['BID_AMOUNT_Rs']   = $sub_row['prizeamount'];
					$main_row['DIVIDEND_Rs']     = FnGetValue("auction","lasdevidentt","GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtInst_No'");
				}
				$main_row['InstNo']     = $reqtxtInst_No;
				$main_row['Month']     = FnGetValue("auction","Month","GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtInst_No'");
				
				$Collection_Code1 = FnGetValue("subdirdetails","CollCode","SubID = '".$main_row['SubID']."'");
				$main_row['Collection_Code'] = FnGetValue("tbl_coll_code","coll_code_name","main_coll_code_id = '$Collection_Code1'");
				
				$main_row['Group_Reg_No'] = FnGetvalue("groupdetails","ChitRegNo","GroupNo = '$reqtxtGrp_No'");
				$main_row['chitvalue'] = FnGetValue("groupdetails","ChitValue","GroupNo = '$reqtxtGrp_No'");
				
				$main_row['AuctionDate']  = sqldateout(FnGetValue("auction","left(AuctionDateTime,10)","GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtInst_No'"));
				$main_row['AuctionTime']  = SQLDateOut_Time(FnGetValue("auction","AuctionDateTime","GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtInst_No'"));
				$main_row['AuctionTime']  = substr($main_row['AuctionTime'],11);
				
				$main_row['branch_address']  = FnGetValue("tbl_branch","branch_address","lang='$ses_lang' and main_branch_id = ".$main_row['main_branch_id']);	
		
				if ($main_row['branch_address'] == '0') $main_row['branch_address'] = "";
				$chit_share_perc = $main_row['chit_share_perc'];
				$main_row['Subscription'] = FnGetValue("auction","((Subcription * $chit_share_perc ) - (lasdevidentt * $chit_share_perc ))","GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtInst_No'");
				
				$main_row['arrears'] = FnGetValue("auction,subscription_details","subscription_details.arrears","subscription_details.Auction_ID = auction.Auction_ID and auction.GroupNo = '$reqtxtGrp_No' and auction.InstNo = '$reqtxtInst_No' and subscription_details.TicketNo = ".$main_row['Ticketno']);	
			   
				$Sub_total_due = fngetvalue("(applicantdetails INNER JOIN auction ON applicantdetails.GroupNo = auction.GroupNo)	LEFT JOIN subscription_details AS `on` ON ( auction.Auction_ID = `on`.Auction_ID   AND `on`.Ticketno = applicantdetails.Ticketno )",
										   "sum( auction.Subcription * chit_share_perc)", "AuctionDateTime IS NOT NULL AND Subscription_ID IS NULL AND applicantdetails.GroupNo = '$reqtxtGrp_No' AND 
											applicantdetails.TicketNo = '".$main_row['Ticketno']."' AND auction.InstNo < ".$reqtxtInst_No." AND applicantdetails.SubCodeNo = '".$main_row['SubID']."' GROUP BY applicantdetails.GroupNo, applicantdetails.Ticketno");		
				
				$main_row['arrears'] = $Sub_total_due;
				$main_row['total']   = ($main_row['Subscription'] + $main_row['arrears']);
				$main_row['LastDayPay']  = sqldateout(FnGetValue("auction","LastDateofPayment","GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtInst_No'"));
				$main_row['GroupNo'] = $reqtxtGrp_No;
				if($main_row['arrears'] > 0){
					$main_row['msg2'] = '*';
					$main_row['msg1'] = 'Please ignore this, if already paid';
				}
				$main_row['Disp_Line']="";
				if (($cnt%2)==0) $main_row['Disp_Line']="No";
				$main_row['cnt']=$cnt;
				$main_data[]  = $main_row;
				
			$SubID = $main_row['SubID'];
			$reqtxtFullName  = fngetvalue("subdirdetails","FullName","SubID= '$SubID'");
			
			//send sms start
			$req_Report_ID = 1;
			$Phone_No = $main_row["MobNoCorres"];
			//echo $Phone_No;
			$req_Message = FnShowEmail_Lang("dear_sir") ." ". $main_row["FullName"] ." ". 
			FnShowEmail_Lang("auction_in_monthly_chit")." ". $main_row['GroupNo'] ." ". 
			FnShowEmail_Lang("will_be_held_on") ." ". $main_row["AuctionDate"] . "-" . $main_row['AuctionTime'] ." ". 
			FnShowEmail_Lang("plz_pay_subscription_amt") ." ".$main_row['total'] . " " . FnShowEmail_Lang("before") ." ".
			$main_row['LastDayPay']." ".FnShowEmail_Lang("ignr_if_alredy_paid")." ". FnShowEmail_Lang("regards");			
		    fnSendSMS($Phone_No,$req_Message,$req_Report_ID);			
            //send sms end  
	
			/* sending email */
			$main_data = array();
			$main_data[]  = $main_row;
			$t->assign ( 'main_data', $main_data );
			
			$personnel_id_temp = fnGetValue("tbl_sur_sign_up","personnel_id","ref_user_code = '".$main_row['SubID']."'");
			$to = FnGetvalue("tbl_sur_sign_up","email","personnel_id = '$personnel_id_temp'");
			$msg = $t->fetch('emailtemplates/intimation_letter.html');
			//echo $msg;
			if (eregi ("<title>(.*)</title>", $msg, $out)) { $subject = $out[1]; }
			fnSend_Email_Pear($to,$from,$subject,$msg,$attch_id);
			/* sending email end*/
			 // end email
		}
}// end of fnSendMSG_Prize_Group


//==============================================================================================================


function fnvalidate_mobile_no($req_no){
	if(strlen($req_no) < 10){
		return 0;
	}else{
		$first_digit = substr($req_no,0,1);
		if(($first_digit == 8)||($first_digit == 9))	return 1;
	}
}
function fnFindPhone_No($req_str){
	$req_chk_comma 		= stripos($req_str, ',');
	$req_chk_semi_comma = stripos($req_str, ";");
	$req_chk_slash 		= stripos($req_str, '/');
	if(($req_chk_comma == "")&&($req_chk_semi_comma == "")&&($req_chk_slash == "")){
		$req_Phone_No = $req_str;
	}else{
		$req_Phone_No = array();
		if($req_chk_comma != "")		$req_Phone_No = split_phone_str('/,',$req_str,$req_Phone_No);
		if($req_chk_semi_comma != "")	$req_Phone_No = split_phone_str('/;',$req_str,$req_Phone_No);
		if($req_chk_slash != "")		$req_Phone_No = split_phone_str('/',$req_str,$req_Phone_No);
	}
	return $req_Phone_No;
}
function split_phone_str($split_by,$req_str,$req_Phone_No){
	$req_array = array();
	$req_start_cnt = count($req_Phone_No);
	$req_array = split('/',$req_str);
	if(is_array($req_array)){
		for($i=0;$i<count($req_array);$i++){
			$req_Phone_No[$req_start_cnt] = $req_array[$i];
			$req_start_cnt++;
		}
	}
	return $req_Phone_No;
}
function fnCheck_mobile_no_in_string($req_str){
	$mobile_flag = 0;
	$req_array = fnFindPhone_No($req_str);
	if(is_array($req_array)){
		for($i=0;$i<count($req_array);$i++){
			$req_Mob_no = $req_array[$i];
			$req_chk = fnvalidate_mobile_no($req_Mob_no);
			if($req_chk == 1){
				$mobile_flag = 1;
			}
		}
	}else{
		$req_chk = fnvalidate_mobile_no($req_array);
		if($req_chk == 1){
			$mobile_flag = 1;
		}
	}
	return $mobile_flag;
}



function fnFindMobile_No($req_str){
	if($req_str != ""){
		$mobile_flag = 0;
		$i = 0;
		$req_array = fnFindPhone_No($req_str);
		if(is_array($req_array)){
			for($i=0;$i<count($req_array);$i++){
				$req_Mob_no = $req_array[$i];
				$req_chk 	= fnvalidate_mobile_no($req_Mob_no);
				if($req_chk == 1){
					$mobile_no[$i] = substr($req_Mob_no,0,10);
					$i++;
				}
			}
		}else{
			$req_chk = fnvalidate_mobile_no($req_array);
			if($req_chk == 1){
				$mobile_no[$i] = substr($req_str,0,10);
				$i++;
			}
		}
	}
	return $mobile_no;
}


?>