<?php
session_start();
//require(dirname(__FILE__)."/class.simplecaptcha.php");
require_once('class.simplecaptcha.php');
/*
*****CONFIGURATION STARTS*****
*/
//Background Image
$config['BackgroundImage'] = "white.png";
//Background Color- HEX
$config['BackgroundColor'] = "FFFFFF";
//image height - same as background image
$config['Height']=30;
//image width - same as background image
$config['Width']=100;
//text font size
$config['Font_Size']=20;
//text font style
$config['Font']=dirname(__FILE__)."/lucon.ttf";
//text angle to the left
$config['TextMinimumAngle']=15;
//text angle to the right
$config['TextMaximumAngle']=30;
//Text Color - HEX
$config['TextColor']='FF0000';
//$config['TextColor']='#FFFFFF';
//Number of Captcha Code Character
$config['TextLength']=4;
//Background Image Transparency
$config['Transparency']=70;
/*
*******CONFIGURATION ENDS******
*/
//Create a new instance of the captcha
$captcha = new SimpleCaptcha($config);
//Save the code as a session dependent string
$_SESSION['string'] = $captcha->Code;
?>