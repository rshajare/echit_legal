<?php

/*
$arr_own_proof=array('light_bill'=>get_lang("config_data_light_bill"),
                     'soc_main_reciept'=>get_lang("config_data_soc_main_reciept"),
                     'sale_agreement'=>get_lang("config_data_sale_agreement"),
					 'autho_comp_dire'=>get_lang("config_data_autho_comp_dire")
					);
*/
$arr_own_proof=array(
					'photo'=>"Photo",
					'pan_card'=>"Pan Card",
					'ratation_card'=>"Ratation Card",
					'voter_id'=>"Voter ID Card",
					'driving_licence'=>"Driving Licence",
					'rent_agreement'=>"Rent Agreement",
					'light_bill'=>"Light Bill",
                     'soc_main_reciept'=>"Society Maintenance Receipt",
                     'sale_agreement'=>"Sales Agreement",
					 'autho_comp_dire'=>"Authorised Company Director Letter",
					 'other'=>"Other"
					);
$t->assign ( 'arr_own_proof', $arr_own_proof );

?>