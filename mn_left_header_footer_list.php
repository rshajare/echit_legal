<? if ( !defined( 'SMARTY_DIR' ) ) { include_once( 'init.php' );}
$t->assign ('unid',md5(uniqid(rand(), true)));
header('Content-Type: text/html; charset=utf-8');

$newtxtmainkey = $_REQUEST['txtmainkey'];
$txtnew_msg    = $_REQUEST['new_msg'];
$t-> assign('tmplocal',$tmplocal);

$rpage =basename(__FILE__);
$rqueryString = $_SERVER['QUERY_STRING']; 
$rpage = $rpage . "?".$rqueryString;
$rpage = urlencode($rpage);
$t-> assign('rpage',$rpage);
 
//===================== Select appropriate language =======================
$lang_pri = 'english';

if (isset($_POST["txt_lang_sec"])) 	$lang_sec = $_POST["txt_lang_sec"];	elseif (isset($_GET["lang_sec"])) $lang_sec = $_GET["lang_sec"];	
else{ if ($_SESSION["tmp_opt_lang_sec"] <> "") $lang_sec= $_SESSION["tmp_opt_lang_sec"]; else $lang_sec = FnGetValue("tbl_lang","language","language<>'$lang_pri'"); }

$_SESSION["tmp_opt_lang_sec"] = $lang_sec;
$tmp_opt_lang_sec_key = FnGetValue("tbl_lang","google_key","language = '$lang_sec'"); 
$t-> assign('tmp_opt_lang_sec_key',$tmp_opt_lang_sec_key);

$t-> assign('lang_sec',$lang_sec);
$t-> assign('lang_pri',$lang_pri);

// list of other languages except lang_pri i.e english code decalared in init 
//$t->assign ( 'lang_sec_list', $lang_sec_list );
//===================end Select appropriate language=================================



//----------------------------------------------------------------------------------------
//=================== Adding new multilanguage message ===================================
//----------------------------------------------------------------------------------------

/*if($txtnew_msg<>''){
	if($newtxtmainkey<>''){
		$newtxtmainkey = trim($_POST[ 'txtmainkey' ]);
		$newdescr      = trim($_POST[ 'txtdescr' ]);
		
		//adding english message
		$sqlins = "Insert into tbl_mn_lhf values(NULL,'english','$newtxtmainkey','','$newdescr')";
		//	$sqlins = "Insert into tbl_mn_lhf values(NULL,'$category_name','$main_id','$hierno','$order_no','$lang','$reqMenu_Type','$page_type','0','0','$Category_Tag')";
		echo $sqlins;
		$result = mysql_query($sqlins) or die(mysql_error().$sqlins);
		
		//adding greek message	
		$sqlins = "Insert into tbl_mn_lhf values(NULL,'greek','$newtxtmainkey','','')";
		$result = mysql_query($sqlins) or die(mysql_error().$sqlins);
		$t->assign ( 'result', 1);
	}
}
*/

//----------------------------------------------------------------------------------------
//===================End of adding new multilanguage message =============================
//----------------------------------------------------------------------------------------

$op = $_GET['op'];

$t-> assign('op',$op);		

if($op<>''){
	if ($op==1){//showing new message adding form
	
		$reqkeyword1  = $_REQUEST['txtsearch'] ;
		$t-> assign('reqkeyword1',$reqkeyword1);
		
		$reqfind = $_REQUEST['find'] ;
		$t-> assign('reqfind',$reqfind);
		
		$reqlastup = $_REQUEST['lastup'] ;
		$t-> assign('reqlastup',$reqlastup);

		if (($_GET['cat_id'] <> '0') && ($_GET['cat_id'] <> '')){	
			$cat_id = $_GET[ 'cat_id' ];
			$main_cat_id = FnGetValue("tbl_mn_lhf","main_cat_id","cat_id = $cat_id");
			$cat_name = FnGetValue("tbl_mn_lhf","name","cat_id = $cat_id");
			$cat_name2 = FnGetValue("tbl_mn_lhf","name","main_cat_id = '$main_cat_id' and lang = '$lang_sec'");  
		}else{
			$cat_id = 0;
			$main_cat_id = 0;
			$cat_name = "---Root Directory";
			$cat_name2 = "----Root Directory";  
		
		}
		$t->assign( 'cat_name', $cat_name );
		$t->assign( 'cat_name2', $cat_name2 );
		$t->assign( 'cat_id', $cat_id );
		
		
	}elseif ($op==2){ // showing Edit Data
	
		$reqkeyword1  = $_REQUEST['txtsearch'] ;
		$t-> assign('reqkeyword1',$reqkeyword1);
		
		$reqfind = $_REQUEST['find'] ;
		$t-> assign('reqfind',$reqfind);
		
		$reqlastup = $_REQUEST['lastup'] ;
		$t-> assign('reqlastup',$reqlastup);

		$cat_id    = $_GET[ 'cat_id' ];
		$heir_no   = FnGetValue("tbl_mn_lhf","hierno","cat_id = $cat_id");
		$lang_name = FnGetValue("tbl_mn_lhf","lang","cat_id = $cat_id");
		
		$name = FngetValue("tbl_mn_lhf","name","cat_id = $cat_id");
		$category_tag = FngetValue("tbl_mn_lhf","category_tag","cat_id = $cat_id");
		
		$t->assign( 'name', $name );
		$t->assign( 'category_tag', $category_tag );
		$t->assign( 'reqNewLang', $reqNewLang );
		$t->assign( 'cat_id', $cat_id );
		$t->assign( 'lang_name', $lang_name );
		//$t->display('nCategoryList.htm');
//		$t->display('category_edit.htm');
		//exit;
		
	}elseif($op=='3'){//deleting multilanguage message
	
		$reqkeyword1  = $_REQUEST['txtsearch'] ;
		$t-> assign('reqkeyword1',$reqkeyword1);
		
		$reqfind = $_REQUEST['find'] ;
		$t-> assign('reqfind',$reqfind);
		
		$reqlastup = $_REQUEST['lastup'] ;
		$t-> assign('reqlastup',$reqlastup);

		$main_cat_id = $_GET[ 'main_cat_id' ];
	

		$sql = "delete from tbl_mn_lhf where main_cat_id = $main_cat_id";
		$result = mysql_query($sql) or die(mysql_error());
		 
		header('Location: mn_left_header_footer_list.php');
		//$t->assign ( 'result',3);
		

		/* --------------inserting deleted message to temprory table
		 $sqlins = "Insert into tbl_mn_lhf values(NULL,'english','$newtxtmainkey','','$newdescr')";
		 $result = mysql_query($sqlins) or die(mysql_error().$sqlins);
		----------------end of inserting deleted message to temprory table  */
		
	}elseif ($op==4){ // showing To Add sub Category
		$reqkeyword1  = $_REQUEST['txtsearch'] ;
		$t-> assign('reqkeyword1',$reqkeyword1);
		
		$reqfind = $_REQUEST['find'] ;
		$t-> assign('reqfind',$reqfind);
		
		$reqlastup = $_REQUEST['lastup'] ;
		$t-> assign('reqlastup',$reqlastup);

		$cat_id = $_GET[ 'cat_id' ];
		$main_cat_id = FnGetValue("tbl_mn_lhf","main_cat_id","cat_id = $cat_id");
		
		$cat_name = FnGetValue("tbl_mn_lhf","name","cat_id = $cat_id");
		$cat_name2 = FnGetValue("tbl_mn_lhf","name","main_cat_id = '$main_cat_id' and lang = '$lang_sec'");  
		
		$t->assign( 'cat_name', $cat_name );
		$t->assign( 'cat_name2', $cat_name2 );
		$t->assign( 'cat_id', $cat_id );
		
		//$t->display('nCategoryList.htm');
//		$t->display('sub_category_add.htm');
		//exit;
	}
	
}//end of shwoing new message form and end of deleting multilanguage message



// --------------------------  Add category and subcategory
if ( isset( $_POST['txtcat_id']) ){//add new main/sub category

	// get and insert first language data
	$cat_id  = trim($_POST[ 'txtcat_id' ]);
	$lang = trim($_POST['txt_lang_pri_add']);
	$main_id = FnGetValue("tbl_mn_lhf","main_cat_id","cat_id = $cat_id");
	if($cat_id<>'0'){
		$hierno  = FnGetValue("tbl_mn_lhf","hierno","main_cat_id = $main_id and lang = '$lang'");
		$hierno  = $hierno + 1;
	}else{
		$hierno  = 0;
	}
	
	$reqMenu_Type = "lhf" ; //$_REQUEST['mntp'] ;
	$page_type = "page"; //trim($_POST[ 'txtpage_type' ]);
	$order_no = 1;
	$category_name = trim($_POST[ 'txtcategory_name' ]);
	$Category_Tag = ""; //trim($_POST[ 'txtCategory_Tag' ]);
	
	$sqlins = "Insert into tbl_mn_lhf values(NULL,'$category_name','$main_id','$hierno','$order_no','$lang','$reqMenu_Type','$page_type','0','0','0','$Category_Tag')";
	echo $sqlins;
	$result = mysql_query($sqlins) or die(mysql_error().$sqlins);
	
	$main_cat_id = mysql_insert_id();
	$sql_up = "Update tbl_mn_lhf SET main_cat_id = $main_cat_id where cat_id = $main_cat_id";
	$res_up = mysql_query($sql_up) or die(mysql_error().$sql_up);
	
	
	// get and insert second language data
	$lang = trim($_POST['txt_lang_sec_add']);
	$page_type = "page"; //trim($_POST[ 'txtpage_type2' ]);
	$order_no = 1;
	$category_name = $category_name;
	$Category_Tag = ""; //trim($_POST[ 'txtCategory_Tag2' ]);
	
	$sqlins = "Insert into tbl_mn_lhf values(NULL,'$category_name','$main_id','$hierno','$order_no','$lang','$reqMenu_Type','$page_type','0','0','$main_cat_id','$Category_Tag')";
	
	$result = mysql_query($sqlins) or die(mysql_error().$sqlins);
	
	$t->assign ( 'result', 1);
}//end of ading main and sub category



//----------------------------------------------------------------------------------------
//=================== For updating & Translating the message for both language =========================
//----------------------------------------------------------------------------------------
if (isset($_POST['txtsub'])){
   
   	$lang_pri = $_POST["txt_lang_pri"];
   	$lang_sec = $_POST["txt_lang_sec"];

	$reqkeyword1  = $_REQUEST['txtsearch'] ;
	$reqmain_category_id  = $_REQUEST['txtmain_category_id'] ;
	$reqfind     = $_REQUEST['find'] ;
	$reqlastup = $_REQUEST['lastup'] ;
	$reqMenu_Type = $_REQUEST['mntp'] ;

	$reqmessageSql = "select * from tbl_mn_lhf where lang='english' ";
	$tmpblnWhere=1;
	if($reqkeyword1<>''){
		if ($tmpblnWhere==0){ $reqmessageSql = $reqmessageSql . " where "; $tmpblnWhere=1; } else	$reqmessageSql = $reqmessageSql . " and ";
		$reqmessageSql = $reqmessageSql . " name like '%$reqkeyword1%' ";
		$t-> assign('reqkeyword1',$reqkeyword1);
	}
	
	if($reqmain_category_id<>''){
		if ($tmpblnWhere==0){ $reqmessageSql = $reqmessageSql . " where "; $tmpblnWhere=1; } else	$reqmessageSql = $reqmessageSql . " and ";
		$reqmessageSql = $reqmessageSql . " main_id = '$reqmain_category_id' ";
		$t-> assign('reqmain_category_id',$reqmain_category_id);
	}
	
	if($reqfind<>''){
		if ($tmpblnWhere==0){ $reqmessageSql = $reqmessageSql . " where "; $tmpblnWhere=1; } else	$reqmessageSql = $reqmessageSql . " and ";
		$reqmessageSql = $reqmessageSql . " name like '$reqfind%' ";
		$t-> assign('reqfind',$reqfind);
	}	
	
	if($reqMenu_Type<>''){
		if ($tmpblnWhere==0){ $reqmessageSql = $reqmessageSql . " where "; $tmpblnWhere=1; } else	$reqmessageSql = $reqmessageSql . " and ";
		$reqmessageSql = $reqmessageSql . " type= '$reqMenu_Type' ";
		$t-> assign('reqMenu_Type',$reqMenu_Type);
   }	


	$reqmessageSql = $reqmessageSql . " group by main_cat_id ";
	if($reqlastup<>''){
		$t-> assign('reqlastup',$reqlastup);
		$reqmessageSql = $reqmessageSql . " desc limit 0,100";
	}


	//echo $reqmessageSql;
	$messagetemp = $db->getall($reqmessageSql);
	foreach($messagetemp as $msgrow){
		$msgmain_key = $msgrow['cat_id'];
		
		$cat_name_pri  = $_POST['txtcat_name_pri'.$msgmain_key];
		$cat_tag_pri  = $_POST['txtcat_tag_pri'.$msgmain_key];
		$order_no_pri  = $_POST['txtorder_no_pri'.$msgmain_key];
		if ($order_no_pri=='') $order_no_pri = 0;
		
		$page_type_pri  = "page";//$_POST['txtpage_type_pri'.$msgmain_key];
		
		if ($page_type_pri =='link'){
			//updating english message
			$upmsg = "UPDATE tbl_mn_lhf SET
			name  = '$cat_name_pri',
			page_type  = '$page_type_pri',
			order_no = '$order_no_pri',
			category_tag = '$cat_tag_pri'
			where cat_id = '$msgmain_key' and lang = '$lang_pri'";
		}else{
			$upmsg = "UPDATE tbl_mn_lhf SET
			page_type  = '$page_type_pri',
			order_no = '$order_no_pri',
			name  = '$cat_name_pri'
			where cat_id = '$msgmain_key' and lang = '$lang_pri'";
		}
		//echo $upmsg;
		$upres = mysql_query($upmsg) or die(mysql_error().$upmsg);
		
		//updating greek message
			
		$cat_name_sec    =  $cat_name_pri; //$_POST['txtcat_name_sec'.$msgmain_key];
		$cat_tag_sec     =  $cat_tag_pri;  //$_POST['txtcat_tag_sec'.$msgmain_key];
		$order_no_sec    =  $_POST['txtorder_no_sec'.$msgmain_key];
		
		if ($order_no_sec=='') $order_no_sec = 0;
		$main_cat_id = FnGetValue("tbl_mn_lhf","main_cat_id","cat_id = $msgmain_key");
		$page_type_sec  = "page";//$_POST['txtpage_type_sec'.$msgmain_key];
		
				
		$chkRecord = FngetCount("tbl_mn_lhf","main_cat_id = $main_cat_id and lang = '$lang_sec'");
		if(($chkRecord<>'') || ($chkRecord<>'0')){
		
		if ($page_type_sec =='link'){
			$upgreemsg = "UPDATE tbl_mn_lhf SET
						name  = '$cat_name_sec',
						page_type  = '$page_type_sec',
						order_no = '$order_no_sec',
						category_tag = '$cat_tag_sec'
						where main_cat_id = '$main_cat_id' and lang = '$lang_sec'";
			}else{
			$upgreemsg = "UPDATE tbl_mn_lhf SET
						page_type  = '$page_type_sec',
						order_no = '$order_no_sec',
						name  = '$cat_name_sec'
						where main_cat_id = '$main_cat_id' and lang = '$lang_sec'";
			}
		}else{
			$heir_no = FnGetValue("tbl_mn_lhf","hierno","cat_id = $msgmain_key");
			$order_no = FnGetValue("tbl_mn_lhf","order_no","cat_id = $msgmain_key");
			$main_id = FnGetValue("tbl_mn_lhf","main_id","cat_id = $msgmain_key");
			$main_cat_id  = FnGetValue("tbl_mn_lhf","main_cat_id","cat_id = $msgmain_key");
			$t_cat_id = $main_cat_id;
			$upgreemsg  = "Insert into tbl_mn_lhf values(NULL,'$cat_name_sec','$main_id','$heir_no','$order_no','$lang_sec','$reqMenu_Type','$page_type_sec','$login_req','$page_code','$main_cat_id','$cat_tag_sec')";
			}
			
		//echo $upgreemsg;
		$upgreekres = mysql_query($upgreemsg) or die(mysql_error().$upgreemsg);
	}
	$t->assign ( 'result', 1);
}
//----------------------------------------------------------------------------------------
//================End for updating the message for both language =========================
//----------------------------------------------------------------------------------------




//----------------------------------------------------------------------------------------
//================showing lang key message ===============================================
//----------------------------------------------------------------------------------------


function heire_menu_cat($main_id,$selected,$lang,$menu_type){
	if($main_id == 0){
		$reqmaincat = "Select * from tbl_mn_lhf where hierno = 0 and type='$menu_type' and lang = '$lang' order by order_no";
	}else{
		$reqmaincat = "Select * from tbl_mn_lhf where main_id = $main_id and type='$menu_type' and lang = '$lang' order by order_no";
	}
	//echo $reqmaincat;
	$main_cat_temp = mysql_query( $reqmaincat ) or die(mysql_error().$reqmaincat);
	$space = '';
	while($row = mysql_fetch_array($main_cat_temp)){
		$space = ' ';
		$cat_id = $row['main_cat_id'];
		$req_present = fngetcount("tbl_mn_lhf","main_id = $cat_id and lang = '$lang'");
		$level = $row['hierno'];
		for($i=1;$i<=$level;$i++){
			$space = $space."---";
		}
		if($req_present > 0){
			//$req_option_string .= "<optgroup label='$space".$row['name']."'></optgroup>";
			if($selected == $cat_id) $sel = "selected='selected'"; else $sel = '';
			$req_option_string .= "<option value='".$cat_id."' ".$sel.">"."<strong>".$space.$row['name']."</strong>"."</option>";
			$req_option_string .=  heire_menu_cat($cat_id,$selected,$lang,$menu_type);
		}else{
			if($selected == $cat_id) $sel = "selected='selected'"; else $sel = '';
			$req_option_string .= "<option value='".$cat_id."' ".$sel.">$space".$row['name']."</option>";			
		}
	}			
	return $req_option_string;
}

/* function heire_menu_cat($main_id,$selected,$lang){
	if($main_id == 0){
		$reqmaincat = "Select * from tbl_mn_lhf where hierno = 0 and lang = '$lang'";
	}else{
		$reqmaincat = "Select * from tbl_mn_lhf where main_id = $main_id and lang = '$lang'";
	}
	//echo $reqmaincat;
	$main_cat_temp = mysql_query( $reqmaincat ) or die(mysql_error().$reqmaincat);
	$space = '';
	while($row = mysql_fetch_array($main_cat_temp)){
		$space = '';
		$cat_id = $row['main_cat_id'];
		$req_present = fngetcount("tbl_mn_lhf","main_id = $cat_id and lang = '$lang'");
		//echo $cat_id;
		$req_next_cat_id = fngetvalue("tbl_mn_lhf","main_cat_id","main_id = $cat_id");
		$req_next_present = fngetcount("tbl_mn_lhf","main_id = $req_next_cat_id");
		
		$level = $row['hierno'];
		for($i=1;$i<=$level;$i++){
			$space = $space."&nbsp;&nbsp;&nbsp;";
		}
		
		//$req_option_string .= "<optgroup label='$space".$row['name']."'></optgroup>";
		if($selected == $cat_id) $sel = "selected='selected'"; else $sel = '';
		$req_option_string .= "<option value='".$cat_id."' ".$sel.">"."<strong>".$space.$row['name']."</strong>"."</option>";
		if(($req_present > 0) && ($req_next_present > 0)){
			$req_option_string .=  heire_menu_cat($cat_id,$selected,$lang);
		}	
	}
			
	return $req_option_string;
}

*/

//echo $lang_pri;
$reqkeyword1  = $_REQUEST['txtsearch'] ;
$reqmain_category_id  = $_REQUEST['txtmain_category_id'] ;
$reqfind   = $_REQUEST['find'] ;
$reqlastup = $_REQUEST['lastup'] ;
$reqlastup = $_REQUEST['lastup'] ;

$reqMenu_Type = $_REQUEST['mntp'] ;
if ($reqMenu_Type=='' )
	$reqMenu_Type = "lhf";  // lhf = is for left header footer 

$req_tmp_laguage =  $_SESSION['opt_lang'];
$sel_menu_name = FnGetValue("tbl_mn_lhf","name","cat_id = '$reqmain_category_id' and lang = '$req_tmp_laguage'"); 
$t->assign('sel_menu_name',$sel_menu_name);

$req_retunr_str = heire_menu_cat(0,$reqmain_category_id,$req_tmp_laguage,$reqMenu_Type);
//$req_retunr_str = "<option value='0'>Root</option>" . $req_retunr_str;
$t->assign('select_option_str',$req_retunr_str);


$sql1 = "select * from tbl_mn_lhf";
$tmpblnWhere=0;
if($reqkeyword1<>''){
	if ($tmpblnWhere==0){ $sql1 = $sql1 . " where "; $tmpblnWhere=1; } else	$sql1 = $sql1 . " and ";
	$sql1 = $sql1 . " name like '%$reqkeyword1%' ";
	$t-> assign('reqkeyword1',$reqkeyword1);
}

if($reqmain_category_id<>''){
	if ($tmpblnWhere==0){ $sql1 = $sql1 . " where "; $tmpblnWhere=1; } else	$sql1 = $sql1 . " and ";
	$sql1 = $sql1 . " main_id = '$reqmain_category_id' ";
	$t-> assign('reqmain_category_id',$reqmain_category_id);
}else{
	$reqmain_category_id=0;
	if ($tmpblnWhere==0){ $sql1 = $sql1 . " where "; $tmpblnWhere=1; } else	$sql1 = $sql1 . " and ";
	$sql1 = $sql1 . " main_id = '$reqmain_category_id' ";
	$t-> assign('reqmain_category_id',$reqmain_category_id);

}

if($reqfind<>''){
	if ($tmpblnWhere==0){ $sql1 = $sql1 . " where "; $tmpblnWhere=1; } else	$sql1 = $sql1 . " and ";
	$sql1 = $sql1 . " name like '$reqfind%' ";
	$t-> assign('reqfind',$reqfind);
}	

if($reqMenu_Type<>''){
	if ($tmpblnWhere==0){ $sql1 = $sql1 . " where "; $tmpblnWhere=1; } else	$sql1 = $sql1 . " and ";
	$sql1 = $sql1 . " type= '$reqMenu_Type' ";
	$t-> assign('reqMenu_Type',$reqMenu_Type);
}	


$sql1 = $sql1 . " and lang='english' group by main_cat_id ";
//if($reqlastup<>''){
//	$t-> assign('reqlastup',$reqlastup);
//	$sql1 = $sql1 . " desc limit 0,50";
//}

//echo $sql1;
$main_key = array();
$main_key_list = $db->getAll( $sql1 );
foreach($main_key_list as $index=>$row){

	$mainkey = $row['cat_id'];
	$main_cat_id = $row['main_cat_id'];
	$row['cat_id_pri'] = $row['cat_id']; 
	
	$row['cat_name_pri'] = FnGetValue("tbl_mn_lhf","name","cat_id = '$mainkey' and lang = '$lang_pri'"); 
	$row['cat_tag_pri'] = FnGetValue("tbl_mn_lhf","category_tag","cat_id = '$mainkey' and lang = '$lang_pri'"); 
	$row['order_no_pri'] = FnGetValue("tbl_mn_lhf","order_no","cat_id = '$mainkey' and lang = '$lang_pri'"); 
	//echo $row['cat_tag_pri'];
	$row['page_type_pri'] = FnGetValue("tbl_mn_lhf","page_type","cat_id = '$mainkey' and lang = '$lang_pri'"); 
	$row['main_cat_name_pri'] = FnGetValue("tbl_mn_lhf","name","main_cat_id = ".$row["main_id"]." and lang = '$lang_pri'"); 
	
	$page_type = $row['page_type'];
	$row['cat_id_sec'] = FnGetValue("tbl_mn_lhf","cat_id","main_cat_id = '$main_cat_id' and lang = '$lang_sec'"); 
	$row['cat_name_sec'] = FnGetValue("tbl_mn_lhf","name","main_cat_id = '$main_cat_id' and lang = '$lang_sec'"); 
	$row['order_no_sec'] = FnGetValue("tbl_mn_lhf","order_no","main_cat_id = '$main_cat_id' and lang = '$lang_sec'"); 
	$row['page_type_sec'] = FnGetValue("tbl_mn_lhf","page_type","main_cat_id = '$main_cat_id' and lang = '$lang_sec'"); 
	$row['cat_tag_sec'] = FnGetValue("tbl_mn_lhf","category_tag","main_cat_id = '$main_cat_id' and lang = '$lang_sec'"); 
	
	$row['main_cat_name_sec'] = FnGetValue("tbl_mn_lhf","name","main_cat_id = ".$row["main_id"]." and lang = '$lang_sec'"); 
	$main_key[] = $row;
}

$t-> assign('lang_pri',$lang_pri);
$t-> assign('lang_sec',$lang_sec);
$t-> assign('reqlangfind',$lang_pri);
$t-> assign('main_key_list',$main_key);
$t-> assign('reqMenu_Type',$reqMenu_Type);

/*$lang_sec_list = array();
$lang_sec_list = array();
$lang_sec_list_tmp = $db->getAll( "SELECT * FROM tbl_lang where language <> '$lang_pri' order by `lang_id` desc" );
foreach( $lang_sec_list_tmp as $index => $lang_sec_row ) {
		$lang_sec_list['language'][] = $lang_sec_row['language'];
		$lang_sec_list['lang_id'][] = $lang_sec_row['lang_id'];
	}
$t->assign ( 'lang_sec_list', $lang_sec_list );*/

//----------------------------------------------------------------------------------------
//================End of showing lang key message ===============================================
//----------------------------------------------------------------------------------------

$t->display('mn_left_header_footer_list.htm');
?>