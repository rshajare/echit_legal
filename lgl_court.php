<?php

include_once ("init.php");
include_once( 'sur_adm_permission.php' );
include_once( 'controllers/lgl_court/classcourt.php' );

header('Content-Type: text/html; charset=utf-8');  
$court = new court();
$StatusMessage = "";
switch($_GET['action']){
	case '1' :		
					$court->Addcourt();							
					break;	
	case '2' :		
					$court->Editcourt();					
					break;	
					
	case '3' :		
					$court->store();					
					break;	
	
	case '4':	
	case 'destroy':	
					$court->destroy($_REQUEST['court_id']);	
					$_SESSION['AlertMessage'] = "Record Deleted Sucessfully";	
					header('Location: lgl_court.php');
					break;	
					
	case '5' :		
					$court->update();	
					break;	
	default:		
					$court->index();
					break;
}

//==========================#  End of Getting records in list page===================================================================
?>