<?
// +----------------------------------------------------------------------------------------------------------------------------+
// |  File Name : sur_privilage_access_rights_functions.php
// |  Version : 2.0 
// |  Create-date : 29, March, 2010
// |  Modify-date : 22, April, 2010
// |  Authors : Sachin / Ravi / Niteen 
// | 
// |  Desciption : old functions from php mysql projects with out smarty
// |  
// |				1  All date functions
// |					-dateFormat
// |					-SQLDateIn
// |					-SQLDateOut
// |					-SQLDateOut_Time
// |					
// |				2  Function realted to image/picture files
// |					-watermarkpng, jpg, gif
// |					-ImageThumbnailHeighWidth
// |					-file uplaod
// |					
// |				3  All fill combo functions
// |					-fnfillcombo
// |				
// |				
// |				4  All fngetvalue functions
// |					-fnGetValue()
// |					
// |				
// |				5  All other funstion
// |					-Fn_IP_Details
// |					-fnSendSMS
// |					-SetDefaultValue
// |				
// | 
// +------------------------------------------------------------------------------------------------------------------------------
//


//***********************************************************
// All date functions
//************************************************************

function dateFormat($input_date, $input_format, $output_format) {
   preg_match("/^([\w]*)/i", $input_date, $regs);
   $sep   = substr($input_date, strlen($regs[0]), 1);
   $label = explode($sep, $input_format);
   $value = explode($sep, $input_date);
   $array_date = array_combine($label, $value);
   if (in_array('Y', $label)) {
       $year = $array_date['Y'];
   } elseif (in_array('y', $label)) {
       $year = $year = $array_date['y'];
   } else {
       return false;
   }
  $output_date = date($output_format, mktime(0,0,0,$array_date['m'], $array_date['d'], $year));
  return $output_date;
}


//function is used to change date format (dd/mm/yy to yy-mm-dd)
function SQLDateIn($input_date){
	//$input_date input date(dd/mm/yy)
    if (strstr($input_date,'-')<>0) return $input_date;
	if (($input_date <> "") && ($input_date<>"NULL")){	
		 $input_format = 'd/m/y';
		 $output_format = 'Y-m-d';
		 preg_match("/^([\w]*)/i", $input_date, $regs);
		 $sep = substr($input_date, strlen($regs[0]), 1);
		 $label = explode($sep, $input_format);
		 $value = explode($sep, $input_date);
		 $array_date = array_combine($label, $value);
		 if (in_array('Y', $label)) {
		       $year = $array_date['Y'];
		   } elseif (in_array('y', $label)) {
		       $year = $year = $array_date['y'];
		   } else {
	       return false;
	      }
     $output_date = date($output_format, mktime(0,0,0,$array_date['m'], $array_date['d'], $year));
     return $output_date;
	}else{
		return "NULL";
	}
}
//function is used to change date format (yy-mm-dd to dd/mm/yy)
function SQLDateOut($tmpDate){
	if ($tmpDate<> "" ){
		if($tmpDate == '0000-00-00'){
			$tmpoutput = "";
			return $tmpoutput;
		}else{
			$tmptime   = strtotime($tmpDate);
			$tmpoutput = date("d/m/Y", $tmptime);
			//echo $tmpoutput."<br>";
			return $tmpoutput;
		}
	}else{
		return;
	}
}

function SQLDateOut1($tmpDate){
	if ($tmpDate<> "" ){
		$reqbirthdatearr = split("-",$tmpDate);
		$reqbirthdateday = $reqbirthdatearr[2];
		$reqbirthdatemon = $reqbirthdatearr[1];
		$reqbirthdateyer = $reqbirthdatearr[0];
		$tmpoutput = $reqbirthdateday."/".$reqbirthdatemon."/".$reqbirthdateyer;
		return $tmpoutput;
	}else{
		return;
	}
}

//Function is used to get month in number format
function FnCntMonth($tmpBetValue){
	if ($tmpBetValue == 1) 
		return "Jan";
	if ($tmpBetValue == 2) 
		return "Feb";
	if ($tmpBetValue == 3) 
		return "Mar";
	if ($tmpBetValue == 4) 
		return "Apr";
	if ($tmpBetValue == 5) 
		return "May";
	if ($tmpBetValue == 6) 
		return "Jun";
	if ($tmpBetValue == 7) 
		return "Jul";
	if ($tmpBetValue == 8) 
		return "Aug";
	if ($tmpBetValue == 9) 
		return "Sep";
	if ($tmpBetValue == 10) 
		return "Oct";
	if ($tmpBetValue == 11) 
		return "Nov";
	if ($tmpBetValue == 12) 
		return "Dec";
}

function FnCntLastDateOfChit($tmpStMon,$tmpStYear,$Dur,$Type){
      if($Type == "month"){
		$mon = $tmpStMon + $Dur ;
		$tyr = intval($mon / 12) ;
		$tmon = $mon % 12 ;
		$yr = $tmpStYear + $tyr ;
		$tmon = $tmon-1;
		$Fmon =  FnCntMonth($tmon);
		$str = $Fmon."-".$yr;
		return $str ;
	  }
}	  


function SQLDateOut_Time($tmpDate){
	if($tmpDate != ""){
		$date_time = split("-",$tmpDate);
		$tmpTime1	= split(" ",$date_time[2]);
		$tmpTime	= split(":",$tmpTime1[1]);
	  if ($_SESSION["DateFormat"]=="mm/dd/yyyy"){$whole_time = date("m/d/Y h:i A", mktime($tmpTime[0],$tmpTime[1],$tmpTime[2],$date_time[1],$tmpTime1[0],$date_time[0]));
	  }else if ($_SESSION["DateFormat"]=="dd/mm/yyyy"){ $whole_time = date("d/m/Y h:i A", mktime($tmpTime[0],$tmpTime[1],$tmpTime[2],$date_time[1],$tmpTime1[0],$date_time[0]));
	  }else { $whole_time = date("d/m/Y h:i A", mktime($tmpTime[0],$tmpTime[1],$tmpTime[2],$date_time[1],$tmpTime1[0],$date_time[0])); }  
		return $whole_time;
	}else{
		return false;
	}
}

function SQLTime_From_date($tmpDate){
	$tmpTime1	= split(" ",$tmpDate);
	$tmpTime	= $tmpTime1[1];
	return $tmpTime;
}


// AM PM Time Out Function
function fnTime_Out_Am_Pm($Time){   
    $split_Appt_From_time  	= split(":",$Time);
	$split_in_time_hr 	= $split_Appt_From_time[0];
	$split_in_time_min 	= $split_Appt_From_time[1];
	$am_pm= "AM";
	$am_pm_hours = $split_in_time_hr;
	if($split_in_time_hr >=12){
		if($split_in_time_hr!=12)	$am_pm_hours = $split_in_time_hr -12;	
		$am_pm= "PM";
	}
	$req_interval = $split_in_time_min;
	if(strlen($am_pm_hours)==1) $am_pm_hours = "0".$am_pm_hours;
	if(strlen($req_interval)==1) $req_interval = "0".$req_interval;
	return $am_pm_hours .":". $req_interval." ".$am_pm;
}

//Calculating Age================
/*function Ageyear($var1,$var2){
	$datearr1=split("/",$var1);
	$datearr2=split("/",$var2);
	//start date
	$base_day=$datearr1[0];
	$base_mon=$datearr1[1];
	$base_yr=$datearr1[2];
	//end date
	$current_day=$datearr2[1];
	$current_mon=$datearr2[0];
	$current_yr=$datearr2[2];
	// we need to know how many days $base_mon had
	//$base_mon_max = date ("t",mktime (0,0,0,$base_mon,$base_day,$base_yr));
    $base_mon_max = $base_mon."/".$base_day."/".$base_yr;
	// days left till the end of that month
    $base_day_diff 	= $base_mon_max - $base_day;
	// month left till end of that year substract one to handle overflow correctly
	$base_mon_diff 	= 12 - $base_mon - 1;
	// start on jan 1st of the next year
	$start_day		= 1;
	$start_mon		= 1;
	$start_yr		= $base_yr + 1;
	// difference to that 1st of jan
	$day_diff	= ($current_day - $start_day) + 1; 
	$mon_diff	= ($current_mon - $start_mon) + 1;
	$yr_diff	= ($current_yr - $start_yr);
	// and add the rest of $base_yr
	$day_diff	= $day_diff + $base_day_diff;
	$mon_diff	= $mon_diff + $base_mon_diff;
	// handle overflow of days
	if ($day_diff >= $base_mon_max){
		$day_diff = $day_diff - $base_mon_max;
		$mon_diff = $mon_diff + 1;
	}	
	// handle overflow of years
	if ($mon_diff >= 12){
		$mon_diff = $mon_diff - 12;
		$yr_diff = $yr_diff + 1;
	}
    $a_year = $yr_diff."y-".$mon_diff."m-". $day_diff."d"; // age in years-months-days
    $yr_diff=sprintf("%01.2f",($yr_diff*365.24));
	$mon_diff=sprintf("%01.2f",($mon_diff*30.4375));
	$day_diff=sprintf("%01.2f",($day_diff));	 
	 
	$days=$yr_diff+$mon_diff+$day_diff; 	  								//age in days;
	 if ( $days>sprintf("%01.0f",($days)) ) {
	  $days=sprintf("%01.0f",($days))+1;
	 }	  
	
	$months=sprintf("%01.2f",($days/30.4375));  						//age in months
	$weeks=(($days)/7);													//age in weeks
	$yr=$days / 365.24;  												//age in only year
    return $a_year."/".$months."/".$days."/".$weeks."/".$yr;
}*/
function Ageyear($var1,$var2){
 	if(($var1 != "")&&($var2 != "")){
		$datearr1=split("-",$var1);
		$datearr2=split("-",$var2);
		//start date
		$base_day=$datearr1[2];
		$base_mon=$datearr1[1];
		$base_yr=$datearr1[0];
		//end date
		$current_day=$datearr2[2];
		$current_mon=$datearr2[1];
		$current_yr =$datearr2[0];
		 // we need to know how many days $base_mon had
		$base_mon_max= date ("t",mktime (0,0,0,$base_mon,$base_day,$base_yr));
		// days left till the end of that month
		$base_day_diff 	= $base_mon_max - $base_day;
		// month left till end of that year substract one to handle overflow correctly
		$base_mon_diff 	= 12 - $base_mon - 1;
		// start on jan 1st of the next year
		$start_day		= 1;
		$start_mon		= 1;
		$start_yr		= $base_yr + 1;
		// difference to that 1st of jan
		$day_diff	= ($current_day - $start_day) + 1; 
		$mon_diff	= ($current_mon - $start_mon) + 1;
		$yr_diff	= ($current_yr - $start_yr);
		// and add the rest of $base_yr
		$day_diff	= $day_diff + $base_day_diff;
		$mon_diff	= $mon_diff + $base_mon_diff;
		// handle overflow of days
		if ($day_diff >= $base_mon_max)
		{
			$day_diff = $day_diff - $base_mon_max;
			$mon_diff = $mon_diff + 1;
		}	
		// handle overflow of years
		if ($mon_diff >= 12)
		{
			$mon_diff = $mon_diff - 12;
			$yr_diff = $yr_diff + 1;
		}
	
		  $a_year = $yr_diff."-".$mon_diff."-". $day_diff.""; // age in years-months-days		  
		  $age_complete=$yr_diff."-".$mon_diff."-".$day_diff;
		 $yr_diff=sprintf("%01.2f",($yr_diff*365.24));
		 $mon_diff=sprintf("%01.2f",($mon_diff*30.4375));
		 $day_diff=sprintf("%01.2f",($day_diff));	 
		 
		 $days=$yr_diff+$mon_diff+$day_diff; 	  								//age in days;
		  if ( $days>sprintf("%01.0f",($days)) ) {
		   $days=sprintf("%01.0f",($days))+1;
		  }	  
		
			$months=sprintf("%01.2f",($days/30.4375));  						//age in months
			$weeks=(($days)/7);													//age in weeks
			$yr=$days / 365.24;  												//age in only year
			
			
		   return $a_year."/".$months."/".$days."/".$weeks."/".$yr."/".$age_complete;
	}else{
		return 0;
	}
}
function age_year($start_date,$end_date){
	
     $s1=Ageyear($start_date,$end_date);        //calling Ageyear()
	$sarr=split("/",$s1);
	$yr=sprintf("%01.2f",$sarr[4]);
    return $yr;
}
//******************************************************************************8
//////////// end of all date functions
//******************************************************************************8





//************************************************************************
//Function realted to image/picture files
//************************************************************************ 

function string_width($string, $size) {
	$single_width = $size + 4;
	return $single_width * strlen($string);
}

function watermarkImage ($SourceFile, $WaterMarkText, $DestinationFile) {
   //function used to mark text on JPG/JPEG type image
   //$SourceFile user file name
   //$WaterMarkText Text msg which is going to print on image
   //$DestinationFile destination file path
   list($width, $height) = getimagesize($SourceFile);
   $image_p = imagecreatetruecolor($width, $height);
   $image = imagecreatefromjpeg($SourceFile);
   imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width, $height);
   $black = imagecolorallocate($image_p, 255, 0, 0);
   $font = 'arial.ttf';
   $font_size = 10;
   $center = ($width / 2) - (string_width($WaterMarkText, 5) / 2);
   $bot_hight = $height - 40;
   imagestring($image_p, 1, $center, $bot_hight, $WaterMarkText, $black);
   if ($DestinationFile<>'') {
      imagejpeg ($image_p, $DestinationFile, 100);
   }else{
      header('Content-Type: image/jpeg');
      imagejpeg($image_p, null, 100);
   };
   imagedestroy($image);
   imagedestroy($image_p);
}

function watermarkImageGIF($SourceFile, $WaterMarkText, $DestinationFile) {
   //function used to mark text on GIF type image
   //$SourceFile user file name
   //$WaterMarkText Text msg which is going to print on image
   //$DestinationFile destination file path
   list($width, $height) = getimagesize($SourceFile);
   $image_p = imagecreatetruecolor($width, $height);
   $image = imagecreatefromgif ($SourceFile);
   imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width, $height);
   $black = imagecolorallocate($image_p, 255, 0, 0);
   $font = 'arial.ttf';
   $font_size = 10;
   $center = ($width / 2) - (string_width($WaterMarkText, 5) / 2);
   $bot_hight = $height - 40;
   imagestring($image_p, 1, $center, $bot_hight, $WaterMarkText, $black);
   if ($DestinationFile<>'') {
	  imagegif ($image_p, $DestinationFile, 100);
   } else {
      header ("Content-type: image/gif");
	 imagegif ($image_p, $DestinationFile, 100);
   };
   imagedestroy($image);
   imagedestroy($image_p);
}

function watermarkImagePNG($SourceFile, $WaterMarkText, $DestinationFile) {
   //function used to mark text on PNG type image
   //$SourceFile user file name
   //$WaterMarkText Text msg which is going to print on image
   //$DestinationFile destination file path
   list($width, $height) = getimagesize($SourceFile);
   $image_p = imagecreatetruecolor($width, $height);
   $image = imagecreatefrompng ($SourceFile);
   imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width, $height);
   $black = imagecolorallocate($image_p, 255, 0, 0);
   $font = 'arial.ttf';
   $font_size = 10;
   $center = ($width / 2) - (string_width($WaterMarkText, 5) / 2);
   $bot_hight = $height - 40;
   imagestring($image_p, 1, $center, $bot_hight, $WaterMarkText, $black);
   if ($DestinationFile<>'') {
	  imagepng ($image_p, $DestinationFile, 100);
   } else {
      header("Content-type: image/png");
 	  imagepng ($image_p, $DestinationFile, 100);
   };
   imagedestroy($image);
   imagedestroy($image_p);
}

// File uplaod functions
function file_upload($file_variable_name,$uploadDir){
	//$file_variable_name file name
	//$uploadDir destination dir name
	$not_present = 0;
	$cnt = 1;
	$uploadFile1 = $_FILES[$file_variable_name]['name'];
	if(file_exists($uploadDir . '/' . $_FILES[$file_variable_name]['name'])){
		if (($_FILES[$file_variable_name]["name"]) <> ""){
			while($not_present != 1){
				if(!file_exists($uploadDir.'/'.$cnt.$_FILES[$file_variable_name]['name'])){
					$uploadFile1 = $cnt.$_FILES[$file_variable_name]['name'];
					$not_present = 1;
				}else{ $cnt += 1; }
			}
			$uploadFile = $uploadDir."/".$uploadFile1;
			if (move_uploaded_file($_FILES[$file_variable_name]["tmp_name"], $uploadFile)) {
				chmod($uploadFile,0644);
				return $uploadFile1;
			}
		}
	}else{//file not exist alleready
		$uploadFile = $uploadDir."/".$_FILES[$file_variable_name]['name'];
		if (move_uploaded_file($_FILES[$file_variable_name]["tmp_name"], $uploadFile)){
			chmod($uploadFile,0644);
		}	
		return $uploadFile1;
	}
}

function file_upload1($file_variable_name,$uploadDir){
	$day = date('d-m-y');
	$not_present = 0;
	$cnt = 1;
	$uploadFile1 = $_FILES[$file_variable_name]['name'];
	if(file_exists($uploadDir . '/' . $_FILES[$file_variable_name]['name'])){
		if (($_FILES[$file_variable_name]["name"]) <> ""){
			while($not_present != 1){
				if(!file_exists($uploadDir.'/'.$cnt.$_FILES[$file_variable_name]['name'])){
					$uploadFile1 = $cnt.$_FILES[$file_variable_name]['name'];
					$not_present = 1;
				}else{ $cnt += 1; }
			}
			$uploadFile = $uploadDir."/".$uploadFile1;
			if (move_uploaded_file($_FILES[$file_variable_name]["tmp_name"], $uploadFile)) {
				chmod($uploadFile,0644);
				return $uploadFile1;
			}
		}
	}else{//file not exist alleready
		$uploadFile2 = $_SESSION["personnel_id"]."_".$day."_".$_FILES[$file_variable_name]['name'];
			$uploadFile = $uploadDir."/".$uploadFile2;
			if (move_uploaded_file($_FILES[$file_variable_name]["tmp_name"], $uploadFile)) {
				chmod($uploadFile,0644);
			}
		return $uploadFile2;
	}
}


//=====imgae handling====================
function ImageThumbnailHeighWidth($image_name,$Canwidth,$Canhieght){
	//$image_name image name
	//$Canwidth   image width as per the user request
	//$Canhieght  image height as per the user request
	$size = getimagesize($image_name);//get actual image size
	$imageht=$size[1];
	$imagewd=$size[0];
	$sizethumb=array();
	if ($imageht > $imagewd){
		if ($imageht>$Canhieght){
			$sizethumb[1]=$Canhieght;
			$sizethumb[0]=($Canhieght/$imageht)*$imagewd;
		}else{
			$sizethumb[0]=$imagewd;
			$sizethumb[1]=$imageht;
		}
	}elseif ($imagewd>$imageht){
		if ($imagewd > $Canwidth){
			$sizethumb[0]=$Canwidth;
			$sizethumb[1]=($Canwidth/$imagewd)*$imageht;
		}else{
			$sizethumb[0]=$imagewd;
			$sizethumb[1]=$imageht;
		}
	}else{
			$sizethumb[0]=$Canwidth;
			$sizethumb[1]=$Canhieght;
	}
	return $sizethumb;
}

//*****************************************
//  end of image/picture file functions
//*****************************************




//******************************************  
// All fill combo functions
//******************************************  
function FnFillComboSQL($tmpsql,$tmpSelected){
    $tmpresult = mysql_query($tmpsql) or die("Couldn't execute query1.");
    // format results by row
	while ($tmprow = mysql_fetch_array($tmpresult)) {
		$reqNameField = $tmprow[0];
		if ($tmpSelected == $reqNameField)
			echo "<option value='$reqNameField' selected>$reqNameField</option>";	
		else
			echo "<option value='$reqNameField'>$reqNameField</option>";	
	    $tmpFlag=1;
    }
}

function FnFillCombo($tmpTable,$tmpNameField,$tmpValueField,$tmpWhere,$tmpSelected){
	$tmpsql = "select $tmpNameField,$tmpValueField from $tmpTable";
	if ($tmpWhere <> "") 
	$tmpsql = $tmpsql . " where " . $tmpWhere;	
	$tmpsql = $tmpsql . " order by " . $tmpValueField ;	
	$tmpresult = mysql_query($tmpsql) or die("FnfillCombo<br>".$tmpsql."<br>".mysql_error());
	while ($tmprow = mysql_fetch_array($tmpresult)) {
		$reqNameField = $tmprow[$tmpNameField];
		$reqValueField = $tmprow[$tmpValueField];
		if ($tmpSelected === $reqNameField)		echo "<option value='$reqNameField' selected>$reqValueField</option>";	
		else									echo "<option value='$reqNameField'>$reqValueField</option>";	
		$tmpFlag=1;
	}
}

function FnFillComboTwo($tmpTable,$tmpNameField,$tmpValueField,$tmpValueField1,$tmpWhere,$tmpSelected){
	$tmpsql = "select $tmpNameField,$tmpValueField,$tmpValueField1 from $tmpTable ";
	if ($tmpWhere <> "")   $tmpsql = $tmpsql . " where " . $tmpWhere;	
	$tmpsql = $tmpsql . " order by " . $tmpValueField ;	
	$tmpresult = mysql_query($tmpsql) or die("Couldn't execute query.".$tmpsql);
	while ($tmprow = mysql_fetch_array($tmpresult)) {
		$reqNameField = $tmprow[$tmpNameField];
		$reqValueField = $tmprow[$tmpValueField];
		$reqValueField1 = $tmprow[$tmpValueField1];
		if ($tmpSelected == $reqNameField)	echo "<option value='$reqNameField' selected>$reqValueField $reqValueField1</option>";	
		else								echo "<option value='$reqNameField'>$reqValueField  $reqValueField1 </option>";	
		$tmpFlag=1;
    }
}
function FnFillComboDist($tmpTable,$tmpNameField,$tmpValueField,$tmpWhere,$tmpSelected){
	$tmpsql = "select Distinct $tmpNameField,$tmpValueField from $tmpTable ";
	if ($tmpWhere <> "")    $tmpsql = $tmpsql . " where " . $tmpWhere;	
	$tmpsql = $tmpsql . " order by " . $tmpValueField ;	
	$tmpresult = mysql_query($tmpsql) or die("Couldn't execute query.".$tmpsql);
	while ($tmprow = mysql_fetch_array($tmpresult)){
		$reqNameField = $tmprow["$tmpNameField"];
		$reqValueField = $tmprow["$tmpValueField"];
		if ($tmpSelected == $reqNameField)			echo "<option value='$reqNameField' selected>$reqValueField</option>";	
		else										echo "<option value='$reqNameField'>$reqValueField</option>";	
		$tmpFlag=1;
	}
}

function FnFillCombo1($tmpTable,$tmpNameField,$tmpValueField,$tmpValueField1,$tmpValueField2,$tmpWhere,$tmpSelected){
	$tmpsql = "select $tmpNameField,$tmpValueField,$tmpValueField1,$tmpValueField2 from $tmpTable ";
	if ($tmpWhere <> "")   $tmpsql = $tmpsql . " where " . $tmpWhere;	
	$tmpsql = $tmpsql . " order by " . $tmpValueField;	
	$tmpresult = mysql_query($tmpsql) or die("Couldn't execute query FnFillCombo1.");
	while ($tmprow = mysql_fetch_array($tmpresult)){
		$reqNameField = $tmprow[$tmpNameField];
		$reqValueField = $tmprow[$tmpValueField];
		$reqValueField1 = $tmprow[$tmpValueField1];
		$reqValueField2 = $tmprow[$tmpValueField2];
		if ($tmpSelected == $reqNameField)	echo "<option value='$reqNameField' selected>$reqValueField $reqValueField1  $reqValueField2</option>";	
		else								echo "<option value='$reqNameField'>$reqValueField $reqValueField1  $reqValueField2</option>";	
	    $tmpFlag=1;
    }
}
function FnFillCombo2($tmpTable,$tmpNameField,$tmpValueField1,$tmpValueField2,$tmpWhere,$tmpSelected){
	$tmpsql = "select $tmpNameField,$tmpValueField1,$tmpValueField2 from $tmpTable ";
	if ($tmpWhere <> "") $tmpsql = $tmpsql . " where " . $tmpWhere;	
	$tmpsql = $tmpsql . " order by " . $tmpValueField1;	
	$tmpresult = mysql_query($tmpsql) or die("Couldn't execute query FnFillCombo1.");
	while ($tmprow = mysql_fetch_array($tmpresult)){
		$reqNameField = $tmprow[$tmpNameField];
		$reqValueField1 = $tmprow[$tmpValueField1];
		$reqValueField2 = $tmprow[$tmpValueField2];
		if ($tmpSelected == $reqNameField)	echo "<option value='$reqNameField' selected>$reqValueField2  $reqValueField1</option>";	
		else								echo "<option value='$reqNameField'>$reqValueField2  $reqValueField1</option>";	
	    $tmpFlag=1;
    }
}
function FnFillComboInv($tmpTable,$tmpNameField,$tmpValueField,$tmpValueField1,$tmpValueField2,$tmpWhere,$tmpSelected){
	$tmpsql = "select $tmpNameField,$tmpValueField,$tmpValueField1,$tmpValueField2 from $tmpTable ";
	if ($tmpWhere <> "") $tmpsql = $tmpsql . " where " . $tmpWhere;	
	$tmpsql = $tmpsql . " order by " . $tmpValueField;	
	$tmpresult = mysql_query($tmpsql) or die("Couldn't execute query FnFillComboInv.");
	while ($tmprow = mysql_fetch_array($tmpresult)){
		$reqNameField = $tmprow[$tmpNameField];
		$reqValueField = $tmprow[$tmpValueField];
		$reqValueField1 = $tmprow[$tmpValueField1];
		$reqValueField2 = $tmprow[$tmpValueField2];
		$reqValueField = "Invoice No: ". $reqNameField . "  ";
		$reqValueField1 = "On Dated: ". SQLDateOut($reqValueField1);
		$reqValueField2 = " With Amt : $" . $tmprow[$tmpValueField2];
		if ($tmpSelected == $reqNameField)	echo "<option value='$reqNameField' selected>$reqValueField ($reqValueField1  $reqValueField2)</option>";	
		else								echo "<option value='$reqNameField'>$reqValueField ($reqValueField1  $reqValueField2)</option>";	
	    $tmpFlag=1;
    }
}
function FnFillComboExp($tmpTable,$tmpNameField,$tmpValueField,$tmpValueField1,$tmpValueField2,$tmpWhere,$tmpSelected){
	$tmpsql = "select $tmpNameField,$tmpValueField,$tmpValueField1,$tmpValueField2 from $tmpTable ";
	if ($tmpWhere <> "") $tmpsql = $tmpsql . " where " . $tmpWhere;	
	$tmpsql = $tmpsql . " order by " . $tmpValueField;	
	$tmpresult = mysql_query($tmpsql) or die("Couldn't execute query FnFillComboExp.");
	while ($tmprow = mysql_fetch_array($tmpresult)){
		$reqNameField = $tmprow[$tmpNameField];
		$reqValueField = $tmprow[$tmpValueField];
		$reqValueField1 = $tmprow[$tmpValueField1];
		$reqValueField2 = $tmprow[$tmpValueField2];
		$reqValueField = "Expense No: ". $reqValueField . "  ";
		$reqValueField1 = "On Dated: ". SQLDateOut($reqValueField1);
		$reqValueField2 = " With Amt : $" . $tmprow[$tmpValueField2];
		if ($tmpSelected == $reqNameField)	echo "<option value='$reqNameField' selected>$reqValueField ($reqValueField1  $reqValueField2)</option>";	
		else								echo "<option value='$reqNameField'>$reqValueField ($reqValueField1  $reqValueField2)</option>";	
	    $tmpFlag=1;
    }
}
function FnFillComboActivityType($tmpTable,$tmpNameField,$tmpValueField,$tmpValueField1,$tmpWhere,$tmpSelected){
	$tmpsql = "select $tmpNameField,$tmpValueField,$tmpValueField1 from $tmpTable ";
	if ($tmpWhere <> "") $tmpsql = $tmpsql . " where " . $tmpWhere;	
	$tmpsql = $tmpsql . " order by " . $tmpValueField;	
	$tmpresult = mysql_query($tmpsql) or die("Couldn't execute query FnFillComboActivityType.");
	while ($tmprow = mysql_fetch_array($tmpresult)){
		$reqNameField = $tmprow[$tmpNameField];
		$reqValueField = $tmprow[$tmpValueField];
		$reqValueField1 = $tmprow[$tmpValueField1];
		if ($tmpSelected == $reqNameField)		echo "<option value='$reqNameField' selected>$reqValueField ($reqValueField1)</option>";	
		else									echo "<option value='$reqNameField'>$reqValueField ($reqValueField1)</option>";	
	    $tmpFlag=1;
    }
}



//****************************************************8
// all getvalue functions
//****************************************************8

function FnGetValue($tmpTable,$tmpField,$tmpWhere){
    //$tmpTable table name
	//$tmpField field name
	//$tmpWhere where contion
	if ($tmpTable<> "")  $frompart="from $tmpTable";	else    $frompart="";

	if ($tmpWhere<> "")   $wherepart = "where $tmpWhere ";   else   $wherepart = "";

	$tmpsql = "select $tmpField $frompart $wherepart";
	   
   	$tmpValue = 0;
   	$tmpresult = mysql_query($tmpsql)	or die(mysql_error()."<br>".$tmpsql) ;
	while ($tmprow = mysql_fetch_array($tmpresult)){
		$tmpValue = $tmprow[0];
    }
    return $tmpValue;
}

function FnGetDateValue($tmpField){
   $tmpsql = "select $tmpField ";
   $tmpValue = 0;
   $tmpresult = mysql_query($tmpsql)	or die("Couldn't execute query111111.".$tmpsql) ;
	while ($tmprow = mysql_fetch_array($tmpresult)){
		$tmpValue = $tmprow[$tmpField];
    }
    return $tmpValue;
}
function FnGetValue5($tmpTable,$tmpField,$tmpWhere){
    global $db;
    $value = $db->getOne( "select $tmpField from $tmpTable where $tmpWhere");
    return isset( $value ) ? $value : 0;
}

function FnGetValue2($tmpField){
    //$tmpTable table name
	//$tmpField field name
	//$tmpWhere where contion
   $tmpsql = "select $tmpField ";
   $tmpValue = 0;
   $tmpresult = mysql_query($tmpsql) or die(mysql_error()."Couldn't execute query111111.".$tmpsql) ;
	while ($tmprow = mysql_fetch_array($tmpresult)){
		$tmpValue = $tmprow[$tmpField];
    }
    return $tmpValue;
}

function FnGetValueSQL($tmpsql,$tmpField){
   //$tmpsql sql query
   //$tmpField field name
   $tmpValue = 0;
   $tmpresult = mysql_query($tmpsql)	or die("Couldn't execute query2.");
	while ($tmprow = mysql_fetch_array($tmpresult)) {
		$tmpValue = $tmprow[$tmpField];
    }
    return $tmpValue;
}

function FnGetCount($tmpTable,$tmpWhere){
	if ($tmpWhere<> "") $tmpsql = "select count(*) as num from $tmpTable where $tmpWhere ";
	else $tmpsql = "select count(*) as num from $tmpTable ";
	$tmpresult = mysql_query($tmpsql) or die("FnGetCount <br>".mysql_error()."<br>" . $tmpsql);
	$num_result_arr = mysql_fetch_assoc($tmpresult);
	$totalreturned = $num_result_arr["num"];
	return $totalreturned;
}



function FnGetCount1($tmpTable,$tmpField,$tmpWhere){
    if ($tmpWhere<> "")   
	   $tmpsql = "select $tmpField from $tmpTable where $tmpWhere ";
    else
	   $tmpsql = "select $tmpField from $tmpTable ";
   $tmpresult = mysql_query($tmpsql) or die("Couldn't execute count query " . $tmpsql );
   return mysql_num_rows($tmpresult); 
}


function FnGetCountSql($tmpsql){
   //$tmpsl sql query
   $tmpresult = mysql_query($tmpsql) or die("Couldn't execute count query " . $tmpsql );
   return mysql_num_rows($tmpresult); 
}

function FnGetValueMultiple($tmpTable,$tmpFields,$tmpWhere,$ReturnType='ArrayOfValues'){
	if ($tmpWhere<> "") $tmpsql = "select $tmpFields from $tmpTable where $tmpWhere ";
	else			   	$tmpsql = "select $tmpFields from $tmpTable ";
	$fields=split(',',$tmpFields);
	 $FieldCount=count($fields);
	$tmpValue = array();
	$tmpRetValue='';
	$tmpresult = mysql_query($tmpsql) or die( "FnGetValue <br>".mysql_error()."<br>" . $tmpsql );
	while ($tmprow = mysql_fetch_array($tmpresult)){ for($i=0;$i<$FieldCount;$i++){ $tmpValue[$i] = $tmprow[$fields[$i]];}}
	switch($ReturnType){
		case 1:
			
		case 'ArrayOfValues': return $tmpValue;  break;
		case 2:
		case 'ConcatValues':
			for($i=0;$i<$FieldCount;$i++){  $tmpRetValue.= (($tmpValue[$i]<>"")?$tmpValue[$i]." ":"");}
			return $tmpRetValue;	 
			break; 
	}    
}
//----------------------------for agreegate function---------------
function FnGetValueMultipleNew($tmpTable,$tmpFields,$tmpWhere,$ReturnType='ArrayOfValues'){
	if ($tmpWhere<> "") $tmpsql = "select $tmpFields from $tmpTable where $tmpWhere ";
	else			   	$tmpsql = "select $tmpFields from $tmpTable ";
	$fields=split(',',$tmpFields);
	 $FieldCount=count($fields);
	$tmpValue = array();
	$tmpRetValue='';
	$tmpresult = mysql_query($tmpsql) or die( "FnGetValue <br>".mysql_error()."<br>" . $tmpsql );
	while ($tmprow = mysql_fetch_array($tmpresult)){ for($i=0;$i<$FieldCount;$i++){ $tmpValue[$i] = $tmprow[$i];}}
	switch($ReturnType){
		case 1:
			
		case 'ArrayOfValues': return $tmpValue;  break;
		case 2:
		case 'ConcatValues':
			for($i=0;$i<$FieldCount;$i++){  $tmpRetValue.= (($tmpValue[$i]<>"")?$tmpValue[$i]." ":"");}
			return $tmpRetValue;	 
			break; 
	}    
}
//--------------------------------------------------------------------

function FnGetValue1($tmpTable,$tmpField1,$tmpField2,$tmpWhere){
    if ($tmpWhere <> "")   $tmpsql = "select $tmpField1,$tmpField2 from $tmpTable where $tmpWhere ";
    else				   $tmpsql = "select $tmpField1 from $tmpTable";
	$tmpValue1 = 0;
	$tmpValue2 = 0;
	$tmpresult1 = mysql_query($tmpsql)	or die( "Couldn't execute query FnGetValue.".mysql_error() . $tmpsql );
	while ($tmprow = mysql_fetch_array($tmpresult1)) {
		$tmpValue1 = $tmprow[$tmpField1];
		$tmpValue2 = $tmprow[$tmpField2];
    }
    return $tmpValue1;
	return $tmpValue2;
}
function FnGetValueThree($tmpTable,$tmpField1,$tmpField2,$tmpField3,$tmpWhere){
    if ($tmpWhere <> "")   $tmpsql = "select $tmpField1,$tmpField2,$tmpField3 from $tmpTable where $tmpWhere ";
    else				   $tmpsql = "select ".(($tmpField1 <> " ")? $tmpField1.",":"")."$tmpField2,$tmpField3 from $tmpTable";	
	$tmpValue1 = 0;
	$tmpValue2 = 0;
	$tmpresult1 = mysql_query($tmpsql)	or die($tmpField1.$tmpsql."<br>".mysql_error());
	while ($tmprow = mysql_fetch_array($tmpresult1)){
		$tmpValue1 = $tmprow[$tmpField1];
		$tmpValue2 = $tmprow[$tmpField2];
		$tmpValue3 = $tmprow[$tmpField3];
    }
    return (($tmpValue1<>"")?$tmpValue1." ":"").(($tmpValue2<>"")?$tmpValue2:"").(($tmpValue3<>"")?" ".$tmpValue3:"");
}
function FnGetValueTwo($tmpTable,$tmpField1,$tmpField2,$tmpWhere){
    if ($tmpWhere <> "")   $tmpsql = "select $tmpField1,$tmpField2 from $tmpTable where $tmpWhere ";
    else				   $tmpsql = "select $tmpField1,$tmpField2 from $tmpTable";	
	$tmpValue1 = 0;
	$tmpValue2 = 0;
	$tmpresult1 = mysql_query($tmpsql)	or die($tmpField1.$tmpsql."<br>".mysql_error());
	while ($tmprow = mysql_fetch_array($tmpresult1)){
		$tmpValue1 = $tmprow[$tmpField1];
		$tmpValue2 = $tmprow[$tmpField2];
		$tmpValue3 = $tmprow[$tmpField3];
    }
    return (($tmpValue1<>"")?$tmpValue1." ":"").(($tmpValue2<>"")?$tmpValue2:"");
}

//****************************************************8
// end of getvalue functions
//****************************************************8




//****************************************************8
// all other functions
//****************************************************8
function Fn_IP_Details($IP_Address){
	// Get the surfer's ip address
	$addr = $IP_Address;
	$ip = sprintf("%u", ip2long($addr));
	// Open the csv file for reading
	$handle = fopen("IpToCountry.csv", "r");
	// Load array with start ips
	$row = 1;
	while (($buffer = fgets($handle, 4096)) !== FALSE) {
	  $array[$row] = substr($buffer, 1, strpos($buffer, ",") - 1);
	  $row++;
	}
	// Locate the row with our ip using bisection
	$row_lower = '0';
	$row_upper = $row;
	while (($row_upper - $row_lower) > 1) {
	  $row_midpt = (int) (($row_upper + $row_lower) / 2);
	  if ($ip >= $array[$row_midpt]) {
		$row_lower = $row_midpt;
	  } else {
		$row_upper = $row_midpt;
	  }
	}
    // Read the row with our ip
	rewind($handle);
	$row = 1;
	while ($row <= $row_lower) {
	  $buffer = fgets($handle, 4096);
	  $row++;
	}
	$buffer = str_replace("\"", "", $buffer);
	$ipdata = explode(",", $buffer);
	fclose($handle);
	return $ipdata[2];
}



function FnShow_Lang($tmpField)
{
   $reqLang = $_SESSION['opt_lang'];
   $tmpValue = FnGetValue("tbl_lang_msg","descr","lang='$reqLang' and mainkey ='$tmpField'");
   if (($tmpValue == "") ||  ($tmpValue == '0')) {
	  $tmpValue = FnGetValue("tbl_lang_msg","descr","lang='english' and mainkey ='$tmpField'");
   }
   if (($tmpValue == "") || ($tmpValue == '0')) {
	   $tmpValue = $tmpField;
   }
    return $tmpValue;
}

function FnShowEmail_Lang($tmpField)
{
   $reqLang = $_SESSION['opt_lang'];
   $tmpValue = 0;
   $tmpValue = FnGetValue("tbl_lang_msg_email","descr","lang='$reqLang' and mainkey ='$tmpField'");
   if ($tmpValue == ""){
	  $tmpValue = FnGetValue("tbl_lang_msg_email","descr","lang='english' and mainkey ='$tmpField'");
   }else{
      $tmpValue = FnGetValue("tbl_lang_msg_email","descr","lang='$reqLang' and mainkey ='$tmpField'");
   }
    return $tmpValue;
}

function FnShowEmail_Lang1($tmpField,$defprelang){
   $reqPre_Lang = $defprelang;
   if($reqPre_Lang == '')
   	$reqPre_Lang = 'english';
   $tmpValue = 0;
   $tmpValue = FnGetValue("tbl_lang_msg_email","descr","lang='$reqPre_Lang' and mainkey ='$tmpField'");
   if ($tmpValue == '0'){
	  $tmpValue = FnGetValue("tbl_lang_msg_email","descr","lang='english' and mainkey ='$tmpField'");
   }else{
      $tmpValue = FnGetValue("tbl_lang_msg_email","descr","lang='$reqPre_Lang' and mainkey ='$tmpField'");
   }
    return $tmpValue;
}

function ins_comma($n){
	$slt_n  = explode('.',$n);
	$cntarr = count($slt_n); 
	if($cntarr > 1){
		$n = $slt_n[0]; 
		$n1 = $slt_n[1]; 
	}	
	$sign = 0;
	if($n < 0){
		$n = substr($n,1);
		$sign = 1;		
	}
	$l = strlen($n);
	$m=$l;
		$str1 = "";
		$str2 = "";
		while ($l > 0){
			$k = $n % 10;
			$j = $n / 10;
			$str2 = $str2.''.$k;
			$n=$j;		
			$l--;
		}	
	
		$l=$m;
		$i=0;
		$p=0;
		while ($l >= 0){
		$str1.=$str2[$l];
		$p=$l+1;
		if($p>=$m+1){
	
		}else{
			if ($l%3==0)
			{	$i++;			
				if($p<>1)
					$str1.=".";						
			}
		}	
			$i++;
			$l--;
		}
		
		if($cntarr > 1){
			if($sign == '1'){
				return "-".($str1).",".$n1;
			}else{
				return $str1.",".$n1;
			}
		}else{
			if($sign == '1')
				return "-".($str1);
			else
				return $str1;
		}
}



//=========================================================================================================
//================================    Fix Magic Quote   ===================================================
//=========================================================================================================
function fix_magic_quotes ($var = NULL, $sybase = NULL){
	// if sybase style quoting isn't specified, use ini setting
	if ( !isset ($sybase) )	{
		$sybase = ini_get ('magic_quotes_sybase');
	}
	// if no var is specified, fix all affected superglobals
	if ( !isset ($var) ){
		// if magic quotes is enabled
		if ( get_magic_quotes_gpc () ){
			// workaround because magic_quotes does not change $_SERVER['argv']
			$argv = isset($_SERVER['argv']) ? $_SERVER['argv'] : NULL; 

			// fix all affected arrays
			foreach ( array ('_ENV', '_REQUEST', '_GET', '_POST', '_COOKIE', '_SERVER') as $var ){
				$GLOBALS[$var] = fix_magic_quotes ($GLOBALS[$var], $sybase);
			}

			$_SERVER['argv'] = $argv;

			// turn off magic quotes, this is so scripts which
			// are sensitive to the setting will work correctly
			ini_set ('magic_quotes_gpc', 0);
		}
		// disable magic_quotes_sybase
		if ( $sybase )
		{
			ini_set ('magic_quotes_sybase', 0);
		}
		// disable magic_quotes_runtime
		set_magic_quotes_runtime (0);
		return TRUE;
	}
	// if var is an array, fix each element
	if ( is_array ($var) )
	{
		foreach ( $var as $key => $val )
		{
			$var[$key] = fix_magic_quotes ($val, $sybase);
		}

		return $var;
	}
	// if var is a string, strip slashes
	if ( is_string ($var) )
	{
		return $sybase ? str_replace ('\'\'', '\'', $var) : stripslashes ($var);
	}
	// otherwise ignore
	return $var;
}

//
if (!function_exists('array_combine')) {
   //{{{ array_combine
   /**
   * Creates an array by using one array for keys and another for its values
   * @param    array    $keys
   * @param    array    $values
   * @return    Returns an array by using the values from the keys array as keys and the values from the values array as the corresponding values.<br />Returns FALSE if the number of elements for each array isn't equal or if the arrays are empty.
   * @author    <dreptack [at] op [dot] pl>
   */
   function array_combine($keys, $values){
       if (!is_array($keys) || !is_array($values) || !$keys || !$values || count($keys) != count($values)) {
           return false;
       }
       reset($keys);
       reset($values);
       $retarr = array();
       while (($keyarr = each($keys)) && ($valarr = each($values))) {
           $retarr[$keyarr['value']] = $valarr['value'];
       } 
       return $retarr;
   } //}}}
} //endif function_exists


?>