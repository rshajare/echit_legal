<?
include_once("classdatabase.php");

class intro_payment extends Database {
	private $TableName ;
	private $TableID ;
	public $TableIDVal ;
	
	
	private $SessionLanguage ;
	private $TableFieldArray;
	public $TableFieldValArray;

	public function __construct(){
		global $t;
		$this->TableName 			= 'tbl_introducer_payment';
		$this->TableID 				= 'payment_id';
		$this->TableFieldArray 		= array(			
			array('payment_id', 1 , 1, PDO::PARAM_INT),
			array('introducer_id', 1 , 0, PDO::PARAM_INT),
			array('amount', 1 , 0, PDO::PARAM_INT),
			array('pay_type', 1 , 0, PDO::PARAM_STR),
			array('cheque_no', 1 , 0, PDO::PARAM_INT),
			array('bank_name', 1 , 0, PDO::PARAM_INT),
			array('cheque_date', 1 , 0, PDO::PARAM_INT),
			array('payment_date', 1 , 0, PDO::PARAM_INT),
			array('tds', 1 , 0, PDO::PARAM_INT),
			array('adjustment', 1 , 0, PDO::PARAM_INT),
			array('main_bank_id', 1 , 1, PDO::PARAM_INT),
			array('main_branch_id', 1 , 0, PDO::PARAM_INT),
			array('note', 1 , 0, PDO::PARAM_STR),
			array('type', 1 , 0, PDO::PARAM_STR)
		);
		$this->SessionLanguage 		= $_SESSION['opt_lang'];
		$this->DBConnect();
		$arrIntro_type 				= array();

	}
	
	public function Addintro_payment(){
		global $t;	
		$BranchCode		= $_POST["txtBranchCode"];
		$req_introtype	= $_POST["txtintro_type"];
		$introducer_id	= $_GET["txtintroducer"];
		$pay_type		= $_POST["txtPayment"];

		$t->assign ( 'BranchCode', $BranchCode );
		$t->assign ( 'req_introtype', $req_introtype );
		$t->assign ( 'introducer_id', $introducer_id );
		
		if($introducer_id!=""){
		     $customer_paid=$this->getCustomerPaid($introducer_id,'')	;
		}
		$Introducer_percentage 		= fngetValue("tbl_mn_app_setting","field_value","field_name = '".$req_introtype."_comm_percentage'");
		$incentive=	$customer_paid*($Introducer_percentage)/100;
		
		$incentive_paid=decimal_num(fnGetValue("tbl_introducer_payment","sum(amount+tds)","introducer_id='$introducer_id'   "));
		
		$balancePayble=decimal_num($incentive-$incentive_paid);
        
		$Introducer_tds_percentage 	= fngetValue("tbl_mn_app_setting","field_value","field_name = 'Introducer_tds_percentage'");
		$tds=$balancePayble*($Introducer_tds_percentage)/100;
		
		//$Adjustment=fnGetValue("tbl_introducer_payment","adjustment","introducer_id='$introducer_id'");
		$netPayble=decimal_num($balancePayble-$tds-$Adjustment);

		$t->assign ( 'customer_paid', $customer_paid);
        $t->assign ( 'incentive', $incentive );	
		$t->assign ( 'incentive_paid', $incentive_paid );
		$t->assign ( 'balancePayble', $balancePayble );
		$t->assign ( 'Introducer_tds_percentage', $Introducer_tds_percentage );
		$t->assign ( 'tds', $tds );
		$t->assign ( 'Adjustment',$Adjustment);		
		$t->assign ( 'netPayble', $netPayble);
		
		
		if(($req_introtype =='subscriber')){
			$introsql = " SELECT *  FROM `tbl_sur_sign_up` LEFT JOIN `subdirdetails` ON `tbl_sur_sign_up`.`ref_user_code` = `subdirdetails`.`SubID` 
					 where ref_user_type='$req_introtype'  and `subdirdetails`.BranchCode='$Branchcode'";
		}else{
			$introsql = " SELECT *  
				 FROM (`tbl_sur_sign_up` left join  `tbl_sur_sign_up_other`  on (`tbl_sur_sign_up`.`personnel_id`=`tbl_sur_sign_up_other`.`personnel_id`))
						left join  `tbl_sur_sign_up_branches`  on (`tbl_sur_sign_up`.`personnel_id`=`tbl_sur_sign_up_branches`.`personnel_id`) 
				 WHERE ref_user_type='$req_introtype' and `tbl_sur_sign_up_branches`.main_branch_id='$BranchCode'  ";

		}
		$intro_list_temp 	= $this->db_pdo->prepare($introsql);
		$intro_list_temp->execute(); 
		foreach( $intro_list_temp as $index => $intro_list_row ) {
			$userCode 							= $intro_list_row["user_code"];
			if(($introlist =='introducer')){$userCode = $userCode." - ";}
			$arrintro_list['personnel_id'][] 	= $intro_list_row['personnel_id'];
			$arrintro_list['first_name'][] 		= $userCode." ".$intro_list_row['first_name']." ".$intro_list_row['last_name'];
			
		}	
		$t->assign ( 'arrintro_list', $arrintro_list );
		
		if ($BranchCode!=""){
			$arrbankinfo = array();
			$bnk_temp = $this->db_pdo->prepare( "SELECT BankID,Bankname,Main_BankID FROM banks where use_for_deposites=1 and lang='".$this->SessionLanguage."' and main_branch_id = '".$BranchCode."' order by `Bankname` asc" );
			$bnk_temp->execute();
			foreach( $bnk_temp as $index => $bnk_row ) {
				$arrbankinfo['Main_BankID'][] = $bnk_row['Main_BankID'];
				$arrbankinfo['Bankname'][] = $bnk_row['Bankname'];
			}
		}
		$t->assign ( 'arrbankinfo', $arrbankinfo );	
		$t->display('intro_payment_add.htm');
	}
	public function Editintro_payment(){
		global $t;	
		$req_payment_id = $_GET["payment_id"];
		$t->assign( 'payment_id', $req_payment_id );
		
		
		$sql= "	SELECT * FROM tbl_introducer_payment where payment_id=$req_payment_id";
		$temp = $this->db_pdo->prepare($sql);
		$temp->execute();
		foreach( $temp as $index => $row ) 
		{
			  $BranchCode =$row["main_branch_id"];
			  
			  $req_introtype=fnGetValue('tbl_sur_sign_up','ref_user_type',"personnel_id ='".$row["introducer_id"]."'");	
			  $t->assign( 'req_introtype', $req_introtype );
 			  
		   	  $customer_paid=$this->getCustomerPaid($row["introducer_id"],$row["payment_date"])	;
			  $row["payment_date"]=sqldateout($row["payment_date"]);
			  $Introducer_percentage 		= fngetValue("tbl_mn_app_setting","field_value","field_name = '".$req_introtype."_comm_percentage'");
			  $Introducer_tds_percentage 	= fngetValue("tbl_mn_app_setting","field_value","field_name = 'Introducer_tds_percentage'");

			  $incentive  =	$customer_paid*($Introducer_percentage)/100;
			 
			  $incentive_paid 	= decimal_num(fnGetValue("tbl_introducer_payment","sum(amount+tds)","introducer_id='".$row["introducer_id"]."' and payment_id < '".$row["payment_id"]."'"));
			  $balancePayble  	= decimal_num($incentive-$incentive_paid);
			  $row["balancepaid"]		= $row["amount"] + $row["tds"] + $row["adjustment"];
			  
			  $data[] = $row;
			  
			  $t->assign( 'customer_paid', $customer_paid );
			  $t->assign( 'incentive', $incentive );
			  $t->assign( 'incentive_paid', $incentive_paid );
			  $t->assign( 'balancePayble', $balancePayble );
			  $t->assign( 'Introducer_tds_percentage', $Introducer_tds_percentage );
		
		}
		$t->assign( 'data', $data );
		if(($req_introtype =='subscriber')){
			$introsql = " SELECT *  FROM `tbl_sur_sign_up` LEFT JOIN `subdirdetails` ON `tbl_sur_sign_up`.`ref_user_code` = `subdirdetails`.`SubID` 
					 where ref_user_type='$req_introtype'  and `subdirdetails`.BranchCode='$Branchcode'";
		}else{
			$introsql = " SELECT *  FROM `tbl_sur_sign_up` left join  `tbl_sur_sign_up_branches`  on 
					`tbl_sur_sign_up`.`personnel_id`=`tbl_sur_sign_up_branches`.`personnel_id` 
					 where ref_user_type='$req_introtype' and `tbl_sur_sign_up_branches`.main_branch_id='$BranchCode'";
		}
		//echo $introsql;	
		$intro_list_temp = $this->db_pdo->prepare($introsql);
		$intro_list_temp->execute();
		foreach( $intro_list_temp as $index => $intro_list_row ) {
			$userCode 							= $intro_list_row["user_code"];
			if(($req_introtype =='introducer')){
				$userCode						= $userCode." - ";
			}
			$arrintro_list['personnel_id'][] 	= $intro_list_row['personnel_id'];
			$arrintro_list['first_name'][] 		= $userCode." ".$intro_list_row['first_name']." ".$intro_list_row['last_name'];
		}	
		$t->assign ( 'arrintro_list', $arrintro_list );
		
		if ($BranchCode!=""){
			$arrbankinfo = array();
			$bnk_temp = $this->db_pdo->prepare( "SELECT BankID,Bankname,Main_BankID FROM banks where use_for_deposites=1 and lang='".$this->SessionLanguage."' and main_branch_id = '".$BranchCode."' order by `Bankname` asc" );
			$bnk_temp->execute();
			foreach( $bnk_temp as $index => $bnk_row ) {
				$arrbankinfo['Main_BankID'][] = $bnk_row['Main_BankID'];
				$arrbankinfo['Bankname'][] = $bnk_row['Bankname'];
			}
		}
		$t->assign ( 'arrbankinfo', $arrbankinfo );	
		
		$t->display('intro_payment_edit.htm');
	}
	public function getCustomerPaid($IntroducerID,$OnDate){
		global $t;	
		
		$sql = "
			SELECT FormRecOn,groupforming.SubID,subdirdetails.SubCode,
				groupforming.main_branch_id,Entrolled,groupforming.IntroducedBy, GroupNoEnrolled,
				GroupIDEnrolled, TktNoEnrolled,ID, ChitValue, 
				first_name, last_name, user_code, subdirdetails.FullName,AddrsCorres,MobNoCorres	
			FROM (groupforming LEFT JOIN tbl_sur_sign_up on (groupforming.IntroducedBy = tbl_sur_sign_up.personnel_id)) 
				LEFT JOIN subdirdetails on groupforming.SubID = subdirdetails.SubID 
			WHERE groupforming.IntroducedBy = '".$IntroducerID."' and Entrolled = '1' ";	
		//echo $sql;			
		$res_temp = $this->db_pdo->prepare( $sql );
		$res_temp->execute();
		foreach( $res_temp as $index => $row ) {
			if (($row['GroupIDEnrolled']!="") && ($row['TktNoEnrolled']!="")){
				$custpaid =GetPaid_Dividend($row['main_branch_id'],$row['GroupNoEnrolled'],$row['TktNoEnrolled'],'','', '','',$OnDate);
				//echo $row['GroupNoEnrolled']."-".$row['TktNoEnrolled']."-".$custpaid[0]."<br>";
				//$custpaid=$req_paid[0];				
/*				$custpaid				= FnGetValueMultiple("subscription_details inner join auction on subscription_details.Auction_ID=auction.Auction_ID inner join
																transaction ON ( subscription_details.Transaction_ID = transaction.Transaction_ID )  ",
												"sum(subscription_details.Subcription),sum(IFNULL(LateFee,0) + IFNULL(Subcription_Fee,0) + IFNULL(other_charge,0) + IFNULL(check_return_fee,0) + IFNULL(transfer_fee,0)+IFNULL(legal_charge,0) + IFNULL(charges_other,0) + IFNULL(ser_tax,0))",
												"auction.main_branch_id='".$row['main_branch_id']."' and TicketNo='".$row['TktNoEnrolled']."' and 
												GroupID='".$row['GroupIDEnrolled']."' and transaction.status<2 and 
												subscription_details.SubID='".$row['SubID']."' ");*/

				$custpaid_advance		= FnGetValueMultiple("tbl_advance_payment inner join
																transaction ON ( tbl_advance_payment.Transaction_ID = transaction.Transaction_ID )  ",
												"sum(tbl_advance_payment.Advance_Amount),sum(IFNULL(LateFee,0) + IFNULL(Subcription_Fee,0) + IFNULL(other_charge,0) + IFNULL(check_return_fee,0) + IFNULL(transfer_fee,0) )",
												"tbl_advance_payment.main_branch_id='".$row['main_branch_id']."' and Ticket_No='".$row['TktNoEnrolled']."' and 
												GroupID='".$row['GroupIDEnrolled']."' and transaction.status<2 and 
												tbl_advance_payment.SubID='".$row['SubID']."' ");

/*				$custpaid_removed		= FnGetValueMultiple("subscription_details_removed inner join auction on subscription_details_removed.Auction_ID=auction.Auction_ID inner join
												  transaction_removed ON ( subscription_details_removed.Transaction_ID = transaction_removed.Transaction_ID_Act )  ",
												"sum(subscription_details_removed.Subcription),sum(IFNULL(LateFee,0) + IFNULL(Subcription_Fee,0) + IFNULL(other_charge,0) + IFNULL(check_return_fee,0) + IFNULL(transfer_fee,0)+IFNULL(legal_charge,0) + IFNULL(charges_other,0) 	)",
												"auction.main_branch_id='".$row['main_branch_id']."' and TicketNo='".$row['TktNoEnrolled']."' and 
												auction.GroupID='".$row['GroupIDEnrolled']."' and transaction_removed.status<2 and 
												subscription_details_removed.SubID='".$row['SubID']."' ");
*/	
				$tot_custpaid =$tot_custpaid+ $custpaid[0]+$custpaid_advance[0]+$custpaid_removed[0]-$custpaid[6]-$custpaid_advance[1];//-$custpaid_removed[1];
			}
		}
		
		return($tot_custpaid);
	}
	public function add_vouchers($req_payment_id){
		//global $t;
		include_once( 'acc_include_functions.php');
		$one_acc_for_group=fnGetValue("tbl_mn_app_setting","field_value","field_name = 'one_acc_for_group'");
		$sql= "	SELECT * FROM tbl_introducer_payment WHERE payment_id='$req_payment_id' ";
		$temp = $this->db_pdo->prepare($sql);
		$temp->execute();
		foreach( $temp as $index => $row ) {
			$req_txtBranchCode	= $row["main_branch_id"];
			$req_txtBank_id		= $row["bank_name"];
			$req_introducer_id	= $row["introducer_id"];
			$req_amount			= $row["amount"];
			$req_txttds			= $row["tds"];
			$req_Adjustment		= $row["adjustment"];

			$req_total_amount	= $req_amount+$req_Adjustment+ $req_txttds;
			
			$req_payment_id		= $row["payment_id"];
			$req_txtpayment_date= $row["payment_date"];
			$req_pay_type		= $row["pay_type"];
			$req_txtCheque_No	= $row["cheque_no"];
			$req_adjustment_ID	= Fngetvalue("tbl_acc_adjustment","adjustment_ID"," AppID='$req_introducer_id' and related_id='$req_payment_id' and related_type='introducer' ");
			$req_paid			= $req_amount;
			
			$finacial_yr_id				= fngetvalue('tbl_acc_financial_years','main_acc_fianancial_year_id'," main_branch_id='$req_txtBranchCode' and is_default='1' and lang='english'");
			$account_type_Agent_Comm 	= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Direct Expenses' and lang = '".$this->SessionLanguage."'"); 
			$req_Agent_Comm_acc			= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang= '".$this->SessionLanguage."' and account_type = '$account_type_Agent_Comm' and account_name='Agent Commission' and main_branch_id='$req_txtBranchCode' and acc_fianancial_year_id='$finacial_yr_id'");    
			if ($req_Agent_Comm_acc==0){ 
				$req_Agent_Comm_acc=create_account("Agent Commission",$account_type_Agent_Comm,"","",$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$req_txtBranchCode);
			}
		
			$req_bank_acc_id 		= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang= '".$this->SessionLanguage."' and related_type  = 'bank' and related_id='$req_txtBank_id' and main_branch_id='$req_txtBranchCode' and acc_fianancial_year_id='$finacial_yr_id'");
			$req_cash_acc_id 		= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang= '".$this->SessionLanguage."' and account_name='Cash' and  main_branch_id='$req_txtBranchCode' and acc_fianancial_year_id='$finacial_yr_id'");
	
			$req_introducer_acc= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang= '".$this->SessionLanguage."' and related_type  = 'introducer' and related_id='$req_introducer_id' and main_branch_id='$req_txtBranchCode' and acc_fianancial_year_id='$finacial_yr_id'");    
			if ($req_introducer_acc==0){ 
				$account_name=fnGetValue("tbl_sur_sign_up","concat(first_name,' ',last_name)","personnel_id = '$req_introducer_id' ");
				$account_type_id = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Sundry Creditors' and lang = '".$this->SessionLanguage."' ");
				$req_related_type="introducer";
				$related_id=$req_introducer_id;			
				$req_introducer_acc=create_account($account_name,$account_type_id,$req_related_type,$related_id,$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$req_txtBranchCode);
			}
			if ($req_txttds > 0){ 
				$account_type_tds = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Duties & Taxes' and lang = '".$this->SessionLanguage."'"); 
				$req_tds_acc   = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '".$this->SessionLanguage."' and account_type = '$account_type_tds' and account_name='TDS' and main_branch_id='$req_txtBranchCode'");
	
				if ($req_tds_acc==0){
					$req_tds_acc=create_account('TDS',$account_type_tds,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$req_txtBranchCode);
				}
			}
			$req_from_acc_id  = array();
			$req_to_acc_id 	  = array();
		
			$req_from_acc_id[0][0] = $req_Agent_Comm_acc;	
			$req_from_acc_id[0][1] = $req_total_amount;	
		
			$req_to_acc_id[0][0]   = $req_introducer_acc;
		
			if ($req_txttds > 0){ 
				$req_to_acc_id[0][1] = $req_total_amount- $req_txttds;	
			}else{	
				$req_to_acc_id[0][1] = $req_total_amount;	
			}
			if ($req_txttds > 0){
				$req_to_acc_id[1][0] = $req_tds_acc;
				$req_to_acc_id[1][1] = $req_txttds;
			}
	  
			if ($one_acc_for_group!=1){
				$acc_trans_main_id_bill 	= fnGetValue("tbl_acc_relate","acc_transaction_id","(acc_transaction_type = 'intro_comm_bill') and refered_id = '$req_payment_id'");
				add_voucher('intro_comm_bill',$req_from_acc_id,$req_payment_id,$req_total_amount,$req_txtpayment_date,"Introducer Commission",$req_txtBranchCode,$acc_trans_main_id_bill,$req_to_acc_id);	
			}
			
			if ($req_paid>0){
				if ($req_pay_type=="cash" && $req_to_acc_id[0][1]>0 ){
					$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","(acc_transaction_type = 'Payment_intro_chk' or acc_transaction_type = 'Payment_intro') and refered_id = '$req_payment_id'");
					add_voucher('Payment_intro',$req_introducer_acc,$req_payment_id,$req_paid,$req_txtpayment_date,"Introducer commision Payment by cash",$req_txtBranchCode,$acc_trans_main_id,$req_cash_acc_id);
				}elseif($req_pay_type=="cheque" && $req_to_acc_id[0][1]>0){
					$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","(acc_transaction_type = 'Payment_intro_chk' or acc_transaction_type = 'Payment_intro') and refered_id = '$req_payment_id'");
					add_voucher('Payment_intro_chk',$req_introducer_acc,$req_payment_id,$req_paid,$req_txtpayment_date,"Introducer commision Payment by cheque No: $req_txtCheque_No",$req_txtBranchCode,$acc_trans_main_id,$req_bank_acc_id);
				}
			}
			if ($req_Adjustment >0){	 
				//account adjace------------------------------
				$account_type_Adjustment= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name ='Sundry Creditors' and lang ='".$this->SessionLanguage."'"); 
				$req_Adjustment_acc= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang= '".$this->SessionLanguage."' and account_type ='$account_type_Adjustment' and account_name='Adjacement' and main_branch_id='$req_txtBranchCode' and acc_fianancial_year_id='$finacial_yr_id'");    
				if ($req_Adjustment_acc==0){ 
					$req_Adjustment_acc=create_account("Adjacement",$account_type_Adjustment,"","",$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$req_txtBranchCode);
				}				
				$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type ='intro_adjace' and refered_id = '$req_adjustment_ID'");
				add_voucher('intro_adjace',$req_introducer_acc,$req_adjustment_ID,$req_Adjustment,$req_txtpayment_date,"Introducer commision Payment Adjustment  ",$req_txtBranchCode,$acc_trans_main_id,$req_Adjustment_acc);
			}else{
				$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type ='intro_adjace' and refered_id = '$req_adjustment_ID'");
				delete_voucher($acc_trans_main_id);
			}
			//---------------------------------		
		}
	}
	public function delete_vouchers($req_payment_id){
		include_once( 'acc_include_functions.php');

		$acc_trans_main_id_bill 	= fnGetValue("tbl_acc_relate","acc_transaction_id","(acc_transaction_type = 'intro_comm_bill') and refered_id = '$req_payment_id'");
		delete_voucher($acc_trans_main_id_bill);

		$req_introducer_id	= Fngetvalue("tbl_introducer_payment","introducer_id"," payment_id='$req_payment_id' ");
		$req_adjustment_ID	= Fngetvalue("tbl_acc_adjustment","adjustment_ID"," AppID='$req_introducer_id' and related_id='$req_payment_id' and related_type='introducer' ");
		$adj_acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type ='intro_adjace' and refered_id = '$req_adjustment_ID'");
		delete_voucher($adj_acc_trans_main_id);
		
		$pay_acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","(acc_transaction_type = 'Payment_intro_chk' or acc_transaction_type = 'Payment_intro') and refered_id = '$req_payment_id'");
		delete_voucher($pay_acc_trans_main_id);
	}

	public function destroy($req_payment_id){
		$this->delete_vouchers($req_payment_id);
		$req_introducer_id	= Fngetvalue("tbl_introducer_payment","introducer_id"," payment_id='$req_payment_id' ");
		$req_adjustment_ID	= Fngetvalue("tbl_acc_adjustment","adjustment_ID"," AppID='$req_introducer_id' and related_id='$req_payment_id' and related_type='introducer' ");
		$this->DeleteRecord("tbl_acc_adjustment","adjustment_ID",$req_adjustment_ID);
		$this->DeleteRecord($this->TableName,$this->TableID,$req_payment_id);

	}
	public function store(){
		$req_txtpayment_date 	= sqldatein($_POST["txtpayment_date"]);
		$req_introducer_id 		= $_POST["txtintroducer"];
		$req_Adjustment 		= $_POST["txtAdjustment"];

		$this->TableFieldValArray[1][0]	= $req_introducer_id;
		$this->TableFieldValArray[2][0]	= $_POST["txtamount"];
		$this->TableFieldValArray[3][0]	= $_POST["txtPayment"];
		$this->TableFieldValArray[4][0]	= $_POST["txtCheque_No"];
		$this->TableFieldValArray[5][0]	= $_POST["txtBank_Name"];
		$this->TableFieldValArray[6][0]	= sqldatein($_POST["txtCheque_Date"]);
		$this->TableFieldValArray[7][0]	= $req_txtpayment_date;
		$this->TableFieldValArray[8][0]	= $_POST["txttds"];
		$this->TableFieldValArray[9][0]	= $req_Adjustment;
		$this->TableFieldValArray[11][0]= $_POST["txtBranchCode"];
		$this->TableFieldValArray[12][0]= $_POST["note"];
		$req_payment_id=$this->InsertRecord($this->TableName,$this->TableFieldArray,$this->TableFieldValArray);
		
		if($req_Adjustment>0){
			$sql_adjace_insert="INSERT INTO `tbl_acc_adjustment` (`adjustment_ID`, `AppID`, `related_type`,`related_id`, 
			`Payment_date`, `AdjustAmount`, `paid`) 
			VALUES (NULL,'$req_introducer_id','introducer','$req_payment_id',
			'$req_txtpayment_date','$req_Adjustment','')";
			$temp = $this->db_pdo->prepare($sql_adjace_insert);
			$temp->execute();
		}
		$this->add_vouchers($req_payment_id);
		header("Location:chit_introducer_payment.php");
	}
	public function update(){
		$req_txtpayment_date 	= sqldatein($_POST["txtpayment_date"]);
		$req_introducer_id 		= $_POST["txtintroducer"];
		$req_Adjustment 		= $_POST["txtAdjustment"];

		$this->TableFieldValArray[1][0]	= $req_introducer_id;
		$this->TableFieldValArray[2][0]	= $_POST["txtamount"];
		$this->TableFieldValArray[3][0]	= $_POST["txtPayment"];
		$this->TableFieldValArray[4][0]	= $_POST["txtCheque_No"];
		$this->TableFieldValArray[5][0]	= $_POST["txtBank_Name"];
		$this->TableFieldValArray[6][0]	= sqldatein($_POST["txtCheque_Date"]);
		$this->TableFieldValArray[7][0]	= $req_txtpayment_date;
		$this->TableFieldValArray[8][0]	= $_POST["txttds"];
		$this->TableFieldValArray[9][0]	= $req_Adjustment;
		$this->TableFieldValArray[11][0]= $_POST["txtBranchCode"];
		$this->TableFieldValArray[12][0]= $_POST["note"];
		
		$req_payment_id=$this->UpdateRecord($this->TableName,$this->TableFieldArray,$this->TableID,$this->TableFieldValArray);

		if($req_Adjustment>0){
			$req_adjustment_ID=Fngetvalue("tbl_acc_adjustment","adjustment_ID"," AppID='$req_introducer_id' and related_id='$req_payment_id' and related_type='introducer' ");
			if ($req_adjustment_ID>0){
				$sql_adjace_update="update tbl_acc_adjustment set AdjustAmount='$req_Adjustment', `Payment_date`='$req_txtpayment_date'  where AppID='$req_introducer_id' and related_id='$req_payment_id' and related_type='introducer'";
				$temp = $this->db_pdo->prepare($sql_adjace_update);
				$temp->execute();
			}else{
				$sql_adjace_insert="INSERT INTO `tbl_acc_adjustment` (`adjustment_ID`, `AppID`, `related_type`,`related_id`, 
				`Payment_date`, `AdjustAmount`, `paid`) 
				VALUES (NULL,'$req_introducer_id','introducer','$req_payment_id',
				'$req_txtpayment_date','$req_Adjustment','')";
				$temp = $this->db_pdo->prepare($sql_adjace_insert);
				$temp->execute();
			}
		}
		$this->add_vouchers($req_payment_id);
		
		header("Location:chit_introducer_payment.php");
	}
	
	public function index(){
		global $t;
		
		$sql= "	SELECT * FROM tbl_introducer_payment 
				LEFT JOIN tbl_sur_sign_up on tbl_introducer_payment.introducer_id = tbl_sur_sign_up.personnel_id ";
				
		$tmpblnWhere = 0;
		if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort'];	else $reqsort='payment_date';
		if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
		if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";	 
		//---------------------------------Paging--------------------------------------------------
		if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
		if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
		$t->assign('reqord',$reqord);
		//-------------------------------end  Paging------------------------------------------------------
					
			if ( isset( $_REQUEST['txtkeyword1'] ) ){
				$reqkeyword1 = $_REQUEST['txtkeyword1'] ;
				$t->assign ( 'reqkeyword1', $reqkeyword1);
				if (strcmp($reqkeyword1,"")<>0){			
					if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
					$sql = $sql . "tbl_sur_sign_up.first_name like '%$reqkeyword1%'";
					$PageString = $PageString  .'&txtkeyword1='.$reqkeyword1;
				}
			}
			
			$reqkeyword21 = $_REQUEST['txtkeyword21'] ;
			$reqkeyword22 = $_REQUEST['txtkeyword22'] ;
			//if($reqkeyword21 == "") $reqkeyword21 = date("d/m/Y");
			if($reqkeyword22 == "") $reqkeyword22 = date("d/m/Y");
			//if ((strcmp($reqkeyword21,"")<>0)||(strcmp($reqkeyword22,"")<>0)){				
			if ((strcmp($reqkeyword21,"")<>0)&&(strcmp($reqkeyword22,"")<>0)){				
			$reqkeyword21 = sqldatein($reqkeyword21) ;
			$reqkeyword22 = sqldatein($reqkeyword22) ;
			if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
			$sql = $sql . " payment_date >= '$reqkeyword21' and payment_date <= '$reqkeyword22' ";
			$PageString = $PageString  .'&txtkeyword21='.$reqkeyword21.'&txtkeyword22='.$reqkeyword22;
			}elseif (strcmp($reqkeyword21,"")<>0){				
			$reqkeyword21 = sqldatein($reqkeyword21) ;
			if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
			$sql = $sql . " payment_date >= '$reqkeyword21'";
			$PageString = $PageString  .'&txtkeyword21='.$reqkeyword21;
			}elseif (strcmp($reqkeyword22,"")<>0){				
			$reqkeyword22 = sqldatein($reqkeyword22) ;
			if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
			$sql = $sql . " payment_date <= '$reqkeyword22'";
			$PageString = $PageString  .'&txtkeyword22='.$reqkeyword22;
			}
			$t->assign ( 'reqkeyword21', sqldateout($reqkeyword21));
			$t->assign ( 'reqkeyword22', sqldateout($reqkeyword22));
		
		
		if (strcmp($reqsort,"")<>0)  $sql = $sql . ' order by ' . $reqsort ;
		$sql = $sql . ' ' . $reqord ;
		
		// ---------------- call paging function---------------------------------------------------
		$strfnPagingSql = $sql; include 'includes/callpaging.php'; 
		$temp = $this->db_pdo->prepare($sql);
		$temp->execute();
		$cnt=1;

		//echo $sql;
		foreach( $temp as $index => $row ) 
		{
			$row["cnt"]	= $cnt++ ;
			$row["payment_date"]=sqldateout($row["payment_date"]);
			$row["cheque_date"]=sqldateout($row["cheque_date"]);
			$row["intro_name"]	= fngetValue("tbl_sur_sign_up","first_name","personnel_id='".$row["introducer_id"]."'");
			$row["bank_name"]	= fngetValue("banks","Bankname","Main_BankID='".$row["bank_name"]."'");
			if($row["adjustment"]>0){
				$req_introducer_id 	= $row["introducer_id"];
				$req_payment_id 	= $row["payment_id"];
				$req_adjustment_ID	= Fngetvalue("tbl_acc_adjustment","adjustment_ID"," AppID='$req_introducer_id' and related_id='$req_payment_id' and related_type='introducer' ");
				$row["adjustment_rec_cnt"]	= fngetcount("transaction inner join transaction_details ON ( transaction.Transaction_ID =transaction_details.trans_id)","transaction_details.adjustment_ID='".$req_adjustment_ID."' and transaction.status<2");
			}
			$intro_paymentResult[]=$row;
		}

		//$t->assign ( 'present', $presentResult);
		$t->assign ( 'intro_payment', $intro_paymentResult);
		$t->display('intro_payment.htm');
		}
		
	}
		
?>
