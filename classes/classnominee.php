<?php 

include_once("classdatabase.php");

class nominee extends Database{
	private $TableName ;
	private $TableID ;
	private $SessionLanguage ;
	private $TableFieldArray;
	private $TableFieldValArray;
	public function __construct(){
		global $t;
		$this->TableName 			= 'tbl_nominee';
		$this->TableID 				= 'nom_id';
		$this->TableFieldArray 		= array(			
			array('nom_id', 1 , 0, PDO::PARAM_INT),
			array('AppID', 1 , 0, PDO::PARAM_INT),
			array('NomineeName', 1 , 1, PDO::PARAM_STR),
			array('AddrsNominee', 1 , 1, PDO::PARAM_STR),
			array('RelationNominee', 1 , 1, PDO::PARAM_STR),
			array('NomineeAge', 1 , 1, PDO::PARAM_INT),
			array('NomineeDob', 1 , 0, PDO::PARAM_STR),
			array('TelNoNominee', 1 , 1, PDO::PARAM_STR),
		);
		$this->SessionLanguage 		= $_SESSION['opt_lang'];
		$this->DBConnect();
	}
	public function add(){
		global $t;	
		if ( isset( $_REQUEST['txtmain_branch_id'] ) ){
				$reqmain_branch_id = $_REQUEST['txtmain_branch_id'];
		}else{
				$reqmain_branch_id = DEFAULT_BRANCH_ID;
		}
		$t->assign ( 'reqmain_branch_id', $reqmain_branch_id );

		if ($reqmain_branch_id!=""){
			$app_arr = array();
			$temp = $this->db_pdo->prepare("SELECT * from groupdetails WHERE  main_branch_id='$reqmain_branch_id' ");
			$temp->execute();		
			foreach( $temp as $index => $row ) {
				$app_arr['GroupID'][] = $row['GroupID'];
				$app_arr['GroupNo'][] = $row['GroupNo'];
			}
			$t->assign ( 'app_arr', $app_arr );
		}
		
		$t->display('admnominee_new.htm');
	}
	public function edit(){ // edit 
		global $t;
		$main 	= array();
		$nom_id = $_GET[ 'nom_id' ];
		
		$app_arr = array();
		$sql= "SELECT * 
				FROM ".$this->TableName. " 
				LEFT JOIN applicantdetails ON (".$this->TableName.".AppID=applicantdetails.AppID)
				WHERE ".$this->TableID." = '$nom_id'"  ;
		$temp = $this->db_pdo->prepare($sql);
		//echo $sql;
		$temp->execute();
		foreach( $temp as $index => $row ) {
			$row['NomineeDob']=sqldateout($row['NomineeDob']);
	  		$data[]=$row;
			if ($row['main_branch_id']!=""){
				$app_arr = array();
				$temp = $this->db_pdo->prepare("SELECT * from groupdetails WHERE  main_branch_id='".$row['main_branch_id']."' ");
				$temp->execute();		
				foreach( $temp as $index => $row ) {
					$app_arr['GroupID'][] = $row['GroupID'];
					$app_arr['GroupNo'][] = $row['GroupNo'];
				}
				$t->assign ( 'app_arr', $app_arr );
			}
			if ($row['GroupNo']!=""){
				$tkt_arr = array();
				$temp = $this->db_pdo->prepare("SELECT * from applicantdetails WHERE  GroupID='".$row['GroupID']."' ORDER BY Ticketno ASC");
				$temp->execute();		
				foreach( $temp as $index => $row ) {
					$tkt_arr['Ticketno'][] = $row['Ticketno'];
				}
				$t->assign ( 'tkt_arr', $tkt_arr );
			}
			

		}
		$t->assign ( 'data', $data );
			
		$t->assign( 'nom_id', $nom_id );
		$t->display('admnominee_edit.htm');
	}
	public function store(){
		
		$reqTicketno = $_POST["txtTicketno"];
		$reqGroupNo = $_POST["txtGroupNo"];
		
		$AppID    = fnGetValue("applicantdetails","AppID"," Ticketno='$reqTicketno' and GroupID ='$reqGroupNo'");
	    $this->TableFieldValArray[1][0]=$AppID;
		
	
		$NomineeDob = sqldatein($_POST['txt'.$this->TableFieldArray[6][0]]);
		$this->TableFieldValArray[6][0]=$NomineeDob;
		//echo $NomineeDob;
		
		$nom_id=$this->InsertRecord($this->TableName,$this->TableFieldArray,$this->TableFieldValArray);
		header("Location: admnominee.php");

	}

	public function update(){
		$reqTicketno = $_POST["txtTicketno"];
		$reqGroupNo = $_POST["txtGroupNo"];
		$this->TableFieldValArray[1][0] = fnGetValue("applicantdetails","AppID"," Ticketno='$reqTicketno' and GroupNo ='$reqGroupNo'");
		
		$this->TableFieldValArray[6][0]=sqldatein($_POST['txt'.$this->TableFieldArray[6][0]])	;
		$this->UpdateRecord($this->TableName,$this->TableFieldArray,$this->TableID,$this->TableFieldValArray,"");

	}
	public function destroy($nom_id){
		$del_id_val	= $nom_id;	
		$this->DeleteRecord($this->TableName,$this->TableID,$del_id_val);	
	}
	public function index(){
		global $t;
		
		if ( isset( $_REQUEST['txtmain_branch_id'] ) ){
			$reqmain_branch_id = $_REQUEST['txtmain_branch_id'];
		}else{
			$reqmain_branch_id = DEFAULT_BRANCH_ID;
		}
		$t->assign ( 'reqmain_branch_id', $reqmain_branch_id );
		
		if ($reqmain_branch_id!=""){
			$app_arr = array();
			$temp = $this->db_pdo->prepare("SELECT * from groupdetails WHERE  main_branch_id='$reqmain_branch_id' ");
			$temp->execute();		
			foreach( $temp as $index => $row ) {
				$app_arr['GroupID'][] = $row['GroupID'];
				$app_arr['GroupNo'][] = $row['GroupNo'];
			}
			$t->assign ( 'app_arr', $app_arr );
		}
		if ($_REQUEST['txtGroupNo']!=""){
			$tkt_arr = array();
			$temp = $this->db_pdo->prepare("SELECT * from applicantdetails WHERE  GroupID='".$_REQUEST['txtGroupNo']."' ORDER BY Ticketno ASC");
			$temp->execute();		
			foreach( $temp as $index => $row ) {
				$tkt_arr['Ticketno'][] = $row['Ticketno'];
			}
			$t->assign ( 'tkt_arr', $tkt_arr );
		}

		$sql= "	SELECT *  
		FROM ".$this->TableName. " 
		LEFT JOIN applicantdetails ON (".$this->TableName.".AppID=applicantdetails.AppID)"  ;
	
	
		if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort'];	else $reqsort='Ticketno';
		if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
		if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";	 
		//---------------------------------Paging--------------------------------------------------
		if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
		if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
		$t->assign('reqord',$reqord);
		//-------------------------------end  Paging------------------------------------------------------
		
		
		$tmpblnWhere = 0;
		
		if (strcmp($reqmain_branch_id,"")<>0){				
			if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
			$sql = $sql . " main_branch_id like '%$reqmain_branch_id%'";
			$PageString = $PageString  .'&txtmain_branch_id='.$reqmain_branch_id;
		}
			
		
		if ( isset( $_REQUEST['txtGroupNo'] ) ){
			$reqGroupNo = $_REQUEST['txtGroupNo'] ;
			$t->assign ( 'reqGroupNo', $reqGroupNo);
			if (strcmp($reqGroupNo,"")<>0){				
				if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
				$sql = $sql . " GroupID = '$reqGroupNo'";
				$PageString = $PageString  .'&txtGroupNo='.$reqGroupNo;
			}
		}	
		
		if ( isset( $_REQUEST['txtTicketno'] ) ){
			$reqTicketno = $_REQUEST['txtTicketno'] ;
			$t->assign ( 'reqTicketno', $reqTicketno);
			if (strcmp($reqTicketno,"")<>0){				
				if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
				$sql = $sql . " Ticketno = '$reqTicketno'";
				$PageString = $PageString  .'&txtTicketno='.$reqTicketno;
			}
		}	


				$strfnPagingSql = $sql; include 'includes/callpaging.php'; 
		//----------------------------------------------------------------------------------- 
				 
		$temp = $this->db_pdo->prepare($sql);
	
		$temp->execute();
		$nomineeResult=array();
		foreach( $temp as $index => $row ) {
			$nomineeResult[]=$row;
		}
		$t->assign('nominee',$nomineeResult);
		
		$t->display('admnominee_list.htm');	
	}
	
	function add_nominee_from_subscriber($SubID,$AppID){
		
		$Insert_nomineedetails_sql= "INSERT INTO `tbl_nominee` (`AppID`,`NomineeName`, `AddrsNominee`,`RelationNominee`,`NomineeAge`, `NomineeDob`, `TelNoNominee`) 
									SELECT 	'$AppID' as AppID,`NomineeName` ,`AddrsNominee` ,`RelationNominee` ,`NomineeAge` ,
									`NomineeDob`,`TelNoNominee`
									 FROM subdirdetails WHERE subdirdetails.SubID=$SubID ";
									// echo $Insert_nomineedetails_sql;
		$Insert_nomineedetails_sql_result = mysql_query($Insert_nomineedetails_sql) or die(mysql_error().$Insert_nomineedetails_sql);	
		$nomineedetails_ID = mysql_insert_id();
		
		
/*		$Insert_nomineedetails_sql= "INSERT INTO `tbl_nominee` (`AppID`,`NomineeName`, `AddrsNominee`,`RelationNominee`,`NomineeAge`, `NomineeDob`, `TelNoNominee`) 
									SELECT 	applicantdetails.`AppID` as AppID,`NomineeName` ,`AddrsNominee` ,`RelationNominee` ,`NomineeAge` ,
									`NomineeDob`,`TelNoNominee`
									 FROM (subdirdetails Inner Join applicantdetails ON subdirdetails.SubCode = applicantdetails.SubCodeNo) ";
									echo $Insert_nomineedetails_sql;
		$Insert_nomineedetails_sql_result = mysql_query($Insert_nomineedetails_sql) or die(mysql_error().$Insert_nomineedetails_sql);	
		$nomineedetails_ID = mysql_insert_id();
*/	
	}
}

?>