<?php
class Database {
	public $db_pdo;
	protected $DB_ENGINE;
	protected $DB_USER;
	protected $DB_HOST;
	protected $DB_NAME;
	protected $DB_PASS;
	protected $DB_TYPE; 
	protected $WEBSITE_PATH;
	protected $TMP_LOCAL;
	protected $SMS_API_TYPE;
	protected $COL_MACHINE;
	protected $COL_MACHINE_SHOW_PAID;
	protected $COL_MACHINE_ONLY_TKT_NO;
	protected $COL_MACHINE_SHOW_COLL_CODE;
	protected $COL_MACHINE_SHOW_TERMINATED_GRP;
	protected $COL_MACHINE_SEND_SMS;
	 
	protected function DBConnect(){
		// Mysql related Definations
		$this->DB_ENGINE = 'mysql'; 
		$tmplocal =1;
		if ($tmplocal == 1 ){
			$this->DB_USER = 'root';
			$this->DB_HOST = 'localhost';
			$this->DB_PORT = '3306';
			$this->DB_NAME = 'echit_legal';
			$this->DB_PASS = '';
			$this->DB_TYPE = 'mysql'; 
		}else{
			$this->DB_USER = 'acfplxzf_associ';
			$this->DB_NAME = 'acfplxzf_legal';	
			$this->DB_HOST = 'localhost';
			$this->DB_PASS = 'rs@2018';
			$this->DB_TYPE = 'mysql';
			$this->DB_PORT = '3306';
		}
		$this->WEBSITE_PATH = "http://localhost/chitfund/";
		$this->TMP_LOCAL=$tmplocal;
		$this->SMS_API_TYPE="proactive";
		$this->COL_MACHINE="balaji";
		$this->COL_MACHINE_SHOW_PAID="1";
		$this->COL_MACHINE_ONLY_TKT_NO="1";
		$this->COL_MACHINE_SHOW_COLL_CODE="1";
		$this->COL_MACHINE_SHOW_TERMINATED_GRP="1";
		$this->COL_MACHINE_SEND_SMS="NO";

		try {
			$this->db_pdo = new PDO($this->DB_ENGINE.':host=' . $this->DB_HOST . ';port='.$this->DB_PORT.';dbname=' . $this->DB_NAME . ';', $this->DB_USER, $this->DB_PASS);
			$this->db_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}catch(PDOException $e){
			echo $e->getMessage();
		}	
	}
	
	protected function InsertRecord($TableName,$TableFieldArray,$TableFieldValArray,$rec_no=""){
		//echo "HI";
		//die();
		$FieldString = "";
		$DataString = "";
		for($loopCnt = 0; $loopCnt < count($TableFieldArray); $loopCnt++){
			if($TableFieldArray[$loopCnt]['1'] == 1){
				if($FieldString != "") $FieldString .= ',';
				$FieldString .= '`'.$TableFieldArray[$loopCnt][0].'`';
				if($DataString != "") $DataString .= ',';
				$DataString .= ':Bind_'.$TableFieldArray[$loopCnt][0];
			}
		}
		$sql 	= 'INSERT INTO `'.$TableName.'` ('.$FieldString.') VALUES ('.$DataString.')';
		//echo $sql; 
		$stmt 	= $this->db_pdo->prepare($sql);
		
		for($loopCnt = 0; $loopCnt < count($TableFieldArray); $loopCnt++){
			if($TableFieldArray[$loopCnt]['1'] == 1){
				if (($TableFieldValArray[$loopCnt][0] != "")){
					//echo $loopCnt."-".$TableFieldValArray[$loopCnt][0]."<br>";
					$stmt->bindParam(':Bind_'.$TableFieldArray[$loopCnt][0],$TableFieldValArray[$loopCnt][0],$TableFieldArray[$loopCnt][3]); 
				}else{
					//echo $loopCnt."-post <br>";

					$stmt->bindParam(':Bind_'.$TableFieldArray[$loopCnt][0],$_POST['txt'.$TableFieldArray[$loopCnt][0].(($rec_no!="")?"_".$rec_no:"")],$TableFieldArray[$loopCnt][3]); 
				}
			}
		}
		
		$stmt->execute()or die(print_r($stmt->errorInfo(), true)); 
		
		return $this->db_pdo->lastInsertId();
	}
	protected function InsertRecordValOnly($TableName,$TableFieldArray,$TableFieldValArray){
		$FieldString = "";
		$DataString = "";
		for($loopCnt = 0; $loopCnt < count($TableFieldArray); $loopCnt++){
			if (($TableFieldValArray[$loopCnt][0] != "")){
				if($FieldString != "") $FieldString .= ',';
				$FieldString .= '`'.$TableFieldArray[$loopCnt][0].'`';
				if($DataString != "") $DataString .= ',';
				$DataString .= ':Bind_'.$TableFieldArray[$loopCnt][0];
			}
		}
		$sql 	= 'INSERT INTO `'.$TableName.'` ('.$FieldString.') VALUES ('.$DataString.')';

		$stmt 	= $this->db_pdo->prepare($sql);
//echo $sql;
		for($loopCnt = 0; $loopCnt < count($TableFieldArray); $loopCnt++){
			if (($TableFieldValArray[$loopCnt][0] != "")){
				$stmt->bindParam(':Bind_'.$TableFieldArray[$loopCnt][0],$TableFieldValArray[$loopCnt][0],$TableFieldArray[$loopCnt][3]); 
			}
		}
		$stmt->execute(); 
		return $this->db_pdo->lastInsertId();
	}

	protected function UpdateRecord($TableName,$TableFieldArray,$IDField,$TableFieldValArray,$rec_no="",$fieldprefix="",$fieldpostfix=""){
		$UpdateString = "";
		$DataString = "";
		
		for($loopCnt = 0; $loopCnt < count($TableFieldArray); $loopCnt++){
			if($TableFieldArray[$loopCnt]['1'] == 1){
				if($UpdateString != "") $UpdateString .= ',';
				$UpdateString .= '`'.$TableFieldArray[$loopCnt][0].'` =  :Bind_'.$TableFieldArray[$loopCnt][0];
			}
		}
		
		$WhereCondition = ' WHERE `'.$IDField.'` = '.$_POST['txt'.$IDField];
		
		$sql 	= 'UPDATE `'.$TableName.'` set '.$UpdateString.$WhereCondition;
		
		$stmt 	= $this->db_pdo->prepare($sql);
		//echo $sql;
		//die();
		for($loopCnt = 0; $loopCnt < count($TableFieldArray); $loopCnt++){
			if (($TableFieldArray[$loopCnt]['2']==0)){
				 $stmt->bindParam(':Bind_'.$TableFieldArray[$loopCnt][0], $TableFieldValArray[$loopCnt][0], $TableFieldArray[$loopCnt][3]); 
				 //echo $TableFieldArray[$loopCnt][0]."=".$TableFieldValArray[$loopCnt][0]."</BR>";
			}else{
				$stmt->bindParam(':Bind_'.$TableFieldArray[$loopCnt][0], $_POST['txt'.(($fieldprefix!="")? $fieldprefix:"").$TableFieldArray[$loopCnt][0].(($fieldpostfix!="")? $fieldpostfix:"").(($rec_no!="")?"_".$rec_no:"")], $TableFieldArray[$loopCnt][3]); 
				// echo $TableFieldArray[$loopCnt][0]."=".$_POST['txt'.(($fieldprefix!="")? $fieldprefix:"").$TableFieldArray[$loopCnt][0].(($fieldpostfix!="")? $fieldpostfix:"").(($rec_no!="")?"_".$rec_no:"")]."</BR>";
			}
		}
		//die();
		$stmt->execute(); 
		return $_POST['txt'.$IDField];
	}
	
	protected function UpdateRecordValOnly($TableName,$TableFieldArray,$TableFieldValArray,$IDField,$IDFieldVal){
		$UpdateString = "";
		$DataString = "";
		for($loopCnt = 0; $loopCnt < count($TableFieldArray); $loopCnt++){
			if (($TableFieldValArray[$loopCnt][0] != "")){
				if($UpdateString != "") $UpdateString .= ',';
				$UpdateString .= '`'.$TableFieldArray[$loopCnt][0].'` =  :Bind_'.$TableFieldArray[$loopCnt][0];
			}
		}
		
		$WhereCondition = ' WHERE `'.$IDField.'` = '.$IDFieldVal;
		
		$sql 	= 'UPDATE `'.$TableName.'` set '.$UpdateString.$WhereCondition;
		//echo $sql;
		//die();
		$stmt 	= $this->db_pdo->prepare($sql);
		for($loopCnt = 0; $loopCnt < count($TableFieldArray); $loopCnt++){
			if (($TableFieldValArray[$loopCnt][0] != "")){
				$stmt->bindParam(':Bind_'.$TableFieldArray[$loopCnt][0], $TableFieldValArray[$loopCnt][0], $TableFieldArray[$loopCnt][3]); 
			}
		}
		$stmt->execute(); 
		return $IDFieldVal;
	}
	
	protected function UpdateStatus($TableName,$IDField,$IDValue,$Status){
		$WhereCondition = ' WHERE `'.$IDField.'` = '.$IDValue;
		$sql 			= 'UPDATE `'.$TableName.'` set `Approve` = '.$Status.$WhereCondition;
		$stmt 			= $this->db_pdo->prepare($sql);
		$stmt->execute(); 
	}
	protected function DeleteRecord($TableName,$IDField,$IDValue){
		$sql 			= 'delete from `'.$TableName.'` WHERE `'.$IDField.'` = '.$IDValue;
		//echo $sql;
		$stmt 			= $this->db_pdo->prepare($sql);
		$stmt->execute(); 
	}
	public function getAll($sql){
		$temp = $this->db_pdo->prepare($sql);
		$temp->execute();
		$Result = $temp->fetchAll(PDO::FETCH_ASSOC);
		return $Result;
	}
	public function getOne($sql){
		$temp = $this->db_pdo->prepare($sql);
		$temp->execute();
		foreach( $temp as $index => $tmprow ) {
			$tmpValue = $tmprow[0];
		}
		return $tmpValue;
	}

	public function query($sql,$var_array){
		$temp = $this->db_pdo->prepare($sql);
		$temp->execute($var_array);
		return $temp;
	}			
	
		
	public function pdo_query($sql){
		$temp = $this->db_pdo->prepare($sql);
		$temp->execute();
		//$Result = $temp->fetchAll(PDO::FETCH_ASSOC);
		return $temp;
	}				
	public function pdo_fetch_array($result){
		return $result->fetch(PDO::FETCH_BOTH );
	}
	public function getAssoc($sql){
		$temp = $this->db_pdo->prepare($sql);
		$temp->execute();
		foreach( $temp as $index => $tmprow ) {
			$tmpValue[$tmprow[0]] = $tmprow[1];
		}
		return $tmpValue;
	}
	
	protected function FnGetValue($tmpTable,$tmpField,$tmpWhere){
		//$tmpTable table name
		//$tmpField field name
		//$tmpWhere where contion
		if ($tmpTable<> "")  $frompart="from $tmpTable";	else    $frompart="";
	
		if ($tmpWhere<> "")   $wherepart = "where $tmpWhere ";   else   $wherepart = "";
	
		$tmpsql = "select $tmpField $frompart $wherepart";
		  
		$tmpValue = 0;
		$tmpresult	= $this->db_pdo->prepare($tmpsql);
		
		$tmpresult->execute(); 
		
		foreach( $tmpresult as $index => $tmprow ) {
			$tmpValue = $tmprow[0];
		}
		
		return $tmpValue;
	}
	protected function FnGetCount($tmpTable,$tmpWhere){
		if ($tmpWhere<> "") $tmpsql = "select count(*) as num from $tmpTable where $tmpWhere ";
		else $tmpsql = "select count(*) as num from $tmpTable ";
		
		$tmpresult	= $this->db_pdo->prepare($tmpsql);
		$tmpresult->execute(); 
		foreach( $tmpresult as $index => $tmprow ) {
			$tmpValue = $tmprow[0];
		}
		
		return $tmpValue;
	}
	protected function FnGetValueMultiple($tmpTable,$tmpFields,$tmpWhere,$ReturnType='ArrayOfValues'){
		if ($tmpWhere<> "") $tmpsql = "select $tmpFields from $tmpTable where $tmpWhere ";
		else			   	$tmpsql = "select $tmpFields from $tmpTable ";
		//$fields=explode(',',$tmpFields);
		$fields	= explode('~',$tmpFields);
		
		$FieldCount=count($fields);
		$tmpValue = array();
		$tmpRetValue='';
		$tmpresult	= $this->db_pdo->prepare($tmpsql);
		$tmpresult->execute(); 
		foreach( $tmpresult as $index => $tmprow ) {
			for($i=0;$i<$FieldCount;$i++){ 
				$tmpValue[$i] = $tmprow[$fields[$i]];
			}
		}
		switch($ReturnType){
			case 1:
				
			case 'ArrayOfValues': return $tmpValue;  break;
			case 2:
			case 'ConcatValues':
				for($i=0;$i<$FieldCount;$i++){  $tmpRetValue.= (($tmpValue[$i]<>"")?$tmpValue[$i]." ":"");}
				return $tmpRetValue;	 
				break; 
		}    
	}
	//----------------------------for agreegate function---------------
	protected function FnGetValueMultipleNew($tmpTable,$tmpFields,$tmpWhere,$ReturnType='ArrayOfValues'){
		if ($tmpWhere<> "") $tmpsql = "select $tmpFields from $tmpTable where $tmpWhere ";
		else			   	$tmpsql = "select $tmpFields from $tmpTable ";
		$fields=explode(',',$tmpFields);
		 $FieldCount=count($fields);
		$tmpValue = array();
		$tmpRetValue='';
		$tmpresult	= $this->db_pdo->prepare($tmpsql);
		$tmpresult->execute(); 
		foreach( $tmpresult as $index => $tmprow ) {for($i=0;$i<$FieldCount;$i++){ $tmpValue[$i] = $tmprow[$i];}}
		switch($ReturnType){
			case 1:
				
			case 'ArrayOfValues': return $tmpValue;  break;
			case 2:
			case 'ConcatValues':
				for($i=0;$i<$FieldCount;$i++){  $tmpRetValue.= (($tmpValue[$i]<>"")?$tmpValue[$i]." ":"");}
				return $tmpRetValue;	 
				break; 
		}    
	}
	protected function FnGetDateValue($tmpField){
		$tmpsql = "select $tmpField ";
		$tmpValue = 0;
		$tmpresult	= $this->db_pdo->prepare($tmpsql);
		$tmpresult->execute(); 
		foreach( $tmpresult as $index => $tmprow ) {
			$tmpValue = $tmprow[$tmpField];
		}	   
		return $tmpValue;
	}	
	//function is used to change date format (dd/mm/yy to yy-mm-dd)
	protected function SQLDateIn($input_date){
		//$input_date input date(dd/mm/yy)
		if (strstr($input_date,'-')<>0) return $input_date;
		if (($input_date <> "") && ($input_date<>"NULL")){	
			 $input_format = 'd/m/y';
			 $output_format = 'Y-m-d';
			 preg_match("/^([\w]*)/i", $input_date, $regs);
			 $sep = substr($input_date, strlen($regs[0]), 1);
			 $label = explode($sep, $input_format);
			 $value = explode($sep, $input_date);
			 $array_date = array_combine($label, $value);
			 if (in_array('Y', $label)) {
				   $year = $array_date['Y'];
			   } elseif (in_array('y', $label)) {
				   $year = $year = $array_date['y'];
			   } else {
			   return false;
			  }
		 $output_date = date($output_format, mktime(0,0,0,$array_date['m'], $array_date['d'], $year));
		 return $output_date;
		}else{
			return "NULL";
		}
	}
	//function is used to change date format (yy-mm-dd to dd/mm/yy)
	protected function SQLDateOut($tmpDate){
		if ($tmpDate<> "" ){
			if($tmpDate == '0000-00-00'){
				$tmpoutput = "";
				return $tmpoutput;
			}else{
				$tmptime   = strtotime($tmpDate);
				$tmpoutput = date("d/m/Y", $tmptime);
				//echo $tmpoutput."<br>";
				return $tmpoutput;
			}
		}else{
			return;
		}
	}	
	// Number to word Function - start
	protected function One($num){
		switch ($num){
			Case 1:
			  return "ONE"; break;
			Case 2:
			  return "TWO"; break;
			Case 3:
			  return "THREE"; break;
			Case 4:
			  return "FOUR"; break;
			Case 5:
			  return "FIVE"; break;
			Case 6:
			  return "SIX"; break;
			Case 7:
			  return "SEVEN"; break;
			Case 8:
			  return "EIGHT"; break;
			Case 9:
			  return "NINE"; break;
			Case 0:
			  return "";
			  
		}
	}
	
	protected function Two($num){ 
		switch (substr($num,0, 1)){	
			Case 1:
				switch ($num) {	
					Case 10:
					  $num1 = "TEN"; break;
					Case 11:
					  $num1 = "ELEVEN"; break;
					Case 12:
					  $num1 = "TWELVE"; break;
					Case 13:
					  $num1 = "THIRTEEN"; break;
					Case 14:
					  $num1 = "FOURTEEN"; break;
					Case 15:
					  $num1 = "FIFTEEN"; break;
					Case 16:
					  $num1 = "SIXTEEN"; break;
					Case 17:
					  $num1 = "SEVENTEEN"; break;
					Case 18:
					  $num1 = "EIGHTEEN"; break;
					Case 19:
					  $num1 = "NINETEEN"; break;
				}
				break;
			Case 2:
			  $num1 = "TWENTY " . One(substr($num,1)); break;
			Case 3:
			  $num1 = "THIRTY " . One(substr($num,1)); break;
			Case 4:
			  $num1 = "FORTY " . One(substr($num,1)); break;
			Case 5:
			  $num1 = "FIFTY " . One(substr($num,1)); break;
			Case 6:
			  $num1 = "SIXTY " . One(substr($num,1)); break;
			Case 7:
			  $num1 = "SEVENTY " . One(substr($num,1)); break;
			Case 8:
			  $num1 = "EIGHTY " . One(substr($num,1)); break; 
			Case 9:
			  $num1 = "NINETY " . One(substr($num,1)); break;
			Case 0:
			  $num1 = One(substr($num,1)); break;
		}
		return $num1;
	}
	
	protected function NumtoWord($txtnum){
		switch (strlen(trim($txtnum))){
			Case 1:
				return One($txtnum); break;
			Case 2:
				return Two($txtnum); break;
			Case 3:
				return ((substr($txtnum, 0,  1) == 0)? NumtoWord(substr($txtnum, 1)) : One(substr($txtnum, 0, 1)) . " HUNDRED " . NumtoWord(substr($txtnum, 1)));
				break;
			Case 4:
				return ((substr($txtnum, 0,  1) == 0)? NumtoWord(substr($txtnum, 1)) : One(substr($txtnum, 0, 1)) . " THOUSAND " . NumtoWord(substr($txtnum, 1)));			
				break;
			Case 5:
				return ((substr($txtnum, 0,  2) == 0)? NumtoWord(substr($txtnum, 2)) : Two(substr($txtnum, 0, 2)) . " THOUSAND " . NumtoWord(substr($txtnum, 2)));						
				break;
			Case 6:
				return ((substr($txtnum, 0,  1) == 0)? NumtoWord(substr($txtnum, 1)) : One(substr($txtnum, 0,  1)) . " LAKH " . NumtoWord(substr($txtnum, 1)));
				break;
			Case 7:
				return ((substr($txtnum, 0,  2) == 0)? NumtoWord(substr($txtnum, 2)) : Two(substr($txtnum, 0,  2)) . " LAKH " . NumtoWord(substr($txtnum, 2)));
				break;
			Case 8:
				return ((substr($txtnum, 0,  1) == 0)? NumtoWord(substr($txtnum, 1)) : One(substr($txtnum, 0,  1)) . " CRORE " . NumtoWord(substr($txtnum, 1)));
				break;
			Case 9:
				return ((substr($txtnum, 0,  2) == 0)? NumtoWord(substr($txtnum, 2)) : Two(substr($txtnum, 0,  2)) . " CRORE " . NumtoWord(substr($txtnum, 2)));
				break;
			Case 0:
				return "ZERO"; break;
		}
	}
	//=========day/month/year===========
	protected function fnReturnMonth($month,$exitdate,$Sub_Per){
	   $month_prf="";
	   switch ($Sub_Per){   
		 case 'daily':
		   $tmpsql = "select concat(DAY(DATE_ADD('$exitdate', Interval $month Day)),left(MONTHNAME(DATE_ADD('$exitdate', Interval $month Day)),3),year(DATE_ADD('$exitdate', Interval $month DAY)))";
		   break;	 
		 case 'weekly':
		   $tmpsql = "select concat(DAY(DATE_ADD('$exitdate', Interval $month WEEK)),left(MONTHNAME(DATE_ADD('$exitdate', Interval $month WEEK)),3),year(DATE_ADD('$exitdate', Interval $month WEEK)))";	 
		   break;	 
		 case 'monthly':
		   $tmpsql = "select concat(left(MONTHNAME(DATE_ADD('$exitdate', Interval $month month)),3),year(DATE_ADD('$exitdate', Interval $month month)))";
		   break;
		 case 'half_monthly':
		   $month_prf = $this->FnGetValue("groupdetails","concat(DAY(DATE_ADD('$exitdate', Interval ".($month* 15)." Day)),left(MONTHNAME(DATE_ADD('$exitdate', Interval ".($month* 15)." Day)),3),year(DATE_ADD('$exitdate', Interval ".($month* 15)." DAY)))","");
		   $month_prf .="-";	   
		   $month =$month+1;
		   $month = ($month)* 15;
		   $month = $month-1;
		   $tmpsql = "select concat(DAY(DATE_ADD('$exitdate', Interval $month Day)),left(MONTHNAME(DATE_ADD('$exitdate', Interval $month Day)),3),year(DATE_ADD('$exitdate', Interval $month DAY)))";
		   break;
		 case 'two_daily':
		   $month_prf = $this->FnGetValue("groupdetails","concat(DAY(DATE_ADD('$exitdate', Interval ".($month* 2)." Day)),left(MONTHNAME(DATE_ADD('$exitdate', Interval ".($month* 2)." Day)),3),year(DATE_ADD('$exitdate', Interval ".($month* 2)." DAY)))","");
		   $month_prf .="-";	   
		   $month =$month+1;
		   $month = ($month)* 2;
		   $month = $month-1;
		   $tmpsql = "select concat(DAY(DATE_ADD('$exitdate', Interval $month Day)),left(MONTHNAME(DATE_ADD('$exitdate', Interval $month Day)),3),year(DATE_ADD('$exitdate', Interval $month DAY)))";
		   break;
	   }
	   	
	
		$tmpresult	= $this->db_pdo->prepare($tmpsql);
		$tmpresult->execute(); 
		
		foreach( $tmpresult as $index => $tmprow ) {
			$tmpValue = $month_prf.$tmprow['0'];
	    }
	   return $tmpValue;
	}
	//=========day/month/year===========
	protected function fnDayInChar($day){
		if($day == '01')
			$reqtxtGrpDate = '1st';
		elseif($day == '02')
			$reqtxtGrpDate = '2nd';	
		elseif($day == '03')
			$reqtxtGrpDate = '3rd';
		else
			$reqtxtGrpDate = $day.'th';
		
		return 	$reqtxtGrpDate;
	}
	//=================================
	// Number to word Function - End
	protected function FnCalTerDate($StartDate,$Duration,$Subper){
		if ($StartDate<>""){
		  if($Subper == 'daily') {
			  $end_date = $this->SqlDateOut($this->FnGetDateValue("DATE_ADD('$StartDate', INTERVAL $Duration DAY)"));
		  }elseif($Subper == 'weekly'){
			  //$duration = ($duration * 7);
			  $end_date = $this->SqlDateOut($this->FnGetDateValue("DATE_ADD('$StartDate', INTERVAL $Duration WEEK)"));
		  }elseif($Subper == 'monthly'){
			  $end_date = $this->SqlDateOut($this->FnGetDateValue("DATE_ADD('$StartDate', INTERVAL $Duration month)"));
		  }
		  elseif($Subper == 'half_monthly'){
			  $end_date = $this->SqlDateOut($this->FnGetDateValue("DATE_ADD('$StartDate', INTERVAL ".($Duration*15)." DAY)"));
		  }	
		  elseif($Subper == 'two_daily'){
			  $end_date = $this->SqlDateOut($this->FnGetDateValue("DATE_ADD('$StartDate', INTERVAL ".($Duration*2)." DAY)"));
		  }	
		
		  return $end_date;
		}else{	
		   return;
		} 	 
	}
	
	protected function fnUpdate_log($create_update,$table_name,$id){
		$reqpersonnel_id = $_SESSION['personnel_id'];
		$flag 			 = 0;
		if(($create_update == "Update")||($create_update == "update"))	$flag = 1;
		$sql 	= "INSERT INTO `tbl_update_log` (`user_id`,`table_name`,`id_feild`,`flag`,`flag_date_time`) VALUES ('$reqpersonnel_id','$table_name','$id','$flag',NOW())";
		$tmpresult	= $this->db_pdo->prepare($sql);
		$tmpresult->execute(); 
	}
	

}
?>