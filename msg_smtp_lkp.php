<? if ( !defined( 'SMARTY_DIR' ) ) { include_once( 'init.php' );}
$t->assign ('unid',md5(uniqid(rand(), true)));
header('Content-Type: text/html; charset=utf-8');


//============================actual data assignment=======================
$pg_file = array();
$pg_file['list_page'] = "msg_smtp_lkp.php";


$TABLE_NAME = "tbl_msg_smtp_lkp";
$MAIN_CAT_ID = "news_letter_smtp_lkp_id";
$lang_pri = "english";

$db_fields = array ('ID' => array('name' => 'news_letter_smtp_lkp_id', 'type' => 'text','dis_name' => 'ID'),
				   'FIELD_1' => array('name' => 'host_name', 'type' => 'text','dis_name' => 'Host Name'),
				   'FIELD_2' => array('name' => 'username', 'type' => 'text','dis_name' => 'Useranme'),
				   'FIELD_3' => array('name' => 'password', 'type' => 'text','dis_name' => 'Password'),
				   'FIELD_4' => array('name' => 'port_no', 'type' => 'text','dis_name' => 'Port_No'),
				   'FIELD_5' => array('name' => 'default_flag', 'type' => 'text','dis_name' => 'Set Default'),
				   'FIELD_6' => array('name' => 'note', 'type' => 'text','dis_name' => 'note')
              );
$t->assign('db_fields', $db_fields);

fnAddUnavialableMsg($db_fields);
$ID_DIS_NAME = ($db_fields["ID"]["dis_name"]);
$FIELD_1_DIS_NAME = ($db_fields["FIELD_1"]["dis_name"]);
$FIELD_2_DIS_NAME = ($db_fields["FIELD_2"]["dis_name"]);
$FIELD_3_DIS_NAME = ($db_fields["FIELD_3"]["dis_name"]);
$FIELD_4_DIS_NAME = ($db_fields["FIELD_4"]["dis_name"]);
$FIELD_5_DIS_NAME = ($db_fields["FIELD_5"]["dis_name"]);
$FIELD_6_DIS_NAME = ($db_fields["FIELD_6"]["dis_name"]);


$sort_fields = array($db_fields["ID"]["name"] => ("ID"), 
					 $db_fields["FIELD_1"]["name"]  => ($db_fields["FIELD_1"]["dis_name"]),
					 $db_fields["FIELD_2"]["name"]  => ($db_fields["FIELD_2"]["dis_name"]),
					 $db_fields["FIELD_3"]["name"]  => ($db_fields["FIELD_3"]["dis_name"]),
					 $db_fields["FIELD_4"]["name"] =>  ($db_fields["FIELD_4"]["dis_name"]));
$t->assign('sort_fields', $sort_fields);
								
								

$ID = $db_fields["ID"]["name"];
$FIELD_1 = $db_fields["FIELD_1"]["name"];
$FIELD_2 = $db_fields["FIELD_2"]["name"]; 
$FIELD_3 = $db_fields["FIELD_3"]["name"]; 
$FIELD_4 = $db_fields["FIELD_4"]["name"]; 
$FIELD_5 = $db_fields["FIELD_5"]["name"]; 
$FIELD_6 = $db_fields["FIELD_6"]["name"]; 
$APPROVE_FIELD = "approve";

$SEARCH_NAME_1 = $FIELD_1;
$SEARCH_NAME_2 = $FIELD_1;

$APPROVE_FLAG = 1 ; //  1=YES 0=NO if u need delete option
$APPROVE_FIELD_VAL_DEF = 1;

$DELETE_FLAG = 1 ; //  1=YES 0=NO if u need delete option
$DELETE_FLAG_Name = "delete";

$t->assign ( 'pg_file', $pg_file );
$t->assign ( 'MAIN_CAT_ID',$MAIN_CAT_ID);
$t->assign ( 'DELETE_FLAG',$DELETE_FLAG);
$t->assign ( 'DELETE_FLAG_Name',$DELETE_FLAG_Name);
$t->assign ( 'APPROVE_FLAG',$APPROVE_FLAG);
$t->assign('ID', $ID);

$t->assign('ID_DIS_NAME', $ID_DIS_NAME);
$t->assign('FIELD_1_DIS_NAME', $FIELD_1_DIS_NAME);
$t->assign('FIELD_2_DIS_NAME', $FIELD_2_DIS_NAME);
$t->assign('FIELD_3_DIS_NAME', $FIELD_3_DIS_NAME);
$t->assign('FIELD_4_DIS_NAME', $FIELD_4_DIS_NAME);
$t->assign('FIELD_5_DIS_NAME', $FIELD_5_DIS_NAME);
$t->assign('FIELD_6_DIS_NAME', $FIELD_6_DIS_NAME);


//==============================nd actual data assignment=============================


//=======================option to delete and approve/unapprove record=======================
$op = $_GET['op'];
$t-> assign('op',$op);		
if($op<>''){
	if($op=='3'){//deleting multilanguage message
	
		$ID_VAL = $_GET[ $ID ];
		
		if (isset($_GET[ 'act' ])){
			$action = $_GET[ 'act' ]; 
			
			if ($action == '2'){
				$sqlins = "update $TABLE_NAME set default_flag = '0'";
				$result = mysql_query($sqlins) or die(mysql_error());
				$sqlins = "update $TABLE_NAME set default_flag = '1' WHERE $ID = $ID_VAL";
			}
			else {
				$sqlins = "update $TABLE_NAME set approve = '$action' WHERE $ID = $ID_VAL";
				}
		 }else{
			$sqlins = "delete from $TABLE_NAME where $ID = $ID_VAL";
			}
		
		$result = mysql_query($sqlins) or die(mysql_error());
		$t->assign ( 'result',3);
	}
	
}//=======================end of shwoing new message form and end of deleting multilanguage message


$reqSearchKeyword  = $_REQUEST['txtsearch'] ;
$reqfind   = $_REQUEST['find'] ;
$reqlastup = $_REQUEST['lastup'] ;
$reqsort_field = $_REQUEST['txtsort_field'] ;
$reqord = $_REQUEST['ord'] ;
if ($reqord=='') $reqord="asc";


$t-> assign('reqSearchKeyword',$reqSearchKeyword);
$t-> assign('reqfind',$reqfind);
$t-> assign('reqlastup',$reqlastup);
$t-> assign('reqsort_field',$reqsort_field);
$t-> assign('reqord',$reqord);



// =======================  Add / Edit  category and subcategory  =======================
if ( isset( $_POST['FIELD_1_PRI']) ){//add new main/sub category

	$lang = trim($_POST['txt_lang_pri_add']);
	$ID_VAL  = trim($_POST[ 'ID' ]);
	$MAIN_CAT_ID_VAL = 0;
	$FIELD_1_PRI_VAL = trim($_POST[ 'FIELD_1_PRI' ]);
	$FIELD_2_PRI_VAL = trim($_POST[ 'FIELD_2_PRI' ]);
	$FIELD_3_PRI_VAL = trim($_POST[ 'FIELD_3_PRI' ]);
	$FIELD_4_PRI_VAL = trim($_POST[ 'FIELD_4_PRI' ]);
	$FIELD_5_PRI_VAL = 0;
	$FIELD_6_PRI_VAL = trim($_POST[ 'FIELD_6_PRI' ]);
	
	$sqlins = "Insert into $TABLE_NAME (
			$ID,$FIELD_1,$FIELD_2,$FIELD_3,$FIELD_4,$FIELD_5,$FIELD_6,$APPROVE_FIELD) 
			values(NULL,'$FIELD_1_PRI_VAL','$FIELD_2_PRI_VAL','$FIELD_3_PRI_VAL','$FIELD_4_PRI_VAL','$FIELD_5_PRI_VAL','$FIELD_6_PRI_VAL','1')";
	$result = mysql_query($sqlins) or die(mysql_error().$sqlins);
}
//==========================end of ading main and sub category=======================




//=================== Edit Existing data & Translating the message for both language =========================	
if (isset($_POST["MAIN_CAT_ID_ARR"])){

  $MAIN_CAT_ID_ARR = $_POST["MAIN_CAT_ID_ARR"]; 
  
  $FIELD_1_PRI_ARR = $_POST["FIELD_1_PRI_ARR"];
  $FIELD_2_PRI_ARR = $_POST["FIELD_2_PRI_ARR"];
  $FIELD_3_PRI_ARR = $_POST["FIELD_3_PRI_ARR"];
  $FIELD_4_PRI_ARR = $_POST["FIELD_4_PRI_ARR"];
  $FIELD_6_PRI_ARR = $_POST["FIELD_6_PRI_ARR"];

  
  
  $TMP_CNT=count($FIELD_1_PRI_ARR);
  for ($i=0;$i<$TMP_CNT;$i++){
  	
		$ID_VAL			 = $MAIN_CAT_ID_ARR[$i];
		$FIELD_1_PRI_VAL = trim($FIELD_1_PRI_ARR[$i]);
		$FIELD_2_PRI_VAL = trim($FIELD_2_PRI_ARR[$i]);
		$FIELD_3_PRI_VAL = trim($FIELD_3_PRI_ARR[$i]);
		$FIELD_4_PRI_VAL = trim($FIELD_4_PRI_ARR[$i]);
		$FIELD_6_PRI_VAL = trim($FIELD_6_PRI_ARR[$i]);

		$upmsg ="UPDATE $TABLE_NAME SET
				$FIELD_1  = '$FIELD_1_PRI_VAL',
				$FIELD_2  = '$FIELD_2_PRI_VAL',
				$FIELD_3  = '$FIELD_3_PRI_VAL',
				$FIELD_4  = '$FIELD_4_PRI_VAL',
				$FIELD_6  = '$FIELD_6_PRI_VAL'
				where $ID = '$ID_VAL'";
				
		//echo $upmsg;
		$upres = mysql_query($upmsg) or die(mysql_error().$upmsg);
		
  }// end of for loop
			
  $t->assign ( 'result', 1);

} // end of submit
//================End for updating the message for both language =========================





//================showing Dispalying records ===============================================

$sql1 = "select * from ". $TABLE_NAME;
$tmpblnWhere=0;
if($reqSearchKeyword<>''){
	if ($tmpblnWhere==0){ $sql1 = $sql1 . " where "; $tmpblnWhere=1; } else	$sql1 = $sql1 . " and ";
	$sql1 = $sql1 .  $SEARCH_NAME_1 . " like '%$reqSearchKeyword%' ";
	$strQueryString = $strQueryString . "&txtsearch=$reqSearchKeyword";
}

	
if($reqsort_field<>''){
	$sql1 = $sql1 . " order by $reqsort_field $reqord" ;
	$strQueryString = $strQueryString . "&txtsort_field=".$reqsort_field."&ord=".$reqord;
	
}	

$dbRecord = array();
$dbRecordList = $db->getAll( $sql1 );
foreach($dbRecordList as $index=>$row){

	$MAIN_CAT_ID_VAL = $row[$MAIN_CAT_ID];
	$row['ID'] = $row[$db_fields["ID"]["name"]]; 
	$row['FIELD_1_PRI'] = $row[$db_fields["FIELD_1"]["name"]];  
	$row['FIELD_2_PRI'] = $row[$db_fields["FIELD_2"]["name"]];  
	$row['FIELD_3_PRI'] = $row[$db_fields["FIELD_3"]["name"]];  
	$row['FIELD_4_PRI'] = $row[$db_fields["FIELD_4"]["name"]];  
	$row['FIELD_5_PRI'] = $row[$db_fields["FIELD_5"]["name"]];  
	$row['FIELD_6_PRI'] = $row[$db_fields["FIELD_6"]["name"]];  
	$dbRecord[] = $row;
}
$t-> assign('dbRecord',$dbRecord);
$t-> assign('lang_pri',$lang_pri);
$t-> assign('strQueryString',$strQueryString);

$t->display('msg_smtp_lkp.htm');

//================End of showing record list ==============================================

?>