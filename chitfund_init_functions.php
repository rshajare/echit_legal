<?


$UsrBranchIdCnt = 0;
$UsrBranchIdlist="(";
$arrUsrbranch_code = array();
$branchtemp = $db->getAll( "SELECT tbl_branch.main_branch_id,branch_name FROM `tbl_sur_sign_up_branches`,tbl_branch WHERE tbl_sur_sign_up_branches.`main_branch_id` = tbl_branch.main_branch_id AND lang = '".$_SESSION['opt_lang']."' AND `personnel_id` ='".$_SESSION["personnel_id"]."'" );
foreach( $branchtemp as $index => $branchrow ) {
	$arrUsrbranch_code['main_branch_id'][] = $branchrow['main_branch_id'];
	$arrUsrbranch_code['branch_name'][] = $branchrow['branch_name'];	
	if ($UsrBranchIdCnt<>0){$UsrBranchIdlist.=","; }
	$UsrBranchIdlist.=$branchrow['main_branch_id'];	
	$UsrBranchIdCnt+=1;
}
$UsrBranchIdlist.=")";
if ($UsrBranchIdCnt==0){
	$UsrBranchIdlist="(0)";
}

$t->assign ( 'UsrBranchIdlist', $UsrBranchIdlist );
$t->assign ( 'arrUsrbranch_code', $arrUsrbranch_code );
$t->assign ( 'company_branch_list', $arrUsrbranch_code );
$ses_branch_code = isset($_SESSION['branch_code'])?$_SESSION['branch_code']:'';
$t->assign ('ses_branch_code', $ses_branch_code);


// Number to word Function - start
function One($num){
	switch ($num){
		Case 1:
		  return "ONE"; break;
		Case 2:
		  return "TWO"; break;
		Case 3:
		  return "THREE"; break;
		Case 4:
		  return "FOUR"; break;
		Case 5:
		  return "FIVE"; break;
		Case 6:
		  return "SIX"; break;
		Case 7:
		  return "SEVEN"; break;
		Case 8:
		  return "EIGHT"; break;
		Case 9:
		  return "NINE"; break;
		Case 0:
		  return "";
		  
	}
}

function Two($num){ 
	switch (substr($num,0, 1)){	
		Case 1:
			switch ($num) {	
				Case 10:
				  $num1 = "TEN"; break;
				Case 11:
				  $num1 = "ELEVEN"; break;
				Case 12:
				  $num1 = "TWELVE"; break;
				Case 13:
				  $num1 = "THIRTEEN"; break;
				Case 14:
				  $num1 = "FOURTEEN"; break;
				Case 15:
				  $num1 = "FIFTEEN"; break;
				Case 16:
				  $num1 = "SIXTEEN"; break;
				Case 17:
				  $num1 = "SEVENTEEN"; break;
				Case 18:
				  $num1 = "EIGHTEEN"; break;
				Case 19:
				  $num1 = "NINETEEN"; break;
			}
			break;
		Case 2:
		  $num1 = "TWENTY " . One(substr($num,1)); break;
		Case 3:
		  $num1 = "THIRTY " . One(substr($num,1)); break;
		Case 4:
		  $num1 = "FORTY " . One(substr($num,1)); break;
		Case 5:
		  $num1 = "FIFTY " . One(substr($num,1)); break;
		Case 6:
		  $num1 = "SIXTY " . One(substr($num,1)); break;
		Case 7:
		  $num1 = "SEVENTY " . One(substr($num,1)); break;
		Case 8:
		  $num1 = "EIGHTY " . One(substr($num,1)); break; 
		Case 9:
		  $num1 = "NINETY " . One(substr($num,1)); break;
		Case 0:
		  $num1 = One(substr($num,1)); break;
	}
	return $num1;
}

function NumtoWord($txtnum){
	switch (strlen(trim($txtnum))){
		Case 1:
			return One($txtnum); break;
		Case 2:
			return Two($txtnum); break;
		Case 3:
			return ((substr($txtnum, 0,  1) == 0)? NumtoWord(substr($txtnum, 1)) : One(substr($txtnum, 0, 1)) . " HUNDRED " . NumtoWord(substr($txtnum, 1)));
			break;
		Case 4:
			return ((substr($txtnum, 0,  1) == 0)? NumtoWord(substr($txtnum, 1)) : One(substr($txtnum, 0, 1)) . " THOUSAND " . NumtoWord(substr($txtnum, 1)));			
			break;
		Case 5:
			return ((substr($txtnum, 0,  2) == 0)? NumtoWord(substr($txtnum, 2)) : Two(substr($txtnum, 0, 2)) . " THOUSAND " . NumtoWord(substr($txtnum, 2)));						
			break;
		Case 6:
			return ((substr($txtnum, 0,  1) == 0)? NumtoWord(substr($txtnum, 1)) : One(substr($txtnum, 0,  1)) . " LAKH " . NumtoWord(substr($txtnum, 1)));
			break;
		Case 7:
			return ((substr($txtnum, 0,  2) == 0)? NumtoWord(substr($txtnum, 2)) : Two(substr($txtnum, 0,  2)) . " LAKH " . NumtoWord(substr($txtnum, 2)));
			break;
		Case 8:
			return ((substr($txtnum, 0,  1) == 0)? NumtoWord(substr($txtnum, 1)) : One(substr($txtnum, 0,  1)) . " CRORE " . NumtoWord(substr($txtnum, 1)));
			break;
		Case 9:
			return ((substr($txtnum, 0,  2) == 0)? NumtoWord(substr($txtnum, 2)) : Two(substr($txtnum, 0,  2)) . " CRORE " . NumtoWord(substr($txtnum, 2)));
			break;
		Case 0:
			return "ZERO"; break;
	}
}
//=========day/month/year===========
function fnReturnMonth($month,$exitdate,$Sub_Per){
   $month_prf="";
   switch ($Sub_Per){   
     case 'daily':
	   $tmpsql = "select concat(DAY(DATE_ADD('$exitdate', Interval $month Day)),left(MONTHNAME(DATE_ADD('$exitdate', Interval $month Day)),3),year(DATE_ADD('$exitdate', Interval $month DAY)))";
	   break;	 
	 case 'weekly':
	   $tmpsql = "select concat(DAY(DATE_ADD('$exitdate', Interval $month WEEK)),left(MONTHNAME(DATE_ADD('$exitdate', Interval $month WEEK)),3),year(DATE_ADD('$exitdate', Interval $month WEEK)))";	 
	   break;	 
	 case 'monthly':
	   $tmpsql = "select concat(left(MONTHNAME(DATE_ADD('$exitdate', Interval $month month)),3),year(DATE_ADD('$exitdate', Interval $month month)))";
	   break;
	 case 'half_monthly':
	   $month_prf = fngetvalue("groupdetails","concat(DAY(DATE_ADD('$exitdate', Interval ".($month* 15)." Day)),left(MONTHNAME(DATE_ADD('$exitdate', Interval ".($month* 15)." Day)),3),year(DATE_ADD('$exitdate', Interval ".($month* 15)." DAY)))","");
	   $month_prf .="-";	   
	   $month =$month+1;
	   $month = ($month)* 15;
	   $month = $month-1;
	   $tmpsql = "select concat(DAY(DATE_ADD('$exitdate', Interval $month Day)),left(MONTHNAME(DATE_ADD('$exitdate', Interval $month Day)),3),year(DATE_ADD('$exitdate', Interval $month DAY)))";
 	   break;
	 case 'two_daily':
	   $month_prf = fngetvalue("groupdetails","concat(DAY(DATE_ADD('$exitdate', Interval ".($month* 2)." Day)),left(MONTHNAME(DATE_ADD('$exitdate', Interval ".($month* 2)." Day)),3),year(DATE_ADD('$exitdate', Interval ".($month* 2)." DAY)))","");
	   $month_prf .="-";	   
	   $month =$month+1;
	   $month = ($month)* 2;
	   $month = $month-1;
	   $tmpsql = "select concat(DAY(DATE_ADD('$exitdate', Interval $month Day)),left(MONTHNAME(DATE_ADD('$exitdate', Interval $month Day)),3),year(DATE_ADD('$exitdate', Interval $month DAY)))";
	   break;
   }
   $tmpresult = mysql_query($tmpsql)	or die(mysql_error()."<br>".$tmpsql) ;
   while ($tmprow = mysql_fetch_array($tmpresult)){
		$tmpValue = $month_prf.$tmprow['0'];
   }
   return $tmpValue;
}
//=========day/month/year===========
function fnDayInChar($day){
	if($day == '01')
		$reqtxtGrpDate = '1st';
	elseif($day == '02')
		$reqtxtGrpDate = '2nd';	
	elseif($day == '03')
		$reqtxtGrpDate = '3rd';
	else
		$reqtxtGrpDate = $day.'th';
	
	return 	$reqtxtGrpDate;
}
//=================================
// Number to word Function - End
function FnCalTerDate($StartDate,$Duration,$Subper){
 if ($StartDate<>""){
	if($Subper == 'daily') {
		$end_date = SqlDateOut(FnGetDateValue("DATE_ADD('$StartDate', INTERVAL $Duration DAY)"));
	}elseif($Subper == 'weekly'){
		//$duration = ($duration * 7);
		$end_date = SqlDateOut(FnGetDateValue("DATE_ADD('$StartDate', INTERVAL $Duration WEEK)"));
	}elseif($Subper == 'monthly'){
		$end_date = SqlDateOut(FnGetDateValue("DATE_ADD('$StartDate', INTERVAL $Duration month)"));
	}
	elseif($Subper == 'half_monthly'){
		$end_date = SqlDateOut(FnGetDateValue("DATE_ADD('$StartDate', INTERVAL ".($Duration*15)." DAY)"));
	}	
	elseif($Subper == 'two_daily'){
		$end_date = SqlDateOut(FnGetDateValue("DATE_ADD('$StartDate', INTERVAL ".($Duration*2)." DAY)"));
	}	

	return $end_date;
 }else{	
	 return;
 } 	 
}


?>