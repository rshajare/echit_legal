<?php include( 'sur_adm_permission.php' );
if ( !defined( 'SMARTY_DIR' ) ){include_once( 'init.php' );}

        $category_arr = array();
        $sql = "SELECT category_name,category_id FROM tbl_lgl_customer_report_category order by `category_id` asc";
        $temp = $db->getAll( $sql );
        foreach ($temp as $index => $row) {
            $category_arr['category_id'][] = $row['category_id'];
            $category_arr['category_name'][] = $row['category_name'];
        }
        $t->assign('category_arr', $category_arr);

		$comp_arr = array();
		$sql = "SELECT company_name,company_id FROM tbl_lgl_company order by `company_id` asc";
        $temp = $db->getAll( $sql );
		foreach ($temp as $index => $row) {
			$comp_arr['company_id'][] = $row['company_id'];
			$comp_arr['company_name'][] = $row['company_name'];
		}
		$t->assign('comp_arr', $comp_arr);

		
		//echo $reqkeyword10;
		
		$sql= "	SELECT * from tbl_lgl_report_draft 
				LEFT JOIN tbl_lgl_customer_report on tbl_lgl_report_draft.report_draft_id = tbl_lgl_customer_report.report_draft_id
				LEFT JOIN tbl_lgl_customer on tbl_lgl_customer_report.customer_id = tbl_lgl_customer.customer_id
				LEFT JOIN tbl_lgl_company on tbl_lgl_customer.company_id = tbl_lgl_company.company_id
				LEFT JOIN tbl_lgl_branch on tbl_lgl_customer.main_branch_id = tbl_lgl_branch.branch_id";

				
		//$tmpblnWhere = 0;
        if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort']; else $reqsort = 'report_name';
				if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
				if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";	 
				//---------------------------------Paging--------------------------------------------------
				if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
				if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
				$t->assign('reqord',$reqord);
				$t->assign('perpage',$perpage);
				
				//-------------------------------end  Paging------------------------------------------------------

		$reqkeyword6 = $_REQUEST['txtkeyword6'] ;
		$reqkeyword7 = $_REQUEST['txtkeyword7'] ;
		if ((strcmp($reqkeyword6,"")<>0)&&(strcmp($reqkeyword7,"")<>0)){				
			$reqkeyword6 = sqldatein($reqkeyword6) ;
			$reqkeyword7 = sqldatein($reqkeyword7) ;
			$t->assign ( 'reqkeyword7', $reqkeyword7);
			$t->assign ( 'reqkeyword6', $reqkeyword6);
			if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
			$sql = $sql . "report_date BETWEEN '$reqkeyword6' and '$reqkeyword7' ";
			$PageString = $PageString  .'&txtkeyword6='.$reqkeyword6.'&txtkeyword7='.$reqkeyword7;
		}elseif (strcmp($reqkeyword6,"")<>0){		
			$t->assign ( 'reqkeyword6', $reqkeyword6);
		
			$reqkeyword6 = sqldatein($reqkeyword6) ;
			if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
			$sql = $sql . "report_date >= '$reqkeyword6'";
			$PageString = $PageString  .'&txtkeyword6='.$reqkeyword6;
		}elseif (strcmp($reqkeyword7,"")<>0){	
			$t->assign ( 'reqkeyword7', $reqkeyword7);
			
			$reqkeyword7 = sqldatein($reqkeyword7) ;
			if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
			$sql = $sql . "report_date <= '$reqkeyword7'";
			$PageString = $PageString  .'&txtkeyword7='.$reqkeyword7;
		}
		
		if ( isset( $_REQUEST['txtkeyword1'] ) ){
			$reqkeyword1 = $_REQUEST['txtkeyword1'] ;
			$t->assign ( 'reqkeyword1', $reqkeyword1);
			if (strcmp($reqkeyword1,"")<>0){
				if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
				$sql = $sql . "tbl_lgl_report_draft.category_id = '$reqkeyword1' ";
				$PageString = $PageString  .'&txtkeyword1='.$reqkeyword1;
			}
		}


		if ( isset( $_REQUEST['txtkeyword10'] ) ){
			$reqkeyword10 = $_REQUEST['txtkeyword10'] ;
			$t->assign ( 'reqkeyword10', $reqkeyword10);
			if (strcmp($reqkeyword10,"")<>0){
				if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
				$sql = $sql . "tbl_lgl_report_draft.report_name LIKE  '$reqkeyword10' ";
				$PageString = $PageString  .'&txtkeyword10='.$reqkeyword10;
			}
		}
		$t->assign ( 'reqkeyword10', $reqkeyword10);
		
		if ( isset( $_REQUEST['txtkeyword4'] ) ){
			$reqkeyword4 = $_REQUEST['txtkeyword4'] ;
			$t->assign ( 'reqkeyword4', $reqkeyword4);
			if (strcmp($reqkeyword4,"")<>0){
				if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
				$sql = $sql . "tbl_lgl_company.company_id = '$reqkeyword4' ";
				$PageString = $PageString  .'&txtkeyword4='.$reqkeyword4;
			}
		}
		if ( isset( $_REQUEST['txtkeyword5'] ) ){
			$reqkeyword5 = $_REQUEST['txtkeyword5'] ;
			$t->assign ( 'reqkeyword5', $reqkeyword5);
			if (strcmp($reqkeyword5,"")<>0){
				if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
				$sql = $sql . "tbl_lgl_branch.branch_id = '$reqkeyword5' ";
				$PageString = $PageString  .'&txtkeyword5='.$reqkeyword5;
			}
		}
		
		$reqkeyword12 = fnGetVAlue("tbl_lgl_customer_report_category","category_name","category_id='".$reqkeyword1."' ");
		$t->assign ( 'reqkeyword12', $reqkeyword12);


        if (strcmp($reqsort, "") <> 0) $sql = $sql . ' order by ' . $reqsort;
        $sql = $sql . ' ' . $reqord;
		
$strfnPagingSql = $sql; include 'includes/callpaging.php';
		$temp = $db->getAll( $sql );
		

//echo $sql;
// ---------------- call paging function---------------------------------------------------
// echo $sql;
		$cnt = 1;
			
			foreach ($temp as $index => $row) {
				$row["cnt"] = $cnt++;
				$row['report_date'] = sqldateout($row['report_date']);				
           		$row["customer_name"] = fnGetVAlue("tbl_lgl_customer","customer_name","customer_id='".$row["customer_id"]."' ");
           		$row["category_name"] = fnGetVAlue("tbl_lgl_customer_report_category","category_name","category_id='".$row["category_id"]."' ");
				$data[]=$row;
			}
			
        $branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$reqkeyword4' order by `branch_name` asc";
		$temp = $db->getAll( $sql );
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);
			
		$t->assign('data',$data);
		$t->display('report_details_summery.htm');
	    ?>