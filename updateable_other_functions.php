<?php

function fnchkif_prize_first($reqGroupID){
	  $grp_if_prize_first 	= fngetvalue("groupdetails","if_prize_first","GroupID=".$reqGroupID);			
	  if (($grp_if_prize_first  != 1) && ($grp_if_prize_first  != 2) ){
	  	  $grp_if_prize_first		= FnGetValue('`tbl_mn_app_setting`','field_value',"field_name = 'if_prize_first'");
	  }
	  return $grp_if_prize_first;
}

function fnaddprize($reqtxtGrp_No,$reqtxtTicket_No,$reqtxtAuction_No,$reqtxtAuction_Amt,$reqtxtForeman,$reqedit,$req_Prev_Prize_details_ID,$reqchkSMS,$Prize_details_ID='',$reqtxtchitvalue_share=''){
	

	include_once( 'acc_include_functions.php');
	
	
//Session Data
	$ses_lang 					= $_SESSION['opt_lang'];
// Variable Declatrion/data
	$Group_acc_type_nature 		= 'Assets';
	$reqtxtCheque_Date  		= '0000-00-00' ; 
//Post Data

/*	$reqtxtInst_Month   		= $_POST['txtInst_Month'];
	$reqtxtCheque_No 			= $_POST['txtCheque_No'];
	if($_POST['txtCheque_Date'] != "")	$reqtxtCheque_Date  	= SqlDateIn($_POST['txtCheque_Date']);
	$reqtxtBank_Name 			= $_POST['txtBank_Name'];

    $reqtxtService_Tax   		= $_POST['txtService_Tax'];
	$reqtxtAdjacement   		= $_POST['txtAdjacement'];
	$reqtxtVerification_Charge  = $_POST['txtVerification_Charge'];
	$reqtxtNetPayable_Amount   	= $_POST['txtNetPayable_Amount'];		
*/	
	//$reqtxtchitvalue_share   	= $_POST['txtchitvalue_share'];		
// Table groupdetails Related Data
	$groupdetails_array			= fngetvalueMultiple('groupdetails','Duration,ChitValue,main_branch_id,GroupID',"GroupNo='".$reqtxtGrp_No."'");
	$reqtxtDuration 			= $groupdetails_array[0];
	$reqtxtchitvalue			= $groupdetails_array[1];
 	$reqtxtgroupbranch			= $groupdetails_array[2];
	$req_GroupID 	 			= $groupdetails_array[3];
// Table Auction Related Data
	$auction_array				= fngetvalueMultiple('auction','AuctionDateTime,LastDateofPayment,Month',"GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtAuction_No'");		
	$reqauctiondate 			= $auction_array[0];
	$reqtxtAuction_Date 		= substr($auction_array[0],10);
	$reqtxtInst_Month   		= $auction_array[2];
	//$reqtxtReciept_Date 		= $auction_array[1];
	$reqtxtAuction_Date  		= sqldateout( $reqtxtAuction_Date);		
	$reqtxtAuction_time  		= SQLDateOut_Time( $auction_array[0]);
	$LastDateofPayment			= sqldateout( $auction_array[1]);		
	$reqtxtAuction_time  		= substr($reqtxtAuction_time,11);
	if($reqauctiondate == ""){ 
		$reqauctiondate  		= '0000-00-00 00:00' ; 
	}
// Table applicantdetails Related Data
	if ($reqtxtTicket_No=='-1'){
		$req_AppID 		 			= '';
		$req_SubCodeNo 	 			= '';
		if ($reqtxtchitvalue_share=='')	$reqtxtchitvalue_share=$reqtxtchitvalue;			
	}else{
		$applicantdetails_array		= fngetvalueMultiple('applicantdetails','AppID,SubCodeNo,chitvalue_share,SubID,is_comp_acc',"GroupNo = '$reqtxtGrp_No' and TicketNo = '$reqtxtTicket_No' ");	
		$req_AppID 		 			= $applicantdetails_array[0];
		$req_SubCodeNo 	 			= $applicantdetails_array[1];
		if ($reqtxtchitvalue_share=='')	$reqtxtchitvalue_share=$applicantdetails_array[2];	
		$req_SubID 	 				= $applicantdetails_array[3];		
		$is_comp_acc 				= $applicantdetails_array[4];		
	}
// Table tbl_mn_app_setting Related Data
	$is_prized_in_assets 		= FnGetValue('`tbl_mn_app_setting`','field_value',"field_name = 'is_prized_in_assets'");
	$if_prize_first 			= fnchkif_prize_first($req_GroupID);//FnGetValue('`tbl_mn_app_setting`','field_value',"field_name = 'if_prize_first'");
	$req_prev_AppID 			= fngetvalue('prize_details','AppID',"Prize_details_ID = '$req_Prev_Prize_details_ID'");			
	$one_acc_for_group			= fngetvalue("tbl_mn_app_setting","field_value","field_name='one_acc_for_group'");
	$round_devidend				= fngetvalue("tbl_mn_app_setting","field_value","field_name='round_devidend'");
	$accounting_format				= fngetvalue("tbl_mn_app_setting","field_value","field_name='accounting_format'");


			
// Table tbl_acc_account_type Related Data
	if ($is_prized_in_assets==1){
		$Group_acc_type_id 	= fnGetValue('tbl_acc_account_type','main_account_type_id',"account_type_name = '".$reqtxtGrp_No."' and account_type_nature = 'Assets' and lang='$ses_lang'");
		$Group_acc_type_before_id 	= fnGetValue('tbl_acc_account_type','main_account_type_id',"account_type_name = '".$reqtxtGrp_No."' and account_type_nature = 'Liability' and lang='$ses_lang'");		
	}else{
		$Group_acc_type_id 	= fnGetValue('tbl_acc_account_type','main_account_type_id',"account_type_name = '".$reqtxtGrp_No."' and account_type_nature = 'Liability' and lang='$ses_lang'");		
		$Group_acc_type_before_id 	= fnGetValue('tbl_acc_account_type','main_account_type_id',"account_type_name = '".$reqtxtGrp_No."' and account_type_nature = 'Assets' and lang='$ses_lang'");		
	}
	
// Table tbl_acc_financial_years Related Data
	$finacial_yr_id				= fngetvalue('tbl_acc_financial_years','main_acc_fianancial_year_id'," main_branch_id='$reqtxtgroupbranch' and is_default='1' and lang='english'");

// Table prize_details Related Data
	if($Prize_details_ID == ""){
		$Prize_details_ID 		= fnGetValue('prize_details','Prize_details_ID',"`GroupNo` = '$reqtxtGrp_No' and `TicketNo` = '$reqtxtTicket_No'");
	}
	if ($reqtxtTicket_No=='-1'){
		$req_present 	 		= fnGetCount('prize_details',"AppID = '' and GroupNo = '$reqtxtGrp_No' and TicketNo = '$reqtxtTicket_No' and prizeinstno = '$reqtxtAuction_No'");		
	}else{ 
		$req_present 	 		= fnGetCount('prize_details',"AppID = $req_AppID and GroupNo = '$reqtxtGrp_No' and TicketNo = '$reqtxtTicket_No' and prizeinstno = '$reqtxtAuction_No'");
	}
	$reqlasdevidentt 			= ($reqtxtAuction_Amt - ($reqtxtchitvalue * $reqtxtForeman/100))/$reqtxtDuration;
	$req_max_inst_paid 			= FnGetValue('auction,subscription_details','max(auction.InstNo)',"auction.Auction_ID = subscription_details.Auction_ID and auction.GroupNo='$reqtxtGrp_No' and subscription_details.Ticketno='".$reqtxtTicket_No."'");
	$reqtxtinstallment 			= ( $reqtxtDuration - $req_max_inst_paid ) * ( $reqtxtchitvalue / $reqtxtDuration );
	//if(USE_CHIT_VALUE_SHARE == 1){



	
	if($reqedit == 1){		
		if( $req_present > 0 ){
			/*
			$ins_sql2 = "Update prize_details SET 
				prizeamount 	= '$reqtxtAuction_Amt',		prizechequeno 	= '$reqtxtCheque_No',			prizebankname 	= '$reqtxtBank_Name',
				prizechequedate = '$reqtxtCheque_Date',		prizemonth 		= '$reqtxtInst_Month',			auctiondate 	= '$reqauctiondate',
				prizeinstno  	= '$reqtxtAuction_No',		installment  	= '$reqtxtinstallment',
				foreman_comm 	= '$reqtxtForeman',			chitvalue_share = '$reqtxtchitvalue_share',		prizepaymentby	= '$reqtxtPayment'	
			where AppID = $req_AppID and GroupNo = '$reqtxtGrp_No' and TicketNo = '$reqtxtTicket_No' and prizeinstno = '$reqtxtAuction_No'";
			*/
			$ins_sql2 = "Update prize_details SET 
				prizeamount 	= '$reqtxtAuction_Amt',		prizemonth 		= '$reqtxtInst_Month',			auctiondate 	= '$reqauctiondate',
				prizeinstno  	= '$reqtxtAuction_No',		installment  	= '$reqtxtinstallment',			foreman_comm 	= '$reqtxtForeman',
				chitvalue_share = '$reqtxtchitvalue_share',	GroupID			= '$req_GroupID',				SubID			= '$req_SubID'
			where Prize_details_ID = $Prize_details_ID";
			$res_sql2 = mysql_query($ins_sql2) or die(mysql_error().$ins_sql2);	
			//$Prize_details_ID	  = fnGetvalue("prize_details","Prize_details_ID","AppID = $req_AppID and GroupNo = '$reqtxtGrp_No' and TicketNo = '$reqtxtTicket_No' and prizeinstno = '$reqtxtAuction_No'");
			$Prize_details_ID	  = $Prize_details_ID;
		}else{
			$sql_previos_delete = "delete from prize_details 	where Prize_details_ID = '$req_Prev_Prize_details_ID'"; 
			$previos_sql1 		= mysql_query($sql_previos_delete) or die(mysql_error().$sql_previos_delete);			
			$ins_sql1 = "INSERT INTO `prize_details` (`Prize_details_ID`, 
					`AppID`, `Ticketno`, `GroupNo`, `SubCodeNo`, `prizeamount`,	`prizemonth`, `auctiondate`, 
					`installment`, `amount`, `prizeinstno`,chitvalue_share, main_branch_id,foreman_comm,GroupID,SubID) 
					VALUES 	(NULL,
					'$req_AppID','$reqtxtTicket_No','$reqtxtGrp_No','$req_SubCodeNo','$reqtxtAuction_Amt','$reqtxtInst_Month','$reqauctiondate',
					'$reqtxtinstallment','','$reqtxtAuction_No','$reqtxtchitvalue_share',$reqtxtgroupbranch,'$reqtxtForeman','$req_GroupID','$req_SubID')";
			$res_sql  = mysql_query($ins_sql1) or die(mysql_error().$ins_sql1);	
			$Prize_details_ID = mysql_insert_id();
		}	
	}else{
		if( $req_present > 0 ){
			$ins_sql2 = "Update prize_details SET 
				prizeamount 	= '$reqtxtAuction_Amt',		prizemonth 		= '$reqtxtInst_Month',			auctiondate 	= '$reqauctiondate',
				prizeinstno  	= '$reqtxtAuction_No',		installment  	= '$reqtxtinstallment',			foreman_comm 	= '$reqtxtForeman',
				chitvalue_share = '$reqtxtchitvalue_share',	GroupID			= '$req_GroupID',				SubID			= '$req_SubID'
			where Prize_details_ID = $Prize_details_ID";
			$res_sql2 = mysql_query($ins_sql2) or die(mysql_error().$ins_sql2);	
			$Prize_details_ID	  = $Prize_details_ID;
		}else{		
			$ins_sql1 = "
				INSERT INTO `prize_details` (`Prize_details_ID`, `AppID`, `Ticketno`, `GroupNo`, `SubCodeNo`, `prizeamount`, 
						`prizemonth`, `auctiondate`, `installment`, `amount`, `prizeinstno`, chitvalue_share,	main_branch_id,foreman_comm,GroupID,SubID) 
				VALUES 	(NULL,'$req_AppID','$reqtxtTicket_No','$reqtxtGrp_No','$req_SubCodeNo','$reqtxtAuction_Amt',
						'$reqtxtInst_Month','$reqauctiondate','$reqtxtinstallment','','$reqtxtAuction_No','$reqtxtchitvalue_share',$reqtxtgroupbranch,'$reqtxtForeman','$req_GroupID','$req_SubID')";
			$res_sql  = mysql_query($ins_sql1) or die(mysql_error().$ins_sql1);	
			$Prize_details_ID = mysql_insert_id();
		}
		//Company ticket auto reciept entry start  
		if ($one_acc_for_group==1){ 
			fnaddcompany_auto_receipt($reqtxtgroupbranch,$req_GroupID,$reqtxtAuction_No,$reqauctiondate);					
		}	
		//Company ticket auto reciept entry end     
	}
	//}	
	if($reqedit == 1){
		/*$update_previos_data = "
				Update 
					applicantdetails 
				SET 
					prizeamount = '0',prizechequeno = '',prizebankname = '',prizechequedate = '0000-00-00',prizemonth = '',
					prizeinstno  = '0',installment='0',status = 'non prized'
				where 
					GroupNo = '$reqtxtGrp_No' and TicketNo = '$req_Prev_Prize_details_ID'"; 
		
		$previos_sql = mysql_query($update_previos_data) or die(mysql_error().$update_previos_data);	*/
		// account related changes start

		//$rel_prev_acc_id 				= fnGetValue('tbl_acc_accounts','main_acc_account_id',"related_type = 'applicantdetails' and related_id = '".$req_prev_AppID ."' and lang='$ses_lang' and main_branch_id='$reqtxtgroupbranch'");
		if ($one_acc_for_group!=1){
			$req_prev_from_prize_acc_id 	= fnGetValue('tbl_acc_accounts','main_acc_account_id',"related_type = 'applicantdetails' and related_id = '".$req_prev_AppID ."' and lang='$ses_lang' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
			if ($reqtxtTicket_No=='-1'){		
				$account_type_group_liability= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = '$reqtxtGrp_No' and account_type_nature='Liability' and lang = '$ses_lang'"); 
				$req_cur_from_prize_acc_id	= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang= '$ses_lang' and account_type = '$account_type_group_liability' and account_name='$reqtxtGrp_No Bunyan Tree' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");    
				if ( $req_cur_from_prize_acc_id == 0 ) 	$req_cur_from_prize_acc_id	= create_account("$reqtxtGrp_No Bunyan Tree",$account_type_group_liability,"","",$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtgroupbranch);			
			}else{
				$req_cur_from_prize_acc_id 		= fnGetValue('tbl_acc_accounts','main_acc_account_id',"related_type = 'applicantdetails' and related_id = '".$req_AppID ."' and lang='$ses_lang' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
			}
			if($req_prev_AppID != $req_AppID){
				$update_sql1111 	= "
					update tbl_acc_relate,tbl_acc_transaction 
					set tbl_acc_transaction.from_acc_id = '$req_cur_from_prize_acc_id' 
					where 
						tbl_acc_relate.acc_transaction_id = tbl_acc_transaction.acc_transaction_main_id and
						tbl_acc_relate.acc_transaction_type in ('Journal_dividend','Journal_foreman_commission') and 
						tbl_acc_transaction.from_acc_id = '$req_prev_from_prize_acc_id'";
				$update_result 		= mysql_query($update_sql1111) or die(mysql_error().$update_sql1111);
				
				$update_sql 		= "update tbl_acc_relate set refered_id = '$Prize_details_ID' where acc_transaction_type = 'Journal_dividend' and refered_id = '$req_Prev_Prize_details_ID'";
				$update_result 		= mysql_query($update_sql) or die(mysql_error().$update_sql);	
				$update_sql 		= "update tbl_acc_relate set refered_id = '$Prize_details_ID' where acc_transaction_type = 'Journal_foreman_commission' and refered_id = '$req_Prev_Prize_details_ID'";
				$update_result 		= mysql_query($update_sql) or die(mysql_error().$update_sql);	
				$update_sql 		= "update tbl_acc_relate set refered_id = '$Prize_details_ID' where acc_transaction_type = 'Journal_dividend_pay' and refered_id = '$req_Prev_Prize_details_ID'";
				$update_result 		= mysql_query($update_sql) or die(mysql_error().$update_sql);			
				$upd_sql 			= "Update tbl_acc_accounts SET account_type = '".$Group_acc_type_before_id."' where main_acc_account_id = '$req_prev_from_prize_acc_id' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'";
				$res_udp 			= mysql_query($upd_sql) or die(mysql_error().$upd_sql);			
			}
		}else{
			if($req_prev_AppID != $req_AppID){
				$update_sql 		= "update tbl_acc_relate set refered_id = '$Prize_details_ID' where acc_transaction_type = 'Journal_Mem_Auction' and refered_id = '$req_Prev_Prize_details_ID'";
				$update_result 		= mysql_query($update_sql) or die(mysql_error().$update_sql);	
				$update_sql 		= "update tbl_acc_relate set refered_id = '$Prize_details_ID' where acc_transaction_type = 'Journal_Mem_Auction_Sub' and refered_id = '$req_Prev_Prize_details_ID'";
				$update_result 		= mysql_query($update_sql) or die(mysql_error().$update_sql);	
			}
		}
		// account related changes end
	}	
	
	//$rel_acc_id 	= find_acc_id_for_applicant($req_AppID,$reqtxtgroupbranch);
	if ($one_acc_for_group!=1){	
		if ($reqtxtTicket_No=='-1'){
			$account_type_group_liability= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = '$reqtxtGrp_No' and account_type_nature='Liability' and lang = '$ses_lang'"); 
			$req_from_prize_acc_id	= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang= '$ses_lang' and account_type = '$account_type_group_liability' and account_name='$reqtxtGrp_No Bunyan Tree' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");    
			if ( $req_from_prize_acc_id == 0 ) 	$req_from_prize_acc_id	= create_account("$reqtxtGrp_No Bunyan Tree",$account_type_group_liability,"","",$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtgroupbranch);					
		}else{
			$req_from_prize_acc_id 	= find_acc_id_for_applicant($req_AppID,$reqtxtgroupbranch);		
			$upd_sql 	= "Update tbl_acc_accounts SET account_type = '".$Group_acc_type_id."' where main_acc_account_id = '$req_from_prize_acc_id' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id' ";
			$res_udp 	= mysql_query($upd_sql) or die(mysql_error().$upd_sql);			
		}
	}
	//echo $req_from_prize_acc_id;
	$formancommision 		= ($reqtxtchitvalue_share * $reqtxtForeman/100);		 		

	//$req_from_prize_acc_id 	= fnGetValue("tbl_acc_accounts","main_acc_account_id","related_id = '".$req_AppID ."' and lang='$ses_lang'");
	if ($one_acc_for_group==1){		
		$req_chit_account_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '11' and account_name='Chit Account' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_chit_account_acc_id==0){
			$req_chit_account_acc_id=create_account('Chit Account','11','','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtgroupbranch,$finacial_yr_id);
		}
	    if ($is_comp_acc ==1){
			$req_liability_on_chit_lifted_by_comp_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '14' and account_name='Liability on Chits Lifted by Company' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
			if ($req_liability_on_chit_lifted_by_comp_acc_id==0){
				$req_liability_on_chit_lifted_by_comp_acc_id=create_account('Liability on Chits Lifted by Company','11','','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtgroupbranch,$finacial_yr_id);
			}			
			$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_comp_auction' and refered_id = '$Prize_details_ID'");
			add_voucher('Journal_comp_auction',$req_chit_account_acc_id,$Prize_details_ID,$reqtxtchitvalue_share,$reqauctiondate,"Company Chit Auction",$reqtxtgroupbranch,$acc_trans_main_id,$req_liability_on_chit_lifted_by_comp_acc_id);			
		}else{
			$req_foreman_comm_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Direct Incomes' and lang = '$ses_lang'");								
			$req_foreman_comm_acc_id   = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_foreman_comm_acc_type' and account_name='Foreman Commission' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
			if ($req_foreman_comm_acc_id==0){
				$req_foreman_comm_acc_id=create_account('Foreman Commission',$req_foreman_comm_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtgroupbranch,$finacial_yr_id);
			}			
			$req_prize_money_payble_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Prize Money Payble' and lang = '$ses_lang'");								
			$req_prize_money_payble_acc_id   = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_prize_money_payble_acc_type' and account_name='Prize Money Payble - ".$reqtxtGrp_No."' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
			if ($req_prize_money_payble_acc_id==0){
				$req_prize_money_payble_acc_id=create_account('Prize Money Payble - '.$reqtxtGrp_No,$req_prize_money_payble_acc_type,'groupdetails',$req_GroupID,$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtgroupbranch,$finacial_yr_id);
			}		
			$req_dividend_payble_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Dividend Payable' and lang = '$ses_lang'");								
			$req_dividend_payble_acc_id   = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_dividend_payble_acc_type' and account_name='Dividend Payable - ".$reqtxtGrp_No."' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
			if ($req_dividend_payble_acc_id==0){
				$req_dividend_payble_acc_id=create_account('Dividend Payable - '.$reqtxtGrp_No,$req_dividend_payble_acc_type,'groupdetails',$req_GroupID,$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtgroupbranch,$finacial_yr_id);
			}			
			$req_arrears_of_subn_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Arrears of Subscription' and lang = '$ses_lang'");								
			$req_arrears_of_subn_acc_id   = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_arrears_of_subn_acc_type' and account_name='Arrears of Subscription - ".$reqtxtGrp_No."' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
			if ($req_arrears_of_subn_acc_id==0){
				$req_arrears_of_subn_acc_id=create_account('Arrears of Subscription - '.$reqtxtGrp_No,$req_arrears_of_subn_acc_type,'groupdetails',$req_GroupID,$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtgroupbranch,$finacial_yr_id);
			}
			
			$to_accs = array();
			$to_accs[0][0]=$req_prize_money_payble_acc_id;		
			$to_accs[0][1]=($reqtxtchitvalue_share-$reqtxtAuction_Amt);						
		    $i=1;

			if ($formancommision > 0){
				$to_accs[$i][0] = $req_foreman_comm_acc_id;		
				$to_accs[$i++][1] = $formancommision;	
			}
			if (($reqtxtAuction_Amt - $formancommision) > 0){	
				$to_accs[$i][0]=$req_dividend_payble_acc_id;		
				$to_accs[$i++][1]=($reqtxtAuction_Amt - $formancommision);										
			}						
						
			$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_Mem_Auction' and refered_id = '$Prize_details_ID'");
			add_voucher('Journal_Mem_Auction',$req_chit_account_acc_id,$Prize_details_ID,$reqtxtchitvalue_share,$reqauctiondate,"Member Auction",$reqtxtgroupbranch,$acc_trans_main_id,$to_accs);						

			$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_Mem_Auction_Sub' and refered_id = '$Prize_details_ID'");
			add_voucher('Journal_Mem_Auction_Sub',$req_arrears_of_subn_acc_id,$Prize_details_ID,$reqtxtchitvalue_share,$reqauctiondate,"Member Auction Subscription",$reqtxtgroupbranch,$acc_trans_main_id,$req_chit_account_acc_id);						

		}
	}elseif ($accounting_format==1){
		if ($is_comp_acc!=1){
			
			if (($reqtxtAuction_Amt - $formancommision) > 0){
				$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_dividend' and refered_id = '$Prize_details_ID'");
				//add_voucher('Journal_dividend',$req_from_prize_acc_id,$Prize_details_ID,($reqtxtAuction_Amt - $formancommision),$reqtxtAuction_Date,"",$acc_trans_main_id);
				add_voucher('Journal_dividend',$req_from_prize_acc_id,$Prize_details_ID,($reqtxtAuction_Amt - $formancommision),$reqauctiondate,"Dividend",$reqtxtgroupbranch,$acc_trans_main_id);
			}
			if ($formancommision > 0){
				$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_foreman_commission' and refered_id = '$Prize_details_ID'");
				//add_voucher('Journal_foreman_commission',$req_from_prize_acc_id,$Prize_details_ID,$formancommision,$reqtxtAuction_Date,"",$acc_trans_main_id);
				add_voucher('Journal_foreman_commission',$req_from_prize_acc_id,$Prize_details_ID,$formancommision,$reqauctiondate,"Foreman Commission",$reqtxtgroupbranch,$acc_trans_main_id);
			}
		}		
	}else{
		if ($is_comp_acc!=1){
			if (($reqtxtAuction_Amt - $formancommision) > 0){
				$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_dividend' and refered_id = '$Prize_details_ID'");
				//add_voucher('Journal_dividend',$req_from_prize_acc_id,$Prize_details_ID,($reqtxtAuction_Amt - $formancommision),$reqtxtAuction_Date,"",$acc_trans_main_id);
				add_voucher('Journal_dividend',$req_from_prize_acc_id,$Prize_details_ID,($reqtxtAuction_Amt - $formancommision),$reqauctiondate,"Dividend",$reqtxtgroupbranch,$acc_trans_main_id);
			}
			if ($formancommision > 0){
				$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_foreman_commission' and refered_id = '$Prize_details_ID'");
				//add_voucher('Journal_foreman_commission',$req_from_prize_acc_id,$Prize_details_ID,$formancommision,$reqtxtAuction_Date,"",$acc_trans_main_id);
				add_voucher('Journal_foreman_commission',$req_from_prize_acc_id,$Prize_details_ID,$formancommision,$reqauctiondate,"Foreman Commission",$reqtxtgroupbranch,$acc_trans_main_id);
			}
		}
	}
	//accounting related end  
	
    //accounting related start
	$totalchitvalue_share 	= fngetvalue("prize_details","sum(chitvalue_share)","GroupNo='".$reqtxtGrp_No."' and 	prizeinstno ='".$reqtxtAuction_No."'");	
	$totalprizeamount 		= fngetvalue("prize_details","sum(prizeamount)","GroupNo='".$reqtxtGrp_No."' and 	prizeinstno ='".$reqtxtAuction_No."'");	
	$totalformancommision 	= fngetvalue("prize_details","sum(chitvalue_share*foreman_comm/100)","GroupNo='".$reqtxtGrp_No."' and 	prizeinstno ='".$reqtxtAuction_No."'");		
	
	if ($round_devidend==1){
		$totalreqlasdevidentt 	= round(($totalprizeamount - $totalformancommision)/$reqtxtDuration);
	}else{
		$totalreqlasdevidentt 	= floor(($totalprizeamount - $totalformancommision)/$reqtxtDuration);
	}
	if ( $totalchitvalue_share >= $reqtxtchitvalue ){
		/*$sql = "SELECT * FROM applicantdetails where GroupNo='$reqtxtGrp_No'";
		$temp = mysql_query($sql) or die(mysql_error().$sql);
		while($row = mysql_fetch_array($temp)){
			$refered_id 	 	= $Prize_details_ID."~".$row['AppID'];
			//$req_from_acc_id 	= fnGetValue("tbl_acc_accounts","main_acc_account_id","related_id = '".$row['AppID'] ."' and lang='$ses_lang'");
			$req_from_acc_id 	= find_acc_id_for_applicant($row['AppID'],$reqtxtgroupbranch);
			$reqtxtReciept_Date = fngetvalue("auction","left(AuctionDateTime,10)","GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtAuction_No'");	
			if ( $reqtxtReciept_Date == 0 ) $reqtxtReciept_Date	= date("Y-m-d");
			$reqlasdevidentt_share = ( $row['chitvalue_share'] / $reqtxtchitvalue ) * $totalreqlasdevidentt;			
			if ( $reqlasdevidentt_share > 0 ){
				$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_dividend_pay' and refered_id = '$refered_id'");
				//add_voucher('Journal_dividend_pay',$req_from_acc_id,$refered_id,$reqlasdevidentt_share,$reqtxtReciept_Date,"",$acc_trans_main_id);
				//add_voucher('Journal_dividend_pay',$req_from_acc_id,$refered_id,$reqlasdevidentt_share,$reqtxtReciept_Date,"Dividend Distribution",$reqtxtgroupbranch,$acc_trans_main_id);
			}
		}*/
		$ins_sql = "Update auction SET prizegiven = '1' where GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtAuction_No'";
		$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);			
		
		if ($if_prize_first == 1){ 	
			$reqNextAuction_No = $reqtxtAuction_No + 1;
			if ($reqNextAuction_No <= $reqtxtDuration){ 
				$ins_sql = "Update auction SET lasdevidentt = '$totalreqlasdevidentt' where GroupNo = '$reqtxtGrp_No' and InstNo = '$reqNextAuction_No'";
				$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);
			}	
		}else{
			$ins_sql = "Update auction SET lasdevidentt = '$totalreqlasdevidentt'  where GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtAuction_No'";
			$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);
		}
	}	
	
	/*
	$ins_sql = "
		Update 
			applicantdetails 
		SET 
			prizeamount = '$reqtxtAuction_Amt',		prizemonth = '$reqtxtInst_Month',		auctiondate = '$reqauctiondate',			
			prizeinstno  = '$reqtxtAuction_No',		installment  = '$reqtxtinstallment',	foreman_comm = '$reqtxtForeman',
			status = 'Auctioned'	
		where 
			GroupNo = '$reqtxtGrp_No' and TicketNo = '$reqtxtTicket_No'";
	$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);	
	// update status of applicant 
	$ins_sql = "Update auction SET prizegiven = '1',bidding_status = 2, biddingendtime = CURTIME()  where GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtAuction_No'";
	$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);
    if ($if_prize_first == 1){ 	
		$reqNextAuction_No = $reqtxtAuction_No + 1;
		if ($reqNextAuction_No <= $reqtxtDuration){ 
			$ins_sql = "Update auction SET lasdevidentt = '$reqlasdevidentt' where GroupNo = '$reqtxtGrp_No' and InstNo = '$reqNextAuction_No'";
			$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);
		}	
	}else{
		$ins_sql = "Update auction SET lasdevidentt = '$reqlasdevidentt'  where GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtAuction_No'";
		$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);
	}

	$rel_acc_id = fnGetValue("tbl_acc_accounts","main_acc_account_id","related_id = '".$req_AppID ."' and lang = '$ses_lang' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
	$upd_sql 	= "Update tbl_acc_accounts SET account_type = '".$Group_acc_type_after_id."' where main_acc_account_id = '$rel_acc_id' ";
	$res_udp 	= mysql_query($upd_sql) or die(mysql_error().$upd_sql);			
	
	$formancommision = ($reqtxtchitvalue * $reqtxtForeman / 100);		 		
	
	$req_from_prize_acc_id 	= fnGetValue("tbl_acc_accounts","main_acc_account_id","related_type = 'applicantdetails' and related_id = '".$req_AppID ."' and lang='$ses_lang' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
	$acc_trans_main_id 		= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_dividend' and refered_id = '$req_AppID'");
	if (($reqtxtAuction_Amt - $formancommision) == 0 && $acc_trans_main_id > 0){ delete_voucher($acc_trans_main_id);}
	if (($reqtxtAuction_Amt - $formancommision) > 0){		
		add_voucher('Journal_dividend',$req_from_prize_acc_id,$req_AppID,($reqtxtAuction_Amt - $formancommision),$reqtxtAuction_Date,"Dividend",$reqtxtgroupbranch,$acc_trans_main_id);
	}
	
	$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_foreman_commission' and refered_id = '$req_AppID'");
	if ($formancommision == 0 && $acc_trans_main_id > 0){delete_voucher($acc_trans_main_id);}
	if ($formancommision > 0){		
		add_voucher('Journal_foreman_commission',$req_from_prize_acc_id,$req_AppID,$formancommision,$reqtxtAuction_Date,"Foreman Commission",$reqtxtgroupbranch,$acc_trans_main_id);
	}
	$req_from_acc_id 	= array();
	$sql 				= "SELECT * FROM applicantdetails where GroupNo='$reqtxtGrp_No'";
	$temp 				= mysql_query($sql) or die(mysql_error().$sql);
	$total_tks 			= mysql_num_rows($temp);
	while($row = mysql_fetch_array($temp)){
		$req_from_acc_id[]	= $row['AppID'];
	}
	$refered_id 			= $req_AppID;
	$reqlasdevidentt_share 	= $reqtxtAuction_Amt - $formancommision ;
	if ( $reqtxtReciept_Date == 0 ) $reqtxtReciept_Date	= date("Y-m-d");
	$acc_trans_main_id 		= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_dividend_pay' and refered_id = '$refered_id'");
	add_voucher('Journal_dividend_pay',$req_from_acc_id,$refered_id,$reqlasdevidentt_share,$reqtxtReciept_Date,"Dividend Distribution",$reqtxtgroupbranch,$acc_trans_main_id);
	
	$reqtxtAmt  = $reqtxtchitvalue - $reqtxtAuction_Amt;
	if ($reqtxtCheque_No<>""){
		$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Payment_chk' and refered_id = '$refered_id'");
		//add_voucher('Payment_chk',$req_from_prize_acc_id,$refered_id,$reqtxtAmt,$reqtxtCheque_Date,"Prize Payment",$reqtxtgroupbranch,$acc_trans_main_id);
	}
	*/
	
	// Outstanding Details
	fnUpdateOutstanding($reqtxtgroupbranch,$reqtxtGrp_No,$reqtxtAuction_No);
	// Outstanding Details
	//accounting related end  			
	if ($reqchkSMS =='YES'){		
		if ($reqtxtTicket_No!='-1'){
			$reqtxtFullName = fngetvalue("subdirdetails,applicantdetails","FullName","applicantdetails.SubID= subdirdetails.SubID and applicantdetails.AppID=$req_AppID");
		}else{
			$reqtxtFullName = "Banyan Tree";
		}
		
		$main_data 		= array();
		$main_sql 		= "	select 
								subdirdetails.*,applicantdetails.main_branch_id as main_br_id,
								applicantdetails.GroupNo,applicantdetails.Ticketno 
							from  subdirdetails,applicantdetails 
							where applicantdetails.SubID = subdirdetails.SubID and 
								  applicantdetails.GroupNo = '$reqtxtGrp_No' and 
				 	    		  (applicantdetails.Remove_status<>'Removed' or applicantdetails.Remove_status is null)";
		$main_tmp 		= mysql_query($main_sql) or die(mysql_error().$main_sql);
		while($main_row = mysql_fetch_array($main_tmp)){
			$Phone_No 				= $main_row['MobNoCorres'];
			$SubID 					= $main_row['SubID'];
			// Table tbl_sur_sign_up Related Data
			$tbl_sur_sign_up_array 	= array();
			$tbl_sur_sign_up_array 	= fngetvalueMultiple('tbl_sur_sign_up','personnel_id,email,first_name',"ref_user_code=$SubID");
			$reqpersonnel_id 		= $tbl_sur_sign_up_array[0];
			$reqemail   		 	= $tbl_sur_sign_up_array[1];
			$firstname 	 			= $tbl_sur_sign_up_array[2];
			
		    $shop_url    			= WEBSITE_PATH . "index.php" ; 
			$shop_name   			= FnGetValue('tbl_mn_lhf','category_tag',"type='lhf' and name='shop_name' and lang='english'");
			//$shop_name   			= 'eChit';
			$shop_logo   			= FnGetValue("tbl_mn_lhf","category_tag","type='lhf' and name='shop_logo' and lang='english'");			
			
			//avdhut change
			$member_details 		= fngetmember_due($main_row['main_br_id'],$main_row['GroupNo'],$main_row['Ticketno']);
			$due					= $member_details[1];
			//----------------------
			$req_Report_ID 	= 1;
			$req_Message = FnShowEmail_Lang("dear_sir") ." ". $main_row['FullName'] . " ".
							//FnShowEmail_Lang("pr_auction_in_monthly_chit")." ". $reqtxtGrp_No ." ". FnShowEmail_Lang("of_Rs") ." ".$reqtxtchitvalue ." ".
							//FnShowEmail_Lang("was_held_on") ." ". $reqtxtAuction_Date ." ".FnShowEmail_Lang("at")." ". $reqtxtAuction_time ." ".
							FnShowEmail_Lang("pr_auction_in_monthly_chit")." ". $reqtxtGrp_No ." ".
							FnShowEmail_Lang("was_held_on") ." ". $main_row['AuctionDate'] ." ".
							FnShowEmail_Lang("prized_amt") ." ". $reqtxtAuction_Amt ." ".FnShowEmail_Lang("prized_to") ." ". $reqtxtFullName ." ".FnShowEmail_Lang("having_tkt_no")." ".$reqtxtTicket_No." ". FnShowEmail_Lang("dividend_is")." ".$reqlasdevidentt." ".
							FnShowEmail_Lang("regards");
							//FnShowEmail_Lang("pls_pay_sub_amt")." ".$due." ".FnShowEmail_Lang("before")." ".$LastDateofPayment." ".FnShowEmail_Lang("regards");
			//echo $req_Message;
			fnSendSMS($Phone_No,$req_Message,$req_Report_ID);
		}
	}
	return $reqlasdevidentt;
}
function fnUpdateOutstanding($main_branch_id,$reqtxtGroupID,$reqtxtAuction_No){
	// Update Outstanding report - By sachin
	$auction_sql 	= "select LastDateofPayment,Subcription,Auction_ID,AuctionDateTime,lasdevidentt,InstNo from auction where main_branch_id = '$main_branch_id' and GroupID = '$reqtxtGroupID' and InstNo = '$reqtxtAuction_No'";
	$auction_result =  mysql_query($auction_sql) or die(mysql_error().$auction_sql);
	while($auction_row = mysql_fetch_array($auction_result)){
		$LastDateofPayment 	= $auction_row['LastDateofPayment'];
		$Subcription 		= $auction_row['Subcription'];
		$Auction_ID 		= $auction_row['Auction_ID'];
		$reqtxtAuction_No	= $auction_row['InstNo'];
		$reqauctiondate		= $auction_row['AuctionDateTime'];
		$reqlasdevidentt	= $auction_row['lasdevidentt'];
	}
	$outstanding_summery_Ins_sql = "";
	$sql = "select GroupNo,Ticketno,applicantdetails.SubID,FullName from applicantdetails,subdirdetails where applicantdetails.SubID = subdirdetails.SubID and main_branch_id = '$main_branch_id' and  GroupID='$reqtxtGroupID' ";
	$result =  mysql_query($sql) or die(mysql_error().$sql);
	while($row = mysql_fetch_array($result)){
		$GroupNo 		= $row['GroupNo'];
		$TicketNo 		= $row['Ticketno'];
		$SubCodeNo 		= $row['SubID'];
		$FullName 		= addslashes($row['FullName']);
		$prize_status 	= $row['status'];
	
		$subscription_details_payble_plus 		= "";
		$subscription_details_paid 				= "";
		$subscription_details_LateFee			= "";
		$subscription_details_Subcription_Fee	= "";
		$subscription_details_check_return_fee	= "";
		$subscription_details_transfer_fee		= "";
		$subscription_details_other_charge		= "";
		$subscription_details_ser_tax			= "";
	
		$subscription_details_sql 	= "select 
			sum(IFNULL(LateFee,0) + IFNULL(Subcription_Fee,0) + IFNULL(other_charge,0) + IFNULL(check_return_fee,0) + IFNULL(transfer_fee,0)) as payble_plus,
			sum(IFNULL(Subcription, 0)) as paid,
			 sum(IFNULL(LateFee, 0)) as sub_LateFee,
			 sum(IFNULL(Subcription_Fee, 0)) as sub_Subcription_Fee,
			 sum(IFNULL(check_return_fee, 0)) as sub_check_return_fee,
			 sum(IFNULL(transfer_fee, 0)) as sub_transfer_fee,
			 sum(IFNULL(other_charge, 0)) as sub_other_charge,sum(IFNULL(ser_tax, 0)) as sub_ser_tax
		from 
			subscription_details 
		where 
			Auction_ID = '$Auction_ID' and TicketNo = '$TicketNo' and SubID = '$SubCodeNo' and main_branch_id = '$main_branch_id'
		group by TicketNo,Auction_ID";
		
		$subscription_details_result =  mysql_query($subscription_details_sql) or die(mysql_error().$subscription_details_sql);
		while($subscription_details_row = mysql_fetch_array($subscription_details_result)){
			$subscription_details_payble_plus 		= $subscription_details_row['payble_plus'];
			$subscription_details_paid 				= $subscription_details_row['paid'];
			$subscription_details_LateFee			= $subscription_details_row['LateFee']; 
			$subscription_details_Subcription_Fee	= $subscription_details_row['Subcription_Fee']; 
			$subscription_details_check_return_fee	= $subscription_details_row['check_return_fee']; 
			$subscription_details_transfer_fee		= $subscription_details_row['transfer_fee'];
			$subscription_details_other_charge		= $subscription_details_row['other_charge']; $subscription_details_ser_taxe		= $subscription_details_row['ser_tax'];
		}
	
		$payble_plus = ($Subcription - $reqlasdevidentt) + $subscription_details_payble_plus;
		$outstanding = (($Subcription - $reqlasdevidentt) + $subscription_details_payble_plus) - $subscription_details_paid;
		
		
		$present = fngetCount("tbl_outstanding_summery","GroupNo = '$GroupNo' and TicketNo = '$TicketNo' and SubID = '$SubCodeNo' and InstNo = '$reqtxtAuction_No' and main_branch_id = '$main_branch_id' ");
		if($present > 0){
			$outstanding_summery_sql = "update tbl_outstanding_summery set 
				AuctionDateTime 	= '$reqauctiondate', 
				LastDateofPayment	= '$LastDateofPayment', 
				Subcription			= '$Subcription', 
				lasdevidentt		= '$reqlasdevidentt', 
				payble				= '$payble_plus', 
				paid_amt			= '$subscription_details_paid', 
				LateFee				= '$subscription_details_LateFee', 
				Subcription_Fee		= '$subscription_details_Subcription_Fee', 
				check_return_fee	= '$subscription_details_check_return_fee', 
				transfer_fee		= '$subscription_details_transfer_fee', 
				other_charge		= '$subscription_details_other_charge',
				outstanding			= '$outstanding',
				Auction_ID 			= '$Auction_ID'
			where GroupNo = '$GroupNo' and TicketNo = '$TicketNo' and SubID = '$SubCodeNo' and InstNo = '$reqtxtAuction_No' and main_branch_id = '$main_branch_id'	";
			mysql_query($outstanding_summery_sql) or die(mysql_error().$outstanding_summery_sql);
		}else{
			if($outstanding_summery_Ins_sql != "") 	$outstanding_summery_Ins_sql .=  ",";
			$outstanding_summery_Ins_sql .=  "(NULL, '$GroupNo', '$TicketNo', '$SubCodeNo', '$FullName', '$prize_status', '$reqtxtAuction_No', '$reqauctiondate', '$LastDateofPayment', '$Subcription', '$reqlasdevidentt', '$payble_plus', '$subscription_details_paid', '$subscription_details_LateFee', '$subscription_details_Subcription_Fee', '$subscription_details_check_return_fee', '$subscription_details_transfer_fee', '$subscription_details_other_charge', '$main_branch_id', '$outstanding', '$Auction_ID')";
		}	
	}
	if($outstanding_summery_Ins_sql != ""){
		$outstanding_summery_Ins_sql = "INSERT INTO `tbl_outstanding_summery` (`id`, `GroupNo`, `TicketNo`, `SubID`, `FullName`, `prize_status`, `InstNo`, `AuctionDateTime`, `LastDateofPayment`, `Subcription`, `lasdevidentt`, `payble`, `paid_amt`, `LateFee`, `Subcription_Fee`, `check_return_fee`, `transfer_fee`, `other_charge`, `main_branch_id`, `outstanding`, `Auction_ID`) VALUES ".$outstanding_summery_Ins_sql;
		mysql_query($outstanding_summery_Ins_sql) or die(mysql_error().$outstanding_summery_Ins_sql);
	}
	// Update Outstanding report - By sachin
}
function fnaddbid($reqtxtGrp_No,$reqtxtTicket_No,$reqtxtAuction_No,$reqtxtAuction_Amt){
	$reqAppid=fngetvalue("applicantdetails","AppID"," Ticketno='$reqtxtTicket_No' and GroupNo='$reqtxtGrp_No'");
	$sql="INSERT INTO `auction_bids` (`auction_bids_id`,`Appid` ,`prizeamount` ,`instno`,`bid_datetime` )
	VALUES (NULL,'$reqAppid','$reqtxtAuction_Amt','$reqtxtAuction_No',NOW())";
	$res_sql = mysql_query($sql) or die(mysql_error().$sql);		
	//return mysql_insert_id($res_sql);
}
function fnfind_member_SubID($reqpersonnel_id){
	$SubID = fnGetValue("subdirdetails,tbl_sur_sign_up","SubID","subdirdetails.SubID = tbl_sur_sign_up.ref_user_code and tbl_sur_sign_up.personnel_id = ".$reqpersonnel_id);
	return $SubID;
}
function fnaddsubscription_online($reqtxtBranchCode,$reqtxtGrp_No,$reqtxtTicket_No,$reqtxtInstallment_No,$reqTotalRecd_Inst){

	$reqtxtReciept_Date = date('Y-m-d');
	$reqtxtTR_Date      = "0000-00-00";
	if($_POST['txtCheque_Date'] <>"")$reqtxtCheque_Date  = SqlDateIn($_POST['txtCheque_Date']) ;
	else $reqtxtCheque_Date  = "0000-00-00";
	
	$reqcurr_yr   = date('Y');	

	$reqbranch_abbrivation = FnGetValue("tbl_branch","branch_code","lang = '".$_SESSION['opt_lang']."' and main_branch_id = '$reqtxtBranchCode'");
	$reqwhere = $reqbranch_abbrivation."/".$reqcurr_yr."/";
	$lenreqwhere = strlen($reqwhere);
	if ($lenreqwhere<> 0){		
		$reqrecipt_no = FnGetValue("transaction","max(cast(right(ReceiptNo,length(ReceiptNo)-".$lenreqwhere.") as unsigned))","ReceiptNo like '$reqwhere%'");
		$reqrecipt_no = $reqrecipt_no + 1;
		$reqrecipt_no = $reqwhere.$reqrecipt_no;		
	}else{
	    echo "Reciept cant be generated";
		exit();
	}	
	$reqtxtPayment = 'online';
	
	$ins_sql = "Insert into transaction (`Transaction_ID` ,
			`ReceiptNo` ,`Reciept_Date` ,`TRNO` ,`TRDate` ,`Amount` ,
			`Remark` ,`ChequeNo` ,`Bankname` ,`ChequeDate` ,`printreciept` ,
			`Area` ,`CheRetTime` ,`Cheretfee` ,`DayBookNo` ,
			`main_branch_id`,receipt_type,`payment_by`,`status`
			)values( NULL,
			'$reqrecipt_no','$reqtxtReciept_Date','$reqtxtTR_No','$reqtxtTR_Date','$reqTotalRecd_Inst',
			'','$reqtxtCheque_No','$reqtxtBank_Name','$reqtxtCheque_Date','0',
			'$reqtxtArea','','$reqtxtChRetFee','$reqtxtDay_Bokk_No',
			'$reqtxtBranchCode','$reqtxtrecipt','$reqtxtPayment','1')";

	$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);	
	$reqtrasaction_id = mysql_insert_id(); 
 

	
	$reqtxtSubID    = FnGetValue("applicantdetails","SubID","GroupNo = '$reqtxtGrp_No' and Ticketno = '$reqtxtTicket_No'");		
	$reqtxtAuction_ID = FnGetValue("auction","Auction_ID","GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtInstallment_No'");		
	$reqprvauction_no = ($reqtxtauction_no - 1);
  
	$ins_sql = "Insert into subscription_details 
	(`Subscription_ID` ,`Auction_ID` ,`TicketNo` ,`Transaction_ID` ,`SubID` ,`LateFee` ,`lasdevidentt` ,
	 `arrears` ,`main_branch_id` ,`Subcription` ) values
	(NULL,'$reqtxtAuction_ID','$reqtxtTicket_No','$reqtrasaction_id','$reqtxtSubID','$reqtxtLateFee','$reqtxtDevident',
	'$reqtxtArrears','$reqtxtBranchCode','$reqTotalRecd_Inst')";				
	$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);
  
	$up_sql = "Update subscription_details SET arrears = 0 where TicketNo = '$reqtxtTicket_No' and Auction_ID = '$reqprvauction_no'";
	$up_res = mysql_query($up_sql) or die(mysql_error().$up_sql);		
  
	$sql_insert_cheque="insert into transaction_details (`Transaction_detail_ID`,`Amount`,`ChequeNo`,`Bankname`,`ChequeDate`,`payment_by`,`Remark`,`trans_id`,`dep_slip_ID` ) 
			values(	NULL,'".$reqTotalRecd_Inst."','','',NULL,'online','','".$reqtrasaction_id."','0' )";
	$res_insert_cheque = mysql_query($sql_insert_cheque) or die(mysql_error().$sql_insert_cheque);	
}
function fnaddcompany_auto_receipt($reqmain_branch_id,$reqGroupID,$reqInstNo,$reqAuction_Date,$finacial_yr_id=''){
	
	$ses_lang 					= $_SESSION['opt_lang'];
	$reqGroup_Det = array();
	$reqGroup_Det = fngetvalueMultiple("groupdetails","GroupNo,ChitValue"," GroupID = '$reqGroupID'");	
	$reqGroupNo = $reqGroup_Det[0];
	$reqChitValue = $reqGroup_Det[1];
	
	$applicantdetails_array	 = array();
	$applicantdetails_array	 = fngetvalueMultiple('applicantdetails','Ticketno,SubID,chitvalue_share',"is_comp_acc='1' and GroupID ='$reqGroupID' and main_branch_id = '$reqmain_branch_id'");	
	$reqTicket_No 		= $applicantdetails_array[0];
	$reqSubID	 		= $applicantdetails_array[1];		
	$reqchitvalue_share	= $applicantdetails_array[2];				
	if ( $reqTicket_No >0 ){
		$ispaid=fnis_inst_paid($reqmain_branch_id,$reqGroupID,$reqTicket_No,"($reqInstNo)",$reqchitvalue_share);	
	
		if ($ispaid[0]==false){
			$reqcurr_yr   = date('Y');	
			$reqbranch_abbrivation = FnGetValue("tbl_branch","branch_code","lang = '".$_SESSION['opt_lang']."' and main_branch_id = '$reqmain_branch_id'");
			$reqwhere = $reqbranch_abbrivation."/".$reqcurr_yr."/C/";
			$lenreqwhere = strlen($reqwhere);
			if ($lenreqwhere<> 0){		
				$reqrecipt_no = FnGetValue("transaction","max(cast(right(ReceiptNo,length(ReceiptNo)-".$lenreqwhere.") as unsigned))","ReceiptNo like '$reqwhere%'");
				$reqrecipt_no = $reqrecipt_no + 1;
				$reqrecipt_no = $reqwhere.$reqrecipt_no;		
			}else{
				echo "Reciept cant be generated";
				exit();
			}	
			
			$reqtxtPayment = 'Auto';
			
			$reqAuction_det		= fngetvalueMultiple("auction","Auction_ID,lasdevidentt,Subcription","GroupID = '$reqGroupID' and InstNo = '$reqInstNo'");		
			$reqAuction_ID 		= $reqAuction_det[0];
			$reqlasdevidentt 	= $reqAuction_det[1];
			$reqSubcription 	= $reqAuction_det[2];
			
			$reqprvauction_no = ($reqInstNo - 1);
			
			
			$reqLast_Did 	= floor(($reqchitvalue_share/$reqChitValue) * $reqlasdevidentt);
			$reqCurr_Inst 	= ($reqSubcription-$reqLast_Did);
			
			$ins_sql = "Insert into transaction (`Transaction_ID` ,
							`ReceiptNo` ,`Reciept_Date` ,`TRNO` ,`TRDate` ,`Amount` ,
							`printreciept`,`Area`,`DayBookNo` ,
							`main_branch_id`,receipt_type,`payment_by`,`status`
						)values( NULL,
							'$reqrecipt_no','$reqAuction_Date','CR','$reqAuction_Date','$reqCurr_Inst',
							'','','',
							'$reqmain_branch_id','','$reqtxtPayment','1')";
		
			$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);	
			$reqtrasaction_id = mysql_insert_id(); 
		 
		  
			$ins_sql = "Insert into subscription_details 
			(`Subscription_ID` ,`Auction_ID` ,`TicketNo` ,`Transaction_ID` ,`SubID` ,`LateFee` ,`lasdevidentt` ,
			 `arrears` ,`main_branch_id` ,`Subcription` ) values
			(NULL,'$reqAuction_ID','$reqTicket_No','$reqtrasaction_id','$reqSubID','0','$reqLast_Did',
			'0','$reqmain_branch_id','$reqCurr_Inst')";				
			$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);
			$reqSubscription_ID = mysql_insert_id(); 
			
			$sql_insert_cheque="insert into transaction_details (`Transaction_detail_ID`,`Amount`,`ChequeNo`,`Bankname`,`ChequeDate`,`payment_by`,`Remark`,`trans_id`,`dep_slip_ID` ) 
					values(	NULL,'".$reqCurr_Inst."','','',NULL,'online','','".$reqtrasaction_id."','0' )";
			$res_insert_cheque = mysql_query($sql_insert_cheque) or die(mysql_error().$sql_insert_cheque);	
			if ($finacial_yr_id==''){
				$finacial_yr_id				= fngetvalue('tbl_acc_financial_years','main_acc_fianancial_year_id'," main_branch_id='$reqmain_branch_id' and is_default='1' and lang = '$ses_lang'");
			}
			$req_comp_invt_in_own_chit_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Investments' and lang = '$ses_lang'");								
			$req_comp_invt_in_own_chit_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_comp_invt_in_own_chit_acc_type' and account_name='Companies Investment In Own Chits' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
			if ($req_comp_invt_in_own_chit_acc_id==0){
				$req_comp_invt_in_own_chit_acc_id=create_account('Companies Investment In Own Chits',$req_comp_invt_in_own_chit_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
			}
						
			$req_arrears_of_subn_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Arrears of Subscription' and lang = '$ses_lang'");								
			$req_arrears_of_subn_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_arrears_of_subn_acc_type' and account_name='Arrears of Subscription - ".$reqGroupNo."' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
			if ($req_arrears_of_subn_acc_id==0){
				$req_arrears_of_subn_acc_id=create_account('Arrears of Subscription - '.$reqGroupNo,$req_arrears_of_subn_acc_type,'groupdetails',$reqtxtGrp_Id,$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
			}	
						
			$req_foreman_dividend_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Indirect Incomes' and lang = '$ses_lang'");								
			$req_foreman_dividend_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_foreman_dividend_acc_type' and account_name='Foreman Dividend' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
			if ($req_foreman_dividend_acc_id==0){
				$req_foreman_dividend_acc_id=create_account('Foreman Dividend',$req_foreman_dividend_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
			}				
		
			// Divident Fee
			if ($reqLast_Did > 0){
				$related_id 	= FnGetValue("applicantdetails","AppID","GroupID = '$reqGroupID' and Ticketno = '$reqTicket_No'");		
				$Devident_acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_dividend_pay' and refered_id = '$reqSubscription_ID'");
				add_voucher('Journal_dividend_pay',$related_id,$reqSubscription_ID,$reqLast_Did,$reqAuction_Date,"Dividend Distribution IntNo: $reqInstNo, TR NO: CR, Reciept No : $reqrecipt_no",$reqmain_branch_id,$Devident_acc_trans_main_id);
		
				$com_foreman_div_acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_com_foreman_div' and refered_id = '$reqSubscription_ID'");
				add_voucher('Journal_com_foreman_div',$req_comp_invt_in_own_chit_acc_id,$reqSubscription_ID,$reqLast_Did,$reqAuction_Date,"Dividend Amt Transfer to Foreman Deividend IntNo: $reqInstNo, TR NO: CR, Reciept No : $reqrecipt_no",$reqmain_branch_id,$com_foreman_div_acc_trans_main_id,$req_foreman_dividend_acc_id);
			}
			
			$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Reciept' and refered_id = '$reqtrasaction_id'");		
			add_voucher('Reciept',$req_arrears_of_subn_acc_id,$reqtrasaction_id,$reqCurr_Inst,$reqAuction_Date,"Subscription TR NO: CR, Reciept No : $reqrecipt_no",$reqmain_branch_id,$acc_trans_main_id,$req_comp_invt_in_own_chit_acc_id);
		}	
	}
}

function fnprize_entry_payment($req_app_id,$req_amount,$reqtxtGrp_No,$req_pay_type,$req_txtCheque_No,$req_txtCheque_Date,$req_txtBank_Name,$req_txtService_Tax,$req_txtVerification_Charge,$req_txtAdjacement,$req_txtcomment,$reqtxtpayment_date,$edit,$Prize_payment_ID,$Prize_details_ID=''){
	$one_acc_for_group			= fngetvalue("tbl_mn_app_setting","field_value","field_name='one_acc_for_group'");
	$ses_lang 	= $_SESSION['opt_lang'];
	$sql 		= "select GroupNo,main_branch_id,prizeamount,Service_Tax,Verification_Charge,Adjacement,foreman_comm,GroupID,
						FullName 
				   from applicantdetails left join subdirdetails on subdirdetails.SubID = applicantdetails.SubID
				   where AppID = '$req_app_id'";
	$result 	= mysql_query($sql) or die($sql.mysql_error());
	while($row 	= mysql_fetch_array($result)){
		$reqGroupID  		= $row['GroupID'];
		$Chitvalue			= fngetvalue("groupdetails","ChitValue","GroupNo='".$row['GroupNo']."'");
		$reqtxtgroupbranch 	= $row['main_branch_id'];
		$foreman_comm		= ($row['foreman_comm']/100)*$Chitvalue;
		$reqtxtAmt			= $Chitvalue - $row['prizeamount'] - $req_txtService_Tax - $req_txtVerification_Charge - $req_txtAdjacement;
		$req_FullName		= $row["FullName"];		
	}
	if($req_txtCheque_Date != "") $req_txtCheque_Date = sqldatein($req_txtCheque_Date);

	$ins_sql2 = "Update 
					prize_details 
				SET 
					Service_Tax			= '$req_txtService_Tax',
					Verification_Charge	= '$req_txtVerification_Charge',
					Adjacement			= '$req_txtAdjacement',		
					adj_comment			= '$req_txtcomment'							
				where 
					Prize_details_ID 	= '$Prize_details_ID'";
	$res_sql  = mysql_query($ins_sql2) or die(mysql_error().$ins_sql2);
	//----insert adjacement details
	if ($req_txtAdjacement >0){	   
		$count_app		= Fngetcount('tbl_acc_adjustment'," Prize_details_ID ='$Prize_details_ID' and AppID='$req_app_id' and related_type='prized' ");
		if($count_app>0){ 
			$sql_adjace = " update 
								tbl_acc_adjustment 
							set 
								AdjustAmount	= '$req_txtAdjacement',
								Payment_date	= '$reqtxtpayment_date' 
							where 
								Prize_details_ID 	= '$Prize_details_ID' and 
								related_type	= 'prized'";
			mysql_query($sql_adjace) or die(mysql_error().$sql_adjace);									
			$adjace_id	= Fngetvalue('tbl_acc_adjustment',"adjustment_ID"," Prize_details_ID 	= '$Prize_details_ID' and related_type='prized' ");
		}else{	
			if($req_txtAdjacement>0){
				$sql_adjace	= "INSERT INTO `tbl_acc_adjustment` (`adjustment_ID`, `AppID`, `Prize_details_ID`, `related_type`,`related_id`, `Payment_date`, `AdjustAmount`, `paid`) 
							   VALUES (NULL,'$req_app_id','$Prize_details_ID','prized','$Prize_details_ID','$reqtxtpayment_date','$req_txtAdjacement','')";
				mysql_query($sql_adjace) or die(mysql_error().$sql_adjace);	
				$adjace_id	= mysql_insert_id();
			}
		}
	}	
	if ($edit==1){ 
		$updsql="	update 
						prize_payment 
					set 
						amount			= '$req_amount', 
						prizechequeno	= '$req_txtCheque_No', 
						prizebankname 	= '$req_txtBank_Name', 
					  	prizechequedate = '$req_txtCheque_Date', 
						prizepaymentby	= '$req_pay_type' , 
						payment_date	= '$reqtxtpayment_date'
	 			 	where 
						Prize_payment_ID= '$Prize_payment_ID'";
		$updresult = mysql_query($updsql)or die("Couldn't execute query.");
	
	
	}else{
		$ins_sql1 = "INSERT INTO `prize_payment` (`Prize_payment_ID`,`Prize_details_ID`,`amount`,`prizechequeno`, `prizebankname`, `prizechequedate`,prizepaymentby,payment_date) 
				VALUES 	(NULL,'$Prize_details_ID','$req_amount','$req_txtCheque_No','$req_txtBank_Name','$req_txtCheque_Date','$req_pay_type','$reqtxtpayment_date')";
		$res_sql  = mysql_query($ins_sql1) or die(mysql_error().$ins_sql1);	
		$Prize_payment_ID =mysql_insert_id();
	}
	//accounting related start  


	$req_prizeinstno 	= fnGetValue("prize_details","prizeinstno","Prize_details_ID 	= '$Prize_details_ID'"); 
	$req_Ticketno	 	= fnGetValue("prize_details","Ticketno","Prize_details_ID 	= '$Prize_details_ID'"); 

	//$is_prev_par_paid_cnt 	= fnGetCount("prize_payment","Prize_details_ID = '$Prize_details_ID' and Prize_payment_ID <> '$Prize_payment_ID' and payment_date < '$reqtxtpayment_date'"); 
	$min_Prize_payment_ID 		= fnGetValue("prize_payment","min(Prize_payment_ID)","Prize_details_ID = '$Prize_details_ID' "); 
	
	include_once( 'acc_include_functions.php');
	$finacial_yr_id				= fngetvalue("tbl_acc_financial_years","main_acc_fianancial_year_id"," main_branch_id='$reqtxtgroupbranch' and is_default='1' and lang='english'");
	$account_type_service_tax 	= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Duties & Taxes' and lang = '$ses_lang'"); 
	$account_type_ver_charge	= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Direct Incomes' and lang = '$ses_lang'"); 
	$acc_type					= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name ='Cash-in-hand' and lang ='$ses_lang'");
	$account_type_adjacement	= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name ='Sundry Creditors' and lang = '$ses_lang'"); 
	
	$req_adjacement_acc			= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang= '$ses_lang' and account_type ='$account_type_adjacement' and account_name='Adjacement' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");    
	$req_service_tax_acc		= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang= '$ses_lang' and account_type = '$account_type_service_tax' and account_name='Service Tax' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");    
	$req_ver_charge_acc   		= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$account_type_ver_charge' and account_name='Verification Charge' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
	$req_def_cash_in_hand 		= fnGetValue("tbl_acc_accounts","main_acc_account_id"," account_name='Cash' and lang ='$ses_lang' and account_type ='$acc_type' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
	$req_Sub_acc_id 			= fnGetValue("tbl_acc_accounts","main_acc_account_id","related_type = 'applicantdetails' and related_id = '".$req_app_id ."' and lang='$ses_lang' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
	$req_bank_acc 				= fnGetValue("tbl_acc_accounts,banks","main_acc_account_id","tbl_acc_accounts.related_id=banks.Main_BankID and related_type='bank' and banks.lang= '$ses_lang' and tbl_acc_accounts.lang= '$ses_lang' and related_type  = 'bank' and banks.Bankname='$req_txtBank_Name' and banks.main_branch_id='$reqtxtgroupbranch' and tbl_acc_accounts.main_branch_id='$reqtxtgroupbranch'  and acc_fianancial_year_id='$finacial_yr_id'");



	if ( $req_service_tax_acc == 0 ) 	$req_service_tax_acc	= create_account("Service Tax",$account_type_service_tax,"","",$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtgroupbranch);
	if ( $req_ver_charge_acc  == 0 )	$req_ver_charge_acc		= create_account('Verification Charge',$account_type_ver_charge,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtgroupbranch);
	if ( $req_adjacement_acc  == 0 )	$req_adjacement_acc		= create_account('Adjacement',$account_type_adjacement,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtgroupbranch);


	if ($one_acc_for_group==1){	
			$req_prize_money_payble_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Prize Money Payble' and lang = '$ses_lang'");								
			$req_prize_money_payble_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_prize_money_payble_acc_type' and account_name='Prize Money Payble - $reqtxtGrp_No' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
			if ($req_prize_money_payble_acc_id==0){
				$req_prize_money_payble_acc_id=create_account('Prize Money Payble - $reqtxtGrp_No',$req_prize_money_payble_acc_type,"groupdetails",$reqGroupID,$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtgroupbranch,$finacial_yr_id);
			}
			$to_accs = array();
			$i=0; $j=0;	
			if(($req_pay_type == 'cheque')||($req_pay_type == 'NEFT')||($req_pay_type == 'neft')){
				$to_accs[$i][$j++]=$req_bank_acc;		
				$to_accs[$i++][$j]=$req_amount;						
			}elseif($req_pay_type == 'cash'){
				$to_accs[$i][$j++]=$req_def_cash_in_hand;		
				$to_accs[$i++][$j]=$req_amount;										
			}
			if ($min_Prize_payment_ID==$Prize_payment_ID){
				if ($req_txtAdjacement > 0){
					$j=0;	
					$to_accs[$i][$j++]=$req_adjacement_acc;		
					$to_accs[$i++][$j]=$req_txtAdjacement;					
				}
				if ($req_txtService_Tax > 0){	
					$j=0;	
					$to_accs[$i][$j++]=$req_service_tax_acc;		
					$to_accs[$i++][$j]=$req_txtService_Tax;										
				}
				if ($req_txtVerification_Charge > 0){		
					$j=0;	
					$to_accs[$i][$j++]=$req_ver_charge_acc;		
					$to_accs[$i++][$j]=$req_txtVerification_Charge;										
				}
				$total_amt=$req_amount+$req_txtService_Tax+$req_txtVerification_Charge+$req_txtAdjacement;
			}else{
				$total_amt=$req_amount;
			}
			//payment
			if($req_pay_type == 'cheque'){
				$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Payment_chk' and refered_id = '$Prize_payment_ID'");
				add_voucher('Payment_chk',$req_prize_money_payble_acc_id,$Prize_payment_ID,$total_amt,$reqtxtpayment_date,"Prize Payment for $req_FullName Inst No: $req_prizeinstno Tktno: $req_Ticketno Cheque :".$req_txtCheque_No,$reqtxtgroupbranch,$acc_trans_main_id,$to_accs,$req_pay_type,$req_txtCheque_No,$req_txtCheque_Date,$req_txtBank_Name);
			}	
			if(($req_pay_type == 'NEFT')||($req_pay_type == 'neft')){
				$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'payment_neft' and refered_id = '$Prize_payment_ID'");
				add_voucher('payment_neft',$req_prize_money_payble_acc_id,$Prize_payment_ID,$total_amt,$reqtxtpayment_date,"Prize Payment for $req_FullName Inst No: $req_prizeinstno Tktno: $req_Ticketno NEFT",$reqtxtgroupbranch,$acc_trans_main_id,$to_accs,$req_pay_type,$req_txtCheque_No,$req_txtCheque_Date,$req_txtBank_Name);
			}	
			if($req_pay_type == 'cash'){
				$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Payment_cash' and refered_id = '$Prize_payment_ID'");
				add_voucher('Payment_cash',$req_prize_money_payble_acc_id,$Prize_payment_ID,$total_amt,$reqtxtpayment_date,"Prize Payment for $req_FullName Inst No: $req_prizeinstno Tktno: $req_Ticketno cash :".$req_txtCheque_No,$reqtxtgroupbranch,$acc_trans_main_id,$to_accs,$req_pay_type,$req_txtCheque_No,$req_txtCheque_Date,$req_txtBank_Name);
			}	
			if($req_pay_type == 'Adjustment'){
				$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'prize_adjace' and refered_id = '$Prize_payment_ID'");
				add_voucher('prize_adjace',$req_prize_money_payble_acc_id,$Prize_payment_ID,$total_amt,$reqtxtpayment_date,"Prize Payment for $req_FullName Inst No: $req_prizeinstno Tktno: $req_Ticketno Adjustment :".$req_txtCheque_No,$reqtxtgroupbranch,$acc_trans_main_id,$to_accs);
			}	

	}else{
		//service tax  
		$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Payment_service_tax' and refered_id = '$Prize_details_ID'");
		if($req_txtService_Tax == 0 && $acc_trans_main_id != 0){
			delete_voucher($acc_trans_main_id);								
		}
		if ($req_txtService_Tax > 0){	
			add_voucher('Payment_service_tax',$req_Sub_acc_id,$Prize_details_ID,$req_txtService_Tax,$reqtxtpayment_date,"Prize Payment Service tax",$reqtxtgroupbranch,$acc_trans_main_id,$req_service_tax_acc,$req_pay_type,$req_txtCheque_No,$req_txtCheque_Date,$req_txtBank_Name);
		}
		//verification charge
		$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Payment_verification_charge' and refered_id ='$Prize_details_ID'");
		if($req_txtVerification_Charge == 0 && $acc_trans_main_id != 0){
			delete_voucher($acc_trans_main_id);						
		}
		if ($req_txtVerification_Charge > 0){		
			add_voucher('Payment_verification_charge',$req_Sub_acc_id,$Prize_details_ID,$req_txtVerification_Charge,$reqtxtpayment_date,"Prize Payment verification charge",$reqtxtgroupbranch,$acc_trans_main_id,$req_ver_charge_acc,$req_pay_type,$req_txtCheque_No,$req_txtCheque_Date,$req_txtBank_Name);
		}
		//adjacement
		if ($req_adjacement_acc == 0){ 
				$req_adjacement_acc=create_account("Adjacement",$account_type_adjacement,"","",$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtgroupbranch);
		}
		if ($req_txtAdjacement > 0){	    
			$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type ='prize_adjace' and refered_id = '$adjace_id'");
			add_voucher('prize_adjace',$req_Sub_acc_id,$adjace_id,$req_txtAdjacement,$reqtxtpayment_date,"Prize Adjacement",$reqtxtgroupbranch,$acc_trans_main_id,$req_adjacement_acc);
		}
		//payment
		$acc_pay_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type in ('Payment_chk','payment_neft','Payment_cash') and refered_id = '$Prize_payment_ID'");

		if($req_pay_type == 'cheque'){
			add_voucher('Payment_chk',$req_Sub_acc_id,$Prize_payment_ID,$req_amount,$reqtxtpayment_date,"Prize Payment Cheque :".$req_txtCheque_No,$reqtxtgroupbranch,$acc_pay_trans_main_id,$req_bank_acc,$req_pay_type,$req_txtCheque_No,$req_txtCheque_Date,$req_txtBank_Name);
		}	
		if(($req_pay_type == 'NEFT')||($req_pay_type == 'neft')){
			add_voucher('payment_neft',$req_Sub_acc_id,$Prize_payment_ID,$req_amount,$reqtxtpayment_date,"Prize Payment NEFT",$reqtxtgroupbranch,$acc_pay_trans_main_id,$req_bank_acc,$req_pay_type,$req_txtCheque_No,$req_txtCheque_Date,$req_txtBank_Name);
		}	
		if($req_pay_type == 'cash'){
			add_voucher('Payment_cash',$req_Sub_acc_id,$Prize_payment_ID,$req_amount,$reqtxtpayment_date,"Prize Payment cash :".$req_txtCheque_No,$reqtxtgroupbranch,$acc_pay_trans_main_id,$req_def_cash_in_hand,$req_pay_type,$req_txtCheque_No,$req_txtCheque_Date,$req_txtBank_Name);
		}
		//accounting related end  
	}
}
function fnprize_entry_payment_baniyan($Values){	
	$req_app_id=$Values[0];
	$req_amount=$Values[1];
	$grp_no=$Values[2];
	$req_pay_type=$Values[3];
	$req_txtCheque_No=$Values[4];
	$req_txtCheque_Date=$Values[5];
	$req_txtBank_Name=$Values[6];
	$req_txtService_Tax=$Values[7];
	$req_txtVerification_Charge=$Values[8];
	$req_txtAdjacement=$Values[9];
	$req_txtcomment=$Values[10];
	$reqtxtpayment_date=$Values[11];
	$edit=$Values[12];
	$Prize_payment_ID=$Values[13];
	$Prize_details_ID=$Values[14];
	$req_txtFullName=$Values[15];
	$req_txtpayment_interest=$Values[16];

	$ses_lang 	= $_SESSION['opt_lang'];
	$Chitvalue			= fngetvalue("groupdetails","ChitValue","GroupNo='".$grp_no."'");	
	$reqtxtgroupbranch	= fngetvalue("groupdetails","main_branch_id","GroupNo='".$grp_no."'");			
	if($req_txtCheque_Date != "") $req_txtCheque_Date = sqldatein($req_txtCheque_Date);

	$ins_sql2 = "Update prize_details 
				SET Service_Tax			= '$req_txtService_Tax',					Verification_Charge	= '$req_txtVerification_Charge',
					Adjacement			= '$req_txtAdjacement',						payment_interest	= '$req_txtpayment_interest',		
					baniyan_pay_ticket 	= '$req_txtFullName',						adj_comment			= '$req_txtcomment'							
				where Prize_details_ID 	= '$Prize_details_ID'";
	$res_sql  = mysql_query($ins_sql2) or die(mysql_error().$ins_sql2);
	
	if ($edit==1){ 
		$updsql="	update prize_payment 
					set amount			= '$req_amount', 						prizechequeno	= '$req_txtCheque_No', 
						prizebankname 	= '$req_txtBank_Name',				  	prizechequedate = '$req_txtCheque_Date', 
						prizepaymentby	= '$req_pay_type' , 					payment_date	= '$reqtxtpayment_date'
	 			 	where Prize_payment_ID= '$Prize_payment_ID'";
		$updresult = mysql_query($updsql)or die("Couldn't execute query.");
	
	}else{
		$ins_sql1 = "INSERT INTO `prize_payment` (`Prize_payment_ID`,`Prize_details_ID`,`amount`,`prizechequeno`, `prizebankname`, `prizechequedate`,prizepaymentby,payment_date) 
				VALUES 	(NULL,'$Prize_details_ID','$req_amount','$req_txtCheque_No','$req_txtBank_Name','$req_txtCheque_Date','$req_pay_type','$reqtxtpayment_date')";
		$res_sql  = mysql_query($ins_sql1) or die(mysql_error().$ins_sql1);	
		$Prize_payment_ID =mysql_insert_id();
	}

	//accounting related start  
	
	include_once( 'acc_include_functions.php');
	$finacial_yr_id				= fngetvalue("tbl_acc_financial_years","main_acc_fianancial_year_id"," main_branch_id='$reqtxtgroupbranch' and is_default='1' and lang='english'");
	$account_type_service_tax 	= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Duties & Taxes' and lang = '$ses_lang'"); 
	$account_type_ver_charge	= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Direct Incomes' and lang = '$ses_lang'"); 
	$account_type_payment_interest	= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Direct Expenses' and lang = '$ses_lang'"); 	

	$acc_type					= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name ='Cash-in-hand' and lang ='$ses_lang'");
	$account_type_adjacement	= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name ='Sundry Creditors' and lang = '$ses_lang'"); 
	
	$req_adjacement_acc			= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang= '$ses_lang' and account_type ='$account_type_adjacement' and account_name='Adjacement' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");    
	$req_service_tax_acc		= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang= '$ses_lang' and account_type = '$account_type_service_tax' and account_name='Service Tax' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");    
	$req_payment_interest_acc	= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang= '$ses_lang' and account_type = '$account_type_payment_interest' and account_name='Payment Intrest' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");    	
	$req_ver_charge_acc   		= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$account_type_ver_charge' and account_name='Verification Charge' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
	$req_def_cash_in_hand 		= fnGetValue("tbl_acc_accounts","main_acc_account_id"," account_name='Cash' and lang ='$ses_lang' and account_type ='$acc_type' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");

	
	$req_Sub_acc_id 			= fnGetValue("tbl_acc_accounts","main_acc_account_id","related_type = 'applicantdetails' and related_id = '".$req_app_id ."' and lang='$ses_lang' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");

	$req_bank_acc 				= fnGetValue("tbl_acc_accounts,banks","main_acc_account_id","tbl_acc_accounts.related_id=banks.Main_BankID and related_type='bank' and banks.lang= '$ses_lang' and tbl_acc_accounts.lang= '$ses_lang' and related_type  = 'bank' and banks.Bankname='$req_txtBank_Name' and banks.main_branch_id='$reqtxtgroupbranch' and tbl_acc_accounts.main_branch_id='$reqtxtgroupbranch'");

	if ($req_service_tax_acc==0) 		$req_service_tax_acc	 = create_account("Service Tax",$account_type_service_tax,"","",$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtgroupbranch);
	if ($req_payment_interest_acc==0) 	$req_payment_interest_acc= create_account("Payment Intrest",$account_type_payment_interest,"","",$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtgroupbranch);	
	if ($req_ver_charge_acc==0)			$req_ver_charge_acc		 = create_account('Verification Charge',$account_type_ver_charge,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtgroupbranch);


	//service tax  
	$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Payment_service_tax' and refered_id = '$Prize_details_ID'");
	if($req_txtService_Tax == 0 && $acc_trans_main_id != 0){
		delete_voucher($acc_trans_main_id);								
	}
	if ($req_txtService_Tax > 0){	
		add_voucher('Payment_service_tax',$req_Sub_acc_id,$Prize_details_ID,$req_txtService_Tax,$reqtxtpayment_date,"Prize Payment Service tax",$reqtxtgroupbranch,$acc_trans_main_id,$req_service_tax_acc,$req_pay_type,$req_txtCheque_No,$req_txtCheque_Date,$req_txtBank_Name);
	}
	//verification charge
	$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Payment_verification_charge' and refered_id ='$Prize_details_ID'");
	if($req_txtVerification_Charge == 0 && $acc_trans_main_id != 0){
		delete_voucher($acc_trans_main_id);						
	}
	if ($req_txtVerification_Charge > 0){		
		add_voucher('Payment_verification_charge',$req_Sub_acc_id,$Prize_details_ID,$req_txtVerification_Charge,$reqtxtpayment_date,"Prize Payment verification charge",$reqtxtgroupbranch,$acc_trans_main_id,$req_ver_charge_acc,$req_pay_type,$req_txtCheque_No,$req_txtCheque_Date,$req_txtBank_Name);
	}

	//Payment Intrest  
	$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Payment_Interest' and refered_id = '$Prize_details_ID'");
	if($req_txtpayment_interest == 0 && $acc_trans_main_id != 0){
		delete_voucher($acc_trans_main_id);								
	}
	if ($req_txtpayment_interest > 0){	
		add_voucher('Payment_Interest',$req_payment_interest_acc,$Prize_details_ID,$req_txtpayment_interest,$reqtxtpayment_date,"Prize Payment Intrest",$reqtxtgroupbranch,$acc_trans_main_id,$req_Sub_acc_id,$req_pay_type,$req_txtCheque_No,$req_txtCheque_Date,$req_txtBank_Name);
	}
	
/*	//adjacement
	if ($req_adjacement_acc == 0){ 
			$req_adjacement_acc=create_account("Adjacement",$account_type_adjacement,"","",$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtgroupbranch);
	}
	if ($req_txtAdjacement > 0){	    
		$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type ='prize_adjace' and refered_id = '$adjace_id'");
		add_voucher('prize_adjace',$req_Sub_acc_id,$adjace_id,$req_txtAdjacement,$reqtxtpayment_date,"Prize Adjacement",$reqtxtgroupbranch,$acc_trans_main_id,$req_adjacement_acc);
	}
*/	//payment
	if($req_pay_type == 'cheque'){
		$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Payment_chk' and refered_id = '$Prize_payment_ID'");
		add_voucher('Payment_chk',$req_Sub_acc_id,$Prize_payment_ID,$req_amount,$reqtxtpayment_date,"Prize Payment Cheque :".$req_txtCheque_No,$reqtxtgroupbranch,$acc_trans_main_id,$req_bank_acc,$req_pay_type,$req_txtCheque_No,$req_txtCheque_Date,$req_txtBank_Name);
	}	
	if($req_pay_type == 'cash'){
		$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Payment_cash' and refered_id = '$Prize_payment_ID'");
		add_voucher('Payment_cash',$req_Sub_acc_id,$Prize_payment_ID,$req_amount,$reqtxtpayment_date,"Prize Payment cash :".$req_txtCheque_No,$reqtxtgroupbranch,$acc_trans_main_id,$req_def_cash_in_hand,$req_pay_type,$req_txtCheque_No,$req_txtCheque_Date,$req_txtBank_Name);
	}
	//accounting related end  
}
function fnPayStatusUpdate($pay_receiept_no,$reqTotalRecd_Inst){
	if ($pay_receiept_no<>""){
		$val_arr= split("~", $pay_receiept_no);
		fnaddsubscription_online($val_arr[0],$val_arr[1],$val_arr[2],$val_arr[3],$reqTotalRecd_Inst);
	}
}
function fnUpdate_log($create_update,$table_name,$id){
	$reqpersonnel_id = $_SESSION['personnel_id'];
	$flag 			 = 0;
	if(($create_update == "Update")||($create_update == "update"))	$flag = 1;
	$sql 	= "INSERT INTO `tbl_update_log` (`user_id`,`table_name`,`id_feild`,`flag`,`flag_date_time`) VALUES ('$reqpersonnel_id','$table_name','$id','$flag',NOW())";
	$result = mysql_query($sql) or die(mysql_error().$sql);
}
function fnRe_Auction($Auction_ID, $reqtxtreason){
  //entry in cancelled auction table
   
   $auc_no = fngetvalue("canceled_auction","max(auc_no)","Auction_ID='$Auction_ID'");   
   $auc_no = $auc_no+1;
   $inrt_auct_sql="INSERT INTO `canceled_auction` (`Auction_ID`, `GroupNo`, `InstNo`, `Month`,`AuctionDateTime`, `LastDateofPayment`, `lasdevidentt`, `prizegiven`, `Subcription`, `bidding_status`, `biddingendtime`, `main_branch_id`, `reauction_reason`,`GroupID`, `auc_no`) 
			  	   SELECT `Auction_ID`, `GroupNo`, `InstNo`, `Month`, `AuctionDateTime`, `LastDateofPayment`, `lasdevidentt`, `prizegiven`, `Subcription`, `bidding_status`, `biddingendtime`, `main_branch_id`, '$reqtxtreason',`GroupID`,'$auc_no'
				   FROM auction WHERE Auction_ID='$Auction_ID'";
   $inrt_auct_result = mysql_query($inrt_auct_sql) or die(mysql_error().$inrt_auct_sql);													  
     
   $GroupNo = fngetvalue("auction","GroupNo","Auction_ID='$Auction_ID'");	 
   $GroupID = fngetvalue("auction","GroupID","Auction_ID='$Auction_ID'");	    
   $InstNo  = fngetvalue("auction","InstNo","Auction_ID='$Auction_ID'");	 
   $prize_det_sql = "SELECT * FROM prize_details WHERE GroupID='$GroupID' and prizeinstno='$InstNo'";
   $prize_det_sql_result = mysql_query($prize_det_sql) or die(mysql_error().$prize_det_sql);													  
   while($row = mysql_fetch_array($prize_det_sql_result)){
	   //entry in cancelled prize details table
	   $inrt_app_det_sql= "INSERT INTO `canceled_prize_details` (`Prize_details_ID`, `AppID`, `Ticketno`, `GroupNo`, `SubCodeNo`, `prizeamount`, 
		`prizemonth`, `auctiondate`, `installment`, `amount`, `prizeinstno`, `main_branch_id`, `chitvalue_share`, `Service_Tax`, 
		`Verification_Charge`, `Adjacement`,`GroupID`,`SubID`, `auc_no`)  
		SELECT `Prize_details_ID`, `AppID`, `Ticketno`, `GroupNo`, `SubCodeNo`, `prizeamount`, 
		`prizemonth`, `auctiondate`, `installment`, `amount`, `prizeinstno`, `main_branch_id`, `chitvalue_share`, `Service_Tax`, 
		`Verification_Charge`, `Adjacement`,`GroupID`,`SubID`,'$auc_no'
		FROM prize_details WHERE Prize_details_ID='".$row['Prize_details_ID']."'";
	   $inrt_app_det_result = mysql_query($inrt_app_det_sql) or die(mysql_error().$inrt_app_det_sql);													  
   }   
   
/*  //entry in cancelled auction bids table
   $inrt_auct_bids_sql="INSERT INTO `canceled_auction_bids` (`auction_bids_id`, `Appid`, `prizeamount`, `winflag`, `instno`, `bid_datetime`,`auc_no`) 
                        SELECT `auction_bids_id`, `Appid`, `prizeamount`, `winflag`, `instno`, `bid_datetime`,'$auc_no' FROM auction_bids WHERE AppID='$AppID'";
   $inrt_auct_bids_result = mysql_query($inrt_auct_bids_sql) or die(mysql_error().$inrt_auct_bids_sql);		*/											    

}	
function fnClear_Auction($Auction_ID){
	   $GroupNo = fngetvalue("auction","GroupNo","Auction_ID='$Auction_ID'");	 
	   $GroupID = fngetvalue("auction","GroupID","Auction_ID='$Auction_ID'");	 	   
	   $InstNo  = fngetvalue("auction","InstNo","Auction_ID='$Auction_ID'");	 
	   $AppID   = fngetvalue("prize_details","AppID"," GroupID='$GroupID' and prizeinstno='$InstNo' ");	 
	   //$FullName = FnGetValue("subdirdetails","FullName","SubID = '".$row['SubID']."'");
	   $main_branch_id= fngetvalue("auction","main_branch_id","Auction_ID='$Auction_ID'");

  //clear auction table
  		$sql_del = "update auction set AuctionDateTime=NULL, LastDateofPayment=NULL,prizegiven=0,bidding_status=NULL,biddingendtime=NULL WHERE Auction_ID= $Auction_ID";
		 mysql_query($sql_del)	or die(mysql_error()."<br>".$sql_del);

		$if_prize_first = fnchkif_prize_first($GroupID);//FnGetValue("`tbl_mn_app_setting`","field_value","field_name = 'if_prize_first'");
		if ($if_prize_first==1){ 	
			$NextInstNo = $InstNo + 1;
			$ins_sql = "Update auction SET lasdevidentt = '0' where GroupID = '$GroupID' and InstNo = '$NextInstNo'";
			$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);
		}else{
			$ins_sql = "Update auction SET lasdevidentt = '0'  where GroupID = '$GroupID' and InstNo = '$InstNo'";
			$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);
		}


/*  //clear applicant details table
		$update_previos_data = "Update applicantdetails SET prizeamount = '0',prizechequeno = '',prizebankname = '',prizechequedate = '0000-00-00',prizemonth = '',
								prizeinstno  = '0',installment='0',status='non prized'
				where AppID = '$AppID'"; 
		
		$previos_sql = mysql_query($update_previos_data) or die(mysql_error().$update_previos_data);	  
*/	

  //clear prize_details table
   		$sql_del_prize_det = "DELETE `prize_details`.* FROM `prize_details` WHERE GroupID='$GroupID' and prizeinstno='$InstNo'";
		$sql_del_prize_det_result = mysql_query($sql_del_prize_det)	or die(mysql_error()."<br>".$sql_del_prize_det); 
	
  //clear auction bids table
   		$sql_del_bid = "delete auction_bids.* from auction_bids,applicantdetails WHERE auction_bids.AppID=applicantdetails.AppID and applicantdetails.GroupID='$GroupID' and instno = '$InstNo'";
		$sql_del_result = mysql_query($sql_del_bid)	or die(mysql_error()."<br>".$sql_del_bid); 

  //Account type change
			$Group_acc_type_Liability_id = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = '".$GroupNo."' and account_type_nature = 'Liability' and lang='$ses_lang'");
			$rel_prev_acc_id = fnGetValue("tbl_acc_accounts","main_acc_account_id","related_id = '".$AppID ."' and lang = '$ses_lang' and main_branch_id='$main_branch_id' and acc_fianancial_year_id='".$_SESSION['financial_year'][$main_branch_id]."'");
			$upd_sql 	= "Update tbl_acc_accounts SET account_type = '".$Group_acc_type_Liability_id."' where main_acc_account_id = '$rel_prev_acc_id' ";
			$res_udp 	= mysql_query($upd_sql) or die(mysql_error().$upd_sql);		
			
			
	//-------------------delete Journal_foreman_commission,Journal_dividend,Journal_dividend----------
	if($AppID<>"" && $AppID<>0){
			$sql_delete="DELETE 
								tbl_acc_transaction.*,tbl_acc_transaction_main.*,`tbl_acc_relate`.* 
						FROM 
								`tbl_acc_transaction` inner join `tbl_acc_transaction_main`
						ON (`tbl_acc_transaction_main`.`acc_transaction_main_id` = `tbl_acc_transaction`.`acc_transaction_main_id`)
						INNER JOIN
								`tbl_acc_relate` 
						ON	 (`tbl_acc_relate`.`acc_transaction_id` = `tbl_acc_transaction_main`.`acc_transaction_main_id`)
	
						WHERE 
						 `tbl_acc_relate`.`acc_transaction_type` in ('Journal_dividend_pay','Journal_foreman_commission','Journal_dividend')  and `tbl_acc_relate`.refered_id='$AppID'";
						 
			 mysql_query($sql_delete) or die(mysql_error().$sql_delete);
	}	 		
	
	//----------------------------------------------------------------------------------------------------		
			
			
				

}
function fnremove_member($reqAppID,$reqremove_date,$reqremove_comment){	
	$ses_lang = $_SESSION['opt_lang'];
	if($ses_lang==''){	$ses_lang = 'english'; }
	
	$one_acc_for_group				= fngetvalue("tbl_mn_app_setting","field_value","field_name='one_acc_for_group'");	
	$rem_in_removed_sub_payble_acc	= fngetvalue("tbl_mn_app_setting","field_value","field_name='rem_in_removed_sub_payble_acc'");		

	//echo $rem_in_removed_sub_payble_acc;
	$appArr				= FngetValuemultiple("applicantdetails","GroupNo,Ticketno,main_branch_id,GroupID,SubCodeNo,SubID"," AppID= '$reqAppID'");
	$reqGroupNo			= $appArr[0]; 
	$reqTicketno 		= $appArr[1]; 		
	$reqmain_branch_id 	= $appArr[2]; 	
	$reqGroupID 		= $appArr[3]; 	
	$reqSubCodeNo 		= $appArr[4]; 	
	$reqSubID 			= $appArr[5]; 	
	

	$Insert_appdetails_sql= "INSERT INTO `applicantdetails_removed` 
	 	(`AppID`, `Ticketno`, `GroupNo`, `SubCodeNo`, `prizeamount`, 
	    `auctiondate`, `amtinword`, `prizeamtinword`, `installmentinword`,`installment`, `amount`, `Sustsubcode`, `remdate`, 
	    `chrettime`, `latetime`, `main_branch_id`, `foreman_comm`,`status`,`Remove_status`,
	    `Service_Tax`,`Verification_Charge`,`Adjacement`,`Netpayable_amount`,`remove_date`,`remove_comment`,`SubID`,`GroupID`,`chitvalue_share`) 
		
		SELECT `AppID`, `Ticketno`, `GroupNo`, `SubCodeNo`,`prizeamount`,
	   `auctiondate`, `amtinword`, `prizeamtinword`, `installmentinword`,`installment`, `amount`, `Sustsubcode`,`remdate`, 
	   `chrettime`, `latetime`, `main_branch_id`, `foreman_comm`,`status`,'removed',
	   `Service_Tax`,`Verification_Charge`,`Adjacement`,`Netpayable_amount`,'$reqremove_date','$reqremove_comment',`SubID`,`GroupID`,`chitvalue_share`
    	FROM applicantdetails WHERE  AppID= '$reqAppID' ";
		
   	$Insert_details_sql_result = mysql_query($Insert_appdetails_sql) or die(mysql_error().$Insert_appdetails_sql);	
	$applicantdetails_removed_ID = mysql_insert_id();
	
	$Insert_enrollfee_sql = "INSERT INTO `enroll_payment_removed` (`enroll_payment_id`, `main_branch_id`, `ticket_no`, `Group_No`, `Sub_ID`, `transaction_ID`, `Amount`) 
							   SELECT NULL as `enroll_payment_id`,`main_branch_id`, `ticket_no`, `Group_No`, `Sub_ID`, `transaction_ID`, `Amount`
							   FROM enroll_payment WHERE Group_No='$reqGroupNo' and ticket_no = '$reqTicketno' and main_branch_id='$reqmain_branch_id'";
  	$Insert_enrollfee_sql_result = mysql_query($Insert_enrollfee_sql) or die(mysql_error().$Insert_enrollfee_sql);


	$Insert_subsdetails_sql = "INSERT INTO `subscription_details_removed` 
									(`Subscription_ID`,`Subscription_Act_ID`,`Auction_ID`,`TicketNo`,`Transaction_ID`,`SubID`,`LateFee`,`lasdevidentt`,`arrears`,`main_branch_id`,`Subcription`,`Subcription_Fee`,`other_charge`,`check_return_fee`,`transfer_fee`,`adv_refund_pay_id`,`refund_amount`,`ser_tax`)
							   SELECT
							    NULL as `Subscription_ID`,subscription_details.`Subscription_ID` as `Subscription_Act_ID`,`subscription_details`.`Auction_ID` as Auction_ID,`TicketNo`,`Transaction_ID`,`SubID`,`LateFee`,`subscription_details`.`lasdevidentt` as lasdevidentt,`arrears`,`subscription_details`.`main_branch_id` as `main_branch_id`,`subscription_details`.`Subcription` as Subcription,`subscription_details`.`Subcription_Fee` as `Subcription_Fee`,`subscription_details`.`other_charge` as `other_charge`,`check_return_fee`,`transfer_fee`,`adv_refund_pay_id`,`refund_amount`,`ser_tax`
							   FROM (subscription_details Inner Join auction ON subscription_details.Auction_ID = auction.Auction_ID) WHERE auction.GroupNo='$reqGroupNo' and Ticketno = $reqTicketno";
  	$Insert_subsdetails_sql_result = mysql_query($Insert_subsdetails_sql) or die(mysql_error().$Insert_subsdetails_sql);

	$Trans_ids			= fngetvalue("(subscription_details Inner Join auction ON subscription_details.Auction_ID = auction.Auction_ID Inner Join transaction ON subscription_details.Transaction_ID = transaction.Transaction_ID)","group_concat(DISTINCT subscription_details.Transaction_ID)","auction.GroupNo='$reqGroupNo' and Ticketno = $reqTicketno");
    
	$advance_trans_ids	= Fngetvalue("tbl_advance_payment","group_concat(Transaction_ID)","Group_No='$reqGroupNo' and Ticket_No='$reqTicketno' and main_branch_id='$reqmain_branch_id'");
	
	
	if ((($Trans_ids == "" )|| ($Trans_ids == 0)) &&(($advance_trans_ids <> "" ) && ($advance_trans_ids <> 0))) {
		 $Trans_ids = $advance_trans_ids;
	}elseif (($advance_trans_ids <> "" ) && ($advance_trans_ids <> 0)){
		$Trans_ids = $Trans_ids.",".$advance_trans_ids;
	}
	//echo $Trans_ids;
	if (($Trans_ids != "" ) && ($Trans_ids != 0)){
		$insert_trans_details_sql = "INSERT INTO `transaction_removed` (`Transaction_ID`,`Transaction_ID_Act`,`ReceiptNo`,`Reciept_Date`,`TRNO`,`TRDate`,`Amount`,`printreciept`,`Area`,`DayBookNo`,`main_branch_id`,`receipt_type`,`payment_by`,`status`,`receipt_enter_by`,`collection_code`,`receipt_flag`,`dep_main_branch_id`)
									SELECT NULL as `Transaction_ID`, transaction.`Transaction_ID` as `Transaction_ID_Act`,`transaction`.`ReceiptNo` as ReceiptNo,`Reciept_Date`,`TRNO`,`TRDate`,`Amount`,`printreciept`,`Area`,`DayBookNo`,`transaction`.`main_branch_id` as main_branch_id,`receipt_type`,`payment_by`,`transaction`.`status` as `status`,`transaction`.`receipt_enter_by` as `receipt_enter_by`,`transaction`.`collection_code` as `collection_code`,`receipt_flag`,`dep_main_branch_id`
									FROM transaction
									WHERE  Transaction_ID in (
										select subscription_details.Transaction_ID 
										from subscription_details Inner Join auction ON subscription_details.Auction_ID = auction.Auction_ID 
											  Inner Join transaction ON subscription_details.Transaction_ID = transaction.Transaction_ID 
									    where auction.GroupNo='$reqGroupNo' and Ticketno = $reqTicketno)";							
		mysql_query($insert_trans_details_sql) or die(mysql_error().$insert_trans_details_sql);			
					
		
		$insert_transdetails_sql = "INSERT INTO `transaction_details_removed` (`Transaction_detail_ID`,`Amount`,`Remark`,`ChequeNo`,`Bankname`,`ChequeDate`,`CheRetTime`,`Cheretfee`,`receipt_type`,`payment_by`,`trans_id`,`status`,`dep_slip_ID`,`main_branch_id`,`value_chequereturn_date`,`value_comment`,`adjustment_ID`,`neft_bank`)
									SELECT 	NULL as `Transaction_detail_ID`,`transaction_details`.`Amount` as Amount,`transaction_details`.`Remark` as Remark,`transaction_details`.`ChequeNo` as ChequeNo,`transaction_details`.`Bankname` as Bankname,`transaction_details`.`ChequeDate` as ChequeDate,`transaction_details`.`CheRetTime` as CheRetTime,`transaction_details`.`Cheretfee` as Cheretfee,`transaction_details`.`receipt_type` as receipt_type,`transaction_details`.`payment_by` as payment_by,`transaction_details`.`trans_id` as trans_id,`transaction_details`.`status` as status,`transaction_details`.`dep_slip_ID` as dep_slip_ID,`transaction_details`.`main_branch_id` as main_branch_id
									,`value_chequereturn_date`,`value_comment`,`adjustment_ID`,`neft_bank`
									FROM  transaction_details
									WHERE trans_id in (
										select subscription_details.Transaction_ID 
										from subscription_details Inner Join auction ON subscription_details.Auction_ID = auction.Auction_ID 
											  Inner Join transaction ON subscription_details.Transaction_ID = transaction.Transaction_ID 
									    where auction.GroupNo='$reqGroupNo' and Ticketno = $reqTicketno)";
		mysql_query($insert_transdetails_sql) or die(mysql_error().$insert_transdetails_sql);						

		//For Advance reciepts
		$insert_trans_details_sql = "INSERT INTO `transaction_removed` (`Transaction_ID`,`Transaction_ID_Act`,`ReceiptNo`,`Reciept_Date`,`TRNO`,`TRDate`,`Amount`,`printreciept`,`Area`,`DayBookNo`,`main_branch_id`,`receipt_type`,`payment_by`,`status`,`receipt_enter_by`,`collection_code`,`receipt_flag`,`dep_main_branch_id`)
									SELECT NULL as `Transaction_ID`, transaction.`Transaction_ID` as `Transaction_ID_Act`,`transaction`.`ReceiptNo` as ReceiptNo,`Reciept_Date`,`TRNO`,`TRDate`,`Amount`,`printreciept`,`Area`,`DayBookNo`,`transaction`.`main_branch_id` as main_branch_id,`receipt_type`,`payment_by`,`transaction`.`status` as `status`,`transaction`.`receipt_enter_by` as `receipt_enter_by`,`transaction`.`collection_code` as `collection_code`,`receipt_flag`,`dep_main_branch_id`
									FROM transaction
									WHERE  Transaction_ID in (
										select tbl_advance_payment.Transaction_ID 
										from tbl_advance_payment
									    where tbl_advance_payment.GroupID='$reqGroupID' and tbl_advance_payment.Ticket_No = $reqTicketno)";							
		mysql_query($insert_trans_details_sql) or die(mysql_error().$insert_trans_details_sql);			
					
		
		$insert_transdetails_sql = "INSERT INTO `transaction_details_removed` (`Transaction_detail_ID`,`Amount`,`Remark`,`ChequeNo`,`Bankname`,`ChequeDate`,`CheRetTime`,`Cheretfee`,`receipt_type`,`payment_by`,`trans_id`,`status`,`dep_slip_ID`,`main_branch_id`,`value_chequereturn_date`,`value_comment`,`adjustment_ID`,`neft_bank`)
									SELECT 	NULL as `Transaction_detail_ID`,`transaction_details`.`Amount` as Amount,`transaction_details`.`Remark` as Remark,`transaction_details`.`ChequeNo` as ChequeNo,`transaction_details`.`Bankname` as Bankname,`transaction_details`.`ChequeDate` as ChequeDate,`transaction_details`.`CheRetTime` as CheRetTime,`transaction_details`.`Cheretfee` as Cheretfee,`transaction_details`.`receipt_type` as receipt_type,`transaction_details`.`payment_by` as payment_by,`transaction_details`.`trans_id` as trans_id,`transaction_details`.`status` as status,`transaction_details`.`dep_slip_ID` as dep_slip_ID,`transaction_details`.`main_branch_id` as main_branch_id
									,`value_chequereturn_date`,`value_comment`,`adjustment_ID`,`neft_bank`
									FROM  transaction_details
									WHERE trans_id in (
										select tbl_advance_payment.Transaction_ID 
										from tbl_advance_payment
									    where tbl_advance_payment.GroupID='$reqGroupID' and tbl_advance_payment.Ticket_No = $reqTicketno)";
		mysql_query($insert_transdetails_sql) or die(mysql_error().$insert_transdetails_sql);						

	}

	$advance_ids	= Fngetvalue("tbl_advance_payment","group_concat(Advance_ID)","Group_No='$reqGroupNo' and Ticket_No='$reqTicketno' and main_branch_id='$reqmain_branch_id'");
	
	if (($advance_ids <> "" )&& ($advance_ids <> 0)){
		$insert_advance_removed	= "
					INSERT INTO `tbl_advance_removed` ( `Advance_ID`,`Group_No`,`Ticket_No`,`Transaction_ID`,`SubID`,`main_branch_id`,`Advance_Amount`,
						LateFee,Subcription_Fee,other_charge,check_return_fee,transfer_fee)
					SELECT 	`Advance_ID`as Advance_ID,`Group_No`as Group_No,`Ticket_No` as Ticket_No,`Transaction_ID`as Transaction_ID,`SubID` as SubID,
						`main_branch_id` as main_branch_id,`Advance_Amount` as Advance_Amount,LateFee as LateFee,Subcription_Fee as Subcription_Fee,other_charge as other_charge,
						check_return_fee as check_return_fee,transfer_fee as transfer_fee
					FROM tbl_advance_payment
					WHERE Advance_ID in ($advance_ids)";
		mysql_query($insert_advance_removed) or die(mysql_error().$insert_advance_removed);	
			
		//$delete_advance_payment	= "	delete from tbl_advance_payment where Advance_ID in ($advance_ids)";
		//mysql_query($delete_advance_payment) or die(mysql_error().$delete_advance_payment);	
	}
	$sqlDelete 	= "	delete subscription_details.*,	transaction.*,	transaction_details.* 
				 	from ((((applicantdetails Inner Join subscription_details ON applicantdetails.SubID=subscription_details.SubID AND applicantdetails.Ticketno = $reqTicketno									)
						 	Inner Join auction ON subscription_details.Auction_ID = auction.Auction_ID) 	
						 	Inner Join transaction ON subscription_details.Transaction_ID=transaction.Transaction_ID)
						 	Inner Join  transaction_details ON transaction.Transaction_ID = transaction_details.trans_id)
			 	 	WHERE 
						AppID 						= '$reqAppID' and 	auction.GroupNo 				= '$reqGroupNo' and 
						applicantdetails.Ticketno 	= $reqTicketno AND 	subscription_details.Ticketno 	= $reqTicketno ";
		
	mysql_query($sqlDelete) or die(mysql_error().$sqlDelete);

	$sqlDelete 	= "	delete tbl_advance_payment.*,	transaction.*,	transaction_details.* 
				 	from (( tbl_advance_payment 	
						 	Inner Join transaction ON tbl_advance_payment.Transaction_ID=transaction.Transaction_ID)
						 	Inner Join  transaction_details ON transaction.Transaction_ID = transaction_details.trans_id)
			 	 	WHERE 
						tbl_advance_payment.Group_No = '$reqGroupNo' AND tbl_advance_payment.Ticket_No 	= $reqTicketno ";
		
	mysql_query($sqlDelete) or die(mysql_error().$sqlDelete);

	$sqlDelete 	= "	delete 	enroll_payment.*, transaction.*, transaction_details.* 
				 	from enroll_payment 
						Inner Join transaction ON enroll_payment.transaction_ID = transaction.Transaction_ID
				 	 	Inner Join transaction_details ON transaction.Transaction_ID = transaction_details.trans_id
			 	 	WHERE enroll_payment.Group_No = '$reqGroupNo' and enroll_payment.ticket_no = '$reqTicketno' and 
						enroll_payment.main_branch_id = '$reqmain_branch_id' ";
		
	mysql_query($sqlDelete) or die(mysql_error().$sqlDelete);

	$sqlins	 	= "	UPDATE applicantdetails SET Remove_status 	= 'Removed' 
					WHERE AppID	= $reqAppID ";							
	mysql_query($sqlins) or die(mysql_error().$sqlins);	
	
	//$SubID		= Fngetvalue("applicantdetails","SubCodeNo","AppID = '$reqAppID'"); 
	$intro_sql	= "	update tbl_introducer_payment_details  set status = 'Removed' 
					where `GroupNo`	= '$reqGroupNo' and `Ticket_No`	= '$reqTicketno' and `main_branch_id`= '$reqmain_branch_id' and `subcode` = '$reqSubCodeNo' ";		
	mysql_query($intro_sql)	or die(mysql_error().$intro_sql);	
	
	if ($one_acc_for_group==1){
		include_once( 'acc_include_functions.php');
		$finacial_yr_id	= fngetvalue("tbl_acc_financial_years","main_acc_fianancial_year_id"," main_branch_id='".$reqmain_branch_id."' and is_default='1' and lang='english'");
		
		$req_arrears_of_subn_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Arrears of Subscription' and lang = '$ses_lang'");								
		$req_arrears_of_subn_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_arrears_of_subn_acc_type' and account_name='Arrears of Subscription - ".$reqGroupNo."' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_arrears_of_subn_acc_id==0){
			$req_arrears_of_subn_acc_id=create_account('Arrears of Subscription - '.$reqGroupNo,$req_arrears_of_subn_acc_type,'groupdetails',$reqGroupID,$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}	
		$req_dividend_payble_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Dividend Payable' and lang = '$ses_lang'");								
		$req_dividend_payble_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_dividend_payble_acc_type' and account_name='Dividend Payable - ".$reqGroupNo."' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_dividend_payble_acc_id==0){
			$req_dividend_payble_acc_id=create_account('Dividend Payable - '.$reqGroupNo,$req_dividend_payble_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}
		$req_comm_of_canc_chit_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Indirect Incomes' and lang = '$ses_lang'");								
		$req_comm_of_canc_chit_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_comm_of_canc_chit_acc_type' and account_name='Commission of Cancelled Chit' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_comm_of_canc_chit_acc_id==0){
			$req_comm_of_canc_chit_acc_id=create_account('Commission of Cancelled Chit',$req_comm_of_canc_chit_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}
					
		$req_removed_sub_payble_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Current Liabilities' and lang = '$ses_lang'");								
		$req_removed_sub_payble_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_removed_sub_payble_acc_type' and account_name='Removed Subscription Payable' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_removed_sub_payble_acc_id==0){
			$req_removed_sub_payble_acc_id=create_account('Removed Subscription Payable',$req_removed_sub_payble_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}
		$req_bank_charges_received_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Direct Incomes' and lang = '$ses_lang'");								
		$req_bank_charges_received_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_bank_charges_received_acc_type' and account_name='Bank Charges Received' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_bank_charges_received_acc_id==0){
			$req_bank_charges_received_acc_id=create_account('Bank Charges Received',$req_bank_charges_received_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}
		$req_forfeited_subscription_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Direct Incomes' and lang = '$ses_lang'");								
		$req_forfeited_subscription_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_forfeited_subscription_acc_type' and account_name='Forfeited Subscription' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_forfeited_subscription_acc_id==0){
			$req_forfeited_subscription_acc_id=create_account('Forfeited Subscription',$req_forfeited_subscription_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}
		
		$req_paid =FnGetValueMultiple("subscription_details_removed  LEFT JOIN auction  ON (auction.Auction_ID = subscription_details_removed.Auction_ID)  INNER JOIN transaction_removed ON ( subscription_details_removed.Transaction_ID = transaction_removed.Transaction_ID_Act )",
			"sum( subscription_details_removed.Subcription),sum( subscription_details_removed.lasdevidentt),sum(subscription_details_removed.Subcription_Fee),sum(subscription_details_removed.other_charge),sum(subscription_details_removed.check_return_fee),sum(subscription_details_removed.transfer_fee),sum(subscription_details_removed.refund_amount),sum(subscription_details_removed.ser_tax)",
			" subscription_details_removed.SubID='$reqSubID' and transaction_removed.status<2 and auction.main_branch_id ='$reqmain_branch_id' and transaction_removed.main_branch_id ='$reqmain_branch_id'  and auction.GroupNo = '$reqGroupNo'  and subscription_details_removed.Ticketno='$reqTicketno'");
		$advance_paid=Fngetvalue("tbl_advance_removed inner join `transaction_removed` on `transaction_removed`.Transaction_ID_Act=tbl_advance_removed.Transaction_ID ","sum(Advance_Amount)","Group_No='$reqGroupNo' and Ticket_No='$reqTicketno' and tbl_advance_removed.main_branch_id='$reqmain_branch_id' and status<2");
	    //$row['subpaid']=$req_paid[0]+$advance_paid;
		$reqsubpaid =($req_paid[0]+$advance_paid+$req_paid[1])-$req_paid[2]-$req_paid[3]-$req_paid[4]-$req_paid[5]-$req_paid[6]-$req_paid[7];			
		
		$reqGroupDet=fngetvaluemultiple("groupdetails","ChitValue,Duration","GroupNo='".$reqGroupNo."'");		
		$reqforemancomper=5;
		$reqChitValue=$reqGroupDet[0];		
		$reqforeman_comm=($reqforemancomper/100)*$reqChitValue;	
		$req_removed_sub_payble=$reqsubpaid-$req_paid[1];//-$reqforeman_comm;
	
		$i=0;
		if ($req_paid[1] > 0 ){ 
			$reqTo_Acc_ID[$i][0]= $req_dividend_payble_acc_id;
			$reqTo_Acc_ID[$i][1]= $req_paid[1];
			$i++;
		}
/*		if ($reqsubpaid <= $reqforeman_comm){	
			$reqTo_Acc_ID[$i][0]= $req_forfeited_subscription_acc_id;
			$reqTo_Acc_ID[$i][1]= $reqsubpaid-$req_paid[1];
		}else{
			$reqTo_Acc_ID[$i][0]= $req_comm_of_canc_chit_acc_id;
			$reqTo_Acc_ID[$i][1]= $reqforeman_comm;
			$i++;
			$reqTo_Acc_ID[$i][0]= $req_removed_sub_payble_acc_id;
			$reqTo_Acc_ID[$i][1]= $req_removed_sub_payble;
		}*/
		
		$reqTo_Acc_ID[$i][0]= $req_removed_sub_payble_acc_id;
		$reqTo_Acc_ID[$i][1]= $req_removed_sub_payble;
					
		$acc_trans_main_id 		= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_rem_mem' and refered_id = '$applicantdetails_removed_ID'");
		add_voucher('Journal_rem_mem',$req_arrears_of_subn_acc_id,$applicantdetails_removed_ID,$reqsubpaid,$reqremove_date,"Remove Member",$reqmain_branch_id,$acc_trans_main_id,$reqTo_Acc_ID);
	
	}else{
		include_once( 'acc_include_functions.php');

		$finacial_yr_id				= fngetvalue('tbl_acc_financial_years','main_acc_fianancial_year_id'," main_branch_id='$reqmain_branch_id' and is_default='1' and lang='english'");
		$Group_Liability_type_id 	= fnGetValue('tbl_acc_account_type','main_account_type_id',"account_type_name = '".$reqGroupNo."' and account_type_nature = 'Liability' and lang='$ses_lang'");		

		$req_removed_sub_payble_acc_type 	= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Removed Subscription Payable' and lang = '$ses_lang'");								

		if ($rem_in_removed_sub_payble_acc==1){
			$removed_acc_type = $req_removed_sub_payble_acc_type;
		}else{
			$removed_acc_type = $Group_Liability_type_id;
		}
		
		$req_cur_acc_id 			= fnGetValue('tbl_acc_accounts','main_acc_account_id',"related_type = 'applicantdetails' and related_id = '".$reqAppID ."' and lang='$ses_lang' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");

		$upd_sql 			= "Update tbl_acc_accounts SET account_type = '".$removed_acc_type."' where main_acc_account_id = '$req_cur_acc_id' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'";
		$res_udp 			= mysql_query($upd_sql) or die(mysql_error().$upd_sql);			
		fnGenerateDividentReturnVoucher($applicantdetails_removed_ID);
	}
	
}
//---------Indirect transfer member--------------------------------------------------------------------------
function fnIndirect_transfer_member($reqAppID,$reqToSubID,$reqremove_date,$reqremove_comment){
	$ses_lang    = $_SESSION['opt_lang'];	

	$one_acc_for_group	= fngetvalue("tbl_mn_app_setting","field_value","field_name='one_acc_for_group'");		
	$appArr				= FngetValuemultiple("applicantdetails","GroupNo,Ticketno,main_branch_id,GroupID,SubCodeNo,SubID"," AppID= '$reqAppID'");	
	$reqGroupNo 		= $appArr[0]; 
	$reqTicketno 		= $appArr[1];	
	$reqmain_branch_id 	= $appArr[2]; 		
	$reqGroupID 		= $appArr[3]; 	
	$reqSubCodeNo 		= $appArr[4]; 	
	$reqFromSubID 		= $appArr[5]; 	
	
	$Insert_appdetails_sql= "INSERT INTO `applicantdetails_removed` (`AppID`,`Ticketno`, `GroupNo`, `SubCodeNo`,`prizeamount`, 
	    `auctiondate`, `amtinword`, `prizeamtinword`, `installmentinword`,`installment`, `amount`, `Sustsubcode`, `remdate`, `prizeinstno`, 
	   `chrettime`, `latetime`, `main_branch_id`, `foreman_comm`,`status`,`Remove_status`,
	   `Service_Tax`,`Verification_Charge`,`Adjacement`,`Netpayable_amount`,`remove_date`,`remove_comment`,`SubID`,`GroupID`,`chitvalue_share`) 
		
		SELECT `AppID`,`Ticketno`, `GroupNo`, `SubCodeNo`,`prizeamount`, 
	   `auctiondate`,`amtinword`, `prizeamtinword`, `installmentinword`,`installment`, `amount`, `Sustsubcode`, `remdate`, `prizeinstno`, 
	   `chrettime`, `latetime`, `main_branch_id`, `foreman_comm`,`status`,'transfered_i',
	   `Service_Tax`,`Verification_Charge`,`Adjacement`,`Netpayable_amount`,'$reqremove_date','$reqremove_comment',`SubID`,`GroupID`,`chitvalue_share`
    	FROM applicantdetails WHERE AppID='$reqAppID' ";
   	$Insert_details_sql_result = mysql_query($Insert_appdetails_sql) or die(mysql_error().$Insert_appdetails_sql);	
	$applicantdetails_removed_ID = mysql_insert_id();
	
	$Insert_enrollfee_sql = "INSERT INTO `enroll_payment_removed` (`enroll_payment_id`, `main_branch_id`, `ticket_no`, `Group_No`, `Sub_ID`, `transaction_ID`, `Amount`) 
							   SELECT NULL as `enroll_payment_id`,`main_branch_id`, `ticket_no`, `Group_No`,`Sub_ID`, `transaction_ID`, `Amount`
							   FROM enroll_payment WHERE Group_No='$reqGroupNo' and ticket_no = '$reqTicketno' and main_branch_id='$reqmain_branch_id'";
  	$Insert_enrollfee_sql_result = mysql_query($Insert_enrollfee_sql) or die(mysql_error().$Insert_enrollfee_sql);

	$Insert_subsdetails_sql = "INSERT INTO `subscription_details_removed` (`Subscription_Act_ID`,`Auction_ID`,`TicketNo`,`Transaction_ID`,`SubID`,`LateFee`,`lasdevidentt`,`arrears`,`main_branch_id`,`Subcription`,`Subcription_Fee`,`other_charge`,`check_return_fee`,`transfer_fee`,`refund_amount`,`ser_tax`)
							   SELECT  subscription_details.Subscription_ID as `Subscription_Act_ID`,`subscription_details`.`Auction_ID` as Auction_ID,`TicketNo`,`Transaction_ID`,`SubID`,`LateFee`,`subscription_details`.`lasdevidentt` as lasdevidentt,`arrears`,`subscription_details`.`main_branch_id` as main_branch_id,`subscription_details`.`Subcription` as Subcription,`subscription_details`.`Subcription_Fee` as Subcription_Fee,`other_charge`,`check_return_fee`,`transfer_fee`,`refund_amount`,`ser_tax`
							   FROM (subscription_details Inner Join auction ON subscription_details.Auction_ID = auction.Auction_ID) WHERE auction.GroupNo='$reqGroupNo' and Ticketno = $reqTicketno";
  	$Insert_subsdetails_sql_result = mysql_query($Insert_subsdetails_sql) or die(mysql_error().$Insert_subsdetails_sql);
	
	$transaction_id=Fngetvalue("subscription_details Inner Join auction ON subscription_details.Auction_ID = auction.Auction_ID Inner Join transaction ON subscription_details.Transaction_ID = transaction.Transaction_ID","group_concat(DISTINCT subscription_details.Transaction_ID)","auction.GroupNo='$reqGroupNo' and Ticketno = $reqTicketno");

	$advance_transaction_id=Fngetvalue("tbl_advance_payment","group_concat(Transaction_ID)","Group_No='$reqGroupNo' and Ticket_No='$reqTicketno' and main_branch_id='$reqmain_branch_id'");
	if ((($transaction_id == "" )|| ($transaction_id == 0)) &&(($advance_trans_ids <> "" ) && ($advance_trans_ids <> 0))) {
		 $transaction_id = $advance_trans_ids;
	}elseif (($advance_trans_ids <> "" ) && ($advance_trans_ids <> 0)){
		$transaction_id = $transaction_id.",".$advance_trans_ids;
	}
	
	//echo $Trans_ids;	
	if (($transaction_id <> "" )&& ($transaction_id <> 0)){
				$insert_trans_details_sql = "INSERT INTO `transaction_removed` (`Transaction_ID`,`Transaction_ID_Act`,`ReceiptNo`,`Reciept_Date`,`TRNO`,`TRDate`,`Amount`,`printreciept`,`Area`,`DayBookNo`,`main_branch_id`,`receipt_type`,`payment_by`,`status`,`receipt_enter_by`,`collection_code`,`receipt_flag`,`dep_main_branch_id`)
											SELECT 	 NULL as `Transaction_ID`, transaction.`Transaction_ID` as `Transaction_ID_Act`,`transaction`.`ReceiptNo` as ReceiptNo,`Reciept_Date`,`TRNO`,`TRDate`,`Amount`,`printreciept`,`Area`,`DayBookNo`,`transaction`.`main_branch_id` as main_branch_id,`receipt_type`,`payment_by`,`transaction`.`status` as `status`,`transaction`.`receipt_enter_by` as `receipt_enter_by`,`transaction`.`collection_code` as `collection_code`,`receipt_flag`,`dep_main_branch_id`
											FROM  transaction
											WHERE Transaction_ID in ($transaction_id)";							
				$insert_trans_details_sql_result = 	mysql_query($insert_trans_details_sql) or die(mysql_error().$insert_trans_details_sql);			
							
				
				$insert_transdetails_sql = "INSERT INTO `transaction_details_removed` (`Amount`,`Remark`,`ChequeNo`,`Bankname`,`ChequeDate`,`CheRetTime`,`Cheretfee`,`receipt_type`,`payment_by`,`trans_id`,`status`,`dep_slip_ID`,`main_branch_id`,`value_chequereturn_date`,`value_comment`,`adjustment_ID`)
											SELECT 	`transaction_details`.`Amount` as Amount,`transaction_details`.`Remark` as Remark,`transaction_details`.`ChequeNo` as ChequeNo,`transaction_details`.`Bankname` as Bankname,`transaction_details`.`ChequeDate` as ChequeDate,`transaction_details`.`CheRetTime` as CheRetTime,`transaction_details`.`Cheretfee` as Cheretfee,`transaction_details`.`receipt_type` as receipt_type,`transaction_details`.`payment_by` as payment_by,`transaction_details`.`trans_id` as trans_id,`transaction_details`.`status` as status,`transaction_details`.`dep_slip_ID` as dep_slip_ID,`transaction_details`.`main_branch_id` as main_branch_id
											,`value_chequereturn_date`,`value_comment`,`adjustment_ID`
											FROM  transaction_details
											WHERE trans_id in ($transaction_id)";
				$insert_transdetails_sql_result = mysql_query($insert_transdetails_sql) or die(mysql_error().$insert_transdetails_sql);	
				
				$update_trans="update transaction set `status`=4 where Transaction_ID in ($transaction_id)";	
				mysql_query($update_trans) or die(mysql_error().$update_trans);	
    } 
	$advance_id=Fngetvalue("tbl_advance_payment","group_concat(Advance_ID)","Group_No='$reqGroupNo' and Ticket_No='$reqTicketno' and main_branch_id='$reqmain_branch_id'");
	if (($advance_id <> "" )&& ($advance_id <> 0)){
		$insert_adv_removed="INSERT INTO `tbl_advance_removed` (`Advance_ID`,`Group_No`,`Ticket_No`,`Transaction_ID`,`SubID`,`main_branch_id`,`Advance_Amount`,LateFee,Subcription_Fee,other_charge,check_return_fee,transfer_fee)
									SELECT 	`Advance_ID`as Advance_ID,`Group_No`as Group_No,`Ticket_No` as Ticket_No,`Transaction_ID`as Transaction_ID,`SubID` as SubID,
									`main_branch_id` as main_branch_id,`Advance_Amount` as Advance_Amount,LateFee as LateFee,Subcription_Fee as Subcription_Fee,other_charge as other_charge,check_return_fee as check_return_fee,transfer_fee as transfer_fee
									FROM  tbl_advance_payment
									WHERE Advance_ID in ($advance_id)";
		mysql_query($insert_adv_removed) or die(mysql_error().$insert_adv_removed);	
	 	 				
	} 
	$SubCode= FnGetValue("subdirdetails","SubCode","SubID ='$reqToSubID'");
	
	$update_applicantdetails_sql	 = "update applicantdetails SET SubCodeNo='$reqSubCodeNo',`SubID`='$reqToSubID' ,Remove_status = 'transfered' WHERE AppID= $reqAppID ";		
	$update_applicantdetails_sql_result = mysql_query($update_applicantdetails_sql)	or die(mysql_error().$update_applicantdetails_sql);			
  	
	$update_groupforming_sql = " update groupforming set `SubID`='$reqToSubID',`SubCode`='$reqSubCodeNo' WHERE GroupNoEnrolled='$reqGroupNo' and TktNoEnrolled = $reqTicketno";
  	$update_groupforming_sql_result = mysql_query($update_groupforming_sql) or die(mysql_error().$update_groupforming_sql);		

	$update_tbl_advance_payment_sql = " update tbl_advance_payment set `SubID`='$reqToSubID' WHERE Group_No='$reqGroupNo' and Ticket_No = $reqTicketno";
  	$update_tbl_advance_payment_sql_result = mysql_query($update_tbl_advance_payment_sql) or die(mysql_error().$update_tbl_advance_payment_sql);		

	//introducer
	$intro_sql="update tbl_introducer_payment_details set subcode='$reqSubCodeNo' where `GroupNo`='$reqGroupNo' and `Ticket_No`='$reqTicketno' and `main_branch_id`='$reqmain_branch_id' and `status`<>'Removed' ";		
	$update_intro_details= mysql_query($intro_sql)	or die(mysql_error().$intro_sql);
	//account related end
	if ($one_acc_for_group==1){
		include_once( 'acc_include_functions.php');
		$finacial_yr_id	= fngetvalue("tbl_acc_financial_years","main_acc_fianancial_year_id"," main_branch_id='".$reqmain_branch_id."' and is_default='1' and lang='english'");
		
		$req_arrears_of_subn_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Arrears of Subscription' and lang = '$ses_lang'");								
		$req_arrears_of_subn_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_arrears_of_subn_acc_type' and account_name='Arrears of Subscription - ".$reqGroupNo."' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_arrears_of_subn_acc_id==0){
			$req_arrears_of_subn_acc_id=create_account('Arrears of Subscription - '.$reqGroupNo,$req_arrears_of_subn_acc_type,'groupdetails',$reqGroupID,$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}	
		$req_dividend_payble_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Dividend Payable' and lang = '$ses_lang'");								
		$req_dividend_payble_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_dividend_payble_acc_type' and account_name='Dividend Payable - ".$reqGroupNo."' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_dividend_payble_acc_id==0){
			$req_dividend_payble_acc_id=create_account('Dividend Payable - '.$reqGroupNo,$req_dividend_payble_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}
/*		$req_comm_of_canc_chit_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Indirect Incomes' and lang = '$ses_lang'");								
		$req_comm_of_canc_chit_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_comm_of_canc_chit_acc_type' and account_name='Commission of Cancelled Chit' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_comm_of_canc_chit_acc_id==0){
			$req_comm_of_canc_chit_acc_id=create_account('Commission of Cancelled Chit',$req_comm_of_canc_chit_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}*/
		$req_transfered_sub_payble_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Current Liabilities' and lang = '$ses_lang'");								
		$req_transfered_sub_payble_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_transfered_sub_payble_acc_type' and account_name='Transferred Subscription Payable' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_transfered_sub_payble_acc_id==0){
			$req_transfered_sub_payble_acc_id=create_account('Transferred Subscription Payable',$req_transfered_sub_payble_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}
		$req_bank_charges_received_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Direct Incomes' and lang = '$ses_lang'");								
		$req_bank_charges_received_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_bank_charges_received_acc_type' and account_name='Bank Charges Received' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_bank_charges_received_acc_id==0){
			$req_bank_charges_received_acc_id=create_account('Bank Charges Received',$req_bank_charges_received_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}
/*		$req_forfeited_subscription_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Direct Incomes' and lang = '$ses_lang'");								
		$req_forfeited_subscription_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_forfeited_subscription_acc_type' and account_name='Forfeited Subscription' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_forfeited_subscription_acc_id==0){
			$req_forfeited_subscription_acc_id=create_account('Forfeited Subscription',$req_forfeited_subscription_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}*/
		
		$req_paid =FnGetValueMultiple("subscription_details_removed  LEFT JOIN auction  ON (auction.Auction_ID = subscription_details_removed.Auction_ID)  INNER JOIN transaction_removed ON ( subscription_details_removed.Transaction_ID = transaction_removed.Transaction_ID_Act )",
			"sum( subscription_details_removed.Subcription),sum( subscription_details_removed.lasdevidentt),sum(subscription_details_removed.Subcription_Fee),sum(subscription_details_removed.other_charge),sum(subscription_details_removed.check_return_fee),sum(subscription_details_removed.transfer_fee),sum(subscription_details_removed.refund_amount),sum(subscription_details_removed.ser_tax)",
			" subscription_details_removed.SubID='$reqFromSubID' and transaction_removed.status<2 and auction.main_branch_id ='$reqmain_branch_id' and transaction_removed.main_branch_id ='$reqmain_branch_id'  and auction.GroupNo = '$reqGroupNo'  and subscription_details_removed.Ticketno='$reqTicketno'");
		$advance_paid=Fngetvalue("tbl_advance_removed inner join `transaction_removed` on `transaction_removed`.Transaction_ID_Act=tbl_advance_removed.Transaction_ID ","sum(Advance_Amount)","Group_No='$reqGroupNo' and Ticket_No='$reqTicketno' and tbl_advance_removed.main_branch_id='$reqmain_branch_id' and status<2");
	    //$row['subpaid']=$req_paid[0]+$advance_paid;
		$reqsubpaid =($req_paid[0]+$advance_paid+$req_paid[1])-$req_paid[2]-$req_paid[3]-$req_paid[4]-$req_paid[5]-$req_paid[6]-$req_paid[7];			
		//print_r($req_paid); 
		$reqGroupDet=fngetvaluemultiple("groupdetails","ChitValue,Duration","GroupNo='".$reqGroupNo."'");		
		$reqforemancomper=5;
		$reqChitValue=$reqGroupDet[0];		
		//$reqforeman_comm=($reqforemancomper/100)*$reqChitValue;	
		$req_removed_sub_payble = $reqsubpaid-$req_paid[1];
		
		if ($req_paid[1] > 0 ){ 
			$reqTo_Acc_ID[0][0] = $req_dividend_payble_acc_id;
			$reqTo_Acc_ID[0][1] = $req_paid[1];
			$reqTo_Acc_ID[1][0] = $req_transfered_sub_payble_acc_id;
			$reqTo_Acc_ID[1][1] = $req_removed_sub_payble;
		}else{
			$reqTo_Acc_ID[0][0] = $req_transfered_sub_payble_acc_id;
			$reqTo_Acc_ID[0][1] = $req_removed_sub_payble;			
		}
		
		$acc_trans_main_id = fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_tranf_mem' and refered_id = '$applicantdetails_removed_ID'");
		add_voucher('Journal_tranf_mem',$req_arrears_of_subn_acc_id,$applicantdetails_removed_ID,$reqsubpaid,$reqremove_date,"Indirect Transferred Member",$reqmain_branch_id,$acc_trans_main_id,$reqTo_Acc_ID);
	}	
}
function fntransfer_member($reqAppID,$reqToSubID,$reqremove_date,$reqremove_comment){
	$ses_lang    		= $_SESSION['opt_lang'];
	$one_acc_for_group	= fngetvalue("tbl_mn_app_setting","field_value","field_name='one_acc_for_group'");		
	$appArr				= FngetValuemultiple("applicantdetails","GroupNo,Ticketno,main_branch_id,GroupID,SubCodeNo,SubID"," AppID= '$reqAppID'");	
	$reqGroupNo 		= $appArr[0]; 
	$reqTicketno 		= $appArr[1];	
	$reqmain_branch_id 	= $appArr[2]; 		
	$reqGroupID 		= $appArr[3]; 	
	$reqSubCodeNo 		= $appArr[4]; 	
	$reqFromSubID 		= $appArr[5]; 	
	
	$Insert_appdetails_sql= "INSERT INTO `applicantdetails_removed` (`AppID`,`Ticketno`, `GroupNo`,`SubCodeNo`,`prizeamount`, 
	    `auctiondate`, `amtinword`, `prizeamtinword`, `installmentinword`,`installment`, `amount`, `Sustsubcode`, `remdate`, 
	   `chrettime`, `latetime`, `main_branch_id`, `foreman_comm`,`status`,`Remove_status`,
	   `Service_Tax`,`Verification_Charge`,`Adjacement`,`Netpayable_amount`,`remove_date`,`remove_comment`,`SubID`,`GroupID`,`chitvalue_share`) 
		
		SELECT `AppID`,`Ticketno`, `GroupNo`, `SubCodeNo`,`prizeamount`, 
	   `auctiondate`, `amtinword`, `prizeamtinword`, `installmentinword`,`installment`, `amount`, `Sustsubcode`, `remdate`, 
	   `chrettime`, `latetime`, `main_branch_id`, `foreman_comm`,`status`,'transfered',
	   `Service_Tax`,`Verification_Charge`,`Adjacement`,`Netpayable_amount`,'$reqremove_date','$reqremove_comment',`SubID`,`GroupID`,`chitvalue_share`
    	FROM applicantdetails WHERE AppID='$reqAppID' ";
   	$Insert_details_sql_result = mysql_query($Insert_appdetails_sql) or die(mysql_error().$Insert_appdetails_sql);	
	$applicantdetails_removed_ID = mysql_insert_id();
	
	$Insert_enrollfee_sql = "INSERT INTO `enroll_payment_removed` (`enroll_payment_id`, `main_branch_id`, `ticket_no`, `Group_No`, `Sub_ID`, `transaction_ID`, `Amount`) 
							   SELECT NULL as `enroll_payment_id`,`main_branch_id`, `ticket_no`, `Group_No`, `Sub_ID`, `transaction_ID`, `Amount`
							   FROM enroll_payment WHERE Group_No='$reqGroupNo' and ticket_no = '$reqTicketno' and main_branch_id='$reqmain_branch_id'";
  	$Insert_enrollfee_sql_result = mysql_query($Insert_enrollfee_sql) or die(mysql_error().$Insert_enrollfee_sql);

	$Insert_subsdetails_sql = "INSERT INTO `subscription_details_removed` (`Subscription_Act_ID`,`Auction_ID`,`TicketNo`,`Transaction_ID`,`SubID`,`LateFee`,`lasdevidentt`,`arrears`,`main_branch_id`,`Subcription`,`Subcription_Fee`,`other_charge`,`check_return_fee`,`transfer_fee` ,`adv_refund_pay_id`,`refund_amount`,`ser_tax`)
							   SELECT subscription_details.`Subscription_ID` as `Subscription_Act_ID`,`subscription_details`.`Auction_ID` as Auction_ID,`TicketNo`,`Transaction_ID`,`SubID`,`LateFee`,`subscription_details`.`lasdevidentt` as lasdevidentt,`arrears`,`subscription_details`.`main_branch_id` as main_branch_id,`subscription_details`.`Subcription` as Subcription,`Subcription_Fee`,`other_charge`,`check_return_fee`,`transfer_fee`,`adv_refund_pay_id`,`refund_amount`,`ser_tax`
							   FROM (subscription_details Inner Join auction ON subscription_details.Auction_ID = auction.Auction_ID) WHERE auction.GroupNo='$reqGroupNo' and Ticketno = $reqTicketno";
  	$Insert_subsdetails_sql_result = mysql_query($Insert_subsdetails_sql) or die(mysql_error().$Insert_subsdetails_sql);
	
	$transaction_id=Fngetvalue("subscription_details Inner Join auction ON subscription_details.Auction_ID = auction.Auction_ID Inner Join transaction ON subscription_details.Transaction_ID = transaction.Transaction_ID","group_concat(DISTINCT subscription_details.Transaction_ID)","auction.GroupNo='$reqGroupNo' and Ticketno = $reqTicketno");

	$advance_transaction_id=Fngetvalue("tbl_advance_payment","group_concat(Transaction_ID)","Group_No='$reqGroupNo' and Ticket_No='$reqTicketno' and main_branch_id='$reqmain_branch_id'");
	if (($advance_transaction_id <> "" )&& ($advance_transaction_id <> 0)){
		 $transaction_id = $transaction_id.",".$advance_transaction_id;
	}elseif (($transaction_id == "" )|| ($transaction_id == 0)){
		$transaction_id = $advance_transaction_id;
	}
	//echo $Trans_ids;	
	if (($transaction_id <> "" )&& ($transaction_id <> 0)){
		$insert_trans_details_sql = "INSERT INTO `transaction_removed` (`Transaction_ID`,`Transaction_ID_Act`,`ReceiptNo`,`Reciept_Date`,`TRNO`,`TRDate`,`Amount`,`printreciept`,`Area`,`DayBookNo`,`main_branch_id`,`receipt_type`,`payment_by`,`status`,`receipt_enter_by`,`collection_code`,`receipt_flag`,`dep_main_branch_id`)
									SELECT 	 NULL as `Transaction_ID`, transaction.`Transaction_ID` as `Transaction_ID_Act`,`transaction`.`ReceiptNo` as ReceiptNo,`Reciept_Date`,`TRNO`,`TRDate`,`Amount`,`printreciept`,`Area`,`DayBookNo`,`transaction`.`main_branch_id` as main_branch_id,`receipt_type`,`payment_by`,`transaction`.`status` as `status`,`transaction`.`receipt_enter_by` as `receipt_enter_by`,`transaction`.`collection_code` as `collection_code`,`receipt_flag`,`dep_main_branch_id`
									FROM  transaction
									WHERE Transaction_ID in ($transaction_id)";							
		$insert_trans_details_sql_result = 	mysql_query($insert_trans_details_sql) or die(mysql_error().$insert_trans_details_sql);			
					
		
		$insert_transdetails_sql = "INSERT INTO `transaction_details_removed` (`Amount`,`Remark`,`ChequeNo`,`Bankname`,`ChequeDate`,`CheRetTime`,`Cheretfee`,`receipt_type`,`payment_by`,`trans_id`,`status`,`dep_slip_ID`,`main_branch_id`,`value_chequereturn_date`,`value_comment`,`adjustment_ID`,`neft_bank`)
									SELECT 	`transaction_details`.`Amount` as Amount,`transaction_details`.`Remark` as Remark,`transaction_details`.`ChequeNo` as ChequeNo,`transaction_details`.`Bankname` as Bankname,`transaction_details`.`ChequeDate` as ChequeDate,`transaction_details`.`CheRetTime` as CheRetTime,`transaction_details`.`Cheretfee` as Cheretfee,`transaction_details`.`receipt_type` as receipt_type,`transaction_details`.`payment_by` as payment_by,`transaction_details`.`trans_id` as trans_id,`transaction_details`.`status` as status,`transaction_details`.`dep_slip_ID` as dep_slip_ID,`transaction_details`.`main_branch_id` as main_branch_id
									,`value_chequereturn_date`,`value_comment`,`adjustment_ID`,`neft_bank`
									FROM  transaction_details
									WHERE trans_id in ($transaction_id)";
		$insert_transdetails_sql_result = mysql_query($insert_transdetails_sql) or die(mysql_error().$insert_transdetails_sql);						
    } 
	
	$advance_id=Fngetvalue("tbl_advance_payment","group_concat(Advance_ID)","Group_No='$reqGroupNo' and Ticket_No='$reqTicketno' and main_branch_id='$reqmain_branch_id'");
	if (($advance_id <> "" )&& ($advance_id <> 0)){
		  $insert_adv_removed="INSERT INTO `tbl_advance_removed` (`Advance_ID`,`Group_No`,`Ticket_No`,`Transaction_ID`,`SubID`,`main_branch_id`,`Advance_Amount`,LateFee,Subcription_Fee,other_charge,check_return_fee,transfer_fee)
									  SELECT 	`Advance_ID`as Advance_ID,`Group_No`as Group_No,`Ticket_No` as Ticket_No,`Transaction_ID`as Transaction_ID,`SubID` as SubID,
									  `main_branch_id` as main_branch_id,`Advance_Amount` as Advance_Amount,LateFee as LateFee,Subcription_Fee as Subcription_Fee,other_charge as other_charge,check_return_fee as check_return_fee,transfer_fee as transfer_fee
									  FROM  tbl_advance_payment
									  WHERE Advance_ID in ($advance_id)";
		  mysql_query($insert_adv_removed) or die(mysql_error().$insert_adv_removed);	
		  
		  $update_advance="update tbl_advance_payment set SubID='$reqToSubID' WHERE Advance_ID in ($advance_id)";		
		  mysql_query($update_advance) or die(mysql_error().$update_advance);	
	 } 
	 
	  
	  $subcode= FnGetValue("subdirdetails","SubCode"," SubID ='$reqToSubID'");
	  
	$update_enroll_payment_sql = " update enroll_payment set enroll_payment.`Sub_ID`='$reqToSubID' where Group_No='$reqGroupNo' and ticket_no = '$reqTicketno' and main_branch_id='$reqmain_branch_id'";
  	$update_enroll_payment_sql_result = mysql_query($update_enroll_payment_sql) or die(mysql_error().$update_enroll_payment_sql);	
	
	
	$update_applicantdetails_sql	 = "update applicantdetails SET SubCodeNo='$subcode',`SubID`='$reqToSubID',Remove_status = 'transfered' WHERE AppID= $reqAppID ";		
	$update_applicantdetails_sql_result = mysql_query($update_applicantdetails_sql)	or die(mysql_error().$update_applicantdetails_sql);			
   
	$update_subsdetails_sql = " update subscription_details,auction set subscription_details.`SubID`='$reqToSubID' where subscription_details.Auction_ID = auction.Auction_ID and auction.GroupNo='$reqGroupNo' and Ticketno = $reqTicketno";
  	$update_subsdetails_sql_result = mysql_query($update_subsdetails_sql) or die(mysql_error().$update_subsdetails_sql);	

	$update_groupforming_sql = " update groupforming set SubID='$reqToSubID',`SubCode`='$subcode' WHERE GroupNoEnrolled='$reqGroupNo' and TktNoEnrolled = $reqTicketno";
  	$update_groupforming_sql_result = mysql_query($update_groupforming_sql) or die(mysql_error().$update_groupforming_sql);		

	//introducer
	$intro_sql="update tbl_introducer_payment_details set  subcode='$subcode' where `GroupNo`='$reqGroupNo' and `Ticket_No`='$reqTicketno' and `main_branch_id`='$reqmain_branch_id' and `status`<>'Removed' ";		
	$update_intro_details= mysql_query($intro_sql)	or die(mysql_error().$intro_sql);
	//account related start
		$FullName = FnGetValue("subdirdetails","FullName","SubID = '$reqToSubID'");
		
		$finacial_yr_id=fngetvalue("tbl_acc_financial_years","main_acc_fianancial_year_id"," main_branch_id='$reqmain_branch_id' and is_default='1' and lang='english'");
		
		$account_id   = FnGetValue("tbl_acc_accounts","main_acc_account_id","related_type='applicantdetails' and  related_id = '$reqAppID' and lang='$ses_lang' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		$account_name = $reqTicketno."-".$FullName;
		$acc_sql	= "update `tbl_acc_accounts` set `account_name`='$account_name' where `acc_account_id` = '$account_id' ";
		$acc_result	= mysql_query($acc_sql) or die(mysql_error().$acc_sql);
	//account related end
	if ($one_acc_for_group==1){
		include_once( 'acc_include_functions.php');
		$finacial_yr_id	= fngetvalue("tbl_acc_financial_years","main_acc_fianancial_year_id"," main_branch_id='".$reqmain_branch_id."' and is_default='1' and lang='english'");
		
		$req_arrears_of_subn_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Arrears of Subscription' and lang = '$ses_lang'");								
		$req_arrears_of_subn_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_arrears_of_subn_acc_type' and account_name='Arrears of Subscription - ".$reqGroupNo."' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_arrears_of_subn_acc_id==0){
			$req_arrears_of_subn_acc_id=create_account('Arrears of Subscription - '.$reqGroupNo,$req_arrears_of_subn_acc_type,'groupdetails',$reqGroupID,$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}	
		$req_dividend_payble_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Dividend Payable' and lang = '$ses_lang'");								
		$req_dividend_payble_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_dividend_payble_acc_type' and account_name='Dividend Payable - ".$reqGroupNo."' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_dividend_payble_acc_id==0){
			$req_dividend_payble_acc_id=create_account('Dividend Payable - '.$reqGroupNo,$req_dividend_payble_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}
/*		$req_comm_of_canc_chit_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Indirect Incomes' and lang = '$ses_lang'");								
		$req_comm_of_canc_chit_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_comm_of_canc_chit_acc_type' and account_name='Commission of Cancelled Chit' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_comm_of_canc_chit_acc_id==0){
			$req_comm_of_canc_chit_acc_id=create_account('Commission of Cancelled Chit',$req_comm_of_canc_chit_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}*/
		$req_transfered_sub_payble_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Current Liabilities' and lang = '$ses_lang'");								
		$req_transfered_sub_payble_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_transfered_sub_payble_acc_type' and account_name='Transferred Subscription Payable' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_transfered_sub_payble_acc_id==0){
			$req_transfered_sub_payble_acc_id=create_account('Transferred Subscription Payable',$req_transfered_sub_payble_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}
		$req_bank_charges_received_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Direct Incomes' and lang = '$ses_lang'");								
		$req_bank_charges_received_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_bank_charges_received_acc_type' and account_name='Bank Charges Received' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_bank_charges_received_acc_id==0){
			$req_bank_charges_received_acc_id=create_account('Bank Charges Received',$req_bank_charges_received_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}
/*		$req_forfeited_subscription_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Direct Incomes' and lang = '$ses_lang'");								
		$req_forfeited_subscription_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_forfeited_subscription_acc_type' and account_name='Forfeited Subscription' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
		if ($req_forfeited_subscription_acc_id==0){
			$req_forfeited_subscription_acc_id=create_account('Forfeited Subscription',$req_forfeited_subscription_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqmain_branch_id,$finacial_yr_id);
		}*/
		
		$req_paid =FnGetValueMultiple("subscription_details_removed  LEFT JOIN auction  ON (auction.Auction_ID = subscription_details_removed.Auction_ID)  INNER JOIN transaction_removed ON ( subscription_details_removed.Transaction_ID = transaction_removed.Transaction_ID_Act )",
			"sum( subscription_details_removed.Subcription),sum( subscription_details_removed.lasdevidentt),sum(subscription_details_removed.Subcription_Fee),sum(subscription_details_removed.other_charge),sum(subscription_details_removed.check_return_fee),sum(subscription_details_removed.transfer_fee),sum(subscription_details_removed.refund_amount),sum(subscription_details_removed.ser_tax)",
			" subscription_details_removed.SubID='$reqFromSubID' and transaction_removed.status<2 and auction.main_branch_id ='$reqmain_branch_id' and transaction_removed.main_branch_id ='$reqmain_branch_id'  and auction.GroupNo = '$reqGroupNo'  and subscription_details_removed.Ticketno='$reqTicketno'");
		$advance_paid=Fngetvalue("tbl_advance_removed inner join `transaction_removed` on `transaction_removed`.Transaction_ID_Act=tbl_advance_removed.Transaction_ID ","sum(Advance_Amount)","Group_No='$reqGroupNo' and Ticket_No='$reqTicketno' and tbl_advance_removed.main_branch_id='$reqmain_branch_id' and status<2");
	    //$row['subpaid']=$req_paid[0]+$advance_paid;
		$reqsubpaid =($req_paid[0]+$advance_paid+$req_paid[1])-$req_paid[2]-$req_paid[3]-$req_paid[4]-$req_paid[5]-$req_paid[6]-$req_paid[7];			
		//print_r($req_paid); 
		$reqGroupDet=fngetvaluemultiple("groupdetails","ChitValue,Duration","GroupNo='".$reqGroupNo."'");		
		$reqforemancomper=5;
		$reqChitValue=$reqGroupDet[0];		
		//$reqforeman_comm=($reqforemancomper/100)*$reqChitValue;	
		$req_removed_sub_payble = $reqsubpaid-$req_paid[1];
		
		if ($req_paid[1] > 0 ){ 
			$reqTo_Acc_ID[0][0] = $req_dividend_payble_acc_id;
			$reqTo_Acc_ID[0][1] = $req_paid[1];
			$reqTo_Acc_ID[1][0] = $req_transfered_sub_payble_acc_id;
			$reqTo_Acc_ID[1][1] = $req_removed_sub_payble;
		}else{
			$reqTo_Acc_ID[0][0] = $req_transfered_sub_payble_acc_id;
			$reqTo_Acc_ID[0][1] = $req_removed_sub_payble;			
		}
		
		$acc_trans_main_id = fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_tranf_mem' and refered_id = '$applicantdetails_removed_ID'");
		add_voucher('Journal_tranf_mem',$req_arrears_of_subn_acc_id,$applicantdetails_removed_ID,$reqsubpaid,$reqremove_date,"Transferred Member",$reqmain_branch_id,$acc_trans_main_id,$reqTo_Acc_ID);
	}
}
function fnGenerateDividentReturnVoucher($ApplicantDetailsRemovedID,$flag=0){
	include_once("acc_include_functions.php");
    $AppID 				= '';
    $SubID 				= '';
	$AccountName 		= '';
	$RemoveDate 		= '';
	$TotalDividentPaid 	= '';
	$main_branch_id 	= '';
	$From_Acc_ID 		= '';
	$To_Acc_ID 			= '';
	$acc_trans_main_id	= '';


	$ApplicantArray		= FngetvalueMultiple("applicantdetails_removed","main_branch_id,SubID,AppID,remove_date,Ticketno","ID = '$ApplicantDetailsRemovedID'");
	$main_branch_id		= $ApplicantArray[0];
	$SubID				= $ApplicantArray[1];
	$AppID				= $ApplicantArray[2];
	$RemoveDate			= $ApplicantArray[3];
	$Ticketno			= $ApplicantArray[4];
	if(($SubID > 0) && ($AppID > 0)){
		$MemberName 		= fnGetValue("subdirdetails","FullName","SubID= ".$SubID);
	
		$AccountName		= $Ticketno."-".$MemberName;
		$acc_transaction_main_id = FnGetValue(
									"	`tbl_acc_transaction`
										left join 
											`tbl_acc_transaction_main`
										on 
											`tbl_acc_transaction_main`.`acc_transaction_main_id` = `tbl_acc_transaction`.`acc_transaction_main_id`
										left join
											 `tbl_acc_accounts`
										on	
											`tbl_acc_accounts`.`main_acc_account_id` = `tbl_acc_transaction`.`from_acc_id`",
									"group_concat(distinct(`tbl_acc_transaction`.`acc_transaction_main_id`))",
									"	`tbl_acc_accounts`.`related_type` 	= 'applicantdetails' and
										`tbl_acc_accounts`.`related_id` 	= '$AppID' and
										`account_name` like \"$AccountName\"");
		if($acc_transaction_main_id != ""){
			$TotalDividentPaid 		= fnGetvalue("`tbl_acc_transaction`,`tbl_acc_accounts`","sum(db_amt)",
												"`tbl_acc_accounts`.`main_acc_account_id` = `tbl_acc_transaction`.`from_acc_id` and
												`tbl_acc_accounts`.`account_name`  	= 'Dividend Paid' and
												`acc_transaction_main_id` in ( $acc_transaction_main_id)");
			$DefaultFinancialYear 	= fnGetValue("tbl_acc_financial_years","main_acc_fianancial_year_id","`is_default` = 1 and `main_branch_id` = $main_branch_id");
			
			$From_Acc_ID 			= fnGetValue("`tbl_acc_accounts`","main_acc_account_id","`tbl_acc_accounts`.`related_type` = 'applicantdetails' and `tbl_acc_accounts`.`related_id` = '$AppID' and `account_name` like '$AccountName' and `acc_fianancial_year_id`  = $DefaultFinancialYear");

			$Dividend_Paid_account_type 	= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Sundry Creditors' and lang = '".$_SESSION['opt_lang']."'");								
			
			$To_Acc_ID 				= fnGetValue("`tbl_acc_accounts`","main_acc_account_id","lang = '".$_SESSION['opt_lang']."' and account_type = '$Dividend_Paid_account_type' and `tbl_acc_accounts`.`account_name` = 'Dividend Paid' and `acc_fianancial_year_id`  = $DefaultFinancialYear");
			
			$acc_trans_main_id 		= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Dividend_Return' and refered_id = '$ApplicantDetailsRemovedID'");
			add_voucher('Dividend_Return',$From_Acc_ID,$ApplicantDetailsRemovedID,$TotalDividentPaid,$RemoveDate,"Dividend Return(Removed)- $AccountName",$main_branch_id,$acc_trans_main_id,$To_Acc_ID);
		}
		
	}
	if($flag == 1){
		?>
		<tr>
		<td><?=$AppID?></td>
		<td><?=$SubID?></td>
		<td><?=$AccountName?></td>
		<td><?=$RemoveDate?></td>
		<td><?=$main_branch_id?></td>
		<td><?=$TotalDividentPaid?></td>
		<td><?=$From_Acc_ID?></td>
		<td><?=$To_Acc_ID?></td>
		<td><?=$acc_trans_main_id?></td>
		</tr><?
	}
}
function fnaddprizePreplaned($reqtxtGrp_No,$reqtxtTicket_No,$reqtxtAuction_No,$reqtxtAuction_Amt,$reqtxtForeman,$reqedit,$req_Prev_Prize_details_ID,$reqchkSMS,$Prize_details_ID=''){
	include_once( 'acc_include_functions.php');
//Session Data
	$ses_lang 					= $_SESSION['opt_lang'];
// Variable Declatrion/data
	$Group_acc_type_nature 		= 'Assets';
	$reqtxtCheque_Date  		= '0000-00-00' ; 
//Post Data
	$reqtxtInst_Month   		= $_POST['txtInst_Month'];
	$reqtxtCheque_No 			= $_POST['txtCheque_No'];
	if($_POST['txtCheque_Date'] != "")	$reqtxtCheque_Date  	= SqlDateIn($_POST['txtCheque_Date']);
	$reqtxtBank_Name 			= $_POST['txtBank_Name'];
	$reqtxtForeman   			= $_POST['txtForeman'];
    $reqtxtService_Tax   		= $_POST['txtService_Tax'];
	$reqtxtAdjacement   		= $_POST['txtAdjacement'];
	$reqtxtVerification_Charge  = $_POST['txtVerification_Charge'];
	$reqtxtNetPayable_Amount   	= $_POST['txtNetPayable_Amount'];		
	$reqtxtchitvalue_share   	= $_POST['txtchitvalue_share'];		
// Table groupdetails Related Data
	$groupdetails_array			= fngetvalueMultiple('groupdetails','Duration,ChitValue,main_branch_id,GroupID',"GroupNo='".$reqtxtGrp_No."' and ");
	$reqtxtDuration 			= $groupdetails_array[0];
	$reqtxtchitvalue			= $groupdetails_array[1];
 	$reqtxtgroupbranch			= $groupdetails_array[2];
	$req_GroupID 	 			= $groupdetails_array[3];	

// Table Auction Related Data

	$auction_array				= fngetvalueMultiple('auction','AuctionDateTime,LastDateofPayment,Month',"GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtAuction_No'");		
	$reqauctiondate 			= $auction_array[0];
	$reqtxtAuction_Date 		= substr($auction_array[0],10);
	$reqtxtInst_Month   		= $auction_array[2];
	//$reqtxtReciept_Date 		= $auction_array[1];
	$reqtxtAuction_Date  		= sqldateout( $reqtxtAuction_Date);		
	$reqtxtAuction_time  		= SQLDateOut_Time( $auction_array[0]);
	$LastDateofPayment			= sqldateout( $auction_array[1]);		
	$reqtxtAuction_time  		= substr($reqtxtAuction_time,11);
	if($reqauctiondate == ""){ 
		$reqauctiondate  		= '0000-00-00 00:00' ; 
	}
// Table applicantdetails Related Data
	$applicantdetails_array		= fngetvalueMultiple('applicantdetails','AppID,SubCodeNo',"GroupNo = '$reqtxtGrp_No' and TicketNo = '$reqtxtTicket_No' ");	
	$req_AppID 		 			= $applicantdetails_array[0];
	$req_SubCodeNo 	 			= $applicantdetails_array[1];
// Table tbl_mn_app_setting Related Data
	$is_prized_in_assets 		= FnGetValue('`tbl_mn_app_setting`','field_value',"field_name = 'is_prized_in_assets'");
	$if_prize_first 			= fnchkif_prize_first($req_GroupID);//FnGetValue('`tbl_mn_app_setting`','field_value',"field_name = 'if_prize_first'");

	$req_prev_AppID 			= fngetvalue('prize_details','AppID',"Prize_details_ID = '$req_Prev_Prize_details_ID'");			
		
// Table tbl_acc_account_type Related Data
	if ($is_prized_in_assets==1){
		$Group_acc_type_id 	= fnGetValue('tbl_acc_account_type','main_account_type_id',"account_type_name = '".$reqtxtGrp_No."' and account_type_nature = 'Assets' and lang='$ses_lang'");
		$Group_acc_type_before_id 	= fnGetValue('tbl_acc_account_type','main_account_type_id',"account_type_name = '".$reqtxtGrp_No."' and account_type_nature = 'Liability' and lang='$ses_lang'");		
	}else{
		$Group_acc_type_id 	= fnGetValue('tbl_acc_account_type','main_account_type_id',"account_type_name = '".$reqtxtGrp_No."' and account_type_nature = 'Liability' and lang='$ses_lang'");		
		$Group_acc_type_before_id 	= fnGetValue('tbl_acc_account_type','main_account_type_id',"account_type_name = '".$reqtxtGrp_No."' and account_type_nature = 'Assets' and lang='$ses_lang'");		
	}
	
// Table tbl_acc_financial_years Related Data
	$finacial_yr_id				= fngetvalue('tbl_acc_financial_years','main_acc_fianancial_year_id'," main_branch_id='$reqtxtgroupbranch' and is_default='1' and lang='english'");

// Table prize_details Related Data
	if($Prize_details_ID == ""){
		$Prize_details_ID 			= fnGetValue('prize_details','Prize_details_ID',"`GroupNo` = '$reqtxtGrp_No' and `TicketNo` = '$reqtxtTicket_No'");
	}
	$req_present 	 			= fnGetCount('prize_details',"AppID = $req_AppID and GroupNo = '$reqtxtGrp_No' and TicketNo = '$reqtxtTicket_No' and prizeinstno = '$reqtxtAuction_No'");
	$reqlasdevidentt 			= ($reqtxtAuction_Amt - ($reqtxtchitvalue * $reqtxtForeman/100))/$reqtxtDuration;
	$req_max_inst_paid 			= FnGetValue('auction,subscription_details','max(auction.InstNo)',"auction.Auction_ID = subscription_details.Auction_ID and auction.GroupNo='$reqtxtGrp_No' and subscription_details.Ticketno='".$reqtxtTicket_No."'");
	$reqtxtinstallment 			= ( $reqtxtDuration - $req_max_inst_paid ) * ( $reqtxtchitvalue / $reqtxtDuration );
	//if(USE_CHIT_VALUE_SHARE == 1){

	
	if($reqedit == 1){		
		if( $req_present > 0 ){
			/*
			$ins_sql2 = "Update prize_details SET 
				prizeamount 	= '$reqtxtAuction_Amt',		prizechequeno 	= '$reqtxtCheque_No',			prizebankname 	= '$reqtxtBank_Name',
				prizechequedate = '$reqtxtCheque_Date',		prizemonth 		= '$reqtxtInst_Month',			auctiondate 	= '$reqauctiondate',
				prizeinstno  	= '$reqtxtAuction_No',		installment  	= '$reqtxtinstallment',
				foreman_comm 	= '$reqtxtForeman',			chitvalue_share = '$reqtxtchitvalue_share',		prizepaymentby	= '$reqtxtPayment'	
			where AppID = $req_AppID and GroupNo = '$reqtxtGrp_No' and TicketNo = '$reqtxtTicket_No' and prizeinstno = '$reqtxtAuction_No'";
			*/
			$ins_sql2 = "Update prize_details SET 
				prizeamount 	= '$reqtxtAuction_Amt',		prizechequeno 	= '$reqtxtCheque_No',			prizebankname 	= '$reqtxtBank_Name',
				prizechequedate = '$reqtxtCheque_Date',		prizemonth 		= '$reqtxtInst_Month',			auctiondate 	= '$reqauctiondate',
				prizeinstno  	= '$reqtxtAuction_No',		installment  	= '$reqtxtinstallment',
				foreman_comm 	= '$reqtxtForeman',			chitvalue_share = '$reqtxtchitvalue_share',		prizepaymentby	= '$reqtxtPayment'	
			where Prize_details_ID = $Prize_details_ID";
			$res_sql2 = mysql_query($ins_sql2) or die(mysql_error().$ins_sql2);	
			//$Prize_details_ID	  = fnGetvalue("prize_details","Prize_details_ID","AppID = $req_AppID and GroupNo = '$reqtxtGrp_No' and TicketNo = '$reqtxtTicket_No' and prizeinstno = '$reqtxtAuction_No'");
			$Prize_details_ID	  = $Prize_details_ID;
		}else{
			$sql_previos_delete = "delete from prize_details 	where Prize_details_ID = '$req_Prev_Prize_details_ID'"; 
			$previos_sql1 		= mysql_query($sql_previos_delete) or die(mysql_error().$sql_previos_delete);			
			$ins_sql1 = "INSERT INTO `prize_details` (`Prize_details_ID`, 
					`AppID`, `Ticketno`, `GroupNo`, `SubCodeNo`, `prizeamount`, 
					`prizechequeno`, `prizebankname`, `prizechequedate`, `prizemonth`, `auctiondate`, 
					`installment`, `amount`, `remdate`, `prizeinstno`, 
					`BlankCh1No`, `BlankCh1Bankname`, `BlankCh2No`, `BlankCh2Bankname`, `chrettime`, 
					`latetime`, `SpecInstruction`,  `foreman_comm`,chitvalue_share,prizepaymentby,
					main_branch_id) 
					VALUES 	(NULL,
					'$req_AppID','$reqtxtTicket_No','$reqtxtGrp_No','$req_SubCodeNo','$reqtxtAuction_Amt',
					'$reqtxtCheque_No','$reqtxtBank_Name','$reqtxtCheque_Date','$reqtxtInst_Month','$reqauctiondate',
					'','$reqtxtinstallment','0000-00-00 00:00:00','$reqtxtAuction_No',
					'','','','','0',
					'0','','$reqtxtForeman','$reqtxtchitvalue_share','$reqtxtPayment',
					$reqtxtgroupbranch)";
			$res_sql  = mysql_query($ins_sql1) or die(mysql_error().$ins_sql1);	
			$Prize_details_ID = mysql_insert_id();
		}	
	}else{
		$ins_sql1 = "
			INSERT INTO `prize_details` (`Prize_details_ID`, 
					`AppID`, `Ticketno`, `GroupNo`, `SubCodeNo`, `prizeamount`, 
					`prizechequeno`, `prizebankname`, `prizechequedate`, `prizemonth`, `auctiondate`, 
					`installment`, `amount`, `remdate`, `prizeinstno`, 
					`BlankCh1No`, `BlankCh1Bankname`, `BlankCh2No`, `BlankCh2Bankname`, `chrettime`, 
					`latetime`, `SpecInstruction`,  `foreman_comm`,chitvalue_share,prizepaymentby,
					main_branch_id) 
			VALUES 	(NULL,
					'$req_AppID','$reqtxtTicket_No','$reqtxtGrp_No','$req_SubCodeNo','$reqtxtAuction_Amt',
					'$reqtxtCheque_No','$reqtxtBank_Name','$reqtxtCheque_Date','$reqtxtInst_Month','$reqauctiondate',
					'','$reqtxtinstallment','0000-00-00 00:00:00','$reqtxtAuction_No',
					'','','','','0',
					'0','','$reqtxtForeman','$reqtxtchitvalue_share','$reqtxtPayment',
					$reqtxtgroupbranch)";
		$res_sql  = mysql_query($ins_sql1) or die(mysql_error().$ins_sql1);	
		$Prize_details_ID = mysql_insert_id();
	}
	//}	
	if($reqedit == 1){
		/*$update_previos_data = "
				Update 
					applicantdetails 
				SET 
					prizeamount = '0',prizechequeno = '',prizebankname = '',prizechequedate = '0000-00-00',prizemonth = '',
					prizeinstno  = '0',installment='0',status = 'non prized'
				where 
					GroupNo = '$reqtxtGrp_No' and TicketNo = '$req_Prev_Prize_details_ID'"; 
		
		$previos_sql = mysql_query($update_previos_data) or die(mysql_error().$update_previos_data);	*/
		// account related changes start

		//$rel_prev_acc_id 				= fnGetValue('tbl_acc_accounts','main_acc_account_id',"related_type = 'applicantdetails' and related_id = '".$req_prev_AppID ."' and lang='$ses_lang' and main_branch_id='$reqtxtgroupbranch'");
		$req_prev_from_prize_acc_id 	= fnGetValue('tbl_acc_accounts','main_acc_account_id',"related_type = 'applicantdetails' and related_id = '".$req_prev_AppID ."' and lang='$ses_lang' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
		$req_cur_from_prize_acc_id 		= fnGetValue('tbl_acc_accounts','main_acc_account_id',"related_type = 'applicantdetails' and related_id = '".$req_AppID ."' and lang='$ses_lang' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
		if($req_prev_AppID != $req_AppID){
			$update_sql1111 	= "
				update 
					tbl_acc_relate,tbl_acc_transaction 
				set 
					tbl_acc_transaction.from_acc_id = '$req_cur_from_prize_acc_id' 
				where 
					tbl_acc_relate.acc_transaction_id = tbl_acc_transaction.acc_transaction_main_id and
			 		tbl_acc_relate.acc_transaction_type in ('Journal_dividend','Journal_foreman_commission') and 
			 		tbl_acc_transaction.from_acc_id = '$req_prev_from_prize_acc_id'";
			$update_result 		= mysql_query($update_sql1111) or die(mysql_error().$update_sql1111);
			
			$update_sql 		= "update tbl_acc_relate set refered_id = '$Prize_details_ID' where acc_transaction_type = 'Journal_dividend' and refered_id = '$req_Prev_Prize_details_ID'";
			$update_result 		= mysql_query($update_sql) or die(mysql_error().$update_sql);	
			$update_sql 		= "update tbl_acc_relate set refered_id = '$Prize_details_ID' where acc_transaction_type = 'Journal_foreman_commission' and refered_id = '$req_Prev_Prize_details_ID'";
			$update_result 		= mysql_query($update_sql) or die(mysql_error().$update_sql);	
			$update_sql 		= "update tbl_acc_relate set refered_id = '$Prize_details_ID' where acc_transaction_type = 'Journal_dividend_pay' and refered_id = '$req_Prev_Prize_details_ID'";
			$update_result 		= mysql_query($update_sql) or die(mysql_error().$update_sql);			
			$upd_sql 			= "Update tbl_acc_accounts SET account_type = '".$Group_acc_type_before_id."' where main_acc_account_id = '$req_prev_from_prize_acc_id' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'";
			$res_udp 			= mysql_query($upd_sql) or die(mysql_error().$upd_sql);			
		}
		// account related changes end
	}	
	
	//$rel_acc_id 	= find_acc_id_for_applicant($req_AppID,$reqtxtgroupbranch);
	
	$req_from_prize_acc_id 	= find_acc_id_for_applicant($req_AppID,$reqtxtgroupbranch);
		
	$upd_sql 	= "Update tbl_acc_accounts SET account_type = '".$Group_acc_type_id."' where main_acc_account_id = '$req_from_prize_acc_id' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id' ";
	$res_udp 	= mysql_query($upd_sql) or die(mysql_error().$upd_sql);			

	$formancommision 		= ($reqtxtchitvalue_share * $reqtxtForeman/100);		 		
	//$req_from_prize_acc_id 	= fnGetValue("tbl_acc_accounts","main_acc_account_id","related_id = '".$req_AppID ."' and lang='$ses_lang'");

	if (($reqtxtAuction_Amt - $formancommision) > 0){
		$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_dividend' and refered_id = '$Prize_details_ID'");
		//add_voucher('Journal_dividend',$req_from_prize_acc_id,$Prize_details_ID,($reqtxtAuction_Amt - $formancommision),$reqtxtAuction_Date,"",$acc_trans_main_id);
		add_voucher('Journal_dividend',$req_from_prize_acc_id,$Prize_details_ID,($reqtxtAuction_Amt - $formancommision),$reqauctiondate,"Dividend",$reqtxtgroupbranch,$acc_trans_main_id);
	}
	if ($formancommision > 0){
		$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_foreman_commission' and refered_id = '$Prize_details_ID'");
		//add_voucher('Journal_foreman_commission',$req_from_prize_acc_id,$Prize_details_ID,$formancommision,$reqtxtAuction_Date,"",$acc_trans_main_id);
		add_voucher('Journal_foreman_commission',$req_from_prize_acc_id,$Prize_details_ID,$formancommision,$reqauctiondate,"Foreman Commission",$reqtxtgroupbranch,$acc_trans_main_id);
	}
	//accounting related end  
	
    //accounting related start
	$totalchitvalue_share 	= fngetvalue("prize_details","sum(chitvalue_share)","GroupNo='".$reqtxtGrp_No."' and 	prizeinstno ='".$reqtxtAuction_No."'");	
	$totalprizeamount 		= fngetvalue("prize_details","sum(prizeamount)","GroupNo='".$reqtxtGrp_No."' and 	prizeinstno ='".$reqtxtAuction_No."'");	
	$totalformancommision 	= fngetvalue("prize_details","sum(chitvalue_share*foreman_comm/100)","GroupNo='".$reqtxtGrp_No."' and 	prizeinstno ='".$reqtxtAuction_No."'");		
	$totalreqlasdevidentt 	= (($totalprizeamount - $totalformancommision)/$reqtxtDuration);

	if ( $totalchitvalue_share >= $reqtxtchitvalue ){
		/*$sql = "SELECT * FROM applicantdetails where GroupNo='$reqtxtGrp_No'";
		$temp = mysql_query($sql) or die(mysql_error().$sql);
		while($row = mysql_fetch_array($temp)){
			$refered_id 	 	= $Prize_details_ID."~".$row['AppID'];
			//$req_from_acc_id 	= fnGetValue("tbl_acc_accounts","main_acc_account_id","related_id = '".$row['AppID'] ."' and lang='$ses_lang'");
			$req_from_acc_id 	= find_acc_id_for_applicant($row['AppID'],$reqtxtgroupbranch);
			$reqtxtReciept_Date = fngetvalue("auction","left(AuctionDateTime,10)","GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtAuction_No'");	
			if ( $reqtxtReciept_Date == 0 ) $reqtxtReciept_Date	= date("Y-m-d");
			$reqlasdevidentt_share = ( $row['chitvalue_share'] / $reqtxtchitvalue ) * $totalreqlasdevidentt;			
			if ( $reqlasdevidentt_share > 0 ){
				$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_dividend_pay' and refered_id = '$refered_id'");
				//add_voucher('Journal_dividend_pay',$req_from_acc_id,$refered_id,$reqlasdevidentt_share,$reqtxtReciept_Date,"",$acc_trans_main_id);
				//add_voucher('Journal_dividend_pay',$req_from_acc_id,$refered_id,$reqlasdevidentt_share,$reqtxtReciept_Date,"Dividend Distribution",$reqtxtgroupbranch,$acc_trans_main_id);
			}
		}*/
		$ins_sql = "Update auction SET prizegiven = '1' where GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtAuction_No'";
		$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);			
		
		if ($if_prize_first == 1){ 	
			$reqNextAuction_No = $reqtxtAuction_No + 1;
			if ($reqNextAuction_No <= $reqtxtDuration){ 
				$ins_sql = "Update auction SET lasdevidentt = '$totalreqlasdevidentt' where GroupNo = '$reqtxtGrp_No' and InstNo = '$reqNextAuction_No'";
				$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);
			}	
		}else{
			$ins_sql = "Update auction SET lasdevidentt = '$totalreqlasdevidentt'  where GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtAuction_No'";
			$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);
		}
		
	}	
	
	/*
	$ins_sql = "
		Update 
			applicantdetails 
		SET 
			prizeamount = '$reqtxtAuction_Amt',		prizemonth = '$reqtxtInst_Month',		auctiondate = '$reqauctiondate',			
			prizeinstno  = '$reqtxtAuction_No',		installment  = '$reqtxtinstallment',	foreman_comm = '$reqtxtForeman',
			status = 'Auctioned'	
		where 
			GroupNo = '$reqtxtGrp_No' and TicketNo = '$reqtxtTicket_No'";
	$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);	
	// update status of applicant 
	$ins_sql = "Update auction SET prizegiven = '1',bidding_status = 2, biddingendtime = CURTIME()  where GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtAuction_No'";
	$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);
    if ($if_prize_first == 1){ 	
		$reqNextAuction_No = $reqtxtAuction_No + 1;
		if ($reqNextAuction_No <= $reqtxtDuration){ 
			$ins_sql = "Update auction SET lasdevidentt = '$reqlasdevidentt' where GroupNo = '$reqtxtGrp_No' and InstNo = '$reqNextAuction_No'";
			$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);
		}	
	}else{
		$ins_sql = "Update auction SET lasdevidentt = '$reqlasdevidentt'  where GroupNo = '$reqtxtGrp_No' and InstNo = '$reqtxtAuction_No'";
		$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);
	}

	$rel_acc_id = fnGetValue("tbl_acc_accounts","main_acc_account_id","related_id = '".$req_AppID ."' and lang = '$ses_lang' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
	$upd_sql 	= "Update tbl_acc_accounts SET account_type = '".$Group_acc_type_after_id."' where main_acc_account_id = '$rel_acc_id' ";
	$res_udp 	= mysql_query($upd_sql) or die(mysql_error().$upd_sql);			
	
	$formancommision = ($reqtxtchitvalue * $reqtxtForeman / 100);		 		
	
	$req_from_prize_acc_id 	= fnGetValue("tbl_acc_accounts","main_acc_account_id","related_type = 'applicantdetails' and related_id = '".$req_AppID ."' and lang='$ses_lang' and main_branch_id='$reqtxtgroupbranch' and acc_fianancial_year_id='$finacial_yr_id'");
	$acc_trans_main_id 		= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_dividend' and refered_id = '$req_AppID'");
	if (($reqtxtAuction_Amt - $formancommision) == 0 && $acc_trans_main_id > 0){ delete_voucher($acc_trans_main_id);}
	if (($reqtxtAuction_Amt - $formancommision) > 0){		
		add_voucher('Journal_dividend',$req_from_prize_acc_id,$req_AppID,($reqtxtAuction_Amt - $formancommision),$reqtxtAuction_Date,"Dividend",$reqtxtgroupbranch,$acc_trans_main_id);
	}
	
	$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_foreman_commission' and refered_id = '$req_AppID'");
	if ($formancommision == 0 && $acc_trans_main_id > 0){delete_voucher($acc_trans_main_id);}
	if ($formancommision > 0){		
		add_voucher('Journal_foreman_commission',$req_from_prize_acc_id,$req_AppID,$formancommision,$reqtxtAuction_Date,"Foreman Commission",$reqtxtgroupbranch,$acc_trans_main_id);
	}
	$req_from_acc_id 	= array();
	$sql 				= "SELECT * FROM applicantdetails where GroupNo='$reqtxtGrp_No'";
	$temp 				= mysql_query($sql) or die(mysql_error().$sql);
	$total_tks 			= mysql_num_rows($temp);
	while($row = mysql_fetch_array($temp)){
		$req_from_acc_id[]	= $row['AppID'];
	}
	$refered_id 			= $req_AppID;
	$reqlasdevidentt_share 	= $reqtxtAuction_Amt - $formancommision ;
	if ( $reqtxtReciept_Date == 0 ) $reqtxtReciept_Date	= date("Y-m-d");
	$acc_trans_main_id 		= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_dividend_pay' and refered_id = '$refered_id'");
	add_voucher('Journal_dividend_pay',$req_from_acc_id,$refered_id,$reqlasdevidentt_share,$reqtxtReciept_Date,"Dividend Distribution",$reqtxtgroupbranch,$acc_trans_main_id);
	
	$reqtxtAmt  = $reqtxtchitvalue - $reqtxtAuction_Amt;
	if ($reqtxtCheque_No<>""){
		$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Payment_chk' and refered_id = '$refered_id'");
		//add_voucher('Payment_chk',$req_from_prize_acc_id,$refered_id,$reqtxtAmt,$reqtxtCheque_Date,"Prize Payment",$reqtxtgroupbranch,$acc_trans_main_id);
	}
	*/
	
	// Outstanding Details
	fnUpdateOutstanding($reqtxtgroupbranch,$reqtxtGrp_No,$reqtxtAuction_No);
	// Outstanding Details
	//accounting related end  			
	if ($reqchkSMS =='YES'){
		$reqtxtFullName = fngetvalue("subdirdetails,applicantdetails","FullName","applicantdetails.SubID= subdirdetails.SubID and applicantdetails.AppID=$req_AppID");
		$main_data 		= array();
		$main_sql 		= "	select 
								subdirdetails.*,applicantdetails.main_branch_id as main_br_id,
								applicantdetails.GroupNo,applicantdetails.Ticketno 
							from 
								subdirdetails,applicantdetails 
							where 
								applicantdetails.SubID = subdirdetails.SubID and 
								applicantdetails.GroupNo = '$reqtxtGrp_No' ";
		$main_tmp 		= mysql_query($main_sql) or die(mysql_error().$main_sql);
		while($main_row = mysql_fetch_array($main_tmp)){
			$Phone_No 				= $main_row['MobNoCorres'];
			$SubID 					= $main_row['SubID'];
			// Table tbl_sur_sign_up Related Data
			$tbl_sur_sign_up_array 	= array();
			$tbl_sur_sign_up_array 	= fngetvalueMultiple('tbl_sur_sign_up','personnel_id,email,first_name',"ref_user_code=$SubID");
			$reqpersonnel_id 		= $tbl_sur_sign_up_array[0];
			$reqemail   		 	= $tbl_sur_sign_up_array[1];
			$firstname 	 			= $tbl_sur_sign_up_array[2];
			
		    $shop_url    			= WEBSITE_PATH . "index.php" ; 
			$shop_name   			= FnGetValue('tbl_mn_lhf','category_tag',"type='lhf' and name='shop_name' and lang='english'");
			$shop_name   			= 'eChit';
			//avdhut change
			$member_details 		= fngetmember_due($main_row['main_br_id'],$main_row['GroupNo'],$main_row['Ticketno']);
			$due					= $member_details[1];
			//----------------------
			$req_Report_ID 	= 1;
			$req_Message = FnShowEmail_Lang("dear_sir") ." ". $main_row['FullName'] . " ".
							//FnShowEmail_Lang("pr_auction_in_monthly_chit")." ". $reqtxtGrp_No ." ". FnShowEmail_Lang("of_Rs") ." ".$reqtxtchitvalue ." ".
							//FnShowEmail_Lang("was_held_on") ." ". $reqtxtAuction_Date ." ".FnShowEmail_Lang("at")." ". $reqtxtAuction_time ." ".
							FnShowEmail_Lang("pr_auction_in_monthly_chit")." ". $reqtxtGrp_No ." ".
							FnShowEmail_Lang("was_held_on") ." ". $main_row['AuctionDate'] ." ".
							FnShowEmail_Lang("prized_amt") ." ". $reqtxtAuction_Amt ." ".FnShowEmail_Lang("prized_to") ." ". $reqtxtFullName ." ".FnShowEmail_Lang("having_tkt_no")." ".$reqtxtTicket_No." ". FnShowEmail_Lang("dividend_is")." ".$reqlasdevidentt." ".
							FnShowEmail_Lang("regards");
							//FnShowEmail_Lang("pls_pay_sub_amt")." ".$due." ".FnShowEmail_Lang("before")." ".$LastDateofPayment." ".FnShowEmail_Lang("regards");
			echo $req_Message;
			fnSendSMS($Phone_No,$req_Message,$req_Report_ID);
			//send email 
			$to = $reqemail;
			// end email
		}
	}
	
}
function GetSubID($Branch){
    $BranchAbrv = FngetValue("tbl_branch","branch_code","main_branch_id='$Branch' and lang = '".$_SESSION['opt_lang']."'");
	$BranchAbrv_len=strlen($BranchAbrv)+2;
	$reqMaxSubID = FngetValue("subdirdetails","max( cast(SUBSTRING(`SubCode`, $BranchAbrv_len ) as unsigned) )","SubCode like '$BranchAbrv/%'");
			
	$reqMaxSubID = ($reqMaxSubID + 1);
	if($reqMaxSubID == '0')
		$reqSubID = $BranchAbrv."/1";
	else
		$reqSubID = $BranchAbrv."/".$reqMaxSubID;	
		
	return $reqSubID;	
}
function GetIntroCode($Branch){
    $BranchAbrv = FngetValue("tbl_branch","branch_code","main_branch_id='$Branch' and lang = '".$_SESSION['opt_lang']."'");
	$reqintroWord = "A";
	$BranchAbrv = $BranchAbrv."/".$reqintroWord;
	$BranchAbrv_len=strlen($BranchAbrv)+2;
	$reqMaxIntroCode = FngetValue("tbl_sur_sign_up","max( cast(SUBSTRING( `user_code` , $BranchAbrv_len ) as unsigned) )","user_code like '$BranchAbrv/%'");
			
	$reqMaxIntroCode = ($reqMaxIntroCode + 1);
	
	if($reqMaxIntroCode == '0')
		$reqMaxIntroCode = $BranchAbrv."/1";
	else
		$reqMaxIntroCode = $BranchAbrv."/".$reqMaxIntroCode;				
		
	return $reqMaxIntroCode;		
} 
function EnrollMember($reqtxtBranchCode,$reqtxtGrp_No,$reqtxtSubID,$reqtxtTicket_No,$reqchkSMS,$SubID,$reqchitvalue_share,$reqchit_share_perc){
	$grouparr=FngetValuemultiple("groupdetails","grpdate,Duration,GroupID,start_tk_no","GroupNo = '$reqtxtGrp_No'");	
	$reqGroupDate =$grouparr[0];
	$reqGroupDuration =$grouparr[1];
	$GroupID=$grouparr[2];
	$start_tk_no=$grouparr[3];
	 //$SubID=fngetvalue("subdirdetails","SubID","SubCode='$reqtxtSubID'");
	//$reqtxtTicket_No1 = FngetValue("applicantdetails","max(Ticketno)","GroupNo = '$reqtxtGrp_No'");

	//$reqtxtTicket_No = ($reqtxtTicket_No1 + 1);

    $tk_num_start_fr_maximum=fngetvalue("tbl_mn_app_setting","field_value","field_name='tk_num_start_fr_maximum'");
	//$reqChitValue = FnGetValue("groupdetails","ChitValue","GroupNo = '$reqtxtGrp_No' ");
	//$num_of_chitvalue=FnGetCount("groupdetails","ChitValue = '$reqChitValue'");
			
	if($tk_num_start_fr_maximum==1 ){		//&& $num_of_chitvalue>1 
				/*$groups=Fngetvalue("groupdetails","group_concat(GroupNo)","ChitValue='$reqChitValue'");						
						$len=strlen($groups);
						$grps="'";
						for($i=0;$i<$len;$i++){
								if($groups[$i]==","){
									$grps=$grps."'";
									$grps=$grps.$groups[$i];
									$grps=$grps."'";
								}else{
									$grps=$grps.$groups[$i];
								}
							
						}
						 $grps=$grps."'";*/
						 
				//$reqMax_Tk = FnGetValue("applicantdetails","max(Ticketno)"," GroupNo in ($grps) and GroupNo <> '$reqtxtGrp_No'");		
		   		$reqGroupDuration=$reqGroupDuration+$start_tk_no;//+$reqMax_Tk;
	}

	if ($reqtxtTicket_No <= $reqGroupDuration){
		$ins_sql = "Insert into `applicantdetails` (
					`AppID` ,`Ticketno` ,`GroupNo` ,`SubCodeNo`,`auctiondate` ,`amtinword` ,`prizeamtinword` ,`installmentinword` ,`installment` 
					,`amount` ,`Sustsubcode` ,`remdate` ,`prizeinstno`,`chrettime` ,`latetime` ,`main_branch_id`,`foreman_comm`					
					,`status` ,`Remove_status` ,`Service_Tax` ,`Verification_Charge` ,`Adjacement` ,`Netpayable_amount` ,`comment` ,`is_comp_acc` ,`SubID` ,`GroupID`,`chitvalue_share`,`chit_share_perc`
					
					) 
					values(NULL,'$reqtxtTicket_No','$reqtxtGrp_No','$reqtxtSubID','0000-00-00','','','',''				
					,'','','','','','','$reqtxtBranchCode','0',
					'non prized','','0','','','','','0','$SubID','$GroupID','$reqchitvalue_share','$reqchit_share_perc')";
		$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);
		$applicant_id=mysql_insert_id();
			
		include_once( 'acc_include_functions.php');
		$account_name 	 = FnGetValue("subdirdetails","FullName","SubID = '".$SubID."'");
			
		$is_prized_in_assets = FnGetValue("`tbl_mn_app_setting`","field_value","field_name = 'is_prized_in_assets'");
		if ($is_prized_in_assets==1){
		
				$account_type_id = FnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = '$reqtxtGrp_No' and account_type_nature = 'Liability'");
		}else{
				$account_type_id = FnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = '$reqtxtGrp_No' and account_type_nature = 'Assets'");
		}
		
		
	
		$related_id 	 = $applicant_id;
		$account_name	 = $reqtxtTicket_No."-".$account_name;
		create_account ($account_name,$account_type_id,"applicantdetails",$related_id,$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtBranchCode);
			
		if ($reqchkSMS =='YES'){
				$req_Report_ID = 1;
				$Phone_No = FnGetValue("subdirdetails","MobNoCorres","SubID = '".$reqtxtSubID."'");
				$FullName = FnGetValue("subdirdetails","FullName","SubID = '".$reqtxtSubID."'");
				$req_Message = FnShowEmail_Lang("dear_sir") ." ".$FullName." ". FnShowEmail_Lang("successfully_enroll_to_group")." ".$reqtxtGrp_No . " ". FnShowEmail_Lang("tkt_no_is")." ".$reqtxtTicket_No." ". FnShowEmail_Lang("thanks_regards");
				fnSendSMS($Phone_No,$req_Message,$req_Report_ID);

				$send_extra_enroll_sms=fngetvalue("tbl_mn_app_setting","field_value","field_name='send_extra_enroll_sms'");
				if ($send_extra_enroll_sms==1){
					$req_Message = "Dear sir ".$FullName." Please collect the receipt for the subscription amount paid to the collection executive by cash or cheque. Company will not be responsible for the amount paid without holding valid receipts. In case of cheque, cross it and write the group/tkt no and mobile no. on the back side ". FnShowEmail_Lang("thanks_regards");
					fnSendSMS($Phone_No,$req_Message,$req_Report_ID);
	
					$req_Message = "Dear sir ".$FullName." you will receive a confirmation SMS on the very next day of your payment, if not received please contact the branch office for verification ". FnShowEmail_Lang("thanks_regards");
					fnSendSMS($Phone_No,$req_Message,$req_Report_ID);
				}
				
				// send email
				$SubID = FnGetValue("subdirdetails","SubID","SubID = '".$reqtxtSubID."'");
				$reqpersonnel_id = FnGetValue("tbl_sur_sign_up","personnel_id","ref_user_code=$SubID");
				$to    = FnGetValue("tbl_sur_sign_up","email","personnel_id=$reqpersonnel_id");
				$firstname 	 = FnGetValue("tbl_sur_sign_up","first_name","personnel_id=$reqpersonnel_id");
				$shop_url    = WEBSITE_PATH . "index.php" ; 
				$shop_name   = FnGetValue("tbl_mn_lhf","category_tag","type='lhf' and name='shop_name' and lang='english'");
				$t->assign('firstname', $firstname); $t->assign('email', $to);  $t->assign('group_enrollment_msg', $req_Message);
				$t->assign('shop_url', $shop_url); $t->assign('shop_name', $shop_name);
				$msg = $t->fetch('emailtemplates/group_enrollment.html');
				if (eregi ("<title>(.*)</title>", $msg, $out)) { $subject = $out[1];  }
				fnSend_Email_Pear($to,$from,$subject,$msg,$attch_id);		 
		}	
		$sql_up = "Update groupforming SET Entrolled = '1',GroupNoEnrolled = '$reqtxtGrp_No',`GroupIDEnrolled`='$GroupID',TktNoEnrolled = '$reqtxtTicket_No' where SubID = '$SubID' and Entrolled=0";
		$res_up = mysql_query($sql_up) or die(mysql_error().$sql_up);		
	}
	
} 
function GetOutStanding($reqtxtBranchCode,$reqtxtGrp_No,$reqtxtTicket_No){
	$chitvalue_share 	= fnGetValue("applicantdetails","chit_share_perc","`Ticketno` = '$reqtxtTicket_No' and `GroupNo` = '$reqtxtGrp_No' and main_branch_id = '$reqtxtBranchCode'");	
	$payable=fngetvalue("auction","sum((auction.Subcription * $chitvalue_share)-(auction.lasdevidentt * $chitvalue_share))", "AuctionDateTime is not null and auction.main_branch_id ='$reqtxtBranchCode' and auction.GroupNo like '$reqtxtGrp_No'");
	$req_paid =array();
	$req_paid = FnGetValueMultiple("subscription_details  LEFT JOIN auction  ON (auction.Auction_ID = subscription_details.Auction_ID)  INNER JOIN transaction ON ( subscription_details.Transaction_ID = transaction.Transaction_ID )",
				"sum(subscription_details.Subcription),sum(subscription_details.lasdevidentt),sum(subscription_details.LateFee),sum(subscription_details.Subcription_Fee),sum(subscription_details.other_charge),sum(subscription_details.check_return_fee),sum(subscription_details.transfer_fee),sum(subscription_details.refund_amount),sum(subscription_details.ser_tax)",
				" transaction.status<2 and auction.main_branch_id ='$reqtxtBranchCode'  and auction.GroupNo = '$reqtxtGrp_No'  and subscription_details.Ticketno='$reqtxtTicket_No'");
	$Due = ($payable)- (($req_paid[0])-($req_paid[2]+ $req_paid[3]+ $req_paid[4]+ $req_paid[5]+ $req_paid[6]+ $req_paid[7]+ $req_paid[8]));
	return $Due;
}
function GetPaid_Dividend($reqtxtBranchCode,$reqtxtGrp_No,$reqtxtTicket_No,$chitvalue_share='',$InstNo='', $chit_share_perc='',$GroupID='',$todate=""){
	if ($GroupID !=""){
		$Group_field='GroupID';
		$reqtxtGrp_No = $GroupID;
	}else{
		$Group_field='GroupNo';
	}
	$cons_paid_div_in_due = fngetvalue("tbl_mn_app_setting","field_value","field_name='cons_paid_div_in_due'");

	$reqtxtchitvalue       	= fngetvalue("groupdetails","ChitValue","$Group_field='".$reqtxtGrp_No."'");		
	
	$appdet_array 			= fnGetValueMultipleNew("applicantdetails","GroupID,SubID,chit_share_perc","`Ticketno` = '$reqtxtTicket_No' and `$Group_field` = '$reqtxtGrp_No' and main_branch_id = '$reqtxtBranchCode'");	
	if($chit_share_perc == ""){
		$chit_share_perc 	= $appdet_array[2];	
	}
	if($InstNo != ""){
		$auction_array 			= fnGetValueMultipleNew("auction","sum(IF(auction.Subcription IS NULL,0,auction.Subcription)* $chit_share_perc ),sum(IF(auction.lasdevidentt IS NULL,0,auction.lasdevidentt)* $chit_share_perc )","AuctionDateTime is not null ".((COL_MACHINE_ONLY_TKT_NO !='1')? "":"and AuctionDateTime <=CURDATE()")." and auction.main_branch_id ='$reqtxtBranchCode' and auction.$Group_field like '$reqtxtGrp_No' and InstNo='$InstNo'");
	}else{
		$auction_array 			= fnGetValueMultipleNew("auction","sum(IF(auction.Subcription IS NULL,0,auction.Subcription)* $chit_share_perc ),sum(IF(auction.lasdevidentt IS NULL,0,auction.lasdevidentt)* $chit_share_perc )","AuctionDateTime is not null ".((COL_MACHINE_ONLY_TKT_NO !='1')? "":"and AuctionDateTime <=CURDATE()")." and auction.main_branch_id ='$reqtxtBranchCode' and auction.$Group_field like '$reqtxtGrp_No'");	
	}
	/*$reqSubscription_share 	= ($chitvalue_share / $reqtxtchitvalue) * $auction_array[0];			
	$reqlasdevidentt_share 	= ($chitvalue_share / $reqtxtchitvalue) * $auction_array[1];			
	$payable 	   	   		= $reqSubscription_share - $reqlasdevidentt_share;			*/
	$payable 	   	   		= $auction_array[0] - $auction_array[1];
	//$payable	= fngetvalue("auction","sum( IFNULL(auction.Subcription,0) - IFNULL(auction.lasdevidentt,0))", "AuctionDateTime is not null and auction.main_branch_id ='$reqtxtBranchCode' and auction.GroupNo like '$reqtxtGrp_No'");
	$where_condition = "";   
   	if($InstNo != ""){
		$where_condition = " and InstNo='$InstNo'";
	}
				
	$paid_sql ="SELECT auction.main_branch_id,auction.GroupNo,sub_det.Ticketno,auction.Auction_ID,auction.InstNO,
					auction.Subcription,auction.lasdevidentt,
					sum(IFNULL(sub_det.Subcription,0)) as paid,
					sum(IFNULL(sub_det.lasdevidentt,0)) as paid_dividend,
					sum(IFNULL(sub_det.LateFee,0)) as LateFee,
					sum(IFNULL(sub_det.Subcription_Fee,0)) as Subcription_Fee,
					sum(IFNULL(sub_det.other_charge,0)) as other_charge,
					sum(IFNULL(sub_det.check_return_fee,0)) as check_return_fee,
					sum(IFNULL(sub_det.transfer_fee,0) ) as transfer_fee,
					sum(IFNULL(sub_det.refund_amount,0) ) as refund_amount,
					sum(IFNULL(sub_det.ser_tax,0) ) as ser_tax
				FROM auction LEFT JOIN 
					( select Subcription, lasdevidentt, LateFee, Subcription_Fee, other_charge, check_return_fee,refund_amount,ser_tax, Auction_ID,
						transfer_fee,subscription_details.Transaction_ID,subscription_details.Ticketno 
					  from subscription_details LEFT JOIN transaction ON ( subscription_details.Transaction_ID = transaction.Transaction_ID ) 
					  where transaction.status<2	and subscription_details.Ticketno='$reqtxtTicket_No' ".(($todate!="")? "and transaction.Reciept_Date<='".$todate."'":"")."			
					 ) as sub_det ON (auction.Auction_ID = sub_det.Auction_ID ) 							
				WHERE  auction.main_branch_id ='$reqtxtBranchCode' and 
					auction.$Group_field = '$reqtxtGrp_No' and AuctionDateTime is not null ".((COL_MACHINE_ONLY_TKT_NO !='1')? "":"and AuctionDateTime <=CURDATE()")."
					 $where_condition ".(($todate!="")? "and left(AuctionDateTime,10)<='".$todate."'":"")."
				GROUP BY 
					auction.main_branch_id,auction.Auction_ID
				ORDER BY auction.main_branch_id  asc,auction.Auction_ID asc ";	
	//if ($reqtxtTicket_No==21) echo 	$paid_sql."<BR><BR>";	
	//echo 	$paid_sql."<BR><BR>";	
	$sql_result = mysql_query($paid_sql) or die(mysql_error().$paid_sql);
	$payable 	=0;
	$Due 		=0;
	$devident 	=0;
	$Paid 		=0;
	
	$LastInstNo =0;
	$TotCharges =0;
	$tot_other_charge=0;
	$act_devident=0;
	$tot_Subcription_Fee=0;

	while($row=mysql_fetch_array($sql_result)){
		$instpayable=0;
		$instpayable_plus=0;
		$instdue=0;
		$LastInstDue=0;
		if ($cons_paid_div_in_due!=1){
			$instpayable = ($row["Subcription"]*$chit_share_perc)-($row["lasdevidentt"]*$chit_share_perc);
		}else{	
			if ($row["paid"] > 0 ){
				$instpayable = ($row["Subcription"]*$chit_share_perc)-$row["paid_dividend"];
			}else{
				$instpayable = ($row["Subcription"]*$chit_share_perc)-($row["lasdevidentt"]*$chit_share_perc);
			}		
		}
		$instpayable_plus= $row["LateFee"]+$row["Subcription_Fee"]+$row["other_charge"]+$row["check_return_fee"]+$row["transfer_fee"]+$row["refund_amount"]+$row["ser_tax"];
		$instpayable = $instpayable + $instpayable_plus;
		
		$instdue	 = $instpayable- $row["paid"];
		$payable 	+= $instpayable ;
		//if ($reqtxtTicket_No==21) echo "(".$row["paid"]."-".$instpayable_plus.")";
		
		$Paid 		+= ($row["paid"]-$instpayable_plus);
		$devident 	+= $row["paid_dividend"];
		$act_devident 	+= $row["lasdevidentt"];
		
		$tot_other_charge += $row["other_charge"];
		$tot_Subcription_Fee += $row["Subcription_Fee"];

		$Due 		+= $instdue;
		$LastInstDue = $instdue;
		$LastInstNo  = $row["InstNO"];
		$TotCharges	+= $instpayable_plus; 
		//if ($reqtxtTicket_No==21) echo $row["InstNO"]."|".$instpayable."-".$row["paid"]."=".$instdue."</br>";
	}
	if ($InstNo==""){
		$expenses_on_cust = fngetvalue("tbl_expense_on_cust","sum(amount)","Ticketno = '$reqtxtTicket_No' and GroupID = '".$appdet_array[0]."' and SubID = '".$appdet_array[1]."' and main_branch_id = '$reqtxtBranchCode' ".(($todate!="")? "and exp_date <='".$todate."'":""));
		if ($tot_other_charge >= 	$expenses_on_cust)	{
			$Due = $Due ;	
		}else{
			$Due = $Due + $expenses_on_cust;
		}
	}
	$Val		= array();	
    $Val[0]		= $Paid;
	$Val[1]		= $devident;
	$Val[2]		= $Due;
	$Val[3]		= $payable;
	$Val[4]		= $LastInstDue;
	$Val[5]		= $LastInstNo;
	$Val[6]		= $TotCharges;
	$Val[7]		= $expenses_on_cust;
	$Val[8]		= $act_devident;
	$Val[9]		= $tot_Subcription_Fee;

	return $Val;
}
function Get_Div_Adjustment_Rem($reqtxtBranchCode,$reqtxtGrp_No,$reqtxtTicket_No){
	$reqtxtchitvalue = fngetvalue("groupdetails","ChitValue","GroupNo='".$reqtxtGrp_No."'");		
	$chit_share_perc = fnGetValue("applicantdetails","chit_share_perc","`Ticketno` = '$reqtxtTicket_No' and `GroupNo` = '$reqtxtGrp_No' and main_branch_id = '$reqtxtBranchCode'");	

	$total_dividend  = fnGetValue("auction","sum(IF(auction.lasdevidentt IS NULL,0,auction.lasdevidentt)* $chit_share_perc )"," auction.main_branch_id ='$reqtxtBranchCode' and auction.GroupNo like '$reqtxtGrp_No'");	
	
	$payable 	   	 = $total_dividend;

	$req_paid 	= array();
	$req_paid 	= FnGetValueMultipleNew("
					(subscription_details  
						LEFT JOIN auction ON (auction.Auction_ID = subscription_details.Auction_ID)  
						INNER JOIN transaction ON ( subscription_details.Transaction_ID = transaction.Transaction_ID ))
						LEFT JOIN transaction_details ON ( transaction_details.trans_id = transaction.Transaction_ID )
				","
					sum(IFNULL(subscription_details.Subcription,0)) as Subcription,
					sum(IFNULL(subscription_details.lasdevidentt,0)) as lasdevidentt,
					sum(IFNULL(subscription_details.LateFee,0)) as LateFee,
					sum(IFNULL(subscription_details.Subcription_Fee,0)) as Subcription_Fee,
					sum(IFNULL(subscription_details.other_charge,0)) as other_charge,
					sum(IFNULL(subscription_details.check_return_fee,0)) as check_return_fee,
					sum(IFNULL(subscription_details.transfer_fee,0) ) as transfer_fee, sum(IFNULL(subscription_details.refund_amount,0) ) as refund_amount, sum(IFNULL(subscription_details.ser_tax,0) ) as ser_tax,
					MAX(InstNo) as InstNo
				"," 
					transaction.status < 2 and 
					auction.main_branch_id ='$reqtxtBranchCode'  and 
					auction.GroupNo = '$reqtxtGrp_No'  and 
					subscription_details.Ticketno='$reqtxtTicket_No' and 
					transaction_details.payment_by = 'div_adj'
					$where_condition 
				GROUP BY 
					auction.main_branch_id,auction.GroupNo,subscription_details.Ticketno");    
	$Val		= array();	
	$Paid 		= $req_paid[0] - ($req_paid[2]+ $req_paid[3]+ $req_paid[4]+ $req_paid[5]+ $req_paid[6]+ $req_paid[7]+ $req_paid[8]);
	$devident 	= $req_paid[1] ;
	$Due 		= ($payable) - ($Paid);	

    $Val[0]		= $Paid;
	$Val[1]		= $Due;
	$Val[2]		= $devident;
	$Val[3]		= $req_paid[9];
	$Val[4]		= $req_paid[0];
	$Val[5]		= $req_paid[3];
	$Val[6]		= $payable;
	return $Val;
}
//-----------------------------------delete enrol member---------------------------------------------------------------------
function delete_enrol_memeber($reqApp_ID){
 						
					$reqSubCodeNo = fnGetValue("applicantdetails","SubCodeNo","AppID='$reqApp_ID'");
					$reqGroup_No = fnGetValue("applicantdetails","GroupNo","AppID='$reqApp_ID'");
					$reqTicket_No = fnGetValue("applicantdetails","Ticketno","AppID='$reqApp_ID'");	
					$reqmain_branch_id=fnGetValue("enroll_payment","main_branch_id ","Group_No='$reqGroup_No'and ticket_no ='$reqTicket_No' and Sub_ID='$reqSubCodeNo'");	
					$main_acc_account_id= fnGetValue("tbl_acc_accounts","main_acc_account_id","related_type='applicantdetails'and related_id='$reqApp_ID'");	
					$sqlDelsub = "delete subscription_details.*,transaction.*,transaction_details.* 
					 from ((((applicantdetails Inner Join subscription_details ON applicantdetails.SubCodeNo = subscription_details.SubID AND applicantdetails.Ticketno = '$reqTicket_No' )
					 Inner Join auction ON subscription_details.Auction_ID = auction.Auction_ID) 	
					 Inner Join transaction ON subscription_details.Transaction_ID=transaction.Transaction_ID)
					 Inner Join  transaction_details ON transaction.Transaction_ID = transaction_details.trans_id)
					 WHERE AppID = '$reqApp_ID' and auction.GroupNo = '$reqGroup_No' and  applicantdetails.Ticketno = '$reqTicket_No' AND subscription_details.Ticketno = '$reqTicket_No' ";
				
					$sqlDelete_result1 = mysql_query($sqlDelsub) or die(mysql_error().$sqlDelsub);
				
					$sqlDelenr= "delete enroll_payment.*,transaction.*,transaction_details.* 
								 from enroll_payment Inner Join transaction ON enroll_payment.transaction_ID=transaction.Transaction_ID
								 Inner Join  transaction_details ON transaction.Transaction_ID = transaction_details.trans_id
								 WHERE enroll_payment.Group_No='$reqGroupNo' and enroll_payment.ticket_no = '$reqTicketno' and enroll_payment.main_branch_id='$reqmain_branch_id' ";
					
		
					$sqlDelete_result2 = mysql_query($sqlDelenr) or die(mysql_error().$sqlDelenr);
					
					$voucher_type= fnGetValue("tbl_acc_voucher_type","main_voucher_type_id","voucher_type_name='Journal'");
					$sqldata="select distinct acc_transaction_main_id from tbl_acc_transaction where from_acc_id='$main_acc_account_id'";
					$data1=array();
					$rec=mysql_query($sqldata) or die(mysql_error().$sqldata);
					while($row=mysql_fetch_array($rec)){
						$reqacc_transaction_main_id=$row['acc_transaction_main_id'];
						$sql_1="delete from tbl_acc_transaction WHERE tbl_acc_transaction.acc_transaction_main_id='$reqacc_transaction_main_id'";		
						mysql_query($sql_1) or die(mysql_error().$sql_1);
						
						$sql_2="delete from tbl_acc_transaction_main WHERE acc_transaction_main_id='$reqacc_transaction_main_id'and voucher_type<>'$voucher_type'";		
						 mysql_query($sql_2) or die(mysql_error().$sql_2);

						$sql_3="delete from tbl_acc_relate WHERE acc_transaction_id='$reqacc_transaction_main_id'";		
						mysql_query($sql_3) or die(mysql_error().$sql_3);

					}					
					$sqlaccdelete1="delete from tbl_acc_accounts where main_acc_account_id='$main_acc_account_id'";		
					 mysql_query($sqlaccdelete1) or die(mysql_error().$sqlaccdelete1);
					
					$sqldel="delete from applicantdetails where AppID = '$reqApp_ID'";	
					 mysql_query($sqldel) or die(mysql_error().$sqldel);									
}
//get paid for removed member-----------
function GetPaid_Dividend_removed($reqtxtBranchCode,$reqtxtGrp_No,$reqtxtTicket_No,$SubID,$chitvalue_share='',$InstNo='', $chit_share_perc='',$GroupID='',$todate=""){
	if ($GroupID !=""){
		$Group_field='GroupID';
		$reqtxtGrp_No = $GroupID;
	}else{
		$Group_field='GroupNo';
	}
	$cons_paid_div_in_due = fngetvalue("tbl_mn_app_setting","field_value","field_name='cons_paid_div_in_due'");

	$reqtxtchitvalue       	= fngetvalue("groupdetails","ChitValue","$Group_field='".$reqtxtGrp_No."'");		
	
	$appdet_array 			= fnGetValueMultipleNew("applicantdetails_removed","GroupID,SubID,chit_share_perc","`Ticketno` = '$reqtxtTicket_No' and `$Group_field` = '$reqtxtGrp_No' and main_branch_id = '$reqtxtBranchCode' and SubID='$SubID'");	
	if($chit_share_perc == ""){
		$chit_share_perc 	= $appdet_array[2];	
	}
	if($InstNo != ""){
		$auction_array 			= fnGetValueMultipleNew("auction","sum(IF(auction.Subcription IS NULL,0,auction.Subcription)* $chit_share_perc ),sum(IF(auction.lasdevidentt IS NULL,0,auction.lasdevidentt)* $chit_share_perc )","AuctionDateTime is not null ".((COL_MACHINE_ONLY_TKT_NO !='1')? "":"and AuctionDateTime <=CURDATE()")." and auction.main_branch_id ='$reqtxtBranchCode' and auction.$Group_field like '$reqtxtGrp_No' and InstNo='$InstNo'");
	}else{
		$auction_array 			= fnGetValueMultipleNew("auction","sum(IF(auction.Subcription IS NULL,0,auction.Subcription)* $chit_share_perc ),sum(IF(auction.lasdevidentt IS NULL,0,auction.lasdevidentt)* $chit_share_perc )","AuctionDateTime is not null ".((COL_MACHINE_ONLY_TKT_NO !='1')? "":"and AuctionDateTime <=CURDATE()")." and auction.main_branch_id ='$reqtxtBranchCode' and auction.$Group_field like '$reqtxtGrp_No'");	
	}

	$payable 	   	   		= $auction_array[0] - $auction_array[1];
	$where_condition = "";   
   	if($InstNo != ""){
		$where_condition = " and InstNo='$InstNo'";
	}

	$paid_sql ="SELECT auction.main_branch_id,auction.GroupNo,sub_det.Ticketno,auction.Auction_ID,auction.InstNO,
					auction.Subcription,auction.lasdevidentt,
					sum(IFNULL(sub_det.Subcription,0)) as paid,
					sum(IFNULL(sub_det.lasdevidentt,0)) as paid_dividend,
					sum(IFNULL(sub_det.LateFee,0)) as LateFee,
					sum(IFNULL(sub_det.Subcription_Fee,0)) as Subcription_Fee,
					sum(IFNULL(sub_det.other_charge,0)) as other_charge,
					sum(IFNULL(sub_det.check_return_fee,0)) as check_return_fee,
					sum(IFNULL(sub_det.transfer_fee,0) ) as transfer_fee,
					sum(IFNULL(sub_det.refund_amount,0) ) as refund_amount,
					sum(IFNULL(sub_det.ser_tax,0) ) as ser_tax
				FROM auction LEFT JOIN 
					( select Subcription, lasdevidentt, LateFee, Subcription_Fee, other_charge, check_return_fee,refund_amount,ser_tax, Auction_ID,
						transfer_fee,subscription_details_removed.Transaction_ID,subscription_details_removed.Ticketno 
					  from subscription_details_removed LEFT JOIN transaction_removed ON ( subscription_details_removed.Transaction_ID = transaction_removed.Transaction_ID_Act ) 
					  where transaction_removed.status<2	and subscription_details_removed.Ticketno='$reqtxtTicket_No' and SubID='$SubID' ".(($todate!="")? "and transaction_removed.Reciept_Date<='".$todate."'":"")."			
					 ) as sub_det ON (auction.Auction_ID = sub_det.Auction_ID ) 							
				WHERE  auction.main_branch_id ='$reqtxtBranchCode' and 
					auction.$Group_field = '$reqtxtGrp_No' and AuctionDateTime is not null ".((COL_MACHINE_ONLY_TKT_NO !='1')? "":"and AuctionDateTime <=CURDATE()")."
					 $where_condition ".(($todate!="")? "and left(AuctionDateTime,10)<='".$todate."'":"")."
				GROUP BY 
					auction.main_branch_id,auction.Auction_ID
				ORDER BY auction.main_branch_id  asc,auction.Auction_ID asc ";	
	$sql_result = mysql_query($paid_sql) or die(mysql_error().$paid_sql);
	$payable 	=0;
	$Due 		=0;
	$devident 	=0;
	$Paid 		=0;
	
	$LastInstNo =0;
	$TotCharges =0;
	$tot_other_charge=0;
	$act_devident=0;
	$tot_Subcription_Fee=0;
	while($row=mysql_fetch_array($sql_result)){
		$instpayable=0;
		$instpayable_plus=0;
		$instdue=0;
		$LastInstDue=0;
		if ($cons_paid_div_in_due!=1){
			$instpayable = ($row["Subcription"]*$chit_share_perc)-($row["lasdevidentt"]*$chit_share_perc);
		}else{	
			if ($row["paid"] > 0 ){
				$instpayable = ($row["Subcription"]*$chit_share_perc)-$row["paid_dividend"];
			}else{
				$instpayable = ($row["Subcription"]*$chit_share_perc)-($row["lasdevidentt"]*$chit_share_perc);
			}		
		}
		$instpayable_plus= $row["LateFee"]+$row["Subcription_Fee"]+$row["other_charge"]+$row["check_return_fee"]+$row["transfer_fee"]+$row["refund_amount"]+$row["ser_tax"];
		$instpayable = $instpayable + $instpayable_plus;
		
		$instdue	 = $instpayable- $row["paid"];
		$payable 	+= $instpayable ;
		
		$Paid 		+= ($row["paid"]-$instpayable_plus);
		$devident 	+= $row["paid_dividend"];
		$act_devident 	+= $row["lasdevidentt"];
		
		$tot_other_charge += $row["other_charge"];

		$tot_Subcription_Fee += $row["Subcription_Fee"];

		$Due 		+= $instdue;
		$LastInstDue = $instdue;
		$LastInstNo  = $row["InstNO"];
		$TotCharges	+= $instpayable_plus; 
	}
	if ($InstNo==""){
		$expenses_on_cust = fngetvalue("tbl_expense_on_cust","sum(amount)","Ticketno = '$reqtxtTicket_No' and GroupID = '".$appdet_array[0]."' and SubID = '".$appdet_array[1]."' and main_branch_id = '$reqtxtBranchCode' ".(($todate!="")? "and exp_date <='".$todate."'":""));
		if ($tot_other_charge >= 	$expenses_on_cust)	{
			$Due = $Due ;	
		}else{
			$Due = $Due + $expenses_on_cust;
		}
	}
	$Val		= array();	
    $Val[0]		= $Paid;
	$Val[1]		= $devident;
	$Val[2]		= $Due;
	$Val[3]		= $payable;
	$Val[4]		= $LastInstDue;
	$Val[5]		= $LastInstNo;
	$Val[6]		= $TotCharges;
	$Val[7]		= $expenses_on_cust;
	$Val[8]		= $act_devident;
	$Val[9]		= $tot_Subcription_Fee;
	
/*	$chitvalue_share 	= fnGetValue("applicantdetails_removed","chit_share_perc","`Ticketno` = '$reqtxtTicket_No' and `GroupNo` = '$reqtxtGrp_No' and main_branch_id = '$reqtxtBranchCode' and SubID='$SubID'");	

	$req_paid =array();
	$req_paid = FnGetValueMultiple("subscription_details_removed  LEFT JOIN auction  ON (auction.Auction_ID = subscription_details_removed.Auction_ID)  INNER JOIN transaction_removed ON ( subscription_details_removed.Transaction_ID = transaction_removed.Transaction_ID_Act )",
				"sum(subscription_details_removed.Subcription),
				sum(subscription_details_removed.lasdevidentt),
				sum(subscription_details_removed.LateFee),
				sum(subscription_details_removed.Subcription_Fee),
				sum(subscription_details_removed.other_charge),
				sum(subscription_details_removed.check_return_fee),
				sum(subscription_details_removed.transfer_fee),
				sum(subscription_details_removed.refund_amount),
				sum(subscription_details_removed.ser_tax),
				MAX(InstNo)",
				"transaction_removed.status not in(2,3) and auction.main_branch_id ='$reqtxtBranchCode'  and auction.GroupNo = '$reqtxtGrp_No'  and subscription_details_removed.Ticketno='$reqtxtTicket_No' and SubID='$SubID' GROUP BY auction.main_branch_id,auction.GroupNo,subscription_details_removed.Ticketno");    //kailas
	$Val=array();
	
	$Paid = $req_paid[0] - ($req_paid[2]+ $req_paid[3]+ $req_paid[4]+ $req_paid[5]+ $req_paid[6]+ $req_paid[7]+ $req_paid[8]);
	$devident = $req_paid[1] ;
	$Due = ($payable) - ($Paid);	

    $Val[0]=$Paid;
	$Val[1]=$devident;
	$Val[2]=$Due;
	$Val[3]=$req_paid[9];
	$Val[4]=$req_paid[0];
	$Val[5]=$req_paid[3];*/
	return $Val;
}
//make removed customer live
function fnmake_member_live($reqAppID){		
	$reqGroupNo = FngetValue("applicantdetails","GroupNo"," AppID= '$reqAppID'");
	$reqTicketno = FngetValue("applicantdetails","Ticketno","AppID = '$reqAppID'"); 		
	$reqmain_branch_id = FngetValue("applicantdetails","main_branch_id","AppID = '$reqAppID'");
	
	$SubID=Fngetvalue("applicantdetails","SubCodeNo","AppID = '$reqAppID'");  		

	   
	$Insert_enrollfee_sql = "INSERT INTO `enroll_payment` (`enroll_payment_id`, `main_branch_id`, `ticket_no`, `Group_No`, `Sub_ID`, `transaction_ID`, `Amount`) 
							   SELECT NULL as `enroll_payment_id`,`main_branch_id`, `ticket_no`, `Group_No`, `Sub_ID`, `transaction_ID`, `Amount`
							   FROM enroll_payment_removed WHERE Group_No='$reqGroupNo' and ticket_no = '$reqTicketno' and main_branch_id='$reqmain_branch_id' and Sub_ID='$SubID'";
  	$Insert_enrollfee_sql_result = mysql_query($Insert_enrollfee_sql) or die(mysql_error().$Insert_enrollfee_sql);


	$Insert_subsdetails_sql = "INSERT INTO `subscription_details` (`Subscription_ID`,`Auction_ID`,`TicketNo`,`Transaction_ID`,`SubID`,`LateFee`,`lasdevidentt`,`arrears`,`main_branch_id`,`Subcription`,`Subcription_Fee`,`other_charge`,`check_return_fee`,`transfer_fee`,`refund_amount`,`ser_tax`)
							   SELECT NULL as `Subscription_ID`,`subscription_details_removed`.`Auction_ID` as Auction_ID,`TicketNo`,`Transaction_ID`,`SubID`,`LateFee`,`subscription_details_removed`.`lasdevidentt` as lasdevidentt,`arrears`,`subscription_details_removed`.`main_branch_id` as main_branch_id,`subscription_details_removed`.`Subcription` as Subcription,`subscription_details_removed`.`Subcription_Fee` as Subcription_Fee,`subscription_details_removed`.`other_charge` as other_charge,`check_return_fee`,`transfer_fee`,`refund_amount`,`ser_tax`
							   FROM (subscription_details_removed Inner Join auction ON subscription_details_removed.Auction_ID = auction.Auction_ID) WHERE auction.GroupNo='$reqGroupNo' and Ticketno = $reqTicketno and SubID='$SubID'";
  	$Insert_subsdetails_sql_result = mysql_query($Insert_subsdetails_sql) or die(mysql_error().$Insert_subsdetails_sql);

	 $Trans_ids=fngetvalue("(subscription_details_removed Inner Join auction ON subscription_details_removed.Auction_ID = auction.Auction_ID Inner Join transaction_removed ON subscription_details_removed.Transaction_ID = transaction_removed.Transaction_ID_Act)","group_concat(DISTINCT subscription_details_removed.Transaction_ID)","auction.GroupNo='$reqGroupNo' and Ticketno = $reqTicketno and SubID='$SubID'");
    $advance_trans_ids=Fngetvalue("tbl_advance_removed","group_concat(Transaction_ID)","Group_No='$reqGroupNo' and Ticket_No='$reqTicketno' and main_branch_id='$reqmain_branch_id' and SubID='$SubID' ");
	if (($advance_trans_ids <> "" )&& ($advance_trans_ids <> 0)){
		 $Trans_ids = $Trans_ids.",".$advance_trans_ids;
	}elseif (($Trans_ids == "" )|| ($Trans_ids == 0)){
		$Trans_ids = $advance_trans_ids;
	}
	//echo $Trans_ids;
	if (($Trans_ids <> "" )&& ($Trans_ids <> 0)){
		$insert_trans_details_sql = "INSERT INTO `transaction` (`Transaction_ID`,`ReceiptNo`,`Reciept_Date`,`TRNO`,`TRDate`,`Amount`,`Remark`,`ChequeNo`,`Bankname`,`ChequeDate`,`printreciept`,`Area`,`CheRetTime`,`Cheretfee`,`DayBookNo`,`main_branch_id`,`receipt_type`,`payment_by`,`status`,`receipt_enter_by`,`collection_code`,`receipt_flag`)
									SELECT  transaction_removed.`Transaction_ID_Act` as `Transaction_ID`,`transaction_removed`.`ReceiptNo` as ReceiptNo,`Reciept_Date`,`TRNO`,`TRDate`,`Amount`,`Remark`,`ChequeNo`,`Bankname`,`ChequeDate`,`printreciept`,`Area`,`CheRetTime`,`Cheretfee`,`DayBookNo`,`transaction_removed`.`main_branch_id` as main_branch_id,`receipt_type`,`payment_by`,`transaction_removed`.`status` as `status`,`transaction_removed`.`receipt_enter_by`,`transaction_removed`.`collection_code`,`receipt_flag`
									FROM transaction_removed
									WHERE  Transaction_ID_Act in ($Trans_ids)";							
		$insert_trans_details_sql_result = 	mysql_query($insert_trans_details_sql) or die(mysql_error().$insert_trans_details_sql);			
					
		
		$insert_transdetails_sql = "INSERT INTO `transaction_details` (`Transaction_detail_ID`,`Amount`,`Remark`,`ChequeNo`,`Bankname`,`ChequeDate`,
										`CheRetTime`,`Cheretfee`,`receipt_type`,`payment_by`,`trans_id`,`status`,`dep_slip_ID`,`main_branch_id`,
										value_chequereturn_date,value_comment,adjustment_ID,neft_bank)
									SELECT 	NULL as `Transaction_detail_ID`,`transaction_details_removed`.`Amount` as Amount,`transaction_details_removed`.`Remark` as Remark,
									`transaction_details_removed`.`ChequeNo` as ChequeNo,`transaction_details_removed`.`Bankname` as Bankname,`transaction_details_removed`.`ChequeDate` as ChequeDate,
									`transaction_details_removed`.`CheRetTime` as CheRetTime,`transaction_details_removed`.`Cheretfee` as Cheretfee,`transaction_details_removed`.`receipt_type` as receipt_type,
									`transaction_details_removed`.`payment_by` as payment_by,`transaction_details_removed`.`trans_id` as trans_id,`transaction_details_removed`.`status` as status,
									`transaction_details_removed`.`dep_slip_ID` as dep_slip_ID,`transaction_details_removed`.`main_branch_id` as main_branch_id,
									value_chequereturn_date,value_comment,adjustment_ID,neft_bank
									FROM  transaction_details_removed
									WHERE trans_id in ($Trans_ids)";
		$insert_transdetails_sql_result = mysql_query($insert_transdetails_sql) or die(mysql_error().$insert_transdetails_sql);						
	}

	$advance_ids=Fngetvalue("tbl_advance_removed","group_concat(Advance_ID)","Group_No='$reqGroupNo' and Ticket_No='$reqTicketno' and main_branch_id='$reqmain_branch_id' and SubID='$SubID' ");
	if (($advance_ids <> "" )&& ($advance_ids <> 0)){
		$insert_advance_removed="INSERT INTO `tbl_advance_payment` (`Advance_ID`,`Group_No`,`Ticket_No`,`Transaction_ID`,`SubID`,`main_branch_id`,`Advance_Amount`,LateFee,Subcription_Fee,other_charge,check_return_fee,transfer_fee)
									SELECT 	`Advance_ID`as Advance_ID,`Group_No`as Group_No,`Ticket_No` as Ticket_No,`Transaction_ID`as Transaction_ID,`SubID` as SubID,
									`main_branch_id` as main_branch_id,`Advance_Amount` as Advance_Amount,LateFee as LateFee,Subcription_Fee as Subcription_Fee,other_charge as other_charge,check_return_fee as check_return_fee,transfer_fee as transfer_fee
									FROM  tbl_advance_removed
									WHERE Advance_ID in ($advance_ids)";
		$insert_advance_removed_result= mysql_query($insert_advance_removed) or die(mysql_error().$insert_advance_removed);	
	
		$delete_advance_payment="delete from tbl_advance_removed where Advance_ID in ($advance_ids)";
		$delete_advance_payment_result= mysql_query($delete_advance_payment) or die(mysql_error().$delete_advance_payment);	
	}
	$sqlDelete = "delete subscription_details_removed.*,transaction_removed.*,transaction_details_removed.* 
				 from ((((applicantdetails_removed Inner Join subscription_details_removed ON applicantdetails_removed.SubCodeNo = subscription_details_removed.SubID AND applicantdetails_removed.Ticketno = $reqTicketno)
			 	 Inner Join auction ON subscription_details_removed.Auction_ID = auction.Auction_ID) 	
			 	 Inner Join transaction_removed ON subscription_details_removed.Transaction_ID=transaction_removed.Transaction_ID_Act)
			 	 Inner Join  transaction_details_removed ON transaction_removed.Transaction_ID_Act = transaction_details_removed.trans_id)
			 	 WHERE AppID = '$reqAppID' and auction.GroupNo = '$reqGroupNo' and  applicantdetails_removed.Ticketno = $reqTicketno AND subscription_details_removed.Ticketno = $reqTicketno ";
		
	$sqlDelete_result = mysql_query($sqlDelete) or die(mysql_error().$sqlDelete);

	$sqlDelete = "delete enroll_payment_removed.*,transaction_removed.*,transaction_details_removed.* 
				 from enroll_payment_removed Inner Join transaction_removed ON enroll_payment_removed.transaction_ID=transaction_removed.Transaction_ID_Act
			 	 Inner Join  transaction_details_removed ON transaction_removed.Transaction_ID = transaction_details_removed.trans_id
			 	 WHERE enroll_payment_removed.Group_No='$reqGroupNo' and enroll_payment_removed.ticket_no = '$reqTicketno' and enroll_payment_removed.main_branch_id='$reqmain_branch_id' ";
		
	$sqlDelete_result = mysql_query($sqlDelete) or die(mysql_error().$sqlDelete);

	$sqlins	 = "update applicantdetails SET Remove_status = '' WHERE AppID= $reqAppID ";		
	$result = mysql_query($sqlins)	or die(mysql_error().$sqlins);	
	
	$intro_sql="update tbl_introducer_payment_details  set status='' where `GroupNo`='$reqGroupNo' and `Ticket_No`='$reqTicketno' and `main_branch_id`='$reqmain_branch_id' and `SubID`='$SubID' ";		
	$update_intro_details= mysql_query($intro_sql)	or die(mysql_error().$intro_sql);	

     $Insert_appdetails_sql= "delete from `applicantdetails_removed` where AppID='$reqAppID' and SubCodeNo='$SubID'";
   	$Insert_details_sql_result = mysql_query($Insert_appdetails_sql) or die(mysql_error().$Insert_appdetails_sql);	
    


}
//----------------------------member due--------------------------
function fngetmember_due($Branch,$Group_no,$Tkt){
	$arr = array();
	$arr = fngetValueMultiple(	"applicantdetails","GroupID,SubID,chit_share_perc","Ticketno = '$Tkt' and GroupNo = '$Group_no' and main_branch_id = '$Branch'");
	$GroupID = $arr[0];
	$SubID = $arr[1];
	$chit_share_perc = $arr[2];


	$outstanding_from_group_date = fngetvalue("tbl_mn_app_setting","field_value","field_name='outstanding_from_group_date'");
	
	if (($GroupID!="") && ($GroupID!=0)){ 
		if (COL_MACHINE_ONLY_TKT_NO==1){
			$reqGroupDet=fngetvaluemultiple("groupdetails","ChitValue,Duration","GroupID='".$GroupID."'");		
			$reqChitValue=$reqGroupDet[0];	
			$reqDuration=$reqGroupDet[1];		
			$subscription = $reqChitValue / $reqDuration;
			if ($subscription=="") $subscription=0;
			if ($outstanding_from_group_date==1){
				$sql = "SELECT applicantdetails.main_branch_id,applicantdetails.SubID,applicantdetails.GroupNo as GroupNo, applicantdetails.Ticketno as TicketNo, grpdate,NOW(),(DATEDIFF(NOW(),grpdate)*($subscription)) as payble,
							sum(IFNULL(subscription_details.Subcription,0)) as collection,((DATEDIFF(ADDTIME( NOW() , '05:30' ),grpdate)*($subscription))- sum(IFNULL(subscription_details.Subcription,0))) as Due,
							DATEDIFF(NOW(),grpdate) as noofinst, ((DATEDIFF(NOW(),grpdate)*($subscription))- sum(IFNULL(subscription_details.Subcription,0)))/ (groupdetails.ChitValue/Duration) as instdue ,
							ADDTIME( NOW() , '05:30' ) as today						
						FROM applicantdetails
							LEFT JOIN subscription_details ON (applicantdetails.SubID = subscription_details.SubID and applicantdetails.Ticketno = subscription_details.TicketNo)
							LEFT JOIN auction ON (subscription_details.Auction_ID=auction.Auction_ID) 
							LEFT JOIN groupdetails ON (applicantdetails.GroupID=groupdetails.GroupID) 
							LEFT JOIN subdirdetails ON (applicantdetails.SubID = subdirdetails.SubID) 
							LEFT JOIN transaction ON ( subscription_details.Transaction_ID = transaction.Transaction_ID ) 		
						WHERE (transaction.status < 2 or transaction.status is null) and applicantdetails.main_branch_id = '$Branch' and 
							applicantdetails.GroupNo like '$Group_no' and 
							applicantdetails.Ticketno = '$Tkt'"; 					
			}else{
				$sql = "SELECT auction.main_branch_id,subscription_details.SubID,auction.GroupNo,TicketNo, FormRecOn,NOW(),(DATEDIFF(NOW(),FormRecOn)*$subscription) as payble,
							sum(subscription_details.Subcription) as collection,((DATEDIFF(ADDTIME( NOW() , '05:30' ),FormRecOn)*$subscription)- sum(subscription_details.Subcription)) as Due,
							DATEDIFF(NOW(),FormRecOn) as noofinst, ((DATEDIFF(NOW(),FormRecOn)*$subscription)- sum(subscription_details.Subcription))/ $subscription as instdue ,
							ADDTIME( NOW() , '05:30' ) as today
						FROM subscription_details  
							LEFT JOIN auction ON (subscription_details.Auction_ID=auction.Auction_ID) 
							LEFT JOIN groupforming ON (groupforming.SubID = subscription_details.SubID and groupforming.TktNoEnrolled = subscription_details.TicketNo) 
							INNER JOIN transaction ON ( subscription_details.Transaction_ID = transaction.Transaction_ID ) 
						WHERE transaction.status < 2 and subscription_details.main_branch_id = '$Branch' and 
							auction.GroupNo like '$Group_no' and 
							subscription_details.Ticketno = '$Tkt'";
			}
			//echo $sql;			
			$sql_result = mysql_query($sql) or die(mysql_error().$sql);
			while($row=mysql_fetch_array($sql_result)){
				$paid= $row["collection"];
				$member_due= $row["Due"];
				$tot_payble= $row["payble"];
			}
			$expenses			= Fngetvalue("tbl_expense_on_cust ","sum(amount)","Ticketno = '$Tkt' and GroupID = '".$GroupID."' and SubID = '".$SubID."'");
		}else{
			$curr_payble		= Fngetvalue(	"auction",
												"sum((IFNULL(Subcription,0) * $chit_share_perc)-(IFNULL(lasdevidentt,0) * $chit_share_perc))",
												"auction.main_branch_id='$Branch' and 
												 GroupNo='$Group_no' and 
												 AuctionDateTime is not null ");	
			$payble_plus		= Fngetvalue("auction 
											inner join subscription_details on auction.Auction_ID=subscription_details.Auction_ID 
											inner join transaction ON ( subscription_details.Transaction_ID = transaction.Transaction_ID ) ",
										"sum(IFNULL(LateFee,0) + IFNULL(Subcription_Fee,0) + IFNULL(other_charge,0) + IFNULL(check_return_fee,0) + IFNULL(transfer_fee,0)+ IFNULL(refund_amount,0)+ IFNULL(ser_tax,0))",
										"auction.main_branch_id='$Branch' and auction.GroupNo = '$Group_no' and subscription_details.TicketNo='$Tkt' and transaction.status<2"); 
			
			$tot_other_charge		= Fngetvalue("auction 
											inner join subscription_details on auction.Auction_ID=subscription_details.Auction_ID 
											inner join transaction ON ( subscription_details.Transaction_ID = transaction.Transaction_ID ) ",
										"sum(IFNULL(other_charge,0))",
										"auction.main_branch_id='$Branch' and auction.GroupNo = '$Group_no' and subscription_details.TicketNo='$Tkt' and transaction.status<2"); 
	
	
			$paid				= Fngetvalue("subscription_details inner join auction on subscription_details.Auction_ID=auction.Auction_ID inner join
															transaction ON ( subscription_details.Transaction_ID = transaction.Transaction_ID )  ","sum(subscription_details.Subcription)","auction.main_branch_id='$Branch' and TicketNo='$Tkt' and GroupNo='$Group_no' and transaction.status<2 ");
			$expenses			= Fngetvalue("tbl_expense_on_cust ","sum(amount)","Ticketno = '$Tkt' and GroupID = '".$GroupID."' and SubID = '".$SubID."'");
			
			
			$tot_payble			= $curr_payble+$payble_plus;		
			$member_due 		= $tot_payble-$paid;
			if ($tot_other_charge >= 	$expenses)	{
				$member_due = $member_due ;	
			}else{
				$member_due = $member_due + $expenses;
			}		
		}
	}
	
	$member_details[0]	= $paid;
	$member_details[1]	= $member_due;
	$member_details[2]	= $tot_payble;
	$member_details[3]	= $expenses;
	return $member_details;
}	
//------------------------fn for get due acc to inst---------	
function fngetmember_due_inst($Branch,$Group_no,$Tkt,$inst,$chitvalue_share=''){
	
	$reqtxtchitvalue       	= fngetvalue("groupdetails","ChitValue","GroupNo='".$Group_no."'");		
	if($chitvalue_share == ""){
		$chitvalue_share 	= fnGetValue("applicantdetails","chitvalue_share","`Ticketno` = '$Tkt' and `GroupNo` = '$Group_no' and main_branch_id = '$Branch'");	
	}
	if($inst != ""){
		$auction_array 		= fnGetValueMultipleNew("auction","sum(IF(auction.Subcription IS NULL,0,auction.Subcription)),sum(IF(auction.lasdevidentt IS NULL,0,auction.lasdevidentt))","AuctionDateTime is not null and auction.main_branch_id ='$Branch' and auction.GroupNo like '$Group_no' and InstNo='$inst'");
	}else{
		$auction_array 		= fnGetValueMultipleNew("auction","sum(IF(auction.Subcription IS NULL,0,auction.Subcription)),sum(IF(auction.lasdevidentt IS NULL,0,auction.lasdevidentt))","AuctionDateTime is not null and auction.main_branch_id ='$Branch' and auction.GroupNo like '$Group_no'");	
	}
	$reqSubscription_share 	= ($chitvalue_share / $reqtxtchitvalue) * $auction_array[0];			
	$reqlasdevidentt_share 	= ($chitvalue_share / $reqtxtchitvalue) * $auction_array[1];					
	$curr_payble 	   	   	= $reqSubscription_share - $reqlasdevidentt_share;	
	//$curr_payble		= Fngetvalue("auction","sum(Subcription-lasdevidentt)","auction.main_branch_id='$Branch' and GroupNo='$Group_no' and AuctionDateTime is not null  and InstNo='$inst'");				
	$payble_plus		= FnGetValue("auction inner join subscription_details on auction.Auction_ID=subscription_details.Auction_ID inner join transaction ON ( subscription_details.Transaction_ID = transaction.Transaction_ID ) 
										 ","sum(IFNULL(LateFee,0) + IFNULL(Subcription_Fee,0) + IFNULL(other_charge,0) + IFNULL(check_return_fee,0) + IFNULL(transfer_fee,0)+ IFNULL(refund_amount,0)+ IFNULL(ser_tax,0))","auction.main_branch_id='$Branch' and auction.GroupNo = '$Group_no' and subscription_details.TicketNo='$Tkt' and transaction.status<2 and InstNo='$inst'"); 
												
	$paid				= Fngetvalue("subscription_details inner join auction on subscription_details.Auction_ID=auction.Auction_ID inner join
													transaction ON ( subscription_details.Transaction_ID = transaction.Transaction_ID )  ","sum(subscription_details.Subcription)","auction.main_branch_id='$Branch' and TicketNo='$Tkt' and GroupNo='$Group_no' and transaction.status<2 and InstNo='$inst'");
	$tot_payble			= $curr_payble + $payble_plus;		
	$member_due 		= $tot_payble - $paid;
	$member_details[0]	= $paid;
	$member_details[1]	= $member_due;
	$member_details[2]	= $tot_payble;
	return $member_details;
}		

//---------------------------------calculate penalty-----------------------
function calculate_penalty($main_branch_id,$GroupNo,$Ticketno){
	$penalty=0;
	$sql_auction="SELECT Auction_ID,main_branch_id,GroupNo,InstNo,Month,auction.lasdevidentt,left(AuctionDateTime,10) as prize_auct_date,
				  		 DATEDIFF(NOW(),LastDateofPayment) as AuctionDateTime1 
				  FROM auction
				  WHERE main_branch_id='$main_branch_id' and GroupNo = '$GroupNo' and AuctionDateTime is not null 
				  ORDER BY `InstNo` DESC";
					
	$res_sql_auction=mysql_query($sql_auction) or die(mysql_error().$sql_auction);
	while($auction_row=mysql_fetch_assoc($res_sql_auction)){
		$arr=fngetmember_due_inst($auction_row['main_branch_id'],$auction_row['GroupNo'],$Ticketno,$auction_row['InstNo']);
		$auction_row['due']=$arr[1];
		//--------------------penalty start---------
		
		$reqCurr_Inst =$auction_row['due'];	
		$no_of_days =$auction_row["AuctionDateTime1"];
		$late_fees=Fngetvalue("subscription_details inner join auction on (auction.Auction_ID = subscription_details.Auction_ID) inner join transaction on (subscription_details.Transaction_ID=transaction.Transaction_ID )","sum(subscription_details.LateFee)","auction.main_branch_id ='".$auction_row['main_branch_id']."' and GroupNo ='".$auction_row['GroupNo']."' and InstNo ='".$auction_row['InstNo']."' and  TicketNo='$Ticketno' and transaction.status<2");
		$late_fee 	= fnGetValue("tbl_mn_app_setting","field_value","field_name = 'Late Fee Charges - In Percent'");
		if($late_fees>0){
			$no_of_days=Fngetvalue("subscription_details inner join auction on (auction.Auction_ID = subscription_details.Auction_ID) inner join transaction on (subscription_details.Transaction_ID=transaction.Transaction_ID )","DATEDIFF(NOW(),max(transaction.Reciept_Date))","auction.main_branch_id ='".$auction_row['main_branch_id']."' and GroupNo ='".$auction_row['GroupNo']."' and InstNo ='".$auction_row['InstNo']."' and  TicketNo='$Ticketno' and transaction.status<2");
		}
			//------------------											
		if($auction_row['due']>0){
			$auction_row['cal_late_fee'] 	= fncal_inst_latefee($auction_row['Auction_ID'],$reqCurr_Inst,$no_of_days);
  		  	$penalty=$penalty+$auction_row['cal_late_fee'];
		}												
	}
	return $penalty;
}
//------------------------------delete all voucher from transaction id----------
function delete_voucher_from_transaction($Transaction_ID){
	if (($Transaction_ID !="")&&($Transaction_ID!=0)){
		$count1=Fngetcount("subscription_details","Transaction_ID='$reqTransaction_ID'");
		if($count1>0){			
			$sql = "SELECT  * FROM  subscription_details WHERE Transaction_ID=$Transaction_ID";
			$result=mysql_query($sql) or die(mysql_error()."<br>".$sql );			
			while($row=mysql_fetch_array($result)){
				$reqSubscription_ID		= $row["Subscription_ID"];
				$delete_subscription=" delete 
					tbl_acc_transaction.*,tbl_acc_transaction_main.*,tbl_acc_relate.* from tbl_acc_transaction,tbl_acc_transaction_main,tbl_acc_relate where 
					tbl_acc_relate.acc_transaction_id = tbl_acc_transaction_main.acc_transaction_main_id and
					tbl_acc_transaction_main.acc_transaction_main_id = tbl_acc_transaction.acc_transaction_main_id and acc_transaction_type in 
					('Sub_Reciept_Enrollment_bill','Sub_Reciept_Enrollment_bill_ret','Sub_Transfer_Fee_bill','Sub_Transfer_Fee_bill_ret',
					'Bank_Charges_bill','Bank_Charges_bill_ret','Late_Fee_bill','Late_Fee_bill_ret','Other_Charges_bill','Other_Charges_bill_ret',
					'Journal_dividend_pay','Journal_dividend_pay_ret','service_tax_bill','service_tax_bill_ret')
					 and refered_id=$reqSubscription_ID ";
				mysql_query($delete_subscription) or die(mysql_error()."<br>".$delete_subscription );	
			}
		}else{
			$count2=Fngetcount("tbl_advance_payment","Transaction_ID='$reqTransaction_ID'");
			if($count2>0){		
				$sql = "SELECT  * FROM  tbl_advance_payment WHERE Transaction_ID=$Transaction_ID";
				$result=mysql_query($sql) or die(mysql_error()."<br>".$sql );			
				while($row=mysql_fetch_array($result)){
					$reqAdvance_ID = $row['Advance_ID']; 	
					$delete_subscription=" DELETE 
						tbl_acc_transaction.*,tbl_acc_transaction_main.*,tbl_acc_relate.* 
						from tbl_acc_transaction,tbl_acc_transaction_main,tbl_acc_relate where 
						tbl_acc_relate.acc_transaction_id = tbl_acc_transaction_main.acc_transaction_main_id and
						tbl_acc_transaction_main.acc_transaction_main_id = tbl_acc_transaction.acc_transaction_main_id and acc_transaction_type in 
						('Sub_Reciept_Enrollment_bill_adv','Sub_Reciept_Enrollment_bill_ret_adv','Sub_Transfer_Fee_bill_adv','Sub_Transfer_Fee_bill_ret_adv',
						'Bank_Charges_bill_adv','Bank_Charges_bill_ret_adv','Late_Fee_bill_adv','Late_Fee_bill_ret_adv','Other_Charges_bill_adv','Other_Charges_bill_ret_adv',
						'service_tax_bill_adv','service_tax_bill_ret_adv')
						 and refered_id=$reqAdvance_ID ";
					mysql_query($delete_subscription) or die(mysql_error()."<br>".$delete_subscription );	
				}
			}
		}
		$delete_trans=" delete 
			tbl_acc_transaction.*,tbl_acc_transaction_main.*,tbl_acc_relate.* from tbl_acc_transaction,tbl_acc_transaction_main,tbl_acc_relate where 
			tbl_acc_relate.acc_transaction_id = tbl_acc_transaction_main.acc_transaction_main_id and
			tbl_acc_transaction_main.acc_transaction_main_id = tbl_acc_transaction.acc_transaction_main_id and acc_transaction_type in 
			('Reciept','Reciept_ret','Reciept_chk_ret','Reciept_chk') and refered_id=$Transaction_ID ";
		mysql_query($delete_trans) or die(mysql_error()."<br>".$delete_trans );

	}
}
function delete_reciept($Transaction_ID){
	$adjustment_ID=Fngetvalue("transaction_details","adjustment_ID"," trans_id = '$Transaction_ID'");
	$adjustment_type=Fngetvalue("tbl_acc_adjustment","related_type"," adjustment_ID = '$adjustment_ID'");

	if (($Transaction_ID !="")&&($Transaction_ID!=0)){   
		delete_voucher_from_transaction($Transaction_ID);	    
		$delete_trans=" Delete transaction_details.*,transaction.*,subscription_details.*
		FROM transaction, transaction_details, subscription_details
		WHERE transaction_details.trans_id = transaction.Transaction_ID
		AND subscription_details.Transaction_ID = transaction.Transaction_ID
		and transaction.Transaction_ID='$Transaction_ID'";
		mysql_query($delete_trans) or die(mysql_error()."<br>".$delete_trans );		
	}
	if ($adjustment_type=='daily_reciept'){
		fndelete_adjustment($adjustment_ID);		
	}else{
		fnupdate_adjustment_paid($adjustment_ID);	
	}
}
function isdate_in_financial_year($req_date,$main_branch_id){
	$tmplaguage =  $_SESSION['opt_lang'];//session language
	if ($tmplaguage== ''){	$tmplaguage = 'english'; }	
	
	//$req_date=strtotime(substr($req_date,0,10));
	$req_date=substr($req_date,0,10);
	
	$fin_yr=Fngetvaluemultiple("tbl_acc_financial_years","from_date,to_date","	is_default='1' and 	lang='".$tmplaguage."' and main_branch_id='$main_branch_id'");

	$isinfinyr=FngetCount("tbl_acc_financial_years","('$req_date' between from_date AND to_date) and is_default='1' and 	lang='".$tmplaguage."' and main_branch_id='$main_branch_id'");

	if($isinfinyr>0){
		return 1;
	}else{
		return 2;
	}		
/*	$start_date=strtotime($fin_yr[0]);
	$end_date=strtotime($fin_yr[1]);
	if($req_date>=$start_date && $req_date<=$end_date){
		return 1;
	}else{
		return 2;
	}		*/
}

function fndelete_adjustment($adjustment_ID){
	if (($adjustment_ID !="")&&($adjustment_ID!=0)){   	
		$adjustment_type=Fngetvalue("tbl_acc_adjustment","related_type"," adjustment_ID = '$adjustment_ID'");
		$adjust_sql="DELETE FROM `tbl_acc_adjustment` WHERE adjustment_ID='$adjustment_ID'";	
		mysql_query($adjust_sql) or die(mysql_error().$adjust_sql);	
		if ($adjustment_type=='daily_reciept'){		
			$dadjust_sql="DELETE FROM `daily_transaction_adjustment` WHERE adjustment_ID='$adjustment_ID'";	
			mysql_query($dadjust_sql) or die(mysql_error().$dadjust_sql);	
		}
	}
}

function fnupdate_adjustment_paid($adjustment_ID){
	if (($adjustment_ID !="")&&($adjustment_ID!=0)){   	
		$paid=Fngetvalue("transaction_details left join transaction on (transaction_details.trans_id = transaction.Transaction_ID)","sum(IFNULL(transaction_details.Amount,0))","transaction.status<2 and  adjustment_ID='$adjustment_ID'");
		$adjust_sql="update `tbl_acc_adjustment` set paid='$paid' where adjustment_ID='$adjustment_ID'";	
		mysql_query($adjust_sql) or die(mysql_error().$adjust_sql);	
	}
}
function fncal_inst_latefee($req_Auction_ID,$reqCurr_Inst_amt,$req_no_of_days){
	//echo "$req_Auction_ID,$reqCurr_Inst_amt,$req_no_of_days<br>";
	$cal_late_fee = 0;
	$late_fee 			= fnGetValue("tbl_mn_app_setting","field_value","field_name = 'Late Fee Charges - In Percent'");
	$grace_day 			= fnGetValue("tbl_mn_app_setting","field_value","field_name = 'Grace Days for Payment'");
	if ($grace_day=="") $grace_day =0;	
	if($grace_day >= 0){
		$req_Last_Payment_Date_Grace_diff   = FnGetValue("auction","DATEDIFF(NOW(), DATE_ADD(LastDateofPayment, INTERVAL $grace_day DAY))"," Auction_ID='".$req_Auction_ID."'");
		if($req_Last_Payment_Date_Grace_diff >= 0){
			$req_Last_Payment_Date_diff 	= FnGetValue("auction","DATEDIFF(LastDateofPayment,NOW())","Auction_ID='".$req_Auction_ID."'");
		}
	}else{
		$req_Last_Payment_Date_diff   	= FnGetValue("auction","DATEDIFF(LastDateofPayment,NOW())","Auction_ID='".$req_Auction_ID."'");
	}
	//$req_no_of_days = FnGetValue("auction","DATEDIFF(NOW(), LastDateofPayment)","Auction_ID='".$req_Auction_ID."'");
	
	if(($late_fee > 0) && ($req_Last_Payment_Date_diff < 0)){
		$cal_late_fee = (($reqCurr_Inst_amt * ($late_fee / 100)) / 365) * $req_no_of_days;		
	}	
	return round($cal_late_fee);	
}
//------------------------fn for to check if  inst paid fully---------	
function fnis_inst_paid($main_branch_id,$Group_ID,$Tkt,$Insts,$chitvalue_share='',$todate=""){
	$reqtxtchitvalue       	= fngetvalue("groupdetails","ChitValue","GroupID='".$Group_ID."'");		
	if($chitvalue_share == ""){
		$chitvalue_share 	= fnGetValue("applicantdetails","chitvalue_share","`Ticketno` = '$Tkt' and `GroupID` = '$Group_ID' and main_branch_id = '$main_branch_id'");	
	}
	
	$cons_paid_div_in_due=fngetvalue("tbl_mn_app_setting","field_value","field_name='cons_paid_div_in_due'");	
	$amtpayble	= Fngetvalue("auction","Subcription - lasdevidentt","main_branch_id = '".$main_branch_id."' and  GroupID='".$Group_ID."' and InstNo in ".$Insts."");

	$amtpayble 	= ($chitvalue_share / $reqtxtchitvalue) * $amtpayble;	

/*	$inst_paid	= FnGetValueMultipleNew("auction 
							inner join subscription_details on (auction.Auction_ID=subscription_details.Auction_ID ) 
							inner join transaction ON ( subscription_details.Transaction_ID = transaction.Transaction_ID )",
						 "sum(subscription_details.Subcription),sum(IFNULL(LateFee,0) + IFNULL(Subcription_Fee,0) + IFNULL(other_charge,0) + IFNULL(check_return_fee,0) + IFNULL(transfer_fee,0)),GROUP_CONCAT(DISTINCT transaction.Transaction_ID)",
						  "	transaction.status<>2 and 
							auction.GroupID			= '".$Group_ID."' and 
							Ticketno 				= '".$Tkt."' and 
							auction.main_branch_id 	= '".$main_branch_id."' and 
							InstNo 					in ".$Insts."");*/
	
    $instpaid_sql="   SELECT auction.main_branch_id,auction.GroupNo,sub_det.Ticketno,auction.Auction_ID,auction.InstNO,auction.Subcription,
							auction.lasdevidentt,
						  sum(IFNULL(sub_det.Subcription,0)) as paid,
						  sum(IFNULL(sub_det.lasdevidentt,0)) as paid_dividend,
						  sum(IFNULL(sub_det.LateFee,0)) as LateFee,
						  sum(IFNULL(sub_det.Subcription_Fee,0)) as Subcription_Fee,
						  sum(IFNULL(sub_det.other_charge,0)) as other_charge,
						  sum(IFNULL(sub_det.check_return_fee,0)) as check_return_fee,
						  sum(IFNULL(sub_det.transfer_fee,0) ) as transfer_fee,
						  sum(IFNULL(sub_det.refund_amount,0) ) as refund_amount,
						  sum(IFNULL(sub_det.ser_tax,0) ) as ser_tax,
						  GROUP_CONCAT(DISTINCT sub_det.Transaction_ID) as Inst_Trans_Ids
					  FROM 
						  auction LEFT JOIN 
						  ( select Subcription, lasdevidentt, LateFee, Subcription_Fee, other_charge, check_return_fee, Auction_ID,
							  transfer_fee,subscription_details.Transaction_ID,subscription_details.Ticketno,refund_amount,ser_tax 
							from subscription_details LEFT JOIN transaction ON ( subscription_details.Transaction_ID = transaction.Transaction_ID ) 
							where transaction.status<2	and subscription_details.Ticketno='$Tkt' ".(($todate!="")? "and transaction.Reciept_Date<='".$todate."'":"")."			
						   ) as sub_det ON (auction.Auction_ID = sub_det.Auction_ID ) 
					 					  
					  WHERE 
					  	  auction.main_branch_id ='$main_branch_id' and auction.GroupID = '".$Group_ID."' and AuctionDateTime is not null and 
						  auction.InstNo 			in ".$Insts." 
					  GROUP BY 	auction.main_branch_id,auction.Auction_ID
					  ORDER BY auction.main_branch_id  asc,auction.Auction_ID asc ";
					  
	$sql_result = mysql_query($instpaid_sql) or die(mysql_error().$instpaid_sql);
	$payable 	=0;
	$Due 		=0;
	$devident 	=0;
	$Paid 		=0;
	while($row=mysql_fetch_array($sql_result)){
		$instpayable=0;
		$instpayable_plus=0;
		$instdue=0;
	
		
		if ($cons_paid_div_in_due!=1){
			$instpayable = $row["Subcription"]-$row["lasdevidentt"];
		}else{	
			if ($row["paid"] > 0 ){
				$instpayable = $row["Subcription"]-$row["paid_dividend"];
			}else{
				$instpayable = $row["Subcription"]-$row["lasdevidentt"];
			}		
		}		
		//echo $row["LateFee"]."+".$row["Subcription_Fee"]."+".$row["other_charge"]."+".$row["check_return_fee"]."+".$row["transfer_fee"];
		$instpayable_plus= $row["LateFee"]+$row["Subcription_Fee"]+$row["other_charge"]+$row["check_return_fee"]+$row["transfer_fee"];
		$instpayable = $instpayable + $instpayable_plus;
		$instdue	 = $instpayable - $row["paid"];
		$payable 	+= $instpayable ;
		$Paid 		+= ($row["paid"]-$instpayable_plus);
		$devident 	+= $row["paid_dividend"];
		$Due 		+= $instdue;
		//echo "Due-".$Due ;
		if ($row["InstNO"]==2){
			$Sec_Inst_Trans = $row["Inst_Trans_Ids"];
		}
		$Inst_Trans = $row["Inst_Trans_Ids"];
	}
	$Ret_Val = array();
	if($Due <= 0){
		$Ret_Val[0]= true;	
		//echo "here";	
	}else{
		$Ret_Val[0]= false;		
	}
	$Ret_Val[1]= $Sec_Inst_Trans;		
	$Ret_Val[2]= $Inst_Trans;	
	//if ($Ret_Val[0]== true)	print_r($Ret_Val);	else echo "false";	
	return $Ret_Val;
}	
function fn_add_agent_comm_bill_vch($req_AppID,$reqtxtReciept_Date,$reqTransaction_ID){
	//echo "$req_AppID,$reqtxtReciept_Date,$reqTransaction_ID</BR>";

	$ses_lang 			= $_SESSION['opt_lang'];
	$Applicant_det    	= array();
	$Applicant_det    	= FnGetValueMultipleNew("applicantdetails","main_branch_id,GroupID,GroupNo,Ticketno,SubID,Remove_status","AppID = '$req_AppID' ");
	$reqtxtBranchCode	= $Applicant_det[0];
	$reqtxtGrp_Id		= $Applicant_det[1];
	$reqtxtGrp_No		= $Applicant_det[2];
	$reqtxtTicket_No	= $Applicant_det[3];
	$reqSubID			= $Applicant_det[4];	
	$reqRemove_status	= $Applicant_det[5];	

	//$finacial_yr_id		= fngetvalue('tbl_acc_financial_years','main_acc_fianancial_year_id'," main_branch_id='$reqtxtBranchCode' and is_default='1' and lang='english'");
	$finacial_yr_id		= fngetvalue("tbl_acc_financial_years","main_acc_fianancial_year_id"," main_branch_id='$reqtxtBranchCode' and from_date <= '$reqtxtReciept_Date' AND to_date >= '$reqtxtReciept_Date'  and lang='english'");			
	
	if ($reqRemove_status!='transfered'){	
		$reqinstpaiddet = array();
		$comm_inst_paid_cnt	= fngetValue("tbl_mn_app_setting","field_value","field_name = 'comm_inst_paid_cnt'");
		if ($comm_inst_paid_cnt=="")$comm_inst_paid_cnt=0;
		$inststr="";
		for($i=1;$i<=$comm_inst_paid_cnt;$i++){
			if ($i!=1){		$inststr.= ",".$i;	}
			else{			$inststr.= $i;		}
		}
		$inststr="(".$inststr.")";
		$reqinstpaiddet = fnis_inst_paid($reqtxtBranchCode,$reqtxtGrp_Id,$reqtxtTicket_No,$inststr,$chitvalue_share='',$reqtxtReciept_Date);
		$reqpaidtransids= $reqinstpaiddet[2];

		//print_r($reqinstpaiddet);

		$is_rec_date_greater	= fngetCount("auction","'$reqtxtReciept_Date' > AuctionDateTime and GroupID = '$reqtxtGrp_Id' and InstNo='$comm_inst_paid_cnt'");		
			
		if ($is_rec_date_greater>0){
			$vchdate	= $reqtxtReciept_Date;			
		}else{
			$vchdate	= fngetValue("auction","left(AuctionDateTime,10)","GroupID = '$reqtxtGrp_Id' and InstNo='$comm_inst_paid_cnt'");		
		}
		
		if ($reqinstpaiddet[0]==true){
			include_once( 'acc_include_functions.php');		
			$account_type_Agent_Comm = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Direct Expenses' and lang = '$ses_lang'"); 
			$req_Agent_Comm_acc= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang= '$ses_lang' and account_type = '$account_type_Agent_Comm' and account_name='Agent Commision' and main_branch_id='$reqtxtBranchCode' and acc_fianancial_year_id='$finacial_yr_id'");    
			if ($req_Agent_Comm_acc==0){ 
				$req_Agent_Comm_acc=create_account("Agent Commision",$account_type_Agent_Comm,"","",$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtBranchCode,$finacial_yr_id);
			}
			$req_introducer_det	= FnGetValueMultipleNew("groupforming LEFT JOIN tbl_sur_sign_up on tbl_sur_sign_up.personnel_id = groupforming.IntroducedBy",
														"IntroducedBy,ref_user_type,ChitValue,ID",
														"Entrolled = 1 and groupforming.GroupNoEnrolled = '".$reqtxtGrp_No."' and groupforming.TktNoEnrolled ='".$reqtxtTicket_No."' and
														 groupforming.SubID='$reqSubID'" );		
															
			$req_introducer_id 	 = $req_introducer_det[0];
			$req_ref_user_type   = $req_introducer_det[1];
			$req_ChitValue		 = $req_introducer_det[2];
			$req_groupforming_ID = $req_introducer_det[3];
			
			$req_introducer_det		= array();
			$req_introducer_det		= FnGetValueMultipleNew("tbl_sur_sign_up","user_code,concat(first_name,' ',last_name)","personnel_id = '$req_introducer_id' ");
			$req_introducer_code 	= $req_introducer_det[0];
			$req_introducer_name 	= $req_introducer_det[1];
			
			if($req_introducer_code !='VD9999'){ //If not a company agent	
				$req_Introducer_percentage 	= fngetValue("tbl_mn_app_setting","field_value","field_name = '".$req_ref_user_type."_comm_percentage'");					
				$req_TDSPercentage 			= fngetValue("tbl_mn_app_setting","field_value","field_name = 'Introducer_tds_percentage'");					
				
				$req_introducer_comm	= ($req_ChitValue * $req_Introducer_percentage)/100;					
				$req_introducer_comm_tds= ($req_introducer_comm * $req_TDSPercentage ) / 100;
				$req_total_comm 		= ($req_introducer_comm + $req_introducer_comm_tds);
				
				//echo $req_ChitValue;
				$req_introducer_acc = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang= '$ses_lang' and related_type  = 'introducer' and related_id='$req_introducer_id' and main_branch_id='$reqtxtBranchCode' and acc_fianancial_year_id='$finacial_yr_id'");    
				if ($req_introducer_acc==0){ 
					$account_name		= $req_introducer_name;
					$account_type_id	= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Sundry Creditors' and lang = '$ses_lang' ");
					$req_introducer_acc	= create_account($account_name,$account_type_id,"introducer",$req_introducer_id,$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtBranchCode,$finacial_yr_id);
				}
				$account_type_tds 	= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Duties & Taxes' and lang = '$ses_lang'"); 
				$req_tds_acc   		= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$account_type_tds' and account_name='TDS' and main_branch_id='$reqtxtBranchCode' and acc_fianancial_year_id='$finacial_yr_id'");
				if ($req_tds_acc==0){
					$req_tds_acc=create_account('TDS',$account_type_tds,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$reqtxtBranchCode,$finacial_yr_id);
				}
				$req_by_acc_id = array();
				$req_to_acc_id = array();
			
				$req_by_acc_id[0][0] = $req_Agent_Comm_acc;	
				$req_by_acc_id[0][1] = $req_introducer_comm;	
			
				$req_to_acc_id[0][0] = $req_introducer_acc;
				
				if ($req_introducer_comm_tds > 0){ 
					$req_to_acc_id[0][1] = $req_introducer_comm - $req_introducer_comm_tds; 
				}else{	
					$req_to_acc_id[0][1] = $req_introducer_comm;	
				}		
				if ($req_introducer_comm_tds > 0){
					$req_to_acc_id[1][0] = $req_tds_acc;
					$req_to_acc_id[1][1] = $req_introducer_comm_tds;
				}
				if ($reqpaidtransids!=""){
					$agent_comm_bill_acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_agent_comm_bill' and refered_id in ($reqpaidtransids)");		
					add_voucher('Journal_agent_comm_bill',$req_by_acc_id,$reqTransaction_ID,$req_total_comm,$vchdate,"Agent Commision Payble for Group No: $reqtxtGrp_No, Ticket No:$reqtxtTicket_No ",$reqtxtBranchCode,$agent_comm_bill_acc_trans_main_id,$req_to_acc_id);					
				}
			}else{
				$agent_comm_bill_acc_trans_main_id 	= fnGetValue("tbl_acc_relate22","acc_transaction_id","acc_transaction_type = 'Journal_agent_comm_bill' and refered_id in ($reqpaidtransids) ");		
				delete_voucher($agent_comm_bill_acc_trans_main_id);
			}
		}		
	}
} 
function fn_add_agent_comm_bill_ret_vch($req_AppID,$reqtxtReciept_Date,$reqTransaction_ID){
	
	$ses_lang 			= $_SESSION['opt_lang'];
	
	//$finacial_yr_id		= fngetvalue('tbl_acc_financial_years','main_acc_fianancial_year_id'," main_branch_id='$reqtxtgroupbranch' and is_default='1' and lang='english'");
	
	$finacial_yr_id		= fngetvalue("tbl_acc_financial_years","main_acc_fianancial_year_id"," main_branch_id='$reqtxtBranchCode' and from_date <= '$reqtxtReciept_Date' AND to_date >= '$reqtxtReciept_Date'  and lang='english'");			
	
	$Applicant_det    	= array();
	$Applicant_det    	= FnGetValueMultipleNew("applicantdetails","main_branch_id,GroupID,GroupNo,Ticketno,SubID","AppID = '$req_AppID' ");
	$reqtxtBranchCode	= $Applicant_det[0];
	$reqtxtGrp_Id		= $Applicant_det[1];
	$reqtxtGrp_No		= $Applicant_det[2];
	$reqtxtTicket_No	= $Applicant_det[3];
	$reqSubID			= $Applicant_det[4];	
	
	include_once( 'acc_include_functions.php');		
	$req_introducer_det	= FnGetValueMultipleNew("groupforming LEFT JOIN tbl_sur_sign_up on tbl_sur_sign_up.personnel_id = groupforming.IntroducedBy",
												"IntroducedBy,ref_user_type,ChitValue,ID",
												"Entrolled = 1 and groupforming.GroupNoEnrolled = '".$reqtxtGrp_No."' and groupforming.TktNoEnrolled ='".$reqtxtTicket_No."' and
												 groupforming.SubID='$reqSubID'" );		
													
	$req_introducer_id 	 = $req_introducer_det[0];
	$req_ref_user_type   = $req_introducer_det[1];
	$req_ChitValue		 = $req_introducer_det[2];
	$req_groupforming_ID = $req_introducer_det[3];
	
	$agent_comm_bill_acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_agent_comm_bill' and refered_id = '$reqTransaction_ID'");		
	if ($agent_comm_bill_acc_trans_main_id > 0){
		add_voucher('Journal_agent_comm_bill_ret',$req_by_acc_id,$reqTransaction_ID,$req_total_comm,$reqtxtReciept_Date,"Agent Commision Payble Return for Group No: $reqtxtGrp_No, Ticket No:$reqtxtTicket_No ",$reqtxtBranchCode,$agent_comm_bill_acc_trans_main_id,$req_to_acc_id);					
	}	
}
function fncal_op_balance($from_acc_id,$financial_yr_id){
	//echo "I am here";
  	$sql="SELECT sum(`cr_amt`) as tot_cr ,sum(`db_amt`) as tot_dr 
	      FROM `tbl_acc_transaction`,`tbl_acc_transaction_main`, tbl_acc_accounts 
		  WHERE tbl_acc_transaction.acc_transaction_main_id = tbl_acc_transaction_main.acc_transaction_main_id and 
	 		  	tbl_acc_transaction.from_acc_id = tbl_acc_accounts.main_acc_account_id and 
				tbl_acc_accounts.lang='".$_SESSION['opt_lang']."'  and tbl_acc_accounts.acc_fianancial_year_id='".$financial_yr_id."'";
					   
	$sql = $sql . "and `tbl_acc_transaction`.from_acc_id = '$from_acc_id' GROUP BY `from_acc_id`";

    //echo $sql;
	$result =  mysql_query($sql) or die($sql."<br>".mysql_error());
	while($row = mysql_fetch_array($result)){
		$tot_db =$tot_db+ $row['tot_dr'];
		$tot_cr =$tot_cr+ $row['tot_cr'];
	}	
	$opening_bal = decimal_num(fngetvalue("tbl_acc_accounts","opening_bal","main_acc_account_id =".$from_acc_id." and lang='".$_SESSION['opt_lang']."'"));	
	$opening_bal_cr_dr = fngetvalue("tbl_acc_accounts","opening_bal_cr_dr","main_acc_account_id =".$from_acc_id." and lang='".$_SESSION['opt_lang']."'");	
	if ($opening_bal_cr_dr=='dr'){ 
		$tot_db = $tot_db + $opening_bal;			
	}else if ($opening_bal_cr_dr=='cr'){                    
		$tot_cr = $tot_cr + $opening_bal;
	}
	if ($tot_db>$tot_cr){ 
		$tot_bal =decimal_num($tot_db-$tot_cr);
		return array($tot_bal,'dr');		
	}else{
	   	$tot_bal =decimal_num($tot_cr-$tot_db);		
	   	return array($tot_bal,'cr');		
	}
	
}
function fnupdateopeningbalace($main_branch_id,$from_financial_yr_id,$to_financial_yr_id,$from_acc_id=''){
	
	$sql="SELECT * FROM tbl_acc_accounts 
		  WHERE main_branch_id='$main_branch_id' and acc_fianancial_year_id='$from_financial_yr_id' and lang='english'";
						 
	if ($from_acc_id !=""){	$sql.=" and main_acc_account_id = '$from_acc_id'"; }
	//echo $sql;
	$res_sql=mysql_query($sql) or die(mysql_error()."<BR>".$sql);
	while($row=mysql_fetch_assoc($res_sql)){
		$req_account_name= $row["account_name"];
		$req_account_type= $row["account_type"];
		$req_related_type= $row["related_type"];
		$req_related_id= $row["related_id"];				
		//calculate opening balance
		
		$acc_bal_arr=fncal_op_balance($row['main_acc_account_id'],$row['acc_fianancial_year_id']);
		$opening_bal=$acc_bal_arr[0];
		$acc_bal_dt_cr=$acc_bal_arr[1];
	    if($opening_bal == "") $opening_bal = 0;		
		
		if ($req_related_type=="applicantdetails"){
			$isaccpresent=fngetcount("tbl_acc_accounts","main_branch_id='$main_branch_id' and  acc_fianancial_year_id='$to_financial_yr_id' and 
					 	 account_name='$req_account_name' and related_type='$req_related_type' and related_id ='$req_related_id' and lang='english'");
		}else{
			$isaccpresent=fngetcount("tbl_acc_accounts","main_branch_id='$main_branch_id' and  acc_fianancial_year_id='$to_financial_yr_id' and 
					 	 account_name='$req_account_name' and account_type='$req_account_type' and lang='english'");
		}
		if (($isaccpresent ==0)||($isaccpresent=="")){
			//echo $req_account_name."-".$to_financial_yr_id."<BR>";
			create_account($req_account_name,$req_account_type,$req_related_type,$req_related_id,$opening_bal,$acc_bal_dt_cr,$opening_bal_date='NOW()',$main_branch_id,$to_financial_yr_id); 
		}else{

		  //Update account		
		  $update_sql="UPDATE tbl_acc_accounts 
					   SET opening_bal='$opening_bal', opening_bal_cr_dr='$acc_bal_dt_cr' 
					   WHERE main_branch_id='$main_branch_id' and  acc_fianancial_year_id='$to_financial_yr_id' and 
						   account_name='$req_account_name'	";
		  if ($req_related_type=="applicantdetails"){						   
			$update_sql.=   " and related_type='$req_related_type' and related_id ='$req_related_id'";		
		  }else{
			$update_sql.=   " and account_type='$req_account_type'";
		  }
		
		  $update_sql.=   " and lang='english'";
		  $res_update_sql=mysql_query($update_sql) or die(mysql_error()."<BR>".$update_sql);
		}
	}
	
}
function fnTransfer_Tkt_to_Other_Group($main_branch_id,$from_Group_ID,$from_Ticketno,$to_Group_ID,$to_Ticketno){
	$ses_lang	= $_SESSION['opt_lang'];
	$reqfromapplicantdetails = FngetvalueMultiple("applicantdetails","GroupNo,SubCodeNo,SubID","GroupID = '$from_Group_ID' and Ticketno = '$from_Ticketno'");
	$reqfromGroupNo= $reqfromapplicantdetails[0];
	$reqfromSubCodeNo= $reqfromapplicantdetails[1];
	$reqfromSubID= $reqfromapplicantdetails[2];

	$reqtogroupdetailsarr	= FngetvalueMultiple("groupdetails","GroupNo,cut_dev_in_rec,ChitValue,Enrollment_Fee","GroupID='$to_Group_ID' and main_branch_id= '$main_branch_id'");
	$reqtxtGrp_No	   	= $reqtogroupdetailsarr[0];
	$reqcut_dev_in_rec 	= $reqtogroupdetailsarr[1];
	$reqtoChitValue    	= $reqtogroupdetailsarr[2];
	$reqEnrollment_Fee	= $reqtogroupdetailsarr[3];

	$comm_inst_paid_cnt	= fngetValue("tbl_mn_app_setting","field_value","field_name = 'comm_inst_paid_cnt'");
	if ($comm_inst_paid_cnt=="")$comm_inst_paid_cnt=0;


	
	$sql="SELECT DISTINCT transaction.Transaction_ID as Transaction_ID
		  FROM subscription_details LEFT JOIN transaction on (subscription_details.Transaction_ID = transaction.Transaction_ID)
		       LEFT JOIN auction on (auction.Auction_ID=subscription_details.Auction_ID) 
		  WHERE transaction.main_branch_id='$main_branch_id' and auction.GroupID='$from_Group_ID' and subscription_details.TicketNo='$from_Ticketno' 
		  ORDER BY transaction.Transaction_ID ASC";
		  
	$res=mysql_query($sql) or die(mysql_error()."<BR>".$sql);
	$p=0;
	while($tranrow=mysql_fetch_assoc($res)){
		$reqTransaction_ID	= $tranrow['Transaction_ID'];
		$tran_det_arr 	= FngetvalueMultiple("transaction_details","Amount,payment_by,neft_bank,dep_slip_ID,ChequeNo","trans_id = '$reqTransaction_ID'");
		$amt_adj_cust	= $tran_det_arr[0];
		$DaysCollection = $tran_det_arr[0];
		$reqtxtamount	= $tran_det_arr[0];
		$reqtxtPayment	= $tran_det_arr[1];
		$reqtxtneft_bank= $tran_det_arr[2];
		$dep_slip_id	= $tran_det_arr[3];
		$ChequeNo		= $tran_det_arr[4];		
		
		$tran_arr 			= FngetvalueMultiple("transaction","ReceiptNo,Reciept_Date,TRNO,TRDate,receipt_flag","Transaction_ID = '$reqTransaction_ID'");
		$reqtxtReciept_No	= $tran_arr[0];
		$reqtxtReciept_Date	= $tran_arr[1];
		$reqtxtTR_No		= $tran_arr[2];
		$req_receipt_flag 	= $tran_arr[3];

		include_once( 'acc_include_functions.php');
		delete_voucher_from_transaction($reqTransaction_ID);
		$reqtxtSubID    	= FnGetValue("applicantdetails","SubID","GroupID = '$to_Group_ID' and Ticketno = '$to_Ticketno'");
		
		$finacial_yr_id	= fngetvalue("tbl_acc_financial_years","main_acc_fianancial_year_id"," main_branch_id='$main_branch_id' and is_default='1' and lang='$ses_lang'");
		$related_id    	= FnGetValue("applicantdetails","AppID","GroupID = '$to_Group_ID' and Ticketno = '$to_Ticketno'");
		$reqis_comp_acc = FnGetValue("applicantdetails","is_comp_acc",	"GroupID = '".$to_Group_ID."' and Ticketno = '".$to_Ticketno."'");

		$one_acc_for_group			= fngetvalue("tbl_mn_app_setting","field_value","field_name = 'one_acc_for_group'");
		$cheque_rec_date_is_vch_date= fngetvalue("tbl_mn_app_setting","field_value","field_name = 'cheque_rec_date_is_vch_date'");

		$max_auction_id = fngetvalue("subscription_details inner join auction on subscription_details.Auction_ID=auction.Auction_ID inner join transaction on transaction.Transaction_ID=subscription_details.Transaction_ID",
									 "max(subscription_details.Auction_ID)"," auction.GroupID='$to_Group_ID' and subscription_details.TicketNo='$to_Ticketno' and auction.main_branch_id='$main_branch_id' and transaction.status<2");
		
		if ($max_auction_id=="") $max_auction_id=0;
		$sql_auction="select *,left(AuctionDateTime,10) as Auction_Date from auction where AuctionDateTime is not null and GroupID='$to_Group_ID' and Auction_ID >='$max_auction_id' order by InstNo ASC";
		$res_sql_auction=mysql_query($sql_auction) or die(mysql_error().$sql_auction);
		$check_for_intr_comm=0;
		$i=0;
		$inst_nos_str ="";
		while($aucrow=mysql_fetch_assoc($res_sql_auction)){
			$reqAuction_Date = $aucrow['Auction_Date'];
			$reqtxtAuction_ID= $aucrow['Auction_ID'];
			$reqtxtInstallment_No= $aucrow['InstNo'];
			$reqtxtDevident  = $aucrow['lasdevidentt'];
			if ($reqcut_dev_in_rec ==1){
				$payble=$aucrow['Subcription']-$aucrow['lasdevidentt'];
			}elseif ($reqcut_dev_in_rec ==2){
				$payble=$aucrow['Subcription'];				
			}
			$sub_arr=FngetvaluemultipleNew("subscription_details inner join auction on subscription_details.Auction_ID=auction.Auction_ID inner join transaction on transaction.Transaction_ID=subscription_details.Transaction_ID",
										"sum(IFNULL(subscription_details.Subcription,0)),sum(IFNULL(LateFee,0) + IFNULL(Subcription_Fee,0) + IFNULL(other_charge,0) + IFNULL(check_return_fee,0) + IFNULL(transfer_fee,0))",
										"auction.GroupID='$to_Group_ID' and subscription_details.TicketNo='$to_Ticketno' and auction.InstNo='".$aucrow['InstNo']."' and
										 auction.main_branch_id='$main_branch_id' and transaction.status<2");
			$sub_paid=$sub_arr[0];
			$payble_plus=$sub_arr[1];
			$payble=$payble+$payble_plus;
			$due=$payble-$sub_paid;
			$max_inst=Fngetvalue("auction","max(InstNo)","AuctionDateTime is not null and GroupID='$to_Group_ID'");				
			
			
			if($due>0 && $DaysCollection>0){
				if($DaysCollection>=$due ){
					if($max_inst==$aucrow['InstNo']){
						$current=$DaysCollection;
						$reqtxtArrears=$payble-$DaysCollection; $DaysCollection=0;		
					}else{
						$DaysCollection=$DaysCollection-$due;$current=$due;
					}
				}else if($DaysCollection<$due){$current=$DaysCollection;  $reqtxtArrears=$due-$current; $DaysCollection=0; }									
				
				if ($reqcut_dev_in_rec ==1){
					$countDevident=fngetcount("subscription_details inner join auction on subscription_details.Auction_ID=auction.Auction_ID
										 inner join transaction on transaction.Transaction_ID=subscription_details.Transaction_ID",
										 " auction.GroupID='$to_Group_ID' and subscription_details.TicketNo='$to_Ticketno'
										and auction.InstNo='".$aucrow['InstNo']."' and auction.main_branch_id='$main_branch_id' 
										and subscription_details.lasdevidentt>0 and transaction.status<2");
				}elseif ($reqcut_dev_in_rec ==2){
					$reqtxtDevident=0;				
				}
				if($countDevident>0){$reqtxtDevident=0;}
				if ($aucrow['InstNo']<=$comm_inst_paid_cnt){
					$check_for_intr_comm=1;
				}
				if ($i > 0){ 
					$inst_nos_str .= ",".$aucrow['InstNo'];
				}else{
					$inst_nos_str .= $aucrow['InstNo'];
				}
				
				$ins_sql = "Insert into subscription_details (`Subscription_ID` ,`Auction_ID` ,`TicketNo` ,`Transaction_ID` ,`SubID` ,`LateFee`,
				`lasdevidentt`,`arrears` ,`main_branch_id` ,`Subcription`,`Subcription_Fee`,`other_charge`,`check_return_fee`,`transfer_fee`,`is_adv`)
				  values (NULL,'$reqtxtAuction_ID','$to_Ticketno','$reqTransaction_ID','$reqtxtSubID','',
				  '$reqtxtDevident','$reqtxtArrears','$main_branch_id','$current','','','','','')";				
				$res_sql = mysql_query($ins_sql) or die(mysql_error().$ins_sql);		
				$reqSubscription_ID = mysql_insert_id();
				 
				$addvch		= 0;
				//account changes start
				if (($reqtxtPayment == "cheque" )&&($dep_slip_id >0 )){ $addvch=1; 
					if ($cheque_rec_date_is_vch_date!=1){
						$voucher_date = FnGetValue("tbl_dep_slip_details","depslip_Date","dep_slip_ID = ".$dep_slip_id);
					}else{
						$voucher_date = $reqtxtReciept_Date;
					}
				}else if (($reqtxtPayment == 'cash')||($reqtxtPayment == 'neft')||($reqtxtPayment == 'adjustment')){ 	
					$addvch=1;
					$voucher_date = $reqtxtReciept_Date;
				}

				if ($reqtxtDevident > 0){
					if ($req_receipt_flag==2){ // If Advance Receipt
						$vch_date= substr($reqAuction_Date,0,8)."01";
					}else{ $vch_date= $voucher_date; } 
					 
					$Devident_acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_dividend_pay' and refered_id = '$reqSubscription_ID'");
					echo "$Devident_acc_trans_main_id $related_id,$reqSubscription_ID,$reqtxtDevident,$vch_date,$main_branch_id";
					
					add_voucher('Journal_dividend_pay',$related_id,$reqSubscription_ID,$reqtxtDevident,$vch_date,"Dividend Distribution IntNo: $reqtxtInstallment_No, TR NO: $reqtxtTR_No, Reciept No : $reqtxtReciept_No",$main_branch_id,$Devident_acc_trans_main_id);
					if (($reqis_comp_acc==1)&&($one_acc_for_group==1)){				
						$req_foreman_dividend_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Indirect Incomes' and lang = '$ses_lang'");								
						$req_foreman_dividend_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_foreman_dividend_acc_type' and account_name='Foreman Dividend' and main_branch_id='$reqmain_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
						if ($req_foreman_dividend_acc_id==0){
							$req_foreman_dividend_acc_id=create_account('Foreman Dividend',$req_foreman_dividend_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$main_branch_id,$finacial_yr_id);
						}			
						$req_comp_invt_in_own_chit_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Investments' and lang = '$ses_lang'");								
						$req_comp_invt_in_own_chit_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_comp_invt_in_own_chit_acc_type' and account_name='Companies Investment In Own Chits' and main_branch_id='$main_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
						if ($req_comp_invt_in_own_chit_acc_id==0){
							$req_comp_invt_in_own_chit_acc_id=create_account('Companies Investment In Own Chits',$req_comp_invt_in_own_chit_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$main_branch_id,$finacial_yr_id);
						}

						$com_foreman_div_acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Journal_com_foreman_div' and refered_id = '$reqSubscription_ID'");
						add_voucher('Journal_com_foreman_div',$req_comp_invt_in_own_chit_acc_id,$reqSubscription_ID,$reqtxtDevident,$vch_date,"Dividend Amt Transfer to Foreman Deividend IntNo: $reqtxtInstallment_No, TR NO: CR, Reciept No : $reqtxtReciept_No",$main_branch_id,$com_foreman_div_acc_trans_main_id,$req_foreman_dividend_acc_id);
					}
				
				}

				//Enroll Ment Fee
				if ($txtSubcription_Fee > 0){
					$enroll_acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Sub_Reciept_Enrollment_bill' and refered_id = '$reqSubscription_ID'");		
					if($txtSubcription_Fee==0 && $enroll_acc_trans_main_id!=0){	delete_voucher($enroll_acc_trans_main_id);}	
					
					$req_enroll_fee_acc_id   = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$account_type_direct_income' and account_name='Enrollment Fee' and main_branch_id='$main_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
					if ($req_enroll_fee_acc_id==0){
						$req_enroll_fee_acc_id=create_account('Enrollment Fee',$account_type_direct_income,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$main_branch_id);
					}
					add_voucher('Sub_Reciept_Enrollment_bill',$req_acc,$reqSubscription_ID,$txtSubcription_Fee,$voucher_date,"Enrollment Fee TR NO: $reqtxtTR_No, Reciept No : $reqtxtReciept_No",$main_branch_id,$enroll_acc_trans_main_id,$req_enroll_fee_acc_id,$reqtxtPayment,$reqtxtCheque_No,$reqtxtCheque_Date,$reqtxtBank_Name);
				}							
			}// end of if 	
			$i++;
		}// end of Auction while loop	
		
		$curr_payble=Fngetvalue("auction","sum(Subcription-lasdevidentt)","auction.main_branch_id='$main_branch_id' and GroupNo='$reqtxtGrp_No' and AuctionDateTime is not null ");				
		$payble_plus=FnGetValue("auction inner join subscription_details on auction.Auction_ID=subscription_details.Auction_ID inner join transaction ON ( subscription_details.Transaction_ID = transaction.Transaction_ID )",
								 "sum(IFNULL(LateFee,0) + IFNULL(Subcription_Fee,0) + IFNULL(other_charge,0) + IFNULL(check_return_fee,0) + IFNULL(transfer_fee,0))",
								 "auction.main_branch_id='$main_branch_id' and auction.GroupNo = '$reqtxtGrp_No' and subscription_details.TicketNo='$to_Ticketno' and transaction.status<2"); 
											
		$paid=Fngetvalue("subscription_details inner join auction on subscription_details.Auction_ID=auction.Auction_ID inner join transaction ON ( subscription_details.Transaction_ID = transaction.Transaction_ID )  ",
						 "sum(subscription_details.Subcription)","auction.main_branch_id='$main_branch_id' and TicketNo='$to_Ticketno' and GroupNo='$reqtxtGrp_No' and transaction.status<2 ");
		$tot_payble=$curr_payble+$payble_plus;
		$member_due=($tot_payble-$paid);	
		if($member_due <=0 && $DaysCollection>0){
			$advance_ins_sql="insert into tbl_advance_payment(Group_No,Ticket_No,Transaction_ID,SubID,main_branch_id,Advance_Amount,LateFee,Subcription_Fee,other_charge,check_return_fee,transfer_fee)
							  values('$reqtxtGrp_No','$to_Ticketno','$reqTransaction_ID','$reqtxtSubID','$main_branch_id','$DaysCollection','','','','','')";
			mysql_query($advance_ins_sql) or die(mysql_error().$advance_ins_sql);
		}	

		

		$SubAcc 		= "";
		
		if ($one_acc_for_group==1){
			$req_arrears_of_subn_acc_type 	= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Arrears of Subscription' and lang = '$ses_lang'");								
			$req_arrears_of_subn_acc_id  	= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_arrears_of_subn_acc_type' and account_name='Arrears of Subscription - ".$reqtxtGrp_No."' and main_branch_id='$main_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
			if ($req_arrears_of_subn_acc_id==0){
				$req_arrears_of_subn_acc_id	= create_account('Arrears of Subscription - '.$reqtxtGrp_No,$req_arrears_of_subn_acc_type,'groupdetails',$to_Group_ID,$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$main_branch_id,$finacial_yr_id);
			}				
			$SubAcc = $req_arrears_of_subn_acc_id;
			if ($req_receipt_flag==2){ // Advance Reciept
				$req_sub_rec_in_adv_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Current Liabilities' and lang = '$ses_lang'");								
				$req_sub_rec_in_adv_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_sub_rec_in_adv_acc_type' and account_name='Subscription received in Advance' and main_branch_id='".$main_branch_id."' and acc_fianancial_year_id='$finacial_yr_id'");
				if ($req_sub_rec_in_adv_acc_id==0){
					$req_sub_rec_in_adv_acc_id=create_account('Subscription received in Advance',$req_sub_rec_in_adv_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$main_branch_id,$finacial_yr_id);
				}
			}
			
		}else{
			$SubAcc = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and related_type = 'applicantdetails' and `related_id` = '$related_id' and main_branch_id='$main_branch_id' and acc_fianancial_year_id='$finacial_yr_id' ");				
		}
		//echo $SubAcc;
		$req_to_acc_id =array();
		if ($req_receipt_flag==2){	$req_to_acc_id[0][0] 	= $req_sub_rec_in_adv_acc_id;		
		}else{						$req_to_acc_id[0][0] 	= $SubAcc;			}	
					
		$req_to_acc_id[0][1]	= $reqtxtamount;

		if ($reqtxtPayment=="adjustment"){
			$account_type_adjacement= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name ='Sundry Creditors' and lang = '$ses_lang'"); 
			$req_adjacement_acc= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang= '$ses_lang' and account_type ='$account_type_adjacement' and account_name='Adjacement' and main_branch_id='$main_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");    
			if ($req_adjacement_acc==0){ 
				$req_adjacement_acc=create_account("Adjacement",$account_type_adjacement,"","",$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$main_branch_id);
			}
			if ($amt_adj_cust >0){	    
				$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type ='receipt_adjace' and refered_id = '$reqTransaction_ID'");
				add_voucher('receipt_adjace',$req_adjacement_acc,$reqTransaction_ID,$amt_adj_cust,$reqtxtReciept_Date,"Adjustment Receipt No :$reqtxtReciept_No ",$main_branch_id,$acc_trans_main_id,$SubAcc);
			}	
			if (($one_acc_for_group==1) && ($check_for_intr_comm==1)){
				fn_add_agent_comm_bill_vch($related_id,$reqtxtReciept_Date,$reqTransaction_ID);
			}
		}		
		
		if ( $reqtxtPayment == "cheque" ){	
			if ($dep_slip_id >0 ){
				$dep_slip_bankname   = FnGetValue("tbl_dep_slip_details","BankName","dep_slip_ID = '".$dep_slip_id."'");						
				if ($cheque_rec_date_is_vch_date!=1){
					$req_voucher_date = FnGetValue("tbl_dep_slip_details","depslip_Date","dep_slip_ID = ".$dep_slip_id);
				}else{
					$req_voucher_date = $reqtxtReciept_Date;
				}
				
				if (($reqis_comp_acc==1)&&($one_acc_for_group==1)){
					$req_comp_invt_in_own_chit_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Investments' and lang = '$ses_lang'");								
					$req_comp_invt_in_own_chit_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_comp_invt_in_own_chit_acc_type' and account_name='Companies Investment In Own Chits' and main_branch_id='$main_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
					if ($req_comp_invt_in_own_chit_acc_id==0){
						$req_comp_invt_in_own_chit_acc_id=create_account('Companies Investment In Own Chits',$req_comp_invt_in_own_chit_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$main_branch_id,$finacial_yr_id);
					}
					$req_by_acc_id[0][0] = $req_comp_invt_in_own_chit_acc_id;
				}else{
					$req_by_acc_id[0][0] = fnGetValue("tbl_acc_accounts,banks","main_acc_account_id","tbl_acc_accounts.related_id=banks.Main_BankID and related_type='bank' and banks.lang= '$ses_lang' and tbl_acc_accounts.lang= '$ses_lang' and related_type  = 'bank' and banks.Bankname='$dep_slip_bankname' and banks.main_branch_id='$main_branch_id' and tbl_acc_accounts.main_branch_id='$main_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");						
				}				
				$req_by_acc_id[0][1] = $reqtxtamount;
				$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","(acc_transaction_type = 'Reciept_chk' or acc_transaction_type = 'Reciept' or acc_transaction_type = 'receipt_adjace') and refered_id = '$reqTransaction_ID'");		
				add_voucher('Reciept_chk',$req_by_acc_id,$reqTransaction_ID,$reqtxtamount,$req_voucher_date ,"Subscription TR NO: $reqtxtTR_No, Reciept No : ".$reqtxtReciept_No." by cheque ".$ChequeNo,$main_branch_id,$acc_trans_main_id,$req_to_acc_id);
				if (($one_acc_for_group==1) && ($check_for_intr_comm==1)){						
					fn_add_agent_comm_bill_vch($related_id,$req_voucher_date,$reqTransaction_ID);
				}
			}
		}elseif(($reqtxtPayment=="cash")||($reqtxtPayment=="neft")){
			if($reqtxtPayment=="neft"){
				$req_to_acc_id = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang= '$ses_lang' and related_type  = 'bank' and related_id='$reqtxtneft_bank' and main_branch_id='$main_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
			}else{
				$account_type_cash= fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name ='Cash-in-hand' and lang = '$ses_lang'"); 
				$req_to_acc_id= fnGetValue("tbl_acc_accounts","main_acc_account_id","lang= '$ses_lang' and account_type ='$account_type_cash' and account_name='Cash' and main_branch_id='$main_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");    				
			}
			if (($reqis_comp_acc==1)&&($one_acc_for_group==1)){
				$req_comp_invt_in_own_chit_acc_type = fnGetValue("tbl_acc_account_type","main_account_type_id","account_type_name = 'Investments' and lang = '$ses_lang'");								
				$req_comp_invt_in_own_chit_acc_id  = fnGetValue("tbl_acc_accounts","main_acc_account_id","lang = '$ses_lang' and account_type = '$req_comp_invt_in_own_chit_acc_type' and account_name='Companies Investment In Own Chits' and main_branch_id='$main_branch_id' and acc_fianancial_year_id='$finacial_yr_id'");
				if ($req_comp_invt_in_own_chit_acc_id==0){
					$req_comp_invt_in_own_chit_acc_id=create_account('Companies Investment In Own Chits',$req_comp_invt_in_own_chit_acc_type,'','',$opening_bal='0',$opening_bal_cr_dr='dr',$opening_bal_date='NOW()',$main_branch_id,$finacial_yr_id);
				}
				$req_to_acc_id=$req_comp_invt_in_own_chit_acc_id;
			}
			$acc_trans_main_id 	= fnGetValue("tbl_acc_relate","acc_transaction_id","acc_transaction_type = 'Reciept' and refered_id = '$reqTransaction_ID'");		
			add_voucher('Reciept',$SubAcc,$reqTransaction_ID,$reqtxtamount,$reqtxtReciept_Date,"Subscription TR NO: $reqtxtTR_No, Reciept No : $reqtxtReciept_No".(($reqtxtPayment=="neft")?" Payment By NEFT":(($reqtxtPayment=="div_adj")?" Dividend Adjustment":"")),$main_branch_id,$acc_trans_main_id,$req_to_acc_id);
			
			if (($one_acc_for_group==1) && ($check_for_intr_comm==1)){
				fn_add_agent_comm_bill_vch($related_id,$reqtxtReciept_Date,$reqTransaction_ID);
			}
		}
		$delsubsql="DELETE FROM subscription_details WHERE Transaction_ID='$reqTransaction_ID' and subscription_details.TicketNo='$from_Ticketno'";
		$res_sql=mysql_query($delsubsql) or die(mysql_error()."<BR>".$delsubsql);
	}//End of Transaction while loop
}


?>