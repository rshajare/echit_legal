        google.load("language", "1");
    function initialize(txtcontrol,engword,engkey,lang) {

      //var content = document.getElementById('content');
      // Setting the text in the div.
      //content.innerHTML = '<div id="text">Hola, me alegro mucho de verte.<\/div><div id="translation"/>';
      // Grabbing the text to translate
      var text = engword;
	  var mainkey = engkey;
	  var clang = lang;
	  var keygen  = txtcontrol+mainkey;
	  	//alert(text+clang+keygen);
	  //alert(text+"== "+mainkey+" "+keygen)
      // Translate from Spanish to English, and have the callback of the request
      // put the resulting translation in the "translation" div.
      // Note: by putting in an empty string for the source language ('es') then the translation
      // will auto-detect the source language.
	    google.language.translate(text, 'en', clang, function(result) {
        	var translated = document.getElementById(keygen);
			//alert(translated);
       	 	if (result.translation) {
				//alert(result.translation);
          		document.getElementById(keygen).value = result.translation;
        	}
     	 }
	  	);
    }
