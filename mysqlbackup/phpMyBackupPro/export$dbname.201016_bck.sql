-- MySQL dump 10.13  Distrib 5.1.36, for Win32 (ia32)
--
-- Host: localhost    Database: chit_new
-- ------------------------------------------------------
-- Server version	5.1.36-community-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `applicantdetails`
--

DROP TABLE IF EXISTS `applicantdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `applicantdetails` (
  `AppID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Ticketno` int(11) DEFAULT NULL,
  `GroupNo` varchar(50) DEFAULT NULL,
  `SubCodeNo` varchar(50) DEFAULT NULL,
  `wit1name` varchar(50) DEFAULT NULL,
  `wit1addrs` varchar(255) DEFAULT NULL,
  `wit2name` varchar(50) DEFAULT NULL,
  `wit2addrs` varchar(255) DEFAULT NULL,
  `prizeamount` int(11) DEFAULT NULL,
  `prizechequeno` varchar(50) DEFAULT NULL,
  `prizebankname` varchar(50) DEFAULT NULL,
  `prizechequedate` date DEFAULT NULL,
  `prizemonth` varchar(50) DEFAULT NULL,
  `auctiondate` datetime DEFAULT NULL,
  `amtinword` varchar(50) DEFAULT NULL,
  `prizeamtinword` varchar(50) DEFAULT NULL,
  `installmentinword` varchar(50) DEFAULT NULL,
  `shuretyname1` varchar(50) DEFAULT NULL,
  `shuretyadd1` varchar(255) DEFAULT NULL,
  `shuretytelno1` varchar(50) DEFAULT NULL,
  `shuretypanno1` varchar(50) DEFAULT NULL,
  `shuretymobno1` varchar(50) DEFAULT NULL,
  `shuretycode1` varchar(50) DEFAULT NULL,
  `shuretyname2` varchar(50) DEFAULT NULL,
  `shuretyadd2` varchar(255) DEFAULT NULL,
  `shuretytelno2` varchar(50) DEFAULT NULL,
  `shuretypanno2` varchar(50) DEFAULT NULL,
  `shuretymobno2` varchar(50) DEFAULT NULL,
  `shuretycode2` varchar(50) DEFAULT NULL,
  `installment` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `Sustsubcode` text,
  `remdate` datetime DEFAULT NULL,
  `prizeinstno` int(11) DEFAULT NULL,
  `BlankCh1No` varchar(50) DEFAULT NULL,
  `BlankCh1Bankname` varchar(50) DEFAULT NULL,
  `BlankCh2No` varchar(50) DEFAULT NULL,
  `BlankCh2Bankname` varchar(50) DEFAULT NULL,
  `chrettime` int(11) DEFAULT NULL,
  `latetime` int(11) DEFAULT NULL,
  `SpecInstruction` varchar(255) DEFAULT NULL,
  `main_branch_id` int(11) DEFAULT NULL,
  `foreman_comm` varchar(200) NOT NULL,
  PRIMARY KEY (`AppID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicantdetails`
--

LOCK TABLES `applicantdetails` WRITE;
/*!40000 ALTER TABLE `applicantdetails` DISABLE KEYS */;
INSERT INTO `applicantdetails` VALUES (7,1,'AM1','1211','','','','',12000,'1211','CANARA BANK','2010-05-20','Jun2010','0000-00-00 00:00:00','','','','rajendra sheti','kk','','','','','','1','','','','',0,0,'','0000-00-00 00:00:00',2,'','','','',0,0,'',1,'5.618'),(8,2,'AM1','121','','','','',12000,'123123123','BANK OF MAHARASHTRA','2010-04-30','Jul2010','0000-00-00 00:00:00','','','','Shurety1','121','','','','','Shurety2Shurety1','11','','','','',0,0,'','0000-00-00 00:00:00',3,'','','','',0,0,'',1,'5.618'),(9,3,'AM1','A/1/2','','','','',100000,'213123','ANNASAHEB MAGAR S BANK','2010-05-06','Aug2010','0000-00-00 00:00:00','','','','','','','','','','','','','','','',0,0,'','0000-00-00 00:00:00',4,'','','','',0,0,'',1,'5.618'),(10,4,'AM1','1211','','','','',100000,'213123','ANNASAHEB MAGAR S BANK','2010-05-14','Aug2010','0000-00-00 00:00:00','','','','','','','','','','','','','','','',0,0,'','0000-00-00 00:00:00',5,'','','','',0,0,'',1,'5.618');
/*!40000 ALTER TABLE `applicantdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `area` (
  `AreaID` int(11) NOT NULL AUTO_INCREMENT,
  `Areaname` varchar(50) DEFAULT NULL,
  `main_branch_id` int(11) NOT NULL,
  `lang` varchar(100) NOT NULL,
  `Main_AreaID` int(11) NOT NULL,
  PRIMARY KEY (`AreaID`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area`
--

LOCK TABLES `area` WRITE;
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
INSERT INTO `area` VALUES (1,'Pimpri',2,'english',1),(2,'Pune',1,'english',2),(3,'Out Station',0,'english',3),(4,'Kalewadi',2,'english',4),(6,'Pune Camp',1,'english',6),(8,'Sath Rasta',1,'english',8),(9,'DYMK',0,'english',9),(10,'पिम्परी',1,'hindi',1),(11,'पुणे',5,'hindi',2),(12,'बाहर स्टेशन',1,'hindi',3),(13,'Kalewadi',5,'hindi',4),(15,'पुणे शिविर',5,'hindi',6),(16,'Sath Rasta',5,'hindi',8),(17,'DYMK',5,'hindi',9),(18,'Ganesh Peth',5,'english',18),(19,'गणेश Peth',5,'hindi',18);
/*!40000 ALTER TABLE `area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auction`
--

DROP TABLE IF EXISTS `auction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auction` (
  `Auction_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `GroupNo` varchar(15) NOT NULL,
  `InstNo` int(11) NOT NULL,
  `Month` varchar(100) NOT NULL,
  `AuctionDateTime` datetime DEFAULT NULL,
  `LastDateofPayment` date DEFAULT NULL,
  `lasdevidentt` int(11) DEFAULT NULL,
  `prizegiven` tinyint(4) DEFAULT NULL,
  `Subcription` bigint(20) DEFAULT NULL,
  `main_branch_id` int(11) NOT NULL,
  PRIMARY KEY (`Auction_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=269 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auction`
--

LOCK TABLES `auction` WRITE;
/*!40000 ALTER TABLE `auction` DISABLE KEYS */;
INSERT INTO `auction` VALUES (1,'AM1',1,'May2010','2010-05-12 14:01:00','2010-05-19',708,1,25000,1),(2,'AM1',2,'Jun2010','2010-05-12 03:02:00','2010-05-11',3000,1,25000,1),(3,'AM1',3,'Jul2010','2010-05-05 06:02:00','2010-04-15',1596,1,25000,1),(4,'AM1',4,'Aug2010','2010-04-27 15:15:00','2010-04-26',23596,1,2500,1),(5,'AM1',3,'Jul2010','2010-05-05 06:02:00','2010-04-15',1596,1,25000,1),(6,'AM1',4,'Aug2010','2010-04-27 15:15:00','2010-04-26',23596,1,2500,1),(7,'AM1',5,'Aug2010','2010-04-30 16:30:00','2010-04-29',23596,1,2500,1),(8,'AM1',6,'Sep2010',NULL,NULL,0,0,0,1),(9,'AM1',7,'Oct2010',NULL,NULL,0,0,0,1),(10,'AM1',8,'Nov2010',NULL,NULL,0,0,0,1),(11,'AM1',9,'Dec2010',NULL,NULL,0,0,0,1),(12,'AM1',10,'Jan2011',NULL,NULL,0,0,0,1),(13,'AM1',11,'Feb2011',NULL,NULL,0,0,0,1),(14,'AM1',12,'Mar2011',NULL,NULL,0,0,0,1),(15,'AM1',13,'Apr2011',NULL,NULL,0,0,0,1),(16,'AM1',14,'May2011',NULL,NULL,0,0,0,1),(17,'AM1',15,'Jun2011',NULL,NULL,0,0,0,1),(18,'AM1',16,'Jul2011',NULL,NULL,0,0,0,1),(19,'AM1',17,'Aug2011',NULL,NULL,0,0,0,1),(20,'AM1',18,'Sep2011',NULL,NULL,0,0,0,1),(21,'AM1',19,'Oct2011',NULL,NULL,0,0,0,1),(22,'AM1',20,'Nov2011',NULL,NULL,0,0,0,1),(23,'AM1',21,'Dec2011',NULL,NULL,0,0,0,1),(24,'AM1',22,'Jan2012',NULL,NULL,0,0,0,1),(25,'AM1',23,'Feb2012',NULL,NULL,0,0,0,1),(26,'AM1',24,'Mar2012',NULL,NULL,0,0,0,1),(27,'AM1',25,'Apr2012',NULL,NULL,0,0,0,1),(28,'AM1',26,'May2012',NULL,NULL,0,0,0,1),(29,'AM1',27,'Jun2012',NULL,NULL,0,0,0,1),(30,'AM1',28,'Jul2012',NULL,NULL,0,0,0,1),(31,'AM1',29,'Aug2012',NULL,NULL,0,0,0,1),(32,'AM1',30,'Sep2012',NULL,NULL,0,0,0,1),(33,'AM1',31,'Oct2012',NULL,NULL,0,0,0,1),(34,'AM1',32,'Nov2012',NULL,NULL,0,0,0,1),(35,'AM1',33,'Dec2012',NULL,NULL,0,0,0,1),(36,'AM1',34,'Jan2013',NULL,NULL,0,0,0,1),(37,'AM1',35,'Feb2013',NULL,NULL,0,0,0,1),(38,'AM1',36,'Mar2013',NULL,NULL,0,0,0,1),(39,'AM1',37,'Apr2013',NULL,NULL,0,0,0,1),(40,'AM1',38,'May2013',NULL,NULL,0,0,0,1),(41,'AM1',39,'Jun2013',NULL,NULL,0,0,0,1),(42,'AM1',40,'Jul2013',NULL,NULL,0,0,0,1),(43,'chkZM1',1,'Mar2010','2010-04-30 11:40:00','2010-04-29',0,0,2500,2),(44,'chkZM1',2,'Apr2010',NULL,NULL,0,0,0,2),(45,'chkZM1',3,'May2010',NULL,NULL,0,0,0,2),(46,'chkZM1',4,'Jun2010',NULL,NULL,0,0,0,2),(47,'chkZM1',5,'Jul2010',NULL,NULL,0,0,0,2),(48,'chkZM1',6,'Aug2010',NULL,NULL,0,0,0,2),(49,'chkZM1',7,'Sep2010',NULL,NULL,0,0,0,2),(50,'chkZM1',8,'Oct2010',NULL,NULL,0,0,0,2),(51,'chkZM1',9,'Nov2010',NULL,NULL,0,0,0,2),(52,'chkZM1',10,'Dec2010',NULL,NULL,0,0,0,2),(53,'chkZM1',11,'Jan2011',NULL,NULL,0,0,0,2),(54,'chkZM1',12,'Feb2011',NULL,NULL,0,0,0,2),(55,'chkZM1',13,'Mar2011',NULL,NULL,0,0,0,2),(56,'chkZM1',14,'Apr2011',NULL,NULL,0,0,0,2),(57,'chkZM1',15,'May2011',NULL,NULL,0,0,0,2),(58,'chkZM1',16,'Jun2011',NULL,NULL,0,0,0,2),(59,'chkZM1',17,'Jul2011',NULL,NULL,0,0,0,2),(60,'chkZM1',18,'Aug2011',NULL,NULL,0,0,0,2),(61,'chkZM1',19,'Sep2011',NULL,NULL,0,0,0,2),(62,'chkZM1',20,'Oct2011',NULL,NULL,0,0,0,2),(63,'AM1',1,'May2010','2010-05-12 14:01:00','2010-05-19',708,1,25000,1),(64,'AM1',2,'Jun2010','2010-05-12 03:02:00','2010-05-11',3000,1,25000,1),(65,'AM1',3,'Jul2010','2010-05-05 06:02:00','2010-04-15',1596,1,25000,1),(66,'AM1',4,'Aug2010',NULL,NULL,23596,1,0,1),(67,'AM1',5,'Aug2010',NULL,NULL,23596,1,0,1),(68,'AM1',6,'Sep2010',NULL,NULL,0,0,0,1),(69,'AM1',7,'Oct2010',NULL,NULL,0,0,0,1),(70,'AM1',8,'Nov2010',NULL,NULL,0,0,0,1),(71,'AM1',9,'Dec2010',NULL,NULL,0,0,0,1),(72,'AM1',10,'Jan2011',NULL,NULL,0,0,0,1),(73,'AM1',11,'Feb2011',NULL,NULL,0,0,0,1),(74,'AM1',12,'Mar2011',NULL,NULL,0,0,0,1),(75,'AM1',13,'Apr2011',NULL,NULL,0,0,0,1),(76,'AM1',14,'May2011',NULL,NULL,0,0,0,1),(77,'AM1',15,'Jun2011',NULL,NULL,0,0,0,1),(78,'AM1',16,'Jul2011',NULL,NULL,0,0,0,1),(79,'AM1',17,'Aug2011',NULL,NULL,0,0,0,1),(80,'AM1',18,'Sep2011',NULL,NULL,0,0,0,1),(81,'AM1',19,'Oct2011',NULL,NULL,0,0,0,1),(82,'AM1',20,'Nov2011',NULL,NULL,0,0,0,1),(83,'AM1',21,'Dec2011',NULL,NULL,0,0,0,1),(84,'AM1',22,'Jan2012',NULL,NULL,0,0,0,1),(85,'AM1',23,'Feb2012',NULL,NULL,0,0,0,1),(86,'AM1',24,'Mar2012',NULL,NULL,0,0,0,1),(87,'AM1',25,'Apr2012',NULL,NULL,0,0,0,1),(88,'AM1',26,'May2012',NULL,NULL,0,0,0,1),(89,'AM1',27,'Jun2012',NULL,NULL,0,0,0,1),(90,'AM1',28,'Jul2012',NULL,NULL,0,0,0,1),(91,'AM1',29,'Aug2012',NULL,NULL,0,0,0,1),(92,'AM1',30,'Sep2012',NULL,NULL,0,0,0,1),(93,'AM1',31,'Oct2012',NULL,NULL,0,0,0,1),(94,'AM1',32,'Nov2012',NULL,NULL,0,0,0,1),(95,'AM1',33,'Dec2012',NULL,NULL,0,0,0,1),(96,'AM1',34,'Jan2013',NULL,NULL,0,0,0,1),(97,'AM1',35,'Feb2013',NULL,NULL,0,0,0,1),(98,'AM1',36,'Mar2013',NULL,NULL,0,0,0,1),(99,'AM1',37,'Apr2013',NULL,NULL,0,0,0,1),(100,'AM1',38,'May2013',NULL,NULL,0,0,0,1),(101,'AM1',39,'Jun2013',NULL,NULL,0,0,0,1),(102,'AM1',40,'Jul2013',NULL,NULL,0,0,0,1),(103,'ajmAM1',1,'Mar2010','2010-05-12 02:02:00','2010-05-12',0,0,2500,1),(104,'ajmAM1',2,'Apr2010',NULL,NULL,0,0,0,1),(105,'ajmAM1',3,'May2010',NULL,NULL,0,0,0,1),(106,'ajmAM1',4,'Jun2010',NULL,NULL,0,0,0,1),(107,'ajmAM1',5,'Jul2010',NULL,NULL,0,0,0,1),(108,'ajmAM1',6,'Aug2010',NULL,NULL,0,0,0,1),(109,'ajmAM1',7,'Sep2010',NULL,NULL,0,0,0,1),(110,'ajmAM1',8,'Oct2010',NULL,NULL,0,0,0,1),(111,'ajmAM1',9,'Nov2010',NULL,NULL,0,0,0,1),(112,'ajmAM1',10,'Dec2010',NULL,NULL,0,0,0,1),(113,'ajmAM1',11,'Jan2011',NULL,NULL,0,0,0,1),(114,'ajmAM1',12,'Feb2011',NULL,NULL,0,0,0,1),(115,'ajmAM1',13,'Mar2011',NULL,NULL,0,0,0,1),(116,'ajmAM1',14,'Apr2011',NULL,NULL,0,0,0,1),(117,'ajmAM1',15,'May2011',NULL,NULL,0,0,0,1),(118,'ajmAM1',16,'Jun2011',NULL,NULL,0,0,0,1),(119,'ajmAM1',17,'Jul2011',NULL,NULL,0,0,0,1),(120,'ajmAM1',18,'Aug2011',NULL,NULL,0,0,0,1),(121,'ajmAM1',19,'Sep2011',NULL,NULL,0,0,0,1),(122,'ajmAM1',20,'Oct2011',NULL,NULL,0,0,0,1),(123,'ajmAM1',21,'Nov2011',NULL,NULL,0,0,0,1),(124,'ajmAM1',22,'Dec2011',NULL,NULL,0,0,0,1),(125,'ajmAM1',23,'Jan2012',NULL,NULL,0,0,0,1),(126,'ajmAM1',24,'Feb2012',NULL,NULL,0,0,0,1),(127,'ajmAM1',25,'Mar2012',NULL,NULL,0,0,0,1),(128,'ajmAM1',26,'Apr2012',NULL,NULL,0,0,0,1),(129,'ajmAM1',27,'May2012',NULL,NULL,0,0,0,1),(130,'ajmAM1',28,'Jun2012',NULL,NULL,0,0,0,1),(131,'ajmAM1',29,'Jul2012',NULL,NULL,0,0,0,1),(132,'ajmAM1',30,'Aug2012',NULL,NULL,0,0,0,1),(133,'ajmAM1',31,'Sep2012',NULL,NULL,0,0,0,1),(134,'ajmAM1',32,'Oct2012',NULL,NULL,0,0,0,1),(135,'ajmAM1',33,'Nov2012',NULL,NULL,0,0,0,1),(136,'ajmAM1',34,'Dec2012',NULL,NULL,0,0,0,1),(137,'ajmAM1',35,'Jan2013',NULL,NULL,0,0,0,1),(138,'ajmAM1',36,'Feb2013',NULL,NULL,0,0,0,1),(139,'ajmAM1',37,'Mar2013',NULL,NULL,0,0,0,1),(140,'ajmAM1',38,'Apr2013',NULL,NULL,0,0,0,1),(141,'ajmAM1',39,'May2013',NULL,NULL,0,0,0,1),(142,'ajmAM1',40,'Jun2013',NULL,NULL,0,0,0,1),(143,'ajmAM1',1,'Mar2010','2010-05-12 02:02:00','2010-05-12',0,0,2500,1),(144,'ajmAM1',2,'Apr2010',NULL,NULL,0,0,0,1),(145,'ajmAM1',3,'May2010',NULL,NULL,0,0,0,1),(146,'ajmAM1',4,'Jun2010',NULL,NULL,0,0,0,1),(147,'ajmAM1',5,'Jul2010',NULL,NULL,0,0,0,1),(148,'ajmAM1',6,'Aug2010',NULL,NULL,0,0,0,1),(149,'ajmAM1',7,'Sep2010',NULL,NULL,0,0,0,1),(150,'ajmAM1',8,'Oct2010',NULL,NULL,0,0,0,1),(151,'ajmAM1',9,'Nov2010',NULL,NULL,0,0,0,1),(152,'ajmAM1',10,'Dec2010',NULL,NULL,0,0,0,1),(153,'ajmAM1',11,'Jan2011',NULL,NULL,0,0,0,1),(154,'ajmAM1',12,'Feb2011',NULL,NULL,0,0,0,1),(155,'ajmAM1',13,'Mar2011',NULL,NULL,0,0,0,1),(156,'ajmAM1',14,'Apr2011',NULL,NULL,0,0,0,1),(157,'ajmAM1',15,'May2011',NULL,NULL,0,0,0,1),(158,'ajmAM1',16,'Jun2011',NULL,NULL,0,0,0,1),(159,'ajmAM1',17,'Jul2011',NULL,NULL,0,0,0,1),(160,'ajmAM1',18,'Aug2011',NULL,NULL,0,0,0,1),(161,'ajmAM1',19,'Sep2011',NULL,NULL,0,0,0,1),(162,'ajmAM1',20,'Oct2011',NULL,NULL,0,0,0,1),(163,'ajmAM1',21,'Nov2011',NULL,NULL,0,0,0,1),(164,'ajmAM1',22,'Dec2011',NULL,NULL,0,0,0,1),(165,'ajmAM1',23,'Jan2012',NULL,NULL,0,0,0,1),(166,'ajmAM1',24,'Feb2012',NULL,NULL,0,0,0,1),(167,'ajmAM1',25,'Mar2012',NULL,NULL,0,0,0,1),(168,'ajmAM1',26,'Apr2012',NULL,NULL,0,0,0,1),(169,'ajmAM1',27,'May2012',NULL,NULL,0,0,0,1),(170,'ajmAM1',28,'Jun2012',NULL,NULL,0,0,0,1),(171,'ajmAM1',29,'Jul2012',NULL,NULL,0,0,0,1),(172,'ajmAM1',30,'Aug2012',NULL,NULL,0,0,0,1),(173,'ajmAM1',31,'Sep2012',NULL,NULL,0,0,0,1),(174,'ajmAM1',32,'Oct2012',NULL,NULL,0,0,0,1),(175,'ajmAM1',33,'Nov2012',NULL,NULL,0,0,0,1),(176,'ajmAM1',34,'Dec2012',NULL,NULL,0,0,0,1),(177,'ajmAM1',35,'Jan2013',NULL,NULL,0,0,0,1),(178,'ajmAM1',36,'Feb2013',NULL,NULL,0,0,0,1),(179,'ajmAM1',37,'Mar2013',NULL,NULL,0,0,0,1),(180,'ajmAM1',38,'Apr2013',NULL,NULL,0,0,0,1),(181,'ajmAM1',39,'May2013',NULL,NULL,0,0,0,1),(182,'ajmAM1',40,'Jun2013',NULL,NULL,0,0,0,1),(183,'DRDZM1',1,'Apr2010','2010-04-15 15:08:00','2010-04-15',0,0,2273,7),(184,'DRDZM1',2,'May2010',NULL,NULL,0,0,0,7),(185,'DRDZM1',3,'Jun2010',NULL,NULL,0,0,0,7),(186,'DRDZM1',4,'Jul2010',NULL,NULL,0,0,0,7),(187,'DRDZM1',5,'Aug2010',NULL,NULL,0,0,0,7),(188,'DRDZM1',6,'Sep2010',NULL,NULL,0,0,0,7),(189,'DRDZM1',7,'Oct2010',NULL,NULL,0,0,0,7),(190,'DRDZM1',8,'Nov2010',NULL,NULL,0,0,0,7),(191,'DRDZM1',9,'Dec2010',NULL,NULL,0,0,0,7),(192,'DRDZM1',10,'Jan2011',NULL,NULL,0,0,0,7),(193,'DRDZM1',11,'Feb2011',NULL,NULL,0,0,0,7),(194,'DRDZM1',12,'Mar2011',NULL,NULL,0,0,0,7),(195,'DRDZM1',13,'Apr2011',NULL,NULL,0,0,0,7),(196,'DRDZM1',14,'May2011',NULL,NULL,0,0,0,7),(197,'DRDZM1',15,'Jun2011',NULL,NULL,0,0,0,7),(198,'DRDZM1',16,'Jul2011',NULL,NULL,0,0,0,7),(199,'DRDZM1',17,'Aug2011',NULL,NULL,0,0,0,7),(200,'DRDZM1',18,'Sep2011',NULL,NULL,0,0,0,7),(201,'DRDZM1',19,'Oct2011',NULL,NULL,0,0,0,7),(202,'DRDZM1',20,'Nov2011',NULL,NULL,0,0,0,7),(203,'DRDZM1',21,'Dec2011',NULL,NULL,0,0,0,7),(204,'DRDZM1',22,'Jan2012',NULL,NULL,0,0,0,7),(205,'ajmAM1',1,'Mar2010','2010-05-12 02:02:00','2010-05-12',0,0,2500,1),(206,'ajmAM1',2,'Apr2010',NULL,NULL,0,0,0,1),(207,'ajmAM1',3,'May2010',NULL,NULL,0,0,0,1),(208,'ajmAM1',4,'Jun2010',NULL,NULL,0,0,0,1),(209,'ajmAM1',5,'Jul2010',NULL,NULL,0,0,0,1),(210,'ajmAM1',6,'Aug2010',NULL,NULL,0,0,0,1),(211,'ajmAM1',7,'Sep2010',NULL,NULL,0,0,0,1),(212,'ajmAM1',8,'Oct2010',NULL,NULL,0,0,0,1),(213,'ajmAM1',9,'Nov2010',NULL,NULL,0,0,0,1),(214,'ajmAM1',10,'Dec2010',NULL,NULL,0,0,0,1),(215,'ajmAM1',11,'Jan2011',NULL,NULL,0,0,0,1),(216,'ajmAM1',12,'Feb2011',NULL,NULL,0,0,0,1),(217,'ajmAM1',13,'Mar2011',NULL,NULL,0,0,0,1),(218,'ajmAM1',14,'Apr2011',NULL,NULL,0,0,0,1),(219,'ajmAM1',15,'May2011',NULL,NULL,0,0,0,1),(220,'ajmAM1',16,'Jun2011',NULL,NULL,0,0,0,1),(221,'ajmAM1',17,'Jul2011',NULL,NULL,0,0,0,1),(222,'ajmAM1',18,'Aug2011',NULL,NULL,0,0,0,1),(223,'ajmAM1',19,'Sep2011',NULL,NULL,0,0,0,1),(224,'ajmAM1',20,'Oct2011',NULL,NULL,0,0,0,1),(225,'ajmAM1',21,'Nov2011',NULL,NULL,0,0,0,1),(226,'ajmAM1',22,'Dec2011',NULL,NULL,0,0,0,1),(227,'ajmAM1',23,'Jan2012',NULL,NULL,0,0,0,1),(228,'ajmAM1',24,'Feb2012',NULL,NULL,0,0,0,1),(229,'ajmAM1',25,'Mar2012',NULL,NULL,0,0,0,1),(230,'ajmAM1',26,'Apr2012',NULL,NULL,0,0,0,1),(231,'ajmAM1',27,'May2012',NULL,NULL,0,0,0,1),(232,'ajmAM1',28,'Jun2012',NULL,NULL,0,0,0,1),(233,'ajmAM1',29,'Jul2012',NULL,NULL,0,0,0,1),(234,'ajmAM1',30,'Aug2012',NULL,NULL,0,0,0,1),(235,'ajmAM1',31,'Sep2012',NULL,NULL,0,0,0,1),(236,'ajmAM1',32,'Oct2012',NULL,NULL,0,0,0,1),(237,'ajmAM1',33,'Nov2012',NULL,NULL,0,0,0,1),(238,'ajmAM1',34,'Dec2012',NULL,NULL,0,0,0,1),(239,'ajmAM1',35,'Jan2013',NULL,NULL,0,0,0,1),(240,'ajmAM1',36,'Feb2013',NULL,NULL,0,0,0,1),(241,'ajmAM1',37,'Mar2013',NULL,NULL,0,0,0,1),(242,'ajmAM1',38,'Apr2013',NULL,NULL,0,0,0,1),(243,'ajmAM1',39,'May2013',NULL,NULL,0,0,0,1),(244,'ajmAM1',40,'Jun2013',NULL,NULL,0,0,0,1),(245,'AM1',1,'May2010','2010-05-12 14:01:00','2010-05-19',0,0,25000,1),(246,'AM1',2,'Jun2010','2010-05-12 03:02:00','2010-05-11',3000,1,25000,1),(247,'AM1',3,'Jul2010','2010-05-05 06:02:00','2010-04-15',1596,1,25000,1),(248,'AM1',4,'Aug2010',NULL,NULL,23596,1,0,1),(249,'AM1',1,'May2010','2010-05-12 14:01:00','2010-05-19',0,0,25000,1),(250,'AM1',2,'Jun2010','2010-05-12 03:02:00','2010-05-11',3000,1,25000,1),(251,'AM1',3,'Jul2010','2010-05-05 06:02:00','2010-04-15',1596,1,25000,1),(252,'AM1',4,'Aug2010',NULL,NULL,23596,1,0,1),(253,'AM1',1,'May2010','2010-05-12 14:01:00','2010-05-19',0,0,25000,1),(254,'AM1',2,'Jun2010','2010-05-12 03:02:00','2010-05-11',3000,1,25000,1),(255,'AM1',3,'Jul2010','2010-05-05 06:02:00','2010-04-15',1596,1,25000,1),(256,'AM1',4,'Aug2010',NULL,NULL,23596,1,0,1),(257,'AM1',1,'May2010','2010-05-12 14:01:00','2010-05-19',0,0,25000,1),(258,'AM1',2,'Jun2010','2010-05-12 03:02:00','2010-05-11',0,0,25000,1),(259,'AM1',3,'Jul2010','2010-05-05 06:02:00','2010-04-15',0,0,25000,1),(260,'AM1',4,'Aug2010',NULL,NULL,23596,1,0,1),(261,'AM1',1,'May2010','2010-05-12 14:01:00','2010-05-19',0,0,25000,1),(263,'AM1',3,'Jul2010','2010-05-05 06:02:00','2010-04-15',0,0,25000,1),(264,'AM1',4,'Aug2010',NULL,NULL,23596,1,0,1),(265,'AM6',1,'May2010',NULL,NULL,0,0,0,1),(266,'AM6',2,'Jun2010',NULL,NULL,0,0,0,1),(267,'AM6',3,'Jul2010',NULL,NULL,0,0,0,1),(268,'AM6',4,'Aug2010',NULL,NULL,0,0,0,1);
/*!40000 ALTER TABLE `auction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banks`
--

DROP TABLE IF EXISTS `banks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banks` (
  `BankID` int(11) NOT NULL AUTO_INCREMENT,
  `Bankname` varchar(50) DEFAULT NULL,
  `Acno` varchar(50) DEFAULT NULL,
  `lang` varchar(100) DEFAULT NULL,
  `Main_BankID` int(11) DEFAULT NULL,
  PRIMARY KEY (`BankID`)
) ENGINE=InnoDB AUTO_INCREMENT=645 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banks`
--

LOCK TABLES `banks` WRITE;
/*!40000 ALTER TABLE `banks` DISABLE KEYS */;
INSERT INTO `banks` VALUES (533,'HDFC BANK','1211','english',533),(534,'ICICI BANK1','0121121','english',534),(535,'S S J M S BANK','012121','english',535),(536,'S B T','232222','english',536),(537,'Centurion Bank','','english',537),(538,'BANK OF MAHARASHTRA','','english',538),(539,'RUPEE CO-OP BANK','','english',539),(540,'BANK OF INDIA','','english',540),(541,'S G L S BANK','','english',541),(542,'CANARA BANK','','english',542),(543,'KARNATAKA BANK','','english',543),(544,'CENTRAL BANK','0','english',544),(545,'COSMOS CO-OP BANK','0','english',545),(546,'SHAMRAO VITHAL CO-OP. BANK','','english',546),(547,'VISHWESHWAR SAH. BANK','0','english',547),(548,'P C S BANK','0','english',548),(549,'P D C CO-OP. BANK','0','english',549),(550,'SHREE JYOTIBA S BANK','0','english',550),(551,'SEVA VIKAS CO-OP BANK','0','english',551),(552,'JIJAMATA MAHILA S BANK','0','english',552),(553,'ANNASAHEB MAGAR S BANK','0','english',553),(554,'PRERNA CO-OP BANK','0','english',554),(555,'UNION BANK OF INDIA','0','english',555),(556,'BANK OF BARODA','0','english',556),(557,'SEVA VIKAS CO-OP.BANK','0','english',557),(620,'???????? BANK1','0','hindi',533),(621,'?????????? BANK1','0','hindi',534),(622,'SSJMS ????','0','hindi',535),(623,'SBT','0','hindi',536),(624,'????????? ????','0','hindi',537),(625,'?????????? ?? ????','0','hindi',538),(626,'0','0','hindi',539),(627,'0','0','hindi',540),(628,'0','0','hindi',541),(629,'0','0','hindi',542),(630,'0','0','hindi',543),(631,'0','0','hindi',544),(632,'0','0','hindi',545),(633,'0','0','hindi',546),(634,'0','0','hindi',547),(635,'0','0','hindi',548),(636,'0','0','hindi',549),(637,'0','0','hindi',550),(638,'0','0','hindi',551),(639,'0','0','hindi',552),(640,'0','0','hindi',553),(641,'0','0','hindi',554),(642,'0','0','hindi',555),(643,'0','0','hindi',556),(644,'0','0','hindi',557);
/*!40000 ALTER TABLE `banks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chequeprint`
--

DROP TABLE IF EXISTS `chequeprint`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chequeprint` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` longtext,
  `Amount` longtext,
  `Amountintext` varchar(255) DEFAULT NULL,
  `Chequedate` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chequeprint`
--

LOCK TABLES `chequeprint` WRITE;
/*!40000 ALTER TABLE `chequeprint` DISABLE KEYS */;
/*!40000 ALTER TABLE `chequeprint` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chitpaymentslip`
--

DROP TABLE IF EXISTS `chitpaymentslip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chitpaymentslip` (
  `ChitpaymentSlipID` bigint(20) NOT NULL AUTO_INCREMENT,
  `GroupNo` varchar(50) DEFAULT NULL,
  `TicketNo` varchar(50) DEFAULT NULL,
  `SubName` varchar(255) DEFAULT NULL,
  `InstNo` varchar(50) DEFAULT NULL,
  `Chitvalue` int(11) DEFAULT NULL,
  `Discount` int(11) DEFAULT NULL,
  `subscriptiondue` int(11) DEFAULT NULL,
  `ChequeNo` varchar(50) DEFAULT NULL,
  `ChequeDate` varchar(50) DEFAULT NULL,
  `FinalPayment` int(11) DEFAULT NULL,
  `FinalPaymentInWord` varchar(255) DEFAULT NULL,
  `TRNo` varchar(50) DEFAULT NULL,
  `Loan` int(11) DEFAULT NULL,
  `LoanChequeNo` varchar(50) DEFAULT NULL,
  `ChequeBankName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ChitpaymentSlipID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chitpaymentslip`
--

LOCK TABLES `chitpaymentslip` WRITE;
/*!40000 ALTER TABLE `chitpaymentslip` DISABLE KEYS */;
/*!40000 ALTER TABLE `chitpaymentslip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `form13`
--

DROP TABLE IF EXISTS `form13`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `form13` (
  `formID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Subscription` int(11) DEFAULT NULL,
  `Chitregno` varchar(255) DEFAULT NULL,
  `Chitamount` int(11) DEFAULT NULL,
  `Rciptsdiv` int(11) DEFAULT NULL,
  `interest` int(11) DEFAULT NULL,
  `fdamount` int(11) DEFAULT NULL,
  `oustanding` int(11) DEFAULT NULL,
  `prizeamount` int(11) DEFAULT NULL,
  `formanscom` int(11) DEFAULT NULL,
  `Groupno` varchar(50) DEFAULT NULL,
  `Instno` varchar(50) DEFAULT NULL,
  `Duration` int(11) DEFAULT NULL,
  `fdno` varchar(50) DEFAULT NULL,
  `fdbank` varchar(50) DEFAULT NULL,
  `fddate` datetime DEFAULT NULL,
  `prtdate` datetime DEFAULT NULL,
  PRIMARY KEY (`formID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `form13`
--

LOCK TABLES `form13` WRITE;
/*!40000 ALTER TABLE `form13` DISABLE KEYS */;
/*!40000 ALTER TABLE `form13` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formxi`
--

DROP TABLE IF EXISTS `formxi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formxi` (
  `GroupNo` varchar(50) DEFAULT NULL,
  `InstNo` int(11) DEFAULT NULL,
  `RecieptNO` varchar(50) DEFAULT NULL,
  `Subcription` int(11) DEFAULT NULL,
  `DayBookNo` int(11) DEFAULT NULL,
  `Ticketno` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `ChitRegNo` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formxi`
--

LOCK TABLES `formxi` WRITE;
/*!40000 ALTER TABLE `formxi` DISABLE KEYS */;
/*!40000 ALTER TABLE `formxi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groupdetails`
--

DROP TABLE IF EXISTS `groupdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groupdetails` (
  `GroupID` bigint(20) NOT NULL AUTO_INCREMENT,
  `GroupNo` varchar(50) NOT NULL,
  `ChitRegNo` varchar(50) DEFAULT NULL,
  `chitregdate` date DEFAULT NULL,
  `ChitValue` varchar(50) DEFAULT NULL,
  `grpdate` date DEFAULT NULL,
  `Duration` int(11) DEFAULT NULL,
  `terdate` date DEFAULT NULL,
  `Day` varchar(50) DEFAULT NULL,
  `Month` varchar(50) DEFAULT NULL,
  `Year` varchar(50) DEFAULT NULL,
  `valinword` varchar(50) DEFAULT NULL,
  `FdNo` varchar(50) DEFAULT NULL,
  `FdDate` date DEFAULT NULL,
  `FdAmount` int(11) DEFAULT NULL,
  `Fdbankname` varchar(50) DEFAULT NULL,
  `sub_per` varchar(100) DEFAULT NULL,
  `main_branch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`GroupID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groupdetails`
--

LOCK TABLES `groupdetails` WRITE;
/*!40000 ALTER TABLE `groupdetails` DISABLE KEYS */;
INSERT INTO `groupdetails` VALUES (7,'AM1','213123','2010-05-10','100000','2010-05-10',4,'2010-09-10','10th','May','2010','','213213','2010-05-10',2000,'','monthly',1),(8,'AM2','213123','2010-05-10','100000','2010-05-10',4,'2010-09-10','10th','May','2010','','','0000-00-00',0,'','monthly',1),(9,'AM3','213123','2010-05-10','100000','2010-05-12',4,'2010-09-12','12th','May','2010','','','0000-00-00',0,'','monthly',1),(10,'AM4','213123','2010-05-10','100000','2010-05-12',4,'2010-09-12','12th','May','2010','','','0000-00-00',0,'','monthly',2),(11,'AM5','213123','0000-00-00','100000','2010-05-12',4,'2010-09-12','12th','May','2010','','213213','0000-00-00',0,'','monthly',1),(12,'AM6','chit1234','2010-05-27','100000','2010-05-28',4,'2010-09-28','28th','May','2010','','fd','2010-05-18',10000,'HDFC','monthly',1);
/*!40000 ALTER TABLE `groupdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groupforming`
--

DROP TABLE IF EXISTS `groupforming`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groupforming` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SubCode` varchar(50) DEFAULT NULL,
  `IntroducedBy` int(11) DEFAULT NULL,
  `ChitValue` int(11) DEFAULT NULL,
  `RefferedBy` varchar(50) DEFAULT NULL,
  `InstRecdAmt` int(11) DEFAULT NULL,
  `TRNo` varchar(50) DEFAULT NULL,
  `FormRecOn` date DEFAULT NULL,
  `Entrolled` tinyint(4) DEFAULT NULL,
  `GroupNoEnrolled` varchar(50) DEFAULT NULL,
  `TktNoEnrolled` varchar(50) DEFAULT NULL,
  `main_branch_id` int(11) NOT NULL,
  `new_member` varchar(10) NOT NULL,
  `Discount` int(11) DEFAULT NULL,
  `Transaction_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`),
  KEY `SubCode` (`SubCode`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groupforming`
--

LOCK TABLES `groupforming` WRITE;
/*!40000 ALTER TABLE `groupforming` DISABLE KEYS */;
INSERT INTO `groupforming` VALUES (13,'1211',1,100000,'',2500,'Tr01','2010-05-10',1,'AM1','4',1,'TRF',0,0),(14,'121',1,100000,'',2500,'Tr01','2010-05-10',1,'AM1','2',1,'TRF',0,0),(16,'A/1/2',1,100000,'',2500,'Tr01','2010-05-10',1,'AM1','3',1,'TRF',0,0),(17,'1211',1,100000,'',2500,'Tr01','2010-05-10',1,'AM1','4',1,'NEW',0,0);
/*!40000 ALTER TABLE `groupforming` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `introducer`
--

DROP TABLE IF EXISTS `introducer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `introducer` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Address` varchar(50) DEFAULT NULL,
  `lang` varchar(100) DEFAULT NULL,
  `Main_ID` int(11) DEFAULT NULL,
  `Introducer_Type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `introducer`
--

LOCK TABLES `introducer` WRITE;
/*!40000 ALTER TABLE `introducer` DISABLE KEYS */;
INSERT INTO `introducer` VALUES (1,'Introducer One','testi','english',1,'3'),(2,'Introducer Two','address','english',2,'2'),(3,'एक Introducer','0','hindi',1,'8'),(4,'दो Introducer','0','hindi',2,'8');
/*!40000 ALTER TABLE `introducer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mastercode`
--

DROP TABLE IF EXISTS `mastercode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mastercode` (
  `Code` bigint(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Adderss` varchar(50) DEFAULT NULL,
  `lang` varchar(100) DEFAULT NULL,
  `Main_Code` bigint(20) DEFAULT NULL,
  KEY `Code` (`Code`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mastercode`
--

LOCK TABLES `mastercode` WRITE;
/*!40000 ALTER TABLE `mastercode` DISABLE KEYS */;
INSERT INTO `mastercode` VALUES (1,'MAS2002010','ads','english',1),(2,'MAS10032009','kk1','english',2),(3,'MAS1002009','Siradi','english',3),(5,'0','0','hindi',1),(6,'0','0','hindi',2),(7,'0','0','hindi',3);
/*!40000 ALTER TABLE `mastercode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noofcoll`
--

DROP TABLE IF EXISTS `noofcoll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noofcoll` (
  `GroupNo` varchar(50) DEFAULT NULL,
  `a` int(11) DEFAULT NULL,
  `b` int(11) DEFAULT NULL,
  `c` int(11) DEFAULT NULL,
  `d` int(11) DEFAULT NULL,
  `e` int(11) DEFAULT NULL,
  `f` int(11) DEFAULT NULL,
  `g` int(11) DEFAULT NULL,
  `h` int(11) DEFAULT NULL,
  `i` int(11) DEFAULT NULL,
  `j` int(11) DEFAULT NULL,
  `k` int(11) DEFAULT NULL,
  `l` int(11) DEFAULT NULL,
  `m` int(11) DEFAULT NULL,
  `n` int(11) DEFAULT NULL,
  `o` int(11) DEFAULT NULL,
  `p` int(11) DEFAULT NULL,
  `q` int(11) DEFAULT NULL,
  `r` int(11) DEFAULT NULL,
  `s` int(11) DEFAULT NULL,
  `t` int(11) DEFAULT NULL,
  `No` int(11) DEFAULT NULL,
  `Total` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noofcoll`
--

LOCK TABLES `noofcoll` WRITE;
/*!40000 ALTER TABLE `noofcoll` DISABLE KEYS */;
/*!40000 ALTER TABLE `noofcoll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noofshares`
--

DROP TABLE IF EXISTS `noofshares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noofshares` (
  `Subcode` varchar(50) DEFAULT NULL,
  `From` int(11) DEFAULT NULL,
  `To` int(11) DEFAULT NULL,
  `Total` int(11) DEFAULT NULL,
  KEY `Subcode` (`Subcode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noofshares`
--

LOCK TABLES `noofshares` WRITE;
/*!40000 ALTER TABLE `noofshares` DISABLE KEYS */;
/*!40000 ALTER TABLE `noofshares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outstanding_statement`
--

DROP TABLE IF EXISTS `outstanding_statement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outstanding_statement` (
  `GroupNo` varchar(50) DEFAULT NULL,
  `TicketNo` int(11) DEFAULT NULL,
  `Subcode` varchar(50) DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Due` int(11) DEFAULT NULL,
  `Received` int(11) DEFAULT NULL,
  `Balance` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outstanding_statement`
--

LOCK TABLES `outstanding_statement` WRITE;
/*!40000 ALTER TABLE `outstanding_statement` DISABLE KEYS */;
/*!40000 ALTER TABLE `outstanding_statement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_type_lk`
--

DROP TABLE IF EXISTS `payment_type_lk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment_type_lk` (
  `payment_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `payment_type` text NOT NULL,
  `cost` int(11) NOT NULL,
  `lang` varchar(100) NOT NULL,
  `main_payment_type_id` int(11) NOT NULL,
  PRIMARY KEY (`payment_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_type_lk`
--

LOCK TABLES `payment_type_lk` WRITE;
/*!40000 ALTER TABLE `payment_type_lk` DISABLE KEYS */;
INSERT INTO `payment_type_lk` VALUES (5,'Payment Due',0,'english',5),(6,'Εξόφληση Τιμολογίου - Απόδειξης',0,'greek',5),(7,'Advance Payment',0,'english',7),(8,'Προκαταβολή',0,'greek',7);
/*!40000 ALTER TABLE `payment_type_lk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paymentchecklist`
--

DROP TABLE IF EXISTS `paymentchecklist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentchecklist` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupNo` varchar(50) DEFAULT NULL,
  `TicketNo` int(11) DEFAULT NULL,
  `SignVerification` tinyint(4) DEFAULT NULL,
  `Documentdetails` tinyint(4) DEFAULT NULL,
  `Photo` tinyint(4) DEFAULT NULL,
  `Pdc1No` varchar(50) DEFAULT NULL,
  `Pdc1BankName` varchar(50) DEFAULT NULL,
  `Pdc2No` varchar(50) DEFAULT NULL,
  `Pdc2BankName` varchar(50) DEFAULT NULL,
  `Pdc3No` varchar(50) DEFAULT NULL,
  `Pdc3BankName` varchar(50) DEFAULT NULL,
  `Pdc4No` varchar(50) DEFAULT NULL,
  `Pdc4BankName` varchar(50) DEFAULT NULL,
  `SignInAllDoc` tinyint(4) DEFAULT NULL,
  `AddProofSub` tinyint(4) DEFAULT NULL,
  `AddProofG1` tinyint(4) DEFAULT NULL,
  `AddProofG2` tinyint(4) DEFAULT NULL,
  `IncomeProofSub` tinyint(4) DEFAULT NULL,
  `IncomeProofG1` tinyint(4) DEFAULT NULL,
  `IncomeProofG2` tinyint(4) DEFAULT NULL,
  `SignatureVerificationSub` tinyint(4) DEFAULT NULL,
  `SignatureVerificationG1` tinyint(4) DEFAULT NULL,
  `SignatureVerificationG2` tinyint(4) DEFAULT NULL,
  `GuaruntorApplnG1` tinyint(4) DEFAULT NULL,
  `GuaruntorApplnG2` tinyint(4) DEFAULT NULL,
  `ShopactLicenseG1` tinyint(4) DEFAULT NULL,
  `ShopactLicenseG2` tinyint(4) DEFAULT NULL,
  `MemorandomOfArticles` tinyint(4) DEFAULT NULL,
  `PartnerShipDead` tinyint(4) DEFAULT NULL,
  `DocumentSendOn` datetime DEFAULT NULL,
  `DocumentReadOn` datetime DEFAULT NULL,
  `DetailsOfDocument` longtext,
  `CommByMis` longtext,
  `CommByMarkExecutive` longtext,
  `CommByVerifictionOfficer` longtext,
  `CommentByAccDepart` longtext,
  `CommentByGM` longtext,
  `CommByDir1` longtext,
  `CommByDir2` longtext,
  `CommByDir3` longtext,
  `CommByMD` longtext,
  `Status` varchar(50) DEFAULT NULL,
  `loginLevel` varchar(50) DEFAULT NULL,
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paymentchecklist`
--

LOCK TABLES `paymentchecklist` WRITE;
/*!40000 ALTER TABLE `paymentchecklist` DISABLE KEYS */;
/*!40000 ALTER TABLE `paymentchecklist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `refffered`
--

DROP TABLE IF EXISTS `refffered`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refffered` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) DEFAULT NULL,
  `Address` varchar(50) DEFAULT NULL,
  `lang` varchar(100) NOT NULL,
  `Main_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refffered`
--

LOCK TABLES `refffered` WRITE;
/*!40000 ALTER TABLE `refffered` DISABLE KEYS */;
INSERT INTO `refffered` VALUES (1,'nitin borate','panchashil building moshi','english',1),(2,'नितिन borate','0','hindi',1),(3,'i am going home','panchashil building pune','english',3),(4,'मेरे घर में आपका स्वागत है','0','hindi',3),(5,'Yashvant Kumar','','english',5),(6,'Yashvant कुमार','0','hindi',5);
/*!40000 ALTER TABLE `refffered` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `returnedcheques`
--

DROP TABLE IF EXISTS `returnedcheques`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `returnedcheques` (
  `RecieptNo` varchar(50) DEFAULT NULL,
  `ChequeNo` varchar(50) DEFAULT NULL,
  `Bankname` varchar(50) DEFAULT NULL,
  `ChequeDate` datetime DEFAULT NULL,
  `Acno` varchar(50) DEFAULT NULL,
  `ChequeNo1` varchar(50) DEFAULT NULL,
  `Bankname1` varchar(50) DEFAULT NULL,
  `ChequeDate1` datetime DEFAULT NULL,
  `Acno1` varchar(50) DEFAULT NULL,
  `ChequeNo2` varchar(50) DEFAULT NULL,
  `Bankname2` varchar(50) DEFAULT NULL,
  `ChequeDate2` datetime DEFAULT NULL,
  `Acno2` varchar(50) DEFAULT NULL,
  `ChequeNo3` varchar(50) DEFAULT NULL,
  `Bankname3` varchar(50) DEFAULT NULL,
  `ChequeDate3` datetime DEFAULT NULL,
  `Acno3` varchar(50) DEFAULT NULL,
  `PaymentMade` tinyint(4) DEFAULT NULL,
  `ReturnedDate` datetime DEFAULT NULL,
  `Status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `returnedcheques`
--

LOCK TABLES `returnedcheques` WRITE;
/*!40000 ALTER TABLE `returnedcheques` DISABLE KEYS */;
/*!40000 ALTER TABLE `returnedcheques` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subdirdetails`
--

DROP TABLE IF EXISTS `subdirdetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subdirdetails` (
  `SubID` bigint(20) NOT NULL AUTO_INCREMENT,
  `SubCode` varchar(50) NOT NULL,
  `FullName` varchar(50) DEFAULT NULL,
  `SonOf` varchar(50) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `AddrsCorres` varchar(255) DEFAULT NULL,
  `TelNoCorres` varchar(50) DEFAULT NULL,
  `MobNoCorres` varchar(50) DEFAULT NULL,
  `PANNoCorres` varchar(50) DEFAULT NULL,
  `OfficeAddrs` varchar(255) DEFAULT NULL,
  `Designation` varchar(50) DEFAULT NULL,
  `Department` varchar(50) DEFAULT NULL,
  `personelno` varchar(50) DEFAULT NULL,
  `TelNoOff` varchar(50) DEFAULT NULL,
  `MobNoOff` varchar(50) DEFAULT NULL,
  `PANNoOff` varchar(50) DEFAULT NULL,
  `NativePlace` varchar(50) DEFAULT NULL,
  `AddrsNative` varchar(255) DEFAULT NULL,
  `TelNoNativeplc` varchar(50) DEFAULT NULL,
  `MobNoNativeplc` varchar(50) DEFAULT NULL,
  `PANNoNativeplc` varchar(50) DEFAULT NULL,
  `NomineeName` varchar(50) DEFAULT NULL,
  `AddrsNominee` varchar(255) DEFAULT NULL,
  `RelationNominee` varchar(50) DEFAULT NULL,
  `NomineeAge` int(11) DEFAULT NULL,
  `TelNoNominee` varchar(50) DEFAULT NULL,
  `DirectorShareholder` varchar(11) DEFAULT '0',
  `Identifyby` varchar(50) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `indfirm` varchar(50) DEFAULT NULL,
  `AddrsCorrone` varchar(255) DEFAULT NULL,
  `CoOrdCode` varchar(50) DEFAULT NULL,
  `CollCode` varchar(50) DEFAULT NULL,
  `MasterCode` varchar(50) DEFAULT NULL,
  `BranchCode` varchar(100) DEFAULT NULL,
  `AddedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`SubID`),
  KEY `CollCode` (`CollCode`),
  KEY `CoOrdCode` (`CoOrdCode`),
  KEY `Identifyby` (`Identifyby`),
  KEY `MasterCode` (`MasterCode`),
  KEY `SubCode` (`SubCode`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subdirdetails`
--

LOCK TABLES `subdirdetails` WRITE;
/*!40000 ALTER TABLE `subdirdetails` DISABLE KEYS */;
INSERT INTO `subdirdetails` VALUES (11,'1211','niteenborate','eknath','2010-04-06','n','','1212112121','','n','eng','','','','','','','n','','','','sachin','n','brother',121,'','0','',0,'Individual','','1','','1','2',1),(12,'121','rjendra hajare','1','2010-05-12','pune','1','9960758220','','pune','121','','','','','','','pune','','','','','pune','son',121,'','0','',0,'Individual','','1','1','2','1',1),(13,'','sachin borate','eknath','2010-05-14','nnnnn','','9960855511','','nnnnn','eng','','','','','','','nnnnn','','','','sachin','nnnnn','brother',121,'','0','',0,'Individual','','1','1','1','1',1),(14,'1211','Sachin Kasid','121','2011-01-01','pak','','9960655511','','pak','11221','','','','','','','pak','','','','','pak','brother',21,'','0','',-1,'Individual','','1','1','2','',1),(15,'','Kumar Wagmode','11','2010-04-08','ppp','','9960655511','','ppp','kk','','','','','','','ppp','','','','Nominee','ppp','brother',121,'','0','',0,'Individual','','1','1','2','',1),(16,'A/1/2','YashKumar','SOM','2010-05-14','Sitai Krishna Nagar ','123333','2133333333','12333','Sitai Krishna Nagar ','abc','abd','212','123','123','','abc','Sitai Krishna Nagar ','123','123','','','Sitai Krishna Nagar ','123',23,'123','0','',0,'Individual','','1','1','1','1',1);
/*!40000 ALTER TABLE `subdirdetails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscription_details`
--

DROP TABLE IF EXISTS `subscription_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscription_details` (
  `Subscription_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Auction_ID` bigint(20) DEFAULT NULL,
  `TicketNo` int(11) NOT NULL,
  `Transaction_ID` bigint(20) DEFAULT NULL,
  `SubID` varchar(100) DEFAULT NULL,
  `LateFee` int(11) DEFAULT NULL,
  `lasdevidentt` int(11) DEFAULT NULL,
  `arrears` int(11) DEFAULT NULL,
  `main_branch_id` int(11) DEFAULT NULL,
  `Subcription` double NOT NULL,
  `GroupNo` varchar(7) DEFAULT NULL,
  `InstNo` int(11) DEFAULT NULL,
  `ReceiptNo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`Subscription_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscription_details`
--

LOCK TABLES `subscription_details` WRITE;
/*!40000 ALTER TABLE `subscription_details` DISABLE KEYS */;
INSERT INTO `subscription_details` VALUES (6,261,2,4,'121',0,0,278,1,2222,NULL,NULL,NULL);
/*!40000 ALTER TABLE `subscription_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_adr_area`
--

DROP TABLE IF EXISTS `tbl_adr_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_adr_area` (
  `area_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `area_name` varchar(50) DEFAULT NULL,
  `note` longtext,
  `country` text NOT NULL,
  `lang` varchar(100) NOT NULL,
  `main_area_id` int(11) NOT NULL,
  PRIMARY KEY (`area_id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_adr_area`
--

LOCK TABLES `tbl_adr_area` WRITE;
/*!40000 ALTER TABLE `tbl_adr_area` DISABLE KEYS */;
INSERT INTO `tbl_adr_area` VALUES (2,'Αττική','','gr','greek',2),(40,'Aegean Islands','','gr','english',10),(4,'Ήπειρος','','gr','greek',4),(5,'Θεσσαλία','','gr','greek',5),(6,'Θράκη','','gr','greek',6),(7,'Κρήτη','','gr','greek',7),(41,'Macedonia','','gr','english',9),(9,'Μακεδονία','','gr','greek',9),(10,'Νησιά Αιγαίου','','gr','greek',10),(39,'Ionian Islands','','gr','english',12),(12,'Νησιά Ιονίου','','gr','greek',12),(13,'Πελοπόννησος','','gr','greek',13),(34,'Κύπρος','','gr','greek',22),(15,'Στερεά Ελλάδα','','gr','greek',15),(35,'Cyprus','','gr','english',22),(32,'Θεσσαλονίκη','','gr','greek',21),(36,'Thessaloniki','','gr','english',21),(37,'Central Greece','','gr','english',15),(38,'Peloponnesse','','gr','english',13),(42,'Thrace','','gr','english',6),(43,'Crete','','gr','english',7),(44,'Thessaly','','gr','english',5),(45,'Ipiros','','gr','english',4),(46,'Attica','','gr','english',2),(47,'gggggggg','','gr','english',23),(48,'north','','in','english',24);
/*!40000 ALTER TABLE `tbl_adr_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_adr_city`
--

DROP TABLE IF EXISTS `tbl_adr_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_adr_city` (
  `city_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `city_name` varchar(100) DEFAULT NULL,
  `area_id` int(11) NOT NULL DEFAULT '0',
  `perfecture_id` int(11) NOT NULL DEFAULT '0',
  `note` longtext,
  `country` text NOT NULL,
  `lang` varchar(100) NOT NULL,
  `main_city_id` int(11) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_adr_city`
--

LOCK TABLES `tbl_adr_city` WRITE;
/*!40000 ALTER TABLE `tbl_adr_city` DISABLE KEYS */;
INSERT INTO `tbl_adr_city` VALUES (1,'luckhnow',24,104,'','in','english',1);
/*!40000 ALTER TABLE `tbl_adr_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_adr_country`
--

DROP TABLE IF EXISTS `tbl_adr_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_adr_country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `iso` varchar(2) NOT NULL DEFAULT '',
  `name` varchar(80) NOT NULL DEFAULT '',
  `lang` varchar(100) NOT NULL,
  `main_country_id` int(11) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=487 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_adr_country`
--

LOCK TABLES `tbl_adr_country` WRITE;
/*!40000 ALTER TABLE `tbl_adr_country` DISABLE KEYS */;
INSERT INTO `tbl_adr_country` VALUES (1,'af','afghanistan','english',1),(2,'al','albania','english',2),(3,'dz','algeria','english',3),(4,'as','american samoa','english',4),(5,'ad','andorra','english',5),(6,'ao','angola','english',6),(7,'ai','anguilla','english',7),(8,'aq','antarctica','english',8),(9,'ag','antigua and barbuda','english',9),(10,'ar','argentina','english',10),(11,'am','armenia','english',11),(12,'aw','aruba','english',12),(13,'au','australia','english',13),(14,'at','austria','english',14),(15,'az','azerbaijan','english',15),(16,'bs','bahamas','english',16),(17,'bh','bahrain','english',17),(18,'bd','bangladesh','english',18),(19,'bb','barbados','english',19),(20,'by','belarus','english',20),(21,'be','belgium','english',21),(22,'bz','belize','english',22),(23,'bj','benin','english',23),(24,'bm','bermuda','english',24),(25,'bt','bhutan','english',25),(26,'bo','bolivia','english',26),(27,'ba','bosnia and herzegovina','english',27),(28,'bw','botswana','english',28),(29,'bv','bouvet island','english',29),(30,'br','brazil','english',30),(31,'io','british indian ocean territory','english',31),(32,'bn','brunei darussalam','english',32),(33,'bg','bulgaria','english',33),(34,'bf','burkina faso','english',34),(35,'bi','burundi','english',35),(36,'kh','cambodia','english',36),(37,'cm','cameroon','english',37),(38,'ca','canada','english',38),(39,'cv','cape verde','english',39),(40,'ky','cayman islands','english',40),(41,'cf','central african republic','english',41),(42,'td','chad','english',42),(43,'cl','chile','english',43),(44,'cn','china','english',44),(45,'cx','christmas island','english',45),(46,'cc','cocos (keeling) islands','english',46),(47,'co','colombia','english',47),(48,'km','comoros','english',48),(49,'cg','congo','english',49),(50,'cd','congo, the democratic republic of the','english',50),(51,'ck','cook islands','english',51),(52,'cr','costa rica','english',52),(53,'ci','cote d\'ivoire','english',53),(54,'hr','croatia','english',54),(55,'cu','cuba','english',55),(56,'cy','cyprus','english',56),(57,'cz','czech republic','english',57),(58,'dk','denmark','english',58),(59,'dj','djibouti','english',59),(60,'dm','dominica','english',60),(61,'do','dominican republic','english',61),(62,'ec','ecuador','english',62),(63,'eg','egypt','english',63),(64,'sv','el salvador','english',64),(65,'gq','equatorial guinea','english',65),(66,'er','eritrea','english',66),(67,'ee','estonia','english',67),(68,'et','ethiopia','english',68),(69,'fk','falkland islands (malvinas)','english',69),(70,'fo','faroe islands','english',70),(71,'fj','fiji','english',71),(72,'fi','finland','english',72),(73,'fr','france','english',73),(74,'gf','french guiana','english',74),(75,'pf','french polynesia','english',75),(76,'tf','french southern territories','english',76),(77,'ga','gabon','english',77),(78,'gm','gambia','english',78),(79,'ge','georgia','english',79),(80,'de','germany','english',80),(81,'gh','ghana','english',81),(82,'gi','gibraltar','english',82),(83,'gr','greece','english',83),(84,'gl','greenland','english',84),(85,'gd','grenada','english',85),(86,'gp','guadeloupe','english',86),(87,'gu','guam','english',87),(88,'gt','guatemala','english',88),(89,'gn','guinea','english',89),(90,'gw','guinea-bissau','english',90),(91,'gy','guyana','english',91),(92,'ht','haiti','english',92),(93,'hm','heard island and mcdonald islands','english',93),(94,'va','holy see (vatican city state)','english',94),(95,'hn','honduras','english',95),(96,'hk','hong kong','english',96),(97,'hu','hungary','english',97),(98,'is','iceland','english',98),(99,'in','india','english',99),(100,'id','indonesia','english',100),(101,'ir','iran, islamic republic of','english',101),(102,'iq','iraq','english',102),(103,'ie','ireland','english',103),(104,'il','israel','english',104),(105,'it','italy','english',105),(106,'jm','jamaica','english',106),(107,'jp','japan','english',107),(108,'jo','jordan','english',108),(109,'kz','kazakhstan','english',109),(110,'ke','kenya','english',110),(111,'ki','kiribati','english',111),(112,'kp','korea, democratic people\'s republic of','english',112),(113,'kr','korea, republic of','english',113),(114,'kw','kuwait','english',114),(115,'kg','kyrgyzstan','english',115),(116,'la','lao people\'s democratic republic','english',116),(117,'lv','latvia','english',117),(118,'lb','lebanon','english',118),(119,'ls','lesotho','english',119),(120,'lr','liberia','english',120),(121,'ly','libyan arab jamahiriya','english',121),(122,'li','liechtenstein','english',122),(123,'lt','lithuania','english',123),(124,'lu','luxembourg','english',124),(125,'mo','macao','english',125),(126,'mk','macedonia, the former yugoslav republic of','english',126),(127,'mg','madagascar','english',127),(128,'mw','malawi','english',128),(129,'my','malaysia','english',129),(130,'mv','maldives','english',130),(131,'ml','mali','english',131),(132,'mt','malta','english',132),(133,'mh','marshall islands','english',133),(134,'mq','martinique','english',134),(135,'mr','mauritania','english',135),(136,'mu','mauritius','english',136),(137,'yt','mayotte','english',137),(138,'mx','mexico','english',138),(139,'fm','micronesia, federated states of','english',139),(140,'md','moldova, republic of','english',140),(141,'mc','monaco','english',141),(142,'mn','mongolia','english',142),(143,'ms','montserrat','english',143),(144,'ma','morocco','english',144),(145,'mz','mozambique','english',145),(146,'mm','myanmar','english',146),(147,'na','namibia','english',147),(148,'nr','nauru','english',148),(149,'np','nepal','english',149),(150,'nl','netherlands','english',150),(151,'an','netherlands antilles','english',151),(152,'nc','new caledonia','english',152),(153,'nz','new zealand','english',153),(154,'ni','nicaragua','english',154),(155,'ne','niger','english',155),(156,'ng','nigeria','english',156),(157,'nu','niue','english',157),(158,'nf','norfolk island','english',158),(159,'mp','northern mariana islands','english',159),(160,'no','norway','english',160),(161,'om','oman','english',161),(162,'pk','pakistan','english',162),(163,'pw','palau','english',163),(164,'ps','palestinian territory, occupied','english',164),(165,'pa','panama','english',165),(166,'pg','papua new guinea','english',166),(167,'py','paraguay','english',167),(168,'pe','peru','english',168),(169,'ph','philippines','english',169),(170,'pn','pitcairn','english',170),(171,'pl','poland','english',171),(172,'pt','portugal','english',172),(173,'pr','puerto rico','english',173),(174,'qa','qatar','english',174),(175,'re','reunion','english',175),(176,'ro','romania','english',176),(177,'ru','russian federation','english',177),(178,'rw','rwanda','english',178),(179,'sh','saint helena','english',179),(180,'kn','saint kitts and nevis','english',180),(181,'lc','saint lucia','english',181),(182,'pm','saint pierre and miquelon','english',182),(183,'vc','saint vincent and the grenadines','english',183),(184,'ws','samoa','english',184),(185,'sm','san marino','english',185),(186,'st','sao tome and principe','english',186),(187,'sa','saudi arabia','english',187),(188,'sn','senegal','english',188),(189,'cs','serbia and montenegro','english',189),(190,'sc','seychelles','english',190),(191,'sl','sierra leone','english',191),(192,'sg','singapore','english',192),(193,'sk','slovakia','english',193),(194,'si','slovenia','english',194),(195,'sb','solomon islands','english',195),(196,'so','somalia','english',196),(197,'za','south africa','english',197),(198,'gs','south georgia and the south sandwich islands','english',198),(199,'es','spain','english',199),(200,'lk','sri lanka','english',200),(201,'sd','sudan','english',201),(202,'sr','suriname','english',202),(203,'sj','svalbard and jan mayen','english',203),(204,'sz','swaziland','english',204),(205,'se','sweden','english',205),(206,'ch','switzerland','english',206),(207,'sy','syrian arab republic','english',207),(208,'tw','taiwan, province of china','english',208),(209,'tj','tajikistan','english',209),(210,'tz','tanzania, united republic of','english',210),(211,'th','thailand','english',211),(212,'tl','timor-leste','english',212),(213,'tg','togo','english',213),(214,'tk','tokelau','english',214),(215,'to','tonga','english',215),(216,'tt','trinidad and tobago','english',216),(217,'tn','tunisia','english',217),(218,'tr','turkey','english',218),(219,'tm','turkmenistan','english',219),(220,'tc','turks and caicos islands','english',220),(221,'tv','tuvalu','english',221),(222,'ug','uganda','english',222),(223,'ua','ukraine','english',223),(224,'ae','united arab emirates','english',224),(225,'gb','united kingdom','english',225),(226,'us','united states','english',226),(227,'um','united states minor outlying islands','english',227),(228,'uy','uruguay','english',228),(229,'uz','uzbekistan','english',229),(230,'vu','vanuatu','english',230),(231,'ve','venezuela','english',231),(232,'vn','viet nam','english',232),(233,'vg','virgin islands, british','english',233),(234,'vi','virgin islands, u.s.','english',234),(235,'wf','wallis and futuna','english',235),(236,'eh','western sahara','english',236),(237,'ye','yemen','english',237),(238,'zm','zambia','english',238),(239,'zw','zimbabwe','english',239),(240,'af','Αφγανιστάν','greek',1),(241,'al','albania','greek',2),(242,'dz','Αλγερία','greek',3),(243,'as','ΑμερικανικήΣαμόα','greek',4),(244,'ad','Ανδόρα','greek',5),(245,'ao','Αγκόλα','greek',6),(246,'ai','Anguilla','greek',7),(247,'aq','Ανταρκτική','greek',8),(248,'ag','Αντίγκουα και Μπαρμπούντα','greek',9),(249,'ar','argentina','greek',10),(250,'am','Αρμενία','greek',11),(251,'aw','Αρούμπα','greek',12),(252,'au','Αυστραλία','greek',13),(253,'at','Αυστρία','greek',14),(254,'az','Αζερμπαϊτζάν','greek',15),(255,'bs','Μπαχάμες','greek',16),(256,'bh','Μπαχρέιν','greek',17),(257,'bd','Μπανγκλαντές','greek',18),(258,'bb','Μπαρμπάντος','greek',19),(259,'by','Λευκορωσία','greek',20),(260,'be','Βέλγιο','greek',21),(261,'bz','Μπελίζ','greek',22),(262,'bj','Μπενίν','greek',23),(263,'bm','Βερμούδες','greek',24),(264,'bt','Μπουτάν','greek',25),(265,'bo','Βολιβία','greek',26),(266,'ba','Βοσνία Ερζεγοβίνη','greek',27),(267,'bw','Μποτσουάνα','greek',28),(268,'bv','Μπουβέ','greek',29),(269,'br','Βραζιλία','greek',30),(270,'io','British Indian έδαφος ωκεανό','greek',31),(271,'bn','Μπρουνέι','greek',32),(272,'bg','Βουλγαρία','greek',33),(273,'bf','Μπουρκίνα','greek',34),(274,'bi','Μπουρούντι','greek',35),(275,'kh','Καμπότζη','greek',36),(276,'cm','Καμερούν','greek',37),(277,'ca','Καναδάς','greek',38),(278,'cv','Πράσινο Ακρωτήριο','greek',39),(279,'ky','Νησιά Καϋμάν','greek',40),(280,'cf','Κεντροαφρικανική Δημοκρατία','greek',41),(281,'td','Τσαντ','greek',42),(282,'cl','Χιλή','greek',43),(283,'cn','πιατικά','greek',44),(284,'cx','Christmas Island','greek',45),(285,'cc','Κόκος (Κίλινγκ)','greek',46),(286,'co','Κολομβία','greek',47),(287,'km','Κομόρες','greek',48),(288,'cg','Κονγκό','greek',49),(289,'cd','Κονγκό, τη Λαϊκή Δημοκρατία του','greek',50),(290,'ck','νησιά μάγειρας','greek',51),(291,'cr','Κόστα Ρίκα','greek',52),(293,'hr','Κροατία','greek',54),(294,'cu','Κούβα','greek',55),(295,'cy','Κύπρος','greek',56),(296,'cz','Τσεχία','greek',57),(297,'dk','Δανία','greek',58),(298,'dj','Τζιμπουτί','greek',59),(299,'dm','Ντομίνικα','greek',60),(300,'do','Δομινικανή Δημοκρατία','greek',61),(301,'ec','Ισημερινός','greek',62),(302,'eg','Αίγυπτος','greek',63),(303,'sv','Σαλβαδόρ','greek',64),(304,'gq','Ισημερινή Γουινέα','greek',65),(305,'er','Ερυθραία','greek',66),(306,'ee','estonia','greek',67),(307,'et','Αιθιοπία','greek',68),(308,'fk','Νήσοι Φώκλαντ (Μαλβίνας)','greek',69),(309,'fo','Νήσοι Φερόε','greek',70),(310,'fj','Φίτζι','greek',71),(311,'fi','Φινλανδία','greek',72),(312,'fr','Γαλλία','greek',73),(313,'gf','Γαλλική Γουιάνα','greek',74),(314,'pf','Γαλλική Πολυνησία','greek',75),(315,'tf','Γαλλικά νότια εδάφη','greek',76),(316,'ga','Γκαμπόν','greek',77),(317,'gm','Γκάμπια','greek',78),(318,'ge','Γεωργία','greek',79),(319,'de','Γερμανία','greek',80),(320,'gh','Γκάνα','greek',81),(321,'gi','Γιβραλτάρ','greek',82),(322,'gr','Ελλάδα','greek',83),(323,'gl','Γροιλανδία','greek',84),(324,'gd','Γρενάδα','greek',85),(325,'gp','Γουαδελούπη','greek',86),(326,'gu','Γκουάμ','greek',87),(327,'gt','Γουατεμάλα','greek',88),(328,'gn','χοιρίδιο','greek',89),(329,'gw','Γουινέα-Μπισάου','greek',90),(330,'gy','Γουιάνα','greek',91),(331,'ht','Αϊτή','greek',92),(332,'hm','ακούσει νησιά νησί και McDonald','greek',93),(333,'va','Αγία Έδρα (Κράτος του Βατικανού πόλη)','greek',94),(334,'hn','Ονδούρα','greek',95),(335,'hk','Χονγκ Κονγκ','greek',96),(336,'hu','Ουγγαρία','greek',97),(337,'is','Ισλανδία','greek',98),(338,'in','Ινδία','greek',99),(339,'id','Ινδονησία','greek',100),(340,'ir','Ιράν, Ισλαμική Δημοκρατία του','greek',101),(341,'iq','iraq','greek',102),(342,'ie','Ιρλανδία','greek',103),(343,'il','israel','greek',104),(344,'it','Ιταλία','greek',105),(345,'jm','Τζαμάικα','greek',106),(346,'jp','Ιαπωνία','greek',107),(347,'jo','jordan','greek',108),(348,'kz','Καζακστάν','greek',109),(349,'ke','Κένυα','greek',110),(350,'ki','Κιριμπάτι','greek',111),(352,'kr','Κορέα, Δημοκρατία της','greek',113),(353,'kw','Κουβέιτ','greek',114),(354,'kg','Κιργιζία','greek',115),(356,'lv','Λετονία','greek',117),(357,'lb','Λίβανος','greek',118),(358,'ls','Λεσόθο','greek',119),(359,'lr','Λιβερία','greek',120),(360,'ly','ΛιβύηΑραβικήΤζαμαχιρία','greek',121),(361,'li','Λιχτενστάιν','greek',122),(362,'lt','Λιθουανία','greek',123),(363,'lu','Λουξεμβούργο','greek',124),(364,'mo','Μακάο','greek',125),(365,'mk','Μακεδονίας, η πρώην Γιουγκοσλαβική Δημοκρατία της','greek',126),(366,'mg','Μαδαγασκάρη','greek',127),(367,'mw','Μαλάουι','greek',128),(368,'my','Μαλαισία','greek',129),(369,'mv','Μαλδίβες','greek',130),(370,'ml','Μαλί','greek',131),(371,'mt','malta','greek',132),(372,'mh','Νησιά Μάρσαλ','greek',133),(373,'mq','Μαρτινίκα','greek',134),(374,'mr','Μαυριτανία','greek',135),(375,'mu','Μαυρίκιος','greek',136),(376,'yt','Μαγιότ','greek',137),(377,'mx','mexico','greek',138),(378,'fm','Μικρονησία, Ομόσπονδες Πολιτείες της','greek',139),(379,'md','Μολδαβία, Δημοκρατία της','greek',140),(380,'mc','Μονακό','greek',141),(381,'mn','Μογγολία','greek',142),(382,'ms','Montserrat','greek',143),(383,'ma','Μαρόκο','greek',144),(384,'mz','Μοζαμβίκη','greek',145),(385,'mm','Μιανμάρ','greek',146),(386,'na','Ναμίμπια','greek',147),(387,'nr','Ναουρού','greek',148),(388,'np','Νεπάλ','greek',149),(389,'nl','Ολλανδία','greek',150),(390,'an','Ολλανδικές Αντίλλες','greek',151),(391,'nc','Νέα Καληδονία','greek',152),(392,'nz','Νέα Ζηλανδία','greek',153),(393,'ni','Νικαράγουα','greek',154),(394,'ne','niger','greek',155),(395,'ng','Νιγηρία','greek',156),(396,'nu','Νιούε','greek',157),(397,'nf','νησί Νόρφολκ','greek',158),(398,'mp','βόρεια νησιά Mariana','greek',159),(399,'no','norway','greek',160),(400,'om','Ομάν','greek',161),(401,'pk','Πακιστάν','greek',162),(402,'pw','Παλάου','greek',163),(403,'ps','παλαιστινιακά εδάφη, που καταλαμβάνει','greek',164),(404,'pa','Παναμάς','greek',165),(405,'pg','Παπούα Νέα Γουινέα','greek',166),(406,'py','Παραγουάη','greek',167),(407,'pe','Περού','greek',168),(408,'ph','Φιλιππίνες','greek',169),(409,'pn','Πίτκερν','greek',170),(410,'pl','Πολωνία','greek',171),(411,'pt','portugal','greek',172),(412,'pr','Πουέρτο Ρίκο','greek',173),(413,'qa','Κατάρ','greek',174),(414,'re','επανένωση','greek',175),(415,'ro','Ρουμανία','greek',176),(416,'ru','Ρωσία','greek',177),(417,'rw','Ρουάντα','greek',178),(418,'sh','Αγία Ελένη','greek',179),(419,'kn','Σαιντ Κιτς και Νέβις','greek',180),(420,'lc','Αγία Λουκία','greek',181),(421,'pm','Σαιν Πιερ και Μικελόν','greek',182),(422,'vc','Άγιος Βικέντιος και Γρεναδίνες','greek',183),(423,'ws','Σαμόα','greek',184),(424,'sm','San Marino','greek',185),(425,'st','Σάο Τομέ και Πρίνσιπε','greek',186),(426,'sa','Σαουδική Αραβία','greek',187),(427,'sn','Σενεγάλη','greek',188),(428,'cs','Σερβία και Μαυροβούνιο','greek',189),(429,'sc','Σεϋχέλλες','greek',190),(430,'sl','Σιέρα Λεόνε','greek',191),(431,'sg','singapore','greek',192),(432,'sk','Σλοβακία','greek',193),(433,'si','Σλοβενία','greek',194),(434,'sb','Νησιά Σολομώντα','greek',195),(435,'so','Σομαλία','greek',196),(436,'za','Νότια Αφρική','greek',197),(437,'gs','Νότια Γεωργία και τη νότια νησιά σάντουιτς','greek',198),(438,'es','Ισπανία','greek',199),(439,'lk','Σρι Λάνκα','greek',200),(440,'sd','Σουδάν','greek',201),(441,'sr','Σουρινάμ','greek',202),(442,'sj','Σβάλμπαρντ και Jan Mayen','greek',203),(443,'sz','Σουαζιλάνδη','greek',204),(444,'se','Σουηδία','greek',205),(445,'ch','Ελβετία','greek',206),(446,'sy','Αραβική Δημοκρατία της Συρίας','greek',207),(447,'tw','Ταϊβάν, Επαρχία της Κίνας','greek',208),(448,'tj','Τατζικιστάν','greek',209),(449,'tz','Τανζανία, Ενωμένη Δημοκρατία της','greek',210),(450,'th','thailand','greek',211),(451,'tl','Ανατολικό Τιμόρ','greek',212),(452,'tg','Τόγκο','greek',213),(453,'tk','Τοκελάου','greek',214),(454,'to','Τόνγκα','greek',215),(455,'tt','Τρινιντάντ και Τομπάγκο','greek',216),(456,'tn','Τυνησία','greek',217),(457,'tr','γαλοπούλα','greek',218),(458,'tm','Τουρκμενιστάν','greek',219),(459,'tc','Τερκς και Κάικος','greek',220),(460,'tv','Τουβαλού','greek',221),(461,'ug','Ουγκάντα','greek',222),(462,'ua','Ουκρανία','greek',223),(463,'ae','Ηνωμένα Αραβικά Εμιράτα','greek',224),(464,'gb','Ηνωμένο Βασίλειο','greek',225),(465,'us','ΗΠΑ','greek',226),(466,'um','μικρά εξωτερικά νησιά των ηνωμένων πολιτειών','greek',227),(467,'uy','Ουρουγουάη','greek',228),(468,'uz','Ουζμπεκιστάν','greek',229),(469,'vu','Βανουάτου','greek',230),(470,'ve','Βενεζουέλα','greek',231),(471,'vn','Βιετνάμ','greek',232),(472,'vg','Βρετανικοί Παρθένοι Νήσοι','greek',233),(473,'vi','Παρθένοι Νήσοι, μας','greek',234),(474,'wf','Wallis and Futuna','greek',235),(475,'eh','Δυτική Σαχάρα','greek',236),(476,'ye','Υεμένη','greek',237),(477,'zm','Ζάμπια','greek',238),(478,'zw','Ζιμπάμπουε','greek',239);
/*!40000 ALTER TABLE `tbl_adr_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_adr_perfecture`
--

DROP TABLE IF EXISTS `tbl_adr_perfecture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_adr_perfecture` (
  `perfecture_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `perfecture_name` varchar(100) DEFAULT NULL,
  `area_id` int(6) NOT NULL DEFAULT '0',
  `note` varchar(200) DEFAULT NULL,
  `country` varchar(100) NOT NULL DEFAULT '',
  `lang` varchar(100) NOT NULL,
  `main_perfecture_id` int(11) NOT NULL,
  PRIMARY KEY (`perfecture_id`)
) ENGINE=MyISAM AUTO_INCREMENT=112 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_adr_perfecture`
--

LOCK TABLES `tbl_adr_perfecture` WRITE;
/*!40000 ALTER TABLE `tbl_adr_perfecture` DISABLE KEYS */;
INSERT INTO `tbl_adr_perfecture` VALUES (2,'Αθήνα - Ανατολικά Προάστια',2,'','gr','greek',2),(3,'Πειραιάς',2,'','gr','greek',3),(4,'Υπόλοιπη Αττική',2,'','gr','greek',4),(46,'Κοζάνης',9,'','gr','greek',40),(45,'Φλωρίνης',9,'','gr','greek',39),(44,'Καστοριάς',9,'','gr','greek',38),(43,'Ημαθίας',9,'','gr','greek',37),(42,'Πέλλας',9,'','gr','greek',36),(41,'Κιλκίς',9,'','gr','greek',35),(40,'Χαλκιδικής',9,'','gr','greek',34),(39,'Σερρών',9,'','gr','greek',33),(38,'Δράμας',9,'','gr','greek',32),(37,'Καβάλας',9,'','gr','greek',31),(18,'Mumbai',16,'','in','english',17),(19,'Mumbai greek',16,'','in','greek',17),(20,'Benglore greek',17,'','in','greek',18),(21,'Benglore',17,'','in','english',18),(27,'Αθήνα - Βόρεια Προάστια',2,'','gr','greek',24),(24,'Αθήνα - Δυτικά Προάστια',2,'','gr','greek',21),(25,'Αθήνα - Νότια Προάστια',2,'','gr','greek',22),(26,'Αθήνα - Κέντρο',2,'','gr','greek',23),(32,'Ανατολικά Προάστια & Δήμοι',21,'','gr','greek',26),(31,'Ανατολική Θεσ/νίκη',21,'','gr','greek',25),(33,'Βόρεια Προάστια & Δήμοι',21,'','gr','greek',27),(34,'Δυτική Θεσ/νίκη',21,'','gr','greek',28),(35,'Δυτικά Προάστια & Δήμοι',21,'','gr','greek',29),(36,'Κεντρική Θεσ/νίκη',21,'','gr','greek',30),(47,'Γρεβενών',9,'','gr','greek',41),(48,'Πιερίας',9,'','gr','greek',42),(49,'Έβρου',6,'','gr','greek',43),(50,'Ροδόπης',6,'','gr','greek',44),(51,'Ξάνθης',6,'','gr','greek',45),(52,'Ιωαννίνων',4,'','gr','greek',46),(53,'Θεσπρωτίας',4,'','gr','greek',47),(54,'Άρτας',4,'','gr','greek',48),(55,'Πρέβεζας',4,'','gr','greek',49),(56,'Λαρίσης',5,'','gr','greek',50),(57,'Μαγνησίας',5,'','gr','greek',51),(58,'Τρικάλων',5,'','gr','greek',52),(59,'Καρδίτσας',5,'','gr','greek',53),(60,'Αιτωλοακαρνανίας',15,'','gr','greek',54),(61,'Ευβοίας',15,'','gr','greek',55),(62,'Ευρυτανίας',15,'','gr','greek',56),(63,'Βοιωτίας',15,'','gr','greek',57),(64,'Φθιώτιδας',15,'','gr','greek',58),(65,'Φωκίδας',15,'','gr','greek',59),(66,'Αργολίδας',13,'','gr','greek',60),(67,'Αρκαδίας',13,'','gr','greek',61),(68,'Αχαΐας',13,'','gr','greek',62),(69,'Ηλείας',13,'','gr','greek',63),(70,'Κορινθίας',13,'','gr','greek',64),(71,'Λακωνίας',13,'','gr','greek',65),(72,'Μεσσηνίας',13,'','gr','greek',66),(73,'Δωδεκάνησα',10,'','gr','greek',67),(74,'Κυκλάδες',10,'','gr','greek',68),(75,'Λέσβου',10,'','gr','greek',69),(76,'Σάμου',10,'','gr','greek',70),(77,'Χίου',10,'','gr','greek',71),(78,'Ζάκυνθος',12,'','gr','greek',72),(79,'Κέρκυρα',12,'','gr','greek',73),(80,'Κεφαλλονιά',12,'','gr','greek',74),(81,'Λευκάδα',12,'','gr','greek',75),(82,'Χανίων',7,'','gr','greek',76),(83,'Ρεθύμνου',7,'','gr','greek',77),(84,'Ηρακλείου',7,'','gr','greek',78),(85,'Λασιθίου',7,'','gr','greek',79),(87,'Αμμόχωστος',22,'','gr','greek',80),(88,'Λάρνακα',22,'','gr','greek',81),(89,'Λεμεσός',22,'','gr','greek',82),(90,'Λευκωσία',22,'','gr','greek',83),(91,'Πάφος',22,'','gr','greek',84),(92,'Paphos',22,'','gr','english',85),(93,'Nicosia',22,'','gr','english',86),(94,'Limassol',22,'','gr','english',87),(95,'Larnaca',22,'','gr','english',88),(96,'Famagusta',22,'','gr','english',89),(97,'Athens - Eastern Suburbs',2,'','gr','english',90),(98,'Athens - Northern Suburbs',2,'','gr','english',91),(99,'Athens - Western Suburbs',2,'','gr','english',92),(100,'Athens - Southern Athens',2,'','gr','english',93),(101,'Athens - Center',2,'','gr','english',94),(102,'Piraeus',2,'','gr','english',95),(103,'Attica Upstate',2,'','gr','english',96),(104,'East Thessaloniki',21,'','gr','english',97),(105,'East Suburbs',21,'','gr','english',98),(106,'North Thessaloniki',21,'','gr','english',99),(107,'West Thessaloniki',21,'','gr','english',100),(108,'West Suburbs',21,'','gr','english',101),(109,'Central Thessaloniki',21,'','gr','english',102),(110,'hhhhhhhhh',23,'','gr','english',103),(111,'up',24,'','in','english',104);
/*!40000 ALTER TABLE `tbl_adr_perfecture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_branch`
--

DROP TABLE IF EXISTS `tbl_branch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_branch` (
  `branch_id` int(11) NOT NULL AUTO_INCREMENT,
  `branch_name` text NOT NULL,
  `branch_address` text,
  `branch_code` varchar(10) DEFAULT NULL,
  `lang` varchar(100) DEFAULT NULL,
  `main_branch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`branch_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_branch`
--

LOCK TABLES `tbl_branch` WRITE;
/*!40000 ALTER TABLE `tbl_branch` DISABLE KEYS */;
INSERT INTO `tbl_branch` VALUES (1,'Ajmera','e','ajm','english',1),(8,'अजमेरा','0','','hindi',1);
/*!40000 ALTER TABLE `tbl_branch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_coll_code`
--

DROP TABLE IF EXISTS `tbl_coll_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_coll_code` (
  `coll_code_id` int(11) NOT NULL AUTO_INCREMENT,
  `coll_code_name` text NOT NULL,
  `main_branch_id` int(11) DEFAULT NULL,
  `lang` varchar(100) NOT NULL,
  `main_coll_code_id` int(11) DEFAULT NULL,
  `Areaname` varchar(20) DEFAULT NULL,
  `GroupLeader` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`coll_code_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_coll_code`
--

LOCK TABLES `tbl_coll_code` WRITE;
/*!40000 ALTER TABLE `tbl_coll_code` DISABLE KEYS */;
INSERT INTO `tbl_coll_code` VALUES (1,'collcode1',1,'english',1,'4',NULL);
/*!40000 ALTER TABLE `tbl_coll_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_company_details`
--

DROP TABLE IF EXISTS `tbl_company_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_company_details` (
  `company_id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(200) CHARACTER SET utf8 NOT NULL,
  `address` text CHARACTER SET utf8 NOT NULL,
  `tel_no` varchar(100) CHARACTER SET utf8 NOT NULL,
  `fax_no` varchar(100) CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 NOT NULL,
  `logo` varchar(100) CHARACTER SET utf8 NOT NULL,
  `website` varchar(100) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_company_details`
--

LOCK TABLES `tbl_company_details` WRITE;
/*!40000 ALTER TABLE `tbl_company_details` DISABLE KEYS */;
INSERT INTO `tbl_company_details` VALUES (1,'EGURU IT Solution India Pvt.Ltd12','cinchwad12','12312365471','12312365484','4eguru1@gmail.com','1fgnya11sssss.jpg','www.4eguru1.com'),(2,'eguru @4eguru','Sitai Sector: 20, Plot No 124, krishnanagar Chinchwad','9960758220','9960758220','yashvant233619@yahoo.co.in','4girl4.jpg','www.echitfuntsoftware.in');
/*!40000 ALTER TABLE `tbl_company_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_coordinator_code`
--

DROP TABLE IF EXISTS `tbl_coordinator_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_coordinator_code` (
  `co_code_id` int(11) NOT NULL AUTO_INCREMENT,
  `co_name` text NOT NULL,
  `lang` varchar(100) DEFAULT NULL,
  `main_co_code_id` int(11) DEFAULT NULL,
  `main_branch_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`co_code_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_coordinator_code`
--

LOCK TABLES `tbl_coordinator_code` WRITE;
/*!40000 ALTER TABLE `tbl_coordinator_code` DISABLE KEYS */;
INSERT INTO `tbl_coordinator_code` VALUES (1,'Coord1','english',1,1),(2,'CRD20101','english',2,2),(3,'Coord1','hindi',1,1),(4,'CRD20101','hindi',2,2),(5,'Coord3','english',5,5),(6,'Coord3','hindi',5,5);
/*!40000 ALTER TABLE `tbl_coordinator_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_dep_slip_details`
--

DROP TABLE IF EXISTS `tbl_dep_slip_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_dep_slip_details` (
  `dep_slip_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `branch_code` varchar(5) DEFAULT NULL,
  `dep_slip_no` bigint(11) NOT NULL,
  `depslip_Date` date NOT NULL,
  `BankName` varchar(30) DEFAULT NULL,
  `AccNo` varchar(30) DEFAULT NULL,
  `Cash_Cheque` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`dep_slip_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_dep_slip_details`
--

LOCK TABLES `tbl_dep_slip_details` WRITE;
/*!40000 ALTER TABLE `tbl_dep_slip_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_dep_slip_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_find`
--

DROP TABLE IF EXISTS `tbl_find`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_find` (
  `find_key` varchar(200) NOT NULL,
  UNIQUE KEY `find_key` (`find_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_find`
--

LOCK TABLES `tbl_find` WRITE;
/*!40000 ALTER TABLE `tbl_find` DISABLE KEYS */;
INSERT INTO `tbl_find` VALUES ('111_225'),('121_123_11'),('action'),('activate'),('active_members'),('add'),('addnew'),('add_area_per_city'),('add_lang'),('add_lang_msg'),('add_menu'),('add_new_app_var'),('add_new_cus_field'),('add_new_entry'),('Add_New_IP_Address'),('add_privilege_type'),('add_product'),('add_top_level_menu'),('add_to_another_lang'),('admin_error_color'),('admin_home'),('adm_home_manage_subscriber_list'),('adm_home_manage_template_list'),('adm_home_multilang_msg_list'),('all'),('all_user_news_letter_report'),('api_conn_setting'),('api_id'),('application_variable_list'),('applied_student_list'),('approve'),('area_list'),('area_name'),('assign_privilege'),('back'),('backup_successfully'),('back_to_admin_home'),('back_to_home'),('back_to_home_page'),('back_to_list_page'),('back_to_messages'),('banner1'),('banner2'),('banner3'),('banner4'),('banner5'),('banner6'),('bannet_detail'),('birth_date'),('black_list_members'),('blk_email'),('blk_your_usrname_email'),('block'),('cancel'),('category'),('change_password'),('change_picture'),('check_box'),('chk_availability'),('chk_unchk'),('choose_default'),('choose_pre_lang_email_notification'),('city'),('city_list'),('city_name'),('click_here_to_login'),('close'),('colomn'),('combo_box'),('common_setting'),('communication'),('contact_info'),('country'),('country_list'),('create_date'),('create_newsletter'),('curr_pc_ip_add'),('database_backup'),('database_restore'),('date'),('dd_mm_yy'),('deactive'),('default'),('default_flag'),('default_smtp'),('delete'),('del_select'),('desc'),('descrip'),('display'),('display_name'),('edit'),('Edit_IP_Address'),('edit_lang'),('edit_news_letter'),('edit_page_content'),('edit_payment_gateway'),('email'),('email_msg'),('enable'),('english'),('enter_email'),('enter_login_nam'),('expense_type'),('field_name'),('field_value'),('first_name'),('forget_pass'),('forgot_password_email_message'),('from'),('full_name'),('gateway'),('gateway_url'),('get'),('go'),('go_back_to_admin_home'),('go_list'),('html'),('html_content'),('image'),('inactive_members'),('inbox'),('incoming_msg'),('incorrect_email_login'),('installment_f'),('interest_mailing_list_wise_report'),('In_Time'),('ip'),('ip_address_list'),('ip_address_name'),('ip_address_type'),('IP_blocked_subject'),('ip_home'),('ip_mem_details'),('join_date'),('key'),('language'),('languages_home'),('lang_list'),('lang_sel'),('Last_Date_Time'),('last_ip'),('last_login_detail'),('last_login_list'),('last_name'),('last_update'),('link'),('login'),('login_details_sent'),('login_id'),('login_info'),('login_name_exist_plz_choose_another'),('login_required'),('logout'),('mail'),('mailing_list_name'),('mail_list'),('members'),('member_type'),('menu_name'),('message'),('message_code'),('mobile_no'),('modify_app_var'),('modify_privilege_type'),('msg'),('msg_date'),('msg_details'),('msg_id'),('msg_send'),('msg_time'),('msg_title'),('new/unread_msg'),('news_letter'),('news_letters'),('news_letter_title'),('news_letter_wise_report'),('new_password'),('new_user'),('new_usr_register'),('no'),('non_members'),('note'),('no_photo'),('no_rec'),('no_rec_found'),('offline'),('on/of_sms_feature'),('online'),('order'),('orderno'),('other'),('outbox'),('Out_Time'),('page'),('page_type'),('password'),('password_v'),('pass_reminder'),('pass_updated_successfully'),('payment_gateway_list'),('pay_metod_name'),('pay_ser_setting'),('perfecture_list'),('perfecture_name'),('personnel_info'),('personnel_list'),('per_details'),('per_info'),('per_logo'),('photo'),('photograph'),('please_type_the_code'),('posted_date'),('priority'),('privilege_type'),('privilege_type_list'),('privilege_type_name'),('pri_language'),('process_id_v'),('profile'),('protocol'),('radio_box'),('radio_button'),('range'),('rec_add'),('rec_del'),('rec_update'),('remove'),('reply'),('request_pass'),('required_info_indicator_color'),('reset'),('reset_pass'),('restore_successfully'),('row'),('save'),('search'),('sec_language'),('select'),('select_checked'),('select_menu'),('select_type'),('send'),('send_date'),('send_time'),('sent'),('sent_msg'),('set_value'),('showfulllist'),('show_on_off'),('signup_email_message'),('single'),('size'),('sms_feature_on'),('sms_setting'),('SMTP_Details'),('sorry_incorrect_username'),('Sorry_Insufficient_Permission_You_need_to_login_first'),('sorry_ip_mismatch'),('sort_asc'),('sort_by'),('sort_desc'),('sort_msg'),('sort_order_no'),('sort_out_list'),('So_will_allow_IP_from_range'),('spammer'),('spammer_report'),('state'),('status'),('store'),('stored_msg'),('Stored_selectd_message'),('string_concat'),('student_list'),('student_name'),('subject'),('subject_topic'),('subject_type_list'),('submit'),('sub_range'),('super_admin_home'),('sys_date_time'),('take_backup'),('targetlist'),('target_interested_category'),('template'),('template_detail'),('template_list'),('template_title'),('term_cond'),('text'),('text_area'),('thank_u_for_ur_interest'),('to'),('top_level_menu'),('total_news_letter'),('total_subscriber'),('total_target_list'),('total_time'),('total_unsubscriber'),('total_user'),('to_date'),('to_person'),('transe_type'),('translate'),('type'),('unblock_email'),('upadmin_details'),('update'),('update_send_by_sms'),('upgrade_membership'),('up_personnel_info'),('user_home'),('user_id'),('user_name'),('usrusr_msg_list'),('usr_activated'),('usr_home_inbox'),('usr_home_manage_sent_messages'),('usr_home_msg_archive_storage'),('usr_home_outbox'),('usr_home_send_msg_to_adm/system'),('usr_welcome_to_mem_sec'),('value'),('vender_id_v'),('view'),('view_demo_page'),('waiting'),('website_description'),('website_keywords'),('website_title'),('with_select'),('with_sel_send_msg'),('yes'),('zip_code');
/*!40000 ALTER TABLE `tbl_find` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_introducer_type`
--

DROP TABLE IF EXISTS `tbl_introducer_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_introducer_type` (
  `introducer_id` int(11) NOT NULL AUTO_INCREMENT,
  `introducer_type` varchar(100) DEFAULT NULL,
  `lang` varchar(100) NOT NULL,
  `main_introducer_id` int(11) NOT NULL,
  PRIMARY KEY (`introducer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_introducer_type`
--

LOCK TABLES `tbl_introducer_type` WRITE;
/*!40000 ALTER TABLE `tbl_introducer_type` DISABLE KEYS */;
INSERT INTO `tbl_introducer_type` VALUES (1,'software Eng','english',1),(2,'Marketing','english',2),(3,'share holder','english',3),(6,'businessman','english',6),(7,'सॉफ्टवेयर अभियांत्रिकी','hindi',1),(8,'विपणन','hindi',2),(9,'शेयर धारक','hindi',3),(11,'व्यवसायी','hindi',6);
/*!40000 ALTER TABLE `tbl_introducer_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_lang`
--

DROP TABLE IF EXISTS `tbl_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_lang` (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(100) NOT NULL DEFAULT '',
  `image` longtext NOT NULL,
  `note` longtext NOT NULL,
  `google_key` varchar(50) NOT NULL,
  `selected` int(11) NOT NULL DEFAULT '0',
  `default_flag` int(11) NOT NULL,
  PRIMARY KEY (`lang_id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_lang`
--

LOCK TABLES `tbl_lang` WRITE;
/*!40000 ALTER TABLE `tbl_lang` DISABLE KEYS */;
INSERT INTO `tbl_lang` VALUES (2,'english','1us.png','','',1,1),(39,'tamil','download.jpg','<meta http-equiv=\'Content-Type\' content=\"text/html; charset=\" value=\'text/html; charset=utf-8\'>','te',0,0),(36,'hindi','3india.jpg','<meta http-equiv=\'Content-Type\' content=\"text/html; charset=\" value=\'text/html; charset=utf-8\'>','hi',0,0),(37,'marathi','download.jpg','<meta http-equiv=\'Content-Type\' content=\"text/html; charset=\" value=\'text/html; charset=utf-8\'>','mr',0,0),(38,'kannada','download.jpg','<meta http-equiv=\'Content-Type\' content=\"text/html; charset=\" value=\'text/html; charset=utf-8\'>','kn',0,0);
/*!40000 ALTER TABLE `tbl_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_lang_msg`
--

DROP TABLE IF EXISTS `tbl_lang_msg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_lang_msg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(30) NOT NULL DEFAULT 'english',
  `mainkey` varchar(200) DEFAULT NULL,
  `subkey` varchar(100) DEFAULT NULL,
  `descr` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32293 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_lang_msg`
--

LOCK TABLES `tbl_lang_msg` WRITE;
/*!40000 ALTER TABLE `tbl_lang_msg` DISABLE KEYS */;
INSERT INTO `tbl_lang_msg` VALUES (7846,'english','111_225',NULL,'( e.g 111-255 )'),(7844,'english','121_123_11',NULL,'(e.g 121.123.11)'),(7738,'english','action',NULL,'Action'),(17236,'english','activate','','Activate'),(17379,'english','active_members','','Active Members'),(17325,'english','add','','Add'),(17943,'english','addnew',NULL,'Add New'),(30942,'english','addnew_msg_id','','Add New MSG ID'),(8611,'english','add_lang',NULL,'Add New Language'),(17310,'english','add_lang_msg','','Add Language Message'),(12380,'english','add_new_app_var',NULL,'Add new application variable'),(15760,'english','add_new_cus_field',NULL,'Add New Custom Field'),(8639,'english','Add_New_IP_Address',NULL,'Add New IP Address'),(10620,'english','add_privilege_type',NULL,'Add new membership type'),(10302,'english','add_product',NULL,'Add Property'),(14184,'english','add_to_another_lang',NULL,'Translate '),(9951,'english','admin_home',NULL,'Admin Home'),(17988,'english','adm_home_manage_subscriber_list','','Manage News Letter Subscriber List'),(17975,'english','adm_home_manage_template_list','','Manage News Letter Templates'),(8595,'english','adm_home_multilang_msg_list',NULL,' Message List'),(7802,'english','all',NULL,'All'),(17994,'english','all_user_news_letter_report','','News letter report for subscriber'),(7787,'english','api_conn_setting',NULL,'API Connection Setting'),(12377,'english','application_variable_list',NULL,'Application variables list'),(7753,'english','approve',NULL,'Approve'),(15630,'english','area_list',NULL,'Area List'),(15626,'english','area_name',NULL,'Area'),(17168,'english','assign_privilege','','Assign Privilege'),(12374,'english','back',NULL,'Back'),(17160,'english','backup_successfully','','Your database has been backed up successfully and is stored online!'),(17937,'english','back_to_admin_home',NULL,'Back To Admin Home'),(30931,'english','back_to_home','','Back To Home Page'),(6315,'english','back_to_home_page',NULL,'Back To Home Page'),(7756,'english','back_to_list_page',NULL,'Back To List Page'),(15786,'english','banner1',NULL,'Banner1'),(15788,'english','banner2',NULL,'Banner2'),(15790,'english','banner3',NULL,'Banner3'),(15792,'english','banner4',NULL,'Banner4'),(15794,'english','banner5',NULL,'Banner5'),(15796,'english','banner6',NULL,'Banner6'),(15798,'english','bannet_detail',NULL,'Banner Details'),(6466,'english','birth_date',NULL,'Birth Date'),(16051,'english','black_list_members',NULL,'Black Listed Members'),(16814,'english','blk_email','','Block Email'),(16818,'english','blk_your_usrname_email','','Your username and email id is blocked by admin'),(7746,'english','block',NULL,'Approved'),(7690,'english','cancel',NULL,'Cancel'),(10473,'english','category',NULL,'Category'),(10158,'english','change_password',NULL,'Change Password'),(10326,'english','change_picture',NULL,'Change Picture'),(17245,'english','check_box','','Check Box'),(30927,'english','chk_availability','','Check Availability'),(16652,'english','chk_unchk',NULL,'Check/Uncheck'),(17164,'english','choose_default','','Choose default'),(17893,'english','choose_pre_lang_email_notification','','Choose preferred language for email notification'),(482,'english','city',NULL,'City'),(15642,'english','city_list',NULL,'City List'),(15648,'english','city_name',NULL,'City Name'),(7859,'english','click_here_to_login',NULL,'Click Here To login'),(10452,'english','close',NULL,'Close'),(15752,'english','colomn',NULL,'Columns'),(30917,'english','combo_box','','Combo Box'),(10530,'english','common_setting',NULL,'Common Settings'),(10119,'english','contact_info',NULL,'Contact Information'),(4025,'english','country',NULL,'Country'),(18004,'english','create_date','','Created Date'),(18002,'english','create_newsletter','','Create new newsletter'),(30925,'english','curr_pc_ip_add','','Current IP Address of PC'),(10404,'english','database_backup',NULL,'One click Backup!'),(10638,'english','database_restore',NULL,'Database Restore'),(10224,'english','date',NULL,'Date'),(4030,'english','dd_mm_yy',NULL,'(DD/MM/YY)'),(17377,'english','deactive','','Deactivate'),(17166,'english','default','','default'),(18023,'english','default_flag','','Set as Default'),(18021,'english','default_smtp','','default_smtp'),(17939,'english','delete',NULL,'Delete'),(15708,'english','del_select',NULL,'Delete selected records'),(10116,'english','desc',NULL,'Description'),(16555,'english','display_name',NULL,'Display Name'),(7737,'english','edit',NULL,'Edit'),(17981,'english','edit_news_letter','','Edit News Letter'),(10560,'english','edit_payment_gateway',NULL,'Edit payment gateway'),(14363,'english','email',NULL,'Email'),(17275,'english','email_msg',NULL,'Email Message'),(10527,'english','enable',NULL,'Enable'),(7999,'english','english',NULL,'English'),(15860,'english','enter_email',NULL,'Enter your email address'),(17206,'english','enter_login_nam','','Enter Your Login Name'),(7790,'english','field_name',NULL,'Field Name'),(7791,'english','field_value',NULL,'Field Value'),(6464,'english','first_name',NULL,'First Name'),(15861,'english','forget_pass',NULL,'Forgotten Password'),(17371,'english','forgot_password_email_message','','Forgot Password Email Message'),(7805,'english','from',NULL,'From'),(6567,'english','full_name',NULL,'Full Name'),(10551,'english','gateway',NULL,'Gateway'),(10539,'english','gateway_url',NULL,'URL To Gateway'),(17969,'english','get','','get'),(15722,'english','go',NULL,'GO'),(30937,'english','host_name',NULL,'Host Name'),(15800,'english','html',NULL,'HTML'),(18003,'english','html_content','','HTML Content:'),(8024,'english','image',NULL,'Image'),(17381,'english','inactive_members','','Inactive Members'),(16427,'english','inbox',NULL,'Inbox'),(16696,'english','incoming_msg',NULL,'INCOMING MESSAGES'),(17208,'english','incorrect_email_login','','Entered email id and login name not match with our system, please enter correct email id and login name'),(10548,'english','installment_f',NULL,'Installments'),(17995,'english','interest_mailing_list_wise_report','','Interest/Mailing list wise report'),(7978,'english','In_Time',NULL,'In-Time'),(7815,'english','ip',NULL,'IP'),(8643,'english','ip_address_list',NULL,'IP Address List'),(7785,'english','ip_address_name',NULL,'IP Address Name'),(7786,'english','ip_address_type',NULL,'IP Type'),(17065,'english','IP_blocked_subject','','IP Address is blocked'),(15394,'english','ip_home',NULL,'IP Home'),(17393,'english','ip_mem_details','','IP Member Details'),(9960,'english','join_date',NULL,'Registration Date'),(30940,'english','langlist','','Language List'),(8022,'english','language',NULL,'Language'),(17312,'english','lang_list','','Language List'),(7977,'english','Last_Date_Time',NULL,'Last Date Time'),(7976,'english','last_ip',NULL,'Last IP'),(7813,'english','last_login_detail',NULL,'Last Login Details'),(7981,'english','last_login_list',NULL,'Last Login List'),(6465,'english','last_name',NULL,'Last Name'),(17269,'english','last_update','','Last Update'),(7764,'english','login',NULL,'Login'),(15864,'english','login_details_sent',NULL,'Password sent successfully on the following email account!!'),(9957,'english','login_id',NULL,'Username'),(6561,'english','login_info',NULL,'Login Information'),(3865,'english','logout',NULL,'Logout'),(18010,'english','mail','','Mail'),(18005,'english','mailing_list_name','','Mailing List Name'),(18014,'english','mail_list','','Mailing List'),(16439,'english','members',NULL,'Total Properties'),(16037,'english','member_type',NULL,'Membership Type'),(15822,'english','menu_name',NULL,'Menu Name'),(10344,'english','message',NULL,'Message'),(17897,'english','message_code','','Message Code'),(6453,'english','mobile_no',NULL,'Mobile number'),(12383,'english','modify_app_var',NULL,'Modify application variables'),(10614,'english','modify_privilege_type',NULL,'Modify privilege  type'),(3772,'english','msg',NULL,'Message'),(3773,'english','msg_date',NULL,'Message Date'),(7828,'english','msg_details',NULL,'Message Details'),(7755,'english','msg_id',NULL,'Msg ID'),(10137,'english','msg_send',NULL,'Message Sent Successfully!'),(3774,'english','msg_time',NULL,'Message Time'),(3775,'english','msg_title',NULL,'Message Title'),(30939,'english','name',NULL,'Name'),(32191,'english','new_unread_msg','','New unread messages'),(17979,'english','news_letter','','News Letter'),(17977,'english','news_letters','','News Letter List'),(17978,'english','news_letter_title','','Title'),(17996,'english','news_letter_wise_report','','News letter statistics'),(114,'english','new_password',NULL,'New Password:'),(9954,'english','new_user',NULL,'Signup'),(10125,'english','new_usr_register',NULL,'New Registration'),(15446,'english','no',NULL,'No'),(18013,'english','non_members','','Non Members'),(3552,'english','note',NULL,'Note'),(15532,'english','no_rec_found',NULL,'No Record Found!!!'),(16660,'english','offline',NULL,'Offline'),(6319,'english','on/of_sms_feature',NULL,'(ON/OFF SMS Feature)'),(16658,'english','online',NULL,'Online'),(16656,'english','order',NULL,'Order'),(30919,'english','orderno','','Order no'),(17438,'english','other','','Other'),(16553,'english','outbox',NULL,'OutBox'),(7979,'english','Out_Time',NULL,'Out-Time'),(10599,'english','page',NULL,'Page'),(3894,'english','password',NULL,'Password'),(15500,'english','pass_reminder',NULL,'Password Reminder'),(10557,'english','payment_gateway_list',NULL,'Payment gateway list'),(10521,'english','pay_metod_name',NULL,'Payment method name'),(15632,'english','perfecture_list',NULL,'Prefecture List'),(15636,'english','perfecture_name',NULL,'Prefecture'),(4244,'english','personnel_info',NULL,'Personal Information'),(3766,'english','personnel_list',NULL,'Member List'),(10338,'english','per_details',NULL,'Personal Details'),(10179,'english','per_info',NULL,'Personal Information'),(10128,'english','per_logo',NULL,'Photo'),(16523,'english','photo',NULL,'Photo'),(10182,'english','photograph',NULL,'Photo'),(10113,'english','posted_date',NULL,'Posted Date'),(17991,'english','priority','','Priority'),(8037,'english','privilege_type',NULL,'Privilege type'),(10617,'english','privilege_type_list',NULL,'List of available membership types'),(10611,'english','privilege_type_name',NULL,'Membership type'),(17395,'english','pri_language','','Primary Language'),(10536,'english','process_id_v',NULL,'Payment Processor ID'),(10524,'english','protocol',NULL,'Protocol'),(17247,'english','radio_box','','Radio Box'),(30915,'english','radio_button','','Radio Button'),(10623,'english','rec_add',NULL,'Record added successfully!!!'),(10188,'english','rec_del',NULL,'Record deleted successfully!!!'),(10173,'english','rec_update',NULL,'Record updated successfully!!'),(16343,'english','remove',NULL,'Remove'),(7747,'english','reply',NULL,'Reply'),(15862,'english','request_pass',NULL,'Request Password'),(8026,'english','reset',NULL,'Reset'),(17970,'english','reset_pass','','Reset Password'),(17261,'english','restore_successfully','','Your MySQL database has been restored up successfully'),(15754,'english','row',NULL,'Rows'),(15372,'english','save',NULL,'Save'),(3977,'english','search',NULL,'Analytical Search'),(17397,'english','sec_language','','Secondary language'),(10686,'english','select',NULL,'Select'),(15750,'english','select_checked',NULL,'Selected/Checked'),(14126,'english','select_type',NULL,'Select type'),(10131,'english','send',NULL,'Send'),(17980,'english','send_date','','Send Date'),(17990,'english','send_time','','Send Time'),(16459,'english','sent',NULL,'Sent'),(16672,'english','sent_msg',NULL,'Sent Messages'),(7783,'english','set_value',NULL,'Set Value'),(7688,'english','showfulllist',NULL,'Show Full List'),(15726,'english','show_on_off',NULL,'Visible'),(17369,'english','signup_email_message','','Signup Email Message'),(17263,'english','single','','Single'),(15756,'english','size',NULL,'Size'),(6318,'english','sms_feature_on',NULL,'SMS ON'),(6317,'english','sms_setting',NULL,'SMS Settings'),(17989,'english','SMTP_Details','','SMTP Details'),(7860,'english','sorry_incorrect_username',NULL,'Sorry incorrect username or password'),(10401,'english','Sorry_Insufficient_Permission_You_need_to_login_first',NULL,'Sorry! Insufficient Permission!!  You need to login first'),(7858,'english','sorry_ip_mismatch',NULL,'Sorry IP Mismatch'),(17440,'english','sort_asc','','asc'),(17444,'english','sort_by','','Sort by'),(17442,'english','sort_desc','','desc'),(7996,'english','sort_msg',NULL,'Sort Messages'),(9966,'english','sort_out_list',NULL,'Sort out list'),(7847,'english','So_will_allow_IP_from_range',NULL,'So will allow IP from range'),(17375,'english','spammer','','Spammer'),(16937,'english','spammer_report','','Spammer Report'),(8627,'english','state',NULL,'State'),(7975,'english','status',NULL,'Status'),(16666,'english','store',NULL,'Store'),(16341,'english','stored_msg',NULL,'Stored Messages'),(16702,'english','Stored_selectd_message',NULL,'Stored selected message'),(17901,'english','string_concat','','No space allowed. String con cat with (_) keyword e.g. test_by_admin etc'),(17973,'english','student_list','','Student List'),(17972,'english','student_name','','Student Name'),(10140,'english','subject',NULL,'Subject'),(10212,'english','subject_type_list',NULL,'Canned Message List'),(7689,'english','submit',NULL,'Submit'),(7845,'english','sub_range',NULL,'Sub Range'),(9990,'english','super_admin_home',NULL,'Super Admin Home'),(18001,'english','sys_date_time','','System Date Time'),(15470,'english','take_backup',NULL,'One click Backup!'),(18009,'english','targetlist','','Target List'),(17985,'english','target_interested_category','','Target List/Interested Category'),(17983,'english','template','','Template'),(17984,'english','template_detail','','Template Details'),(17987,'english','template_list','','Template List'),(17982,'english','template_title','','Template Title'),(10155,'english','term_cond',NULL,'I understand and accept the Terms & Conditions'),(17243,'english','text','','Text'),(17241,'english','text_area','','Text Area'),(17986,'english','thank_u_for_ur_interest','','Thank you for your interest'),(7806,'english','to',NULL,'To'),(18000,'english','total_news_letter','','Total News Letter'),(17997,'english','total_subscriber','','Total Subscriber'),(17999,'english','total_target_list','','Total Target List'),(7818,'english','total_time',NULL,'Total Time'),(17998,'english','total_unsubscriber','','Total Unsubscriber'),(17343,'english','total_user','','Total Users'),(9353,'english','to_date',NULL,'To Date'),(9972,'english','to_person',NULL,'To person'),(6485,'english','transe_type',NULL,'Transaction Type'),(17323,'english','translate','','Translate'),(6575,'english','type',NULL,'Type'),(17218,'english','unblock_email','','Unblock Email'),(17389,'english','upadmin_details','','Administrators Details'),(15720,'english','update_send_by_sms',NULL,'Update Send By SMS'),(17348,'english','upgrade_membership','','Upgrade Membership'),(10176,'english','up_personnel_info',NULL,'Update Personal Information'),(30938,'english','username',NULL,'User name'),(9981,'english','user_home',NULL,'User Home'),(7788,'english','user_id',NULL,'Customer ID'),(7781,'english','user_name',NULL,'Username'),(10170,'english','usrusr_msg_list',NULL,'List of Sent Messages to members'),(12236,'english','usr_activated',NULL,'Your Account has been activated successfully !!'),(10074,'english','usr_home_inbox',NULL,'Inbox'),(10068,'english','usr_home_manage_sent_messages',NULL,'Sent Messages'),(16413,'english','usr_home_msg_archive_storage',NULL,'Message Archive/Storage'),(10077,'english','usr_home_outbox',NULL,'Communication'),(10167,'english','usr_home_send_msg_to_adm/system',NULL,'Message the Administrator'),(10029,'english','usr_welcome_to_mem_sec',NULL,'Welcome To Members Section'),(7794,'english','value',NULL,'Value'),(10542,'english','vender_id_v',NULL,'Merchant Code'),(17410,'english','view','','View'),(17106,'english','view_demo_page','','View Demo Page'),(18007,'english','waiting','','Waiting'),(30921,'english','website_description','','chitfund software'),(30935,'english','website_keywords','','chit fund online'),(7694,'english','website_title',NULL,'Kynya Chit Fund'),(15704,'english','with_select',NULL,'With Selected'),(10395,'english','with_sel_send_msg',NULL,'With selected send message'),(15444,'english','yes',NULL,'Yes'),(4187,'english','zip_code',NULL,'Zip Code'),(30943,'hindi','addnew_msg_id','','जोड़ें नए MSG आईडी'),(30944,'hindi','langlist','','भाषा सूची'),(30945,'hindi','name','','नाम'),(30946,'hindi','username','','उपयोगकर्ता नाम'),(30947,'hindi','host_name','','मेजबान का नाम'),(30948,'hindi','website_keywords','','चिट फंड ऑनलाइन'),(30949,'hindi','back_to_home','','वापस मुख पृष्ठ करने के लिए'),(30950,'hindi','chk_availability','','उपलब्धता जाँचिये'),(30951,'hindi','curr_pc_ip_add','','पीसी की वर्तमान आईपी पता'),(30952,'hindi','website_description','','chitfund सॉफ्टवेयर'),(30953,'hindi','orderno','','कोई आदेश'),(30954,'hindi','combo_box','','कॉम्बो बॉक्स'),(30955,'hindi','radio_button','','रेडियो बटन'),(30956,'hindi','default_flag','','डिफ़ॉल्ट रूप में सेट करें'),(30957,'hindi','default_smtp','','default_smtp'),(30958,'hindi','mail_list','','मेलिंग सूची'),(30959,'hindi','non_members','','गैर सदस्य'),(30960,'hindi','mail','','डाक'),(30961,'hindi','targetlist','','लक्ष्य सूची'),(30962,'hindi','waiting','','प्रतीक्षा'),(30963,'hindi','mailing_list_name','','मेलिंग सूची नाम'),(30964,'hindi','create_date','','बनाया गया दिनांक'),(30965,'hindi','html_content','','HTML सामग्री:'),(30966,'hindi','create_newsletter','','नया समाचार पत्र'),(30967,'hindi','sys_date_time','','दिनांक प्रणाली का समय'),(30968,'hindi','total_news_letter','','कुल समाचार पत्र'),(30969,'hindi','total_target_list','','कुल लक्ष्य सूची'),(30970,'hindi','total_unsubscriber','','कुल Unsubscriber'),(30971,'hindi','total_subscriber','','कुल सदस्य'),(30972,'hindi','news_letter_wise_report','','समाचार पत्र के आँकड़े'),(30973,'hindi','interest_mailing_list_wise_report','','ब्याज / मेलिंग सूची बुद्धिमान रिपोर्ट'),(30974,'hindi','all_user_news_letter_report','','समाचार पत्र की रिपोर्ट के लिए सदस्य'),(30975,'hindi','priority','','प्राथमिकता'),(30976,'hindi','send_time','','भेजने का समय'),(30977,'hindi','SMTP_Details','','SMTP विवरण'),(30978,'hindi','adm_home_manage_subscriber_list','','प्रबंधित समाचार पत्र ग्राहक सूची'),(30979,'hindi','template_list','','टेम्पलेट सूची'),(30980,'hindi','thank_u_for_ur_interest','','आप अपनी रुचि के लिए धन्यवाद'),(30981,'hindi','target_interested_category','','लक्ष्य सूची / श्रेणी इच्छुक'),(30982,'hindi','template_detail','','टेम्पलेट विवरण'),(30983,'hindi','template','','टेम्पलेट'),(30984,'hindi','template_title','','टेम्पलेट शीर्षक'),(30985,'hindi','edit_news_letter','','संपादित करें समाचार पत्र'),(30986,'hindi','send_date','','भेजें दिनांक'),(30987,'hindi','news_letter','','समाचार पत्र'),(30988,'hindi','news_letter_title','','शीर्षक'),(30989,'hindi','news_letters','','समाचार पत्र की सूची'),(30990,'hindi','adm_home_manage_template_list','','प्रबंधित समाचार पत्र टेम्पलेट्स'),(30991,'hindi','student_list','','छात्र सूची'),(30992,'hindi','student_name','','छात्र का नाम'),(30993,'hindi','reset_pass','','पासवर्ड रीसेट'),(30994,'hindi','get','','मिल'),(30995,'hindi','addnew','','जोड़ें नए'),(30996,'hindi','delete','','हटाना'),(30997,'hindi','back_to_admin_home','','घर वापस करने के लिए व्यवस्थापक'),(30998,'hindi','string_concat','','कोई जगह नहीं की अनुमति दी. तार के साथ चोर बिल्ली (_) test_by_admin आदि जैसे खोजशब्द'),(30999,'hindi','message_code','','संदेश कोड'),(31000,'hindi','choose_pre_lang_email_notification','','चुनें ईमेल सूचना के लिए पसंद की भाषा'),(31001,'hindi','sort_by','','द्वारा क्रमबद्ध करें'),(31002,'hindi','sort_desc','','desc'),(31003,'hindi','sort_asc','','ASC'),(31004,'hindi','other','','अन्य'),(31005,'hindi','view','','देखना'),(31006,'hindi','sec_language','','माध्यमिक भाषा'),(31007,'hindi','pri_language','','प्राथमिक भाषा'),(31008,'hindi','ip_mem_details','','आईपी सदस्य विवरण'),(31009,'hindi','upadmin_details','','प्रशासक का विवरण'),(31010,'hindi','inactive_members','','निष्क्रिय सदस्य'),(31011,'hindi','active_members','','सक्रिय सदस्य'),(31012,'hindi','deactive','','निष्क्रिय'),(31013,'hindi','spammer','','Spammer'),(31014,'hindi','forgot_password_email_message','','पासवर्ड भूल गए ईमेल संदेश'),(31015,'hindi','signup_email_message','','साइनअप ईमेल संदेश'),(31016,'hindi','upgrade_membership','','सदस्यता का उन्नयन'),(31017,'hindi','total_user','','कुल उपयोगकर्ता'),(31018,'hindi','add','','जोड़ना'),(31019,'hindi','translate','','अनुवाद करना'),(31020,'hindi','lang_list','','भाषा सूची'),(31021,'hindi','add_lang_msg','','भाषा संदेश जोड़ें'),(31243,'hindi','back_to_list_page','','वापस सूची में पृष्ठ'),(31022,'hindi','email_msg','','ईमेल संदेश'),(31023,'hindi','last_update','','अंतिम अद्यतन'),(31024,'hindi','single','','एक'),(31025,'hindi','restore_successfully','','आपके MySQL डाटाबेस अप किया गया है सफलतापूर्वक बहाल'),(31026,'hindi','radio_box','','रेडियो बॉक्स'),(31027,'hindi','check_box','','बॉक्स को चेक करें'),(31028,'hindi','text','','टेक्स्ट'),(31029,'hindi','text_area','','पाठ क्षेत्र'),(31030,'hindi','activate','','सक्रिय'),(31031,'hindi','unblock_email','','खुलना ईमेल'),(31032,'hindi','incorrect_email_login','','प्रवेश ईमेल आईडी और लॉगिन नाम हमारे सिस्टम के साथ मेल नहीं खाता, सही ईमेल आईडी और लॉगिन नाम दर्ज करें'),(31033,'hindi','enter_login_nam','','अपना लॉगिन नाम'),(31034,'hindi','assign_privilege','','असाइन विशेषाधिकार'),(31241,'hindi','back','','वापस'),(31035,'hindi','default','','डिफ़ॉल्ट'),(31036,'hindi','choose_default','','चुनें डिफ़ॉल्ट'),(31037,'hindi','backup_successfully','','अपने डेटाबेस अप किया गया है सफलतापूर्वक समर्थित है और ऑनलाइन संग्रह!'),(31038,'hindi','view_demo_page','','देखें डेमो पेज'),(31039,'hindi','IP_blocked_subject','','आईपी पता अवरुद्ध है'),(31040,'hindi','spammer_report','','Spammer रिपोर्ट'),(31041,'hindi','blk_your_usrname_email','','अपने उपयोगकर्ता नाम और ईमेल आईडी व्यवस्थापक द्वारा अवरुद्ध है'),(31042,'hindi','blk_email','','ब्लॉक ईमेल'),(31043,'hindi','action','','कार्य'),(31044,'hindi','add_lang','','नई भाषा जोड़ना'),(31045,'hindi','add_new_app_var','','नया आवेदन चर जोड़ें'),(31046,'hindi','add_new_cus_field','','जोड़ें नए कस्टम फील्ड'),(31047,'hindi','Add_New_IP_Address','','नई आईपी पता जोड़ें'),(31048,'hindi','add_privilege_type','','नई सदस्यता प्रकार को जोड़ना'),(31049,'hindi','add_product','','संपत्ति जोड़ें'),(31050,'hindi','admin_home','','व्यवस्थापक होम'),(31051,'hindi','all','','सब'),(31052,'hindi','api_conn_setting','','एपीआई स्थापना कनेक्शन'),(31242,'hindi','back_to_home_page','','वापस मुख पृष्ठ करने के लिए'),(31053,'hindi','application_variable_list','','आवेदन चर सूची'),(31054,'hindi','approve','','स्वीकृत'),(31055,'hindi','area_list','','क्षेत्र सूची'),(31056,'hindi','area_name','','क्षेत्रफल'),(31057,'hindi','block','','अनुमोदित'),(31058,'hindi','zip_code','','ज़िप कोड'),(31059,'hindi','usr_activated','','आपका खाता सक्रिय कर दिया गया है सफलतापूर्वक!'),(31060,'hindi','yes','','हां'),(31061,'hindi','usr_welcome_to_mem_sec','','सदस्यों के स्वागत करने के लिए धारा'),(31062,'hindi','with_select','','साथ चयनित'),(31063,'hindi','with_sel_send_msg','','संदेश भेजने के साथ चयनित'),(31064,'hindi','show_on_off','','दृश्यमान'),(31065,'hindi','value','','मूल्य'),(31066,'hindi','gateway_url','','यूआरएल के लिए प्रवेश द्वार'),(31067,'hindi','login_id','','उपयोगकर्ता नाम'),(31068,'hindi','update_send_by_sms','','अद्यतन एसएमएस द्वारा भेजें'),(31069,'hindi','up_personnel_info','','व्यक्तिगत जानकारी अद्यतन'),(31070,'hindi','user_home','','उपयोगकर्ता के घर'),(31071,'hindi','user_name','','उपयोगकर्ता नाम'),(31072,'hindi','add_to_another_lang','','अनुवाद करना'),(31073,'hindi','members','','कुल पूंजी'),(31074,'hindi','to','','से'),(31075,'hindi','total_time','','कुल समय'),(31076,'hindi','to_date','','तिथि करने के लिए'),(31077,'hindi','to_person','','व्यक्ति के लिए'),(31078,'hindi','transe_type','','लेनदेन के प्रकार'),(31079,'hindi','type','','टाइप'),(31080,'hindi','cancel','','रद्द करना'),(31081,'hindi','category','','श्रेणी'),(31082,'hindi','change_password','','पासवर्ड बदलें'),(31083,'hindi','change_picture','','चित्र बदलें'),(31084,'hindi','chk_unchk','','चेक / अ'),(31085,'hindi','city','','शहर'),(31086,'hindi','city_list','','शहर सूची'),(31087,'hindi','city_name','','शहर के नाम'),(31088,'hindi','click_here_to_login','','यहाँ क्लिक करें लॉगिन करने के लिए'),(31089,'hindi','close','','पास'),(31090,'hindi','colomn','','कॉलम'),(31091,'hindi','common_setting','','सामान्य सेटिंग्स'),(31092,'hindi','contact_info','','संपर्क करने संबंधी जानकारी'),(31093,'hindi','country','','देश'),(31094,'hindi','subject_type_list','','डिब्बाबंद संदेश सूची'),(31095,'hindi','user_id','','ग्राहक आईडी'),(31096,'hindi','usr_home_outbox','','संचार'),(31097,'hindi','database_restore','','डेटाबेस बहाल'),(31098,'hindi','date','','तिथि'),(31099,'hindi','del_select','','हटाएँ चयनित रिकॉर्ड'),(31100,'hindi','desc','','विवरण'),(31101,'hindi','display_name','','प्रदर्शन नाम'),(31102,'hindi','field_name','','फ़ील्ड का नाम'),(31103,'hindi','field_value','','क्षेत्र मान'),(31104,'hindi','first_name','','नाम'),(31105,'hindi','forget_pass','','भूल पासवर्ड'),(31106,'hindi','from','','से'),(31107,'hindi','full_name','','पूरा नाम'),(31108,'hindi','gateway','','द्वार'),(31109,'hindi','go','','GO'),(31110,'hindi','html','','HTML'),(31111,'hindi','image','','छवि'),(31112,'hindi','inbox','','इनबॉक्स'),(31113,'hindi','incoming_msg','','आने वाले संदेशों'),(31114,'hindi','installment_f','','किस्तों'),(31115,'hindi','In_Time','','समय में'),(31116,'hindi','ip','','आईपी'),(31117,'hindi','ip_address_list','','आईपी पता सूची'),(31118,'hindi','ip_address_name','','आईपी पता नाम'),(31119,'hindi','ip_address_type','','आईपी प्रकार'),(31120,'hindi','ip_home','','आईपी होम'),(31121,'hindi','term_cond','','मैं समझता हूँ और नियम व शर्तों को स्वीकार'),(31122,'hindi','usr_home_inbox','','इनबॉक्स'),(31123,'hindi','website_title','','Kynya चिट फंड'),(31124,'hindi','language','','भाषा'),(31125,'hindi','Last_Date_Time','','अंतिम तिथि का समय'),(31126,'hindi','last_ip','','पिछले आईपी'),(31127,'hindi','last_login_detail','','अंतिम लॉगिन विवरण'),(31128,'hindi','last_login_list','','अंतिम लॉगिन सूची'),(31129,'hindi','last_name','','अंतिम नाम'),(31130,'hindi','login','','लॉग इन'),(31131,'hindi','login_info','','लॉगिन जानकारी'),(31132,'hindi','logout','','लॉगआउट'),(31133,'hindi','privilege_type_list','','सदस्यता के प्रकार उपलब्ध की सूची'),(31134,'hindi','usrusr_msg_list','','भेजे गए संदेश के सदस्यों को सूची'),(31135,'hindi','member_type','','सदस्यता प्रकार'),(31136,'hindi','menu_name','','मेनू का नाम'),(31137,'hindi','message','','संदेश'),(31138,'hindi','mobile_no','','मोबाइल नंबर'),(31139,'hindi','modify_app_var','','संशोधित आवेदन चर'),(31140,'hindi','modify_privilege_type','','संशोधित विशेषाधिकार प्रकार'),(31141,'hindi','msg','','संदेश'),(31142,'hindi','msg_date','','संदेश दिनांक'),(31143,'hindi','msg_details','','संदेश का विवरण'),(31144,'hindi','msg_id','','Msg आईडी'),(31145,'hindi','msg_send','','संदेश सफलतापूर्वक भेजा गया!'),(31146,'hindi','msg_time','','संदेश समय'),(31147,'hindi','msg_title','','संदेश शीर्षक'),(31148,'hindi','personnel_list','','सदस्य सूची'),(31149,'hindi','privilege_type_name','','सदस्यता के प्रकार'),(31150,'hindi','usr_home_msg_archive_storage','','संदेश पुरालेख भंडारण /'),(31151,'hindi','usr_home_send_msg_to_adm/system','','प्रशासक संदेश'),(31152,'hindi','vender_id_v','','व्यापारी कोड'),(31153,'hindi','database_backup','','एक बैकअप क्लिक करें!'),(31154,'hindi','offline','','ऑफ़लाइन'),(31155,'hindi','online','','ऑनलाइन'),(31156,'hindi','order','','क्रम'),(31157,'hindi','outbox','','आउटबॉक्स'),(31158,'hindi','Out_Time','','बाहर समय'),(31159,'hindi','take_backup','','एक बैकअप क्लिक करें!'),(31160,'hindi','join_date','','पंजीकरण की तारीख'),(31161,'hindi','rec_add','','रिकॉर्ड सफलतापूर्वक जोड़!'),(31162,'hindi','rec_del','','रिकॉर्ड सफलतापूर्वक नष्ट कर दिया!'),(31163,'hindi','rec_update','','रिकॉर्ड सफलतापूर्वक नवीनीकृत!'),(31164,'hindi','remove','','निकालें'),(31165,'hindi','reply','','उत्तर'),(31166,'hindi','request_pass','','अनुरोध पासवर्ड'),(31167,'hindi','reset','','रीसेट'),(31168,'hindi','row','','पंक्तियाँ'),(31169,'hindi','login_details_sent','','पासवर्ड निम्नलिखित ईमेल खाते पर सफलतापूर्वक भेज दिया!'),(31170,'hindi','page','','पृष्ठ'),(31171,'hindi','password','','पासवर्ड'),(31172,'hindi','pass_reminder','','कूटशब्द की यादाश्ति'),(31173,'hindi','payment_gateway_list','','भुगतान गेटवे सूची'),(31174,'hindi','pay_metod_name','','भुगतान विधि नाम'),(31175,'hindi','perfecture_list','','प्रान्त की सूची'),(31176,'hindi','perfecture_name','','प्रान्त'),(31177,'hindi','personnel_info','','व्यक्तिगत जानकारी'),(31178,'hindi','per_details','','व्यक्तिगत विवरण'),(31179,'hindi','per_info','','व्यक्तिगत जानकारी'),(31180,'hindi','per_logo','','फ़ोटो'),(31181,'hindi','photo','','फ़ोटो'),(31182,'hindi','photograph','','फ़ोटो'),(31183,'hindi','posted_date','','पोस्ट दिनांक'),(31184,'hindi','privilege_type','','विशेषाधिकार प्रकार'),(31185,'hindi','process_id_v','','भुगतान प्रोसेसर आईडी'),(31186,'hindi','protocol','','प्रोटोकॉल'),(31187,'hindi','new_user','','पंजीकरण'),(31188,'hindi','save','','बचाना'),(31189,'hindi','search','','खोज'),(31190,'hindi','select','','चुनें'),(31191,'hindi','select_checked','','चयनित / चेक'),(31192,'hindi','select_type','','प्रकार का चयन करें'),(31193,'hindi','send','','भेजना'),(31194,'hindi','sent','','भेजा'),(31195,'hindi','sent_msg','','भेजे गए संदेश'),(31196,'hindi','set_value','','निर्धारित मूल्य'),(31197,'hindi','showfulllist','','दिखाएँ पूर्ण सूची'),(31198,'hindi','size','','आकार'),(31199,'hindi','sms_feature_on','','पर एसएमएस'),(31200,'hindi','sms_setting','','एसएमएस सेटिंग्स'),(31201,'hindi','sorry_incorrect_username','','माफ करना, गलत उपयोगकर्ता नाम या पासवर्ड'),(31202,'hindi','Sorry_Insufficient_Permission_You_need_to_login_first','','क्षमा करें! अपर्याप्त अनुमति! तुम पहले प्रवेश करने की जरूरत'),(31203,'hindi','sorry_ip_mismatch','','खेद आईपी मेल नहीं खाता'),(31204,'hindi','sort_msg','','Sort संदेश'),(31205,'hindi','sort_out_list','','Sort बाहर सूची'),(31206,'hindi','So_will_allow_IP_from_range','','इसलिए आईपी रेंज से की अनुमति देगा'),(31207,'hindi','state','','राज्य'),(31208,'hindi','status','','स्थिति'),(31209,'hindi','store','','दुकान'),(31210,'hindi','stored_msg','','संग्रहित संदेश'),(31211,'hindi','Stored_selectd_message','','संग्रहित संदेश चयनित'),(31212,'hindi','subject','','विषय'),(31213,'hindi','submit','','प्रस्तुत करना'),(31214,'hindi','sub_range','','उप रेंज'),(31215,'hindi','super_admin_home','','सुपर व्यवस्थापक होम'),(31216,'hindi','usr_home_manage_sent_messages','','भेजे गए संदेश'),(31928,'tamil','block',NULL,''),(31927,'tamil','blk_your_usrname_email',NULL,''),(31926,'tamil','blk_email',NULL,''),(31925,'tamil','black_list_members',NULL,''),(31924,'tamil','birth_date',NULL,''),(31923,'tamil','bannet_detail',NULL,''),(31249,'hindi','banner6','','Banner6'),(31922,'tamil','banner6',NULL,''),(31921,'tamil','banner5',NULL,''),(31248,'hindi','banner5','','Banner5'),(31920,'tamil','banner4',NULL,''),(31919,'tamil','banner3',NULL,''),(31247,'hindi','banner4','','Banner4'),(31918,'tamil','banner2',NULL,''),(31917,'tamil','banner1',NULL,''),(31246,'hindi','banner3','','Banner3'),(31916,'tamil','back_to_list_page',NULL,''),(31915,'tamil','back_to_home_page',NULL,''),(31914,'tamil','back_to_home',NULL,''),(31913,'tamil','back_to_admin_home',NULL,''),(31245,'hindi','banner2','','Banner2'),(31912,'tamil','backup_successfully',NULL,''),(31911,'tamil','back',NULL,''),(31910,'tamil','assign_privilege',NULL,''),(31909,'tamil','area_name',NULL,''),(31908,'tamil','area_list',NULL,''),(31244,'hindi','banner1','','Banner1'),(31907,'tamil','approve',NULL,''),(31906,'tamil','application_variable_list',NULL,''),(31250,'hindi','bannet_detail','','बैनर विवरण'),(31251,'hindi','birth_date','','जन्म दिनांक'),(31252,'hindi','black_list_members','','काले सूचीबद्ध सदस्य'),(31905,'tamil','api_conn_setting',NULL,''),(31903,'tamil','all',NULL,''),(31904,'tamil','all_user_news_letter_report',NULL,''),(31902,'tamil','adm_home_multilang_msg_list',NULL,''),(31900,'tamil','adm_home_manage_subscriber_list',NULL,''),(31901,'tamil','adm_home_manage_template_list',NULL,''),(31898,'tamil','add_to_another_lang',NULL,''),(31899,'tamil','admin_home',NULL,''),(31897,'tamil','add_product',NULL,''),(31896,'tamil','add_privilege_type',NULL,''),(31895,'tamil','Add_New_IP_Address',NULL,''),(31894,'tamil','add_new_cus_field',NULL,''),(31893,'tamil','add_new_app_var',NULL,''),(31892,'tamil','add_lang_msg',NULL,''),(31891,'tamil','add_lang',NULL,''),(31888,'tamil','add',NULL,''),(31889,'tamil','addnew',NULL,''),(31887,'tamil','active_members',NULL,''),(31890,'tamil','addnew_msg_id',NULL,''),(31885,'tamil','action',NULL,''),(31886,'tamil','activate',NULL,''),(31884,'tamil','121_123_11',NULL,''),(31883,'tamil','111_225',NULL,''),(31271,'hindi','edit','','संपादित करें'),(31272,'hindi','edit_payment_gateway','','संपादित भुगतान प्रवेश द्वार'),(31273,'hindi','email','','ईमेल'),(31274,'hindi','enable','','सक्षम'),(31275,'hindi','english','','अंग्रेजी'),(31276,'hindi','enter_email','','अपना ईमेल पता दर्ज करें'),(31277,'marathi','111_225',NULL,''),(31278,'marathi','121_123_11',NULL,''),(31279,'marathi','action',NULL,''),(31280,'marathi','activate',NULL,''),(31281,'marathi','active_members',NULL,''),(31282,'marathi','add',NULL,''),(31283,'marathi','addnew',NULL,''),(31284,'marathi','addnew_msg_id',NULL,''),(31285,'marathi','add_lang',NULL,''),(31286,'marathi','add_lang_msg',NULL,''),(31287,'marathi','add_new_app_var',NULL,''),(31288,'marathi','add_new_cus_field',NULL,''),(31289,'marathi','Add_New_IP_Address',NULL,''),(31290,'marathi','add_privilege_type',NULL,''),(31291,'marathi','add_product',NULL,''),(31292,'marathi','add_to_another_lang',NULL,''),(31293,'marathi','admin_home',NULL,''),(31294,'marathi','adm_home_manage_subscriber_list',NULL,''),(31295,'marathi','adm_home_manage_template_list',NULL,''),(31296,'marathi','adm_home_multilang_msg_list',NULL,''),(31297,'marathi','all',NULL,''),(31298,'marathi','all_user_news_letter_report',NULL,''),(31299,'marathi','api_conn_setting',NULL,''),(31300,'marathi','application_variable_list',NULL,''),(31301,'marathi','approve',NULL,''),(31302,'marathi','area_list',NULL,''),(31303,'marathi','area_name',NULL,''),(31304,'marathi','assign_privilege',NULL,''),(31305,'marathi','back',NULL,''),(31306,'marathi','backup_successfully',NULL,''),(31307,'marathi','back_to_admin_home',NULL,''),(31308,'marathi','back_to_home',NULL,''),(31309,'marathi','back_to_home_page',NULL,''),(31310,'marathi','back_to_list_page',NULL,''),(31311,'marathi','banner1',NULL,''),(31312,'marathi','banner2',NULL,''),(31313,'marathi','banner3',NULL,''),(31314,'marathi','banner4',NULL,''),(31315,'marathi','banner5',NULL,''),(31316,'marathi','banner6',NULL,''),(31317,'marathi','bannet_detail',NULL,''),(31318,'marathi','birth_date',NULL,''),(31319,'marathi','black_list_members',NULL,''),(31320,'marathi','blk_email',NULL,''),(31321,'marathi','blk_your_usrname_email',NULL,''),(31322,'marathi','block',NULL,''),(31323,'marathi','cancel',NULL,''),(31324,'marathi','category',NULL,''),(31325,'marathi','change_password',NULL,''),(31326,'marathi','change_picture',NULL,''),(31327,'marathi','check_box',NULL,''),(31328,'marathi','chk_availability',NULL,''),(31329,'marathi','chk_unchk',NULL,''),(31330,'marathi','choose_default',NULL,''),(31331,'marathi','choose_pre_lang_email_notification',NULL,''),(31332,'marathi','city',NULL,''),(31333,'marathi','city_list',NULL,''),(31334,'marathi','city_name',NULL,''),(31335,'marathi','click_here_to_login',NULL,''),(31336,'marathi','close',NULL,''),(31337,'marathi','colomn',NULL,''),(31338,'marathi','combo_box',NULL,''),(31339,'marathi','common_setting',NULL,''),(31340,'marathi','contact_info',NULL,''),(31341,'marathi','country',NULL,''),(31342,'marathi','create_date',NULL,''),(31343,'marathi','create_newsletter',NULL,''),(31344,'marathi','curr_pc_ip_add',NULL,''),(31345,'marathi','database_backup',NULL,''),(31346,'marathi','database_restore',NULL,''),(31347,'marathi','date',NULL,''),(31348,'marathi','dd_mm_yy',NULL,''),(31349,'marathi','deactive',NULL,''),(31350,'marathi','default',NULL,''),(31351,'marathi','default_flag',NULL,''),(31352,'marathi','default_smtp',NULL,''),(31353,'marathi','delete',NULL,''),(31354,'marathi','del_select',NULL,''),(31355,'marathi','desc',NULL,''),(31356,'marathi','display_name',NULL,''),(31357,'marathi','edit',NULL,''),(31358,'marathi','edit_news_letter',NULL,''),(31359,'marathi','edit_payment_gateway',NULL,''),(31360,'marathi','email',NULL,''),(31361,'marathi','email_msg',NULL,''),(31362,'marathi','enable',NULL,''),(31363,'marathi','english',NULL,''),(31364,'marathi','enter_email',NULL,''),(31365,'marathi','enter_login_nam',NULL,''),(31366,'marathi','field_name',NULL,''),(31367,'marathi','field_value',NULL,''),(31368,'marathi','first_name',NULL,''),(31369,'marathi','forget_pass',NULL,''),(31370,'marathi','forgot_password_email_message',NULL,''),(31371,'marathi','from',NULL,''),(31372,'marathi','full_name',NULL,''),(31373,'marathi','gateway',NULL,''),(31374,'marathi','gateway_url',NULL,''),(31375,'marathi','get',NULL,''),(31376,'marathi','go',NULL,''),(31377,'marathi','host_name',NULL,''),(31378,'marathi','html',NULL,''),(31379,'marathi','html_content',NULL,''),(31380,'marathi','image',NULL,''),(31381,'marathi','inactive_members',NULL,''),(31382,'marathi','inbox',NULL,''),(31383,'marathi','incoming_msg',NULL,''),(31384,'marathi','incorrect_email_login',NULL,''),(31385,'marathi','installment_f',NULL,''),(31386,'marathi','interest_mailing_list_wise_report',NULL,''),(31387,'marathi','In_Time',NULL,''),(31388,'marathi','ip',NULL,''),(31389,'marathi','ip_address_list',NULL,''),(31390,'marathi','ip_address_name',NULL,''),(31391,'marathi','ip_address_type',NULL,''),(31392,'marathi','IP_blocked_subject',NULL,''),(31393,'marathi','ip_home',NULL,''),(31394,'marathi','ip_mem_details',NULL,''),(31395,'marathi','join_date',NULL,''),(31396,'marathi','langlist',NULL,''),(31397,'marathi','language',NULL,''),(31398,'marathi','lang_list',NULL,''),(31399,'marathi','Last_Date_Time',NULL,''),(31400,'marathi','last_ip',NULL,''),(31401,'marathi','last_login_detail',NULL,''),(31402,'marathi','last_login_list',NULL,''),(31403,'marathi','last_name',NULL,''),(31404,'marathi','last_update',NULL,''),(31405,'marathi','login',NULL,''),(31406,'marathi','login_details_sent',NULL,''),(31407,'marathi','login_id',NULL,''),(31408,'marathi','login_info',NULL,''),(31409,'marathi','logout',NULL,''),(31410,'marathi','mail',NULL,''),(31411,'marathi','mailing_list_name',NULL,''),(31412,'marathi','mail_list',NULL,''),(31413,'marathi','members',NULL,''),(31414,'marathi','member_type',NULL,''),(31415,'marathi','menu_name',NULL,''),(31416,'marathi','message',NULL,''),(31417,'marathi','message_code',NULL,''),(31418,'marathi','mobile_no',NULL,''),(31419,'marathi','modify_app_var',NULL,''),(31420,'marathi','modify_privilege_type',NULL,''),(31421,'marathi','msg',NULL,''),(31422,'marathi','msg_date',NULL,''),(31423,'marathi','msg_details',NULL,''),(31424,'marathi','msg_id',NULL,''),(31425,'marathi','msg_send',NULL,''),(31426,'marathi','msg_time',NULL,''),(31427,'marathi','msg_title',NULL,''),(31428,'marathi','name',NULL,''),(31429,'marathi','new/unread_msg',NULL,''),(31430,'marathi','news_letter',NULL,''),(31431,'marathi','news_letters',NULL,''),(31432,'marathi','news_letter_title',NULL,''),(31433,'marathi','news_letter_wise_report',NULL,''),(31434,'marathi','new_password',NULL,''),(31435,'marathi','new_user',NULL,''),(31436,'marathi','new_usr_register',NULL,''),(31437,'marathi','no',NULL,''),(31438,'marathi','non_members',NULL,''),(31439,'marathi','note',NULL,''),(31440,'marathi','no_rec_found',NULL,''),(31441,'marathi','offline',NULL,''),(31442,'marathi','on/of_sms_feature',NULL,''),(31443,'marathi','online',NULL,''),(31444,'marathi','order',NULL,''),(31445,'marathi','orderno',NULL,''),(31446,'marathi','other',NULL,''),(31447,'marathi','outbox',NULL,''),(31448,'marathi','Out_Time',NULL,''),(31449,'marathi','page',NULL,''),(31450,'marathi','password',NULL,''),(31451,'marathi','pass_reminder',NULL,''),(31452,'marathi','payment_gateway_list',NULL,''),(31453,'marathi','pay_metod_name',NULL,''),(31454,'marathi','perfecture_list',NULL,''),(31455,'marathi','perfecture_name',NULL,''),(31456,'marathi','personnel_info',NULL,''),(31457,'marathi','personnel_list',NULL,''),(31458,'marathi','per_details',NULL,''),(31459,'marathi','per_info',NULL,''),(31460,'marathi','per_logo',NULL,''),(31461,'marathi','photo',NULL,''),(31462,'marathi','photograph',NULL,''),(31463,'marathi','posted_date',NULL,''),(31464,'marathi','priority',NULL,''),(31465,'marathi','privilege_type',NULL,''),(31466,'marathi','privilege_type_list',NULL,''),(31467,'marathi','privilege_type_name',NULL,''),(31468,'marathi','pri_language',NULL,''),(31469,'marathi','process_id_v',NULL,''),(31470,'marathi','protocol',NULL,''),(31471,'marathi','radio_box',NULL,''),(31472,'marathi','radio_button',NULL,''),(31473,'marathi','rec_add',NULL,''),(31474,'marathi','rec_del',NULL,''),(31475,'marathi','rec_update',NULL,''),(31476,'marathi','remove',NULL,''),(31477,'marathi','reply',NULL,''),(31478,'marathi','request_pass',NULL,''),(31479,'marathi','reset',NULL,''),(31480,'marathi','reset_pass',NULL,''),(31481,'marathi','restore_successfully',NULL,''),(31482,'marathi','row',NULL,''),(31483,'marathi','save',NULL,''),(31484,'marathi','search',NULL,''),(31485,'marathi','sec_language',NULL,''),(31486,'marathi','select',NULL,''),(31487,'marathi','select_checked',NULL,''),(31488,'marathi','select_type',NULL,''),(31489,'marathi','send',NULL,''),(31490,'marathi','send_date',NULL,''),(31491,'marathi','send_time',NULL,''),(31492,'marathi','sent',NULL,''),(31493,'marathi','sent_msg',NULL,''),(31494,'marathi','set_value',NULL,''),(31495,'marathi','showfulllist',NULL,''),(31496,'marathi','show_on_off',NULL,''),(31497,'marathi','signup_email_message',NULL,''),(31498,'marathi','single',NULL,''),(31499,'marathi','size',NULL,''),(31500,'marathi','sms_feature_on',NULL,''),(31501,'marathi','sms_setting',NULL,''),(31502,'marathi','SMTP_Details',NULL,''),(31503,'marathi','sorry_incorrect_username',NULL,''),(31504,'marathi','Sorry_Insufficient_Permission_You_need_to_login_first',NULL,''),(31505,'marathi','sorry_ip_mismatch',NULL,''),(31506,'marathi','sort_asc',NULL,''),(31507,'marathi','sort_by',NULL,''),(31508,'marathi','sort_desc',NULL,''),(31509,'marathi','sort_msg',NULL,''),(31510,'marathi','sort_out_list',NULL,''),(31511,'marathi','So_will_allow_IP_from_range',NULL,''),(31512,'marathi','spammer',NULL,''),(31513,'marathi','spammer_report',NULL,''),(31514,'marathi','state',NULL,''),(31515,'marathi','status',NULL,''),(31516,'marathi','store',NULL,''),(31517,'marathi','stored_msg',NULL,''),(31518,'marathi','Stored_selectd_message',NULL,''),(31519,'marathi','string_concat',NULL,''),(31520,'marathi','student_list',NULL,''),(31521,'marathi','student_name',NULL,''),(31522,'marathi','subject',NULL,''),(31523,'marathi','subject_type_list',NULL,''),(31524,'marathi','submit',NULL,''),(31525,'marathi','sub_range',NULL,''),(31526,'marathi','super_admin_home',NULL,''),(31527,'marathi','sys_date_time',NULL,''),(31528,'marathi','take_backup',NULL,''),(31529,'marathi','targetlist',NULL,''),(31530,'marathi','target_interested_category',NULL,''),(31531,'marathi','template',NULL,''),(31532,'marathi','template_detail',NULL,''),(31533,'marathi','template_list',NULL,''),(31534,'marathi','template_title',NULL,''),(31535,'marathi','term_cond',NULL,''),(31536,'marathi','text',NULL,''),(31537,'marathi','text_area',NULL,''),(31538,'marathi','thank_u_for_ur_interest',NULL,''),(31539,'marathi','to',NULL,''),(31540,'marathi','total_news_letter',NULL,''),(31541,'marathi','total_subscriber',NULL,''),(31542,'marathi','total_target_list',NULL,''),(31543,'marathi','total_time',NULL,''),(31544,'marathi','total_unsubscriber',NULL,''),(31545,'marathi','total_user',NULL,''),(31546,'marathi','to_date',NULL,''),(31547,'marathi','to_person',NULL,''),(31548,'marathi','transe_type',NULL,''),(31549,'marathi','translate',NULL,''),(31550,'marathi','type',NULL,''),(31551,'marathi','unblock_email',NULL,''),(31552,'marathi','upadmin_details',NULL,''),(31553,'marathi','update_send_by_sms',NULL,''),(31554,'marathi','upgrade_membership',NULL,''),(31555,'marathi','up_personnel_info',NULL,''),(31556,'marathi','username',NULL,''),(31557,'marathi','user_home',NULL,''),(31558,'marathi','user_id',NULL,''),(31559,'marathi','user_name',NULL,''),(31560,'marathi','usrusr_msg_list',NULL,''),(31561,'marathi','usr_activated',NULL,''),(31562,'marathi','usr_home_inbox',NULL,''),(31563,'marathi','usr_home_manage_sent_messages',NULL,''),(31564,'marathi','usr_home_msg_archive_storage',NULL,''),(31565,'marathi','usr_home_outbox',NULL,''),(31566,'marathi','usr_home_send_msg_to_adm/system',NULL,''),(31567,'marathi','usr_welcome_to_mem_sec',NULL,''),(31568,'marathi','value',NULL,''),(31569,'marathi','vender_id_v',NULL,''),(31570,'marathi','view',NULL,''),(31571,'marathi','view_demo_page',NULL,''),(31572,'marathi','waiting',NULL,''),(31573,'marathi','website_description',NULL,''),(31574,'marathi','website_keywords',NULL,''),(31575,'marathi','website_title',NULL,''),(31576,'marathi','with_select',NULL,''),(31577,'marathi','with_sel_send_msg',NULL,''),(31578,'marathi','yes',NULL,''),(31579,'marathi','zip_code',NULL,''),(31580,'kannada','111_225',NULL,''),(31581,'kannada','121_123_11',NULL,''),(31582,'kannada','action',NULL,''),(31583,'kannada','activate',NULL,''),(31584,'kannada','active_members',NULL,''),(31585,'kannada','add',NULL,''),(31586,'kannada','addnew',NULL,''),(31587,'kannada','addnew_msg_id',NULL,''),(31588,'kannada','add_lang',NULL,''),(31589,'kannada','add_lang_msg',NULL,''),(31590,'kannada','add_new_app_var',NULL,''),(31591,'kannada','add_new_cus_field',NULL,''),(31592,'kannada','Add_New_IP_Address',NULL,''),(31593,'kannada','add_privilege_type',NULL,''),(31594,'kannada','add_product',NULL,''),(31595,'kannada','add_to_another_lang',NULL,''),(31596,'kannada','admin_home',NULL,''),(31597,'kannada','adm_home_manage_subscriber_list',NULL,''),(31598,'kannada','adm_home_manage_template_list',NULL,''),(31599,'kannada','adm_home_multilang_msg_list',NULL,''),(31600,'kannada','all',NULL,''),(31601,'kannada','all_user_news_letter_report',NULL,''),(31602,'kannada','api_conn_setting',NULL,''),(31603,'kannada','application_variable_list',NULL,''),(31604,'kannada','approve',NULL,''),(31605,'kannada','area_list',NULL,''),(31606,'kannada','area_name',NULL,''),(31607,'kannada','assign_privilege',NULL,''),(31608,'kannada','back',NULL,''),(31609,'kannada','backup_successfully',NULL,''),(31610,'kannada','back_to_admin_home',NULL,''),(31611,'kannada','back_to_home',NULL,''),(31612,'kannada','back_to_home_page',NULL,''),(31613,'kannada','back_to_list_page',NULL,''),(31614,'kannada','banner1',NULL,''),(31615,'kannada','banner2',NULL,''),(31616,'kannada','banner3',NULL,''),(31617,'kannada','banner4',NULL,''),(31618,'kannada','banner5',NULL,''),(31619,'kannada','banner6',NULL,''),(31620,'kannada','bannet_detail',NULL,''),(31621,'kannada','birth_date',NULL,''),(31622,'kannada','black_list_members',NULL,''),(31623,'kannada','blk_email',NULL,''),(31624,'kannada','blk_your_usrname_email',NULL,''),(31625,'kannada','block',NULL,''),(31626,'kannada','cancel',NULL,''),(31627,'kannada','category',NULL,''),(31628,'kannada','change_password',NULL,''),(31629,'kannada','change_picture',NULL,''),(31630,'kannada','check_box',NULL,''),(31631,'kannada','chk_availability',NULL,''),(31632,'kannada','chk_unchk',NULL,''),(31633,'kannada','choose_default',NULL,''),(31634,'kannada','choose_pre_lang_email_notification',NULL,''),(31635,'kannada','city',NULL,''),(31636,'kannada','city_list',NULL,''),(31637,'kannada','city_name',NULL,''),(31638,'kannada','click_here_to_login',NULL,''),(31639,'kannada','close',NULL,''),(31640,'kannada','colomn',NULL,''),(31641,'kannada','combo_box',NULL,''),(31642,'kannada','common_setting',NULL,''),(31643,'kannada','contact_info',NULL,''),(31644,'kannada','country',NULL,''),(31645,'kannada','create_date',NULL,''),(31646,'kannada','create_newsletter',NULL,''),(31647,'kannada','curr_pc_ip_add',NULL,''),(31648,'kannada','database_backup',NULL,''),(31649,'kannada','database_restore',NULL,''),(31650,'kannada','date',NULL,''),(31651,'kannada','dd_mm_yy',NULL,''),(31652,'kannada','deactive',NULL,''),(31653,'kannada','default',NULL,''),(31654,'kannada','default_flag',NULL,''),(31655,'kannada','default_smtp',NULL,''),(31656,'kannada','delete',NULL,''),(31657,'kannada','del_select',NULL,''),(31658,'kannada','desc',NULL,''),(31659,'kannada','display_name',NULL,''),(31660,'kannada','edit',NULL,''),(31661,'kannada','edit_news_letter',NULL,''),(31662,'kannada','edit_payment_gateway',NULL,''),(31663,'kannada','email',NULL,''),(31664,'kannada','email_msg',NULL,''),(31665,'kannada','enable',NULL,''),(31666,'kannada','english',NULL,''),(31667,'kannada','enter_email',NULL,''),(31668,'kannada','enter_login_nam',NULL,''),(31669,'kannada','field_name',NULL,''),(31670,'kannada','field_value',NULL,''),(31671,'kannada','first_name',NULL,''),(31672,'kannada','forget_pass',NULL,''),(31673,'kannada','forgot_password_email_message',NULL,''),(31674,'kannada','from',NULL,''),(31675,'kannada','full_name',NULL,''),(31676,'kannada','gateway',NULL,''),(31677,'kannada','gateway_url',NULL,''),(31678,'kannada','get',NULL,''),(31679,'kannada','go',NULL,''),(31680,'kannada','host_name',NULL,''),(31681,'kannada','html',NULL,''),(31682,'kannada','html_content',NULL,''),(31683,'kannada','image',NULL,''),(31684,'kannada','inactive_members',NULL,''),(31685,'kannada','inbox',NULL,''),(31686,'kannada','incoming_msg',NULL,''),(31687,'kannada','incorrect_email_login',NULL,''),(31688,'kannada','installment_f',NULL,''),(31689,'kannada','interest_mailing_list_wise_report',NULL,''),(31690,'kannada','In_Time',NULL,''),(31691,'kannada','ip',NULL,''),(31692,'kannada','ip_address_list',NULL,''),(31693,'kannada','ip_address_name',NULL,''),(31694,'kannada','ip_address_type',NULL,''),(31695,'kannada','IP_blocked_subject',NULL,''),(31696,'kannada','ip_home',NULL,''),(31697,'kannada','ip_mem_details',NULL,''),(31698,'kannada','join_date',NULL,''),(31699,'kannada','langlist',NULL,''),(31700,'kannada','language',NULL,''),(31701,'kannada','lang_list',NULL,''),(31702,'kannada','Last_Date_Time',NULL,''),(31703,'kannada','last_ip',NULL,''),(31704,'kannada','last_login_detail',NULL,''),(31705,'kannada','last_login_list',NULL,''),(31706,'kannada','last_name',NULL,''),(31707,'kannada','last_update',NULL,''),(31708,'kannada','login',NULL,''),(31709,'kannada','login_details_sent',NULL,''),(31710,'kannada','login_id',NULL,''),(31711,'kannada','login_info',NULL,''),(31712,'kannada','logout',NULL,''),(31713,'kannada','mail',NULL,''),(31714,'kannada','mailing_list_name',NULL,''),(31715,'kannada','mail_list',NULL,''),(31716,'kannada','members',NULL,''),(31717,'kannada','member_type',NULL,''),(31718,'kannada','menu_name',NULL,''),(31719,'kannada','message',NULL,''),(31720,'kannada','message_code',NULL,''),(31721,'kannada','mobile_no',NULL,''),(31722,'kannada','modify_app_var',NULL,''),(31723,'kannada','modify_privilege_type',NULL,''),(31724,'kannada','msg',NULL,''),(31725,'kannada','msg_date',NULL,''),(31726,'kannada','msg_details',NULL,''),(31727,'kannada','msg_id',NULL,''),(31728,'kannada','msg_send',NULL,''),(31729,'kannada','msg_time',NULL,''),(31730,'kannada','msg_title',NULL,''),(31731,'kannada','name',NULL,''),(31732,'kannada','new/unread_msg',NULL,''),(31733,'kannada','news_letter',NULL,''),(31734,'kannada','news_letters',NULL,''),(31735,'kannada','news_letter_title',NULL,''),(31736,'kannada','news_letter_wise_report',NULL,''),(31737,'kannada','new_password',NULL,''),(31738,'kannada','new_user',NULL,''),(31739,'kannada','new_usr_register',NULL,''),(31740,'kannada','no',NULL,''),(31741,'kannada','non_members',NULL,''),(31742,'kannada','note',NULL,''),(31743,'kannada','no_rec_found',NULL,''),(31744,'kannada','offline',NULL,''),(31745,'kannada','on/of_sms_feature',NULL,''),(31746,'kannada','online',NULL,''),(31747,'kannada','order',NULL,''),(31748,'kannada','orderno',NULL,''),(31749,'kannada','other',NULL,''),(31750,'kannada','outbox',NULL,''),(31751,'kannada','Out_Time',NULL,''),(31752,'kannada','page',NULL,''),(31753,'kannada','password',NULL,''),(31754,'kannada','pass_reminder',NULL,''),(31755,'kannada','payment_gateway_list',NULL,''),(31756,'kannada','pay_metod_name',NULL,''),(31757,'kannada','perfecture_list',NULL,''),(31758,'kannada','perfecture_name',NULL,''),(31759,'kannada','personnel_info',NULL,''),(31760,'kannada','personnel_list',NULL,''),(31761,'kannada','per_details',NULL,''),(31762,'kannada','per_info',NULL,''),(31763,'kannada','per_logo',NULL,''),(31764,'kannada','photo',NULL,''),(31765,'kannada','photograph',NULL,''),(31766,'kannada','posted_date',NULL,''),(31767,'kannada','priority',NULL,''),(31768,'kannada','privilege_type',NULL,''),(31769,'kannada','privilege_type_list',NULL,''),(31770,'kannada','privilege_type_name',NULL,''),(31771,'kannada','pri_language',NULL,''),(31772,'kannada','process_id_v',NULL,''),(31773,'kannada','protocol',NULL,''),(31774,'kannada','radio_box',NULL,''),(31775,'kannada','radio_button',NULL,''),(31776,'kannada','rec_add',NULL,''),(31777,'kannada','rec_del',NULL,''),(31778,'kannada','rec_update',NULL,''),(31779,'kannada','remove',NULL,''),(31780,'kannada','reply',NULL,''),(31781,'kannada','request_pass',NULL,''),(31782,'kannada','reset',NULL,''),(31783,'kannada','reset_pass',NULL,''),(31784,'kannada','restore_successfully',NULL,''),(31785,'kannada','row',NULL,''),(31786,'kannada','save',NULL,''),(31787,'kannada','search',NULL,''),(31788,'kannada','sec_language',NULL,''),(31789,'kannada','select',NULL,''),(31790,'kannada','select_checked',NULL,''),(31791,'kannada','select_type',NULL,''),(31792,'kannada','send',NULL,''),(31793,'kannada','send_date',NULL,''),(31794,'kannada','send_time',NULL,''),(31795,'kannada','sent',NULL,''),(31796,'kannada','sent_msg',NULL,''),(31797,'kannada','set_value',NULL,''),(31798,'kannada','showfulllist',NULL,''),(31799,'kannada','show_on_off',NULL,''),(31800,'kannada','signup_email_message',NULL,''),(31801,'kannada','single',NULL,''),(31802,'kannada','size',NULL,''),(31803,'kannada','sms_feature_on',NULL,''),(31804,'kannada','sms_setting',NULL,''),(31805,'kannada','SMTP_Details',NULL,''),(31806,'kannada','sorry_incorrect_username',NULL,''),(31807,'kannada','Sorry_Insufficient_Permission_You_need_to_login_first',NULL,''),(31808,'kannada','sorry_ip_mismatch',NULL,''),(31809,'kannada','sort_asc',NULL,''),(31810,'kannada','sort_by',NULL,''),(31811,'kannada','sort_desc',NULL,''),(31812,'kannada','sort_msg',NULL,''),(31813,'kannada','sort_out_list',NULL,''),(31814,'kannada','So_will_allow_IP_from_range',NULL,''),(31815,'kannada','spammer',NULL,''),(31816,'kannada','spammer_report',NULL,''),(31817,'kannada','state',NULL,''),(31818,'kannada','status',NULL,''),(31819,'kannada','store',NULL,''),(31820,'kannada','stored_msg',NULL,''),(31821,'kannada','Stored_selectd_message',NULL,''),(31822,'kannada','string_concat',NULL,''),(31823,'kannada','student_list',NULL,''),(31824,'kannada','student_name',NULL,''),(31825,'kannada','subject',NULL,''),(31826,'kannada','subject_type_list',NULL,''),(31827,'kannada','submit',NULL,''),(31828,'kannada','sub_range',NULL,''),(31829,'kannada','super_admin_home',NULL,''),(31830,'kannada','sys_date_time',NULL,''),(31831,'kannada','take_backup',NULL,''),(31832,'kannada','targetlist',NULL,''),(31833,'kannada','target_interested_category',NULL,''),(31834,'kannada','template',NULL,''),(31835,'kannada','template_detail',NULL,''),(31836,'kannada','template_list',NULL,''),(31837,'kannada','template_title',NULL,''),(31838,'kannada','term_cond',NULL,''),(31839,'kannada','text',NULL,''),(31840,'kannada','text_area',NULL,''),(31841,'kannada','thank_u_for_ur_interest',NULL,''),(31842,'kannada','to',NULL,''),(31843,'kannada','total_news_letter',NULL,''),(31844,'kannada','total_subscriber',NULL,''),(31845,'kannada','total_target_list',NULL,''),(31846,'kannada','total_time',NULL,''),(31847,'kannada','total_unsubscriber',NULL,''),(31848,'kannada','total_user',NULL,''),(31849,'kannada','to_date',NULL,''),(31850,'kannada','to_person',NULL,''),(31851,'kannada','transe_type',NULL,''),(31852,'kannada','translate',NULL,''),(31853,'kannada','type',NULL,''),(31854,'kannada','unblock_email',NULL,''),(31855,'kannada','upadmin_details',NULL,''),(31856,'kannada','update_send_by_sms',NULL,''),(31857,'kannada','upgrade_membership',NULL,''),(31858,'kannada','up_personnel_info',NULL,''),(31859,'kannada','username',NULL,''),(31860,'kannada','user_home',NULL,''),(31861,'kannada','user_id',NULL,''),(31862,'kannada','user_name',NULL,''),(31863,'kannada','usrusr_msg_list',NULL,''),(31864,'kannada','usr_activated',NULL,''),(31865,'kannada','usr_home_inbox',NULL,''),(31866,'kannada','usr_home_manage_sent_messages',NULL,''),(31867,'kannada','usr_home_msg_archive_storage',NULL,''),(31868,'kannada','usr_home_outbox',NULL,''),(31869,'kannada','usr_home_send_msg_to_adm/system',NULL,''),(31870,'kannada','usr_welcome_to_mem_sec',NULL,''),(31871,'kannada','value',NULL,''),(31872,'kannada','vender_id_v',NULL,''),(31873,'kannada','view',NULL,''),(31874,'kannada','view_demo_page',NULL,''),(31875,'kannada','waiting',NULL,''),(31876,'kannada','website_description',NULL,''),(31877,'kannada','website_keywords',NULL,''),(31878,'kannada','website_title',NULL,''),(31879,'kannada','with_select',NULL,''),(31880,'kannada','with_sel_send_msg',NULL,''),(31881,'kannada','yes',NULL,''),(31882,'kannada','zip_code',NULL,''),(31929,'tamil','cancel',NULL,''),(31930,'tamil','category',NULL,''),(31931,'tamil','change_password',NULL,''),(31932,'tamil','change_picture',NULL,''),(31933,'tamil','check_box',NULL,''),(31934,'tamil','chk_availability',NULL,''),(31935,'tamil','chk_unchk',NULL,''),(31936,'tamil','choose_default',NULL,''),(31937,'tamil','choose_pre_lang_email_notification',NULL,''),(31938,'tamil','city',NULL,''),(31939,'tamil','city_list',NULL,''),(31940,'tamil','city_name',NULL,''),(31941,'tamil','click_here_to_login',NULL,''),(31942,'tamil','close',NULL,''),(31943,'tamil','colomn',NULL,''),(31944,'tamil','combo_box',NULL,''),(31945,'tamil','common_setting',NULL,''),(31946,'tamil','contact_info',NULL,''),(31947,'tamil','country',NULL,''),(31948,'tamil','create_date',NULL,''),(31949,'tamil','create_newsletter',NULL,''),(31950,'tamil','curr_pc_ip_add',NULL,''),(31951,'tamil','database_backup',NULL,''),(31952,'tamil','database_restore',NULL,''),(31953,'tamil','date',NULL,''),(31954,'tamil','dd_mm_yy',NULL,''),(31955,'tamil','deactive',NULL,''),(31956,'tamil','default',NULL,''),(31957,'tamil','default_flag',NULL,''),(31958,'tamil','default_smtp',NULL,''),(31959,'tamil','delete',NULL,''),(31960,'tamil','del_select',NULL,''),(31961,'tamil','desc',NULL,''),(31962,'tamil','display_name',NULL,''),(31963,'tamil','edit',NULL,''),(31964,'tamil','edit_news_letter',NULL,''),(31965,'tamil','edit_payment_gateway',NULL,''),(31966,'tamil','email',NULL,''),(31967,'tamil','email_msg',NULL,''),(31968,'tamil','enable',NULL,''),(31969,'tamil','english',NULL,''),(31970,'tamil','enter_email',NULL,''),(31971,'tamil','enter_login_nam',NULL,''),(31972,'tamil','field_name',NULL,''),(31973,'tamil','field_value',NULL,''),(31974,'tamil','first_name',NULL,''),(31975,'tamil','forget_pass',NULL,''),(31976,'tamil','forgot_password_email_message',NULL,''),(31977,'tamil','from',NULL,''),(31978,'tamil','full_name',NULL,''),(31979,'tamil','gateway',NULL,''),(31980,'tamil','gateway_url',NULL,''),(31981,'tamil','get',NULL,''),(31982,'tamil','go',NULL,''),(31983,'tamil','host_name',NULL,''),(31984,'tamil','html',NULL,''),(31985,'tamil','html_content',NULL,''),(31986,'tamil','image',NULL,''),(31987,'tamil','inactive_members',NULL,''),(31988,'tamil','inbox',NULL,''),(31989,'tamil','incoming_msg',NULL,''),(31990,'tamil','incorrect_email_login',NULL,''),(31991,'tamil','installment_f',NULL,''),(31992,'tamil','interest_mailing_list_wise_report',NULL,''),(31993,'tamil','In_Time',NULL,''),(31994,'tamil','ip',NULL,''),(31995,'tamil','ip_address_list',NULL,''),(31996,'tamil','ip_address_name',NULL,''),(31997,'tamil','ip_address_type',NULL,''),(31998,'tamil','IP_blocked_subject',NULL,''),(31999,'tamil','ip_home',NULL,''),(32000,'tamil','ip_mem_details',NULL,''),(32001,'tamil','join_date',NULL,''),(32002,'tamil','langlist',NULL,''),(32003,'tamil','language',NULL,''),(32004,'tamil','lang_list',NULL,''),(32005,'tamil','Last_Date_Time',NULL,''),(32006,'tamil','last_ip',NULL,''),(32007,'tamil','last_login_detail',NULL,''),(32008,'tamil','last_login_list',NULL,''),(32009,'tamil','last_name',NULL,''),(32010,'tamil','last_update',NULL,''),(32011,'tamil','login',NULL,''),(32012,'tamil','login_details_sent',NULL,''),(32013,'tamil','login_id',NULL,''),(32014,'tamil','login_info',NULL,''),(32015,'tamil','logout',NULL,''),(32016,'tamil','mail',NULL,''),(32017,'tamil','mailing_list_name',NULL,''),(32018,'tamil','mail_list',NULL,''),(32019,'tamil','members',NULL,''),(32020,'tamil','member_type',NULL,''),(32021,'tamil','menu_name',NULL,''),(32022,'tamil','message',NULL,''),(32023,'tamil','message_code',NULL,''),(32024,'tamil','mobile_no',NULL,''),(32025,'tamil','modify_app_var',NULL,''),(32026,'tamil','modify_privilege_type',NULL,''),(32027,'tamil','msg',NULL,''),(32028,'tamil','msg_date',NULL,''),(32029,'tamil','msg_details',NULL,''),(32030,'tamil','msg_id',NULL,''),(32031,'tamil','msg_send',NULL,''),(32032,'tamil','msg_time',NULL,''),(32033,'tamil','msg_title',NULL,''),(32034,'tamil','name',NULL,''),(32035,'tamil','new/unread_msg',NULL,''),(32036,'tamil','news_letter',NULL,''),(32037,'tamil','news_letters',NULL,''),(32038,'tamil','news_letter_title',NULL,''),(32039,'tamil','news_letter_wise_report',NULL,''),(32040,'tamil','new_password',NULL,''),(32041,'tamil','new_user',NULL,''),(32042,'tamil','new_usr_register',NULL,''),(32043,'tamil','no',NULL,''),(32044,'tamil','non_members',NULL,''),(32045,'tamil','note',NULL,''),(32046,'tamil','no_rec_found',NULL,''),(32047,'tamil','offline',NULL,''),(32048,'tamil','on/of_sms_feature',NULL,''),(32049,'tamil','online',NULL,''),(32050,'tamil','order',NULL,''),(32051,'tamil','orderno',NULL,''),(32052,'tamil','other',NULL,''),(32053,'tamil','outbox',NULL,''),(32054,'tamil','Out_Time',NULL,''),(32055,'tamil','page',NULL,''),(32056,'tamil','password',NULL,''),(32057,'tamil','pass_reminder',NULL,''),(32058,'tamil','payment_gateway_list',NULL,''),(32059,'tamil','pay_metod_name',NULL,''),(32060,'tamil','perfecture_list',NULL,''),(32061,'tamil','perfecture_name',NULL,''),(32062,'tamil','personnel_info',NULL,''),(32063,'tamil','personnel_list',NULL,''),(32064,'tamil','per_details',NULL,''),(32065,'tamil','per_info',NULL,''),(32066,'tamil','per_logo',NULL,''),(32067,'tamil','photo',NULL,''),(32068,'tamil','photograph',NULL,''),(32069,'tamil','posted_date',NULL,''),(32070,'tamil','priority',NULL,''),(32071,'tamil','privilege_type',NULL,''),(32072,'tamil','privilege_type_list',NULL,''),(32073,'tamil','privilege_type_name',NULL,''),(32074,'tamil','pri_language',NULL,''),(32075,'tamil','process_id_v',NULL,''),(32076,'tamil','protocol',NULL,''),(32077,'tamil','radio_box',NULL,''),(32078,'tamil','radio_button',NULL,''),(32079,'tamil','rec_add',NULL,''),(32080,'tamil','rec_del',NULL,''),(32081,'tamil','rec_update',NULL,''),(32082,'tamil','remove',NULL,''),(32083,'tamil','reply',NULL,''),(32084,'tamil','request_pass',NULL,''),(32085,'tamil','reset',NULL,''),(32086,'tamil','reset_pass',NULL,''),(32087,'tamil','restore_successfully',NULL,''),(32088,'tamil','row',NULL,''),(32089,'tamil','save',NULL,''),(32090,'tamil','search',NULL,''),(32091,'tamil','sec_language',NULL,''),(32092,'tamil','select',NULL,''),(32093,'tamil','select_checked',NULL,''),(32094,'tamil','select_type',NULL,''),(32095,'tamil','send',NULL,''),(32096,'tamil','send_date',NULL,''),(32097,'tamil','send_time',NULL,''),(32098,'tamil','sent',NULL,''),(32099,'tamil','sent_msg',NULL,''),(32100,'tamil','set_value',NULL,''),(32101,'tamil','showfulllist',NULL,''),(32102,'tamil','show_on_off',NULL,''),(32103,'tamil','signup_email_message',NULL,''),(32104,'tamil','single',NULL,''),(32105,'tamil','size',NULL,''),(32106,'tamil','sms_feature_on',NULL,''),(32107,'tamil','sms_setting',NULL,''),(32108,'tamil','SMTP_Details',NULL,''),(32109,'tamil','sorry_incorrect_username',NULL,''),(32110,'tamil','Sorry_Insufficient_Permission_You_need_to_login_first',NULL,''),(32111,'tamil','sorry_ip_mismatch',NULL,''),(32112,'tamil','sort_asc',NULL,''),(32113,'tamil','sort_by',NULL,''),(32114,'tamil','sort_desc',NULL,''),(32115,'tamil','sort_msg',NULL,''),(32116,'tamil','sort_out_list',NULL,''),(32117,'tamil','So_will_allow_IP_from_range',NULL,''),(32118,'tamil','spammer',NULL,''),(32119,'tamil','spammer_report',NULL,''),(32120,'tamil','state',NULL,''),(32121,'tamil','status',NULL,''),(32122,'tamil','store',NULL,''),(32123,'tamil','stored_msg',NULL,''),(32124,'tamil','Stored_selectd_message',NULL,''),(32125,'tamil','string_concat',NULL,''),(32126,'tamil','student_list',NULL,''),(32127,'tamil','student_name',NULL,''),(32128,'tamil','subject',NULL,''),(32129,'tamil','subject_type_list',NULL,''),(32130,'tamil','submit',NULL,''),(32131,'tamil','sub_range',NULL,''),(32132,'tamil','super_admin_home',NULL,''),(32133,'tamil','sys_date_time',NULL,''),(32134,'tamil','take_backup',NULL,''),(32135,'tamil','targetlist',NULL,''),(32136,'tamil','target_interested_category',NULL,''),(32137,'tamil','template',NULL,''),(32138,'tamil','template_detail',NULL,''),(32139,'tamil','template_list',NULL,''),(32140,'tamil','template_title',NULL,''),(32141,'tamil','term_cond',NULL,''),(32142,'tamil','text',NULL,''),(32143,'tamil','text_area',NULL,''),(32144,'tamil','thank_u_for_ur_interest',NULL,''),(32145,'tamil','to',NULL,''),(32146,'tamil','total_news_letter',NULL,''),(32147,'tamil','total_subscriber',NULL,''),(32148,'tamil','total_target_list',NULL,''),(32149,'tamil','total_time',NULL,''),(32150,'tamil','total_unsubscriber',NULL,''),(32151,'tamil','total_user',NULL,''),(32152,'tamil','to_date',NULL,''),(32153,'tamil','to_person',NULL,''),(32154,'tamil','transe_type',NULL,''),(32155,'tamil','translate',NULL,''),(32156,'tamil','type',NULL,''),(32157,'tamil','unblock_email',NULL,''),(32158,'tamil','upadmin_details',NULL,''),(32159,'tamil','update_send_by_sms',NULL,''),(32160,'tamil','upgrade_membership',NULL,''),(32161,'tamil','up_personnel_info',NULL,''),(32162,'tamil','username',NULL,''),(32163,'tamil','user_home',NULL,''),(32164,'tamil','user_id',NULL,''),(32165,'tamil','user_name',NULL,''),(32166,'tamil','usrusr_msg_list',NULL,''),(32167,'tamil','usr_activated',NULL,''),(32168,'tamil','usr_home_inbox',NULL,''),(32169,'tamil','usr_home_manage_sent_messages',NULL,''),(32170,'tamil','usr_home_msg_archive_storage',NULL,''),(32171,'tamil','usr_home_outbox',NULL,''),(32172,'tamil','usr_home_send_msg_to_adm/system',NULL,''),(32173,'tamil','usr_welcome_to_mem_sec',NULL,''),(32174,'tamil','value',NULL,''),(32175,'tamil','vender_id_v',NULL,''),(32176,'tamil','view',NULL,''),(32177,'tamil','view_demo_page',NULL,''),(32178,'tamil','waiting',NULL,''),(32179,'tamil','website_description',NULL,''),(32180,'tamil','website_keywords',NULL,''),(32181,'tamil','website_title',NULL,''),(32182,'tamil','with_select',NULL,''),(32183,'tamil','with_sel_send_msg',NULL,''),(32184,'tamil','yes',NULL,''),(32185,'tamil','zip_code',NULL,''),(32186,'english','admin_last_login_history','','Admin Last Login History'),(32187,'hindi','admin_last_login_history','','व्यवस्थापक अंतिम लॉगिन इतिहास'),(32188,'english','user_last_login_history','','User Last Login History'),(32189,'hindi','user_last_login_history','','उपयोगकर्ता अंतिम लॉगिन इतिहास'),(32190,'english','subject_type_name',NULL,'Subject Type'),(32192,'hindi','new_unread_msg','','नई अपठित संदेश'),(32193,'hindi','new_password','','नया पासवर्ड:'),(32194,'hindi','new_usr_register','','नई पंजीकरण'),(32195,'hindi','no','','नहीं'),(32196,'hindi','note','','नोट'),(32197,'hindi','no_rec_found','','कोई रिकॉर्ड नहीं मिला!'),(32198,'hindi','subject_type_name','','विषय लिखें'),(32199,'english','branch_name',NULL,'Branch Name'),(32200,'english','branch_address',NULL,'Address'),(32201,'english','Areaname',NULL,'Area Name'),(32202,'english','main_branch_id',NULL,'branch name'),(32203,'english','Adderss',NULL,'Adderss'),(32204,'english','Address',NULL,'Address'),(32205,'english','introducer_type',NULL,'introducer type'),(32206,'english','receipt_type',NULL,'Receipt type'),(32207,'english','co_name',NULL,'Co-Ordinator code'),(32208,'english','link','','Link'),(32209,'hindi','link','',''),(32210,'english','display','','Display'),(32211,'hindi','display','',''),(32212,'english','usr_already_exit','','existing userid already present plz user another one'),(32213,'hindi','usr_already_exit','',''),(32216,'english','sms_credit_details','','Counts 70 Chars as 1 sms unit (Only if body contains unicode characters.)\r\nFor Text Message\r\n1-160 characters = 1 SMS Credit.\r\n161-306 characters = 2 SMS Credits.\r\n307-459 characters = 3 SMS Credits.\r\nFor Unicode Message\r\n1-70 characters = 1 SMS Credit.\r\n71-126 characters = 2 SMS Credits.\r\n127-189 characters = 3 SMS Credits.'),(32284,'english','cheque_date','','cheque_date'),(32218,'english','blk_sms','','Block SMS'),(32220,'english','unblock_sms','','Unblock SMS'),(32222,'english','mob','','Mob'),(32283,'english','cheque_dd_no','','cheque_dd_no'),(32224,'english','send_sms','','Send Sms'),(32226,'english','canned_message','','Canned Message'),(32282,'english','payment_by','','payment_by'),(32228,'english','member_list','','Member List'),(32230,'english','member_name','','Member Name'),(32281,'english','tr_date','','tr_date'),(32232,'english','password_v','','Password_v'),(32234,'english','one_time','','One Time'),(32280,'english','tr_no','','tr_no'),(32236,'english','repeat','','Repeat'),(32238,'english','payment_for','','Payment For'),(32279,'english','receipt_no','','receipt_no'),(32240,'english','price','','Price'),(32242,'english','payment_type','','Payment Type'),(32244,'english','reciept_no','','Receiept No'),(32278,'english','edit_page_content','','Edit Page Content'),(32246,'english','choose_a_gateway','','Choose Gateway'),(32248,'english','CheckOut','','CheckOut'),(32277,'english','login_required','','Login Required'),(32250,'english','thanks_you_for_your_order','','Thanks you for your order'),(32272,'english','ID','','ID'),(32271,'english','mob1','','mob'),(32273,'english','Bankname','','Bank Name'),(32274,'english','Acno','','Bank A/cno'),(32275,'english','select_menu','','Select Menu'),(32276,'english','top_level_menu','','Top Level Menu'),(32254,'english','tracking_no','','tracking_no'),(32256,'english','sub_total','','Sub Total'),(32286,'english','group_no','','group_no'),(32287,'english','ticket_no_name','','ticket_no_name'),(32285,'english','day_book_no','','day_book_no'),(32262,'hindi','branch_address','','पता'),(32263,'hindi','Areaname','','क्षेत्र का नाम'),(32264,'hindi','Adderss','','Adderss'),(32265,'hindi','Address','','पता'),(32288,'english','inst_no','','inst_no'),(32267,'english','abc','','abc'),(32268,'hindi','abc','','एबीसी'),(32289,'english','inst_month','','inst_month'),(32290,'english','divident','','divident'),(32291,'english','amt_recd','','amt_recd'),(32292,'english','installation_no','','installation_no');
/*!40000 ALTER TABLE `tbl_lang_msg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_lang_msg_email`
--

DROP TABLE IF EXISTS `tbl_lang_msg_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_lang_msg_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lang` varchar(30) NOT NULL DEFAULT 'english',
  `mainkey` varchar(200) DEFAULT NULL,
  `subkey` varchar(100) DEFAULT NULL,
  `descr` text,
  `msg_type` varchar(100) NOT NULL,
  `order_no` int(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lang_mainkey_subkey` (`lang`,`mainkey`,`subkey`),
  KEY `mainkey` (`mainkey`)
) ENGINE=MyISAM AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_lang_msg_email`
--

LOCK TABLES `tbl_lang_msg_email` WRITE;
/*!40000 ALTER TABLE `tbl_lang_msg_email` DISABLE KEYS */;
INSERT INTO `tbl_lang_msg_email` VALUES (4,'english','email_subject_after_registration','','Welcome to our community','signup',1),(5,'greek','email_subject_after_registration','','Καλώς ήρθατε στην κοινότητά μας','signup',1),(10,'english','email_thank_you','','Thank You','signup',4),(11,'greek','email_thank_you','','Σε ευχαριστώ','signup',4),(6,'english','email_hello','','Hello','signup',2),(7,'greek','email_hello','','Γειά','signup',2),(8,'english','email_message_after_registration','','Thank you for your registration, please click the following Link to activate your account','signup',3),(26,'english','email_usr_mail_subject','','New Message Received','inbox_outbox',1),(9,'greek','email_message_after_registration','','Σας ευχαριστούμε για την εγγραφή σας, κάντε κλικ στον παρακάτω σύνδεσμο για να ενεργοποιήσετε το λογαριασμό σας','signup',3),(12,'english','email_sign','','NET FORCE Ltd\r\nINTERNET BUSINESS SERVICES\r\n59 Solomou street\r\nAthens, Greece\r\nTel  0030 210 5236749\r\nFax 0030 210 5246860 \r\nwww.netforce.gr\r\nEmail: info@netforce.gr','signup',6),(13,'greek','email_sign','','NET FORCE Ltd\r\nINTERNET BUSINESS SERVICES\r\n59 Solomou street\r\nAthens, Greece\r\nTel  0030 210 5236749\r\nFax 0030 210 5246860 \r\nwww.netforce.gr\r\nEmail: info@netforce.gr','signup',6),(14,'english','email_u_r_forget_pass_req','','Forgot password request','pass',1),(15,'greek','email_u_r_forget_pass_req','','Ξεχάσατε τον κωδικό αίτηση','pass',1),(16,'english','email_here_is_you_login_detail','','Here is you login details','pass',2),(17,'greek','email_here_is_you_login_detail','','Εδώ είναι τα στοιχεία σύνδεσής σας','pass',2),(18,'english','email_login_id','','Username','pass',3),(19,'greek','email_login_id','','Username','pass',3),(20,'english','email_new_password','','New Password','pass',4),(21,'greek','email_new_password','','Νέο Password','pass',4),(22,'english','email_pass_changed','','Password changed','pass',5),(23,'greek','email_pass_changed','','Κωδικός πρόσβασης άλλαξε','pass',5),(24,'english','email_pass_msg','','Your passwords has been changed, please check the following details','pass',6),(25,'greek','email_pass_msg','','Κωδικοί πρόσβασης σας έχει αλλάξει, ελέγξτε τα ακόλουθα στοιχεία','pass',6),(27,'greek','email_usr_mail_subject','','Νέο μήνυμα Ελήφθη','inbox_outbox',1),(28,'english','email_user_msg_des1','','You have received new message from','inbox_outbox',2),(29,'greek','email_user_msg_des1','','Έχετε λάβει ένα νέο μήνυμα από','inbox_outbox',2),(30,'english','email_usr_msg_des2','','for more details please refer following link','inbox_outbox',2),(31,'greek','email_usr_msg_des2','','για περισσότερες πληροφορίες αναφέρονται παρακάτω σύνδεσμο','inbox_outbox',2),(32,'english','total_no_product','','Total number of product','inbox_outbox',4),(33,'greek','total_no_product','','Συνολικός αριθμός προϊόντων','inbox_outbox',4),(34,'english','new_sub_mem','','Total number of property added after your last login','inbox_outbox',5),(35,'greek','new_sub_mem','','Συνολικός αριθμός των περιουσιακών προστίθεται μετά την τελευταία σύνδεσή σας','inbox_outbox',5),(79,'greek','password','','Κωδικός πρόσβασης','signup',5),(78,'english','password','','Password','signup',5),(76,'english','login','','User Name','signup',3),(77,'greek','login','','Σύνδεση','signup',3),(75,'greek','email_chak_details','','Τα στοιχεία σύνδεσής σας δίνονται παρακάτω','signup',1),(74,'english','email_chak_details','','Your login details are given below','signup',1),(72,'english','email_the_order_details_is_given_below','','email the order details is given below','order',7),(73,'greek','email_the_order_details_is_given_below','','e-mail τις λεπτομέρειες για παρατίθεται κατωτέρω','order',7),(71,'greek','pay_method','','Τρόπος πληρωμής','order',5),(70,'english','pay_method','','Payment Method','order',5),(69,'greek','total','','Σύνολο','order',5),(68,'english','total','','Total','order',5),(66,'english','email_total_number_of_purchased_item_are','','email total number of purchased item are','order',4),(67,'greek','email_total_number_of_purchased_item_are','','email συνολικός αριθμός των σημείων είναι αγοράζονται','order',4),(65,'greek','order_number','','Παραλαβή Αριθμός','order',3),(63,'greek','recived_msg','','Μήνυμα ελήφθη','order',2),(64,'english','order_number','','Receipt Number','order',3),(62,'english','recived_msg','','Received Message','order',2),(61,'greek','email_thank_you_for_purchasing_order_sucessfully_confermed','','Σας ευχαριστώ για την πληρωμή','order',1),(80,'english','email_order_status_subject','','Your Payment Status','payment',1),(60,'english','email_thank_you_for_purchasing_order_sucessfully_confermed','','Thank you for payment ','order',1),(81,'greek','email_order_status_subject','','Κατάσταση πληρωμής','payment',1),(82,'english','email_recipt_number','','Receipt Number','payment',2),(83,'greek','email_recipt_number','','Παραλαβή Αριθμός','payment',2),(84,'english','email_payment_status','','Payment Status','payment',3),(85,'greek','email_payment_status','','Κατάσταση Πληρωμής','payment',3),(86,'english','email_order_status_msg','','Your payment staus has beed changes, please chck the following status','payment',4),(87,'greek','email_order_status_msg','','Staus πληρωμή σας έχει beed αλλαγές, παρακαλώ chck την ακόλουθη κατάσταση','payment',4),(88,'english','email_click_here_to_make_login','','Click here to make login','pass',7),(89,'greek','email_click_here_to_make_login','','Κάντε κλικ εδώ για να συνδεθείτε','pass',7),(90,'english','payment_type','','Payment Type','payment',5),(91,'greek','payment_type','','Πληρωμής','payment',5),(92,'english','order_status','','Order Status','payment',6),(93,'greek','order_status','','Κατάσταση Παραγγελίας','payment',6),(94,'english','birth_day_riminder_sub','','Birthday Reminder','birthday',1),(95,'greek','birth_day_riminder_sub','','Υπενθύμιση γενεθλίων','birthday',1),(96,'english','birth_day_riminder_msg','','Today is your birth day, enjoy the day.','birthday',2),(97,'greek','birth_day_riminder_msg','','Σήμερα είναι η ημέρα γέννησης σας, απολαύστε την ημέρα.','birthday',2);
/*!40000 ALTER TABLE `tbl_lang_msg_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mn_app_setting`
--

DROP TABLE IF EXISTS `tbl_mn_app_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mn_app_setting` (
  `app_setting_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `field_name` varchar(30) DEFAULT NULL,
  `field_value` varchar(100) DEFAULT NULL,
  `note` text NOT NULL,
  `approved` int(3) NOT NULL DEFAULT '0',
  `type` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`app_setting_id`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mn_app_setting`
--

LOCK TABLES `tbl_mn_app_setting` WRITE;
/*!40000 ALTER TABLE `tbl_mn_app_setting` DISABLE KEYS */;
INSERT INTO `tbl_mn_app_setting` VALUES (37,'session_timeout','30','Session timeout',1,'text'),(38,'def_country','gr','default country',0,'text'),(39,'def_meta_keyword','Here you will find your dream real estate','Here you will find your dream real estate',0,'text'),(35,'admin_email_id','guru@vip.gr','Το email από το οποίο θα στέλνονται θα μηνύματα στους χρήστες',1,'text'),(34,'RecordsPerPage','25','Number of records per page',1,'text'),(41,'max_limit_day','50','',0,'text'),(42,'def_currency','€','euro currency',0,'text'),(51,'for_deposite_slip_no','3','',0,''),(52,'for_deposite_slip_no','3','',0,'');
/*!40000 ALTER TABLE `tbl_mn_app_setting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mn_banner`
--

DROP TABLE IF EXISTS `tbl_mn_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mn_banner` (
  `banner_id` varchar(10) NOT NULL,
  `banner1` varchar(200) NOT NULL,
  `banner2` varchar(200) NOT NULL,
  `banner3` varchar(200) NOT NULL,
  `banner4` varchar(200) NOT NULL,
  `banner5` varchar(200) NOT NULL,
  `banner6` varchar(200) NOT NULL,
  `html` longtext NOT NULL,
  `note` longtext CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `url1` text NOT NULL,
  `url2` text NOT NULL,
  `url3` text NOT NULL,
  `url4` text NOT NULL,
  `url5` text NOT NULL,
  `url6` text NOT NULL,
  PRIMARY KEY (`banner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mn_banner`
--

LOCK TABLES `tbl_mn_banner` WRITE;
/*!40000 ALTER TABLE `tbl_mn_banner` DISABLE KEYS */;
INSERT INTO `tbl_mn_banner` VALUES ('banner_id','girl1.jpg','1girl4.jpg','Tulips.jpg','8girl1.jpg','19girl3.jpg','Koala.jpg','HTMLHTMLHTMLHTML1','NoteNoteNoteNoteNote1','','','www.netforce.gr','www.netforce.gr','http://projects.netforce.gr/test_real_estate/index.php','http://projects.netforce.gr/test_real_estate/index.php');
/*!40000 ALTER TABLE `tbl_mn_banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mn_db_backup`
--

DROP TABLE IF EXISTS `tbl_mn_db_backup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mn_db_backup` (
  `backup_id` int(11) NOT NULL AUTO_INCREMENT,
  `backup_filename` varchar(200) NOT NULL DEFAULT '',
  `restore_date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`backup_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mn_db_backup`
--

LOCK TABLES `tbl_mn_db_backup` WRITE;
/*!40000 ALTER TABLE `tbl_mn_db_backup` DISABLE KEYS */;
INSERT INTO `tbl_mn_db_backup` VALUES (1,'29_04_2010_14:19:39_chit_bck.doc.gz','2010-04-29 14:19:39'),(2,'30_04_2010_14:42:26_chit_bck.doc.gz','2010-04-30 14:42:26'),(3,'04_05_2010_13:27:31_chit_new_bck.doc.gz','2010-05-04 13:27:31'),(4,'04_05_2010_13:28:43_chit_new_bck.doc.gz','2010-05-04 13:28:43'),(5,'04_05_2010_13:29:22_chit_new_bck.doc.gz','2010-05-04 13:29:22'),(6,'04_05_2010_00:31:39_chit_new_bck.doc.gz','2010-05-05 00:31:39'),(7,'04_05_2010_00:35:12_chit_new_bck.doc.gz','2010-05-05 00:35:12'),(8,'04_05_2010_00:37:48_chit_new_bck.doc.gz','2010-05-05 00:37:48'),(9,'04_05_2010_01:01:05_chit_new_bck.doc.gz','2010-05-05 01:01:05'),(10,'04_05_2010_01:01:16_chit_new_bck.doc.gz','2010-05-05 01:01:16'),(11,'04_05_2010_01:04:18_chit_new_bck.doc.gz','2010-05-05 01:04:18');
/*!40000 ALTER TABLE `tbl_mn_db_backup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mn_menu_page`
--

DROP TABLE IF EXISTS `tbl_mn_menu_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mn_menu_page` (
  `cat_id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `main_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `hierno` int(20) NOT NULL DEFAULT '0',
  `order_no` int(11) NOT NULL DEFAULT '0',
  `lang` text CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `type` varchar(20) NOT NULL,
  `page_type` varchar(20) NOT NULL,
  `login_req` int(11) NOT NULL DEFAULT '0',
  `main_cat_id` int(11) NOT NULL,
  `category_tag` text NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=601 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mn_menu_page`
--

LOCK TABLES `tbl_mn_menu_page` WRITE;
/*!40000 ALTER TABLE `tbl_mn_menu_page` DISABLE KEYS */;
INSERT INTO `tbl_mn_menu_page` VALUES (21,'Surveillance','0',0,8,'english','adm','link',0,21,'#'),(22,'Επιτήρηση','0',0,9,'greek','adm','link',0,21,'<p>test</p>'),(25,'Setting','0',0,9,'english','adm','link',0,25,'#'),(26,'Εισερχόμενα','0',0,8,'greek','adm','link',0,25,''),(39,'Home','0',0,1,'english','usr','page',0,39,'<p>&nbsp;</p>\r\n<p>hello ia m index page</p>'),(40,'Σπίτι','0',0,1,'greek','usr','link',0,39,'<p>index.php</p>'),(53,'Add New Course','3',1,11,'english','adm','link',0,53,'AdmCourse_List.php?op=1'),(54,'Προσθήκη νέου μαθήματος','3',1,11,'greek','adm','link',0,53,'AdmCourse_List.php?op=1'),(55,'Course  List','3',1,12,'english','adm','link',0,55,'AdmCourse_List.php'),(56,'Κατάλογος Μαθημάτων','3',1,12,'greek','adm','link',0,55,'AdmCourse_List.php'),(71,'Offers / group buy lists','23',1,23,'english','adm','link',0,71,'AdmGroupBuyList.php'),(72,'Î ÏÎ¿ÏƒÏ†Î¿ÏÎ­Ï‚ / Î¿Î¼Î¬Î´Î± Î±Î³Î¿ÏÎ¬ÏƒÎ¿Ï…Î½ ÎºÎ±Ï„Î±Î»ÏŒÎ³Î¿Ï…Ï‚','23',1,23,'greek','adm','link',0,71,'AdmGroupBuyList.php'),(91,'top','0',0,0,'english','hdr','page',0,91,''),(92,'top','0',0,0,'greek','hdr','page',0,91,''),(99,'Menu for Admin and User','95',1,31,'english','adm','link',0,99,'nmenu_page.php?mntp=adm'),(100,'Μενού για Admin και User','95',1,31,'greek','adm','link',0,99,'nmenu_page.php?mntp=adm'),(103,'Banner Details','95',1,33,'english','adm','link',0,103,'Banner_Details.php'),(104,'Banner Λεπτομέρειες','95',1,33,'greek','adm','link',0,103,'Banner_Details.php'),(105,'Drop Down Menu','25',1,5,'english','adm','link',0,105,'mn_menu_page_list.php'),(106,'Εισερχόμενα','25',1,41,'greek','adm','link',0,105,'AdmUsr_MsgList.php'),(107,'Website Banners','25',1,6,'english','adm','link',0,107,'mn_banner_list.php'),(108,'Αποστολή Email','25',1,42,'greek','adm','link',0,107,'AdmMsg_PersonnelList.php?Select_Personnel=All'),(109,'Left Header Footer','25',1,7,'english','adm','link',0,109,'mn_left_header_footer_list.php'),(110,'Σταλεί λίστα μηνυμάτων','25',1,43,'greek','adm','link',0,109,'AdmAdm_MsgList.php'),(115,'Last login history','21',1,51,'english','adm','link',0,115,'sur_last_login_history.php'),(116,'Πελατών ιστορία login','21',1,51,'greek','adm','link',0,115,'AdmLastLoginList.php'),(119,'IP Address Settings','21',1,53,'english','adm','link',0,119,'sur_ip_address_list.php'),(120,'IP Address Settings','21',1,53,'greek','adm','link',0,119,'AdmIP_AddressList.php'),(127,'Application Variables','27',1,64,'english','adm','link',0,127,'AdmApp_SettingList.php'),(128,'Εφαρμογή Μεταβλητές','27',1,64,'greek','adm','link',0,127,'AdmApp_SettingList.php'),(133,'Database backup','131',1,81,'english','adm','link',1,133,'Take_MySql_Backup.php?db_backup_set_id=db'),(134,'Database backup','131',1,81,'greek','adm','link',1,133,'Take_MySql_Backup.php?db_backup_set_id=db'),(135,'Database Restore','131',1,82,'english','adm','link',1,135,'Take_MySql_Backup_Restore.php?db_backup_set_id=db'),(136,'Επαναφορά βάσης δεδομένων','131',1,82,'greek','adm','link',1,135,'Take_MySql_Backup_Restore.php?db_backup_set_id=db'),(137,'Administrators Details','21',1,64,'english','adm','link',0,137,'sur_adm_sys_user_update.php?personnel_id_temp=1&main=1'),(138,'Administrator\'s Details','21',1,64,'greek','adm','link',0,137,'Adm_Ac_Update.php?personnel_id_temp=1&main=1'),(139,'Delete temporary files','21',1,66,'english','adm','link',0,139,'sur_admin_home.php?del=1'),(140,'Διαγράψτε τα προσωρινά αρχεία','21',1,66,'greek','adm','link',0,139,'Admin_Home.php?del=1'),(143,'Course Type Lookup','3',1,13,'english','adm','link',0,143,'nlist_cource_type.php'),(144,'Φυσικά Lookup Τύπος','3',1,13,'greek','adm','link',0,143,'nlist_cource_type.php'),(155,'Inbox','0',0,6,'english','usr','link',1,155,'#'),(156,'Εισερχόμενα','0',0,6,'greek','usr','link',1,155,'#'),(157,'Inbox','155',1,1,'english','usr','link',1,157,'msg_inbox.php'),(158,'Inbox','155',1,1,'greek','usr','link',1,157,'UsrAdm_MsgList.php'),(159,'Message Archive','155',1,2,'english','usr','link',1,159,'msg_stored.php'),(160,'Message Archive','155',1,2,'greek','usr','link',1,159,'UsrStored_MsgList.php'),(161,'Send Message to Admin','155',1,3,'english','usr','link',1,161,'msg_send_message.php'),(162,'Send Message to Admin','155',1,3,'greek','usr','link',1,161,'UsrSend_Sel_Personnel.php'),(163,'Sent Messages','155',1,4,'english','usr','link',1,163,'msg_sent.php'),(164,'Sent Messages','155',1,4,'greek','usr','link',1,163,'UsrUsr_MsgList.php'),(165,'Personal Info','0',0,3,'english','usr','link',1,165,'#'),(166,'Προσωπικές Πληροφορίες','0',0,3,'greek','usr','link',1,165,'#'),(167,'Update Personal Information','165',1,1,'english','usr','link',1,167,'sur_adm_user_edit.php'),(168,'Ενημέρωση προσωπικών στοιχείων','165',1,1,'greek','usr','link',1,167,'Ac_Update.php'),(174,'header','0',0,1,'english','lhf','page',0,174,'<p><font color=\"#ffff00\"><img height=\"47\" width=\"230\" alt=\"\" src=\"doc/fgnya11sssss(2).jpg\" />1</font></p>'),(175,'header','0',0,1,'greek','lhf','page',0,174,'<p>Newsletter &tau;&eta;&sigmaf; NET FORCE12</p>'),(176,'footer','0',0,1,'english','lhf','page',0,176,'<table height=\"38\" cellspacing=\"1\" cellpadding=\"1\" border=\"0\" align=\"center\" width=\"100%\">\r\n    <tbody>\r\n        <tr>\r\n            <td bgcolor=\"#710000\" align=\"center\"><font size=\"2\" face=\"Arial\" color=\"#ffffff\">Copy right 2010-2009 kanya chit fund</font></td>\r\n        </tr>\r\n    </tbody>\r\n</table>'),(177,'footer','0',0,1,'greek','lhf','page',0,176,'<table height=\"38\" cellspacing=\"1\" cellpadding=\"1\" border=\"0\" align=\"center\" width=\"100%\">\r\n    <tbody>\r\n        <tr>\r\n            <td bgcolor=\"#710000\" align=\"center\"><font size=\"2\" face=\"Arial\" color=\"#ffffff\"><a href=\"http://www.netforce.gr/\">Created by NET FORCE</a></font></td>\r\n        </tr>\r\n    </tbody>\r\n</table>'),(182,'Subject/Topic Lookup','3',1,14,'english','adm','link',0,182,'nlist_subject.php'),(183,'Θέμα / Topic Lookup','3',1,14,'greek','adm','link',0,182,'nlist_subject.php'),(218,'Import Products','3',1,15,'english','adm','link',0,218,'import.php'),(219,'Προϊόντα εισαγωγής','3',1,15,'greek','adm','link',0,218,'import.php'),(220,'Add New Expense Details','145',1,1,'english','adm','link',0,220,'admexpences_list.php?op=1'),(221,'Προσθήκη νέου Λεπτομέρειες Εξόδων','145',1,1,'greek','adm','link',0,220,'admexpences_list.php?op=1'),(240,'Expense Details','145',1,2,'english','adm','link',0,240,'admexpences_list.php'),(241,'Δαπάνη Λεπτομέρειες','145',1,2,'greek','adm','link',0,240,'admexpences_list.php'),(242,'Expense Type Lookup','145',1,3,'english','adm','link',0,242,'nlist_can_expence_type.php'),(243,'Δαπάνη Lookup Τύπος','145',1,3,'greek','adm','link',0,242,'nlist_can_expence_type.php'),(244,'Made By Lookup','145',1,4,'english','adm','link',0,244,'nlist_can_madeby_type.php'),(245,'Made Με Lookup','145',1,4,'greek','adm','link',0,244,'nlist_can_madeby_type.php'),(246,'News letter report for subscriber','151',1,1,'english','adm','link',0,246,'news_letter_user_report.php'),(247,'Νέα έκθεση για την επιστολή συνδρομητή','151',1,1,'greek','adm','link',0,246,'news_letter_user_report.php'),(248,'News letter statistics','151',1,2,'english','adm','link',0,248,'news_letter_statistics.php'),(249,'Ημερομηνία σοφός / par ημέρα / μήνας / χρόνος ρεπόρτερ','151',1,2,'greek','adm','link',0,248,'news_letter_statistics.php'),(262,'Student  List','149',1,1,'english','adm','link',0,262,'AdmUserList.php?usrflag=1'),(263,'Φοιτητής Κατάλογος','149',1,0,'greek','adm','link',0,262,'AdmUserList.php?usrflag=1'),(264,'Teacher List','149',1,2,'english','adm','link',0,264,'AdmUserList.php?usrflag=2'),(265,'Δάσκαλος List','149',1,0,'greek','adm','link',0,264,'AdmUserList.php?usrflag=2'),(274,'Low stock report','272',1,0,'english','adm','link',0,274,'AdmStock_Report.php'),(275,'Χαμηλή έκθεση stock','272',1,0,'greek','adm','link',0,274,'AdmStock_Report.php'),(276,'Low Stock Setting','272',1,0,'english','adm','link',0,276,'AdmLowStock_Setting.php'),(277,'Χαμηλή Stock Περιβάλλον','272',1,0,'greek','adm','link',0,276,'AdmLowStock_Setting.php'),(278,'Manage Country List','272',1,0,'english','adm','link',0,278,'nlist_country.php'),(279,'Διαχειριστείτε Χώρα Κατάλογος','272',1,0,'greek','adm','link',0,278,'nlist_country.php'),(280,'Area, Perfecture and City','272',1,0,'english','adm','link',0,280,'Area_Prefecture_City_List.php?main=3&op=1&blk=3'),(281,'Περιοχή, Νομαρχία και Πόλη','272',1,0,'greek','adm','link',0,280,'Area_Prefecture_City_List.php?main=3&op=1&blk=3'),(282,'Shipping Method','272',1,0,'english','adm','link',0,282,'AdmShipping_Method_List.php'),(283,'Shipping Method','272',1,0,'greek','adm','link',0,282,'AdmShipping_Method_List.php'),(294,'About Us','0',0,8,'english','usr','page',0,294,'<p>Test about is sdsd</p>'),(295,'About Us','0',0,8,'greek','usr','page',0,294,'<p>Test greeek about ussds</p>'),(300,'Category','5',1,0,'english','adm','link',0,300,'ncategory_list.php?mntp=cat'),(301,'Κατηγορία','5',1,0,'greek','adm','link',0,300,'ncategory_list.php?mntp=cat'),(308,'Newsletters','0',0,7,'english','adm','link',0,308,'#'),(309,'','0',0,0,'greek','adm','link',0,308,''),(314,'Add New News Letter','308',1,0,'english','adm','link',0,314,'news_letter_list_send.php?op=1'),(315,'Προσθήκη νέου Επιστολή Ειδήσεις','308',1,0,'greek','adm','link',0,314,'news_letter_list_send.php?op=1'),(316,'Manage News Letter','308',1,0,'english','adm','link',0,316,'news_letter_list_send.php'),(317,'Διαχειριστείτε Επιστολή Ειδήσεις','308',1,0,'greek','adm','link',0,316,'news_letter_list_send.php'),(318,'Manage News Letter Templates ','308',1,0,'english','adm','link',0,318,'news_letter_templates.php'),(319,'Διαχειριστείτε Επιστολή Νέα Πρότυπα','308',1,0,'greek','adm','link',0,318,'news_letter_templates.php'),(320,'Manage news letter mailing list ','308',1,0,'english','adm','link',0,320,'news_letter_mailing_list.php'),(321,'Διαχειριστείτε επιστολή Νέα λίστα','308',1,0,'greek','adm','link',0,320,'news_letter_mailing_list.php'),(322,'Subscribe to newsletter','0',0,2,'english','usr','link',0,322,'news_letter_subscribe.php'),(323,'Εγγραφή στο newsletter','0',0,2,'greek','usr','link',0,322,'news_letter_subscribe.php'),(328,'Communication','0',0,6,'english','adm','link',0,328,'#'),(329,'','0',0,0,'greek','adm','link',0,328,''),(332,'Inbox','328',1,0,'english','adm','link',0,332,'msg_inbox.php'),(333,'','328',1,0,'greek','adm','link',0,332,''),(334,'Send new message','328',1,0,'english','adm','link',0,334,'msg_send_message.php'),(335,'','328',1,0,'greek','adm','link',0,334,''),(336,'Sent messages','328',1,0,'english','adm','link',0,336,'msg_sent.php'),(337,'','328',1,0,'greek','adm','link',0,336,''),(338,'Stored','328',1,0,'english','adm','link',0,338,'msg_stored.php'),(339,'','328',1,0,'greek','adm','link',0,338,''),(340,'Canned messages','328',1,0,'english','adm','link',0,340,'nlist_can_subject_type.php'),(341,'','328',1,0,'greek','adm','link',0,340,''),(342,'Country Area Perfecture City','25',1,14,'english','adm','link',0,342,'mn_area_prefecture_city_list.php?blk=3&op=1&main=3'),(343,'','25',1,0,'greek','adm','link',0,342,''),(344,'Language Translation','25',1,1,'english','adm','link',0,344,'mn_lang_msg_list.php'),(345,'','25',1,0,'greek','adm','link',0,344,''),(346,'Canned email/SMS message','25',1,2,'english','adm','link',0,346,'mn_lang_canned_email_sms_list.php'),(347,'','25',1,0,'greek','adm','link',0,346,''),(350,'Custom Fields & Profiles','25',1,13,'english','adm','link',0,350,'mn_profile_cus_fields.php?maintype=user'),(351,'','25',1,0,'greek','adm','link',0,350,''),(352,'SMS Setting','25',1,12,'english','adm','link',0,352,'mn_sms_setting.php'),(353,'','25',1,0,'greek','adm','link',0,352,''),(354,'Payment Gateway List','25',1,11,'english','adm','link',0,354,'mn_payment_gateway_list.php'),(355,'','25',1,0,'greek','adm','link',0,354,''),(356,'Application Variable','25',1,10,'english','adm','link',0,356,'mn_app_variable_list.php'),(357,'','25',1,0,'greek','adm','link',0,356,''),(360,'Contact Us','0',0,9,'english','usr','page',0,360,'<p><span class=\"Title\"><strong>Contact us&nbsp; </strong></span>:</p>\r\n<p><img height=\"77\" width=\"230\" src=\"http://localhost/chitfund/doc/fgnya11sssss(3).jpg\" alt=\"\" /></p>\r\n<p>&nbsp;</p>'),(361,'','0',0,0,'greek','usr','link',0,360,''),(362,'User List','21',1,1,'english','adm','link',0,362,'sur_adm_user_list.php'),(363,'','21',1,0,'greek','adm','link',0,362,''),(364,'User Previlage Types','21',1,2,'english','adm','link',0,364,'sur_privilege_type_list.php'),(365,'','21',1,0,'greek','adm','link',0,364,''),(370,'News letter report for subscriber','308',1,0,'english','adm','link',0,370,'news_letter_user_report.php'),(371,'','308',1,0,'greek','adm','link',0,370,''),(372,'News letter statistics','308',1,0,'english','adm','link',0,372,'news_letter_statistics.php'),(373,'','308',1,0,'greek','adm','link',0,372,''),(374,'Manage SMTP Details ','308',1,0,'english','adm','link',0,374,'news_letter_smtp_lkp.php'),(375,'','308',1,0,'greek','adm','link',0,374,''),(376,'Manage Newsletter Priority Lookup ','308',1,0,'english','adm','link',0,376,'news_letter_priority_lkp.php'),(377,'','308',1,0,'greek','adm','link',0,376,''),(378,'Multi language Text Finder Utility','25',1,3,'english','adm','link',0,378,'mn_lang_nit_text_finder.php'),(379,'','25',1,0,'greek','adm','link',0,378,''),(380,'Install Script','25',1,4,'english','adm','link',0,380,'install.php'),(381,'','25',1,0,'greek','adm','link',0,380,''),(382,'समाचारपत्रिकाएँ','0',0,0,'hindi','adm','link',0,308,'#'),(383,'इनबॉक्स','0',0,0,'hindi','adm','link',0,328,'#'),(384,'निगरानी','0',0,8,'hindi','adm','link',0,21,'#'),(385,'सेटिंग','0',0,9,'hindi','adm','link',0,25,'#'),(386,'नया समाचार पत्र में जोड़ें','308',1,0,'hindi','adm','link',0,314,'news_letter_list_send.php?op=1'),(387,'समाचार पत्र का प्रबंधन','308',1,0,'hindi','adm','link',0,316,'news_letter_list_send.php'),(388,'प्रबंधित समाचार पत्र टेम्पलेट्स','308',1,0,'hindi','adm','link',0,318,'news_letter_templates.php'),(389,'समाचार पत्र का प्रबंधन मेलिंग सूची','308',1,0,'hindi','adm','link',0,320,'news_letter_mailing_list.php'),(390,'समाचार पत्र की रिपोर्ट के लिए सदस्य','308',1,0,'hindi','adm','link',0,370,'news_letter_user_report.php'),(391,'समाचार पत्र के आँकड़े','308',1,0,'hindi','adm','link',0,372,'news_letter_statistics.php'),(392,'प्रबंधन SMTP विवरण','308',1,0,'hindi','adm','link',0,374,'news_letter_smtp_lkp.php'),(393,'प्रबंधन न्यूज़लैटर प्राथमिकता लुकअप','308',1,0,'hindi','adm','link',0,376,'news_letter_priority_lkp.php'),(394,'इनबॉक्स','328',1,0,'hindi','adm','link',0,332,'msg_inbox.php'),(395,'नया संदेश भेजें','328',1,0,'hindi','adm','link',0,334,'msg_send_message.php'),(396,'भेजे गए संदेश','328',1,0,'hindi','adm','link',0,336,'msg_sent.php'),(397,'संग्रहित','328',1,0,'hindi','adm','link',0,338,'msg_stored.php'),(398,'डिब्बाबंद संदेश','328',1,0,'hindi','adm','link',0,340,'nlist_can_subject_type.php'),(399,'उपयोक्ता सूची','21',1,0,'hindi','adm','link',0,362,'sur_adm_user_list.php'),(400,'उपयोगकर्ता Previlages','21',1,0,'hindi','adm','link',0,364,'sur_privilege_type_list.php'),(401,'अंतिम लॉगिन इतिहास','21',1,51,'hindi','adm','link',0,115,'sur_last_login_history.php'),(402,'आईपी पता सेटिंग्स','21',1,53,'hindi','adm','link',0,119,'sur_ip_address_list.php'),(403,'प्रशासक का विवरण','21',1,64,'hindi','adm','link',0,137,'sur_adm_sys_user_update.php?personnel_id_temp=1&main=1'),(404,'अस्थायी फ़ाइलों को नष्ट','21',1,66,'hindi','adm','link',0,139,'sur_admin_home.php?del=1'),(405,'भाषा अनुवाद','25',1,1,'hindi','adm','link',0,344,'mn_lang_msg_list.php'),(406,'कैन्ड ईमेल / एसएमएस संदेश','25',1,2,'hindi','adm','link',0,346,'mn_lang_canned_email_sms_list.php'),(407,'बहु भाषा पाठ खोजक उपयोगिता','25',1,3,'hindi','adm','link',0,378,'mn_lang_nit_text_finder.php'),(408,'स्क्रिप्ट स्थापित','25',1,4,'hindi','adm','link',0,380,'install.php'),(409,'ड्रॉप डाउन मेनू','25',1,5,'hindi','adm','link',0,105,'mn_menu_page_list.php'),(410,'वेबसाइट बैनर्स','25',1,6,'hindi','adm','link',0,107,'mn_banner_list.php'),(411,'वाम हैडर फूटर','25',1,7,'hindi','adm','link',0,109,'mn_left_header_footer_list.php'),(414,'आवेदन वैरिएबल','25',1,10,'hindi','adm','link',0,356,'mn_app_variable_list.php'),(415,'भुगतान गेटवे सूची','25',1,11,'hindi','adm','link',0,354,'mn_payment_gateway_list.php'),(416,'एसएमएस स्थापना','25',1,12,'hindi','adm','link',0,352,'mn_sms_setting.php'),(417,'कस्टम फील्ड्स और प्रोफाइल','25',1,13,'hindi','adm','link',0,350,'mn_profile_cus_fields.php?maintype=user'),(418,'देश क्षेत्र सिटी Perfecture','25',1,14,'hindi','adm','link',0,342,'mn_area_prefecture_city_list.php?blk=3&op=1&main=3'),(420,'header','0',0,1,'hindi','lhf','page',0,174,'<p><font color=\"#ff0000\"><strong><font size=\"5\"><span id=\"result_box\" class=\"short_text\"><span style=\"background-color: rgb(230, 236, 249); color: rgb(0, 0, 0);\" title=\"\">कन्या चिट फंड कंपनी</span></span></font></strong></font></p>'),(421,'footer','0',0,1,'hindi','lhf','page',0,176,'<table height=\"38\" cellspacing=\"1\" cellpadding=\"1\" border=\"0\" align=\"center\" width=\"100%\">\r\n    <tbody>\r\n        <tr>\r\n            <td bgcolor=\"#710000\" align=\"center\"><font size=\"3\" color=\"#ffffff\"><span class=\"short_text\" id=\"result_box\"><span title=\"\" style=\"\">प्रतिलिपि  अधिकार 2010-2009 कन्या चिट फंड</span></span></font></td>\r\n        </tr>\r\n    </tbody>\r\n</table>'),(422,'घर','0',0,1,'hindi','usr','page',0,39,''),(423,'सदस्यता लें न्यूज़लेटर','0',0,2,'hindi','usr','link',0,322,'news_letter_subscribe.php'),(424,'व्यक्तिगत जानकारी','0',0,3,'hindi','usr','link',1,165,'#'),(425,'इनबॉक्स','0',0,6,'hindi','usr','link',1,155,'#'),(426,'हमारे बारे में','0',0,8,'hindi','usr','page',0,294,''),(427,'हमसे संपर्क करें','0',0,9,'hindi','usr','page',0,360,''),(428,'इनबॉक्स','155',1,1,'hindi','usr','link',0,157,'0'),(429,'संदेश पुरालेख','155',1,2,'hindi','usr','link',0,159,'0'),(430,'संदेश भेजें व्यवस्थापक के लिए','155',1,3,'hindi','usr','link',0,161,'0'),(431,'भेजे गए संदेश','155',1,4,'hindi','usr','link',0,163,'0'),(432,'in','328',1,0,'marathi','adm','link',0,332,'msg_inbox.php'),(433,'pathwa','328',1,0,'marathi','adm','link',0,334,'msg_send_message.php'),(434,'gelele','328',1,0,'marathi','adm','link',0,336,'msg_sent.php'),(435,'st','328',1,0,'marathi','adm','link',0,338,'msg_stored.php'),(436,'cn','328',1,0,'marathi','adm','link',0,340,'nlist_can_subject_type.php'),(437,'Master','0',0,1,'english','adm','link',0,437,'#'),(438,'मालिक','0',0,0,'hindi','adm','link',0,437,'#'),(439,'Enrolment','0',0,2,'english','adm','link',0,439,'#'),(440,'नामांकन','0',0,0,'hindi','adm','link',0,439,'#'),(441,'Entry','0',0,3,'english','adm','link',0,441,'#'),(442,'प्रविष्टि','0',0,0,'hindi','adm','link',0,441,'#'),(443,'Prints & Reports','0',0,4,'english','adm','link',0,443,'#'),(444,'प्रिंट और रिपोर्टें','0',0,0,'hindi','adm','link',0,443,'#'),(445,'Bank Deposit','0',0,5,'english','adm','link',0,445,'#'),(446,'बैंक जमा','0',0,0,'hindi','adm','link',0,445,'#'),(467,'Receipt List','441',1,4,'english','adm','link',0,467,'AdmSubscription_Entry_List.php'),(468,'Intimation Print','441',1,5,'english','adm','link',0,468,'AdmPrintAuction_Intimation.php'),(469,'Group Forming / Wish List','439',1,0,'english','adm','link',0,469,'AdmGroupForming_List.php'),(470,'Company Profile','437',1,1,'english','adm','link',0,470,'AdmCompany_Entry_List.php'),(471,'Director/Share Holder/Member','437',1,2,'english','adm','link',0,471,'AdmShareHolderList.php'),(472,'Branch List','437',1,3,'english','adm','link',0,472,'nlist_branch.php'),(473,'Area List','437',1,4,'english','adm','link',0,473,'nlist_area.php'),(474,'CoOrdinator Code List','437',1,5,'english','adm','link',0,474,'nlist_coordinator_code.php'),(475,'Bank List','437',1,6,'english','adm','link',0,475,'nlist_bank.php'),(476,'Master Code List','437',1,7,'english','adm','link',0,476,'nlist_master_code.php'),(477,'Introducer List','437',1,8,'english','adm','link',0,477,'nlist_introducer.php'),(478,'Introducer Type List','437',1,9,'english','adm','link',0,478,'nlist_intorducer_type.php'),(479,'Receipt Type List','437',1,10,'english','adm','link',0,479,'nlist_receipt_type.php'),(480,'New Group / Group List','439',1,0,'english','adm','link',0,480,'AdmGroupList.php'),(481,'Group Enrollment','439',1,0,'english','adm','link',0,481,'AdmGroupEnrollment_List.php'),(482,'Applicant Substitution','439',1,0,'english','adm','link',0,482,'ApplicantSubstitution.php'),(483,'Prize Entry','441',1,1,'english','adm','link',0,483,'AdmPrizeEntry_List.php'),(484,'Intimation Entry','441',1,2,'english','adm','link',0,484,'AdmSendAuction_Intimation.php'),(485,'Subscription Entry','441',1,3,'english','adm','link',0,485,'AdmSubscription_Entry.php'),(488,'Groupwise Member List','443',1,1,'english','adm','link',0,488,'AdmGroupwise_List.php'),(489,'Outstanding','443',1,2,'english','adm','link',0,489,'AdmOutstanding_List.php'),(490,'Outstanding Group Wise','443',1,3,'english','adm','link',0,490,'AdmOutstanding_List.php?groupwise=1'),(491,'Outstanding Branch Wise','443',1,4,'english','adm','link',0,491,'AdmOutstanding_List.php?branchwise=1'),(492,'Incentive Report','443',1,6,'english','adm','link',0,492,'AdmIncentive_List.php'),(493,'Minutes Report (IX, Security, XI Forms )','443',1,6,'english','adm','link',0,493,'AdmMinutReport.php'),(494,'Agreement/Ledger Report','443',1,7,'english','adm','link',0,494,'admAgreement_Ledger.php'),(495,'Master Code Report','443',1,8,'english','adm','link',0,495,'master_code_report.php'),(496,'No Of Collection','443',1,9,'english','adm','link',0,496,'no_of_collection.php'),(497,'Chit Payment Slip','443',1,10,'english','adm','link',0,497,'chit_payment_slip.php'),(498,'Auction Dates & Prized Subscriber List Month Wise','443',1,11,'english','adm','link',0,498,'month_wise.php'),(499,'Collection Report','443',1,12,'english','adm','link',0,499,'collection_report.php'),(500,'Intimation Print Mastercode wise','441',1,6,'english','adm','link',0,500,'admintimationprintmastercodewise.php'),(501,'Bank Deposit','445',1,0,'english','adm','link',0,501,'AdmBankDeposit_Entry.php'),(502,'','445',1,0,'hindi','adm','link',0,501,'AdmBankDeposit_Entry.php'),(503,'Bank Deposit Print','445',1,0,'english','adm','link',0,503,'AdmBankDeposit_Entry.php?Print=1'),(504,'','445',1,0,'hindi','adm','link',0,503,'AdmBankDeposit_Entry.php?Print=1'),(505,'Google Transliteration IME','25',1,2,'english','adm','link',0,505,'mn_google_translator_editor.php'),(506,'गूगल लिप्यंतरण IME','25',1,0,'hindi','adm','link',0,505,'mn_google_translator_editor.php'),(507,'report_header','0',0,1,'english','lhf','page',0,507,'<p>report header will come from here</p>'),(508,'report_header','0',0,1,'hindi','lhf','page',0,507,''),(509,'version','0',0,0,'english','lhf','page',0,509,'<p>Version : 3.0</p>'),(510,'version','0',0,0,'hindi','lhf','page',0,509,''),(511,'receipt_header','0',0,1,'english','lhf','page',0,511,''),(512,'receipt_header','0',0,1,'hindi','lhf','page',0,511,''),(515,'test','0',0,1,'english','lhf','page',0,515,''),(516,'test','0',0,1,'hindi','lhf','page',0,515,''),(517,'Reffered By','437',1,10,'english','adm','link',0,517,'nlist_reffered.php'),(518,'','437',1,0,'hindi','adm','link',0,517,'nlist_reffered.php'),(519,'0','437',1,1,'hindi','adm','link',0,470,'AdmCompany_Entry_List.php'),(520,'0','437',1,2,'hindi','adm','link',0,471,'AdmShareHolderList.php'),(521,'0','437',1,3,'hindi','adm','link',0,472,'nlist_branch.php'),(522,'0','437',1,4,'hindi','adm','link',0,473,'nlist_area.php'),(523,'0','437',1,5,'hindi','adm','link',0,474,'nlist_coordinator_code.php'),(524,'0','437',1,6,'hindi','adm','link',0,475,'nlist_bank.php'),(525,'0','437',1,7,'hindi','adm','link',0,476,'nlist_master_code.php'),(526,'0','437',1,8,'hindi','adm','link',0,477,'nlist_introducer.php'),(527,'0','437',1,9,'hindi','adm','link',0,478,'nlist_intorducer_type.php'),(528,'0','437',1,10,'hindi','adm','link',0,479,'nlist_receipt_type.php'),(529,'report_print','0',0,1,'english','lhf','page',0,529,'<p>&nbsp;</p>\r\n<p>mn_lhf_report_print</p>'),(530,'report_print','0',0,1,'hindi','lhf','page',0,529,''),(531,'header','0',0,1,'tamil','lhf','page',0,174,'<p>0</p>'),(532,'Set Privileges & Access Rights','21',1,3,'english','adm','link',0,532,'sur_privi_home.php'),(533,'','21',1,0,'tamil','adm','link',0,532,'sur_privi_home.php'),(534,'0','21',1,0,'tamil','adm','link',0,362,'sur_adm_user_list.php'),(535,'0','21',1,0,'tamil','adm','link',0,364,'sur_privilege_type_list.php'),(536,'0','21',1,51,'tamil','adm','link',0,115,'sur_last_login_history.php'),(537,'0','21',1,53,'tamil','adm','link',0,119,'sur_ip_address_list.php'),(538,'0','21',1,64,'tamil','adm','link',0,137,'sur_adm_sys_user_update.php?personnel_id_temp=1&main=1'),(539,'0','21',1,66,'tamil','adm','link',0,139,'sur_admin_home.php?del=1'),(540,'make pay','21',1,0,'english','adm','link',0,540,'pay_make_payment.php'),(541,'','21',1,0,'tamil','adm','link',0,540,'pay_make_payment.php'),(542,'view pay','21',1,0,'english','adm','link',0,542,'pay_order_payment_list.php'),(543,'','21',1,0,'tamil','adm','link',0,542,'pay_order_payment_list.php'),(544,'Pay Archive','21',1,0,'english','adm','link',0,544,'pay_order_payment_list_archive.php'),(545,'','21',1,0,'tamil','adm','link',0,544,'pay_order_payment_list_archive.php'),(546,'Cash in Hand','443',1,5,'english','adm','link',0,546,'cash_cheque_in_hand.php?cash_cheque=1'),(547,'','443',1,0,'tamil','adm','link',0,546,'cash_cheque_in_hand.php?cash_cheque=1'),(548,'Cheque in Hand','443',1,5,'english','adm','link',0,548,'cash_cheque_in_hand.php?cash_cheque=2'),(549,'','443',1,0,'tamil','adm','link',0,548,'cash_cheque_in_hand.php?cash_cheque=2'),(558,'0','0',0,1,'tamil','','link',0,437,'#'),(559,'0','0',0,2,'tamil','','link',0,439,'#'),(560,'0','0',0,3,'tamil','','link',0,441,'#'),(561,'0','0',0,4,'tamil','','link',0,443,'#'),(562,'0','0',0,5,'tamil','','link',0,445,'#'),(563,'0','0',0,6,'tamil','','link',0,328,'#'),(564,'0','0',0,7,'tamil','','link',0,308,'#'),(565,'0','0',0,8,'tamil','','link',0,21,'#'),(566,'0','0',0,9,'tamil','','link',0,25,'#'),(567,'0','443',1,1,'tamil','','link',0,488,'AdmGroupwise_List.php'),(568,'0','443',1,2,'tamil','','link',0,489,'AdmOutstanding_List.php'),(569,'0','443',1,3,'tamil','','link',0,490,'AdmOutstanding_List.php?groupwise=1'),(570,'0','443',1,4,'tamil','','link',0,491,'AdmOutstanding_List.php?branchwise=1'),(571,'0','443',1,6,'tamil','','link',0,492,'AdmIncentive_List.php'),(572,'0','443',1,6,'tamil','','link',0,493,'AdmMinutReport.php'),(573,'0','443',1,7,'tamil','','link',0,494,'admAgreement_Ledger.php'),(574,'0','443',1,8,'tamil','','link',0,495,'master_code_report.php'),(575,'0','443',1,9,'tamil','','link',0,496,'no_of_collection.php'),(576,'0','443',1,10,'tamil','','link',0,497,'chit_payment_slip.php'),(577,'0','443',1,11,'tamil','','link',0,498,'month_wise.php'),(578,'0','443',1,12,'tamil','','link',0,499,'collection_report.php'),(579,'Cheque/Cash deposit slip','443',1,10,'english','adm','link',0,579,'adm_cheque_cash_slip_rpt.php'),(580,'','443',1,0,'tamil','adm','link',0,579,'adm_cheque_cash_slip_rpt.php'),(581,'header','0',0,1,'kannada','lhf','page',0,174,'<p>0</p>'),(584,'Data Base Backup Home','25',1,6,'english','adm','link',0,584,'mn_db_backup_home.php'),(585,'','25',1,0,'kannada','adm','link',0,584,'mn_db_backup_home.php'),(586,'0','25',1,1,'kannada','','link',0,344,'mn_lang_msg_list.php'),(587,'0','25',1,2,'kannada','','link',0,346,'mn_lang_canned_email_sms_list.php'),(588,'0','25',1,2,'kannada','','link',0,505,'mn_google_translator_editor.php'),(589,'0','25',1,3,'kannada','','link',0,378,'mn_lang_nit_text_finder.php'),(590,'0','25',1,4,'kannada','','link',0,380,'install.php'),(591,'0','25',1,5,'kannada','','link',0,105,'mn_menu_page_list.php'),(592,'0','25',1,6,'kannada','','link',0,107,'mn_banner_list.php'),(593,'0','25',1,7,'kannada','','link',0,109,'mn_left_header_footer_list.php'),(596,'0','25',1,10,'kannada','','link',0,356,'mn_app_variable_list.php'),(597,'0','25',1,11,'kannada','','link',0,354,'mn_payment_gateway_list.php'),(598,'0','25',1,12,'kannada','','link',0,352,'mn_sms_setting.php'),(599,'0','25',1,13,'kannada','','link',0,350,'mn_profile_cus_fields.php?maintype=user'),(600,'0','25',1,14,'kannada','','link',0,342,'mn_area_prefecture_city_list.php?blk=3&op=1&main=3');
/*!40000 ALTER TABLE `tbl_mn_menu_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mn_payment_gateway`
--

DROP TABLE IF EXISTS `tbl_mn_payment_gateway`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mn_payment_gateway` (
  `gate_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(255) DEFAULT NULL,
  `method_name` varchar(255) DEFAULT NULL,
  `protocol_support` varchar(255) DEFAULT NULL,
  `protocol` varchar(255) DEFAULT NULL,
  `Enable` int(11) DEFAULT NULL,
  `process_id_f` varchar(255) DEFAULT NULL,
  `process_id_v` varchar(255) DEFAULT NULL,
  `gateway_url` varchar(255) DEFAULT NULL,
  `test_mode_f` varchar(255) DEFAULT NULL,
  `test_mode_v` varchar(255) DEFAULT NULL,
  `vender_id_f` varchar(255) DEFAULT NULL,
  `vender_id_v` varchar(255) DEFAULT NULL,
  `currency_f` varchar(255) DEFAULT NULL,
  `currency_v` varchar(255) DEFAULT NULL,
  `password_f` varchar(255) DEFAULT NULL,
  `password_v` varchar(255) DEFAULT NULL,
  `user_name_f` varchar(255) DEFAULT NULL,
  `user_name_v` varchar(255) DEFAULT NULL,
  `partner_f` varchar(255) DEFAULT NULL,
  `partner_v` varchar(255) DEFAULT NULL,
  `extra_f` varchar(255) DEFAULT NULL,
  `extra_v` varchar(255) DEFAULT NULL,
  `email_f` varchar(255) DEFAULT NULL,
  `email_v` varchar(255) DEFAULT NULL,
  `amount_f` varchar(255) DEFAULT NULL,
  `product_desc_f` varchar(255) DEFAULT NULL,
  `cart_item_id_f` varchar(255) DEFAULT NULL,
  `active` char(1) DEFAULT NULL,
  `payment_title` varchar(255) DEFAULT NULL,
  `payment_msg` text,
  `thanks_msg` text,
  `last_modified_date` datetime DEFAULT NULL,
  `installment_f` varchar(255) DEFAULT NULL,
  `holderName_f` varchar(255) DEFAULT NULL,
  `installment_v` int(3) DEFAULT NULL,
  `lang` text NOT NULL,
  `online_offline` varchar(50) DEFAULT '0',
  PRIMARY KEY (`gate_id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mn_payment_gateway`
--

LOCK TABLES `tbl_mn_payment_gateway` WRITE;
/*!40000 ALTER TABLE `tbl_mn_payment_gateway` DISABLE KEYS */;
INSERT INTO `tbl_mn_payment_gateway` VALUES (3,'direct','Î‘Î½Ï„Î¹ÎºÎ±Ï„Î±Î²Î¿Î»Î®',NULL,'https',1,NULL,NULL,'','TransactionType','','MerchantCode','','currencycode','840',NULL,'',NULL,NULL,NULL,NULL,'param1',NULL,'CardHolderEmail',NULL,'Charge',NULL,NULL,NULL,'','','','2006-08-29 17:24:27','Installments','CardHolderName',0,'greek','0'),(4,'Website','ÎœÎµ Ï€Î¹ÏƒÏ„Ï‰Ï„Î¹ÎºÎ­Ï‚ ÎœÎ¿Î½Î¬Î´ÎµÏ‚ (Î±Î½ Î­Ï‡ÎµÏ„Îµ Ï…Ï€ÏŒÎ»Î¿Î¹Ï€Î¿)',NULL,'https',0,NULL,NULL,'',NULL,NULL,NULL,'',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,'greek','1'),(5,'proxypay','ÎœÎµ Î Î¹ÏƒÏ„Ï‰Ï„Î¹ÎºÎ® ÎšÎ¬ÏÏ„Î±','both','https',1,NULL,'paypalipn','https://ep.eurocommerce.gr/proxypay/apacsonline','no_note','LIVE','business','90000858','currency_code','USD',NULL,'E60594B580AF492E8F0FB693FEA32B570F328A2E',NULL,NULL,NULL,NULL,'cmd','_xclick','business',NULL,'amount','','item_name','A','','','','2005-10-19 13:53:49','Instalment1',NULL,NULL,'greek','1'),(7,'Website','By Website',NULL,'https',1,NULL,NULL,'',NULL,NULL,NULL,'',NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,'english','1'),(9,'paypal','PayPal','both','https',1,NULL,'paypalipn','https://www.paypal.com/cgi-bin/webscr','no_note','LIVE','business','manager@netforce.gr','currency_code','EUR',NULL,'',NULL,NULL,NULL,NULL,'cmd','_xclick','business',NULL,'amount','','item_name','A','','','','2005-10-19 13:53:49','Instalment1',NULL,NULL,'english','1'),(8,'proxypay','Eurobank','both','https',1,NULL,'paypalipn','https://ep.eurocommerce.gr/proxypay/apacsonline','no_note','LIVE','business','90000858','currency_code','USD',NULL,'E60594B580AF492E8F0FB693FEA32B570F328A2E',NULL,NULL,NULL,NULL,'cmd','_xclick','business',NULL,'amount','','item_name','A','','','','2005-10-19 13:53:49','Instalment1',NULL,NULL,'english','1'),(10,'paypal','PayPal','both','https',1,NULL,'paypalipn','https://www.paypal.com/cgi-bin/webscr','no_note','LIVE','business','manager@netforce.gr','currency_code','EUR',NULL,'',NULL,NULL,NULL,NULL,'cmd','_xclick','business',NULL,'amount','','item_name','A','','','','2005-10-19 13:53:49','Instalment1',NULL,NULL,'greek','1'),(11,'bank','ÎšÎ±Ï„Î¬Î¸ÎµÏƒÎ· ÏƒÎµ Ï„ÏÎ¬Ï€ÎµÎ¶Î±','','https',1,'','','http://www.computers.gr/banks.htm','','','','','','','','','','','','','','','','','','','','','','','','2008-06-19 00:00:00','','',0,'greek','0'),(12,'bank','Bank Deposit','','http',1,'','bank','http://www.computers.gr/banks.htm','','','','','','','','','','','','','','','','','','','','','','','','2008-06-23 00:00:00','1','',0,'english','0'),(17,'paypal','qqq','','http',1,'','22','www.jklj.com','','','','233','','','','22','','','','','','','','','','','','','','','','2009-07-16 00:00:00','hg','',0,'','0'),(18,'direct','kk','','http',1,'','ll','ll','','','','121','','','','ll','','','','','','','','','','','','','','','','2010-04-26 00:00:00','11','',0,'greek',''),(20,'bank','k','','http',1,'','k','','','','','k','','','','','','','','','','','','','','','','','','','','2010-04-28 00:00:00','k','',0,'english','0'),(21,'bank','Delta Pay','','http',1,'','12','http://www.deltapay.gr/entry.asp','','','','12345','','','','123','','','','','','','','','','','','','','','','2010-05-09 00:00:00','0','',0,'english','');
/*!40000 ALTER TABLE `tbl_mn_payment_gateway` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mn_sms_set`
--

DROP TABLE IF EXISTS `tbl_mn_sms_set`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mn_sms_set` (
  `sms_set_id` varchar(10) NOT NULL,
  `api_url` text NOT NULL,
  `sender_name` varchar(50) NOT NULL,
  `on_off_flag` int(11) NOT NULL DEFAULT '0',
  `user_id` varchar(50) NOT NULL,
  `password` varchar(40) NOT NULL,
  `api_id` varchar(40) NOT NULL,
  `max_sms_limit` int(11) NOT NULL,
  PRIMARY KEY (`sms_set_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mn_sms_set`
--

LOCK TABLES `tbl_mn_sms_set` WRITE;
/*!40000 ALTER TABLE `tbl_mn_sms_set` DISABLE KEYS */;
INSERT INTO `tbl_mn_sms_set` VALUES ('sms_set','http://208.101.14.59/api/pushsms.php','Info SMS',1,'7036','nit712','',500);
/*!40000 ALTER TABLE `tbl_mn_sms_set` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_msg`
--

DROP TABLE IF EXISTS `tbl_msg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_msg` (
  `msg_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `msg_date` date NOT NULL DEFAULT '0000-00-00',
  `msg_time` time NOT NULL DEFAULT '00:00:00',
  `msg_title` varchar(100) NOT NULL DEFAULT '',
  `msg` longtext,
  `to_personnel_id` int(6) NOT NULL DEFAULT '0',
  `from_personnel_id` int(6) NOT NULL DEFAULT '0',
  `from_usr_adm` varchar(20) NOT NULL DEFAULT '',
  `ref_to_ads_id` int(6) NOT NULL DEFAULT '0',
  `viewed` smallint(6) NOT NULL DEFAULT '0',
  `msg_type` varchar(100) NOT NULL DEFAULT '',
  `friend` int(3) NOT NULL DEFAULT '0',
  `block` int(3) NOT NULL DEFAULT '0',
  `store` int(3) NOT NULL DEFAULT '0',
  `admin_store` int(3) NOT NULL DEFAULT '0',
  `user_del` int(3) NOT NULL DEFAULT '0',
  `admin_del` int(3) NOT NULL DEFAULT '0',
  `to_del` int(2) NOT NULL,
  `from_del` int(2) NOT NULL,
  `reply` int(2) NOT NULL,
  `sent_store` int(2) NOT NULL,
  PRIMARY KEY (`msg_id`),
  KEY `to_personnel_id` (`to_personnel_id`,`from_personnel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_msg`
--

LOCK TABLES `tbl_msg` WRITE;
/*!40000 ALTER TABLE `tbl_msg` DISABLE KEYS */;
INSERT INTO `tbl_msg` VALUES (1,'2010-04-01','02:01:00','welcome greek','sdsddsdds',3,1,'0',0,1,'common',0,0,0,0,0,0,0,0,0,0),(2,'2010-04-01','19:39:40','Hii','<p>I have sent you frd request</p>',4,3,'0',0,1,'0',0,0,0,0,0,0,0,0,0,0),(3,'2010-04-01','19:45:21','Thank you','<p>Thank you, i will approve it soon</p>',3,4,'0',0,1,'0',0,0,0,0,0,0,0,0,0,0),(4,'2010-04-01','20:05:56','Hello Rocky','<p>Thank you me adding as friend</p>',4,3,'0',0,1,'0',0,0,1,0,0,0,0,0,0,0),(5,'2010-04-07','19:18:11','sdsd','<p>dsddsds</p>',1,5,'0',0,0,'0',0,0,0,0,0,0,0,0,0,1),(6,'2010-04-07','19:33:41','kk','<p>test</p>',1,5,'0',0,0,'0',0,0,0,0,0,0,0,0,0,0),(7,'2010-04-07','19:37:48','kk','<p>test</p>',1,5,'0',0,0,'0',0,0,0,0,0,0,0,0,0,0),(8,'2010-04-07','02:15:00','welcome to system','welcome to system',5,1,'0',0,1,'common',0,0,0,0,0,0,0,0,0,0),(9,'2010-04-07','19:46:16','hi','<p>hi</p>',1,5,'0',0,1,'0',0,0,0,1,0,0,0,0,0,0),(10,'2010-04-07','19:48:41','kkkk','<p>kk</p>',1,5,'0',0,1,'0',0,0,0,0,0,0,0,0,0,0),(11,'2010-04-07','19:55:58','t1t','<p>1t2</p>',1,5,'0',0,0,'0',0,0,0,0,0,0,0,0,0,0),(12,'2010-04-07','20:05:03','hi','<p>m also visit this side</p>',1,4,'0',0,0,'0',0,0,0,0,0,0,0,0,0,1),(13,'2010-04-07','20:06:13','k','<p>kk</p>',3,4,'0',0,0,'0',0,0,0,0,0,0,0,0,0,1),(14,'2010-04-09','21:05:28','test','<p>hello</p>\r\n<p>&nbsp;</p>',1,8,'0',0,1,'0',0,0,0,0,0,0,0,0,0,1),(15,'2010-04-15','11:33:08','test','test',1,11,'0',0,0,'0',0,0,0,1,0,0,0,0,0,0),(16,'2010-04-16','17:21:35','maria','<p>maria</p>',1,12,'0',0,0,'0',0,0,0,0,0,0,0,0,0,0),(17,'2010-04-24','01:55:00','ssss','ss',4,1,'0',0,1,'common',0,0,0,0,0,0,0,0,0,0),(18,'2010-04-24','01:55:00','ssss','ss',4,1,'0',0,1,'common',0,0,1,0,0,0,0,0,0,0),(19,'2010-04-26','22:58:05','regarding late patment','<p>hello</p>\r\n<p>&nbsp;</p>\r\n<p>i wouuld like to inform u that this month i wiill payment ob 12 aplril 2010</p>\r\n<p>&nbsp;</p>\r\n<p>thanks</p>\r\n<p>nitnen</p>',1,15,'0',0,1,'0',0,0,0,0,0,0,0,0,0,1),(20,'2010-04-26','05:28:00','ok no problme','',15,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(21,'2010-04-28','09:39:00','kk','ssds',1,4,'0',0,0,'common',0,0,0,0,0,0,0,0,0,1),(22,'2010-04-29','11:38:00','sds','sds',1,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(23,'2010-04-30','06:36:00','विषय','विवरण विषय',1,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(24,'2010-04-30','06:38:00','कल मीटिंग हे ','कल मीटिंग हे ',1,1,'0',0,1,'common',0,0,0,0,0,0,0,0,0,0),(25,'2010-05-07','10:34:00','test sms nit new','',7,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(26,'2010-05-07','10:34:00','hello i am testing my script','',7,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(27,'2010-05-07','10:34:00','hello i am testing my script','',7,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(28,'2010-05-07','10:36:00','hello i am testing my system','',7,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(29,'2010-05-07','10:36:00','hello i am','',7,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(30,'2010-05-07','10:36:00','hello i am testing','',7,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(31,'2010-05-07','10:37:00','hello i am testing','',7,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(32,'2010-05-07','10:41:00','test sms sending','',8,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(33,'2010-05-07','10:45:00','hello this is test sms ','',27,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(34,'2010-05-07','11:16:00','kk','',9,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(35,'2010-05-07','11:17:00','tommarrow is meeting','',1,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(36,'2010-05-07','11:33:00','testing from nit','',27,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(37,'2010-05-07','11:33:00','testing from nit','',28,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(38,'2010-05-07','11:33:00','testing from nit','',29,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(39,'2010-05-07','11:33:00','testing from nit','',30,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(40,'2010-05-07','11:33:00','testing from nit','',31,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(41,'2010-05-07','11:53:00','testing','testing',28,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(42,'2010-05-07','11:53:00','testing','testing',29,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(43,'2010-05-07','11:53:00','testing','testing',30,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(44,'2010-05-07','11:53:00','testing','testing',31,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(45,'2010-05-13','12:41:00','hello yash i am niteen','',35,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(46,'2010-05-13','02:21:00','hello i am niteen from website','',36,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(47,'2010-05-13','02:21:00','hello i am niteen ','',36,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(48,'2010-05-13','02:24:00','hello i am niteen borte','',36,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0),(49,'2010-05-13','02:24:00','hello i am niteen','',36,1,'0',0,0,'common',0,0,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `tbl_msg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_msg_subject_type`
--

DROP TABLE IF EXISTS `tbl_msg_subject_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_msg_subject_type` (
  `subject_type_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `subject_type_name` varchar(50) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `lang` varchar(100) NOT NULL,
  `main_subject_type_id` int(11) NOT NULL,
  PRIMARY KEY (`subject_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_msg_subject_type`
--

LOCK TABLES `tbl_msg_subject_type` WRITE;
/*!40000 ALTER TABLE `tbl_msg_subject_type` DISABLE KEYS */;
INSERT INTO `tbl_msg_subject_type` VALUES (26,'your a/c has been activated','your a/c has been activated','english',26),(27,'Î´Î¿ÎºÎ¹Î¼Î®','sd','greek',26),(28,'welcome to system','welcome to system','english',28),(29,'welcome greek','sdsddsdds','greek',28),(32,'','','Array',26),(33,'','','Array',28),(34,'','','Array',26),(35,'','','Array',28);
/*!40000 ALTER TABLE `tbl_msg_subject_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_msg_tmp_personnel`
--

DROP TABLE IF EXISTS `tbl_msg_tmp_personnel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_msg_tmp_personnel` (
  `personnel_id` int(11) NOT NULL,
  `from_personnel_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_msg_tmp_personnel`
--

LOCK TABLES `tbl_msg_tmp_personnel` WRITE;
/*!40000 ALTER TABLE `tbl_msg_tmp_personnel` DISABLE KEYS */;
INSERT INTO `tbl_msg_tmp_personnel` VALUES (1,4),(1,1),(2,1),(7,1),(8,1),(9,1),(10,1),(11,1),(13,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1);
/*!40000 ALTER TABLE `tbl_msg_tmp_personnel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_news_letter`
--

DROP TABLE IF EXISTS `tbl_news_letter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_news_letter` (
  `news_letter_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `news_letter_name` varchar(200) DEFAULT NULL,
  `news_letter` text,
  `lang` varchar(100) NOT NULL,
  `main_news_letter_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  PRIMARY KEY (`news_letter_id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_news_letter`
--

LOCK TABLES `tbl_news_letter` WRITE;
/*!40000 ALTER TABLE `tbl_news_letter` DISABLE KEYS */;
INSERT INTO `tbl_news_letter` VALUES (1,'testing','<p>testing in english laguage</p>','english',1,78),(2,'testing greek','<p>testing in&nbsp;greek laguage</p>','greek',1,52),(3,'Networking News','<p>Networking News Letter in english laguage</p>','english',2,52),(4,'Networking News greek','<p>Networking News Letter in greek laguage</p>','greek',2,80),(5,'Auto Cad 14','<p>Networking latest news in english laguage</p>','english',3,80),(6,'Networking latest news greek','<p>Networking latest news in&nbsp;greek laguage</p>','greek',3,34),(7,'ASP.net programming Class','<p>Testing by nil</p>','english',4,76),(8,'PHP Computer Program ','<p><center>\r\n<table height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"800\">\r\n    <tbody>\r\n        <tr>\r\n            <td width=\"100%\">\r\n            <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                <tbody>\r\n                    <tr>\r\n                        <td height=\"249\" background=\"http://demo.newsletter.pro//pictures/business3/top_back.gif\" width=\"100%\">\r\n                        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                            <tbody>\r\n                                <tr>\r\n                                    <td>\r\n                                    <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                                        <tbody>\r\n                                            <tr>\r\n                                                <td height=\"208\"><img hspace=\"39\" border=\"0\" align=\"absMiddle\" src=\"http://demo.newsletter.pro//pictures/business3/logo.gif\" alt=\"\" /></td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td style=\"padding-left: 18px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\">e-mail: <a href=\"mailto:support@pilotgroup.net\" style=\"padding-left: 2px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\">support@pilotgroup.net</a>    <a href=\"http://www.pilotgroup.net/\" style=\"padding-left: 2px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\">http://www.pilotgroup.net/</a></td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                    </td>\r\n                                    <td align=\"right\"><img border=\"0\" align=\"right\" src=\"http://demo.newsletter.pro//pictures/business3/top_picture.gif\" alt=\"\" /></td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td width=\"100%\">\r\n            <table height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                <tbody>\r\n                    <tr>\r\n                        <td bgcolor=\"#ffffff\" width=\"100%\" valign=\"top\">\r\n                        <table height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                            <tbody>\r\n                                <tr>\r\n                                    <td valign=\"top\">\r\n                                    <table height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                                        <tbody>\r\n                                            <tr>\r\n                                                <td valign=\"top\" style=\"padding: 21px;\"><font style=\"font-size: 11px; color: rgb(0, 46, 88); font-family: Tahoma;\"><strong>Dear %BASIC:FIRSTNAME% %BASIC:SECONDNAME% !</strong><br />\r\n                                                <br />\r\n                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.\r\n                                                <p style=\"padding-left: 30px; padding-bottom: 0px; padding-top: 0px;\"><a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">Our products </a><br />\r\n                                                <a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">About company </a><br />\r\n                                                <a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">Last news </a></p>\r\n                                                </font></td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td background=\"http://demo.newsletter.pro//pictures/business2/punt_line.gif\">&nbsp;</td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td style=\"padding: 21px;\"><img hspace=\"14\" height=\"30\" border=\"0\" align=\"left\" width=\"28\" src=\"http://demo.newsletter.pro//pictures/business3/house.gif\" style=\"width: 227px; height: 131px;\" alt=\"\" /> <font style=\"font-size: 11px; color: rgb(0, 46, 88); font-family: Tahoma; text-align: left;\">Iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.<br />\r\n                                                <br />\r\n                                                <a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">Our products </a><br />\r\n                                                <a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">About company </a><br />\r\n                                                <a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">Last news </a><br />\r\n                                                </font></td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td height=\"100%\">&nbsp;</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td height=\"39\" bgcolor=\"#bdd1e5\" align=\"middle\" width=\"100%\" valign=\"center\" style=\"font-size: 11px; color: rgb(188, 90, 92); font-family: Tahoma;\">Copyright 2005 Р&nbsp;вЂ&trade;Р&rsquo;В&copy; All right reserved</td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n</center></p>','english',5,76),(9,'Testing grrek','<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><center>\r\n<table height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"800\">\r\n    <tbody>\r\n        <tr>\r\n            <td width=\"100%\">\r\n            <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                <tbody>\r\n                    <tr>\r\n                        <td height=\"249\" background=\"http://demo.newsletter.pro//pictures/business3/top_back.gif\" width=\"100%\">\r\n                        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                            <tbody>\r\n                                <tr>\r\n                                    <td>\r\n                                    <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                                        <tbody>\r\n                                            <tr>\r\n                                                <td height=\"208\"><img hspace=\"39\" border=\"0\" align=\"absMiddle\" src=\"http://demo.newsletter.pro//pictures/business3/logo.gif\" alt=\"\" /></td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td style=\"padding-left: 18px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\">e-mail: <a href=\"mailto:support@pilotgroup.net\" style=\"padding-left: 2px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\">support@pilotgroup.net</a>    <a href=\"http://www.pilotgroup.net/\" style=\"padding-left: 2px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\">http://www.pilotgroup.net/</a></td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                    </td>\r\n                                    <td align=\"right\"><img border=\"0\" align=\"right\" src=\"http://demo.newsletter.pro//pictures/business3/top_picture.gif\" alt=\"\" /></td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td width=\"100%\">\r\n            <table height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                <tbody>\r\n                    <tr>\r\n                        <td bgcolor=\"#ffffff\" width=\"100%\" valign=\"top\">\r\n                        <table height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                            <tbody>\r\n                                <tr>\r\n                                    <td valign=\"top\">\r\n                                    <table height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                                        <tbody>\r\n                                            <tr>\r\n                                                <td valign=\"top\" style=\"padding: 21px;\"><font style=\"font-size: 11px; color: rgb(0, 46, 88); font-family: Tahoma;\"><strong>Dear %BASIC:FIRSTNAME% %BASIC:SECONDNAME% !</strong><br />\r\n                                                <br />\r\n                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.\r\n                                                <p style=\"padding-left: 30px; padding-bottom: 0px; padding-top: 0px;\"><a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">Our products </a><br />\r\n                                                <a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">About company </a><br />\r\n                                                <a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">Last news </a></p>\r\n                                                </font></td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td background=\"http://demo.newsletter.pro//pictures/business2/punt_line.gif\">&nbsp;</td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td style=\"padding: 21px;\"><img hspace=\"14\" height=\"30\" border=\"0\" align=\"left\" width=\"28\" src=\"http://demo.newsletter.pro//pictures/business3/house.gif\" style=\"width: 227px; height: 131px;\" alt=\"\" /> <font style=\"font-size: 11px; color: rgb(0, 46, 88); font-family: Tahoma; text-align: left;\">Iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.<br />\r\n                                                <br />\r\n                                                <a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">Our products </a><br />\r\n                                                <a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">About company </a><br />\r\n                                                <a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">Last news </a><br />\r\n                                                </font></td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td height=\"100%\">&nbsp;</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td height=\"39\" bgcolor=\"#bdd1e5\" align=\"middle\" width=\"100%\" valign=\"center\" style=\"font-size: 11px; color: rgb(188, 90, 92); font-family: Tahoma;\">Copyright 2005 Р&nbsp;вЂ&trade;Р&rsquo;В&copy; All right reserved</td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n</center></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>','greek',5,76),(10,'Yoga news letter','<p><span class=\"Title\">Yoga letter</span></p>','english',6,58),(11,'kkkk','<p>kkkkkkkkkkk</p>','english',7,34),(12,'jjjjjjjj','<p>jjjjjjjjjjj</p>','english',8,86),(13,'टी  -  20 वर्ल्ड कप में शर्मनाक प्रदर्शन ','<p>&nbsp;</p>\r\n<p><span style=\"font-size: 14px; font-weight: normal; color: rgb(0, 0, 0); font-family: ARIAL UNICODE MS,mangal,raghu8; line-height: 25px;\" name=\"test\" id=\"test\"><span style=\"font-weight: bold;\">सेंट लूसिया।। </span>   टी <span style=\"\">  - </span>  20 वर्ल्ड कप में शर्मनाक प्रदर्शन के कारण टीम इंडिया को बाहर का रास्ता देखना पड़ा। भारत के सेमीफाइलन तक नहीं पहुंच पाने के लिए दोषी आईपीएल पार्टीज़\r\n<table cellspacing=\"0\" cellpadding=\"0\" align=\"left\" width=\"205\" style=\"margin-top: 3px; margin-right: 6px;\">\r\n    <tbody>\r\n        <tr>\r\n            <td style=\"padding-left: 3px;\" id=\"bellyad\">\r\n            <div class=\"mod_grafico_foto2\">\r\n            <div class=\"foto_mg\"><a href=\"javascript:openslideshow(\'/slideshow/5920333.cms\')\"><img border=\"0\" vspace=\"0\" marginheight=\"0\" marginwidth=\"0\" src=\"http://navbharattimes.indiatimes.com/thumb.cms?msid=5920333&amp;width=200&amp;resizemode=4\" alt=\"\" /></a>\r\n            <div class=\"ampliar\"><a href=\"javascript:openslideshow(\'/slideshow/5920333.cms\')\"><img border=\"0\" src=\"http://navbharattimes.indiatimes.com/photo.cms?msid=3000949\" alt=\"\" /></a></div>\r\n            </div>\r\n            </div>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\nहै। यह कहना है टीम इंडिया के कप्तान महेंद्र सिंह धोनी का। <br />\r\n<br />\r\nबुधवार को श्रीलंका से हार टी-20 वर्ल्ड कप से बाहर होने के बाद कप्तान धोनी ने कहा कि इस टूर्नामेंट में निराशाजनक प्रदर्शन का दोष आईपीएल पार्टीज़ और खिलाड़ियों की थकान को जाता है। प्रेस कॉन्फ्रेंस को संबोधित करने हुए धोनी ने कहा कि आईपीएल पार्टीज़ के चक्कर में खिलाड़ी काफी थकान महसूस कर रहे हैं।  </span><br />\r\n<br />\r\n<span style=\"font-size: 14px; font-weight: normal; color: rgb(0, 0, 0); font-family: ARIAL UNICODE MS,mangal,raghu8; line-height: 25px;\" name=\"test\" id=\"test\">उन्होंने कहा कि देर रात तक गेम खेलने और उसके बाद पार्टीज़ में शरीक होने के चलते खिलाड़ी बुरी तरह थक जाते हैं। उनका कहना है कि इन पार्टियों में भाग लेने के कारण हमारे खिलाड़ी बर्न आउट हो गए। यानी इतने थक जाते थे कि उन्हें ढंग से आराम तक करने का मौका नहीं मिला। खिलाड़ियों को यह सीखना पड़ेगा कि आखिर मैच और पार्टियों के बीच बैलेंस कैसे बनाएं ताकि उनमें आगे के मैचों के लिए एनर्जी बची रहे। धोनी ने कहा: शुक्र है अगले साल आईपीएल-4 का आयोजन क्रिकेट वर्ल्ड कप के बाद है। <br />\r\n<br />\r\nगौरतलब है कि अगले साल क्रिकेट वर्ल्ड कप फरवरी-मार्च में भारत, श्रीलंका और बांग्लादेश में होना है। पाकिस्तान भी एक सह-आयोजक था,  पर आतंकी गतिविधियों के कारण वहां से मेजबानी वापस ले ली गई है। आईपीएल-4  इस टूर्नामेंट के बाद आयोजित किया जाएगा। </span></p>\r\n<p>&nbsp;</p>','hindi',9,34);
/*!40000 ALTER TABLE `tbl_news_letter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_news_letter_attachment`
--

DROP TABLE IF EXISTS `tbl_news_letter_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_news_letter_attachment` (
  `news_letter_attachment_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_letter_details_id` int(11) NOT NULL,
  `document` text NOT NULL,
  PRIMARY KEY (`news_letter_attachment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_news_letter_attachment`
--

LOCK TABLES `tbl_news_letter_attachment` WRITE;
/*!40000 ALTER TABLE `tbl_news_letter_attachment` DISABLE KEYS */;
INSERT INTO `tbl_news_letter_attachment` VALUES (2,22,'Chrysanthemum.jpg'),(3,23,'Hydrangeas.jpg'),(4,47,'sachin-tendulkar-arjun-sara-dr-anjali01.jpg');
/*!40000 ALTER TABLE `tbl_news_letter_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_news_letter_attachment_temp`
--

DROP TABLE IF EXISTS `tbl_news_letter_attachment_temp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_news_letter_attachment_temp` (
  `news_letter_attachment_temp_id` int(11) NOT NULL AUTO_INCREMENT,
  `news_letter_details_id` int(11) NOT NULL,
  `document` text NOT NULL,
  PRIMARY KEY (`news_letter_attachment_temp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_news_letter_attachment_temp`
--

LOCK TABLES `tbl_news_letter_attachment_temp` WRITE;
/*!40000 ALTER TABLE `tbl_news_letter_attachment_temp` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_news_letter_attachment_temp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_news_letter_category`
--

DROP TABLE IF EXISTS `tbl_news_letter_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_news_letter_category` (
  `cat_id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `main_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `hierno` int(20) NOT NULL DEFAULT '0',
  `order_no` int(11) NOT NULL DEFAULT '0',
  `lang` text CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `type` varchar(200) NOT NULL,
  `photo` varchar(200) NOT NULL,
  `main_cat_id` int(11) NOT NULL,
  `category_tag` text NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_news_letter_category`
--

LOCK TABLES `tbl_news_letter_category` WRITE;
/*!40000 ALTER TABLE `tbl_news_letter_category` DISABLE KEYS */;
INSERT INTO `tbl_news_letter_category` VALUES (32,'Computer & IT','0',0,0,'english','cat','',32,''),(33,'Computer & IT','0',0,0,'greek','cat','',32,''),(34,'Sport & Fitness','0',0,0,'english','cat','',34,''),(35,'Sport & Fitness','0',0,0,'greek','cat','',34,''),(36,'Health & Buity','0',0,0,'english','cat','',36,''),(37,'Health & Buity','0',0,0,'greek','cat','',36,''),(38,'Art Craft & Hobies','0',0,0,'english','cat','',38,''),(39,'Art Craft & Hobies','0',0,0,'greek','cat','',38,''),(48,'Massage','36',1,0,'english','cat','',48,''),(49,'Μασάζ','36',1,0,'greek','cat','',48,''),(50,'Nutritional Therapy','36',1,0,'english','cat','',50,''),(51,'Θρεπτική θεραπεία','36',1,0,'greek','cat','',50,''),(52,'Personal Development','36',1,0,'english','cat','',52,''),(53,'Προσωπική Ανάπτυξη','36',1,0,'greek','cat','',52,''),(54,'Fashion Design','36',1,0,'english','cat','',54,''),(55,'Σχέδιο Μόδας','36',1,0,'greek','cat','',54,''),(56,'Beauty Care','36',1,0,'english','cat','',56,''),(57,'Beauty Care','36',1,0,'greek','cat','',56,''),(58,'Yoga','34',1,0,'english','cat','',58,''),(59,'Γιόγκα','34',1,0,'greek','cat','',58,''),(60,'Watersports','34',1,0,'english','cat','',60,''),(61,'Θαλάσσια σπορ','34',1,0,'greek','cat','',60,''),(62,'Fitness Instruction','34',1,0,'english','cat','',62,''),(63,'Fitness Instruction','34',1,0,'greek','cat','',62,''),(64,'Aerobics','34',1,0,'english','cat','',64,''),(65,'Αεροβική γυμναστική','34',1,0,'greek','cat','',64,''),(66,'Team Sports','34',1,0,'english','cat','',66,''),(67,'Ομάδα Αθλητισμός','34',1,0,'greek','cat','',66,''),(68,'Painting & Photography','38',1,0,'english','cat','',68,''),(69,'Ζωγραφική &amp; Φωτογραφία','38',1,0,'greek','cat','',68,''),(70,'Cookery','38',1,0,'english','cat','',70,''),(71,'Μαγειρική','38',1,0,'greek','cat','',70,''),(72,'Dress & Costume Design','38',1,0,'english','cat','',72,''),(73,'Φόρεμα &amp; Κοστούμια','38',1,0,'greek','cat','',72,''),(76,'Computer Programming','32',1,0,'english','cat','',76,''),(77,'Προγραμματισμός Η / Υ','32',1,0,'greek','cat','',76,''),(78,'Network+ Certification','32',1,0,'english','cat','',78,''),(79,'Δίκτυο + Πιστοποίηση','32',1,0,'greek','cat','',78,''),(80,'CAD CAM Design','32',1,0,'english','cat','',80,''),(81,'Σχεδιασμός CAD CAM','32',1,0,'greek','cat','',80,''),(82,'Microsoft Certified','32',1,0,'english','cat','',82,''),(83,'Microsoft Certified','32',1,0,'greek','cat','',82,''),(84,'PC Maintenance','32',1,0,'english','cat','',84,''),(85,'Συντήρηση Η / Υ','32',1,0,'greek','cat','',84,''),(86,'Web Design','32',1,0,'english','cat','',86,''),(87,'Web Design','32',1,0,'greek','cat','',86,'');
/*!40000 ALTER TABLE `tbl_news_letter_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_news_letter_details`
--

DROP TABLE IF EXISTS `tbl_news_letter_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_news_letter_details` (
  `news_letter_details_id` int(11) NOT NULL AUTO_INCREMENT,
  `posted_date` date DEFAULT NULL,
  `posted_time` time DEFAULT NULL,
  `send_date` date DEFAULT NULL,
  `msg_title` varchar(200) NOT NULL,
  `msg` text NOT NULL,
  `send_time` time NOT NULL,
  `smtp` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `sent_msg` int(2) NOT NULL,
  PRIMARY KEY (`news_letter_details_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_news_letter_details`
--

LOCK TABLES `tbl_news_letter_details` WRITE;
/*!40000 ALTER TABLE `tbl_news_letter_details` DISABLE KEYS */;
INSERT INTO `tbl_news_letter_details` VALUES (5,'2010-04-07','20:04:40','2010-04-07','Company information','<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><center>\r\n<table height=\"100%\" width=\"800\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n    <tbody>\r\n        <tr>\r\n            <td width=\"100%\">\r\n            <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n                <tbody>\r\n                    <tr>\r\n                        <td height=\"249\" width=\"100%\" background=\"http://demo.newsletter.pro//pictures/business3/top_back.gif\">\r\n                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n                            <tbody>\r\n                                <tr>\r\n                                    <td>\r\n                                    <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n                                        <tbody>\r\n                                            <tr>\r\n                                                <td height=\"208\"><img hspace=\"39\" border=\"0\" align=\"absMiddle\" src=\"http://demo.newsletter.pro//pictures/business3/logo.gif\" alt=\"\" /></td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td style=\"padding-left: 18px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\">e-mail: <a href=\"mailto:support@pilotgroup.net\" style=\"padding-left: 2px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\">support@pilotgroup.net</a>    <a href=\"http://www.pilotgroup.net/\" style=\"padding-left: 2px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\">http://www.pilotgroup.net/</a></td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                    </td>\r\n                                    <td align=\"right\"><img border=\"0\" align=\"right\" src=\"http://demo.newsletter.pro//pictures/business3/top_picture.gif\" alt=\"\" /></td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td width=\"100%\">\r\n            <table height=\"100%\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n                <tbody>\r\n                    <tr>\r\n                        <td width=\"100%\" valign=\"top\" bgcolor=\"#ffffff\">\r\n                        <table height=\"100%\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n                            <tbody>\r\n                                <tr>\r\n                                    <td valign=\"top\">\r\n                                    <table height=\"100%\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n                                        <tbody>\r\n                                            <tr>\r\n                                                <td valign=\"top\" style=\"padding: 21px;\"><font style=\"font-size: 11px; color: rgb(0, 46, 88); font-family: Tahoma;\"><strong>Dear %BASIC:FIRSTNAME% %BASIC:SECONDNAME% !</strong><br />\r\n                                                <br />\r\n                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.\r\n                                                <p style=\"padding-left: 30px; padding-bottom: 0px; padding-top: 0px;\"><a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">Our products </a><br />\r\n                                                <a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">About company </a><br />\r\n                                                <a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">Last news </a></p>\r\n                                                </font></td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td background=\"http://demo.newsletter.pro//pictures/business2/punt_line.gif\">&nbsp;</td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td style=\"padding: 21px;\"><img hspace=\"14\" height=\"30\" width=\"28\" border=\"0\" align=\"left\" src=\"http://demo.newsletter.pro//pictures/business3/house.gif\" style=\"width: 227px; height: 131px;\" alt=\"\" /> <font style=\"font-size: 11px; color: rgb(0, 46, 88); font-family: Tahoma; text-align: left;\">Iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.<br />\r\n                                                <br />\r\n                                                <a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">Our products </a><br />\r\n                                                <a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">About company </a><br />\r\n                                                <a href=\"http://demo.newsletter.pro//template/change.php?id=28#\" style=\"color: rgb(183, 31, 24);\">Last news </a><br />\r\n                                                </font></td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td height=\"100%\">&nbsp;</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td height=\"39\" width=\"100%\" valign=\"center\" bgcolor=\"#bdd1e5\" align=\"middle\" style=\"font-size: 11px; color: rgb(188, 90, 92); font-family: Tahoma;\">Copyright 2005 Р&nbsp;вЂ&trade;Р&rsquo;В&copy; All right reserved</td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n</center></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>','00:00:00',1,3,1),(18,'2010-04-14','16:56:01','2010-04-14','This is subject','<p>This is message</p>','16:55:14',1,7,1),(19,'2010-04-14','17:26:44','2010-04-14','PHP Computer Program ','<p>&nbsp;</p>\r\n<p><center>\r\n<table height=\"100%\" width=\"800\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n    <tbody>\r\n        <tr>\r\n            <td width=\"100%\">\r\n            <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n                <tbody>\r\n                    <tr>\r\n                        <td height=\"249\" width=\"100%\" background=\"http://demo.newsletter.pro//pictures/business3/top_back.gif\">\r\n                        <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n                            <tbody>\r\n                                <tr>\r\n                                    <td>\r\n                                    <table width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n                                        <tbody>\r\n                                            <tr>\r\n                                                <td height=\"208\"><img hspace=\"39\" border=\"0\" align=\"absMiddle\" alt=\"\" src=\"http://demo.newsletter.pro//pictures/business3/logo.gif\" /></td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td style=\"padding-left: 18px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\">e-mail: <a style=\"padding-left: 2px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\" href=\"mailto:support@pilotgroup.net\">support@pilotgroup.net</a>    <a style=\"padding-left: 2px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\" href=\"http://www.pilotgroup.net/\">http://www.pilotgroup.net/</a></td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                    </td>\r\n                                    <td align=\"right\"><img border=\"0\" align=\"right\" alt=\"\" src=\"http://demo.newsletter.pro//pictures/business3/top_picture.gif\" /></td>\r\n                                    <td align=\"right\">&nbsp;</td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td width=\"100%\">\r\n            <table height=\"100%\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n                <tbody>\r\n                    <tr>\r\n                        <td width=\"100%\" valign=\"top\" bgcolor=\"#ffffff\">\r\n                        <table height=\"100%\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n                            <tbody>\r\n                                <tr>\r\n                                    <td valign=\"top\">\r\n                                    <table height=\"100%\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\">\r\n                                        <tbody>\r\n                                            <tr>\r\n                                                <td valign=\"top\" style=\"padding: 21px;\"><font style=\"font-size: 11px; color: rgb(0, 46, 88); font-family: Tahoma;\"><strong>Dear %BASIC:FIRSTNAME% %BASIC:SECONDNAME% !</strong><br />\r\n                                                <br />\r\n                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.\r\n                                                <p style=\"padding-left: 30px; padding-bottom: 0px; padding-top: 0px;\"><a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">Our products </a><br />\r\n                                                <a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">About company </a><br />\r\n                                                <a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">Last news </a></p>\r\n                                                </font></td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td background=\"http://demo.newsletter.pro//pictures/business2/punt_line.gif\">&nbsp;</td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td style=\"padding: 21px;\"><img hspace=\"14\" height=\"30\" width=\"28\" border=\"0\" align=\"left\" alt=\"\" style=\"width: 227px; height: 131px;\" src=\"http://demo.newsletter.pro//pictures/business3/house.gif\" /> <font style=\"font-size: 11px; color: rgb(0, 46, 88); font-family: Tahoma; text-align: left;\">Iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.<br />\r\n                                                <br />\r\n                                                <a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">Our products </a><br />\r\n                                                <a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">About company </a><br />\r\n                                                <a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">Last news </a><br />\r\n                                                </font></td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td height=\"100%\">&nbsp;</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td height=\"39\" width=\"100%\" valign=\"center\" bgcolor=\"#bdd1e5\" align=\"middle\" style=\"font-size: 11px; color: rgb(188, 90, 92); font-family: Tahoma;\">Copyright 2005 Р&nbsp;вЂ&trade;Р&rsquo;В&copy; All right reserved</td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n</center></p>\r\n<p>&nbsp;</p>','17:25:53',1,3,1),(26,'2010-04-16','16:27:26','2010-04-16','Yoga news letter','<p><span class=\"Title\">Yoga letter</span></p>','16:24:04',3,7,1),(27,'2010-04-16','17:04:28','2010-04-16','PHP Computer Program ','<p>&nbsp;</p>\r\n<p><center>\r\n<table height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"800\">\r\n    <tbody>\r\n        <tr>\r\n            <td width=\"100%\">\r\n            <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                <tbody>\r\n                    <tr>\r\n                        <td height=\"249\" background=\"http://demo.newsletter.pro//pictures/business3/top_back.gif\" width=\"100%\">\r\n                        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                            <tbody>\r\n                                <tr>\r\n                                    <td>\r\n                                    <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                                        <tbody>\r\n                                            <tr>\r\n                                                <td height=\"208\"><img hspace=\"39\" border=\"0\" align=\"absMiddle\" alt=\"\" src=\"http://demo.newsletter.pro//pictures/business3/logo.gif\" /></td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td style=\"padding-left: 18px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\">e-mail: <a style=\"padding-left: 2px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\" href=\"mailto:support@pilotgroup.net\">support@pilotgroup.net</a>    <a style=\"padding-left: 2px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\" href=\"http://www.pilotgroup.net/\">http://www.pilotgroup.net/</a></td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                    </td>\r\n                                    <td align=\"right\"><img border=\"0\" align=\"right\" alt=\"\" src=\"http://demo.newsletter.pro//pictures/business3/top_picture.gif\" /></td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td width=\"100%\">\r\n            <table height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                <tbody>\r\n                    <tr>\r\n                        <td bgcolor=\"#ffffff\" width=\"100%\" valign=\"top\">\r\n                        <table height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                            <tbody>\r\n                                <tr>\r\n                                    <td valign=\"top\">\r\n                                    <table height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                                        <tbody>\r\n                                            <tr>\r\n                                                <td valign=\"top\" style=\"padding: 21px;\"><font style=\"font-size: 11px; color: rgb(0, 46, 88); font-family: Tahoma;\"><strong>Dear %BASIC:FIRSTNAME% %BASIC:SECONDNAME% !</strong><br />\r\n                                                <br />\r\n                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.\r\n                                                <p style=\"padding-left: 30px; padding-bottom: 0px; padding-top: 0px;\"><a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">Our products </a><br />\r\n                                                <a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">About company </a><br />\r\n                                                <a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">Last news </a></p>\r\n                                                </font></td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td background=\"http://demo.newsletter.pro//pictures/business2/punt_line.gif\">&nbsp;</td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td style=\"padding: 21px;\"><img hspace=\"14\" height=\"30\" border=\"0\" align=\"left\" width=\"28\" alt=\"\" style=\"width: 227px; height: 131px;\" src=\"http://demo.newsletter.pro//pictures/business3/house.gif\" /> <font style=\"font-size: 11px; color: rgb(0, 46, 88); font-family: Tahoma; text-align: left;\">Iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.<br />\r\n                                                <br />\r\n                                                <a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">Our products </a><br />\r\n                                                <a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">About company </a><br />\r\n                                                <a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">Last news </a><br />\r\n                                                </font></td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td height=\"100%\">&nbsp;</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td height=\"39\" bgcolor=\"#bdd1e5\" align=\"middle\" width=\"100%\" valign=\"center\" style=\"font-size: 11px; color: rgb(188, 90, 92); font-family: Tahoma;\">Copyright 2005 Р&nbsp;вЂ&trade;Р&rsquo;В&copy; All right reserved</td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n</center></p>\r\n<p>&nbsp;</p>','17:04:10',3,3,1),(29,'2010-04-16','17:27:06','2010-04-16','ASP.net programming Class','<p><span class=\"Title\">Testing by nil</span></p>\r\n<p>s</p>\r\n<p>dsd</p>\r\n<p><span class=\"Code\">sds</span></p>','17:25:20',3,3,1),(30,'2010-04-16','17:45:04','2010-04-16','Auto Cad 14','<p>Networking latest news in english lagu</p>\r\n<p>&nbsp;</p>\r\n<p><span class=\"Title\">age</span></p>','17:44:44',3,7,1),(31,'2010-04-16','17:57:12','2010-04-16','Auto Cad 14','<p>Networking latest news<span class=\"Title\"> in english laguage</span></p>','17:56:53',3,3,1),(32,'2010-04-16','17:59:38','2010-04-16','Auto Cad 14','<p>Networking latest news in engl</p>\r\n<p>&nbsp;</p>\r\n<p><span class=\"Code\">sdsdssdsish laguage</span></p>','17:59:19',3,3,1),(33,'2010-04-16','18:01:19','2010-04-16','PHP Computer Program ','<p>&nbsp;</p>\r\n<p><center>\r\n<table height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"800\">\r\n    <tbody>\r\n        <tr>\r\n            <td width=\"100%\">\r\n            <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                <tbody>\r\n                    <tr>\r\n                        <td height=\"249\" background=\"http://demo.newsletter.pro//pictures/business3/top_back.gif\" width=\"100%\">\r\n                        <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                            <tbody>\r\n                                <tr>\r\n                                    <td>\r\n                                    <table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                                        <tbody>\r\n                                            <tr>\r\n                                                <td height=\"208\"><img hspace=\"39\" border=\"0\" align=\"absMiddle\" alt=\"\" src=\"http://demo.newsletter.pro//pictures/business3/logo.gif\" /></td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td style=\"padding-left: 18px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\">e-mail: <a style=\"padding-left: 2px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\" href=\"mailto:support@pilotgroup.net\">support@pilotgroup.net</a>    <a style=\"padding-left: 2px; font-size: 11px; color: rgb(188, 32, 25); font-family: Tahoma;\" href=\"http://www.pilotgroup.net/\">http://www.pilotgroup.net/</a></td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                    </td>\r\n                                    <td align=\"right\"><img border=\"0\" align=\"right\" alt=\"\" src=\"http://demo.newsletter.pro//pictures/business3/top_picture.gif\" /></td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            </td>\r\n        </tr>\r\n        <tr>\r\n            <td width=\"100%\">\r\n            <table height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                <tbody>\r\n                    <tr>\r\n                        <td bgcolor=\"#ffffff\" width=\"100%\" valign=\"top\">\r\n                        <table height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                            <tbody>\r\n                                <tr>\r\n                                    <td valign=\"top\">\r\n                                    <table height=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" width=\"100%\">\r\n                                        <tbody>\r\n                                            <tr>\r\n                                                <td valign=\"top\" style=\"padding: 21px;\"><font style=\"font-size: 11px; color: rgb(0, 46, 88); font-family: Tahoma;\"><strong>Dear %BASIC:FIRSTNAME% %BASIC:SECONDNAME% !</strong><br />\r\n                                                <br />\r\n                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.\r\n                                                <p style=\"padding-left: 30px; padding-bottom: 0px; padding-top: 0px;\"><a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">Our products </a><br />\r\n                                                <a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">About company </a><br />\r\n                                                <a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">Last news </a></p>\r\n                                                </font></td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td background=\"http://demo.newsletter.pro//pictures/business2/punt_line.gif\">&nbsp;</td>\r\n                                            </tr>\r\n                                            <tr>\r\n                                                <td style=\"padding: 21px;\"><img hspace=\"14\" height=\"30\" border=\"0\" align=\"left\" width=\"28\" alt=\"\" style=\"width: 227px; height: 131px;\" src=\"http://demo.newsletter.pro//pictures/business3/house.gif\" /> <font style=\"font-size: 11px; color: rgb(0, 46, 88); font-family: Tahoma; text-align: left;\">Iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.Iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.<br />\r\n                                                <br />\r\n                                                <a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">Our products </a><br />\r\n                                                <a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">About company </a><br />\r\n                                                <a style=\"color: rgb(183, 31, 24);\" href=\"http://demo.newsletter.pro//template/change.php?id=28#\">Last news </a><br />\r\n                                                </font></td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                    </td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td height=\"100%\">&nbsp;</td>\r\n                                </tr>\r\n                                <tr>\r\n                                    <td height=\"39\" bgcolor=\"#bdd1e5\" align=\"middle\" width=\"100%\" valign=\"center\" style=\"font-size: 11px; color: rgb(188, 90, 92); font-family: Tahoma;\">Copyright 2005 Р&nbsp;вЂ&trade;Р&rsquo;В&copy; All right reserved</td>\r\n                                </tr>\r\n                            </tbody>\r\n                        </table>\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            </td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n</center></p>\r\n<p>&nbsp;</p>','18:01:03',3,3,1),(34,'2010-04-16','20:03:17','2010-04-16','Networking News','<p>\r\n<tbody>\r\n    <tr>\r\n        <td><font size=\"2\" face=\"Verdana\">\r\n        <p align=\"justify\"><font size=\"2\" face=\"Verdana\">&Epsilon;&mu;&epsilon;ί&sigmaf; &sigma;&tau;&eta; <strong>NET  FORCE</strong> <strong><u>&nu;&omicron;&iota;&alpha;&zeta;ό&mu;&alpha;&sigma;&tau;&epsilon;</u> &gamma;&iota;&alpha;</strong> &tau;&omicron;&upsilon;&sigmaf; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf;  &kappa;&alpha;&iota; &sigma;&kappa;&omicron;&pi;ό&sigmaf; &mu;&alpha;&sigmaf; &epsilon;ί&nu;&alpha;&iota; &nu;&alpha; &epsilon;ί&mu;&alpha;&sigma;&tau;&epsilon; <strong>&omicron; <span lang=\"el\">&alpha;&gamma;&alpha;&pi;&eta;&mu;έ&nu;&omicron;&sigmaf;  &sigma;&alpha;&sigmaf; </span>&sigma;&upsilon;&nu;&epsilon;&rho;&gamma;ά&tau;&eta;&sigmaf; </strong><span lang=\"el\">&gamma;</span>&iota;&alpha; ό&tau;&iota; &alpha;&phi;&omicron;&rho;ά &sigma;&tau;&omicron;  Internet!</font></p>\r\n        <p align=\"justify\"><font size=\"2\" face=\"Verdana\">&Pi;&alpha;&rho;έ&chi;&omicron;&upsilon;&mu;&epsilon; <strong>&pi;&omicron;&iota;&omicron;&tau;&iota;&kappa;έ&sigmaf;  <span lang=\"el\">&upsilon;&pi;&eta;&rho;&epsilon;&sigma;ί&epsilon;&sigmaf; &sigma;&epsilon; &alpha;&nu;&tau;&alpha;&gamma;&omega;&nu;&iota;&sigma;&tau;&iota;&kappa;έ&sigmaf; &tau;&iota;&mu;έ&sigmaf; &kappa;&alpha;&iota; </span>&sigma;&alpha;&sigmaf;  &kappa;&alpha;&lambda;&omicron;ύ&mu;&epsilon; &nu;&alpha; <span lang=\"el\">&tau;&iota;&sigmaf; </span>&sigma;&upsilon;&gamma;&kappa;&rho;ί&nu;&epsilon;&tau;&epsilon;<span lang=\"el\"> </span>...</strong></font></p>\r\n        <p align=\"center\"><span lang=\"el\"><font size=\"2\">&Upsilon;&pi;ά&rho;&chi;&omicron;&upsilon;&nu;  &kappa;&alpha;&iota; &kappa;ά&pi;&omicron;&iota;&alpha; &theta;έ&mu;&alpha;&tau;&alpha; &pi;&omicron;&upsilon; &delta;&epsilon;&nu; &theta;&alpha; &mu;&pi;&omicron;&rho;έ&sigma;&epsilon;&tau;&epsilon; &nu;&alpha; &mu;&alpha;&sigmaf; &sigma;&upsilon;&gamma;&kappa;&rho;ί&nu;&epsilon;&tau;&epsilon; ! </font></span></p>\r\n        <p align=\"justify\"><strong><font size=\"2\" /></strong></p>\r\n        </font></td>\r\n        <td align=\"right\" valign=\"top\">\r\n        <p><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\" color=\"#000080\"><font size=\"2\" face=\"Verdana\"><img height=\"302\" width=\"270\" alt=\"\" src=\"http://netforce.gr/images/netforce/klidi.jpg\" /></font></font></font></p>\r\n        </td>\r\n    </tr>\r\n</tbody>\r\n</p>','20:00:14',3,3,1),(35,'2010-04-16','21:04:09','2010-04-16','testing','<p>testing in english laguage</p>\r\n<p>&nbsp;</p>\r\n<table class=\"contentpaneopen\">\r\n    <tbody>\r\n        <tr>\r\n            <td valign=\"top\" colspan=\"2\"><font size=\"2\" face=\"Verdana\">\r\n            <table cellspacing=\"1\" border=\"0\" align=\"left\" width=\"540\">\r\n                <tbody>\r\n                    <tr>\r\n                        <td>\r\n                        <p align=\"justify\"><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\">&Epsilon;&mu;&epsilon;ί&sigmaf; &sigma;&tau;&eta; <strong>NET FORCE</strong>  <strong><u>&nu;&omicron;&iota;&alpha;&zeta;ό&mu;&alpha;&sigma;&tau;&epsilon;</u> &gamma;&iota;&alpha;</strong> &tau;&omicron;&upsilon;&sigmaf; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &kappa;&alpha;&iota; &sigma;&kappa;&omicron;&pi;ό&sigmaf; &mu;&alpha;&sigmaf;  &epsilon;ί&nu;&alpha;&iota; &nu;&alpha; &epsilon;ί&mu;&alpha;&sigma;&tau;&epsilon; <strong>&omicron; <span lang=\"el\">&alpha;&gamma;&alpha;&pi;&eta;&mu;έ&nu;&omicron;&sigmaf; &sigma;&alpha;&sigmaf; </span>&sigma;&upsilon;&nu;&epsilon;&rho;&gamma;ά&tau;&eta;&sigmaf;  </strong><span lang=\"el\">&gamma;</span>&iota;&alpha; ό&tau;&iota; &alpha;&phi;&omicron;&rho;ά &sigma;&tau;&omicron; Internet!</font> </font></p>\r\n                        <font size=\"2\" face=\"Verdana\">            </font>\r\n                        <p align=\"justify\"><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\">&Pi;&alpha;&rho;έ&chi;&omicron;&upsilon;&mu;&epsilon; <strong>&pi;&omicron;&iota;&omicron;&tau;&iota;&kappa;έ&sigmaf;  <span lang=\"el\">&upsilon;&pi;&eta;&rho;&epsilon;&sigma;ί&epsilon;&sigmaf; &sigma;&epsilon; &alpha;&nu;&tau;&alpha;&gamma;&omega;&nu;&iota;&sigma;&tau;&iota;&kappa;έ&sigmaf; &tau;&iota;&mu;έ&sigmaf; &kappa;&alpha;&iota; </span>&sigma;&alpha;&sigmaf;  &kappa;&alpha;&lambda;&omicron;ύ&mu;&epsilon; &nu;&alpha; <span lang=\"el\">&tau;&iota;&sigmaf; </span>&sigma;&upsilon;&gamma;&kappa;&rho;ί&nu;&epsilon;&tau;&epsilon;<span lang=\"el\"> </span>...</strong></font>  </font></p>\r\n                        <font size=\"2\" face=\"Verdana\">            </font>\r\n                        <p align=\"center\"><span lang=\"el\"><font size=\"2\" face=\"Verdana\"><font size=\"2\">&Upsilon;&pi;ά&rho;&chi;&omicron;&upsilon;&nu; &kappa;&alpha;&iota;  &kappa;ά&pi;&omicron;&iota;&alpha; &theta;έ&mu;&alpha;&tau;&alpha; &pi;&omicron;&upsilon; &delta;&epsilon;&nu; &theta;&alpha; &mu;&pi;&omicron;&rho;έ&sigma;&epsilon;&tau;&epsilon; &nu;&alpha; &mu;&alpha;&sigmaf; &sigma;&upsilon;&gamma;&kappa;&rho;ί&nu;&epsilon;&tau;&epsilon; ! </font></font></span></p>\r\n                        <font size=\"2\" face=\"Verdana\">            </font>\r\n                        <p align=\"justify\"><font size=\"2\" face=\"Verdana\"><strong><font size=\"2\" /></strong></font></p>\r\n                        <font size=\"2\" face=\"Verdana\">            </font></td>\r\n                        <td align=\"right\" valign=\"top\">\r\n                        <p><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\" color=\"#000080\"><font size=\"2\" face=\"Verdana\"><img height=\"302\" width=\"270\" alt=\"\" src=\"http://netforce.gr/images/netforce/klidi.jpg\" /></font></font></font></p>\r\n                        </td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td colspan=\"2\"><span lang=\"el\"><font size=\"2\">\r\n                        <p align=\"justify\"><strong><span lang=\"el\"><font size=\"2\">&Theta;&upsilon;&mu;ό&mu;&alpha;&sigma;&tau;&epsilon;  &tau;&omicron;&upsilon; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &chi;&omega;&rho;ί&sigmaf; &nu;&alpha; &psi;ά&chi;&nu;&omicron;&upsilon;&mu;&epsilon; &kappa;ά&pi;&omicron;&iota;&alpha; </font></span><font size=\"2\">database  <span lang=\"el\">&epsilon;&nu;ώ &delta;&epsilon;&nu; &alpha;&pi;&alpha;&iota;&tau;&omicron;ύ&mu;&epsilon; &nu;&alpha; &mu;&alpha;&sigmaf; &sigma;&tau;&epsilon;ί&lambda;&epsilon;&tau;&epsilon; &tau;&eta; &tau;&alpha;&upsilon;&tau;ό&tau;&eta;&tau;ά &sigma;&alpha;&sigmaf; &mu;&epsilon; </span>fax  <span lang=\"el\">&pi;&rho;&omicron;&kappa;&epsilon;&iota;&mu;έ&nu;&omicron;&upsilon; &gamma;&iota;&alpha; &nu;&alpha; &sigma;&alpha;&sigmaf; &epsilon;&xi;&upsilon;&pi;&eta;&rho;&epsilon;&tau;ή&sigma;&omicron;&upsilon;&mu;&epsilon;...!</span></font></strong></p>\r\n                        <p align=\"justify\">&Sigma;ύ&nu;&tau;&omicron;&mu;&alpha; &theta;&alpha; &mu;&alpha;&sigmaf; &theta;&upsilon;&mu;ά&sigma;&tau;&epsilon; &kappa;&alpha;&iota; &epsilon;&sigma;&epsilon;ί&sigmaf; &kappa;&alpha;&theta;ό&sigma;&omicron;&nu;  &theta;&alpha; &xi;έ&rho;&epsilon;&tau;&epsilon; &mu;&epsilon; &pi;&omicron;&iota;ό&nu; &theta;&alpha; &epsilon;&pi;&iota;&kappa;&omicron;&iota;&nu;&omega;&nu;ή&sigma;&epsilon;&tau;&epsilon; &kappa;&alpha;&iota; &delta;&epsilon;&nu; &theta;&alpha; &chi;&rho;&epsilon;&iota;ά&zeta;&epsilon;&tau;&alpha;&iota; &nu;&alpha;  &quot;&xi;&alpha;&nu;&alpha;&sigma;&upsilon;&sigma;&tau;&eta;&theta;&epsilon;ί&tau;&epsilon;&quot;...</p>\r\n                        <p align=\"justify\"><span lang=\"el\"><font size=\"2\">&Eta; </font></span><font size=\"2\">NET FORCE </font><span lang=\"el\"><font size=\"2\">&epsilon;ί&nu;&alpha;&iota; &alpha;&rho;&kappa;&epsilon;&tau;ά <strong>&mu;&epsilon;&gamma;ά&lambda;&eta;</strong>  &gamma;&iota;&alpha; &nu;&alpha; &mu;&pi;&omicron;&rho;&epsilon;ί &nu;&alpha; &sigma;&alpha;&sigmaf; <strong>&pi;&alpha;&rho;έ&chi;&epsilon;&iota; &tau;&iota;&sigmaf; &lambda;ύ&sigma;&epsilon;&iota;&sigmaf; &pi;&omicron;&upsilon; &lambda;ί&gamma;&omicron;&iota; &theta;&alpha; &mu;&pi;&omicron;&rho;&omicron;ύ&sigma;&alpha;&nu;</strong>  &kappa;&alpha;&iota; <strong>&alpha;&rho;&kappa;&epsilon;&tau;ά &mu;&iota;&kappa;&rho;ή &gamma;&iota;&alpha; &nu;&alpha; &sigma;&alpha;&sigmaf; &epsilon;&xi;&upsilon;&pi;&eta;&rho;&epsilon;&tau;&epsilon;ί &pi;&rho;&omicron;&sigma;&omega;&pi;&iota;&kappa;ά, &mu;&epsilon;  &epsilon;&pi;&alpha;&gamma;&gamma;&epsilon;&lambda;&mu;&alpha;&tau;&iota;&sigma;&mu;ό &kappa;&alpha;&iota; &mu;&epsilon;&rho;ά&kappa;&iota;.</strong></font></span></p>\r\n                        <p align=\"justify\"><span lang=\"el\"><font size=\"2\">&Pi;&alpha;&rho;έ&chi;&omicron;&upsilon;&mu;&epsilon;  &tau;&iota;&sigmaf; &upsilon;&pi;&eta;&rho;&epsilon;&sigma;ί&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &alpha;&pi;ό &tau;ό&tau;&epsilon; &pi;&omicron;&upsilon; &tau;&omicron; </font></span><font size=\"2\">Internet <span lang=\"el\">&sigma;&tau;&eta;&nu; &Epsilon;&lambda;&lambda;ά&delta;&alpha; ή&tau;&alpha;&nu; &quot;ά&gamma;&nu;&omega;&sigma;&tau;&eta; &lambda;έ&xi;&eta;&quot; &kappa;&alpha;&iota; &pi;&omicron;&upsilon; &omicron;&iota; &epsilon;&tau;&alpha;&iota;&rho;ί&epsilon;&sigmaf; &sigma;&tau;&omicron; &chi;ώ&rho;&omicron;  &mu;&alpha;&sigmaf; ή&tau;&alpha;&nu; &lambda;&iota;&gamma;ό&tau;&epsilon;&rho;&epsilon;&sigmaf; &alpha;&pi;ό 10. <strong>&Epsilon;ί&mu;&alpha;&sigma;&tau;&epsilon; &pi;&alpha;&rho;ό&nu;&tau;&epsilon;&sigmaf; 12 &chi;&rho;ό&nu;&iota;&alpha; !</strong></span></font></p>\r\n                        <p align=\"justify\"><span lang=\"el\"><font size=\"2\">&Eta; </font></span><font size=\"2\"><strong>NET FORCE</strong> <span lang=\"el\">&delta;&epsilon;&nu; &delta;&eta;&mu;&iota;&omicron;&upsilon;&rho;&gamma;ή&theta;&eta;&kappa;&epsilon;  &mu;&epsilon; &chi;&rho;ή&mu;&alpha;&tau;&alpha; &epsilon;&tau;&alpha;&iota;&rho;&iota;ώ&nu; &tau;&omicron;&upsilon; &delta;&eta;&mu;&omicron;&sigma;ί&omicron;&upsilon; ή &epsilon;&pi;&iota;&chi;&omicron;&rho;&eta;&gamma;ή&sigma;&epsilon;&iota;&sigmaf; &mu;&epsilon; &alpha;&pi;&omicron;&tau;έ&lambda;&epsilon;&sigma;&mu;&alpha; &nu;&alpha; <u>&tau;&eta;&nu;  &gamma;&nu;&omega;&rho;ί&zeta;&omicron;&upsilon;&nu; &omicron;&iota; &quot;&pi;&alpha;&lambda;&iota;&omicron;ί&quot; &sigma;&tau;&omicron; </u></span><u>Internet <span lang=\"el\">&kappa;&alpha;&iota;  ό&chi;&iota; ό&sigma;&omicron;&iota;</span> </u></font><span lang=\"el\"><font size=\"2\"><u>&beta;&lambda;έ&pi;&omicron;&upsilon;&nu;  &tau;&eta;&lambda;&epsilon;ό&rho;&alpha;&sigma;&eta;</u>. &Tau;&omicron; ά&mu;&epsilon;&sigma;&omicron; &kappa;έ&rho;&delta;&omicron;&sigmaf; &gamma;&iota;&alpha; &tau;&omicron;&upsilon;&sigmaf; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &epsilon;ί&nu;&alpha;&iota; &nu;&alpha; &mu;&eta;&nu;  &chi;&rho;&epsilon;ώ&nu;&omicron;&nu;&tau;&alpha;&iota; &mu;&epsilon; &tau;&omicron; &kappa;ό&sigma;&tau;&omicron;&sigmaf; &tau;&omega;&nu; &pi;&alpha;&nu;ά&kappa;&rho;&iota;&beta;&omega;&nu; &delta;&iota;&alpha;&phi;&eta;&mu;ί&sigma;&epsilon;&omega;&nu; &pi;&omicron;&upsilon; &alpha;&nu;&alpha;&gamma;&kappa;&alpha;&sigma;&tau;&iota;&kappa;ά &theta;&alpha;  &epsilon;&pi;&iota;&beta;ά&rho;&upsilon;&nu;&alpha;&nu; &tau;&omicron; &kappa;ό&sigma;&tau;&omicron;&sigmaf; &tau;&omega;&nu; &upsilon;&pi;&eta;&rho;&epsilon;&sigma;&iota;ώ&nu; &mu;&alpha;&sigmaf;... !</font></span></p>\r\n                        <p align=\"justify\"><font size=\"2\" face=\"Verdana\"><strong><span lang=\"el\">&Mu;&iota;&lambda;ή&sigma;&tau;&epsilon; </span>&mu;&alpha;&sigmaf; &tau;ώ&rho;&alpha;</strong> &sigma;&tau;&omicron; <strong>210-5236749</strong>  ή <a href=\"http://netforce.gr/index.php?option=com_chronocontact&amp;chronoformname=general1\">&sigma;&tau;&epsilon;ί&lambda;&tau;&epsilon;  &mu;&alpha;&sigmaf; &tau;&omicron; &mu;ή&nu;&upsilon;&mu;ά &sigma;&alpha;&sigmaf; &eta;&lambda;&epsilon;&kappa;&tau;&rho;&omicron;&nu;&iota;&kappa;ά</a><span lang=\"el\"> &sigma;&upsilon;&mu;&pi;&lambda;&eta;&rho;ώ&nu;&omicron;&nu;&tau;&alpha;&sigmaf; &tau;&eta;&nu;  &sigma;&chi;&epsilon;&tau;&iota;&kappa;ή &phi;ό&rho;&mu;&alpha; &epsilon;&pi;&iota;&kappa;&omicron;&iota;&nu;&omega;&nu;ί&alpha;&sigmaf;</span>.</font></p>\r\n                        <p align=\"justify\"><font size=\"2\" face=\"Tahoma\">&Sigma;&alpha;&sigmaf;  &epsilon;&upsilon;&chi;&alpha;&rho;&iota;&sigma;&tau;&omicron;ύ&mu;&epsilon; <span lang=\"el\">&gamma;&iota;&alpha; &tau;&eta;&nu; &eta;&lambda;&epsilon;&kappa;&tau;&rho;&omicron;&nu;&iota;&kappa;ή &sigma;&alpha;&sigmaf; &epsilon;&pi;ί&sigma;&kappa;&epsilon;&psi;&eta; </span>&kappa;&alpha;&iota;  &theta;&alpha; </font><font size=\"2\" face=\"Verdana\">&chi;&alpha;&rho;&omicron;ύ&mu;&epsilon; &nu;&alpha; &sigma;&alpha;&sigmaf; &epsilon;&xi;&upsilon;&pi;&eta;&rho;&epsilon;&tau;ή&sigma;&omicron;&upsilon;&mu;&epsilon;.</font></p>\r\n                        <p align=\"justify\">&nbsp;</p>\r\n                        <p align=\"center\"><img height=\"103\" border=\"0\" width=\"216\" alt=\" Your\r\n                        ALT-Text here \" src=\"http://www.netforce.gr/images/paraskevopoulos2.gif\" /></p>\r\n                        <p align=\"center\"><font size=\"2\" face=\"Tahoma\" color=\"#000000\"><strong>&Nu;&iota;&kappa;ό&lambda;&alpha;&sigmaf; &Pi;&alpha;&rho;&alpha;&sigma;&kappa;&epsilon;&upsilon;ό&pi;&omicron;&upsilon;&lambda;&omicron;&sigmaf;, General Manager</strong></font></p>\r\n                        </font></span></td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            </font></td>\r\n        </tr>\r\n        <tr>\r\n            <td align=\"left\" class=\"modifydate\" colspan=\"2\">Last Updated ( Wednesday, 19 September 2007 )</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<p>&nbsp;  		<span class=\"article_seperator\" /></p>','21:03:09',3,3,1),(36,'2010-04-16','21:08:31','2010-04-16','Auto Cad 14','<p>Networking latest news in english laguage</p>\r\n<table class=\"contentpaneopen\">\r\n    <tbody>\r\n        <tr>\r\n            <td valign=\"top\" colspan=\"2\"><font size=\"2\" face=\"Verdana\">\r\n            <table cellspacing=\"1\" border=\"0\" align=\"left\" width=\"540\">\r\n                <tbody>\r\n                    <tr>\r\n                        <td>\r\n                        <p align=\"justify\"><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\">&Epsilon;&mu;&epsilon;ί&sigmaf; &sigma;&tau;&eta; <strong>NET FORCE</strong>  <strong><u>&nu;&omicron;&iota;&alpha;&zeta;ό&mu;&alpha;&sigma;&tau;&epsilon;</u> &gamma;&iota;&alpha;</strong> &tau;&omicron;&upsilon;&sigmaf; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &kappa;&alpha;&iota; &sigma;&kappa;&omicron;&pi;ό&sigmaf; &mu;&alpha;&sigmaf;  &epsilon;ί&nu;&alpha;&iota; &nu;&alpha; &epsilon;ί&mu;&alpha;&sigma;&tau;&epsilon; <strong>&omicron; <span lang=\"el\">&alpha;&gamma;&alpha;&pi;&eta;&mu;έ&nu;&omicron;&sigmaf; &sigma;&alpha;&sigmaf; </span>&sigma;&upsilon;&nu;&epsilon;&rho;&gamma;ά&tau;&eta;&sigmaf;  </strong><span lang=\"el\">&gamma;</span>&iota;&alpha; ό&tau;&iota; &alpha;&phi;&omicron;&rho;ά &sigma;&tau;&omicron; Internet!</font> </font></p>\r\n                        <font size=\"2\" face=\"Verdana\">            </font>\r\n                        <p align=\"justify\"><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\">&Pi;&alpha;&rho;έ&chi;&omicron;&upsilon;&mu;&epsilon; <strong>&pi;&omicron;&iota;&omicron;&tau;&iota;&kappa;έ&sigmaf;  <span lang=\"el\">&upsilon;&pi;&eta;&rho;&epsilon;&sigma;ί&epsilon;&sigmaf; &sigma;&epsilon; &alpha;&nu;&tau;&alpha;&gamma;&omega;&nu;&iota;&sigma;&tau;&iota;&kappa;έ&sigmaf; &tau;&iota;&mu;έ&sigmaf; &kappa;&alpha;&iota; </span>&sigma;&alpha;&sigmaf;  &kappa;&alpha;&lambda;&omicron;ύ&mu;&epsilon; &nu;&alpha; <span lang=\"el\">&tau;&iota;&sigmaf; </span>&sigma;&upsilon;&gamma;&kappa;&rho;ί&nu;&epsilon;&tau;&epsilon;<span lang=\"el\"> </span>...</strong></font>  </font></p>\r\n                        <font size=\"2\" face=\"Verdana\">            </font>\r\n                        <p align=\"center\"><span lang=\"el\"><font size=\"2\" face=\"Verdana\"><font size=\"2\">&Upsilon;&pi;ά&rho;&chi;&omicron;&upsilon;&nu; &kappa;&alpha;&iota;  &kappa;ά&pi;&omicron;&iota;&alpha; &theta;έ&mu;&alpha;&tau;&alpha; &pi;&omicron;&upsilon; &delta;&epsilon;&nu; &theta;&alpha; &mu;&pi;&omicron;&rho;έ&sigma;&epsilon;&tau;&epsilon; &nu;&alpha; &mu;&alpha;&sigmaf; &sigma;&upsilon;&gamma;&kappa;&rho;ί&nu;&epsilon;&tau;&epsilon; ! </font></font></span></p>\r\n                        <font size=\"2\" face=\"Verdana\">            </font>\r\n                        <p align=\"justify\"><font size=\"2\" face=\"Verdana\"><strong><font size=\"2\" /></strong></font></p>\r\n                        <font size=\"2\" face=\"Verdana\">            </font></td>\r\n                        <td align=\"right\" valign=\"top\">\r\n                        <p><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\" color=\"#000080\"><font size=\"2\" face=\"Verdana\"><img height=\"302\" width=\"270\" alt=\"\" src=\"http://netforce.gr/images/netforce/klidi.jpg\" /></font></font></font></p>\r\n                        </td>\r\n                    </tr>\r\n                    <tr>\r\n                        <td colspan=\"2\"><span lang=\"el\"><font size=\"2\">\r\n                        <p align=\"justify\"><strong><span lang=\"el\"><font size=\"2\">&Theta;&upsilon;&mu;ό&mu;&alpha;&sigma;&tau;&epsilon;  &tau;&omicron;&upsilon; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &chi;&omega;&rho;ί&sigmaf; &nu;&alpha; &psi;ά&chi;&nu;&omicron;&upsilon;&mu;&epsilon; &kappa;ά&pi;&omicron;&iota;&alpha; </font></span><font size=\"2\">database  <span lang=\"el\">&epsilon;&nu;ώ &delta;&epsilon;&nu; &alpha;&pi;&alpha;&iota;&tau;&omicron;ύ&mu;&epsilon; &nu;&alpha; &mu;&alpha;&sigmaf; &sigma;&tau;&epsilon;ί&lambda;&epsilon;&tau;&epsilon; &tau;&eta; &tau;&alpha;&upsilon;&tau;ό&tau;&eta;&tau;ά &sigma;&alpha;&sigmaf; &mu;&epsilon; </span>fax  <span lang=\"el\">&pi;&rho;&omicron;&kappa;&epsilon;&iota;&mu;έ&nu;&omicron;&upsilon; &gamma;&iota;&alpha; &nu;&alpha; &sigma;&alpha;&sigmaf; &epsilon;&xi;&upsilon;&pi;&eta;&rho;&epsilon;&tau;ή&sigma;&omicron;&upsilon;&mu;&epsilon;...!</span></font></strong></p>\r\n                        <p align=\"justify\">&Sigma;ύ&nu;&tau;&omicron;&mu;&alpha; &theta;&alpha; &mu;&alpha;&sigmaf; &theta;&upsilon;&mu;ά&sigma;&tau;&epsilon; &kappa;&alpha;&iota; &epsilon;&sigma;&epsilon;ί&sigmaf; &kappa;&alpha;&theta;ό&sigma;&omicron;&nu;  &theta;&alpha; &xi;έ&rho;&epsilon;&tau;&epsilon; &mu;&epsilon; &pi;&omicron;&iota;ό&nu; &theta;&alpha; &epsilon;&pi;&iota;&kappa;&omicron;&iota;&nu;&omega;&nu;ή&sigma;&epsilon;&tau;&epsilon; &kappa;&alpha;&iota; &delta;&epsilon;&nu; &theta;&alpha; &chi;&rho;&epsilon;&iota;ά&zeta;&epsilon;&tau;&alpha;&iota; &nu;&alpha;  &quot;&xi;&alpha;&nu;&alpha;&sigma;&upsilon;&sigma;&tau;&eta;&theta;&epsilon;ί&tau;&epsilon;&quot;...</p>\r\n                        <p align=\"justify\"><span lang=\"el\"><font size=\"2\">&Eta; </font></span><font size=\"2\">NET FORCE </font><span lang=\"el\"><font size=\"2\">&epsilon;ί&nu;&alpha;&iota; &alpha;&rho;&kappa;&epsilon;&tau;ά <strong>&mu;&epsilon;&gamma;ά&lambda;&eta;</strong>  &gamma;&iota;&alpha; &nu;&alpha; &mu;&pi;&omicron;&rho;&epsilon;ί &nu;&alpha; &sigma;&alpha;&sigmaf; <strong>&pi;&alpha;&rho;έ&chi;&epsilon;&iota; &tau;&iota;&sigmaf; &lambda;ύ&sigma;&epsilon;&iota;&sigmaf; &pi;&omicron;&upsilon; &lambda;ί&gamma;&omicron;&iota; &theta;&alpha; &mu;&pi;&omicron;&rho;&omicron;ύ&sigma;&alpha;&nu;</strong>  &kappa;&alpha;&iota; <strong>&alpha;&rho;&kappa;&epsilon;&tau;ά &mu;&iota;&kappa;&rho;ή &gamma;&iota;&alpha; &nu;&alpha; &sigma;&alpha;&sigmaf; &epsilon;&xi;&upsilon;&pi;&eta;&rho;&epsilon;&tau;&epsilon;ί &pi;&rho;&omicron;&sigma;&omega;&pi;&iota;&kappa;ά, &mu;&epsilon;  &epsilon;&pi;&alpha;&gamma;&gamma;&epsilon;&lambda;&mu;&alpha;&tau;&iota;&sigma;&mu;ό &kappa;&alpha;&iota; &mu;&epsilon;&rho;ά&kappa;&iota;.</strong></font></span></p>\r\n                        <p align=\"justify\"><span lang=\"el\"><font size=\"2\">&Pi;&alpha;&rho;έ&chi;&omicron;&upsilon;&mu;&epsilon;  &tau;&iota;&sigmaf; &upsilon;&pi;&eta;&rho;&epsilon;&sigma;ί&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &alpha;&pi;ό &tau;ό&tau;&epsilon; &pi;&omicron;&upsilon; &tau;&omicron; </font></span><font size=\"2\">Internet <span lang=\"el\">&sigma;&tau;&eta;&nu; &Epsilon;&lambda;&lambda;ά&delta;&alpha; ή&tau;&alpha;&nu; &quot;ά&gamma;&nu;&omega;&sigma;&tau;&eta; &lambda;έ&xi;&eta;&quot; &kappa;&alpha;&iota; &pi;&omicron;&upsilon; &omicron;&iota; &epsilon;&tau;&alpha;&iota;&rho;ί&epsilon;&sigmaf; &sigma;&tau;&omicron; &chi;ώ&rho;&omicron;  &mu;&alpha;&sigmaf; ή&tau;&alpha;&nu; &lambda;&iota;&gamma;ό&tau;&epsilon;&rho;&epsilon;&sigmaf; &alpha;&pi;ό 10. <strong>&Epsilon;ί&mu;&alpha;&sigma;&tau;&epsilon; &pi;&alpha;&rho;ό&nu;&tau;&epsilon;&sigmaf; 12 &chi;&rho;ό&nu;&iota;&alpha; !</strong></span></font></p>\r\n                        <p align=\"justify\"><span lang=\"el\"><font size=\"2\">&Eta; </font></span><font size=\"2\"><strong>NET FORCE</strong> <span lang=\"el\">&delta;&epsilon;&nu; &delta;&eta;&mu;&iota;&omicron;&upsilon;&rho;&gamma;ή&theta;&eta;&kappa;&epsilon;  &mu;&epsilon; &chi;&rho;ή&mu;&alpha;&tau;&alpha; &epsilon;&tau;&alpha;&iota;&rho;&iota;ώ&nu; &tau;&omicron;&upsilon; &delta;&eta;&mu;&omicron;&sigma;ί&omicron;&upsilon; ή &epsilon;&pi;&iota;&chi;&omicron;&rho;&eta;&gamma;ή&sigma;&epsilon;&iota;&sigmaf; &mu;&epsilon; &alpha;&pi;&omicron;&tau;έ&lambda;&epsilon;&sigma;&mu;&alpha; &nu;&alpha; <u>&tau;&eta;&nu;  &gamma;&nu;&omega;&rho;ί&zeta;&omicron;&upsilon;&nu; &omicron;&iota; &quot;&pi;&alpha;&lambda;&iota;&omicron;ί&quot; &sigma;&tau;&omicron; </u></span><u>Internet <span lang=\"el\">&kappa;&alpha;&iota;  ό&chi;&iota; ό&sigma;&omicron;&iota;</span> </u></font><span lang=\"el\"><font size=\"2\"><u>&beta;&lambda;έ&pi;&omicron;&upsilon;&nu;  &tau;&eta;&lambda;&epsilon;ό&rho;&alpha;&sigma;&eta;</u>. &Tau;&omicron; ά&mu;&epsilon;&sigma;&omicron; &kappa;έ&rho;&delta;&omicron;&sigmaf; &gamma;&iota;&alpha; &tau;&omicron;&upsilon;&sigmaf; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &epsilon;ί&nu;&alpha;&iota; &nu;&alpha; &mu;&eta;&nu;  &chi;&rho;&epsilon;ώ&nu;&omicron;&nu;&tau;&alpha;&iota; &mu;&epsilon; &tau;&omicron; &kappa;ό&sigma;&tau;&omicron;&sigmaf; &tau;&omega;&nu; &pi;&alpha;&nu;ά&kappa;&rho;&iota;&beta;&omega;&nu; &delta;&iota;&alpha;&phi;&eta;&mu;ί&sigma;&epsilon;&omega;&nu; &pi;&omicron;&upsilon; &alpha;&nu;&alpha;&gamma;&kappa;&alpha;&sigma;&tau;&iota;&kappa;ά &theta;&alpha;  &epsilon;&pi;&iota;&beta;ά&rho;&upsilon;&nu;&alpha;&nu; &tau;&omicron; &kappa;ό&sigma;&tau;&omicron;&sigmaf; &tau;&omega;&nu; &upsilon;&pi;&eta;&rho;&epsilon;&sigma;&iota;ώ&nu; &mu;&alpha;&sigmaf;... !</font></span></p>\r\n                        <p align=\"justify\"><font size=\"2\" face=\"Verdana\"><strong><span lang=\"el\">&Mu;&iota;&lambda;ή&sigma;&tau;&epsilon; </span>&mu;&alpha;&sigmaf; &tau;ώ&rho;&alpha;</strong> &sigma;&tau;&omicron; <strong>210-5236749</strong>  ή <a href=\"http://netforce.gr/index.php?option=com_chronocontact&amp;chronoformname=general1\">&sigma;&tau;&epsilon;ί&lambda;&tau;&epsilon;  &mu;&alpha;&sigmaf; &tau;&omicron; &mu;ή&nu;&upsilon;&mu;ά &sigma;&alpha;&sigmaf; &eta;&lambda;&epsilon;&kappa;&tau;&rho;&omicron;&nu;&iota;&kappa;ά</a><span lang=\"el\"> &sigma;&upsilon;&mu;&pi;&lambda;&eta;&rho;ώ&nu;&omicron;&nu;&tau;&alpha;&sigmaf; &tau;&eta;&nu;  &sigma;&chi;&epsilon;&tau;&iota;&kappa;ή &phi;ό&rho;&mu;&alpha; &epsilon;&pi;&iota;&kappa;&omicron;&iota;&nu;&omega;&nu;ί&alpha;&sigmaf;</span>.</font></p>\r\n                        <p align=\"justify\"><font size=\"2\" face=\"Tahoma\">&Sigma;&alpha;&sigmaf;  &epsilon;&upsilon;&chi;&alpha;&rho;&iota;&sigma;&tau;&omicron;ύ&mu;&epsilon; <span lang=\"el\">&gamma;&iota;&alpha; &tau;&eta;&nu; &eta;&lambda;&epsilon;&kappa;&tau;&rho;&omicron;&nu;&iota;&kappa;ή &sigma;&alpha;&sigmaf; &epsilon;&pi;ί&sigma;&kappa;&epsilon;&psi;&eta; </span>&kappa;&alpha;&iota;  &theta;&alpha; </font><font size=\"2\" face=\"Verdana\">&chi;&alpha;&rho;&omicron;ύ&mu;&epsilon; &nu;&alpha; &sigma;&alpha;&sigmaf; &epsilon;&xi;&upsilon;&pi;&eta;&rho;&epsilon;&tau;ή&sigma;&omicron;&upsilon;&mu;&epsilon;.</font></p>\r\n                        <p align=\"justify\">&nbsp;</p>\r\n                        <p align=\"center\"><img height=\"103\" border=\"0\" width=\"216\" alt=\" Your\r\n                        ALT-Text here \" src=\"http://www.netforce.gr/images/paraskevopoulos2.gif\" /></p>\r\n                        <p align=\"center\"><font size=\"2\" face=\"Tahoma\" color=\"#000000\"><strong>&Nu;&iota;&kappa;ό&lambda;&alpha;&sigmaf; &Pi;&alpha;&rho;&alpha;&sigma;&kappa;&epsilon;&upsilon;ό&pi;&omicron;&upsilon;&lambda;&omicron;&sigmaf;, General Manager</strong></font></p>\r\n                        </font></span></td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n            </font></td>\r\n        </tr>\r\n        <tr>\r\n            <td align=\"left\" class=\"modifydate\" colspan=\"2\">Last Updated ( Wednesday, 19 September 2007 )</td>\r\n        </tr>\r\n    </tbody>\r\n</table>\r\n<p>&nbsp;  		<span class=\"article_seperator\" /></p>','21:07:50',3,7,1),(37,'2010-04-16','21:19:58','2010-04-16','testing','<p>testing in english laguage</p>\r\n<p>&nbsp;</p>\r\n<p><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\">&Epsilon;&mu;&epsilon;ί&sigmaf; &sigma;&tau;&eta; <strong>NET FORCE</strong> <strong><u>&nu;&omicron;&iota;&alpha;&zeta;ό&mu;&alpha;&sigma;&tau;&epsilon;</u>  &gamma;&iota;&alpha;</strong> &tau;&omicron;&upsilon;&sigmaf; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &kappa;&alpha;&iota; &sigma;&kappa;&omicron;&pi;ό&sigmaf; &mu;&alpha;&sigmaf; &epsilon;ί&nu;&alpha;&iota; &nu;&alpha; &epsilon;ί&mu;&alpha;&sigma;&tau;&epsilon; <strong>&omicron;  <span lang=\"el\">&alpha;&gamma;&alpha;&pi;&eta;&mu;έ&nu;&omicron;&sigmaf; &sigma;&alpha;&sigmaf; </span>&sigma;&upsilon;&nu;&epsilon;&rho;&gamma;ά&tau;&eta;&sigmaf; </strong><span lang=\"el\">&gamma;</span>&iota;&alpha; ό&tau;&iota; &alpha;&phi;&omicron;&rho;ά &sigma;&tau;&omicron; Internet</font></font></font></p>\r\n<p>&nbsp;</p>','21:18:34',3,3,1),(38,'2010-04-16','21:25:57','2010-04-16','Networking News','<p>Networking News Letter in english laguage</p>\r\n<p>&nbsp;</p>\r\n<p><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\">&Epsilon;&mu;&epsilon;ί&sigmaf; &sigma;&tau;&eta; <strong>NET FORCE</strong> <strong><u>&nu;&omicron;&iota;&alpha;&zeta;ό&mu;&alpha;&sigma;&tau;&epsilon;</u>  &gamma;&iota;&alpha;</strong> &tau;&omicron;&upsilon;&sigmaf; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &kappa;&alpha;&iota; &sigma;&kappa;&omicron;&pi;ό&sigmaf; &mu;&alpha;&sigmaf; &epsilon;ί&nu;&alpha;&iota; &nu;&alpha; &epsilon;ί&mu;&alpha;&sigma;&tau;&epsilon; <strong>&omicron;  <span lang=\"el\">&alpha;&gamma;&alpha;&pi;&eta;&mu;έ&nu;&omicron;&sigmaf; &sigma;&alpha;&sigmaf; </span>&sigma;&upsilon;&nu;&epsilon;&rho;&gamma;ά&tau;&eta;&sigmaf; </strong><span lang=\"el\">&gamma;</span>&iota;&alpha; ό&tau;&iota; &alpha;&phi;&omicron;&rho;ά &sigma;&tau;&omicron; Internet</font></font></font></p>','21:25:17',3,7,1),(39,'2010-04-16','21:27:37','2010-04-16','ss','<p>&nbsp;</p>\r\n<p><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\">&Epsilon;&mu;&epsilon;ί&sigmaf; &sigma;&tau;&eta; <strong>NET FORCE</strong> <strong><u>&nu;&omicron;&iota;&alpha;&zeta;ό&mu;&alpha;&sigma;&tau;&epsilon;</u>  &gamma;&iota;&alpha;</strong> &tau;&omicron;&upsilon;&sigmaf; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &kappa;&alpha;&iota; &sigma;&kappa;&omicron;&pi;ό&sigmaf; &mu;&alpha;&sigmaf; &epsilon;ί&nu;&alpha;&iota; &nu;&alpha; &epsilon;ί&mu;&alpha;&sigma;&tau;&epsilon; <strong>&omicron;  <span lang=\"el\">&alpha;&gamma;&alpha;&pi;&eta;&mu;έ&nu;&omicron;&sigmaf; &sigma;&alpha;&sigmaf; </span>&sigma;&upsilon;&nu;&epsilon;&rho;&gamma;ά&tau;&eta;&sigmaf; </strong><span lang=\"el\">&gamma;</span>&iota;&alpha; ό&tau;&iota; &alpha;&phi;&omicron;&rho;ά &sigma;&tau;&omicron; Internet</font></font></font></p>','21:27:16',3,7,1),(40,'2010-04-16','21:28:33','2010-04-16','ff','<p><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\">&Epsilon;&mu;&epsilon;ί&sigmaf; &sigma;&tau;&eta; <strong>NET FORCE</strong> <strong><u>&nu;&omicron;&iota;&alpha;&zeta;ό&mu;&alpha;&sigma;&tau;&epsilon;</u>  &gamma;&iota;&alpha;</strong> &tau;&omicron;&upsilon;&sigmaf; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &kappa;&alpha;&iota; &sigma;&kappa;&omicron;&pi;ό&sigmaf; &mu;&alpha;&sigmaf; &epsilon;ί&nu;&alpha;&iota; &nu;&alpha; &epsilon;ί&mu;&alpha;&sigma;&tau;&epsilon; <strong>&omicron;  <span lang=\"el\">&alpha;&gamma;&alpha;&pi;&eta;&mu;έ&nu;&omicron;&sigmaf; &sigma;&alpha;&sigmaf; </span>&sigma;&upsilon;&nu;&epsilon;&rho;&gamma;ά&tau;&eta;&sigmaf; </strong><span lang=\"el\">&gamma;</span>&iota;&alpha; ό&tau;&iota; &alpha;&phi;&omicron;&rho;ά &sigma;&tau;&omicron; Internet</font></font></font></p>','21:28:18',3,3,1),(41,'2010-04-16','21:42:40','2010-04-16','testing','<p>testing in engli</p>\r\n<p><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\">&Epsilon;&mu;&epsilon;ί&sigmaf; &sigma;&tau;&eta; <strong>NET FORCE</strong> <strong><u>&nu;&omicron;&iota;&alpha;&zeta;ό&mu;&alpha;&sigma;&tau;&epsilon;</u>  &gamma;&iota;&alpha;</strong> &tau;&omicron;&upsilon;&sigmaf; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &kappa;&alpha;&iota; &sigma;&kappa;&omicron;&pi;ό&sigmaf; &mu;&alpha;&sigmaf; &epsilon;ί&nu;&alpha;&iota; &nu;&alpha; &epsilon;ί&mu;&alpha;&sigma;&tau;&epsilon; <strong>&omicron;  <span lang=\"el\">&alpha;&gamma;&alpha;&pi;&eta;&mu;έ&nu;&omicron;&sigmaf; &sigma;&alpha;&sigmaf; </span>&sigma;&upsilon;&nu;&epsilon;&rho;&gamma;ά&tau;&eta;&sigmaf; </strong><span lang=\"el\">&gamma;</span>&iota;&alpha; ό&tau;&iota; &alpha;&phi;&omicron;&rho;ά &sigma;&tau;&omicron; Internet</font></font></font>sh laguage</p>','21:42:17',3,3,1),(42,'2010-04-16','21:44:25','2010-04-16','www','<p><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\">&Epsilon;&mu;&epsilon;ί&sigmaf; &sigma;&tau;&eta; <strong>NET FORCE</strong> <strong><u>&nu;&omicron;&iota;&alpha;&zeta;ό&mu;&alpha;&sigma;&tau;&epsilon;</u>  &gamma;&iota;&alpha;</strong> &tau;&omicron;&upsilon;&sigmaf; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &kappa;&alpha;&iota; &sigma;&kappa;&omicron;&pi;ό&sigmaf; &mu;&alpha;&sigmaf; &epsilon;ί&nu;&alpha;&iota; &nu;&alpha; &epsilon;ί&mu;&alpha;&sigma;&tau;&epsilon; <strong>&omicron;  <span lang=\"el\">&alpha;&gamma;&alpha;&pi;&eta;&mu;έ&nu;&omicron;&sigmaf; &sigma;&alpha;&sigmaf; </span>&sigma;&upsilon;&nu;&epsilon;&rho;&gamma;ά&tau;&eta;&sigmaf; </strong><span lang=\"el\">&gamma;</span>&iota;&alpha; ό&tau;&iota; &alpha;&phi;&omicron;&rho;ά &sigma;&tau;&omicron; Internet</font></font></font></p>','21:44:10',3,3,1),(43,'2010-04-16','21:47:11','2010-04-16','vvvvvvvvvvvvvvvvvvvvvvv','<p><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\">&Epsilon;&mu;&epsilon;ί&sigmaf; &sigma;&tau;&eta; <strong>NET FORCE</strong> <strong><u>&nu;&omicron;&iota;&alpha;&zeta;ό&mu;&alpha;&sigma;&tau;&epsilon;</u>  &gamma;&iota;&alpha;</strong> &tau;&omicron;&upsilon;&sigmaf; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &kappa;&alpha;&iota; &sigma;&kappa;&omicron;&pi;ό&sigmaf; &mu;&alpha;&sigmaf; &epsilon;ί&nu;&alpha;&iota; &nu;&alpha; &epsilon;ί&mu;&alpha;&sigma;&tau;&epsilon; <strong>&omicron;  <span lang=\"el\">&alpha;&gamma;&alpha;&pi;&eta;&mu;έ&nu;&omicron;&sigmaf; &sigma;&alpha;&sigmaf; </span>&sigma;&upsilon;&nu;&epsilon;&rho;&gamma;ά&tau;&eta;&sigmaf; </strong><span lang=\"el\">&gamma;</span>&iota;&alpha; ό&tau;&iota; &alpha;&phi;&omicron;&rho;ά &sigma;&tau;&omicron; Internet</font></font></font></p>','21:46:35',3,7,1),(44,'2010-04-16','21:50:02','2010-04-16','vbvbvbvbvbvbvbvbvvvb','<p><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\">&Epsilon;&mu;&epsilon;ί&sigmaf; &sigma;&tau;&eta; <strong>NET FORCE</strong> <strong><u>&nu;&omicron;&iota;&alpha;&zeta;ό&mu;&alpha;&sigma;&tau;&epsilon;</u>  &gamma;&iota;&alpha;</strong> &tau;&omicron;&upsilon;&sigmaf; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &kappa;&alpha;&iota; &sigma;&kappa;&omicron;&pi;ό&sigmaf; &mu;&alpha;&sigmaf; &epsilon;ί&nu;&alpha;&iota; &nu;&alpha; &epsilon;ί&mu;&alpha;&sigma;&tau;&epsilon; <strong>&omicron;  <span lang=\"el\">&alpha;&gamma;&alpha;&pi;&eta;&mu;έ&nu;&omicron;&sigmaf; &sigma;&alpha;&sigmaf; </span>&sigma;&upsilon;&nu;&epsilon;&rho;&gamma;ά&tau;&eta;&sigmaf; </strong><span lang=\"el\">&gamma;</span>&iota;&alpha; ό&tau;&iota; &alpha;&phi;&omicron;&rho;ά &sigma;&tau;&omicron; Internet</font></font></font></p>','21:49:18',3,3,1),(45,'2010-04-16','23:35:54','2010-04-16','xxxxxxxxxx','<p>&nbsp;</p>\r\n<p><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\">&Epsilon;&mu;&epsilon;ί&sigmaf; &sigma;&tau;&eta; <strong>NET FORCE</strong> <strong><u>&nu;&omicron;&iota;&alpha;&zeta;ό&mu;&alpha;&sigma;&tau;&epsilon;</u>  &gamma;&iota;&alpha;</strong> &tau;&omicron;&upsilon;&sigmaf; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &kappa;&alpha;&iota; &sigma;&kappa;&omicron;&pi;ό&sigmaf; &mu;&alpha;&sigmaf; &epsilon;ί&nu;&alpha;&iota; &nu;&alpha; &epsilon;ί&mu;&alpha;&sigma;&tau;&epsilon; <strong>&omicron;  <span lang=\"el\">&alpha;&gamma;&alpha;&pi;&eta;&mu;έ&nu;&omicron;&sigmaf; &sigma;&alpha;&sigmaf; </span>&sigma;&upsilon;&nu;&epsilon;&rho;&gamma;ά&tau;&eta;&sigmaf; </strong><span lang=\"el\">&gamma;</span>&iota;&alpha; ό&tau;&iota; &alpha;&phi;&omicron;&rho;ά &sigma;&tau;&omicron; Internet</font></font></font></p>','23:35:34',3,3,1),(46,'2010-04-16','23:39:10','2010-04-16','Auto Cad 14','<p>Networking latest news in english laguage</p>\r\n<p><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\">&Epsilon;&mu;&epsilon;ί&sigmaf; &sigma;&tau;&eta; <strong>NET FORCE</strong> <strong><u>&nu;&omicron;&iota;&alpha;&zeta;ό&mu;&alpha;&sigma;&tau;&epsilon;</u>  &gamma;&iota;&alpha;</strong> &tau;&omicron;&upsilon;&sigmaf; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &kappa;&alpha;&iota; &sigma;&kappa;&omicron;&pi;ό&sigmaf; &mu;&alpha;&sigmaf; &epsilon;ί&nu;&alpha;&iota; &nu;&alpha; &epsilon;ί&mu;&alpha;&sigma;&tau;&epsilon; <strong>&omicron;  <span lang=\"el\">&alpha;&gamma;&alpha;&pi;&eta;&mu;έ&nu;&omicron;&sigmaf; &sigma;&alpha;&sigmaf; </span>&sigma;&upsilon;&nu;&epsilon;&rho;&gamma;ά&tau;&eta;&sigmaf; </strong><span lang=\"el\">&gamma;</span>&iota;&alpha; ό&tau;&iota; &alpha;&phi;&omicron;&rho;ά &sigma;&tau;&omicron; Internet</font></font></font></p>\r\n<p>&nbsp;</p>','23:38:23',3,3,1),(47,'2010-04-16','23:49:21','2010-04-16','ddd','<p><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\"><font size=\"2\" face=\"Verdana\">&Epsilon;&mu;&epsilon;ί&sigmaf; &sigma;&tau;&eta; <strong>NET FORCE</strong> <strong><u>&nu;&omicron;&iota;&alpha;&zeta;ό&mu;&alpha;&sigma;&tau;&epsilon;</u>  &gamma;&iota;&alpha;</strong> &tau;&omicron;&upsilon;&sigmaf; &pi;&epsilon;&lambda;ά&tau;&epsilon;&sigmaf; &mu;&alpha;&sigmaf; &kappa;&alpha;&iota; &sigma;&kappa;&omicron;&pi;ό&sigmaf; &mu;&alpha;&sigmaf; &epsilon;ί&nu;&alpha;&iota; &nu;&alpha; &epsilon;ί&mu;&alpha;&sigma;&tau;&epsilon; <strong>&omicron;  <span lang=\"el\">&alpha;&gamma;&alpha;&pi;&eta;&mu;έ&nu;&omicron;&sigmaf; &sigma;&alpha;&sigmaf; </span>&sigma;&upsilon;&nu;&epsilon;&rho;&gamma;ά&tau;&eta;&sigmaf; </strong><span lang=\"el\">&gamma;</span>&iota;&alpha; ό&tau;&iota; &alpha;&phi;&omicron;&rho;ά &sigma;&tau;&omicron; Internet</font></font></font></p>','23:48:50',1,3,1),(48,'2010-04-24','15:01:05','2010-04-24','testing emalsds','<p>Helllo</p>\r\n<p><img height=\"111\" width=\"122\" src=\"http://projects.netforce.gr/new_newsletter_prj/doc/girl1.jpg\" alt=\"\" /></p>\r\n<p>&nbsp;</p>\r\n<p>this is testing email from ks</p>\r\n<p>&nbsp;</p>\r\n<p>niteen</p>\r\n<p>&nbsp;</p>','14:56:27',3,1,1),(49,'2010-04-24','15:16:10','2010-04-24','niteen img','<p>hellos</p>\r\n<p><img height=\"111\" width=\"122\" src=\"http://projects.netforce.gr/new_newsletter_prj/doc/girl3(1).jpg\" alt=\"\" /></p>\r\n<p>sdds</p>\r\n<p><img height=\"134\" width=\"198\" src=\"http://projects.netforce.gr/new_newsletter_prj/doc/girl4.jpg\" alt=\"\" /></p>\r\n<p>sd</p>','15:14:16',3,7,1),(50,'2010-04-24','22:03:06','2010-04-24','tests','<p>testdfs</p>\r\n<p>s</p>\r\n<p>ds</p>\r\n<p>d</p>','22:02:43',3,7,0),(51,'2010-04-24','22:03:19','2010-04-24','tests','<p>testdfs</p>\r\n<p>s</p>\r\n<p>ds</p>\r\n<p>d</p>','22:02:43',3,7,0),(52,'2010-04-24','22:03:40','2010-04-24','tests','<p>testdfs</p>\r\n<p>s</p>\r\n<p>ds</p>\r\n<p>d</p>','22:02:43',3,7,0);
/*!40000 ALTER TABLE `tbl_news_letter_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_news_letter_details_cat`
--

DROP TABLE IF EXISTS `tbl_news_letter_details_cat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_news_letter_details_cat` (
  `new_letter_details_id` int(11) NOT NULL,
  `category` varchar(100) NOT NULL,
  `cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_news_letter_details_cat`
--

LOCK TABLES `tbl_news_letter_details_cat` WRITE;
/*!40000 ALTER TABLE `tbl_news_letter_details_cat` DISABLE KEYS */;
INSERT INTO `tbl_news_letter_details_cat` VALUES (2,'category',64),(2,'category',32),(2,'category',50),(2,'topic',5),(2,'category',60),(5,'category',64),(5,'category',32),(5,'topic',5),(5,'topic',3),(6,'category',54),(6,'category',82),(6,'topic',1),(6,'topic',5),(6,'topic',3),(6,'category',58),(9,'category',64),(9,'category',32),(9,'topic',1),(9,'category',54),(10,'category',64),(10,'category',32),(10,'topic',5),(11,'category',32),(11,'category',54),(12,'category',64),(12,'category',32),(12,'topic',1),(12,'mail',19),(12,'mail',1),(15,'category',32),(15,'category',54),(15,'topic',1),(15,'mail',3),(18,'category',32),(18,'category',76),(18,'category',80),(18,'category',70),(19,'category',80),(19,'category',32),(19,'category',70),(19,'topic',1),(26,'category',32),(27,'category',32),(29,'category',32),(30,'category',32),(31,'category',32),(32,'category',32),(33,'category',32),(34,'category',32),(35,'category',32),(36,'category',32),(37,'category',32),(38,'category',32),(40,'category',32),(41,'category',32),(42,'category',32),(43,'category',32),(44,'category',32),(45,'category',32),(46,'category',32),(47,'category',32),(47,'topic',1),(48,'category',32),(49,'category',32),(50,'category',64),(50,'category',32),(51,'category',64),(51,'category',32),(52,'category',64),(52,'category',32);
/*!40000 ALTER TABLE `tbl_news_letter_details_cat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_news_letter_mailing_list`
--

DROP TABLE IF EXISTS `tbl_news_letter_mailing_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_news_letter_mailing_list` (
  `type_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `lang` varchar(100) NOT NULL,
  `main_id` int(11) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_news_letter_mailing_list`
--

LOCK TABLES `tbl_news_letter_mailing_list` WRITE;
/*!40000 ALTER TABLE `tbl_news_letter_mailing_list` DISABLE KEYS */;
INSERT INTO `tbl_news_letter_mailing_list` VALUES (1,'mailing list 1rr','','0000-00-00 00:00:00','english',1),(2,'Mailing List - 1','','0000-00-00 00:00:00','greek',1),(3,'mailing list 41','','0000-00-00 00:00:00','english',3),(4,'Mailing List - 2','','0000-00-00 00:00:00','greek',3),(19,'mailing list 31','','2010-04-09 19:10:47','english',19),(20,'Computer Newsletter','','2010-04-09 19:10:47','greek',19),(21,'mailing list 2','','2010-04-16 16:30:54','english',21),(22,'Holi','','2010-04-16 16:30:54','greek',21),(23,'mailing list 5','','2010-04-19 16:57:37','english',23),(24,'mail by maaria','','2010-04-19 16:57:37','greek',23),(25,'मेलिंग सूची - 1','','0000-00-00 00:00:00','Array',1),(26,'मेलिंग सूची - 2','','0000-00-00 00:00:00','Array',3),(27,'कंप्यूटर न्यूज़लैटर','','0000-00-00 00:00:00','Array',19),(28,'होली','','0000-00-00 00:00:00','Array',21),(29,'मारिया द्वारा मेल','','0000-00-00 00:00:00','Array',23),(30,'मेलिंग सूची - 1','','0000-00-00 00:00:00','Array',1),(31,'मेलिंग सूची - 2','','0000-00-00 00:00:00','Array',3),(32,'कंप्यूटर न्यूज़लैटर','','0000-00-00 00:00:00','Array',19),(33,'होली','','0000-00-00 00:00:00','Array',21),(34,'मारिया द्वारा मेल','','0000-00-00 00:00:00','Array',23),(35,'1 मेलिंग सूची','','0000-00-00 00:00:00','hindi',1),(36,'4 मेलिंग सूची','','0000-00-00 00:00:00','hindi',3),(37,'3 मेलिंग सूची','','0000-00-00 00:00:00','hindi',19),(38,'2 मेलिंग सूची','','0000-00-00 00:00:00','hindi',21),(39,'5 मेलिंग सूची','','0000-00-00 00:00:00','hindi',23),(40,'','','0000-00-00 00:00:00','marathi',1),(41,'','','0000-00-00 00:00:00','marathi',3),(42,'','','0000-00-00 00:00:00','marathi',19),(43,'','','0000-00-00 00:00:00','marathi',21),(44,'','','0000-00-00 00:00:00','marathi',23);
/*!40000 ALTER TABLE `tbl_news_letter_mailing_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_news_letter_priority_lkp`
--

DROP TABLE IF EXISTS `tbl_news_letter_priority_lkp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_news_letter_priority_lkp` (
  `type_id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `lang` varchar(100) NOT NULL,
  `main_id` int(11) NOT NULL,
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_news_letter_priority_lkp`
--

LOCK TABLES `tbl_news_letter_priority_lkp` WRITE;
/*!40000 ALTER TABLE `tbl_news_letter_priority_lkp` DISABLE KEYS */;
INSERT INTO `tbl_news_letter_priority_lkp` VALUES (1,'normal','norma','english',1),(2,'Normal','','greek',1),(9,'','','Array',1),(14,'','','',1),(18,'0','0','Array',1),(22,'0','0','Array',1),(26,'','','Array',1),(30,'0','0','Array',1),(34,'kk kan','0','Array',1),(42,'0','0','Array',1),(48,'0121','0','Array',1),(54,'121','0','Array',1),(60,'kanmmmmm','0','kannada',1),(66,'mediam','heigh','english',66),(67,'kk kan','kk kan','kannada',66),(68,'heigh','heigh','english',68),(69,'mmmm','mmm','kannada',68),(70,'साधारण','0','hindi',1),(71,'mediam','0','hindi',66),(72,'अजी','0','hindi',68),(73,'kk1','kk','english',73),(74,'केके','kk','hindi',73);
/*!40000 ALTER TABLE `tbl_news_letter_priority_lkp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_news_letter_smtp_lkp`
--

DROP TABLE IF EXISTS `tbl_news_letter_smtp_lkp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_news_letter_smtp_lkp` (
  `news_letter_smtp_lkp_id` int(11) NOT NULL AUTO_INCREMENT,
  `host_name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `note` text NOT NULL,
  `port_no` int(11) NOT NULL,
  `default_flag` int(11) NOT NULL,
  `approve` int(11) NOT NULL,
  PRIMARY KEY (`news_letter_smtp_lkp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_news_letter_smtp_lkp`
--

LOCK TABLES `tbl_news_letter_smtp_lkp` WRITE;
/*!40000 ALTER TABLE `tbl_news_letter_smtp_lkp` DISABLE KEYS */;
INSERT INTO `tbl_news_letter_smtp_lkp` VALUES (1,'mail.yahoo.com','niteeen','nit712','S',3,0,0),(2,'mail.google.com','niteen241','nit712','google a/c',21,0,0),(3,'System Account','','','System account do not enter username and pass',30,1,1);
/*!40000 ALTER TABLE `tbl_news_letter_smtp_lkp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_news_letter_subscription`
--

DROP TABLE IF EXISTS `tbl_news_letter_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_news_letter_subscription` (
  `subscription_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_id` varchar(200) NOT NULL,
  `subcriber_id` int(11) NOT NULL,
  `category` varchar(200) NOT NULL,
  `category_id` int(11) NOT NULL,
  `join_date` date DEFAULT NULL,
  `sub_unsub_flag` int(2) NOT NULL,
  PRIMARY KEY (`subscription_list_id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_news_letter_subscription`
--

LOCK TABLES `tbl_news_letter_subscription` WRITE;
/*!40000 ALTER TABLE `tbl_news_letter_subscription` DISABLE KEYS */;
INSERT INTO `tbl_news_letter_subscription` VALUES (16,'niteen241@yahoo.co.in',8,'category',64,'2010-04-09',1),(17,'niteen241@yahoo.co.in',8,'category',32,'2010-04-09',1),(18,'niteen241@yahoo.co.in',8,'mail',1,'2010-04-09',1),(19,'niteen241@yahoo.co.in',8,'mail',3,'2010-04-09',1),(20,'nilkantkalshetti@gmail.com',0,'category',64,'2010-04-09',1),(21,'nilkantkalshetti@gmail.com',0,'mail',19,'2010-04-09',1),(22,'nilkantkalshetti@gmail.com',0,'mail',1,'2010-04-09',1),(23,'niteen241@yahoo.co.in',8,'mail',1,'2010-04-10',1),(25,'kumarwaghmode@gmail.com',1,'category',64,'2010-04-13',1),(26,'kumarwaghmode@gmail.com',1,'category',80,'2010-04-13',1),(27,'kumarwaghmode@gmail.com',1,'category',32,'2010-04-13',1),(28,'kumarwaghmode@gmail.com',1,'category',76,'2010-04-13',1),(29,'kumarwaghmode@gmail.com',1,'category',54,'2010-04-13',1),(30,'kumarwaghmode@gmail.com',1,'topic',1,'2010-04-13',1),(31,'kumarwaghmode@gmail.com',1,'mail',3,'2010-04-13',1),(32,'select2006@gmail.com',0,'category',32,'2010-04-14',1),(33,'select2006@gmail.com',0,'category',76,'2010-04-14',0),(34,'navinkdas@gmail.com',10,'category',80,'2010-04-14',0),(35,'navinkdas@gmail.com',10,'category',32,'2010-04-14',1),(36,'navinkdas@gmail.com',10,'category',76,'2010-04-14',1),(37,'navinkdas@gmail.com',10,'category',70,'2010-04-14',0),(38,'select2006@gmail.com',0,'topic',1,'2010-04-14',1),(39,'select2006@gmail.com',0,'category',60,'2010-04-14',1),(42,'kumarwaghmode@gmail.com',7,'mail',19,'2010-04-15',1),(43,'niteen241@yahoo.co.in',8,'mail',19,'2010-04-15',1),(44,'jon@netforce.gr',3,'mail',19,'2010-04-15',1),(45,'rocky@netforce.gr',4,'mail',19,'2010-04-15',1),(46,'navinkdas@gmail.com',10,'mail',19,'2010-04-15',1),(47,'niteen241@yahoo.co.in',11,'category',76,'2010-04-15',1),(48,'niteen241@yahoo.co.in',11,'category',62,'2010-04-15',1),(49,'kumarwagh2010@gmail.com',0,'mail',19,'2010-04-15',1),(50,'kumarwagh2010@gmail.com',0,'topic',1,'2010-04-15',1),(51,'mariasupport@hotmail.com',0,'category',48,'2010-04-16',0),(52,'jon@netforce.gr',3,'mail',21,'2010-04-16',1),(53,'rocky@netforce.gr',4,'mail',21,'2010-04-16',0),(54,'kumarwaghmode@gmail.com',7,'mail',21,'2010-04-16',1),(55,'niteen241@yahoo.co.in',8,'mail',21,'2010-04-16',1),(56,'mariasupport@hotmail.com',12,'category',86,'2010-04-16',0),(57,'mariasupport@hotmail.com',12,'category',68,'2010-04-16',0),(58,'mariasupport@hotmail.com',12,'topic',3,'2010-04-16',0),(59,'mariasupport@hotmail.com',12,'mail',19,'2010-04-16',1),(60,'niteen241@gmail.com',1,'category',32,'2010-04-16',1),(61,'jon@netforce.gr',3,'mail',3,'2010-04-19',1),(62,'rocky@netforce.gr',4,'mail',3,'2010-04-19',0),(63,'jon@netforce.gr',3,'mail',23,'2010-04-19',1),(64,'rocky@netforce.gr',4,'mail',23,'2010-04-19',0),(65,'kumarwaghmode@gmail.com',7,'mail',23,'2010-04-19',1),(66,'niteen241@yahoo.co.in',8,'mail',23,'2010-04-19',1),(67,'navinkdas@gmail.com',10,'mail',23,'2010-04-19',1),(68,'niteen241@gmail.com',11,'mail',23,'2010-04-19',1),(69,'mariasupport@hotmail.com',12,'mail',23,'2010-04-19',1),(70,'kumarwagh2010@gmail.com',13,'mail',23,'2010-04-19',1),(71,'mariasupport@hotmail.com',0,'category',32,'2010-04-20',0),(72,'mariasupport@hotmail.com',0,'category',84,'2010-04-20',1),(73,'mariasupport@hotmail.com',0,'topic',5,'2010-04-20',1),(74,'mariasupport@hotmail.com',0,'topic',1,'2010-04-20',0),(75,'mariasupport@hotmail.com',14,'category',76,'2010-04-20',0),(76,'mariasupport@hotmail.com',14,'category',54,'2010-04-20',0);
/*!40000 ALTER TABLE `tbl_news_letter_subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_news_letter_track`
--

DROP TABLE IF EXISTS `tbl_news_letter_track`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_news_letter_track` (
  `news_letter_id` int(11) NOT NULL,
  `user_email` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `read_flag` int(11) NOT NULL,
  UNIQUE KEY `news_letter_id` (`news_letter_id`,`user_email`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_news_letter_track`
--

LOCK TABLES `tbl_news_letter_track` WRITE;
/*!40000 ALTER TABLE `tbl_news_letter_track` DISABLE KEYS */;
INSERT INTO `tbl_news_letter_track` VALUES (2,'jon@netforce.gr',3,1),(2,'rocky@netforce.gr',4,1),(3,'jon@netforce.gr',3,1),(3,'rocky@netforce.gr',4,1),(4,'jon@netforce.gr',3,1),(4,'rocky@netforce.gr',4,1),(5,'jon@netforce.gr',3,1),(5,'rocky@netforce.gr',4,1),(6,'jon@netforce.gr',3,1),(6,'rocky@netforce.gr',4,1),(9,'jon@netforce.gr',3,1),(9,'rocky@netforce.gr',4,1),(10,'jon@netforce.gr',3,1),(10,'rocky@netforce.gr',4,1),(11,'jon@netforce.gr',3,1),(11,'nilkant@gmail.com',0,1),(11,'niteen241@yahoo.co.in',8,1),(11,'rocky@netforce.gr',4,1),(12,'jon@netforce.gr',3,1),(12,'nilkant@gmail.com',0,1),(12,'niteen241@yahoo.co.in',8,1),(12,'rocky@netforce.gr',4,1),(13,'jon@netforce.gr',3,1),(13,'nilkant@gmail.com',0,1),(13,'niteen241@yahoo.co.in',8,1),(13,'rocky@netforce.gr',4,1),(14,'jon@netforce.gr',3,1),(14,'nilkant@gmail.com',0,1),(14,'niteen241@yahoo.co.in',8,1),(14,'rocky@netforce.gr',4,1),(15,'jon@netforce.gr',3,1),(15,'kumarwaghmode@gmail.com',1,1),(15,'nilkant@gmail.com',0,1),(15,'niteen241@yahoo.co.in',8,1),(15,'rocky@netforce.gr',4,1),(20,'kumarwagh2010@gmail.com',0,1),(20,'kumarwagh2010@gnail.com',0,1),(20,'kumarwaghmode@gmail.com',1,1),(20,'navinkdas@gmail.com',10,1),(20,'nilkantkalshetti@gmail.com',0,1),(20,'niteen241@yahoo.co.in',8,1),(20,'select2006@gmail.com',0,1);
/*!40000 ALTER TABLE `tbl_news_letter_track` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_news_topics`
--

DROP TABLE IF EXISTS `tbl_news_topics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_news_topics` (
  `subject_id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(200) NOT NULL,
  `note` text NOT NULL,
  `main_subject_id` int(11) NOT NULL,
  `lang` varchar(200) NOT NULL,
  PRIMARY KEY (`subject_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_news_topics`
--

LOCK TABLES `tbl_news_topics` WRITE;
/*!40000 ALTER TABLE `tbl_news_topics` DISABLE KEYS */;
INSERT INTO `tbl_news_topics` VALUES (1,'Computer','m1',1,'english'),(2,'Υπολογιστών','m1',1,'greek'),(3,'Yoga Teaching','',3,'english'),(4,'Γιόγκα Διδασκαλία','',3,'greek'),(5,'Hardware','Hardware',5,'english'),(6,'Hardware','hardware',5,'greek');
/*!40000 ALTER TABLE `tbl_news_topics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_pay_order_details`
--

DROP TABLE IF EXISTS `tbl_pay_order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pay_order_details` (
  `product_id` int(11) NOT NULL DEFAULT '0',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `quantity_deliver` int(11) DEFAULT NULL,
  `list_price` float(11,2) DEFAULT NULL,
  `your_price` float(11,2) DEFAULT NULL,
  `custom_fields` varchar(255) DEFAULT NULL,
  `vat` varchar(10) NOT NULL,
  `store` int(2) NOT NULL,
  PRIMARY KEY (`product_id`,`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_pay_order_details`
--

LOCK TABLES `tbl_pay_order_details` WRITE;
/*!40000 ALTER TABLE `tbl_pay_order_details` DISABLE KEYS */;
INSERT INTO `tbl_pay_order_details` VALUES (1,88,0,10.00,10.00,'0','',0),(3,89,0,100.00,100.00,'0','',0),(1,5,0,12.00,12.00,'0','',0),(1,6,0,12.00,12.00,'0','',0),(1,7,0,12.00,12.00,'0','',0),(1,8,0,12.00,12.00,'0','',0),(1,9,0,12.00,12.00,'0','',0),(1,10,0,12.00,12.00,'0','',0),(1,11,0,12.00,12.00,'0','',0),(1,12,0,12.00,12.00,'0','',0),(1,13,0,12.00,12.00,'0','',0),(1,15,0,100.00,100.00,'0','',0),(1,16,0,12.00,12.00,'0','',0),(1,17,0,100.00,100.00,'0','',0),(1,18,0,12.00,12.00,'0','',0),(3,19,0,100.00,100.00,'0','',0),(7,20,0,100.00,100.00,'0','',0),(3,21,0,100.00,100.00,'0','',0),(5,92,0,0.00,0.00,'0','',1),(5,93,0,100.00,100.00,'0','',0),(7,27,0,10.00,10.00,'0','',0),(9,28,0,10.00,10.00,'0','',0),(9,29,0,0.00,0.00,'0','',0),(9,30,0,100.00,100.00,'0','',0),(1,31,0,10.00,10.00,'0','',0),(3,32,0,100.00,100.00,'0','',0),(3,34,0,100.00,100.00,'0','',0),(9,35,0,10.00,10.00,'0','',0),(3,36,0,100.00,100.00,'0','',0),(9,37,0,100.00,100.00,'0','',0),(9,38,0,1000.00,1000.00,'0','',0),(3,39,0,100.00,100.00,'0','',0),(3,51,0,100.00,100.00,'0','',0),(1,52,0,10.00,10.00,'0','',0),(1,53,0,10.00,10.00,'0','',0),(1,54,0,10.00,10.00,'0','',0),(1,55,0,10.00,10.00,'0','',0),(1,56,0,10.00,10.00,'0','',0),(5,57,0,0.00,0.00,'0','',0),(5,58,0,100.00,100.00,'0','',0),(5,59,0,100.00,100.00,'0','',0),(1,60,0,10.00,10.00,'0','',0),(1,61,0,10.00,10.00,'0','',0),(1,62,0,10.00,10.00,'0','',0),(3,63,0,100.00,100.00,'0','',0),(3,64,0,100.00,100.00,'0','',0),(1,65,0,10.00,10.00,'0','',0),(1,66,0,10.00,10.00,'0','',0),(3,67,0,100.00,100.00,'0','',0),(1,68,0,10.00,10.00,'0','',0),(3,69,0,100.00,100.00,'0','',0),(1,70,0,10.00,10.00,'0','',0),(1,71,0,10.00,10.00,'0','',0),(3,80,0,100.00,100.00,'0','',0),(1,81,0,10.00,10.00,'0','',0),(1,82,0,10.00,10.00,'0','',1),(1,83,0,10.00,10.00,'0','',0),(5,84,0,0.00,0.00,'0','',0),(3,85,0,100.00,100.00,'0','',0),(3,86,0,100.00,100.00,'0','',0),(3,87,0,100.00,100.00,'0','',0),(1,90,0,10.00,10.00,'0','',0),(3,91,0,100.00,100.00,'0','',1),(0,94,0,100.00,100.00,'0','',0),(0,95,0,100.00,100.00,'0','',0),(0,96,0,100.00,100.00,'0','',0),(0,97,0,100.00,100.00,'0','',0),(0,98,0,100.00,100.00,'0','',0),(0,99,0,100.00,100.00,'0','',0),(0,100,0,100.00,100.00,'0','',0),(7,101,0,100.00,100.00,'0','',0),(7,102,0,100.00,100.00,'0','',0),(7,103,0,100.00,100.00,'0','',0),(7,104,0,66.00,66.00,'0','',0),(7,105,0,66.00,66.00,'0','',0),(7,106,0,66.00,66.00,'0','',0),(7,107,0,66.00,66.00,'0','',1),(7,108,0,345.00,345.00,'0','',0),(7,109,0,100.00,100.00,'0','',0),(7,110,0,100.00,100.00,'0','',0),(5,111,0,100.00,100.00,'0','',0),(7,112,0,100.00,100.00,'0','',0),(7,113,0,999.00,999.00,'0','',1),(5,114,0,100.00,100.00,'0','',0),(7,115,0,12.00,12.00,'0','',1),(5,116,0,100.00,100.00,'0','',0),(5,117,0,100.00,100.00,'0','',1);
/*!40000 ALTER TABLE `tbl_pay_order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_pay_order_status_lk`
--

DROP TABLE IF EXISTS `tbl_pay_order_status_lk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pay_order_status_lk` (
  `order_status_id` int(10) NOT NULL AUTO_INCREMENT,
  `order_status` longtext NOT NULL,
  `note` longtext NOT NULL,
  `main_order_status_id` int(11) NOT NULL,
  `lang` varchar(100) NOT NULL,
  `set_def` int(11) NOT NULL,
  PRIMARY KEY (`order_status_id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_pay_order_status_lk`
--

LOCK TABLES `tbl_pay_order_status_lk` WRITE;
/*!40000 ALTER TABLE `tbl_pay_order_status_lk` DISABLE KEYS */;
INSERT INTO `tbl_pay_order_status_lk` VALUES (4,'Λήψη Πληρωμής','Λήψη Πληρωμής',1,'greek',0),(5,'Έλεγχος διαθεσιμότητας','Έλεγχος διαθεσιμότητας',2,'greek',1),(7,'Payment Received','Your Payment Received thanks',1,'english',0),(8,'Checking Availability','Checking Service Availability Compatibility',2,'english',1),(26,'Payment Not Received','Payment Not Received',26,'english',0),(27,'Πληρωμής δεν λαμβάνεται','Πληρωμής δεν λαμβάνεται',26,'greek',0);
/*!40000 ALTER TABLE `tbl_pay_order_status_lk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_pay_orders`
--

DROP TABLE IF EXISTS `tbl_pay_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pay_orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `order_date` date DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `payment_mode` varchar(255) DEFAULT NULL,
  `shipping_date` datetime DEFAULT NULL,
  `delivery_in_days` int(11) DEFAULT NULL,
  `order_number` varchar(255) DEFAULT NULL,
  `shipping_charges` float(11,2) DEFAULT NULL,
  `tax_charges` float(11,2) DEFAULT NULL,
  `DeltaPayId` varchar(255) DEFAULT NULL,
  `total_amt` double NOT NULL DEFAULT '0',
  `usr_cmt` text NOT NULL,
  `product_id` int(11) NOT NULL,
  `payment_type` varchar(100) NOT NULL,
  `store` int(2) NOT NULL,
  `ord_stat_note` text NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=MyISAM AUTO_INCREMENT=118 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_pay_orders`
--

LOCK TABLES `tbl_pay_orders` WRITE;
/*!40000 ALTER TABLE `tbl_pay_orders` DISABLE KEYS */;
INSERT INTO `tbl_pay_orders` VALUES (93,33,'2010-03-29','P','11',NULL,NULL,'21321.213',0.00,0.00,NULL,100,'',5,'OneTime',0,''),(29,21,'2010-02-25','P','12',NULL,NULL,'ORD29',0.00,0.00,NULL,0,'',9,'',0,''),(30,21,'2010-02-25','P','6',NULL,NULL,'ORD30',0.00,0.00,NULL,100,'',9,'',0,''),(31,21,'2010-02-25','P','6',NULL,NULL,'ORD31',0.00,0.00,NULL,10,'',1,'OneTime',0,''),(32,21,'2010-02-25','P','6',NULL,NULL,'ORD32',0.00,0.00,NULL,100,'',3,'OneTime',0,''),(34,21,'2010-02-25','P','6',NULL,NULL,'ORD34',0.00,0.00,NULL,100,'',3,'',0,''),(35,21,'2010-02-25','P','6',NULL,NULL,'ORD35',0.00,0.00,NULL,10,'',9,'OneTime',0,''),(36,21,'2010-02-25','P','6',NULL,NULL,'ORD36',0.00,0.00,NULL,100,'',3,'',0,''),(37,21,'2010-02-25','P','12',NULL,NULL,'ORD37',0.00,0.00,NULL,100,'',9,'OneTime',0,''),(38,21,'2010-02-25','P','6',NULL,NULL,'ORD38',0.00,0.00,NULL,1000,'',9,'',0,''),(113,1,'2010-05-10','P','7',NULL,NULL,'1919',0.00,0.00,NULL,999,'',7,'OneTime',1,'Checking Service Availability Compatibility'),(117,1,'2010-05-11','P','7',NULL,NULL,'345343',0.00,0.00,NULL,100,'',5,'OneTime',1,'Payment Received'),(115,1,'2010-05-10','P','7',NULL,NULL,'11011',0.00,0.00,NULL,12,'',7,'OneTime',1,'Payment Received');
/*!40000 ALTER TABLE `tbl_pay_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_profile_cus_fields`
--

DROP TABLE IF EXISTS `tbl_profile_cus_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_profile_cus_fields` (
  `cus_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `lang` text CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL,
  `main_cus_id` int(11) NOT NULL,
  `order_no` int(11) NOT NULL,
  `cus_type` varchar(200) NOT NULL,
  `visible` int(1) NOT NULL,
  `approve` int(1) NOT NULL,
  `sub_cus_type` varchar(100) NOT NULL,
  PRIMARY KEY (`cus_id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_profile_cus_fields`
--

LOCK TABLES `tbl_profile_cus_fields` WRITE;
/*!40000 ALTER TABLE `tbl_profile_cus_fields` DISABLE KEYS */;
INSERT INTO `tbl_profile_cus_fields` VALUES (1,'text','landline',0,'english',1,1,'signup_student',1,1,'student'),(2,'text','landline grk',0,'greek',1,1,'signup_student',1,1,'student'),(3,'textarea','Address',0,'english',3,2,'signup_student',1,1,'student'),(4,'textarea','Address Grk',0,'greek',3,2,'signup_student',1,1,'student'),(5,'combobox','12th pass',0,'english',5,1,'signup_student',1,1,'student'),(6,'combobox','12th pass grk',0,'greek',5,1,'signup_student',1,1,'student'),(7,'checkbox','Education',0,'english',7,4,'signup_student',1,1,'student'),(8,'checkbox','Education Grk',0,'greek',7,4,'signup_student',1,1,'student'),(9,'text','Tecnology',0,'english',9,1,'signup_teacher',1,1,''),(10,'text','Tecnology',0,'greek',9,1,'signup_teacher',1,1,''),(11,'radio','Know english',0,'english',11,0,'signup_student',1,1,''),(12,'radio','Know english grk',0,'greek',11,0,'signup_student',1,1,''),(15,'text','note',0,'english',15,2,'payment',1,1,''),(16,'text','note grk',0,'greek',15,2,'payment',1,1,''),(25,'text','country',0,'english',25,2,'user',1,1,''),(26,'text','country',0,'greek',25,2,'user',1,1,''),(27,'text','phone',0,'english',27,1,'user',1,1,''),(28,'text','phone',0,'greek',27,1,'user',1,1,''),(36,'text','',0,'greek',35,4,'user',1,1,''),(35,'text','nickname',0,'english',35,4,'user',1,1,''),(31,'combobox','interest',0,'english',31,0,'user',1,1,''),(32,'combobox','interest',0,'greek',31,0,'user',1,1,''),(33,'radio','sex',0,'english',33,0,'user',1,1,''),(34,'radio','sex',0,'greek',33,0,'user',1,1,''),(37,'combobox','bankname',0,'english',37,0,'user',1,1,''),(38,'combobox','',0,'greek',37,0,'user',1,1,''),(39,'checkbox','sex',0,'english',39,0,'user',1,1,''),(40,'checkbox','',0,'greek',39,0,'user',1,1,'');
/*!40000 ALTER TABLE `tbl_profile_cus_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_profile_cus_fields_value`
--

DROP TABLE IF EXISTS `tbl_profile_cus_fields_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_profile_cus_fields_value` (
  `value_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cat_id` bigint(20) NOT NULL DEFAULT '0',
  `sub_title` varchar(80) NOT NULL,
  `sub_value` varchar(80) NOT NULL,
  `selected` varchar(15) NOT NULL,
  `combo_extra` varchar(150) NOT NULL,
  `main_cus_id` int(11) NOT NULL DEFAULT '0',
  `lang` text NOT NULL,
  `type` text NOT NULL,
  `rows` int(11) DEFAULT NULL,
  `cols` int(11) DEFAULT NULL,
  `main_value_id` int(11) NOT NULL,
  PRIMARY KEY (`value_id`)
) ENGINE=MyISAM AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_profile_cus_fields_value`
--

LOCK TABLES `tbl_profile_cus_fields_value` WRITE;
/*!40000 ALTER TABLE `tbl_profile_cus_fields_value` DISABLE KEYS */;
INSERT INTO `tbl_profile_cus_fields_value` VALUES (1,0,'','','','',1,'english','text',2,20,1),(2,0,'','','','',1,'greek','text',2,20,1),(3,0,'','','','',3,'english','textarea',2,20,3),(4,0,'','','','',3,'greek','textarea',2,20,3),(5,0,'12th pass','Yes','selected','Yes',5,'english','combobox',0,0,5),(6,0,'12th pass grk','Yes','selected','Yes',5,'greek','combobox',0,0,5),(7,0,'12th pass','No','','No',5,'english','combobox',0,0,7),(8,0,'12th pass grk','No','','No',5,'greek','combobox',0,0,7),(9,0,'Education','BCS','','BCS',7,'english','checkbox',0,0,9),(10,0,'Education Grk','BCS Grk','','BCS Grk',7,'greek','checkbox',0,0,9),(11,0,'Education','MCS','','MCS',7,'english','checkbox',0,0,11),(12,0,'Education Grk','MCS Grk','','MCS Grk',7,'greek','checkbox',0,0,11),(13,0,'Education','MCA','','MCA',7,'english','checkbox',0,0,13),(14,0,'Education Grk','MCA Grk','','MCA Grk',7,'greek','checkbox',0,0,13),(15,0,'','','','',9,'english','text',2,20,15),(16,0,'','','','',9,'greek','text',2,20,15),(17,0,'Know english','yes','checked','yes',11,'english','radio',0,0,17),(18,0,'Know english grk','yes grk','checked','yes grk',11,'greek','radio',0,0,17),(19,0,'Know english','no','','no',11,'english','radio',0,0,19),(20,0,'Know english grk','no grk','','no grk',11,'greek','radio',0,0,19),(23,0,'','','','',15,'english','text',2,20,23),(24,0,'','','','',15,'greek','text',2,20,23),(33,0,'name','name','','',25,'english','text',2,10,33),(34,0,'name','name','','',25,'greek','text',2,10,33),(35,0,'121','121','','',27,'english','text',2,10,35),(36,0,'121','121','','',27,'greek','text',2,10,35),(37,0,'interest','cricket','selected','',31,'english','combobox',0,0,37),(38,0,'interest','rrr','selected','',31,'greek','combobox',0,0,37),(39,0,'interest','footboll','','',31,'english','combobox',0,0,39),(40,0,'interest','r','','',31,'greek','combobox',0,0,39),(41,0,'interest','rr','','',31,'english','combobox',0,0,41),(42,0,'interest','r','','',31,'greek','combobox',0,0,41),(43,0,'sex','male','checked','',33,'english','radio',0,0,43),(44,0,'sex','male','checked','',33,'greek','radio',0,0,43),(45,0,'sex','female','','',33,'english','radio',0,0,45),(46,0,'sex','female','','',33,'greek','radio',0,0,45),(47,0,'','','','',35,'english','text',2,30,47),(48,0,'','','','',35,'greek','text',2,30,47);
/*!40000 ALTER TABLE `tbl_profile_cus_fields_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_profile_data`
--

DROP TABLE IF EXISTS `tbl_profile_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_profile_data` (
  `profile_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `main_cus_id` int(11) NOT NULL,
  `cus_type` varchar(100) NOT NULL,
  `value` text NOT NULL,
  `main_value_id` int(11) NOT NULL,
  PRIMARY KEY (`profile_data_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_profile_data`
--

LOCK TABLES `tbl_profile_data` WRITE;
/*!40000 ALTER TABLE `tbl_profile_data` DISABLE KEYS */;
INSERT INTO `tbl_profile_data` VALUES (1,3,1,'signup_student','1111',0),(2,3,3,'signup_student','greece',0),(3,3,5,'signup_student','5',5),(4,3,11,'signup_student','17',17),(5,4,1,'signup_student','1211212',0),(6,4,3,'signup_student','athense, greece',0),(7,4,5,'signup_student','5',5),(8,4,11,'signup_student','17',17),(9,5,9,'signup_student','1212',0),(10,6,9,'signup_student','',0),(11,7,1,'signup_student','123',0),(12,7,3,'signup_student','121223',0),(13,7,5,'signup_student','5',5),(14,7,11,'signup_student','17',17),(15,8,1,'signup_student','12',0),(16,8,3,'signup_student','12',0),(17,8,5,'signup_student','5',5),(18,8,11,'signup_student','0',0),(19,9,1,'signup_student','',0),(20,9,3,'signup_student','',0),(21,9,5,'signup_student','0',0),(22,9,11,'signup_student','0',0),(23,10,1,'signup_student','4563789',0),(24,10,3,'signup_student','Address',0),(25,10,5,'signup_student','0',0),(26,10,11,'signup_student','0',0),(27,11,1,'signup_student','test',0),(28,11,3,'signup_student','Address4\r\n	\r\n',0),(29,11,5,'signup_student','0',0),(30,11,11,'signup_student','0',0),(31,9,1,'signup_student','',0),(32,9,3,'signup_student','',0),(33,9,5,'signup_student','0',0),(34,9,11,'signup_student','0',0),(35,10,1,'signup_student','',0),(36,11,1,'signup_student','test',0),(37,12,1,'signup_student','',0),(38,13,1,'signup_student','',0),(39,14,1,'signup_student','',0);
/*!40000 ALTER TABLE `tbl_profile_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_profile_sub_data`
--

DROP TABLE IF EXISTS `tbl_profile_sub_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_profile_sub_data` (
  `profile_sub_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `main_cus_id` int(11) NOT NULL,
  `cus_type` varchar(100) NOT NULL,
  `main_value_id` int(11) NOT NULL,
  PRIMARY KEY (`profile_sub_data_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_profile_sub_data`
--

LOCK TABLES `tbl_profile_sub_data` WRITE;
/*!40000 ALTER TABLE `tbl_profile_sub_data` DISABLE KEYS */;
INSERT INTO `tbl_profile_sub_data` VALUES (1,3,7,'signup_student',9),(2,3,7,'signup_student',11),(3,4,7,'signup_student',9),(4,4,7,'signup_student',11),(5,7,7,'signup_student',9),(6,7,7,'signup_student',11),(7,8,7,'signup_student',9),(8,8,7,'signup_student',11),(9,8,7,'signup_student',13);
/*!40000 ALTER TABLE `tbl_profile_sub_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_receipt_type`
--

DROP TABLE IF EXISTS `tbl_receipt_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_receipt_type` (
  `receipt_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `receipt_type` varchar(255) DEFAULT NULL,
  `lang` varchar(100) NOT NULL,
  `main_receipt_type_id` int(11) NOT NULL,
  PRIMARY KEY (`receipt_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_receipt_type`
--

LOCK TABLES `tbl_receipt_type` WRITE;
/*!40000 ALTER TABLE `tbl_receipt_type` DISABLE KEYS */;
INSERT INTO `tbl_receipt_type` VALUES (1,'Receipt_1','english',1),(2,'Receipt_2','english',2),(3,'Receipt_3','english',3),(4,'Receipt_4','english',4),(5,'Receipt_5','english',5),(6,'Receipt_1','hindi',1),(7,'Receipt_2','hindi',2),(8,'Receipt_3','hindi',3),(9,'Receipt_3','hindi',4),(10,'केके','hindi',5),(11,'Receipt_6','english',11);
/*!40000 ALTER TABLE `tbl_receipt_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sur_ip_addresss`
--

DROP TABLE IF EXISTS `tbl_sur_ip_addresss`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sur_ip_addresss` (
  `IP_Address_ID` int(11) NOT NULL AUTO_INCREMENT,
  `IP_Address_Name` varchar(200) NOT NULL DEFAULT '',
  `IP_Address_Type` varchar(200) NOT NULL DEFAULT '',
  `Note` text NOT NULL,
  `Sub_Range` varchar(200) NOT NULL DEFAULT '',
  PRIMARY KEY (`IP_Address_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sur_ip_addresss`
--

LOCK TABLES `tbl_sur_ip_addresss` WRITE;
/*!40000 ALTER TABLE `tbl_sur_ip_addresss` DISABLE KEYS */;
INSERT INTO `tbl_sur_ip_addresss` VALUES (1,'127.*.*.*','single','128.*.*.*','127.*.*.*'),(2,'121.246.215.55','Single','121.246.215.55',''),(4,'91.138.*.*','Single','',''),(5,'115.240.117.114','Single','115.240.117.114',''),(6,'91.138.224.80','Single','91.138.224.80',''),(7,'115.240.5.161','Single','115.240.5.161',''),(8,'77.83.136.180','Single','115.240.91.45',''),(9,'62.57.52.22','Single','',''),(10,'62.57.52.22','Single','',''),(11,'77.49.155.95','','nf',''),(12,'85.72.32.211','','85.72.32.211',''),(13,'79.*.*.*','Single','',''),(14,'62.57.52.22','','',''),(15,'115.*.*.*','Single','115.240.25.63',''),(17,'*.*.*.*','Single','*','');
/*!40000 ALTER TABLE `tbl_sur_ip_addresss` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sur_last_login_details`
--

DROP TABLE IF EXISTS `tbl_sur_last_login_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sur_last_login_details` (
  `Last_Login_ID` int(11) NOT NULL AUTO_INCREMENT,
  `User_ID` int(11) NOT NULL DEFAULT '0',
  `User_Type` varchar(200) NOT NULL DEFAULT '',
  `Last_Date_Time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Last_IP` varchar(200) NOT NULL DEFAULT '',
  `In_Time` time NOT NULL DEFAULT '00:00:00',
  `Out_Time` time NOT NULL DEFAULT '00:00:00',
  `Total_Time` double NOT NULL DEFAULT '0',
  `Country` varchar(200) NOT NULL DEFAULT '',
  `Logged_Out` int(11) NOT NULL DEFAULT '0',
  `up_date_time` datetime NOT NULL,
  PRIMARY KEY (`Last_Login_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=351 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sur_last_login_details`
--

LOCK TABLES `tbl_sur_last_login_details` WRITE;
/*!40000 ALTER TABLE `tbl_sur_last_login_details` DISABLE KEYS */;
INSERT INTO `tbl_sur_last_login_details` VALUES (1,1,'','2010-03-31 20:35:53','192.168.1.108','20:35:53','20:35:53',0,'',0,'2010-03-31 20:39:50'),(2,1,'','2010-03-31 20:40:56','192.168.1.13','20:40:56','22:10:16',5360,'',0,'2010-03-31 20:44:54'),(3,3,'','2010-03-31 20:44:41','127.0.0.1','20:44:41','20:44:57',16,'',0,'2010-03-31 20:44:57'),(4,1,'','2010-03-31 20:45:11','127.0.0.1','20:45:11','20:56:55',704,'',0,'2010-03-31 20:56:54'),(5,1,'','2010-03-31 20:57:54','127.0.0.1','20:57:54','20:57:54',0,'',0,'2010-03-31 21:40:45'),(6,4,'','2010-03-31 21:37:22','127.0.0.1','21:37:22','21:37:22',0,'',0,'2010-03-31 22:45:45'),(7,1,'','2010-03-31 21:40:45','192.168.1.13','21:40:45','21:40:45',0,'',0,'2010-03-31 22:12:32'),(8,1,'','2010-03-31 22:13:00','192.168.1.13','22:13:00','22:13:00',0,'',0,'2010-03-31 23:48:25'),(9,1,'','2010-04-01 11:35:19','127.0.0.1','11:35:19','11:36:11',52,'',0,'2010-04-01 11:36:11'),(10,3,'','2010-04-01 11:36:18','127.0.0.1','11:36:18','11:37:44',86,'',0,'2010-04-01 11:37:44'),(11,1,'','2010-04-01 11:37:50','127.0.0.1','11:37:50','11:37:50',0,'',0,'2010-04-01 11:38:53'),(12,3,'','2010-04-01 11:38:01','127.0.0.1','11:38:01','11:38:01',0,'',0,'2010-04-01 11:39:45'),(13,1,'','2010-04-01 11:39:54','192.168.1.108','11:39:54','11:39:54',0,'',0,'2010-04-01 12:58:09'),(14,4,'','2010-04-01 11:45:06','192.168.1.108','11:45:06','11:45:06',0,'',0,'2010-04-01 12:59:06'),(15,1,'','2010-04-01 12:59:20','192.168.1.108','12:59:20','12:59:20',0,'',0,'2010-04-01 13:00:09'),(16,1,'','2010-04-01 13:06:31','127.0.0.1','13:06:31','13:06:31',0,'',0,'2010-04-01 16:12:58'),(17,1,'','2010-04-01 16:13:01','192.168.1.13','16:13:01','16:13:01',0,'',0,'2010-04-01 16:29:33'),(18,1,'','2010-04-01 16:33:48','192.168.1.108','16:33:48','16:53:56',1208,'',0,'2010-04-01 16:53:56'),(19,4,'','2010-04-01 16:54:33','192.168.1.108','16:54:33','16:54:33',0,'',0,'2010-04-01 17:52:18'),(20,1,'','2010-04-01 16:56:44','192.168.1.108','16:56:44','16:56:44',0,'',0,'2010-04-01 20:19:04'),(21,3,'','2010-04-01 17:33:56','127.0.0.1','17:33:56','19:40:07',7571,'',0,'2010-04-01 19:40:07'),(22,4,'','2010-04-01 19:40:15','127.0.0.1','19:40:15','20:05:04',1489,'',0,'2010-04-01 20:05:04'),(23,3,'','2010-04-01 20:05:11','127.0.0.1','20:05:11','20:36:08',1857,'',0,'2010-04-01 20:36:08'),(24,4,'','2010-04-01 20:36:16','127.0.0.1','20:36:16','20:36:16',0,'',0,'2010-04-01 20:37:02'),(25,3,'','2010-04-02 10:55:01','127.0.0.1','10:55:01','10:55:44',43,'',0,'2010-04-02 10:55:44'),(26,4,'','2010-04-02 10:55:53','127.0.0.1','10:55:53','12:21:15',5122,'',0,'2010-04-02 12:21:15'),(27,1,'','2010-04-02 10:58:05','127.0.0.1','10:58:05','10:58:05',0,'',0,'2010-04-02 13:10:24'),(28,3,'','2010-04-02 12:21:20','127.0.0.1','12:21:20','12:21:20',0,'',0,'2010-04-02 13:03:03'),(29,1,'','2010-04-02 14:38:41','127.0.0.1','14:38:41','14:38:41',0,'',0,'2010-04-02 14:41:06'),(30,1,'','2010-04-02 14:43:45','192.168.1.108','14:43:45','14:43:45',0,'',0,'2010-04-02 15:24:56'),(31,3,'','2010-04-02 15:06:35','127.0.0.1','15:06:35','15:23:51',1036,'',0,'2010-04-02 15:23:51'),(32,4,'','2010-04-02 15:23:59','127.0.0.1','15:23:59','15:23:59',0,'',0,'2010-04-02 15:28:38'),(33,1,'','2010-04-02 15:43:01','192.168.1.108','15:43:01','18:01:04',8283,'',0,'2010-04-02 18:01:04'),(34,3,'','2010-04-02 17:41:18','127.0.0.1','17:41:18','17:44:41',203,'',0,'2010-04-02 17:44:40'),(35,4,'','2010-04-02 17:44:47','127.0.0.1','17:44:47','17:44:47',0,'',0,'2010-04-02 18:18:16'),(36,1,'','2010-04-02 18:01:35','192.168.1.108','18:01:35','18:01:35',0,'',0,'2010-04-02 18:12:30'),(37,1,'','2010-04-02 18:13:03','127.0.0.1','18:13:03','19:10:35',3452,'',0,'2010-04-02 19:10:35'),(38,4,'','2010-04-02 19:41:29','127.0.0.1','19:41:29','19:42:14',45,'',0,'2010-04-02 19:42:14'),(39,3,'','2010-04-02 19:42:20','127.0.0.1','19:42:20','20:24:48',2548,'',0,'2010-04-02 20:24:48'),(40,1,'','2010-04-02 20:24:54','127.0.0.1','20:24:54','20:30:40',346,'',0,'2010-04-02 20:30:40'),(41,1,'','2010-04-02 20:33:41','127.0.0.1','20:33:41','20:33:41',0,'',0,'2010-04-02 21:15:26'),(42,1,'','2010-04-06 11:03:03','192.168.1.108','11:03:03','12:05:52',3769,'',0,'2010-04-06 11:03:40'),(43,1,'','2010-04-06 12:05:58','192.168.1.108','12:05:58','12:06:45',47,'',0,'2010-04-06 12:06:44'),(44,4,'','2010-04-06 12:06:57','192.168.1.108','12:06:57','12:06:57',0,'',0,'2010-04-06 12:28:26'),(45,1,'','2010-04-06 12:12:39','192.168.1.108','12:12:39','12:12:39',0,'',0,'2010-04-06 12:41:07'),(46,4,'','2010-04-06 12:31:32','192.168.1.108','12:31:32','12:31:32',0,'',0,'2010-04-06 12:41:34'),(47,1,'','2010-04-06 13:02:17','192.168.1.13','13:02:17','13:02:17',0,'',0,'2010-04-06 13:14:01'),(48,1,'','2010-04-06 14:43:14','192.168.1.108','14:43:14','14:43:14',0,'',0,'2010-04-06 15:04:03'),(49,1,'','2010-04-06 18:10:26','192.168.1.13','18:10:26','18:10:26',0,'',0,'2010-04-06 20:49:50'),(50,1,'','2010-04-07 12:55:42','127.0.0.1','12:55:42','12:55:42',0,'',0,'2010-04-07 12:56:38'),(51,1,'','2010-04-07 14:26:05','127.0.0.1','14:26:05','14:26:05',0,'',0,'2010-04-07 14:36:47'),(52,1,'','2010-04-07 14:40:31','192.168.1.108','14:40:31','17:13:12',9161,'',0,'2010-04-07 17:13:12'),(53,4,'','2010-04-07 17:13:34','192.168.1.108','17:13:34','17:15:53',139,'',0,'2010-04-07 17:15:52'),(54,1,'','2010-04-07 17:16:01','192.168.1.108','17:16:01','17:25:41',580,'',0,'2010-04-07 17:25:41'),(55,4,'','2010-04-07 17:25:50','192.168.1.108','17:25:50','17:27:47',117,'',0,'2010-04-07 17:27:47'),(56,1,'','2010-04-07 17:27:55','192.168.1.108','17:27:55','17:32:29',274,'',0,'2010-04-07 17:32:29'),(57,4,'','2010-04-07 17:32:37','192.168.1.108','17:32:37','17:36:58',261,'',0,'2010-04-07 17:36:58'),(58,1,'','2010-04-07 17:33:29','127.0.0.1','17:33:29','17:33:29',0,'',0,'2010-04-07 17:35:40'),(59,1,'','2010-04-07 17:37:06','192.168.1.108','17:37:06','17:38:05',59,'',0,'2010-04-07 17:38:05'),(60,5,'','2010-04-07 17:38:15','192.168.1.108','17:38:15','17:51:34',799,'',0,'2010-04-07 17:38:50'),(61,1,'','2010-04-07 17:39:58','127.0.0.1','17:39:58','17:40:03',5,'',0,'2010-04-07 17:40:03'),(62,5,'','2010-04-07 17:40:11','127.0.0.1','17:40:11','20:26:14',9963,'',0,'2010-04-07 17:51:33'),(63,1,'','2010-04-07 17:44:59','127.0.0.1','17:44:59','17:44:59',0,'',0,'2010-04-07 17:56:23'),(64,5,'','2010-04-07 17:51:47','192.168.1.108','17:51:47','17:56:53',306,'',0,'2010-04-07 17:56:53'),(65,1,'','2010-04-07 17:56:59','192.168.1.108','17:56:59','18:00:08',189,'',0,'2010-04-07 18:00:08'),(66,5,'','2010-04-07 18:00:20','192.168.1.108','18:00:20','18:03:33',193,'',0,'2010-04-07 18:03:33'),(67,1,'','2010-04-07 18:00:43','127.0.0.1','18:00:43','18:00:43',0,'',0,'2010-04-07 18:03:37'),(68,5,'','2010-04-07 18:03:40','192.168.1.108','18:03:40','18:04:00',20,'',0,'2010-04-07 18:03:59'),(69,1,'','2010-04-07 18:04:08','192.168.1.108','18:04:08','18:04:18',10,'',0,'2010-04-07 18:04:18'),(70,5,'','2010-04-07 18:04:30','192.168.1.108','18:04:30','18:04:30',0,'',0,'2010-04-07 18:04:46'),(71,5,'','2010-04-07 18:04:46','192.168.1.108','18:04:46','18:09:11',265,'',0,'2010-04-07 18:09:11'),(72,1,'','2010-04-07 18:05:56','127.0.0.1','18:05:56','18:05:56',0,'',0,'2010-04-07 18:35:55'),(73,4,'','2010-04-07 18:09:18','192.168.1.108','18:09:18','18:11:02',104,'',0,'2010-04-07 18:11:02'),(74,5,'','2010-04-07 18:11:10','192.168.1.108','18:11:10','18:17:32',382,'',0,'2010-04-07 18:17:32'),(75,4,'','2010-04-07 18:17:43','192.168.1.108','18:17:43','18:22:04',261,'',0,'2010-04-07 18:22:04'),(76,5,'','2010-04-07 18:22:11','192.168.1.108','18:22:11','18:30:13',482,'',0,'2010-04-07 18:30:13'),(77,4,'','2010-04-07 18:30:22','192.168.1.108','18:30:22','18:30:50',28,'',0,'2010-04-07 18:30:50'),(78,4,'','2010-04-07 18:32:15','192.168.1.108','18:32:15','18:33:34',79,'',0,'2010-04-07 18:33:34'),(79,5,'','2010-04-07 18:33:42','192.168.1.108','18:33:42','18:37:42',240,'',0,'2010-04-07 18:37:42'),(80,1,'','2010-04-07 18:37:47','192.168.1.108','18:37:47','18:41:18',211,'',0,'2010-04-07 18:41:18'),(81,5,'','2010-04-07 18:41:24','192.168.1.108','18:41:24','19:07:33',1569,'',0,'2010-04-07 19:07:33'),(82,1,'','2010-04-07 18:42:54','127.0.0.1','18:42:54','18:42:54',0,'',0,'2010-04-07 19:40:54'),(83,4,'','2010-04-07 19:07:43','192.168.1.108','19:07:43','19:11:28',225,'',0,'2010-04-07 19:11:27'),(84,5,'','2010-04-07 19:11:37','192.168.1.108','19:11:37','20:02:43',3066,'',0,'2010-04-07 20:02:43'),(85,1,'','2010-04-07 19:43:18','192.168.1.108','19:43:18','19:43:18',0,'',0,'2010-04-07 20:47:13'),(86,5,'','2010-04-07 20:02:52','192.168.1.108','20:02:52','20:03:15',23,'',0,'2010-04-07 20:03:14'),(87,4,'','2010-04-07 20:03:22','192.168.1.108','20:03:22','20:03:22',0,'',0,'2010-04-07 20:13:30'),(88,4,'','2010-04-07 20:13:38','192.168.1.108','20:13:38','20:13:38',0,'',0,'2010-04-07 20:55:47'),(89,1,'','2010-04-08 10:31:30','192.168.1.108','10:31:30','10:31:30',0,'',0,'2010-04-08 10:32:47'),(90,1,'','2010-04-08 10:34:19','127.0.0.1','10:34:19','10:34:19',0,'',0,'2010-04-08 13:29:19'),(91,4,'','2010-04-08 10:36:30','192.168.1.108','10:36:30','10:41:19',289,'',0,'2010-04-08 10:41:19'),(92,5,'','2010-04-08 10:41:28','192.168.1.108','10:41:28','10:59:15',1067,'',0,'2010-04-08 10:49:05'),(93,5,'','2010-04-08 10:50:43','127.0.0.1','10:50:43','10:52:32',109,'',0,'2010-04-08 10:52:32'),(94,4,'','2010-04-08 10:52:40','127.0.0.1','10:52:40','12:00:20',4060,'',0,'2010-04-08 10:53:13'),(95,5,'','2010-04-08 10:59:23','192.168.1.108','10:59:23','10:59:29',6,'',0,'2010-04-08 10:59:29'),(96,4,'','2010-04-08 10:59:35','192.168.1.108','10:59:35','10:59:35',0,'',0,'2010-04-08 12:20:07'),(97,4,'','2010-04-08 12:21:27','127.0.0.1','12:21:27','12:37:27',960,'',0,'2010-04-08 12:37:27'),(98,4,'','2010-04-08 12:38:00','127.0.0.1','12:38:00','13:00:30',1350,'',0,'2010-04-08 13:00:30'),(99,4,'','2010-04-08 13:02:10','192.168.1.108','13:02:10','13:02:10',0,'',0,'2010-04-08 13:17:17'),(100,3,'','2010-04-08 13:02:27','127.0.0.1','13:02:27','13:02:27',0,'',0,'2010-04-08 13:08:12'),(101,1,'','2010-04-08 14:34:13','127.0.0.1','14:34:13','14:34:13',0,'',0,'2010-04-08 14:34:17'),(102,1,'','2010-04-08 14:42:28','192.168.1.13','14:42:28','14:42:28',0,'',0,'2010-04-08 14:51:34'),(103,4,'','2010-04-08 14:48:35','192.168.1.108','14:48:35','18:18:22',12587,'',0,'2010-04-08 16:25:55'),(104,1,'','2010-04-08 14:52:19','192.168.1.9','14:52:19','14:52:19',0,'',0,'2010-04-08 18:07:32'),(105,4,'','2010-04-08 16:28:02','127.0.0.1','16:28:02','19:20:36',10354,'',0,'2010-04-08 18:18:22'),(106,1,'','2010-04-08 18:18:37','192.168.1.108','18:18:37','18:19:23',46,'',0,'2010-04-08 18:19:23'),(107,4,'','2010-04-08 18:19:32','192.168.1.108','18:19:32','18:19:32',0,'',0,'2010-04-08 19:44:13'),(108,5,'','2010-04-08 18:25:12','192.168.1.108','18:25:12','18:35:28',616,'',0,'2010-04-08 18:35:28'),(109,1,'','2010-04-08 18:30:29','192.168.1.13','18:30:29','18:30:29',0,'',0,'2010-04-08 18:35:09'),(110,1,'','2010-04-08 18:35:47','192.168.1.108','18:35:47','18:35:47',0,'',0,'2010-04-08 19:06:15'),(111,1,'','2010-04-08 19:06:55','192.168.1.108','19:06:55','19:06:58',3,'',0,'2010-04-08 19:06:58'),(112,1,'','2010-04-08 19:07:12','192.168.1.108','19:07:12','19:07:12',0,'',0,'2010-04-08 20:39:26'),(113,3,'','2010-04-08 19:20:42','127.0.0.1','19:20:42','19:20:42',0,'',0,'2010-04-08 20:36:48'),(114,4,'','2010-04-08 20:37:24','192.168.1.108','20:37:24','20:37:24',0,'',0,'2010-04-08 20:38:11'),(115,1,'','2010-04-09 11:15:20','127.0.0.1','11:15:20','11:15:20',0,'',0,'2010-04-09 11:15:28'),(116,1,'','2010-04-09 11:21:04','192.168.1.108','11:21:04','11:21:17',13,'',0,'2010-04-09 11:21:17'),(117,4,'','2010-04-09 11:21:24','192.168.1.108','11:21:24','11:21:24',0,'',0,'2010-03-09 13:39:30'),(118,1,'','2010-04-09 11:22:53','127.0.0.1','11:22:53','11:22:53',0,'',0,'2010-04-09 11:57:21'),(119,1,'','2010-04-09 12:58:25','192.168.1.108','12:58:25','12:58:25',0,'',0,'2010-04-09 13:38:03'),(120,1,'','2010-04-09 13:40:01','192.168.1.13','13:40:01','13:40:01',0,'',0,'2010-04-09 16:20:13'),(121,4,'','2010-04-09 13:42:59','192.168.1.108','13:42:59','16:29:56',10017,'',0,'2010-04-09 16:29:56'),(122,7,'','2010-04-09 13:44:14','127.0.0.1','13:44:14','19:53:53',22179,'',0,'2010-04-09 18:43:07'),(123,1,'','2010-04-09 16:22:59','192.168.1.13','16:22:59','16:33:56',657,'',0,'2010-04-09 16:29:24'),(124,1,'','2010-04-09 16:30:01','192.168.1.108','16:30:01','16:30:01',0,'',0,'2010-04-09 16:33:56'),(125,1,'','2010-04-09 16:34:06','192.168.1.13','16:34:06','16:34:06',0,'',0,'2010-04-09 17:38:01'),(126,1,'','2010-04-09 18:09:28','127.0.0.1','18:09:28','18:09:28',0,'',0,'2010-04-09 18:13:00'),(127,1,'','2010-04-09 18:16:12','192.168.1.108','18:16:12','19:37:04',4852,'',0,'2010-04-09 18:32:40'),(128,1,'','2010-04-09 18:34:06','192.168.1.13','18:34:06','20:14:29',6023,'',0,'2010-04-09 19:37:09'),(129,1,'','2010-04-09 19:37:10','192.168.1.108','19:37:10','19:37:48',38,'',0,'2010-04-09 19:37:48'),(130,1,'','2010-04-09 19:38:36','192.168.1.108','19:38:36','19:38:36',0,'',0,'2010-04-09 19:39:56'),(131,1,'','2010-04-09 19:40:14','127.0.0.1','19:40:14','19:40:14',0,'',0,'2010-04-09 20:16:23'),(132,3,'','2010-04-09 20:15:19','192.168.1.13','20:15:19','20:17:18',119,'',0,'2010-04-09 20:17:18'),(133,1,'','2010-04-09 20:17:29','192.168.1.13','20:17:29','20:44:03',1594,'',0,'2010-04-09 20:44:03'),(134,3,'','2010-04-09 20:44:11','192.168.1.13','20:44:11','20:46:22',131,'',0,'2010-04-09 20:46:21'),(135,1,'','2010-04-09 20:44:42','192.168.1.108','20:44:42','20:44:42',0,'',0,'2010-04-09 20:45:37'),(136,1,'','2010-04-09 20:46:29','192.168.1.13','20:46:29','20:47:24',55,'',0,'2010-04-09 20:47:24'),(137,3,'','2010-04-09 20:47:32','192.168.1.13','20:47:32','20:47:54',22,'',0,'2010-04-09 20:47:54'),(138,1,'','2010-04-09 20:48:12','192.168.1.13','20:48:12','20:48:50',38,'',0,'2010-04-09 20:48:50'),(139,1,'','2010-04-09 20:49:22','192.168.1.13','20:49:22','20:49:22',0,'',0,'2010-04-09 21:07:27'),(140,8,'','2010-04-09 20:55:00','192.168.1.13','20:55:00','21:06:34',694,'',0,'2010-04-09 21:06:33'),(141,3,'','2010-04-09 21:06:42','192.168.1.13','21:06:42','21:06:59',17,'',0,'2010-04-09 21:06:59'),(142,8,'','2010-04-09 21:07:07','192.168.1.13','21:07:07','21:07:39',32,'',0,'2010-04-09 21:07:39'),(143,1,'','2010-04-09 21:08:36','192.168.1.13','21:08:36','21:11:44',188,'',0,'2010-04-09 21:11:44'),(144,8,'','2010-04-09 21:11:58','192.168.1.13','21:11:58','21:11:58',0,'',0,'2010-04-09 21:12:08'),(145,1,'','2010-04-09 21:14:23','192.168.1.13','21:14:23','21:14:23',0,'',0,'2010-04-09 21:14:31'),(146,1,'','2010-04-09 21:15:36','127.0.0.1','21:15:36','21:15:36',0,'',0,'2010-04-09 21:43:54'),(147,3,'','2010-04-10 10:51:50','127.0.0.1','10:51:50','12:17:49',5159,'',0,'2010-04-10 12:17:49'),(148,1,'','2010-04-10 10:58:07','127.0.0.1','10:58:07','10:58:07',0,'',0,'2010-04-10 11:42:49'),(149,1,'','2010-04-10 13:05:25','127.0.0.1','13:05:25','13:05:25',0,'',0,'2010-04-10 13:05:38'),(150,1,'','2010-04-10 13:14:37','127.0.0.1','13:14:37','13:14:37',0,'',0,'2010-04-10 13:31:20'),(151,1,'','2010-04-12 11:47:29','192.168.1.108','11:47:29','11:47:29',0,'',0,'2010-04-12 13:23:24'),(152,1,'','2010-04-12 14:35:13','192.168.1.108','14:35:13','14:35:13',0,'',0,'2010-04-12 14:35:47'),(153,1,'','2010-04-12 14:36:05','127.0.0.1','14:36:05','14:36:05',0,'',0,'2010-04-12 16:03:22'),(154,1,'','2010-04-12 16:03:24','127.0.0.1','16:03:24','19:53:08',13784,'',0,'2010-04-12 19:53:07'),(155,1,'','2010-04-12 19:53:15','127.0.0.1','19:53:15','19:53:15',0,'',0,'2010-04-12 20:08:26'),(156,1,'','2010-04-12 20:23:28','192.168.1.13','20:23:28','20:23:28',0,'',0,'2010-04-12 20:29:35'),(157,2,'','2010-04-12 22:31:43','115.240.101.85','22:31:43','22:32:08',25,'',0,'2010-04-12 22:32:08'),(158,1,'','2010-04-12 22:32:43','115.240.101.85','22:32:43','22:32:43',0,'',0,'2010-04-12 22:37:14'),(159,1,'','2010-04-13 12:11:53','115.240.7.156','12:11:53','12:11:53',0,'',0,'2010-04-13 12:12:02'),(160,1,'','2010-04-13 12:12:46','90.202.182.248','12:12:46','12:12:46',0,'',0,'2010-04-13 12:22:47'),(161,1,'','2010-04-13 12:43:59','121.246.215.55','12:43:59','12:43:59',0,'',0,'2010-04-13 13:08:39'),(162,1,'','2010-04-13 15:01:45','85.72.247.205','15:01:45','15:03:01',76,'',0,'2010-04-13 15:03:01'),(163,1,'','2010-04-13 15:08:07','85.72.247.205','15:08:07','15:08:07',0,'',0,'2010-04-13 15:12:04'),(164,1,'','2010-04-13 17:44:54','121.246.215.55','17:44:54','18:26:42',2508,'',0,'2010-04-13 18:00:33'),(165,1,'','2010-04-13 18:00:35','115.242.6.180','18:00:35','18:00:35',0,'',0,'2010-04-13 18:26:42'),(166,9,'','2010-04-13 18:27:37','121.246.215.55','18:27:37','18:27:37',0,'',0,'2010-04-13 18:27:58'),(167,1,'','2010-04-13 18:31:15','121.246.215.55','18:31:15','18:31:15',0,'',0,'2010-04-13 18:31:17'),(168,1,'','2010-04-13 18:31:17','121.246.215.55','18:31:17','18:31:17',0,'',0,'2010-04-13 18:34:16'),(169,1,'','2010-04-14 10:22:44','121.246.215.55','10:22:44','10:22:44',0,'',0,'2010-04-14 10:31:29'),(170,1,'','2010-04-14 11:59:58','121.246.215.55','11:59:58','11:59:58',0,'',0,'2010-04-14 11:59:59'),(171,2,'','2010-04-14 12:00:15','121.246.215.55','12:00:15','12:00:23',8,'',0,'2010-04-14 12:00:23'),(172,1,'','2010-04-14 12:00:42','121.246.215.55','12:00:42','12:00:42',0,'',0,'2010-04-14 12:01:11'),(173,1,'','2010-04-14 12:03:53','121.246.215.55','12:03:53','12:52:35',2922,'',0,'2010-04-14 12:52:35'),(174,1,'','2010-04-14 15:59:45','121.246.215.55','15:59:45','15:59:45',0,'',0,'2010-04-14 16:00:07'),(175,1,'','2010-04-14 16:24:40','121.246.215.55','16:24:40','16:24:40',0,'',0,'2010-04-14 16:24:40'),(176,1,'','2010-04-14 16:53:25','121.246.215.55','16:53:25','16:53:25',0,'',0,'2010-04-14 17:52:09'),(177,10,'','2010-04-14 17:04:34','121.246.215.55','17:04:34','17:04:34',0,'',0,'2010-04-14 17:07:18'),(178,10,'','2010-04-14 17:13:18','121.246.215.55','17:13:18','17:34:44',1286,'',0,'2010-04-14 17:34:44'),(179,1,'','2010-04-14 18:23:09','121.246.215.55','18:23:09','18:23:09',0,'',0,'2010-04-14 18:42:35'),(180,1,'','2010-04-15 06:10:09','115.240.43.225','06:10:09','06:10:09',0,'',0,'2010-04-15 06:12:36'),(181,1,'','2010-04-15 11:18:10','115.242.18.109','11:18:10','11:30:27',737,'',0,'2010-04-15 11:30:27'),(182,11,'','2010-04-15 11:31:16','115.242.18.109','11:31:16','11:31:16',0,'',0,'2010-04-15 12:07:00'),(183,1,'','2010-04-15 13:21:27','121.246.215.55','13:21:27','13:27:44',377,'',0,'2010-04-15 13:27:44'),(184,1,'','2010-04-15 17:24:29','115.240.222.109','17:24:29','17:24:29',0,'',0,'2010-04-15 17:32:17'),(185,1,'','2010-04-16 07:01:28','121.246.215.55','07:01:28','07:01:28',0,'',0,'2010-04-16 07:56:33'),(186,1,'','2010-04-16 08:08:37','121.246.215.55','08:08:37','08:08:37',0,'',0,'2010-04-16 08:35:02'),(187,1,'','2010-04-16 10:51:45','85.72.208.251','10:51:45','13:47:54',10569,'',0,'2010-04-16 12:48:22'),(188,1,'','2010-04-16 12:50:02','121.246.215.55','12:50:02','12:50:02',0,'',0,'2010-04-16 13:47:54'),(189,1,'','2010-04-16 13:48:04','85.72.208.251','13:48:04','13:48:04',0,'',0,'2010-04-16 14:33:17'),(190,6,'','2010-04-16 14:24:17','85.72.208.251','14:24:17','14:24:17',0,'',0,'2010-04-16 14:24:19'),(191,6,'','2010-04-16 14:24:35','85.72.208.251','14:24:35','14:24:35',0,'',0,'2010-04-16 14:24:37'),(192,1,'','2010-04-16 14:33:22','85.72.208.251','14:33:22','14:33:22',0,'',0,'2010-04-16 16:14:31'),(193,1,'','2010-04-16 16:14:44','121.246.215.55','16:14:44','16:14:44',0,'',0,'2010-04-16 17:04:10'),(194,1,'','2010-04-16 17:04:13','85.72.208.251','17:04:13','17:09:13',300,'',0,'2010-04-16 17:09:12'),(195,6,'','2010-04-16 17:08:47','85.72.208.251','17:08:47','17:08:47',0,'',0,'2010-04-16 17:08:50'),(196,6,'','2010-04-16 17:09:04','85.72.208.251','17:09:04','17:09:04',0,'',0,'2010-04-16 17:09:09'),(197,6,'','2010-04-16 17:09:26','85.72.208.251','17:09:26','17:09:26',0,'',0,'2010-04-16 17:09:29'),(198,1,'','2010-04-16 17:09:48','85.72.208.251','17:09:48','17:12:28',160,'',0,'2010-04-16 17:12:27'),(199,12,'','2010-04-16 17:15:14','85.72.208.251','17:15:14','17:15:14',0,'',0,'2010-04-16 17:45:04'),(200,1,'','2010-04-16 17:22:54','121.246.215.55','17:22:54','17:22:54',0,'',0,'2010-04-16 17:50:15'),(201,1,'','2010-04-16 17:53:59','121.246.215.55','17:53:59','17:53:59',0,'',0,'2010-04-16 17:54:59'),(202,1,'','2010-04-16 17:55:14','121.246.215.55','17:55:14','17:55:14',0,'',0,'2010-04-16 18:01:22'),(203,1,'','2010-04-16 19:58:57','115.242.31.123','19:58:57','19:58:57',0,'',0,'2010-04-16 21:50:08'),(204,1,'','2010-04-16 23:35:25','115.242.31.123','23:35:25','23:35:25',0,'',0,'2010-04-16 23:49:25'),(205,1,'','2010-04-17 23:13:30','90.202.182.248','23:13:30','23:13:30',0,'',0,'2010-04-17 23:30:48'),(206,1,'','2010-04-19 11:41:59','85.72.246.226','11:41:59','11:41:59',0,'',0,'2010-04-19 11:50:00'),(207,1,'','2010-04-19 11:50:13','85.72.246.226','11:50:13','11:50:13',0,'',0,'2010-04-19 13:03:55'),(208,1,'','2010-04-19 14:35:51','85.72.246.226','14:35:51','14:35:51',0,'',0,'2010-04-19 15:34:56'),(209,1,'','2010-04-19 15:48:13','121.246.215.55','15:48:13','16:03:18',905,'',0,'2010-04-19 16:03:18'),(210,13,'','2010-04-19 16:07:57','121.246.215.55','16:07:57','16:14:56',419,'',0,'2010-04-19 16:14:56'),(211,1,'','2010-04-19 16:15:53','121.246.215.55','16:15:53','16:15:53',0,'',0,'2010-04-19 18:04:40'),(212,1,'','2010-04-20 11:20:43','85.72.215.149','11:20:43','11:20:43',0,'',0,'2010-04-20 12:19:24'),(213,6,'','2010-04-20 11:30:08','85.72.215.149','11:30:08','11:30:08',0,'',0,'2010-04-20 11:30:11'),(214,14,'','2010-04-20 11:32:57','85.72.215.149','11:32:57','11:37:43',286,'',0,'2010-04-20 11:37:43'),(215,14,'','2010-04-20 11:38:21','85.72.215.149','11:38:21','12:13:18',2097,'',0,'2010-04-20 12:13:18'),(216,12,'','2010-04-20 12:24:03','85.72.215.149','12:24:03','12:24:03',0,'',0,'2010-04-20 13:21:32'),(217,1,'','2010-04-20 13:20:29','85.72.215.149','13:20:29','13:20:29',0,'',0,'2010-04-20 13:21:52'),(218,1,'','2010-04-23 15:35:44','121.246.215.55','15:35:44','15:35:44',0,'',0,'2010-04-23 16:01:53'),(219,1,'','2010-04-24 14:53:46','115.242.47.220','14:53:46','14:53:46',0,'',0,'2010-04-24 15:24:06'),(220,1,'','2010-04-24 20:19:02','127.0.0.1','20:19:02','20:19:02',0,'',0,'2010-04-24 20:19:02'),(221,1,'','2010-04-24 21:47:26','127.0.0.1','21:47:26','21:47:26',0,'',0,'2010-04-24 21:47:26'),(222,1,'','2010-04-25 19:01:30','127.0.0.1','19:01:30','19:01:30',0,'',0,'2010-04-25 19:01:30'),(223,1,'','2010-04-26 12:24:26','127.0.0.1','12:24:26','12:24:26',0,'',0,'2010-04-26 12:24:26'),(224,1,'','2010-04-26 17:27:07','127.0.0.1','17:27:07','18:10:58',2631,'',0,'2010-04-26 17:27:07'),(225,1,'','2010-04-26 22:10:19','127.0.0.1','22:10:19','22:27:07',1008,'',0,'2010-04-26 22:10:19'),(226,1,'','2010-04-26 22:39:25','127.0.0.1','22:39:25','22:39:38',13,'',0,'2010-04-26 22:39:25'),(227,15,'','2010-04-26 22:55:49','127.0.0.1','22:55:49','22:58:41',172,'',0,'2010-04-26 22:55:49'),(228,1,'','2010-04-26 22:58:46','127.0.0.1','22:58:46','22:59:22',36,'',0,'2010-04-26 22:58:46'),(229,1,'','2010-04-26 23:00:51','127.0.0.1','23:00:51','23:04:27',216,'',0,'2010-04-26 23:00:51'),(230,1,'','2010-04-26 23:04:53','127.0.0.1','23:04:53','23:04:53',0,'',0,'2010-04-26 23:04:53'),(231,1,'','2010-04-27 12:37:41','127.0.0.1','12:37:41','12:37:41',0,'',0,'2010-04-27 12:37:41'),(232,16,'','2010-04-27 19:47:35','127.0.0.1','19:47:35','19:49:09',94,'',0,'2010-04-27 19:47:35'),(233,1,'','2010-04-27 19:51:50','127.0.0.1','19:51:50','19:51:50',0,'',0,'2010-04-27 19:51:50'),(234,1,'','2010-04-28 12:16:06','127.0.0.1','12:16:06','12:16:06',0,'',0,'2010-04-28 12:16:06'),(235,1,'','2010-04-28 12:37:08','127.0.0.1','12:37:08','12:37:38',30,'',0,'2010-04-28 12:37:08'),(236,1,'','2010-04-28 12:38:31','127.0.0.1','12:38:31','12:39:40',69,'',0,'2010-04-28 12:38:31'),(237,17,'','2010-04-28 12:47:11','127.0.0.1','12:47:11','12:49:53',162,'',0,'2010-04-28 12:47:11'),(238,18,'','2010-04-28 12:56:18','127.0.0.1','12:56:18','12:58:18',120,'',0,'2010-04-28 12:56:18'),(239,1,'','2010-04-28 12:58:23','127.0.0.1','12:58:23','12:59:16',53,'',0,'2010-04-28 12:58:23'),(240,4,'','2010-04-28 12:59:25','127.0.0.1','12:59:25','12:59:25',0,'',0,'2010-04-28 12:59:25'),(241,4,'','2010-04-28 13:00:58','127.0.0.1','13:00:58','13:01:06',8,'',0,'2010-04-28 13:00:58'),(242,2,'','2010-04-28 13:01:12','127.0.0.1','13:01:12','13:01:12',0,'',0,'2010-04-28 13:01:12'),(243,2,'','2010-04-28 13:02:14','127.0.0.1','13:02:14','13:02:14',0,'',0,'2010-04-28 13:02:14'),(244,2,'','2010-04-28 13:02:20','127.0.0.1','13:02:20','13:02:20',0,'',0,'2010-04-28 13:02:20'),(245,2,'','2010-04-28 13:02:48','127.0.0.1','13:02:48','13:02:48',0,'',0,'2010-04-28 13:02:48'),(246,2,'','2010-04-28 13:04:14','127.0.0.1','13:04:14','13:04:14',0,'',0,'2010-04-28 13:04:14'),(247,2,'','2010-04-28 13:10:08','127.0.0.1','13:10:08','13:10:08',0,'',0,'2010-04-28 13:10:08'),(248,2,'','2010-04-28 13:10:14','127.0.0.1','13:10:14','13:10:14',0,'',0,'2010-04-28 13:10:14'),(249,2,'','2010-04-28 13:23:40','127.0.0.1','13:23:40','13:23:40',0,'',0,'2010-04-28 13:23:40'),(250,2,'','2010-04-28 13:24:21','127.0.0.1','13:24:21','13:37:23',782,'',0,'2010-04-28 13:24:21'),(251,1,'','2010-04-28 13:37:28','127.0.0.1','13:37:28','14:15:14',2266,'',0,'2010-04-28 13:37:28'),(252,4,'','2010-04-28 14:15:26','127.0.0.1','14:15:26','14:30:33',907,'',0,'2010-04-28 14:15:26'),(253,1,'','2010-04-28 14:30:38','127.0.0.1','14:30:38','14:31:09',31,'',0,'2010-04-28 14:30:38'),(254,4,'','2010-04-28 14:31:22','127.0.0.1','14:31:22','15:12:47',2485,'',0,'2010-04-28 14:31:22'),(255,4,'','2010-04-28 15:12:57','127.0.0.1','15:12:57','15:48:18',2121,'',0,'2010-04-28 15:12:57'),(256,1,'','2010-04-28 15:48:26','127.0.0.1','15:48:26','15:49:51',85,'',0,'2010-04-28 15:48:26'),(257,1,'','2010-04-28 15:52:30','127.0.0.1','15:52:30','15:52:58',28,'',0,'2010-04-28 15:52:30'),(258,1,'','2010-04-28 15:53:56','127.0.0.1','15:53:56','15:54:44',48,'',0,'2010-04-28 15:53:56'),(259,1,'','2010-04-28 15:55:08','127.0.0.1','15:55:08','15:55:34',26,'',0,'2010-04-28 15:55:08'),(260,4,'','2010-04-28 15:55:46','127.0.0.1','15:55:46','16:04:38',532,'',0,'2010-04-28 15:55:46'),(261,4,'','2010-04-28 16:04:54','127.0.0.1','16:04:54','16:05:06',12,'',0,'2010-04-28 16:04:54'),(262,1,'','2010-04-28 16:12:34','127.0.0.1','16:12:34','16:13:07',33,'',0,'2010-04-28 16:12:34'),(263,4,'','2010-04-28 16:13:20','127.0.0.1','16:13:20','16:13:38',18,'',0,'2010-04-28 16:13:20'),(264,1,'','2010-04-28 16:13:43','127.0.0.1','16:13:43','16:14:24',41,'',0,'2010-04-28 16:13:43'),(265,4,'','2010-04-28 16:14:31','127.0.0.1','16:14:31','16:15:04',33,'',0,'2010-04-28 16:14:31'),(266,1,'','2010-04-28 16:15:11','127.0.0.1','16:15:11','16:15:52',41,'',0,'2010-04-28 16:15:11'),(267,4,'','2010-04-28 16:17:07','127.0.0.1','16:17:07','16:17:55',48,'',0,'2010-04-28 16:17:07'),(268,19,'','2010-04-28 16:23:06','127.0.0.1','16:23:06','16:23:16',10,'',0,'2010-04-28 16:23:06'),(269,1,'','2010-04-28 16:33:13','127.0.0.1','16:33:13','16:33:13',0,'',0,'2010-04-28 16:33:13'),(270,1,'','2010-04-28 22:14:11','127.0.0.1','22:14:11','22:14:11',0,'',0,'2010-04-28 22:14:11'),(271,1,'','2010-04-29 13:17:40','127.0.0.1','13:17:40','15:00:35',6175,'',0,'2010-04-29 13:17:40'),(272,1,'','2010-04-29 15:03:17','127.0.0.1','15:03:17','15:03:17',0,'',0,'2010-04-29 15:03:17'),(273,1,'','2010-04-29 15:03:56','127.0.0.1','15:03:56','18:04:20',10824,'',0,'2010-04-29 15:03:56'),(274,4,'','2010-04-29 18:04:29','127.0.0.1','18:04:29','19:53:25',6536,'',0,'2010-04-29 18:04:29'),(275,1,'','2010-04-29 19:53:40','127.0.0.1','19:53:40','19:53:40',0,'',0,'2010-04-29 19:53:40'),(276,1,'','2010-04-29 20:22:37','127.0.0.1','20:22:37','03:13:30',-61747,'',0,'2010-04-29 20:22:37'),(277,1,'','2010-04-30 14:35:43','127.0.0.1','14:35:43','17:25:16',10173,'',0,'2010-04-30 14:35:43'),(278,1,'','2010-04-30 17:35:25','127.0.0.1','17:35:25','00:09:49',-62736,'',0,'2010-04-30 17:35:25'),(279,20,'','2010-05-01 00:10:14','127.0.0.1','00:10:14','00:10:25',11,'',0,'2010-05-01 00:10:14'),(280,1,'','2010-05-01 00:10:54','127.0.0.1','00:10:54','00:12:01',67,'',0,'2010-05-01 00:10:54'),(281,20,'','2010-05-01 00:12:12','127.0.0.1','00:12:12','00:14:27',135,'',0,'2010-05-01 00:12:12'),(282,1,'','2010-05-01 00:14:37','127.0.0.1','00:14:37','00:14:37',0,'',0,'2010-05-01 00:14:37'),(283,1,'','2010-05-01 13:21:34','192.168.1.100','13:21:34','13:21:34',0,'',0,'2010-05-01 13:21:34'),(284,1,'','2010-05-01 13:22:05','127.0.0.1','13:22:05','13:22:05',0,'',0,'2010-05-01 13:22:05'),(285,1,'','2010-05-01 14:11:56','192.168.1.100','14:11:56','14:11:56',0,'',0,'2010-05-01 14:11:56'),(286,1,'','2010-05-01 15:14:59','127.0.0.1','15:14:59','15:14:59',0,'',0,'2010-05-01 15:14:59'),(287,1,'','2010-05-02 02:35:35','127.0.0.1','02:35:35','02:35:35',0,'',0,'2010-05-02 02:35:35'),(288,1,'','2010-05-03 13:23:41','127.0.0.1','13:23:41','13:23:41',0,'',0,'2010-05-03 13:23:41'),(289,1,'','2010-05-03 15:01:02','127.0.0.1','15:01:02','15:01:02',0,'',0,'2010-05-03 15:01:02'),(290,1,'','2010-05-03 15:01:37','127.0.0.1','15:01:37','15:06:27',290,'',0,'2010-05-03 15:01:37'),(291,1,'','2010-05-03 15:26:05','127.0.0.1','15:26:05','15:26:05',0,'',0,'2010-05-03 15:26:05'),(292,1,'','2010-05-03 17:37:44','127.0.0.1','17:37:44','17:37:44',0,'',0,'2010-05-03 17:37:44'),(293,1,'','2010-05-03 22:26:42','127.0.0.1','22:26:42','22:26:42',0,'',0,'2010-05-03 22:26:42'),(294,1,'','2010-05-04 00:45:37','127.0.0.1','00:45:37','00:45:37',0,'',0,'2010-05-04 00:45:37'),(295,1,'','2010-05-04 11:59:33','127.0.0.1','11:59:33','15:30:03',12630,'',0,'2010-05-04 11:59:33'),(296,1,'','2010-05-04 15:31:57','127.0.0.1','15:31:57','15:31:57',0,'',0,'2010-05-04 15:31:57'),(297,22,'','2010-05-04 16:13:13','127.0.0.1','16:13:13','16:13:13',0,'',1,'2010-05-04 16:13:13'),(298,1,'','2010-05-04 16:17:53','127.0.0.1','16:17:53','17:53:01',5708,'',0,'2010-05-04 16:17:53'),(299,0,'','2010-05-04 17:55:00','127.0.0.1','17:55:00','17:55:08',8,'',0,'2010-05-04 17:55:00'),(300,0,'','2010-05-04 18:02:50','127.0.0.1','18:02:50','18:02:58',8,'',0,'2010-05-04 18:02:50'),(301,29,'','2010-05-04 18:03:04','127.0.0.1','18:03:04','18:03:07',3,'',0,'2010-05-04 18:03:04'),(302,29,'','2010-05-04 18:03:15','127.0.0.1','18:03:15','18:03:21',6,'',0,'2010-05-04 18:03:15'),(303,1,'','2010-05-04 18:03:42','127.0.0.1','18:03:42','18:04:02',20,'',0,'2010-05-04 18:03:42'),(304,29,'','2010-05-04 18:04:10','127.0.0.1','18:04:10','18:07:12',182,'',0,'2010-05-04 18:04:10'),(305,1,'','2010-05-04 18:07:18','127.0.0.1','18:07:18','18:07:18',0,'',0,'2010-05-04 18:07:18'),(306,0,'','2010-05-04 18:23:14','127.0.0.1','18:23:14','18:23:29',15,'',0,'2010-05-04 18:23:14'),(307,29,'','2010-05-04 18:23:36','127.0.0.1','18:23:36','18:23:47',11,'',0,'2010-05-04 18:23:36'),(308,1,'','2010-05-04 18:23:54','127.0.0.1','18:23:54','18:23:54',0,'',0,'2010-05-04 18:23:54'),(309,1,'','2010-05-04 18:24:52','127.0.0.1','18:24:52','18:24:52',0,'',0,'2010-05-04 18:24:52'),(310,31,'','2010-05-04 18:25:25','127.0.0.1','18:25:25','18:25:42',17,'',0,'2010-05-04 18:25:25'),(311,1,'','2010-05-04 18:25:49','127.0.0.1','18:25:49','18:27:28',99,'',0,'2010-05-04 18:25:49'),(312,31,'','2010-05-04 18:27:34','127.0.0.1','18:27:34','18:31:08',214,'',0,'2010-05-04 18:27:34'),(313,1,'','2010-05-04 18:31:20','127.0.0.1','18:31:20','18:31:20',0,'',0,'2010-05-04 18:31:20'),(314,1,'','2010-05-05 00:18:40','127.0.0.1','00:18:40','00:18:40',0,'',0,'2010-05-05 00:18:40'),(315,1,'','2010-05-05 00:29:50','127.0.0.1','00:29:50','00:29:50',0,'',0,'2010-05-05 00:29:50'),(316,1,'','2010-05-07 11:52:55','127.0.0.1','11:52:55','11:52:55',0,'',0,'2010-05-07 11:52:55'),(317,1,'','2010-05-08 00:14:11','127.0.0.1','00:14:11','00:14:11',0,'',0,'2010-05-08 00:14:11'),(318,1,'','2010-05-08 16:12:15','127.0.0.1','16:12:15','16:12:15',0,'',0,'2010-05-08 16:12:15'),(319,1,'','2010-05-08 18:01:29','127.0.0.1','18:01:29','18:01:29',0,'',0,'2010-05-08 18:01:29'),(320,1,'','2010-05-09 11:38:36','127.0.0.1','11:38:36','11:38:36',0,'',0,'2010-05-09 11:38:36'),(321,1,'','2010-05-09 11:39:10','127.0.0.1','11:39:10','11:39:10',0,'',0,'2010-05-09 11:39:10'),(322,1,'','2010-05-09 19:55:16','127.0.0.1','19:55:16','19:55:16',0,'',0,'2010-05-09 19:55:16'),(323,1,'','2010-05-10 05:06:45','127.0.0.1','05:06:45','05:06:45',0,'',0,'2010-05-10 05:06:45'),(324,1,'','2010-05-10 12:53:24','127.0.0.1','12:53:24','12:53:24',0,'',0,'2010-05-10 12:53:24'),(325,1,'','2010-05-11 12:17:23','127.0.0.1','12:17:23','12:17:23',0,'',0,'2010-05-11 12:17:23'),(326,1,'','2010-05-11 15:39:11','127.0.0.1','15:39:11','15:39:11',0,'',0,'2010-05-11 15:39:11'),(327,1,'','2010-05-12 01:46:09','127.0.0.1','01:46:09','01:46:09',0,'',0,'2010-05-12 01:46:09'),(328,1,'','2010-05-12 12:07:16','127.0.0.1','12:07:16','12:07:16',0,'',0,'2010-05-12 12:07:16'),(329,1,'','2010-05-12 12:46:48','127.0.0.1','12:46:48','23:11:34',37486,'',0,'2010-05-12 12:46:48'),(330,1,'','2010-05-13 15:56:23','192.168.1.102','15:56:23','15:56:23',0,'',0,'2010-05-13 15:56:23'),(331,1,'','2010-05-13 16:30:08','127.0.0.1','16:30:08','16:30:08',0,'',0,'2010-05-13 16:30:08'),(332,1,'','2010-05-13 18:22:08','192.168.1.9','18:22:08','18:22:08',0,'',0,'2010-05-13 18:22:08'),(333,1,'','2010-05-13 18:54:56','192.168.1.102','18:54:56','18:54:56',0,'',0,'2010-05-13 18:54:56'),(334,1,'','2010-05-13 22:17:03','127.0.0.1','22:17:03','22:17:03',0,'',0,'2010-05-13 22:17:03'),(335,1,'','2010-05-14 13:20:02','192.168.1.101','13:20:02','13:20:02',0,'',0,'2010-05-14 13:20:02'),(336,1,'','2010-05-14 13:25:23','192.168.1.102','13:25:23','13:25:23',0,'',0,'2010-05-14 13:25:23'),(337,1,'','2010-05-14 14:07:33','192.168.1.9','14:07:33','14:07:33',0,'',0,'2010-05-14 14:07:33'),(338,1,'','2010-05-14 14:45:18','192.168.1.5','14:45:18','14:45:18',0,'',0,'2010-05-14 14:45:18'),(339,1,'','2010-05-14 15:19:31','192.168.1.101','15:19:31','15:54:26',2095,'',0,'2010-05-14 15:19:31'),(340,1,'','2010-05-14 15:54:38','192.168.1.101','15:54:38','15:58:29',231,'',0,'2010-05-14 15:54:38'),(341,1,'','2010-05-14 15:58:39','192.168.1.101','15:58:39','16:00:20',101,'',0,'2010-05-14 15:58:39'),(342,1,'','2010-05-14 16:00:27','192.168.1.101','16:00:27','16:00:27',0,'',0,'2010-05-14 16:00:27'),(343,1,'','2010-05-14 16:02:26','192.168.1.16','16:02:26','19:37:58',12932,'',0,'2010-05-14 16:02:26'),(344,1,'','2010-05-14 17:25:59','127.0.0.1','17:25:59','17:25:59',0,'',0,'2010-05-14 17:25:59'),(345,1,'','2010-05-14 17:42:56','192.168.1.5','17:42:56','17:42:56',0,'',0,'2010-05-14 17:42:56'),(346,1,'','2010-05-14 17:59:21','127.0.0.1','17:59:21','17:59:21',0,'',0,'2010-05-14 17:59:21'),(347,1,'','2010-05-14 18:46:52','127.0.0.1','18:46:52','18:46:52',0,'',0,'2010-05-14 18:46:52'),(348,1,'','2010-05-14 19:04:38','127.0.0.1','19:04:38','19:04:38',0,'',0,'2010-05-14 19:04:38'),(349,1,'','2010-05-15 14:52:40','127.0.0.1','14:52:40','14:52:40',0,'',0,'2010-05-15 14:52:40'),(350,1,'','2010-05-15 20:40:35','127.0.0.1','20:40:35','20:40:35',0,'',1,'2010-05-15 20:40:35');
/*!40000 ALTER TABLE `tbl_sur_last_login_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sur_mem_privileges`
--

DROP TABLE IF EXISTS `tbl_sur_mem_privileges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sur_mem_privileges` (
  `member_privilege_id` int(11) NOT NULL AUTO_INCREMENT,
  `max_msg` int(11) NOT NULL,
  `max_photos` int(11) NOT NULL,
  `max_search` int(11) NOT NULL,
  `privilege_type_id` int(11) NOT NULL,
  PRIMARY KEY (`member_privilege_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sur_mem_privileges`
--

LOCK TABLES `tbl_sur_mem_privileges` WRITE;
/*!40000 ALTER TABLE `tbl_sur_mem_privileges` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_sur_mem_privileges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sur_p_app_pages`
--

DROP TABLE IF EXISTS `tbl_sur_p_app_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sur_p_app_pages` (
  `App_Page_ID` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `Section_Name` varchar(30) DEFAULT NULL,
  `Display_Page_Name` varchar(100) DEFAULT NULL,
  `Ref_Page_Code` varchar(100) NOT NULL,
  PRIMARY KEY (`App_Page_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sur_p_app_pages`
--

LOCK TABLES `tbl_sur_p_app_pages` WRITE;
/*!40000 ALTER TABLE `tbl_sur_p_app_pages` DISABLE KEYS */;
INSERT INTO `tbl_sur_p_app_pages` VALUES (1,'MASTERS','Company_Profile','Company_Profile'),(2,'MASTERS','Company_Profile_Add','Company_Profile_Add'),(3,'MASTERS','Company_Profile_Edit','Company_Profile_Edit'),(4,'MASTERS','Company_Profile_Del','Company_Profile_Del'),(5,'MASTERS','Director_ShareHolder_Member','Director_ShareHolder_Member'),(6,'MASTERS','Director_ShareHolder_Add','Director_ShareHolder_Add'),(7,'MASTERS','Subsriber_Add ','Subsriber_Add '),(8,'MASTERS','Director_ShareHolder_Member_Edit','Director_ShareHolder_Member_Edit'),(9,'MASTERS','Director_ShareHolder_Member_Del','Director_ShareHolder_Member_Del'),(10,'MASTERS','Director_ShareHolder_Member_Assign_User','Director_ShareHolder_Member_Assign_User'),(11,'MASTERS','Branch_List','Branch_List'),(12,'MASTERS','Branch_List_Add','Branch_List_Add'),(13,'MASTERS','Branch_List_Del','Branch_List_Del'),(14,'MASTERS','Area_List','Area_List'),(15,'MASTERS','Area_List_Add','Area_List_Add'),(16,'MASTERS','Area_List_Del','Area_List_Del'),(17,'MASTERS','Collection_Code_List ','Collection_Code_List'),(18,'MASTERS','Collection_Code_List_Add','Collection_Code_List_Add'),(19,'MASTERS','Collection_Code_List_Add_Group_Leader','Collection_Code_List_Add_Group_Leader'),(20,'MASTERS','Collection_Code_List_Del','Collection_Code_List_Del'),(21,'MASTERS','Bank_List','Bank_List'),(22,'MASTERS','Bank_List_Add','Bank_List_Add'),(23,'MASTERS','Bank_List_Del','Bank_List_Del'),(24,'MASTERS','Master_Code_List ','Master_Code_List '),(25,'MASTERS','Master_Code_List_Add','Master_Code_List_Add'),(26,'MASTERS','Master_Code_List_Del','Master_Code_List_Del'),(27,'MASTERS','Introducer_List','Introducer_List'),(28,'MASTERS','Introducer_List_Add','Introducer_List_Add'),(29,'MASTERS','Introducer_List_Del','Introducer_List_Del'),(30,'MASTERS','Branch_List_Edit','Branch_List_Edit'),(31,'MASTERS','Area_List_Edit','Area_List_Edit'),(32,'MASTERS','Collection_Code_List_Edit','Collection_Code_List_Edit'),(33,'MASTERS','Bank_List_Edit','Bank_List_Edit'),(34,'MASTERS','Master_Code_List_Edit','Master_Code_List_Edit'),(35,'MASTERS','Introducer_List_Edit','Introducer_List_Edit'),(36,'MASTERS','Introducer_Type_List ','Introducer_Type_List '),(37,'MASTERS','Introducer_Type_List_Add','Introducer_Type_List_Add'),(38,'MASTERS','Introducer_Type_List_Edit','Introducer_Type_List_Edit'),(39,'MASTERS','Introducer_Type_List_Del','Introducer_Type_List_Del'),(40,'ENROLLMENT','Group_Management','Group_Management'),(41,'ENROLLMENT','Group_Management_Add','Group_Management_Add'),(42,'ENROLLMENT','Group_Management_Edit','Group_Management_Edit'),(43,'ENROLLMENT','Group_Management_Del','Group_Management_Del'),(44,'ENROLLMENT','Group_Forming','Group_Forming'),(45,'ENROLLMENT','Group_Forming_Add','Group_Forming_Add'),(46,'ENROLLMENT','Group_Forming_Edit','Group_Forming_Edit'),(47,'ENROLLMENT','Group_Forming_Del','Group_Forming_Del'),(48,'ENROLLMENT','Group_Enrollment','Group_Enrollment'),(49,'ENROLLMENT','Group_Enrollment_Add','Group_Enrollment_Add'),(50,'ENROLLMENT','Group_Enrollment_Edit','Group_Enrollment_Edit'),(51,'ENROLLMENT','Group_Enrollment_Del','Group_Enrollment_Del'),(52,'ENROLLMENT','Applicant_Substitution','Applicant_Substitution'),(53,'ENROLLMENT','Applicant_Substitution_Add','Applicant_Substitution_Add'),(54,'ENROLLMENT','Applicant_Substitution_Edit','Applicant_Substitution_Edit'),(55,'ENROLLMENT','Applicant_Substitution_Del','Applicant_Substitution_Del'),(56,'ENTRY','Priza_Entry ','Priza_Entry'),(57,'ENTRY','Priza_Entry_Add','Priza_Entry_Add'),(58,'ENTRY','Priza_Entry_Edit','Priza_Entry_Edit'),(59,'ENTRY','Priza_Entry_Del','Priza_Entry_Del'),(60,'ENTRY','Intimation_Entry','Intimation_Entry'),(61,'ENTRY','Intimation_Entry_Add','Intimation_Entry_Add'),(62,'ENTRY','Intimation_Entry_Edit','Intimation_Entry_Edit'),(63,'ENTRY','Intimation_Entry_Del','Intimation_Entry_Del'),(64,'ENTRY','Subscription_Entry ','Subscription_Entry '),(65,'ENTRY','Subscription_Entry_Add ','Subscription_Entry_Add'),(66,'ENTRY','Subscription_Entry_Edit','Subscription_Entry_Edit'),(67,'ENTRY','Subscription_Entry_Del','Subscription_Entry_Del'),(68,'ENTRY','Recipt_List ','Recipt_List '),(69,'ENTRY','Recipt_List_Add ','Recipt_List_Add '),(70,'ENTRY','Recipt_List_Edit','Recipt_List_Edit'),(71,'ENTRY','Recipt_List_Del','Recipt_List_Del'),(72,'ENTRY','Intimation_Print','Intimation_Print'),(73,'ENTRY','Intimation_Print_Add','Intimation_Print_Add'),(74,'ENTRY','Intimation_Print_Edit','Intimation_Print_Edit'),(75,'ENTRY','Intimation_Print_Del','Intimation_Print_Del'),(76,'BANK_DEPOSITE','Bank_Deposite','Bank_Deposite'),(77,'BANK_DEPOSITE','Bank_Deposite_Add','Bank_Deposite_Add'),(78,'BANK_DEPOSITE','Bank_Deposite_Edit','Bank_Deposite_Edit'),(79,'BANK_DEPOSITE','Bank_Deposite_Del','Bank_Deposite_Del'),(80,'BANK_DEPOSITE','Bank_Deposite_Print','Bank_Deposite_Print'),(81,'PRINTS','Groupwise_List  ','Groupwise_List '),(82,'PRINTS','Outstading_Report ','Outstading_Report '),(83,'PRINTS','Outstading_Report_GroupWise','Outstading_Report_GroupWise'),(84,'PRINTS','Outstading_Report_BranchWise','Outstading_Report_BranchWise'),(85,'PRINTS','Incentive_Report ','Incentive_Report '),(86,'PRINTS','Minutes_Report','Minutes_Report'),(87,'PRINTS','Agreement_Ledger_Report','Agreement_Ledger_Report'),(88,'PRINTS','Master_Code_Report','Master_Code_Report'),(89,'OPTIONS','Section_List','Section_List'),(90,'OPTIONS','Section_List_Add','Section_List_Add'),(91,'OPTIONS','Section_List_Edit','Section_List_Edit'),(92,'OPTIONS','Section_List_Del','Section_List_Del'),(93,'OPTIONS','Application_Pages','Application_Pages'),(94,'OPTIONS','Application_Pages_Add','Application_Pages_Add'),(95,'OPTIONS','Application_Pages_Edit','Application_Pages_Edit'),(96,'OPTIONS','Application_Pages_Del','Application_Pages_Del'),(97,'OPTIONS','Privilege_Type_List','Privilege_Type_List'),(98,'OPTIONS','Privilege_Type_List_Add','Privilege_Type_List_Add'),(99,'OPTIONS','Privilege_Type_List_Edit','Privilege_Type_List_Edit'),(100,'OPTIONS','Privilege_Type_List_Del','Privilege_Type_List_Del'),(101,'OPTIONS','View_Assigned_Privileges','View_Assigned_Privileges'),(102,'OPTIONS','View_Assigned_Privileges_Add','View_Assigned_Privileges_Add'),(103,'OPTIONS','View_Assigned_Privileges_Edit','View_Assigned_Privileges_Edit'),(104,'OPTIONS','View_Assigned_Privileges_Del','View_Assigned_Privileges_Del'),(105,'OPTIONS','Make_copy_of_privilege','Make_copy_of_privilege'),(106,'MASTERS','Master','Master'),(107,'ENROLLMENT','Enrollment','Enrollment'),(108,'ENTRY','Entry','Entry'),(109,'BANK_DEPOSITE','Bank_Deposit_Header','Bank_Deposit_Header'),(110,'PRINTS','Print','Print'),(111,'OPTIONS','Option','Option'),(112,'MASTERS','Cordinator_Code_List','Cordinator_Code_List'),(113,'MASTERS','Cordinator_Code_List_Add','Cordinator_Code_List_Add'),(114,'MASTERS','Cordinator_Code_List_Edit','Cordinator_Code_List_Edit'),(115,'MASTERS','Cordinator_Code_List_del','Cordinator_Code_List_del'),(116,'PRINTS','No_Of_Collection','No_Of_Collection'),(117,'PRINTS','Month_Wise','Month_Wise'),(118,'PRINTS','chit_payment_slip','chit_payment_slip');
/*!40000 ALTER TABLE `tbl_sur_p_app_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sur_p_field_links`
--

DROP TABLE IF EXISTS `tbl_sur_p_field_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sur_p_field_links` (
  `Field_Link_ID` mediumint(9) NOT NULL AUTO_INCREMENT,
  `Table1` text NOT NULL,
  `Table2` text NOT NULL,
  `Field1` text NOT NULL,
  `Field2` text NOT NULL,
  `Field1_Value` text NOT NULL,
  `Field1_New_Value` text NOT NULL,
  `Update_Auto` text NOT NULL,
  `Delete_Auto` text NOT NULL,
  PRIMARY KEY (`Field_Link_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sur_p_field_links`
--

LOCK TABLES `tbl_sur_p_field_links` WRITE;
/*!40000 ALTER TABLE `tbl_sur_p_field_links` DISABLE KEYS */;
INSERT INTO `tbl_sur_p_field_links` VALUES (12,'form13','chitpaymentslip','formID','ChitpaymentSlipID','','','No','No'),(13,'chitpaymentslip','auction','ChitpaymentSlipID','Auction_ID','','','No','No'),(15,'banks','subdirdetails','BankID','BranchCode','','','Yes','Yes');
/*!40000 ALTER TABLE `tbl_sur_p_field_links` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sur_p_privileges`
--

DROP TABLE IF EXISTS `tbl_sur_p_privileges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sur_p_privileges` (
  `Privilege_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Privilege_Type_ID` mediumint(10) NOT NULL DEFAULT '0',
  `Page` varchar(255) DEFAULT NULL,
  `Privileges1` varchar(20) DEFAULT NULL,
  `Filter` text,
  `Tables` text,
  PRIMARY KEY (`Privilege_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=335 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sur_p_privileges`
--

LOCK TABLES `tbl_sur_p_privileges` WRITE;
/*!40000 ALTER TABLE `tbl_sur_p_privileges` DISABLE KEYS */;
INSERT INTO `tbl_sur_p_privileges` VALUES (2,1,'Master_Comp_Pro','YES','',''),(3,1,'Enroll_Grp_Manage','YES','',''),(4,1,'Enroll_Grp_Forming','YES','',''),(5,1,'Master_Director','YES','',''),(6,2,'Enroll_Grp_Enroll','YES','',''),(7,2,'Enroll_Grp_Forming','YES','',''),(8,4,'Company_Profile','YES','                              ',''),(9,4,'Director_ShareHolder_Member','YES','',''),(10,1,'Company_Profile','YES','                              ',''),(11,1,'Director_ShareHolder_Member','YES','',''),(13,4,'Branch_List','YES','',''),(14,4,'Area_List','YES','',''),(15,4,'Collection_Code_List','YES','',''),(16,4,'Bank_List','YES','',''),(17,4,'Master_Code_List ','YES','',''),(18,4,'Introducer_List','YES','',''),(19,4,'Introducer_Type_List ','YES','',''),(20,4,'Group_Management','YES','',''),(21,4,'Group_Forming','YES','',''),(22,4,'Group_Enrollment','YES','',''),(23,4,'Applicant_Substitution','YES','',''),(24,4,'Priza_Entry','YES','',''),(25,4,'Intimation_Entry','YES','',''),(26,4,'Intimation_Print','YES','',''),(27,4,'Recipt_List ','YES','',''),(28,4,'Subscription_Entry ','YES','',''),(29,4,'Bank_Deposite','YES','',''),(30,4,'Bank_Deposite_Print','YES','',''),(31,4,'Agreement_Ledger_Report','YES','',''),(32,4,'Groupwise_List ','YES','',''),(33,4,'Incentive_Report ','YES','',''),(34,4,'Master_Code_Report','YES','',''),(35,4,'Minutes_Report','YES','',''),(36,4,'Outstading_Report ','YES','',''),(37,4,'Outstading_Report_BranchWise','YES','',''),(38,4,'Outstading_Report_GroupWise','YES','',''),(39,4,'View_Assigned_Privileges','YES','',''),(40,4,'Application_Pages','YES','',''),(41,4,'Make_copy_of_privilege','YES','',''),(42,4,'Privilege_Type_List','YES','',''),(43,4,'Section_List','YES','',''),(44,4,'Option','YES','',''),(45,4,'Master','YES','',''),(46,4,'Enrollment','YES','',''),(47,4,'Entry','YES','',''),(48,4,'Bank_Deposit_Header','YES','',''),(49,4,'Print','YES','',''),(50,3,'Master','YES','',''),(51,1,'Master','YES','                              ',''),(52,2,'Master','YES','                              ',''),(53,2,'Enrollment','YES','',''),(54,4,'Company_Profile_Add','YES','',''),(55,4,'Company_Profile_Edit','YES','',''),(56,4,'Company_Profile_Del','YES','',''),(57,4,'Director_ShareHolder_Add','YES','',''),(58,4,'Subsriber_Add ','YES','',''),(59,4,'Director_ShareHolder_Member_Assign_User','YES','',''),(60,4,'Director_ShareHolder_Member_Del','YES','',''),(61,4,'Director_ShareHolder_Member_Edit','YES','',''),(62,4,'Branch_List_Add','YES','                                                            ',''),(63,4,'Branch_List_Del','YES','',''),(64,4,'Branch_List_Edit','YES','                              ',''),(65,4,'Area_List_Add','YES','',''),(66,4,'Area_List_Edit','YES','',''),(67,4,'Area_List_Del','YES','',''),(68,4,'Collection_Code_List_Add','YES','',''),(69,4,'Collection_Code_List_Add_Group_Leader','YES','',''),(70,4,'Collection_Code_List_Del','YES','',''),(71,4,'Collection_Code_List_Edit','YES','',''),(72,4,'Bank_List_Add','YES','',''),(73,4,'Bank_List_Del','YES','',''),(74,4,'Bank_List_Edit','YES','',''),(75,4,'Master_Code_List_Add','YES','',''),(76,4,'Master_Code_List_Del','YES','',''),(77,4,'Master_Code_List_Edit','YES','',''),(78,4,'Introducer_List_Add','YES','',''),(79,4,'Introducer_List_Del','YES','',''),(80,4,'Introducer_List_Edit','YES','',''),(81,4,'Introducer_Type_List_Add','YES','',''),(82,4,'Introducer_Type_List_Del','YES','',''),(83,4,'Introducer_Type_List_Edit','YES','',''),(84,4,'Group_Management_Add','YES','                                                            ',''),(85,4,'Group_Management_Del','YES','',''),(86,4,'Group_Management_Edit','YES','',''),(87,4,'Group_Forming_Add','YES','',''),(88,4,'Group_Forming_Del','YES','',''),(89,4,'Group_Forming_Edit','YES','',''),(90,4,'Group_Enrollment_Add','YES','',''),(91,4,'Group_Enrollment_Del','YES','',''),(92,4,'Group_Enrollment_Edit','YES','',''),(93,4,'Subscription_Entry_Add','YES','                              ',''),(94,4,'Subscription_Entry_Del','YES','',''),(95,4,'Subscription_Entry_Edit','YES','',''),(96,4,'Recipt_List_Add ','YES','',''),(97,4,'Recipt_List_Del','YES','',''),(98,4,'Recipt_List_Edit','YES','',''),(99,4,'Cordinator_Code_List','YES','',''),(100,4,'Bank_Deposite_Add','YES','',''),(101,4,'Cordinator_Code_List_Add','YES','',''),(102,4,'Cordinator_Code_List_del','YES','                              ',''),(103,4,'Cordinator_Code_List_Edit','YES','                                                            ',''),(104,3,'Company_Profile','YES','',''),(105,5,'Entry','YES','',''),(106,5,'Subscription_Entry ','YES','',''),(107,5,'Subscription_Entry_Add','YES','',''),(108,5,'Subscription_Entry_Del','YES','',''),(109,5,'Subscription_Entry_Edit','YES','',''),(110,6,'Entry','YES','',''),(111,6,'Subscription_Entry ','YES','',''),(112,6,'Subscription_Entry_Add','YES','',''),(113,6,'Subscription_Entry_Del','YES','',''),(114,6,'Subscription_Entry_Edit','YES','',''),(117,7,'Entry','YES','',''),(118,7,'Subscription_Entry ','YES','',''),(119,7,'Subscription_Entry_Add','YES','',''),(120,7,'Subscription_Entry_Del','YES','',''),(121,7,'Subscription_Entry_Edit','YES','',''),(124,6,'Recipt_List ','YES','',''),(125,6,'Recipt_List_Add ','YES','',''),(126,6,'Recipt_List_Del','YES','',''),(127,6,'Recipt_List_Edit','YES','',''),(128,7,'Recipt_List ','YES','',''),(129,7,'Recipt_List_Del','YES','',''),(130,7,'Recipt_List_Add ','YES','',''),(131,7,'Recipt_List_Edit','YES','',''),(132,17,'Recipt_List ','YES','',''),(133,17,'Recipt_List_Add ','YES','',''),(134,17,'Recipt_List_Del','YES','',''),(135,17,'Recipt_List_Edit','YES','',''),(136,1,'Intimation_Print','YES','',''),(137,1,'Intimation_Print_Add','YES','',''),(138,1,'Intimation_Print_Del','YES','',''),(139,1,'Intimation_Print_Edit','YES','                              ',''),(140,14,'Intimation_Print','YES','',''),(141,14,'Intimation_Print_Add','YES','',''),(142,14,'Intimation_Print_Del','YES','',''),(143,14,'Intimation_Print_Edit','YES','',''),(144,15,'Intimation_Print','YES','',''),(145,15,'Intimation_Print_Add','YES','',''),(146,15,'Intimation_Print_Del','YES','',''),(147,15,'Intimation_Print_Edit','YES','',''),(151,16,'Intimation_Print','YES','',''),(152,16,'Intimation_Print_Add','YES','',''),(153,16,'Intimation_Print_Del','YES','',''),(154,16,'Intimation_Print_Edit','YES','',''),(158,9,'Intimation_Print','YES','',''),(159,9,'Intimation_Print_Add','YES','',''),(160,9,'Intimation_Print_Del','YES','',''),(161,9,'Intimation_Print_Edit','YES','',''),(165,8,'Intimation_Print','YES','',''),(166,8,'Intimation_Print_Add','YES','',''),(167,8,'Intimation_Print_Del','YES','',''),(168,8,'Intimation_Print_Edit','YES','',''),(172,13,'Intimation_Print','YES','',''),(173,13,'Intimation_Print_Add','YES','',''),(174,13,'Intimation_Print_Del','YES','',''),(175,13,'Intimation_Print_Edit','YES','',''),(179,17,'Intimation_Print','YES','',''),(180,17,'Intimation_Print_Add','YES','',''),(181,17,'Intimation_Print_Del','YES','',''),(182,17,'Intimation_Print_Edit','YES','',''),(187,11,'Intimation_Print_Add','YES','',''),(188,11,'Intimation_Print_Del','YES','',''),(189,11,'Intimation_Print_Edit','YES','',''),(193,10,'Intimation_Print','YES','',''),(194,10,'Intimation_Print_Add','YES','',''),(195,10,'Intimation_Print_Del','YES','',''),(196,10,'Intimation_Print_Edit','YES','',''),(200,2,'Intimation_Print','YES','',''),(201,2,'Intimation_Print_Add','YES','',''),(202,2,'Intimation_Print_Del','YES','',''),(203,2,'Intimation_Print_Edit','YES','',''),(207,5,'Intimation_Print','YES','',''),(208,5,'Intimation_Print_Add','YES','',''),(209,5,'Intimation_Print_Del','YES','',''),(210,5,'Intimation_Print_Edit','YES','',''),(214,6,'Intimation_Print','YES','',''),(215,6,'Intimation_Print_Add','YES','',''),(216,6,'Intimation_Print_Del','YES','',''),(217,6,'Intimation_Print_Edit','YES','',''),(221,7,'Intimation_Print','YES','',''),(222,7,'Intimation_Print_Add','YES','',''),(223,7,'Intimation_Print_Del','YES','',''),(224,7,'Intimation_Print_Edit','YES','',''),(228,3,'Intimation_Print','YES','',''),(229,3,'Intimation_Print_Add','YES','',''),(230,3,'Intimation_Print_Del','YES','',''),(231,3,'Intimation_Print_Edit','YES','',''),(235,12,'Intimation_Print','YES','',''),(236,12,'Intimation_Print_Add','YES','',''),(237,12,'Intimation_Print_Del','YES','',''),(238,12,'Intimation_Print_Edit','YES','',''),(242,18,'Intimation_Print','YES','',''),(243,18,'Intimation_Print_Add','YES','',''),(244,18,'Intimation_Print_Del','YES','',''),(245,18,'Intimation_Print_Edit','YES','',''),(249,1,'Entry','YES','',''),(250,14,'Entry','YES','',''),(251,18,'Entry','YES','',''),(252,15,'Entry','YES','',''),(253,16,'Entry','YES','',''),(254,9,'Entry','YES','',''),(255,8,'Entry','YES','',''),(256,13,'Entry','YES','',''),(257,17,'Entry','YES','',''),(259,10,'Entry','YES','',''),(260,2,'Entry','YES','',''),(262,3,'Entry','YES','',''),(263,12,'Entry','YES','',''),(264,17,'Intimation_Entry','YES','',''),(265,17,'Intimation_Entry_Add','YES','',''),(266,17,'Intimation_Entry_Del','YES','',''),(267,17,'Intimation_Entry_Edit','YES','',''),(268,17,'Priza_Entry','YES','',''),(269,17,'Priza_Entry_Add','YES','',''),(270,19,'Intimation_Print','YES','',''),(271,19,'Intimation_Print_Add','YES','',''),(272,19,'Intimation_Print_Del','YES','',''),(273,19,'Intimation_Print_Edit','YES','',''),(274,19,'Entry','YES','',''),(277,19,'Priza_Entry','YES','',''),(278,19,'Priza_Entry_Del','YES','',''),(279,8,'Enrollment','YES','',''),(280,8,'Group_Management','YES','',''),(281,8,'Group_Management_Add','YES','',''),(282,17,'Enrollment','YES','',''),(283,17,'Group_Management','YES','',''),(284,17,'Group_Management_Add','YES','',''),(285,8,'Group_Management_Edit','YES','',''),(286,17,'Group_Management_Edit','YES','',''),(287,8,'Group_Forming','YES','',''),(288,8,'Group_Forming_Add','YES','',''),(289,8,'Group_Forming_Edit','YES','',''),(290,17,'Group_Forming','YES','',''),(291,17,'Group_Forming_Add','YES','',''),(292,17,'Group_Forming_Edit','YES','',''),(293,8,'Group_Enrollment','YES','',''),(294,8,'Group_Enrollment_Add','YES','',''),(295,8,'Group_Enrollment_Edit','YES','',''),(296,17,'Group_Enrollment','YES','',''),(297,17,'Group_Enrollment_Add','YES','',''),(298,17,'Group_Forming_Edit','YES','',''),(299,8,'Applicant_Substitution','YES','',''),(300,8,'Applicant_Substitution_Add','YES','',''),(301,8,'Applicant_Substitution_Edit','YES','                              ',''),(302,17,'Applicant_Substitution','YES','',''),(303,17,'Applicant_Substitution_Add','YES','',''),(304,17,'Applicant_Substitution_Edit','YES','',''),(305,7,'Bank_Deposite','YES','',''),(306,7,'Bank_Deposite_Add','YES','',''),(307,7,'Bank_Deposite_Del','YES','',''),(308,7,'Bank_Deposite_Edit','YES','',''),(309,7,'Bank_Deposite_Print','YES','',''),(310,7,'Bank_Deposit_Header','YES','',''),(311,17,'Bank_Deposite','YES','',''),(312,17,'Bank_Deposite_Add','YES','',''),(313,17,'Bank_Deposite_Del','YES','',''),(314,17,'Bank_Deposite_Edit','YES','',''),(315,17,'Bank_Deposite_Print','YES','',''),(316,17,'Bank_Deposit_Header','YES','',''),(317,4,'No_Of_Collection','YES','',''),(318,4,'Month_Wise','YES','                                                                                                                        ',''),(319,4,'chit_payment_slip','YES','',''),(320,11,'Bank_Deposite_Add','YES','',''),(321,11,'Applicant_Substitution','YES','',''),(323,29,'Intimation_Print_Add','YES','',''),(324,29,'Intimation_Print_Del','YES','',''),(325,29,'Intimation_Print_Edit','YES','',''),(326,29,'Bank_Deposite_Add','YES','',''),(327,29,'Applicant_Substitution','YES','',''),(330,27,'Intimation_Print_Add','YES','',''),(331,27,'Intimation_Print_Del','YES','',''),(332,27,'Intimation_Print_Edit','YES','',''),(333,27,'Bank_Deposite_Add','YES','',''),(334,27,'Applicant_Substitution','YES','','');
/*!40000 ALTER TABLE `tbl_sur_p_privileges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sur_p_qry_privileges`
--

DROP TABLE IF EXISTS `tbl_sur_p_qry_privileges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sur_p_qry_privileges` (
  `Privilege_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Privilege_Type_ID` mediumint(10) NOT NULL DEFAULT '0',
  `Page` varchar(50) DEFAULT NULL,
  `Privileges1` varchar(20) DEFAULT NULL,
  `Filter` text,
  `Tables` text,
  PRIMARY KEY (`Privilege_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=322 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sur_p_qry_privileges`
--

LOCK TABLES `tbl_sur_p_qry_privileges` WRITE;
/*!40000 ALTER TABLE `tbl_sur_p_qry_privileges` DISABLE KEYS */;
INSERT INTO `tbl_sur_p_qry_privileges` VALUES (187,27,'Intimation_Print_Add','YES','',''),(188,27,'Intimation_Print_Del','YES','',''),(189,27,'Intimation_Print_Edit','YES','',''),(320,27,'Bank_Deposite_Add','YES','',''),(321,27,'Applicant_Substitution','YES','','');
/*!40000 ALTER TABLE `tbl_sur_p_qry_privileges` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sur_p_sections`
--

DROP TABLE IF EXISTS `tbl_sur_p_sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sur_p_sections` (
  `Section_Code` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `Section_Name` varchar(15) DEFAULT NULL,
  `Gr_Section_Name` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`Section_Code`),
  UNIQUE KEY `Devision_Name` (`Section_Name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sur_p_sections`
--

LOCK TABLES `tbl_sur_p_sections` WRITE;
/*!40000 ALTER TABLE `tbl_sur_p_sections` DISABLE KEYS */;
INSERT INTO `tbl_sur_p_sections` VALUES (3,'MASTERS','MASTERS'),(4,'ENROLLMENT','ENROLLMENT'),(6,'BANK_DEPOSITE','BANK_DEPOSITE'),(7,'PRINTS','PRINTS'),(8,'OPTIONS','OPTIONS');
/*!40000 ALTER TABLE `tbl_sur_p_sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sur_privilege_types`
--

DROP TABLE IF EXISTS `tbl_sur_privilege_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sur_privilege_types` (
  `privilege_type_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `privilege_type_name` varchar(50) DEFAULT NULL,
  `gr_privilege_type_name` varchar(50) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `def_flag` int(2) NOT NULL,
  `lang` varchar(100) NOT NULL,
  `main_privilege_type_id` int(11) NOT NULL,
  PRIMARY KEY (`privilege_type_id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sur_privilege_types`
--

LOCK TABLES `tbl_sur_privilege_types` WRITE;
/*!40000 ALTER TABLE `tbl_sur_privilege_types` DISABLE KEYS */;
INSERT INTO `tbl_sur_privilege_types` VALUES (11,'admin','System Admin','System Admin',0,'english',11),(23,'ip','IP User','IP User',0,'english',23),(27,'Golden','Golden','Paid membership',0,'english',27),(29,'Basic','User','Basic users.',1,'english',29);
/*!40000 ALTER TABLE `tbl_sur_privilege_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sur_sign_up`
--

DROP TABLE IF EXISTS `tbl_sur_sign_up`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sur_sign_up` (
  `personnel_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_code` varchar(10) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `privilege_type` varchar(200) NOT NULL,
  `login` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `mr_mrs` varchar(100) NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `email` varchar(250) NOT NULL,
  `address` text,
  `zip_code` varchar(200) DEFAULT '0',
  `city` text,
  `country` varchar(200) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `mobile_phone_no` varchar(25) DEFAULT NULL,
  `personnel_logo` text,
  `join_date` date DEFAULT '0000-00-00',
  `approved` int(2) DEFAULT NULL,
  `Online_Offline` int(11) DEFAULT '0',
  `ip_address` text NOT NULL,
  `join_time` time NOT NULL DEFAULT '00:00:00',
  `blk_email` int(3) NOT NULL,
  `blk_usr` int(3) NOT NULL,
  `def_lang` varchar(100) NOT NULL,
  `birth_date` date DEFAULT NULL,
  `sms_flag` int(11) NOT NULL,
  `ref_user_code` int(11) NOT NULL,
  `ref_user_type` varchar(200) NOT NULL,
  PRIMARY KEY (`personnel_id`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sur_sign_up`
--

LOCK TABLES `tbl_sur_sign_up` WRITE;
/*!40000 ALTER TABLE `tbl_sur_sign_up` DISABLE KEYS */;
INSERT INTO `tbl_sur_sign_up` VALUES (1,'1','1','11','admin','21232f297a57a5a743894a0e4a801fc3','','admin','admin','nick@gmail.com','','','','af','','121212212','','0000-00-00',1,1,'','00:00:00',0,0,'',NULL,0,0,''),(2,'2','2','23','ip','957b527bcfbad2e80f58d20683931435','','ip','ip','vip@netforce.gr',NULL,'0',NULL,NULL,NULL,'111111','2gan.jpg','0000-00-00',1,1,'','00:00:00',0,0,'',NULL,0,0,''),(39,'','hrbtv4t5mcklk8fg3bd0nmj630','29','916.K.C','23ce1851341ec1fa9e0c259de10bf87c','','Y.K. CH.','','','','','','','','2133333333','','2010-05-13',1,0,'192.168.1.102','19:02:55',0,0,'','0000-00-00',1,16,'subscriber'),(7,'U100007','ukmbcm9952fv6p312tmo95buj4','27','ravi','63dd3e154ca6d948fc380fa576343ba6','','ravi','hajare','kumarwaghmode@gmail.com','','','','','','211121','NULL','2010-04-09',1,1,'127.0.0.1','13:44:14',0,0,'english','1983-04-09',0,4,'subscriber'),(8,'U100008','rs36ekcscacnnhf3v8gq6qdlq5','27','niteen','b3f1ae6d05561b3de95b6971874f729c','','yashvant','Kumar','niteen241@yahoo.co.in','','','','','','9960758220','NULL','2010-04-09',1,1,'192.168.1.13','20:55:00',0,0,'english','2010-04-22',1,0,''),(9,'U100009','949f6ee6140321ca80aa8e5325b61797','27','kk2','0eb9f54b97eee576a0ec83b41d5f6228','','kk2','ww2','kumarwaghmode@gmail.com','','','','','','123467','NULL','2010-04-13',1,1,'121.246.215.55','18:27:36',0,0,'english','2010-04-08',0,0,''),(10,'U100010','2cdfd3dbfa85747b6cd30fc597621543','27','navin007','53ec45d86f6f5dedd54fd796fa89bef7','','navin','kumar','navinkdas@gmail.com','','','','','','9890098900','NULL','2010-04-14',1,1,'121.246.215.55','17:04:34',0,0,'english','1982-08-02',0,6,'subscriber'),(11,'U100011','a3a2769c51f045dd6c1081c6dbfceaa7','27','niteen1','b3f1ae6d05561b3de95b6971874f729c','','niteen','borate','niteen241@gmail.com','','','','','','9960655511','NULL','2010-04-15',1,1,'115.242.18.109','11:31:16',0,0,'english','2010-04-04',0,7,'subscriber'),(37,'','3cprdgs3h8oqacm33ms3id28t2','29','652i t','30ef30b64204a3088a26bc2e6ecf7602','','ni t  e   e e e','','','','','','','','9960655511','','2010-05-13',1,0,'127.0.0.1','18:17:42',0,0,'','0000-00-00',1,14,'subscriber'),(13,'U100013','800f01ff6027463e2263f60b86de79fb','27','admin1','e00cf25ad42683b3df678c61f42c6bda','','admfrist','admlast','kumarwagh2010@gmail.com','','','','','','2134567891','NULL','2010-04-19',1,1,'121.246.215.55','16:07:57',0,0,'english','1979-04-12',0,0,''),(38,'','3cprdgs3h8oqacm33ms3id28t2','29','609it','a48e977b2d9450e3be04dc92a8f92485','','n i  t  e   e e e','','','','','','','','9960655511','','2010-05-13',1,0,'127.0.0.1','18:20:15',0,0,'','0000-00-00',1,15,'subscriber'),(15,'','4fnhkr03eihf3bq79g924u7q76','29','raju','2f6d8f924f260623d24332b0c6288e67','','niteen','norate','niteen241@gmail.com','','','','','','0121029','16girl3.jpg','2010-04-26',0,0,'127.0.0.1','22:55:49',0,0,'english','2010-04-15',0,0,''),(16,'','cq52rl8rnfmokjfubdtta102q3','29','test','098f6bcd4621d373cade4e832627b4f6','','1','1','niteen241@gmail.com','','','','','','121991919','','2010-04-27',0,0,'127.0.0.1','19:47:34',0,0,'','0000-00-00',0,0,''),(17,'','9an5msge3gis4cf17eh91gub77','29','jn','17cedeccc3a6555b9a5826e4d726eae3','','jj','j','niteen241@gmail.com','','','','','','111','','2010-04-28',0,0,'127.0.0.1','12:47:11',0,0,'','0000-00-00',0,0,''),(18,'','9an5msge3gis4cf17eh91gub77','29','kb','ba34ea40525a4379add785228e37fe86','','kk','k','niteen241@gmail.com','','','','','','12112','','2010-04-28',0,0,'127.0.0.1','12:56:18',0,0,'','0000-00-00',0,0,''),(19,'','9an5msge3gis4cf17eh91gub77','29','niteen5','a4766104ad327ca89a68edc50d417a82','','niteen','borate','niteen241@gmail.com','','','','','','991919','','2010-04-28',0,0,'127.0.0.1','16:23:06',0,0,'','0000-00-00',1,0,''),(20,'','ifuo7msvrpno6fs5ccs0ld3oe5','29','कल','cc64c4ed18d695132c045f46efc888f6','','कल','कल','niteen241@gmail.com','','','','','','212111','','2010-05-01',1,1,'127.0.0.1','00:10:14',0,0,'','0000-00-00',1,3,'subscriber'),(32,'','honamjca1c2oscus8lrmdtqp05','29','en borate31','0537fb40a68c18da59a35c2bfe1ca554','','niteen borate','','','','','','','','9960758220','','2010-05-12',1,0,'127.0.0.1','22:56:51',0,0,'','0000-00-00',1,0,'subscriber'),(33,'','honamjca1c2oscus8lrmdtqp05','29','en borate32','598b3e71ec378bd83e0a727608b5db01','','niteen borate','','','','','','','','9960758220','','2010-05-12',1,0,'127.0.0.1','23:01:49',0,0,'','0000-00-00',1,0,'subscriber'),(34,'','honamjca1c2oscus8lrmdtqp05','29','en borate133','dc468c70fb574ebd07287b38d0d0676d','','niteen borate1','','','','','','','','1212112121','','2010-05-12',1,0,'127.0.0.1','23:10:42',0,0,'','0000-00-00',1,11,'subscriber'),(35,'','3cprdgs3h8oqacm33ms3id28t2','29','390jendr','dc468c70fb574ebd07287b38d0d0676d','','rjendra hajare','','','','','','','','9960758220','','2010-05-13',1,0,'127.0.0.1','16:52:48',0,0,'','0000-00-00',1,12,'subscriber'),(36,'','3cprdgs3h8oqacm33ms3id28t2','29','578achin','dc468c70fb574ebd07287b38d0d0676d','','sachin borate','','','','','','','','9960855511','','2010-05-13',1,0,'127.0.0.1','17:10:19',0,0,'','0000-00-00',1,13,'subscriber'),(28,'','6h5trg834psa60svoe9k73q960','29','kk','dc468c70fb574ebd07287b38d0d0676d','','kk','kk','niteen241@gmail.com','','','','','','121121','','2010-05-04',1,0,'127.0.0.1','17:55:00',0,0,'','0000-00-00',1,0,'subscriber'),(29,'','6h5trg834psa60svoe9k73q960','29','jj','bf2bc2545a4a5f5683d9ef3ed0d977e0','','jj','jj','niteen241@gmail.com','','','','','','12121','','2010-05-04',1,1,'127.0.0.1','18:02:50',0,0,'','0000-00-00',1,0,'subscriber'),(30,'','6h5trg834psa60svoe9k73q960','29','user','ee11cbb19052e40b07aac0ca060c23ee','','user','user','niteen241@gmail.com','','','','','','000','','2010-05-04',1,0,'127.0.0.1','18:23:14',0,0,'','0000-00-00',1,5,'subscriber'),(31,'','6h5trg834psa60svoe9k73q960','29','ll','5b54c0a045f179bcbbbc9abcb8b5cd4c','','Yashvant Kumar','ss','niteen241@gmail.com','','','','','','aaaaaaasa','','2010-05-04',1,1,'127.0.0.1','18:24:52',0,0,'','0000-00-00',1,2,'subscriber');
/*!40000 ALTER TABLE `tbl_sur_sign_up` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tempappdet`
--

DROP TABLE IF EXISTS `tempappdet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tempappdet` (
  `Ticketno` int(11) DEFAULT NULL,
  `GroupNo` varchar(50) DEFAULT NULL,
  `SubCodeNo` varchar(50) DEFAULT NULL,
  `wit1name` varchar(50) DEFAULT NULL,
  `wit1addrs` varchar(255) DEFAULT NULL,
  `wit2name` varchar(50) DEFAULT NULL,
  `wit2addrs` varchar(255) DEFAULT NULL,
  `prizeamount` int(11) DEFAULT NULL,
  `prizechequeno` varchar(50) DEFAULT NULL,
  `prizebankname` varchar(50) DEFAULT NULL,
  `prizechequedate` datetime DEFAULT NULL,
  `prizemonth` varchar(50) DEFAULT NULL,
  `auctiondate` datetime DEFAULT NULL,
  `amtinword` varchar(50) DEFAULT NULL,
  `prizeamtinword` varchar(50) DEFAULT NULL,
  `installmentinword` varchar(50) DEFAULT NULL,
  `shuretyname1` varchar(50) DEFAULT NULL,
  `shuretyadd1` varchar(255) DEFAULT NULL,
  `shuretytelno1` varchar(50) DEFAULT NULL,
  `shuretypanno1` varchar(50) DEFAULT NULL,
  `shuretymobno1` varchar(50) DEFAULT NULL,
  `shuretycode1` varchar(50) DEFAULT NULL,
  `shuretyname2` varchar(50) DEFAULT NULL,
  `shuretyadd2` varchar(255) DEFAULT NULL,
  `shuretytelno2` varchar(50) DEFAULT NULL,
  `shuretypanno2` varchar(50) DEFAULT NULL,
  `shuretymobno2` varchar(50) DEFAULT NULL,
  `shuretycode2` varchar(50) DEFAULT NULL,
  `installment` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `prizeinstno` int(11) DEFAULT NULL,
  KEY `ApplicantDetailsGroupNo` (`GroupNo`),
  KEY `ApplicantDetailsTicketNo` (`Ticketno`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tempappdet`
--

LOCK TABLES `tempappdet` WRITE;
/*!40000 ALTER TABLE `tempappdet` DISABLE KEYS */;
/*!40000 ALTER TABLE `tempappdet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tmp_tbl_dep_slip_details`
--

DROP TABLE IF EXISTS `tmp_tbl_dep_slip_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tmp_tbl_dep_slip_details` (
  `tmp_dep_slip_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `branch_code` varchar(5) DEFAULT NULL,
  `dep_slip_no` bigint(20) DEFAULT '0',
  `depslip_Date` date DEFAULT NULL,
  `BankName` varchar(30) DEFAULT NULL,
  `AccNo` varchar(30) DEFAULT NULL,
  `Cash_Cheque` varchar(6) DEFAULT NULL,
  `ses_dep_slip_no` varchar(50) DEFAULT NULL,
  `trans_detail_id` varchar(50) DEFAULT '0',
  PRIMARY KEY (`tmp_dep_slip_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tmp_tbl_dep_slip_details`
--

LOCK TABLES `tmp_tbl_dep_slip_details` WRITE;
/*!40000 ALTER TABLE `tmp_tbl_dep_slip_details` DISABLE KEYS */;
INSERT INTO `tmp_tbl_dep_slip_details` VALUES (9,'ajm',0,'2010-05-15','','','cheque','3','8');
/*!40000 ALTER TABLE `tmp_tbl_dep_slip_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `Transaction_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ReceiptNo` varchar(20) NOT NULL,
  `Reciept_Date` date NOT NULL,
  `TRNO` varchar(100) NOT NULL,
  `TRDate` date NOT NULL,
  `Amount` int(11) NOT NULL,
  `Remark` text NOT NULL,
  `ChequeNo` varchar(100) NOT NULL,
  `Bankname` varchar(100) NOT NULL,
  `ChequeDate` date DEFAULT NULL,
  `printreciept` tinyint(4) NOT NULL,
  `Area` varchar(20) NOT NULL,
  `CheRetTime` varchar(20) NOT NULL,
  `Cheretfee` int(11) NOT NULL DEFAULT '0',
  `DayBookNo` int(11) DEFAULT NULL,
  `main_branch_id` int(11) DEFAULT NULL,
  `receipt_type` varchar(255) DEFAULT NULL,
  `payment_by` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`Transaction_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` VALUES (4,'2010/1','2010-05-20','1211','2010-05-20',2222,'','','','0000-00-00',0,'2','',1211,0,1,'','cash');
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_details`
--

DROP TABLE IF EXISTS `transaction_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_details` (
  `Transaction_detail_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Amount` int(11) NOT NULL,
  `Remark` text NOT NULL,
  `ChequeNo` varchar(100) NOT NULL,
  `Bankname` varchar(100) NOT NULL,
  `ChequeDate` date DEFAULT NULL,
  `CheRetTime` varchar(20) DEFAULT '0',
  `Cheretfee` int(11) NOT NULL DEFAULT '0',
  `receipt_type` varchar(255) DEFAULT NULL,
  `payment_by` varchar(250) DEFAULT NULL,
  `trans_id` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `dep_slip_ID` varchar(20) DEFAULT '0',
  PRIMARY KEY (`Transaction_detail_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_details`
--

LOCK TABLES `transaction_details` WRITE;
/*!40000 ALTER TABLE `transaction_details` DISABLE KEYS */;
INSERT INTO `transaction_details` VALUES (8,1500,'1500 by cash','123123123','RUPEE CO-OP BANK','2010-05-28','0',0,NULL,'cheque','4',NULL,'0');
/*!40000 ALTER TABLE `transaction_details` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2010-05-16  0:22:02
