<?php
/// -----------
//define( 'VERSION', '1.1.7' );

function check_dateDiff($dformat, $endDate, $beginDate){
	$date_parts1=explode($dformat, $beginDate);
	$date_parts2=explode($dformat, $endDate);
	$start_date=gregoriantojd($date_parts1[0], $date_parts1[1], $date_parts1[2]);
	$end_date=gregoriantojd($date_parts2[0], $date_parts2[1], $date_parts2[2]);
	return ($end_date - $start_date);
}

if (check_dateDiff("/", "12/30/2021",date("m/d/Y"))<0){
	echo "License expired Contact www.echitfundsoftware.com for more details";	exit;
}
 
  
//-----------
//DB Settings
//-----------

global $tmplocal;
$tmplocal =1;
if ($tmplocal ==1){
	define( 'DB_USER', 'root' );
	define( 'DB_NAME', 'echit_legal' );	
    define( 'DB_HOST', 'localhost' );          
	define( 'DB_PASS', '' );
	define( 'DB_TYPE', 'mysql' ); 
	define( 'WEBSITE_PATH', "http://localhost/chitfund/" );
}else{
	define( 'DB_USER', 'echitfun_accoci' );
	define( 'DB_NAME', 'echitfun_legal' );
	define( 'DB_HOST', 'localhost' );          
	define( 'DB_PASS', 'rs@2016' );
	define( 'DB_TYPE', 'mysql' ); 
	define( 'WEBSITE_PATH', "http://localhost" );
}


// chanage tmplocal to 1 if u want to do server side testing on local

define( 'TMP_LOCAL', $tmplocal );

// Set Operating System
define( 'OS', "win" );
//define( 'OS', "linux" );


//Set SMS API Type
//define( 'SMS_API_TYPE', "nagpurtrade" );
//define( 'SMS_API_TYPE', "smsgupshup" );
//define( 'SMS_API_TYPE', "rapidsms" );
//define( 'SMS_API_TYPE', "msandesh" );
//define( 'SMS_API_TYPE', "bulksms-service.com" );
//define( 'SMS_API_TYPE', "dndopen" );
//define( 'SMS_API_TYPE', "Technopenta" );
//define( 'SMS_API_TYPE', "smscountry" );
//define( 'SMS_API_TYPE', "whitelist" );
//define( 'SMS_API_TYPE', "sinfini" );
//define( 'SMS_API_TYPE', "ihante" );
//define( 'SMS_API_TYPE', "rapidsms_pro" );
//define( 'SMS_API_TYPE', "smshorizon" );
//define( 'SMS_API_TYPE', "mobicomm_dove" );
//define( 'SMS_API_TYPE', "smsjust" );
//define( 'SMS_API_TYPE', "rmtech" );



//Set Collection machine api
define( 'COL_MACHINE', "balaji" );
//define( 'COL_MACHINE', "balaji_16" );
//define( 'COL_MACHINE', "balaji_16_sai" );
//define( 'COL_MACHINE', "softland" );
//define( 'COL_MACHINE_SHOW_PAID', "1" );
//define( 'COL_MACHINE_ONLY_TKT_NO', "1" );
//define( 'COL_MACHINE_SHOW_BRANCH', "1" );
define( 'COL_MACHINE_SHOW_COLL_CODE', "1" );
define( 'COL_MACHINE_SHOW_TERMINATED_GRP', "1" );
define( 'COL_MACHINE_SEND_SMS', "NO" );


// For Print flag for Preprint format
define( 'PRE_PRINT_RECEIPT', 	"0" );
define( 'PRE_PRINT_INTIMATION', "0" );
define( 'PRE_PRINT_AGREEMENT',	"0" );
//define( 'PRE_PRINT_RECEIPT_FORMAT', "jupitar" );
// update report_header from mn_lhf_report_header for Receipt Header/Report Header
// update header for Company name on top after login
// update news_alert from  mn_lhf_report_header for Display news /alert in morque in header section.


// Set IP Login
//define( 'IP_LOGIN', "ON" );
define( 'IP_LOGIN', "OFF" );

// Define language
$lang_msg['language'] = array('english','hindi');
$lang_sec_list['language'] = array('hindi');


// -----------
// PATH Settings
// -----------
define( 'FULL_PATH', dirname(__FILE__) . '/' );  //Get ROOT Path
define( 'ROOT_DIR', FULL_PATH );
define( 'SMARTY_DIR', FULL_PATH . 'libs/Smarty/' );
define( 'AUTH_DIR', FULL_PATH . 'libs/Auth/' );
define( 'TEMPLATE_DIR', FULL_PATH . 'templates/' );
define( 'TEMPLATE_C_DIR', FULL_PATH . 'templates_c/' );
define( 'CACHE_DIR', FULL_PATH . 'cache/' );
define( 'CONFIG_DIR', FULL_PATH . 'configs/' );
define( 'PEAR_DIR', FULL_PATH . 'libs/Pear/' );
define( 'LANG_DIR', FULL_PATH . 'language/' );
define( 'MAIL_CLASSES_DIR', FULL_PATH . 'libs/mail/' );
define( 'TEMP_DIR', FULL_PATH.'temp/' );
define( 'DOC', '/doc/' );

// ---------------------
// GLOBAL  Settings
// --------------------
define( 'MAX_SMS_LIMIT_ONETIME', '500' );  //// set maximum sms limit for batch sms
define( 'MAX_SMS_CHAR_LIMIT', '1000' );  //// set maximum char per sms limit

define( 'LONG_DATE_FORMAT', 'F j, Y' );
define( 'SHORT_DATE_FORMAT', 'd/m/Y' );
define( 'DISPLAY_DATE_FORMAT', 'MMM DD, YYYY');
define( 'DATE_TIME_FORMAT', '%b %d, %Y %I:%M:%S %P');
define( 'DATE_FORMAT', '%b %d, %Y');


// ----------------
// DB TABLE Names
// ---------------
@ini_set('include_path',PEAR_DIR.':'.get_include_path());
/* MOD START */
define ('RATING_BLANK','3');
/* MOD END */
define ('BRANCH_NO','1');


?>