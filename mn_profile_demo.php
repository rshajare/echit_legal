<? if ( !defined( 'SMARTY_DIR' ) ){include_once( 'init.php' );} 
header('Content-Type: text/html; charset=utf-8');
$lang = $_SESSION['opt_lang'];
if($lang<>'') $lang = $lang; else $lang = 'english';

$reqmainprofile_type = $_REQUEST['maintype'];
$t->assign ( 'reqmainprofile_type', $reqmainprofile_type );

$reqprofile_type = $_REQUEST['subtype'];
$t->assign ( 'reqprofile_type', $reqprofile_type );

$lang_pri = 'english';
$lang_sec = 'greek';
$t->assign ( 'pri_lang', $lang_pri );
$t->assign ( 'sec_lang', $lang_sec );

if($reqprofile_type <> '')
	$sql = "SELECT * FROM tbl_profile_cus_fields where cus_type = '$reqmainprofile_type' and sub_cus_type = '$reqprofile_type' group by main_cus_id";
else
	$sql = "SELECT * FROM tbl_profile_cus_fields where cus_type = '$reqmainprofile_type' group by main_cus_id";	

$data = array();
$temp = $db->getAll( $sql );
foreach ($temp as $index => $row){
	$main_cus_id = $row['main_cus_id'];
	$controlname = $row['type'];
	$row['title'] = FnGetValue("tbl_profile_cus_fields","title","lang = '$lang_pri' and main_cus_id = '$main_cus_id'");  
	$row['grk_title'] = FnGetValue("tbl_profile_cus_fields","title","lang = '$lang_sec' and main_cus_id = '$main_cus_id'");  
	$row['grk_order_value'] = FnGetValue("tbl_profile_cus_fields","order_no","lang = '$lang_sec' and main_cus_id = '$main_cus_id'");  

	if($row['type']=='text'){
		$row['engclos']   = FnGetValue("tbl_profile_cus_fields_value","cols","main_cus_id = '$main_cus_id' and lang = '$lang_pri'"); 	
		$row['grkclos']   = FnGetValue("tbl_profile_cus_fields_value","cols","main_cus_id = '$main_cus_id' and lang = '$lang_sec'"); 	
	}

	if($row['type']=='textarea'){
		$row['engrows']   = FnGetValue("tbl_profile_cus_fields_value","rows","main_cus_id = '$main_cus_id' and lang = '$lang_pri'"); 	
		$row['engclos']   = FnGetValue("tbl_profile_cus_fields_value","cols","main_cus_id = '$main_cus_id' and lang = '$lang_pri'"); 	

		$row['grkrows']   = FnGetValue("tbl_profile_cus_fields_value","rows","main_cus_id = '$main_cus_id' and lang = '$lang_sec'"); 	
		$row['grkclos']   = FnGetValue("tbl_profile_cus_fields_value","cols","main_cus_id = '$main_cus_id' and lang = '$lang_sec'"); 	
	}

	if($row['type']=='combobox'){
		$engsql_combobox = "SELECT * FROM tbl_profile_cus_fields_value where main_cus_id = '$main_cus_id' and lang = '$lang_pri' ";
		$engtemp_combobox[$controlname] = $db->getall($engsql_combobox);

		$grksql_combobox = "SELECT * FROM tbl_profile_cus_fields_value where main_cus_id = '$main_cus_id' and lang = '$lang_sec' ";
		$grktemp_combobox[$controlname] = $db->getall($grksql_combobox);
	}

	if($row['type']=='checkbox'){
		$engsql_checkbox = "SELECT * FROM tbl_profile_cus_fields_value where main_cus_id = '$main_cus_id' and lang = '$lang_pri' ";
		$engtemp_checkbox[$controlname] = $db->getall($engsql_checkbox);

		$grksql_checkbox = "SELECT * FROM tbl_profile_cus_fields_value where main_cus_id = '$main_cus_id' and lang = '$lang_sec' ";
		$grktemp_checkbox[$controlname] = $db->getall($grksql_checkbox);
	}

	if($row['type']=='radio'){
		$engsql_radio = "SELECT * FROM tbl_profile_cus_fields_value where main_cus_id = '$main_cus_id' and lang = '$lang_pri' ";
		$engtemp_radio[$controlname] = $db->getall($engsql_radio);

		$grksql_radio = "SELECT * FROM tbl_profile_cus_fields_value where main_cus_id = '$main_cus_id' and lang = '$lang_sec' ";
		$grktemp_radio[$controlname] = $db->getall($grksql_radio);
	}

	$data[]  = $row;
}
$t->assign ( 'data', $data );
$t->assign ( 'comboboxcontrol', $engtemp_combobox );
$t->assign ( 'chechboxcontrol', $engtemp_checkbox );
$t->assign ( 'grkcomboboxcontrol', $grktemp_combobox );
$t->assign ( 'grkchechboxcontrol', $grktemp_checkbox );
$t->assign ( 'engtemp_radio', $engtemp_radio );
$t->assign ( 'grktemp_radio', $grktemp_radio );
$t->display('mn_profile_demo.htm');
exit;
?>