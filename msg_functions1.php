<?php if ( !defined( 'SMARTY_DIR' ) ){include_once( 'init.php' );} 

// +----------------------------------------------------------------------------------------------------------------------------+
// |  File Name : msg_functions.php
// |  Version : 2.0 
// |  Create-date : 29, March, 2010
// |  Modify-date : 16, June, 2010
// |  Authors : Niteen Borate                        
// | 
// |  Desciption : functions for internal message comminication
// |  How to use : 
// |
// |  function heire_msg_cat($main_id,$selected,$lang)
// |
// |  function fnSend_Email_Pear($to,$from,$subject,$msg,$attch_id)
// |  
// |  function fnSend_Email_Pear_pass($to,$from,$subject,$msg,$attch_id)
// |  
// |  Ajex Functions for new letter
// |  op= 26 // enter fck editor for selected template
// |  op= 28  // Attachment 
// +------------------------------------------------------------------------------------------------------------------------------
//

// function to show newsletter Category
function heire_msg_cat($main_id,$selected,$lang){
	if($main_id == 0){
		$reqmaincat = "Select * from tbl_msg_category where hierno = 0 and lang = '$lang'";
	}else{
		$reqmaincat = "Select * from tbl_msg_category where main_id = $main_id and lang = '$lang'";
	}
	//echo $reqmaincat;
	$main_cat_temp = mysql_query( $reqmaincat ) or die(mysql_error().$reqmaincat);
	$space = '';
	while($row = mysql_fetch_array($main_cat_temp)){
		$space = '';
		$cat_id = $row['main_cat_id'];
		$req_present = fngetcount("tbl_msg_category","main_id = $cat_id");
		$level = $row['hierno'];
		for($i=1;$i<=$level;$i++){
			$space = $space."&nbsp;&nbsp;";
		}
		if($req_present > 0){
			//$req_option_string .= "<optgroup label='$space".$row['name']."'></optgroup>";
			if($selected == $cat_id) $sel = "selected='selected'"; else $sel = '';
			$req_option_string .= "<option value='".$cat_id."' ".$sel.">"."<strong>".$row['name']."</strong>"."</option>";
			$req_option_string .=  heire_news_cat($cat_id,$selected,$lang);
		}else{
			if($selected == $cat_id) $sel = "selected='selected'"; else $sel = '';
			$req_option_string .= "<option value='".$cat_id."' ".$sel.">$space".$row['name']."</option>";			
		}
	}			
	return $req_option_string;
}




//===========News Letter Functions===================
function fnSend_Email_Pear($to,$from,$subject,$msg,$attch_id){

		//if ($from =='')	
		$from    = FnGetValue("tbl_mn_app_setting","field_value","field_name='admin_email_id'");
		$reqtoname = FnGetValue("tbl_sur_sign_up","concat(first_name,' ',last_name)","email = '$to'"); if($reqtoname == '0') $reqtoname = '$to';
		$prefered_language = FnGetValue("tbl_sur_sign_up","def_lang","email = '$reqto'"); if ($prefered_language =='0') $prefered_language = $_SESSION['opt_lang'];
		
		
		if(($to <> '') && ($from <> '')){
			if(TMP_LOCAL == 1 ){ 	
				echo "<HR>".$to; echo $from;	echo $subject;	echo $msg;
			}else{		
				
			$mime_boundary = "--------".md5(time()); 
			$headers = "From:  ".$from."\n";
			$headers .= "Reply-To:  ".$from."\n";
			$headers .= "MIME-Version: 1.0\n";
			$headers .= "Content-Type: multipart/alternative; boundary=\"$mime_boundary\"\n";
			
			# -=-=-=- TEXT EMAIL PART
			$message = "--$mime_boundary\n";
			$message .= "Content-Type: text/plain; charset=UTF-8\n";
			$message .= "Content-Transfer-Encoding: 8bit\n\n";
			
			# -=-=-=- HTML EMAIL PART
			$message .= "--$mime_boundary\n";
			$message .= "Content-Type: text/html; charset=UTF-8\n";
			$message .= "Content-Transfer-Encoding: 8bit\n\n";
			$message .= $msg;
			$message .= "<html>\n";
			$message .= "<body style=\"font-family:Verdana, Verdana, Geneva, sans-serif; font-size:14px; color:#666666;\">\n";
			
			$message .= "</body>\n";
			$message .= "</html>\n";
			
			# -=-=-=- FINAL BOUNDARY
			
			$message .= "--$mime_boundary--\n\n";
			
			# -=-=-=- SEND MAIL
			
			$mail_sent = @mail( $to, $subject, $message, $headers );
			
	    }// end of local and online
	} // end if validation for from and to
} // end of function			
//== end of sending mail=======================




//===========News Letter Functions===================
function fnSend_Email_Pear_pass($to,$from,$subject,$msg,$attch_id){
		//if ($from =='')	
		$from    = FnGetValue("tbl_mn_app_setting","field_value","field_name='admin_email_id'");
		$reqtoname = FnGetValue("tbl_sur_sign_up","concat(first_name,' ',last_name)","email = '$to'"); if($reqtoname == '0') $reqtoname = '$to';
		$prefered_language = FnGetValue("tbl_sur_sign_up","def_lang","email = '$reqto'"); if ($prefered_language =='0') $prefered_language = $_SESSION['opt_lang'];
		
		if(($to <> '') && ($from <> '')){
			if($flag == '1' ){ 	echo "<HR>".$reqto;	echo $from;	echo $subject;	echo $msg;
			}else{				
			// get smtp details
			$smtp_sql = "select * from tbl_msg_smtp_lkp where default_flag = 1 ";$smtp_result = mysql_query($smtp_sql) or die(mysql_error().$smtp_sql);
			while($smtp_row = mysql_fetch_array($smtp_result)){ $host 	= trim($smtp_row["host_name"]); $username = trim($smtp_row["username"]); $password 	= trim($smtp_row["password"]);			}
			
			require_once PEAR_DIR . 'Mail.php';	require_once PEAR_DIR . 'Mail/mime.php';
			$headers = array ('Content-Type'=>'text/html; charset=utf-8','From' => $from,  'To' => $to, 'Subject' => $subject); 
			define("CRLF","\r\n");	$crlf = CRLF;	$mime = new Mail_Mime($crlf);$mime->setHtmlBody($msg);
				 
			$toatal_attachment = fnGetCount("tbl_msg_attachment","news_letter_details_id='".$attch_id."'");
			if($toatal_attachment > 0){ 
					$sql="Select * from tbl_msg_attachment where news_letter_details_id = $attch_id"; $result = mysql_query($sql) or die("$sql"); 
					while ($row = mysql_fetch_array($result)){ $filename = "doc/mail/".$row['document']; if(file_exists($filename)) { $mime->addAttachment($filename);  }	}	
			}
			//echo $msg; 
			$msg = $mime->get(); $headers = $mime->headers($headers);
			//echo $msg;
			if (($username <>'') && ($password <> '')) {	
				$smtp = Mail::factory('smtp',  array ('host' => $host, 'auth' => true, 'username' => $username, 'password' => $password));
			}else{
				 $smtp = Mail::factory('smtp');
			 }
			 if (TMP_LOCAL == 1 ){ 
				echo $to; echo $from; echo $subject; 	echo $msg;
			}else{
				//echo "<HR>"."To = ".$to;	echo "  from=".$from;	echo "subject = ".$subject;	echo "headers".$headers; echo $msg;
				$mail = $smtp->send($to, $headers, $msg); if (PEAR::isError($mail)) {  echo("<p>" . $mail->getMessage() . "</p>");  } 
			}				
				
	    }// end of local and online
	} // end if validation for from and to
} // end of function			
//== end of sending mail=======================








?>