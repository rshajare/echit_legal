<?php
include_once ("init.php");
include_once( 'sur_adm_permission.php' );
include_once( 'controllers/lgl_report_draft_lk/classreport_draft_lk.php' );

$report_draft_lk = new report_draft_lk();
$StatusMessage = "";
switch($_GET['action']){
	case '1' :
            if(isset($_POST['txtreport_draft_id']) && $_POST['txtreport_draft_id'] <> "" ){ 
                $report_draft_lk->update();
            }else{
                $report_draft_lk->store();
            }
            header('Location: lgl_report_draft_lk.php');
            break;

	case '4':
	case 'destroy':	
            $report_draft_lk->destroy($_REQUEST['report_draft_id']);
            $_SESSION['AlertMessage'] = "Record Deleted Sucessfully";
            header('Location: lgl_report_draft_lk.php');
            break;

	default:
            $report_draft_lk->index();
            break;
}

//==========================#  End of Getting records in list page===================================================================
?>