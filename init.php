<?php 
if(session_id() == '') {
	session_start();
}
$sclient=isset($_SESSION['client']) ? $_SESSION['client'] : '';
if ( $sclient <> "")
	require_once( dirname(__FILE__). "/". $_SESSION["client"] . '/config.php' );
else
	require_once( dirname(__FILE__).'/config.php' );

// Set Operating System
//define( 'OS', "win" );
//define( 'OS', "linux" );
ini_set('date.timezone', 'Asia/Calcutta'); 
error_reporting(~E_DEPRECATED & ~E_NOTICE & ~E_WARNING & ~E_STRICT);
//error_reporting(0);

// ----------------
// DB TABLE Names
// ---------------
//@ini_set('include_path',PEAR_DIR.':'.get_include_path());
/* MOD START */
//define ('RATING_BLANK','3');
/* MOD END */
if (version_compare(PHP_VERSION, '7.0.0') >= 0) {
   require_once( dirname(__FILE__).'/topdo.php' );

}

require_once SMARTY_DIR . 'Smarty.class.php';
require_once PEAR_DIR . 'Mail.php';

if (version_compare(PHP_VERSION, '7.0.0') <= 0) {
   require_once PEAR_DIR . 'DB.php';
}

/************************/
// SECURITY CHECK TO CHECK IF SCRIPT IS INSTALLED OR NOT
/************************/

if ( $_SERVER['HTTP_HOST'] != 'localhost' && ( file_exists( FULL_PATH.'install1.php' ) || is_dir( FULL_PATH.'install_files' ) ) ) {
	echo '
	<br /><br /><br /><center><table border=0 width=500 cellpadding=2 cellspacing=0><tr><td align=center><font color=red face=Arial size=2>
	<B>SECURITY ALERT<br /><br />Please remove the following from your server before continuing: install.php file, and the install_files folder. Then, click "Reload osDate" below to continue.<br /><br />	<a href=index.php>Reload </a></B></font></td></tr>	</table></center>';
	exit;
}

/**********************************/
// PEAR SETUP
/**********************************/
if ( !isset( $db ) ) {
	
	if (version_compare(PHP_VERSION, '7.0.0') <= 0) {
			

		$dsn = DB_TYPE . '://' . DB_USER . ':' . DB_PASS . '@' . DB_HOST .':'.DB_PORT. '/' . DB_NAME;
		$db = @DB::connect( $dsn, 'true' );
		if (DB::isError($db)) {
        	print $db->getMessage();
		}

		$db->setFetchMode( DB_FETCHMODE_ASSOC );
		//mysql_query('SET character_set_results="utf8"'); mysql_query("SET CHARACTER SET utf8"); mysql_query("SET NAMES 'utf8'");mysql_query('SET time_zone = "+5:30"');mysql_query("set sql_mode = ''");
		function errhndl ( $err ) {
			echo '<pre>' . $err->message;
			print_r( $err );
			die();
		}
		PEAR::setErrorHandling( PEAR_ERROR_CALLBACK, 'errhndl' );
	}
}



if ( !get_magic_quotes_gpc() ) {
   function addslashes_deep($value) {
       return is_array($value) ? array_map('addslashes_deep', $value) : addslashes($value);
   }
   $_POST 	= array_map('addslashes_deep', $_POST);
   $_GET 	= array_map('addslashes_deep', $_GET);
   $_COOKIE	= array_map('addslashes_deep', $_COOKIE);
}


$lang = array();
$t = new Smarty;
//echo $t->version;
//$t->testInstall(); 
//echo Smarty::SMARTY_VERSION; 
//$smarty = new Smarty(); 
//echo $smarty->version;


$opt_lang = "english";
$_SESSION['opt_lang'] = $opt_lang;

$t->assign ('lang_msg', $lang_msg );
$t->assign ('lang_sec_list', $lang_sec_list );
	
/**********************************/
// Call other function files which requred data base connectivity 
/**********************************/
require_once( dirname(__FILE__).'/non_updateable_other_functions.php' );
//require_once( dirname(__FILE__).'/news_letter_functions.php'  ) ;
require_once( dirname(__FILE__).'/msg_functions.php'  ) ;
//require_once( dirname(__FILE__).'/mn_menu_page_functions.php' );
require_once( dirname(__FILE__).'/sur_privi_access_functions.php' );
require_once( dirname(__FILE__).'/msg_sms_functions.php' );
//require_once( dirname(__FILE__).'/sur_adm_user_functions.php' );
require_once( dirname(__FILE__).'/updateable_other_functions.php' );
require_once( dirname(__FILE__).'/sur_user_functions.php' );
require_once( dirname(__FILE__).'/config_data.php' );
//require_once( dirname(__FILE__).'/chitfund_init_functions.php' );
//require_once( dirname(__FILE__).'/acc_init_functions.php' );
require_once( dirname(__FILE__).'/mn_paging_init_functions.php' );

//require_once( dirname(__FILE__).'/emp_sal_functions.php' );

$cookie_prefix=isset($config['cookie_prefix']) ? $config['cookie_prefix'] : '';

setcookie($cookie_prefix.'opt_lang',$opt_lang,time()+60*60*24*365);


// Flag for Chitvalue Sharing
define('USE_CHIT_VALUE_SHARE',1);								$t->assign('USE_CHIT_VALUE_SHARE', USE_CHIT_VALUE_SHARE);
// Defalut Branch
define('DEFAULT_BRANCH_ID',$_SESSION["DEFAULT_BRANCH_ID"]);		$t->assign('DEFAULT_BRANCH_ID', DEFAULT_BRANCH_ID);







// require_once LANG_DIR.$language_files['english'];
/*$langs_loaded = $db->getAll("select distinct lang from tbl_lang_msg");
$loaded_languages = array();
foreach ($langs_loaded as $lng) {
	$loaded_languages[$lng['lang']] = $language_options[$lng['lang']];
}

$lang['error_msg_color'] = 'red';
$t->assign('languages_options', $languages_options);
$t->assign('loaded_languages', $loaded_languages);*/


// Store prev searched string
$ses_prv_search = isset($config['ses_prv_search']) ? $config['ses_prv_search'] : '';
$ses_search_pg  = isset($config['ses_search_pg']) ? $config['ses_search_pg'] : '';
if($ses_prv_search<>''){
	$t->assign ( 'ses_prv_search', $ses_prv_search );
	$t->assign ( 'ses_search_pg', $ses_search_pg );
}

$year_arr = array('2012','2013','2014');
$month_arr = array('1'=>'jan','2'=>'feb','3'=>'mar','4'=>'apr','5'=>'may','6'=>'jun','7'=>'jul','8'=>'aug','9'=>'sep','10'=>'oct','11'=>'nov','12'=>'dec');
$t->assign ( 'year_arr', $year_arr );
$t->assign ( 'month_arr', $month_arr );



/**********************************/
// set home page according to privilage
/**********************************/
$privilege_type=$_SESSION["privilege_type"];
if($privilege_type=="subscriber"){	
		$Home_Page="web_sur_user_home.php";
}elseif($privilege_type=="ip"){			
		$Home_Page="sur_ip_address_list.php";	
}else{	
		$Home_Page="sur_admin_home.php";		
}
$t->assign ('Home_Page', $Home_Page);


//// set the default handler and other values for caching and faster loading
$t->template_dir = TEMPLATE_DIR .'/';
$t->compile_dir = TEMPLATE_C_DIR;
$t->cache_dir = CACHE_DIR;
$t->default_template_handler_func = 'getTplFile';   
$t->caching = false;
$t->force_compile = false;
$t->register_prefilter("prefilter_getlang");
$t->compile_id=$_SESSION['opt_lang'];

$agecounter = array();
$configstart_year=isset($config['start_year']) ? $config['start_year'] : '';
$configend_year=isset($config['end_year']) ? $config['end_year'] : '';

for($i=($configend_year*-1); $i<=($configstart_year*-1); $i++ ) {
	$agecounter[] = $i;
}
$lang['start_agerange'] = $agecounter;
$lang['end_agerange']   = $agecounter;



//FnSetDefValue();
$tmpsql = "select field_value,field_name from tbl_mn_app_setting where regular = 1";
$tmpresult = mysql_query($tmpsql) or die(mysql_error()."<br>".$tmpsql);
while ($tmprow = mysql_fetch_array($tmpresult)) {
	$_SESSION[$tmprow['field_name']] = $tmprow["field_value"];
	$t->assign ($tmprow['field_name'],$tmprow["field_value"]);
}		

/*$comp_logo = fnGetValue("tbl_company_details","logo","logo!='' ORDER BY company_id ASC limit 0, 1");
$comp_logo = "doc/".$comp_logo; 
$t->assign ( 'comp_logo', $comp_logo );*/


/**********************************/
// Set user details to user arry
/**********************************/
$user_details = array();
$user = array();
if($_SESSION["personnel_id"]<>''){
    $reqPersonnel_ID =	$_SESSION["personnel_id"];
	$t->assign ( 'personnel_id', $reqPersonnel_ID );
	$user_details['personnel_id'] = $reqPersonnel_ID;
	$user_details_arr = FnGetValueMultiple("tbl_sur_sign_up","personnel_logo,mr_mrs,first_name,privilege_type","personnel_id= $reqPersonnel_ID"); 
	$user_details['personnel_logo'] = $user_details_arr[0]; 
	$user_details['mr_mrs'] 		= $user_details_arr[1];  
	$user_details['name'] 			= $user_details['mr_mrs'] . " ". $user_details_arr[2];  
	$pri_type 						= $user_details_arr[3];
	$user_details['type'] = FnGetValue("tbl_sur_privilege_types","privilege_type_name","privilege_type_id = '".$pri_type."'");
	$_SESSION["privilege_type"] = $user_details['type'];	
}else{
   $reqPersonnel_ID=0;
}
$t->assign ('user', $user_details);



/****************query string****************/
function querystring( $arr ){
	$str = '';
	foreach( $arr as $item ) {
		if( !is_array( $_GET[$item]) ){
			$str .= $item . '=' . urlencode($_GET[$item]) . '&';
		} elseif (is_array( $_GET[$item]) ) {
			foreach( $_GET[$item] as $subitem) {
				$str .= $item . urlencode('[]') . '=' . urlencode($subitem) . '&';
			}
		} elseif( !is_array( $_POST[$item]) ){
			$str .= $item . '=' . urlencode($_POST[$item]) . '&';
		} elseif (is_array( $_POST[$item]) ) {
			foreach( $_POST[$item] as $subitem) {
				$str .= $item . urlencode('[]') . '=' . urlencode($subitem) . '&';
			}
		}
	}
	return $str;
}



/****************HMAC Hash Generation Function used in Credit Card Transaction****************/
function hmac ($key, $data){
// RFC 2104 HMAC implementation for php.
// Creates an md5 HMAC.
// Eliminates the need to install mhash to compute a HMAC
	$b = 64; // byte length for md5
	if (strlen($key) > $b) {
	   $key = pack("H*",md5($key));
	}
	$key  = str_pad($key, $b, chr(0x00));
	$ipad = str_pad('', $b, chr(0x36));
	$opad = str_pad('', $b, chr(0x5c));
	$k_ipad = $key ^ $ipad ;
	$k_opad = $key ^ $opad;
	return md5($k_opad  . pack("H*",md5($k_ipad . $data)));
}
// end code from lance (resume authorize.net code)



function getLastId() {
	global $db;
	return $db->getOne( 'select LAST_INSERT_ID()' );
}


function get_lang ($mainkey, $subkey='') {
	global $db, $config;
    $primary_lang=$db->getOne("select language from tbl_lang  where default_flag='1'");
	if ($subkey != '') {
	   	$y = $db->getOne("select descr from tbl_lang_msg  where lang = '".$_SESSION['opt_lang']."' and mainkey= '$mainkey' and subkey='$subkey'");
	} else {
	   	$y = $db->getOne("select descr from tbl_lang_msg  where lang = '".$_SESSION['opt_lang']."' and mainkey= '$mainkey'");
	}
	if (!$y) {
		if ($subkey != '') {
		   	$y = $db->getOne("select descr from tbl_lang_msg  where lang = '".$primary_lang."' and mainkey= '$mainkey' and subkey='$subkey'");
		} else {
		   	$y = $db->getOne("select descr from tbl_lang_msg  where lang = '".$primary_lang."' and mainkey= '$mainkey'");
		}
	}

	$y = str_replace('SITENAME', $config['site_name'], $y);
	$y = str_replace('DATE_FORMAT',DATE_FORMAT,$y);
	$y = str_replace('DATE_TIME_FORMAT',DATE_TIME_FORMAT,$y);
	$y = str_replace('DISPLAY_DATE_FORMAT',DISPLAY_DATE_FORMAT,$y);
	return html_entity_decode($y);
}


function prefilter_getlang($source, &$smarty_obj) {
	if (!function_exists('get_my_lang')) {
		function get_my_lang($m){
			$keys=explode(' ',$m[1]);
			list($x,$mkey) = split('=',$keys[0]);
			$skey='';
			if (isset($keys['1']) ){
				list($x1, $skey) = split('=',$keys[1]);
			}
			$mkey=str_replace("'","",$mkey);
			$mkey=str_replace('"','',$mkey);
			$skey=str_replace("'","",$skey);
			$skey=str_replace('"','',$skey);
			return stripslashes(get_lang($mkey,$skey));
		}
	}

	return preg_replace_callback('/{lang (.+?)}/s', 'get_my_lang', $source);
}

/*function get_lang_values ($mainkey) {
	global $db;
    $y = $db->getAll('select subkey, descr from tbl_lang_msg  where lang=? and mainkey= ? order by id', array( 'english', $mainkey));
	$x=array();
	foreach ($y as $ky => $vl) {
		$x[$vl['subkey']] = $vl['descr'];
	}
	$y = $db->getAll('select subkey, descr from tbl_lang_msg  where lang=? and mainkey= ? order by id', array( $_SESSION['opt_lang'], $mainkey));
	foreach ($y as $ky => $vl) {
		$x[$vl['subkey']] = $vl['descr'];
	}
	return $x;
}*/


//====================// Function to Create thumb images ====================//====================
// Since this requires the GD library, you will need an installation of PHP with at least GD 2.0.1 enabled. 
function getExtension($str) {
	$i = strrpos($str,".");
	if (!$i) { return ""; }
	$l = strlen($str) - $i;
	$ext = substr($str,$i+1,$l);
	return $ext;
}

function createThumbnail($imageDirectory, $imageName, $thumbDirectory, $ratioFlag, $thumbWidth, $thumbHeight ){
    $filename = "$imageDirectory/$imageName";
    $ext = strtolower(getExtension($filename));
   
    if($ext=='jpg' || $ext=='jpeg')
        $srcImg = imagecreatefromjpeg($filename);
    if($ext=='gif')
        $srcImg = imagecreatefromgif($filename);
    if($ext=='png')
        $srcImg = imagecreatefrompng($filename); 
		
	$origWidth    = imagesx($srcImg);
	$origHeight   = imagesy($srcImg);

	if ($ratioFlag ==1){
		if ($thumbWidth <> 0){
			$ratio        = $origWidth / $thumbWidth;
			$thumbHeight   = $origHeight * $ratio;
		}else{
			$ratio        = $origHeight / $thumbHeight;
			$thumbWidth   = $origWidth * $ratio;
		}
	}
	//imagecreatetruecolor
	
	$thumbImg = imagecreatetruecolor($thumbWidth, $thumbHeight);
	imagecopyresized($thumbImg, $srcImg, 0, 0, 0, 0, $thumbWidth, $thumbHeight, $origWidth, $origHeight);
	
	$filename2 = "$thumbDirectory/$imageName";
    $ext2 = strtolower(getExtension($filename2));
   
    if($ext2=='jpg' || $ext2=='jpeg')
		imagejpeg($thumbImg, $filename2);
    if($ext2=='gif')
		imagegif($thumbImg, $filename2);
    if($ext2=='png')
		imagepng($thumbImg, $filename2);	
}
//====================// End Function to Create thumb images ====================//====================


//====================// Function to add mkey in multilanguage if not there ====================//====================
function fnAddUnavialableMsg($db_fields){
	/*if ($db_fields["FIELD_1"]["name"]<>''){
		$chkins_cnt = FnGetCount("tbl_lang_msg","mainkey='".$db_fields['FIELD_1']['name']."' and lang='english'");
		if ($chkins_cnt == 0){ 
			$sql_eng_ins = "Insert into tbl_lang_msg (id,lang,mainkey,descr) values(NULL,'english','".$db_fields['FIELD_1']['name']."','".$db_fields['FIELD_1']['dis_name']."')";
			$result = mysql_query($sql_eng_ins) or die(mysql_error().$sql_eng_ins);
		}
	}
	
	if ($db_fields["FIELD_2"]["name"]<>''){
		$chkins_cnt = FnGetCount("tbl_lang_msg","mainkey='".$db_fields['FIELD_2']['name']."' and lang='english'");
		if ($chkins_cnt == 0){ 
			$sql_eng_ins = "Insert into tbl_lang_msg (id,lang,mainkey,descr) values(NULL,'english','".$db_fields['FIELD_2']['name']."','".$db_fields['FIELD_2']['dis_name']."')";
			$result = mysql_query($sql_eng_ins) or die(mysql_error().$sql_eng_ins);
		}
	}*/
}
//====================// end Function to add mkey in multilanguage if not there ====================//====================


//=================================

/*$_SESSION["RecordsPerPage"]  	= 15 ;*/  
function decimal_num($num){
	$num = sprintf("%0.2f",$num);
	return $num;
}
function DefineComboBoxArray($FirstValue,$SecondValue,$TableName,$OrderBy,$WhereCondition){
	$sql = "SELECT $FirstValue,$SecondValue FROM $TableName ";
	if($WhereCondition != "")
		$sql .= " where ".$WhereCondition;
	$sql .= " order by $OrderBy asc ";
	//echo $sql;
	$DataArray 	= array();
	$temp 		= mysql_query( $sql) or die(mysql_error()."<br>".$sql);
	while($row = mysql_fetch_array($temp)) {
		$DataArray[$FirstValue][] 	= $row[$FirstValue];
		$DataArray[$SecondValue][] 	= $row[$SecondValue];
	}
	return $DataArray;
}	
function echo_memory_usage() {
	$mem_usage = memory_get_usage(true);
   
	if ($mem_usage < 1024)
		echo $mem_usage." bytes";
	elseif ($mem_usage < 1048576)
		echo round($mem_usage/1024,2)." kilobytes";
	else
		echo round($mem_usage/1048576,2)." megabytes";
	   
	echo "<br/>";
}
// Number to word Function - start
function One($num){
	switch ($num){
		Case 1:
		  return "One"; break;
		Case 2:
		  return "Two"; break;
		Case 3:
		  return "Three"; break;
		Case 4:
		  return "Four"; break;
		Case 5:
		  return "Five"; break;
		Case 6:
		  return "Six"; break;
		Case 7:
		  return "Seven"; break;
		Case 8:
		  return "Eight"; break;
		Case 9:
		  return "Nine"; break;
		Case 0:
		  return "";
		  
	}
}

function Two($num){ 
	switch (substr($num,0, 1)){	
		Case 1:
			switch ($num) {	
				Case 10:
				  $num1 = "Ten"; break;
				Case 11:
				  $num1 = "Eeleven"; break;
				Case 12:
				  $num1 = "Twelve"; break;
				Case 13:
				  $num1 = "Thirteen"; break;
				Case 14:
				  $num1 = "Fourteen"; break;
				Case 15:
				  $num1 = "Fifteen"; break;
				Case 16:
				  $num1 = "Sixteen"; break;
				Case 17:
				  $num1 = "Seventeen"; break;
				Case 18:
				  $num1 = "Eighteen"; break;
				Case 19:
				  $num1 = "Nineteen"; break;
			}
			break;
		Case 2:
		  $num1 = "Twenty " . One(substr($num,1)); break;
		Case 3:
		  $num1 = "Thirty " . One(substr($num,1)); break;
		Case 4:
		  $num1 = "Forty " . One(substr($num,1)); break;
		Case 5:
		  $num1 = "Fifty " . One(substr($num,1)); break;
		Case 6:
		  $num1 = "Sixty " . One(substr($num,1)); break;
		Case 7:
		  $num1 = "Seventy " . One(substr($num,1)); break;
		Case 8:
		  $num1 = "Eighty " . One(substr($num,1)); break; 
		Case 9:
		  $num1 = "Ninety " . One(substr($num,1)); break;
		Case 0:
		  $num1 = One(substr($num,1)); break;
	}
	return $num1;
}

function NumtoWord($txtnum){
	switch (strlen(trim($txtnum))){
		Case 1:
			return One($txtnum); break;
		Case 2:
			return Two($txtnum); break;
		Case 3:
			return ((substr($txtnum, 0,  1) == 0)? NumtoWord(substr($txtnum, 1)) : One(substr($txtnum, 0, 1)) . " Hundred " . NumtoWord(substr($txtnum, 1)));
			break;
		Case 4:
			return ((substr($txtnum, 0,  1) == 0)? NumtoWord(substr($txtnum, 1)) : One(substr($txtnum, 0, 1)) . " Thousand " . NumtoWord(substr($txtnum, 1)));			
			break;
		Case 5:
			return ((substr($txtnum, 0,  2) == 0)? NumtoWord(substr($txtnum, 2)) : Two(substr($txtnum, 0, 2)) . " Thousand " . NumtoWord(substr($txtnum, 2)));						
			break;
		Case 6:
			return ((substr($txtnum, 0,  1) == 0)? NumtoWord(substr($txtnum, 1)) : One(substr($txtnum, 0,  1)) . " Lakh " . NumtoWord(substr($txtnum, 1)));
			break;
		Case 7:
			return ((substr($txtnum, 0,  2) == 0)? NumtoWord(substr($txtnum, 2)) : Two(substr($txtnum, 0,  2)) . " Lakh " . NumtoWord(substr($txtnum, 2)));
			break;
		Case 8:
			return ((substr($txtnum, 0,  1) == 0)? NumtoWord(substr($txtnum, 1)) : One(substr($txtnum, 0,  1)) . " Crore " . NumtoWord(substr($txtnum, 1)));
			break;
		Case 9:
			return ((substr($txtnum, 0,  2) == 0)? NumtoWord(substr($txtnum, 2)) : Two(substr($txtnum, 0,  2)) . " Crore " . NumtoWord(substr($txtnum, 2)));
			break;
		Case 0:
			return "Zero"; break;
	}
}

function numberToCurrency($num){
    if(setlocale(LC_MONETARY, 'en_IN'))
      return money_format('%.0n', $num);
    else {
      $explrestunits = "" ;
      if(strlen($num)>3){
          $lastthree = substr($num, strlen($num)-3, strlen($num));
          $restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
          $restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
          $expunit = str_split($restunits, 2);
          for($i=0; $i<sizeof($expunit); $i++){
              // creates each of the 2's group and adds a comma to the end
              if($i==0)
              {
                  $explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
              }else{
                  $explrestunits .= $expunit[$i].",";
              }
          }
          $thecash = $explrestunits.$lastthree;
      } else {
          $thecash = $num;
      }
      return '₹ ' . $thecash;
    }
}
?>