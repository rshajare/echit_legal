<?php if ( !defined( 'SMARTY_DIR' ) ){include_once( 'init.php' );} 

// +----------------------------------------------------------------------------------------------------------------------------+
// |  File Name : msg_ajex_functions.php
// |  Version : 2.0 
// |  Create-date : 29, March, 2010
// |  Modify-date : 16, June, 2010
// |  Authors : Niteen Borate                        
// | 
// |  Desciption : functions for internal message comminication
// |  How to use : 
// |
// |  
// |  Ajex Functions for new letter
// |  op= 52 // category folter
// |  op= 51 // enter fck editor for selected template
// |  op= 50 // Attachment delete
// +------------------------------------------------------------------------------------------------------------------------------
//

// Other ajex Functions for new letter
$op = $_GET['op'];

if($op == 52){
	$reqid 	  = $_GET['cat_id'];
	if($reqid == '0')
		$sql = "SELECT * FROM tbl_msg_template where lang = '$tmplaguage' order by news_letter_name asc";
	else	
		$sql = "SELECT * FROM tbl_msg_template where lang = '$tmplaguage' and cat_id = $reqid order by news_letter_name asc";
	
	//echo $sql;
	$temp = mysql_query($sql) or dir(mysql_error().$sql);
	$str = "";
	$str .= " <select id='subid' class='TextBox' name='txtmsglist' onChange='Javascript: fnchangesub();'>";
	$str .= "<option value=''>select</option>";
	while($row = mysql_fetch_array($temp)){
		$main_news_letter_id   = $row['main_news_letter_id'];
		$str .= "<option value='".$main_news_letter_id ."'>".$row['news_letter_name']."</option>";
	}
	$str .= "</select>";
	echo $str;
	
}elseif($op == 51){ // Get canned message
	
	
	header('Content-Type: text/xml');
	header("Cache-Control: no-cache, must-revalidate");
	include_once("fckeditor.php") ;
	$offset = 60 * 60 * 24 * 1;
	$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";
	header($ExpStr);
	echo "<xml_result>";
	$sub_id  = $_GET['sub_id'];
	$msgsub  = FnGetValue("tbl_msg_template","news_letter_name","lang = '$tmplaguage' and main_news_letter_id = $sub_id");
	$mainmsg = FnGetValue("tbl_msg_template","news_letter","lang = '$tmplaguage' and main_news_letter_id = $sub_id");
	$mainmsgstr = "";
	$virdir= split("/",$_SERVER["PHP_SELF"]);
	echo "<msg><![CDATA[";

	$NewPageEditor= new FCKeditor("txtArticle");
	//$NewPageEditor->BasePath = "/".$virdir[1]."/";
	$NewPageEditor->Height = 300;
	$NewPageEditor->Value = "$mainmsg";
	$NewPageEditor->Create();
	echo "]]></msg>";
	echo "<subject><![CDATA[".$msgsub."]]></subject>";
	echo "</xml_result>";
	


}elseif($op == 50){

	header('Content-Type: text/xml');
	header("Cache-Control: no-cache, must-revalidate");
	include_once("fckeditor.php") ;
	$offset = 60 * 60 * 24 * 1;
	$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";
	header($ExpStr);
	echo "<xml_result>";
	if($_GET['edit']==1){
		$attach_table="tbl_msg_attachment";	
		$attach_tableid="news_letter_attachment_id";
	}else{
		$attach_table="tbl_msg_attachment_temp";
		$attach_tableid="news_letter_attachment_temp_id";
	}	
	
	$attach_id  = $_GET['attach_id'];
	$del_filename=fngetvalue("$attach_table","document"," $attach_tableid = $attach_id");
	unlink("doc/mail/".$del_filename);
	$sql_del = "Delete from $attach_table where $attach_tableid = $attach_id";
	$sql_res = mysql_query($sql_del) or die(mysql_error().$sql_del);
	
	$sql = "select * from $attach_table";
	$result = mysql_query($sql)	or die(mysql_error()."<br>".$sql);
	$cc_list_str ='';
	$cc_list_str ="<table BORDER='0' CELLPADDING='0' CELLSPACING='1' CLASS='WhiteBG'>";			 
	$result_select = mysql_query($sql) or die(mysql_error().$sql); 
	$attach_cnt = 0; 
	while ($row_select = mysql_fetch_array($result_select)){ 
		$attach_file = $row_select['document'];	
		//echo $attach_file."<br>";
		$news_letter_attachment_temp_id = $row_select[$attach_tableid];	
		$cc_list_str .="<tr>";
		$cc_list_str .="<td CLASS='WhiteBG' width='200' height='20'><a href='mail_doc/'".$attach_file."' target='_blank'>".$attach_file."</a></td>";
		$cc_list_str .="<td CLASS='WhiteBG' width='20' align='center' height='20'><a href='javascript: fndel_attach(".$news_letter_attachment_temp_id.")';><img src='images/del.gif' border='0' height='18'></a></td></tr>";
				
	}	
	$cc_list_str.="</table>";	
	echo "<attachment_list><![CDATA[".$cc_list_str."]]></attachment_list>";	
	echo "</xml_result>";
	
}elseif($op == 28){

	$reqfrom_personnel_id    = $_SESSION['personnel_id'];
	$reqPersonnel_ID 	  = $_GET['personnel_id'];
	
	$sql="delete from tbl_msg_tmp_personnel where Personnel_ID = '$reqPersonnel_ID' and from_personnel_id = '$reqfrom_personnel_id' ";
	//echo $sql;
	$db->query( $sql);
	
	//$sql = "SELECT * FROM tbl_news_letter where lang = '$tmplaguage' and cat_id = $reqid order by `news_letter_name` asc";
	//$temp = mysql_query($sql) or dir(mysql_error().$sql);
	
}elseif ($op==61){

	$print_page_url=$_GET["print_page_url"];
	$basepath = "http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']);  //"/".$_SERVER['REQUEST_URI'];
	$req_Url = "$basepath/$print_page_url";
	$mainmsgstr= (SendSMScurl($req_Url)); 		// send actual sms
	//$mainmsgstr= $req_Url; 		// send actual sms
	header('Content-Type: text/xml');
	header("Cache-Control: no-cache, must-revalidate");
	include_once("fckeditor.php") ;
	$offset = 60 * 60 * 24 * 1; 
	$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s", time() + $offset) . " GMT";
	header($ExpStr);
	echo "<xml_result>";
	//$msgsub = "Purchase order request";

	//$mainmsgstr = "";
	echo $mainmsgstr;
	$virdir= split("/",$_SERVER["PHP_SELF"]);
	echo "<msg><![CDATA[".$mainmsgstr;
	echo "]]></msg>";
	echo "<subject><![CDATA[".$msgsub."]]></subject>";
	echo "</xml_result>";


}



?>