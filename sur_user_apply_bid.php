<?php
if ( !defined( 'SMARTY_DIR' ) ){include_once( 'init.php' );}
include( 'sur_usr_permission.php' );
//include("updateable_other_functions.php");

$reqpersonnel_id = $_SESSION['personnel_id'];
header('Content-Type: text/html; charset=utf-8');

$req_privilege_type = $_SESSION["privilege_type"];
$t->assign( 'privilege_type' , $req_privilege_type );

$SubID	 = fnfind_member_SubID($reqpersonnel_id);
$req_GroupNo = $_REQUEST["GroupNo"];
$req_InstNo  = $_REQUEST["InstNo"];
$req_Auction_ID = $_REQUEST["Auction_ID"];
$req_ad_flg = $_REQUEST["ad_flg"];
//INSERT INTO `tbl_mn_app_setting` (`app_setting_id`, `field_name`, `field_value`, `note`, `approved`, `type`) VALUES (NULL, NULL, NULL, '', '0', '');

$req_Max_bid_percentage = fnGetValue("tbl_mn_app_setting","field_value","field_name = 'Max_bid_percentage'");
$req_Min_bid_percentage = fnGetValue("tbl_mn_app_setting","field_value","field_name = 'Min_bid_percentage'");

if($req_Max_bid_percentage == ""){
	$req_present = fnGetCount("tbl_mn_app_setting","field_name = 'Max_bid_percentage'");
	if($req_present == 0){
		$sql = "INSERT INTO `tbl_mn_app_setting` (`app_setting_id`, `field_name`, `field_value`, `note`, `approved`, `type`) VALUES (NULL, 'Max_bid_percentage', '30', '', '1', 'text')";
		$db->getAll( $sql );
	}
	$req_Max_bid_percentage = 30;
}
if($req_Min_bid_percentage == ""){
	$req_present = fnGetCount("tbl_mn_app_setting","field_name = 'Min_bid_percentage'");
	if($req_present == 0){
		$sql = "INSERT INTO `tbl_mn_app_setting` (`app_setting_id`, `field_name`, `field_value`, `note`, `approved`, `type`) VALUES (NULL, 'Min_bid_percentage', '5', '', '1', 'text')";
		$db->getAll( $sql );
	}
	$req_Min_bid_percentage = 5;
}
$req_chit_value		= fnGetValue("groupdetails","ChitValue","GroupNo = '$req_GroupNo'");
$req_GroupID		= fnGetValue("groupdetails","GroupID","GroupNo = '$req_GroupNo'");

$req_min_bid_amt  = ($req_chit_value * $req_Min_bid_percentage)/100;
$req_max_bid_amt  = ($req_chit_value * $req_Max_bid_percentage)/100;

$t->assign( 'min_bid_amt' , $req_min_bid_amt );
$t->assign( 'max_bid_amt' , $req_max_bid_amt );
if($_GET["error"] == 1){
	$str_error = "Bid can be placed with one amount only once & Bid amount should be more than previous maximun placed bid amount ";
}
if($_GET["error"] == 2){
	$str_error = "Prized subscriber can not place bid";
}

$t->assign( 'str_error' , $str_error );
$req_Bidding_status = fnGetValue("auction","bidding_status","Auction_ID = ".$req_Auction_ID);
if($req_Bidding_status != 2){
	$bid_info_sql 		= "select * from groupdetails where GroupNo = '$req_GroupNo'";
	$bid_info_result  	= mysql_query($bid_info_sql) or die(mysql_error().$bid_info_sql);
	while($bid_info_row = mysql_fetch_array($bid_info_result)){
		$Start_Auctiontime 	= $bid_info_row["Def_Auctiontime"];
		$bid_open_mins 		= $bid_info_row["bid_open_mins"];
		$Def_AuctionDay  	= $bid_info_row["Def_AuctionDay"];
		$End_Auctiontime 	= $bid_info_row["End_Auctiontime"];
	}	
	//$start_date = date("Y-m-").$Def_AuctionDay." ".$Start_Auctiontime;
	if ($bid_open_mins == "")$bid_open_mins=0;
	$start_date = fnGetValue("auction","AuctionDateTime","Auction_ID = ".$req_Auction_ID);
	
	$end_date 	= fnGetValue("groupdetails","DATE_ADD('$start_date', INTERVAL $bid_open_mins MINUTE)","");
	$curtime 	= fnGetValue("groupdetails","NOW()","");;

	$chk_start_time 	= fnGetValue("groupdetails","TIME_TO_SEC(TIMEDIFF('".$start_date."','".$curtime."'))","");;
	$chk_end_time 		= fnGetValue("groupdetails","TIME_TO_SEC(TIMEDIFF('".$curtime."','".$end_date."'))","");;
	
	if(($chk_start_time < 0)&&($chk_end_time < 0)){
		$sql = "update auction set bidding_status = 0 where Auction_ID = ".$req_Auction_ID;
		$sql_tmp = $db->getAll( $sql );
	}elseif(($chk_start_time < 0)&&($chk_end_time > 0)){
		$sql = "update auction set bidding_status = 1, biddingendtime = CURTIME() where Auction_ID = ".$req_Auction_ID;
		$sql_tmp = $db->getAll( $sql );
		$reqtxtForeman		= fnGetvalue("tbl_mn_app_setting","field_value","field_name = 'def_foreman_comm'");
		$auction_bid_sql  	= "select auction_bids.prizeamount,auction_bids_id,Ticketno 
							   from auction_bids,applicantdetails,subdirdetails 
							   where applicantdetails.SubID = subdirdetails.SubID and auction_bids.Appid = applicantdetails.AppID and 
							   instno = '$req_InstNo' and applicantdetails.GroupNo='$req_GroupNo' 
							   order by auction_bids.prizeamount desc";
		$auction_bid_temp 	= $db->getAll($auction_bid_sql);
		$req_max_amt_cnt 	= 0;
		$auction_max_amt 	= array();
		foreach( $auction_bid_temp as $index => $auction_bid_row ) {
			if($auction_bid_row[ 'prizeamount' ] >= $req_max_amount){
				$auction_max_amount['Ticketno'][$req_max_amt_cnt] 		 = $auction_bid_row[ 'Ticketno' ];
				$auction_max_amount['auction_bids_id'][$req_max_amt_cnt] = $auction_bid_row[ 'auction_bids_id' ];
				$auction_max_amount['prizeamount'][$req_max_amt_cnt] 	 = $auction_bid_row[ 'prizeamount' ];
				$auction_max_amt[$req_max_amt_cnt] 						 = $auction_bid_row[ 'Ticketno' ];
				
				$req_max_amount = $auction_bid_row[ 'prizeamount' ];
				$req_max_amt_cnt++;
			}
		}
		if($req_max_amt_cnt > 1){
			$rand_keys = array_rand($auction_max_amt, 1);
			$req_Ticket_no  = $auction_max_amount['Ticketno'][$rand_keys];
			$req_auction_bids_id = $auction_max_amount['auction_bids_id'][$rand_keys];
		}else{
			$req_Ticket_no  = $auction_max_amount['Ticketno'][0];
			$req_auction_bids_id = $auction_max_amount['auction_bids_id'][0];
		}
		if($req_auction_bids_id != ""){
			$updq_sql = "update auction_bids set winflag =1 where auction_bids_id = $req_auction_bids_id";	
			$auction_bid_temp = $db->getAll($updq_sql);
			//fnaddprize($req_GroupNo,$req_Ticket_no,$req_InstNo,$req_max_amount,$reqtxtForeman,0,'');
			fnaddprize($req_GroupNo,$req_Ticket_no,$req_InstNo,$req_max_amount,$reqtxtForeman,0,'','YES');
		}		
		$sql = "update auction set bidding_status = 2, biddingendtime = CURTIME() where Auction_ID = ".$req_Auction_ID;
		$sql_tmp = $db->getAll( $sql );
	}
}
$req_Bidding_status = fnGetValue("auction","bidding_status","Auction_ID = ".$req_Auction_ID);


if($_GET["place_bid"] == 1){
	$req_Ticket_no  	= $_REQUEST["Ticket_no"];
	$reqtxtAuction_Amt 	= $_REQUEST["txt_user_bid"];
	$req_bid_max_amt_cnt = 0;
	$reqAppid			 = fngetvalue("applicantdetails","AppID"," Ticketno='$req_Ticket_no' and GroupNo='$req_GroupNo'");
	$IsPrizedAppid		= fnGetCount("applicantdetails"," Ticketno='$req_Ticket_no' and GroupNo='$req_GroupNo' and prizeinstno >0 ");
	
	$req_user_bid_cnt 	= fnGetCount("auction_bids","prizeamount = '$reqtxtAuction_Amt' and instno = ".$req_InstNo."  and Appid = ".$reqAppid);
	$req_user_max_bid 	= fnGetValue("auction_bids","max(prizeamount)", "instno = ".$req_InstNo." and Appid = ".$reqAppid);

	if($IsPrizedAppid > 0){
		?><script language="javascript">alert("Prized subscriber can not place bid");
		window.location.href = "sur_user_apply_bid.php?ad_flg=<?=$req_ad_flg?>&GroupNo=<?=$req_GroupNo?>&InstNo=<?=$req_InstNo?>&Auction_ID=<?=$req_Auction_ID?>";
		</script><?		
	}else if(($req_user_bid_cnt < 1) && ($reqtxtAuction_Amt > $req_user_max_bid)){
	//if($req_bid_max_amt_cnt < 2){
		if(($reqtxtAuction_Amt >= $req_min_bid_amt)&&($reqtxtAuction_Amt <= $req_max_bid_amt)){
			fnaddbid($req_GroupNo,$req_Ticket_no,$req_InstNo,$reqtxtAuction_Amt);
		}
		header("Location: sur_user_apply_bid.php?ad_flg=$req_ad_flg&GroupNo=$req_GroupNo&InstNo=$req_InstNo&Auction_ID=".$req_Auction_ID);
	/*}else{
		?><script language="javascript">alert("Sorry we can not accept your bid.Only two peaple can bid with <?=$req_max_bid_amt?>");
		window.location.href = "sur_user_apply_bid.php?ad_flg=<?=$req_ad_flg?>&GroupNo=<?=$req_GroupNo?>&InstNo=<?=$req_InstNo?>&Auction_ID=<?=$req_Auction_ID?>";
		</script><?
	}*/
	}else{
		//header("Location: web_sur_user_apply_bid.php?error=1&error_amt=$reqtxtAuction_Amt&ad_flg=$req_ad_flg&GroupNo=$req_GroupNo&InstNo=$req_InstNo&Auction_ID=".$req_Auction_ID);
		?><script language="javascript">alert("Bid can be placed with one amount only once & Bid amount should be more than previous maximun placed bid amount");
		window.location.href = "sur_user_apply_bid.php?ad_flg=<?=$req_ad_flg?>&GroupNo=<?=$req_GroupNo?>&InstNo=<?=$req_InstNo?>&Auction_ID=<?=$req_Auction_ID?>";
		</script><?		
	}
}
if($_GET["start_bid"] == 1){
	$req_Ticket_no  	= $_REQUEST["Ticket_no"];
	$sql = "update auction set bidding_status = 0 where Auction_ID = ".$req_Auction_ID;
	$sql_tmp = $db->getAll( $sql );
	header("Location: sur_user_apply_bid.php?ad_flg=1&GroupNo=$req_GroupNo&InstNo=$req_InstNo&Auction_ID=".$req_Auction_ID);
}
if($_GET["close_bid"] == 1){
	$req_Ticket_no  	= $_REQUEST["Ticket_no"];
	$sql = "update auction set bidding_status = 1, biddingendtime = CURTIME() where Auction_ID = ".$req_Auction_ID;
	$sql_tmp = $db->getAll( $sql );
	header("Location: sur_user_apply_bid.php?ad_flg=1&GroupNo=$req_GroupNo&InstNo=$req_InstNo&Auction_ID=".$req_Auction_ID);
}
if($_GET["find_winner"] == 1){
	$reqtxtForeman	= $_REQUEST["txtForeman"];
	$auction_bid_sql  = "select auction_bids.prizeamount,auction_bids_id,Ticketno from auction_bids,applicantdetails,subdirdetails where applicantdetails.SubCodeNo = subdirdetails.SubID and auction_bids.Appid = applicantdetails.AppID and instno = '$req_InstNo' and applicantdetails.GroupNo='$req_GroupNo' order by auction_bids.prizeamount desc";
	$auction_bid_temp = $db->getAll($auction_bid_sql);
	$req_max_amt_cnt = 0;
	$auction_max_amt = array();
	foreach( $auction_bid_temp as $index => $auction_bid_row ) {
		if($auction_bid_row[ 'prizeamount' ] >= $req_max_amount){
			$auction_max_amount['Ticketno'][$req_max_amt_cnt] 		 = $auction_bid_row[ 'Ticketno' ];
			$auction_max_amount['auction_bids_id'][$req_max_amt_cnt] = $auction_bid_row[ 'auction_bids_id' ];
			$auction_max_amount['prizeamount'][$req_max_amt_cnt] 	 = $auction_bid_row[ 'prizeamount' ];
			$auction_max_amt[$req_max_amt_cnt] 						 = $auction_bid_row[ 'Ticketno' ];
			
			$req_max_amount = $auction_bid_row[ 'prizeamount' ];
			$req_max_amt_cnt ++;
		}
	}
	if($req_max_amt_cnt > 1){
		$rand_keys = array_rand($auction_max_amt, 1);
		$req_Ticket_no  = $auction_max_amount['Ticketno'][$rand_keys];
		$req_auction_bids_id = $auction_max_amount['auction_bids_id'][$rand_keys];
	}else{
		$req_Ticket_no  = $auction_max_amount['Ticketno'][0];
		$req_auction_bids_id = $auction_max_amount['auction_bids_id'][0];
	}
	$updq_sql = "update auction_bids set winflag =1 where auction_bids_id = $req_auction_bids_id";	
	$auction_bid_temp = $db->getAll($updq_sql);
	
	$sql = "update auction set bidding_status = 2, biddingendtime = CURTIME() where Auction_ID = ".$req_Auction_ID;
	$sql_tmp = $db->getAll( $sql );

	fnaddprize($req_GroupNo,$req_Ticket_no,$req_InstNo,$req_max_amount,$reqtxtForeman,0,'','YES');
	header("Location: sur_user_apply_bid.php?ad_flg=1&GroupNo=$req_GroupNo&InstNo=$req_InstNo&Auction_ID=".$req_Auction_ID);
}
$auction_sql = "select * from auction where Auction_ID = ".$req_Auction_ID;
$auction_temp = $db->getAll( $auction_sql );
$data = array();
foreach( $auction_temp as $index => $auction_row ) {
	$auction_row[ 'AuctionDateTime' ] 		= sqldateout_time($auction_row[ 'AuctionDateTime' ]);
	$auction_row[ 'LastDateofPayment' ] 	= sqldateout($auction_row[ 'LastDateofPayment' ]);
	$req_Ticket_no = $auction_row[ 'Ticketno' ];
	$req_Closed = $auction_row[ 'bidding_status' ];
	$data[] = $auction_row;
}
$t->assign( 'auction_row' , $data );
$t->assign( 'req_Closed' , $req_Closed );
// list of other languages except lang_pri i.e english
//$tickect_no_list = array();
$tickect_no_list = array();
$tickect_no_tmp = $db->getAll( "SELECT * FROM applicantdetails where GroupNo = '$req_GroupNo' order by Ticketno" );
foreach( $tickect_no_tmp as $index => $tickect_no_row ) {
		$tickect_no_list['Ticketno'][] = $tickect_no_row['Ticketno'];
}
$t->assign ( 'tickect_no_list', $tickect_no_list );

if(($SubID != "") && ($req_Ticket_no == "")) $req_Ticket_no = fngetvalue("applicantdetails","Ticketno"," SubCodeNo='$SubID' and GroupNo='$req_GroupNo'");
if($req_Ticket_no != "") $reqAppid = fngetvalue("applicantdetails","AppID"," Ticketno='$req_Ticket_no' and GroupNo='$req_GroupNo'");

$auction_bid_sql  = "select bid_datetime,auction_bids.prizeamount,FullName,winflag 
					 from auction_bids,applicantdetails,subdirdetails 
					 where applicantdetails.SubID = subdirdetails.SubID and auction_bids.Appid = applicantdetails.AppID and 
					 	   instno = '$req_InstNo' and applicantdetails.GroupNo='$req_GroupNo'";
						   
//if($reqAppid != "") $auction_bid_sql  .= " and auction_bids.Appid = '$reqAppid'";
$auction_bid_sql  .= " order by winflag desc, auction_bids.prizeamount desc";
$auction_bid_temp = $db->getAll($auction_bid_sql);
$data1 = array();
$req_max_amount = 0;
$req_max_amt_cnt = 0;
foreach( $auction_bid_temp as $index => $auction_bid_row ) {
	if($req_max_amount <= $auction_bid_row[ 'prizeamount' ]){
		$auction_bid_row[ 'max_amount' ] = 1;
		$req_max_amount = $auction_bid_row[ 'prizeamount' ];
		$req_max_amt_cnt ++;
	}else{ 
		$auction_bid_row[ 'max_amount' ] = 0;
	}
	$auction_bid_row[ 'bid_datetime' ] 		= sqldateout_time($auction_bid_row[ 'bid_datetime' ]);
	$data1[] = $auction_bid_row;
}
$arrforeman_comm = array();
$arrforeman_comm['Value'][] = 1;
$arrforeman_comm['Value'][] = 2;
$arrforeman_comm['Value'][] = 3;
$arrforeman_comm['Value'][] = 3.70;
$arrforeman_comm['Value'][] = 4;
$arrforeman_comm['Value'][] = 5;
$arrforeman_comm['Value'][] = 5.681;
$arrforeman_comm['Value'][] = 6;
$arrforeman_comm['Value'][] = 7;
$arrforeman_comm['Value'][] = 7.87;
$arrforeman_comm['Value'][] = 8;
$arrforeman_comm['Value'][] = 8.70;
$arrforeman_comm['Value'][] = 9;
$arrforeman_comm['Value'][] = 10;
$arrforeman_comm['Value'][] = 14.035;
$t->assign ( 'arrforeman_comm', $arrforeman_comm );
$req_def_foreman_comm = fnGetValue("tbl_mn_app_setting","field_value","field_name = 'def_foremancomm'");
$t->assign ( 'def_foreman_comm', $req_def_foreman_comm);


$t->assign( 'auction_bid_row' , $data1 );
$t->assign( 'ad_flg' , $req_ad_flg );
$t->assign( 'req_max_bid' , $req_max_bid );
$t->assign( 'req_Ticket_no' , $req_Ticket_no );
$t->assign( 'req_Auction_ID' , $req_Auction_ID );
$t->assign( 'req_GroupNo' , $req_GroupNo );
$t->assign( 'req_GroupID' , $req_GroupID );

$t->assign( 'req_InstNo' , $req_InstNo ); 
$t->display('sur_user_apply_bid.htm');exit;
?>