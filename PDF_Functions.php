<?PHP
include( 'sur_adm_permission.php' );
//include_once("includes/common.php");
//include_once("includes/other_common_function.php");
//include_once("All_Popup.php");
//include_once("includes/Appt_updateable.php");
$tmpPrivilege_Type_ID =  $_SESSION['Privilege_Type_ID'];

function pdf_patient_info($Patient_ID){
	$sel_sql = "Select * from patient where Patient_ID = ".$Patient_ID;
	$result = mysql_query($sel_sql) or die(mysql_error().$sel_sql);
	while($row = mysql_fetch_array($result)){
		$req_Full_Name = full_name("Patient",$Patient_ID);
		$req_Age 		= $row["age"];
		$req_CRNO 		= $row["Reg_No"];
		$req_Address 	= $row["Address"];
		$req_Gender 	= $row["Gender"];
		$req_City 		= $row["City"];
		$req_Zip_Code	= $row["Zip_Code"];
		$req_Ph_No		= $row["Phone"];
		$req_Ref_By		= $row["Ref_By"];
		$reqPatient_Type= $row["Patient_Type"];
		$reqAge_Month	= $row["Age_Month"];
		$reqAge_Days	= $row["Age_Days"];
	}
	$htmlcontent = "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td colspan=\"2\">".$req_Full_Name."&nbsp;</td><td align=\"right\" ><strong>CR. NO. : ".$req_CRNO." </strong></td></tr>";
	$req_display_string = "";
	if($req_Gender != ""){  
		if($req_Gender == "Female")	$req_display_string .= "F";	else	$req_display_string .= "M";
	}
    if($req_Age != ""){
		if($req_display_string != "") 	$req_display_string .= " / ";
  		$req_display_string .= $req_Age."yrs "; 
	}
	if($reqAge_Month != "")		$req_display_string .= " - " . $reqAge_Month . "m";
	if($reqAge_Days != "")		$req_display_string .= " - " . $reqAge_Days . "d";
	if($req_display_string != ""){
		$htmlcontent .= "<tr><td colspan=\"2\"><font size='8'>".$req_display_string."</font></td><td>&nbsp;</td></tr>";
    }
	$req_display_string = "";
	if($req_Address != ""){ 	
		if($req_display_string != "") 	$req_display_string .= " , ";
		$req_display_string .= $req_Address;
	}
	if($req_City != ""){
		if($req_display_string != "") 	$req_display_string .= " , ";
		$req_display_string .= $req_City;
	}
	if($req_Zip_Code != ""){ 
		if($req_display_string != "") 	$req_display_string .= " , ";
		$req_display_string .= $req_Zip_Code;
	}
	if($req_display_string != ""){
		$htmlcontent .= "<tr><td colspan=\"2\"><font size='8'>".$req_display_string."</font></td><td>&nbsp;</td></tr>";
	}
	$req_display_string = "";
	if($req_Ph_No != "")	
		$req_display_string = "PH : ".strtoupper($req_Ph_No);
	if($req_display_string != ""){
		$htmlcontent .= "<tr><td><font size='8' colspan=\"2\">".$req_display_string."</font></td><td>&nbsp;</td></tr>";
	}
	$req_Ref_By 	 = fnGetValue("tbl_staff","concat(First_Name,' ',Last_Name)","Staff_ID = '$req_Ref_By'");
	$htmlcontent 	.= "<tr><td>";
	$req_show 		 = fnShow_Patient_Type_last(0,$reqPatient_Type);
	$req_show_array  = split("~",$req_show);
	$req_str 		 = "";
	for($i=0;$i<count($req_show_array)-1;$i++){
		if($req_str != "")	$req_str .= "/";
		$req_str 	.= $req_show_array[$i];
	}
	if($req_str != ""){
		$htmlcontent 	.= "<font size='8'>$req_str</font>";
	}else{
		$htmlcontent 	.= "&nbsp;";
	}
	$htmlcontent 	.= "</td><td align=\"left\"><font size='8'><strong>Speciality Clinic :</strong> </font></td><td align=\"right\">";
	if($req_Ref_By <>"") { 
		$htmlcontent .= "<font size='8'><strong>Ref. By :</strong> ".strtoupper($req_Ref_By)."</font>"; 
	}else{
		$htmlcontent .= "&nbsp;";
	}
	$htmlcontent 	.= "</td></tr>";
	$htmlcontent 	.= "</table><hr />";
	return $htmlcontent;
}
function pdf_vital_sign($reqVisit_ID,$cc,$req_FromDate,$req_ToDate){
	$vital_sign_sql = "select * from tbl_vital_sign, patient_diagnosis where  tbl_vital_sign.Visit_ID=patient_diagnosis.Pat_Diag_ID and Visit_ID = $reqVisit_ID"; //$reqpatient_ID " ;
	if($in_past_visit_cases == 1)	$vital_sign_sql = $vital_sign_sql . "  and pending != 1 ";
	if($cc != '')					$vital_sign_sql = $vital_sign_sql . " and  Chief_Complaint like '%$cc%' ";
	if(($req_FromDate != '')&&($req_ToDate != '')){
		$vital_sign_sql = $vital_sign_sql . " and (left(visit_date,10) >= '$req_FromDate' and left(visit_date,10) <= '$req_ToDate')";
	}elseif($req_FromDate != ''){
		$vital_sign_sql = $vital_sign_sql . " and  left(visit_date,10) >= $req_FromDate";
	}elseif($req_ToDate != ''){
		$vital_sign_sql = $vital_sign_sql . " and  left(visit_date,10) <= $req_ToDate ";
	}	
	$vital_sign_result = mysql_query($vital_sign_sql) or die(mysql_error()."Couldn't execute query.");
	while ($vital_sign_row = mysql_fetch_array($vital_sign_result)){
		if(($vital_sign_row['height'] >0)||($vital_sign_row['weight'] > 0)||($vital_sign_row['current_bp'] != "")||($vital_sign_row['normal_bp'] != "")||($vital_sign_row['HCin'] > 0)||($vital_sign_row['temp'] != "")||($vital_sign_row['pulse'] > 0)||($vital_sign_row['rr'] > 0)||($vital_sign_row['bmi'] > 0)||($vital_sign_row['physical_exam_note'] != "")){
			$req_Vital_sign = "<table align='left' border='0' cellpadding='1' cellspacing='0' ><tr>";
			if($vital_sign_row['height'] >0){
				$req_Vital_sign .= '<td width="60">';
				if($vital_sign_row['height'] != "") $req_Vital_sign .= "<strong>Height</strong>";
				$req_Vital_sign .= '</td>';
			}
			if($vital_sign_row['weight'] > 0){
				$req_Vital_sign .= '<td width="60" align="center">';
				if($vital_sign_row['weight'] != "") $req_Vital_sign .= "<strong>Weight</strong>";
				$req_Vital_sign .= '</td>';
			}
			if(($vital_sign_row['current_bp'] != "")||($vital_sign_row['normal_bp'] != "")){
				$req_Vital_sign .= '<td width="60" align="center">';
				if(($vital_sign_row['current_bp'] != "")||($vital_sign_row['normal_bp'] != "")) $req_Vital_sign .= "<strong>BP</strong>"; 
				$req_Vital_sign .= '</td>';
			}
			if($vital_sign_row['HCin'] > 0){
				$req_Vital_sign .= '<td width="60" align="center">';
				if($vital_sign_row['HCin'] != "") $req_Vital_sign .= "<strong>HC</strong>"; 
				$req_Vital_sign .= '</td>';
			}
			if($vital_sign_row['temp'] != ""){
				$req_Vital_sign .= '<td width="60" align="center">';
				if($vital_sign_row['temp']) $req_Vital_sign .= "<strong>Temp</strong>"; 
				$req_Vital_sign .= '</td>';
			}
			if($vital_sign_row['pulse'] > 0){
				$req_Vital_sign .= '<td width="60" align="center">';
				if($vital_sign_row['pulse']) $req_Vital_sign .= "<strong>Pulse</strong>"; 
				$req_Vital_sign .= '</td>';
			}
			if($vital_sign_row['rr'] > 0){
				$req_Vital_sign .= '<td width="60" align="center">';
				if($vital_sign_row['rr']) $req_Vital_sign .= "<strong>RR</strong>";
				$req_Vital_sign .= '</td>';
			}
			if($vital_sign_row['bmi'] > 0){
				$req_Vital_sign .= '<td width="60" align="center">';
				if($vital_sign_row['bmi']) $req_Vital_sign .= "<strong>BMI</strong>"; 
				$req_Vital_sign .= '</td>';
			}
			$req_Vital_sign .= '</tr><tr>';
			$req_col_span = 0;
			if($vital_sign_row['height'] >0){
				$req_Vital_sign .= '<td width="60">';
				if($vital_sign_row['height'] != "") $req_Vital_sign .= $vital_sign_row['height']." ".$vital_sign_row['height_type'];
				$req_Vital_sign .= '</td>';
				$req_col_span++;
			}
			if($vital_sign_row['weight'] > 0){
				$req_Vital_sign .= '<td width="60" align="center">';
				if($vital_sign_row['weight'] != "") $req_Vital_sign .= $vital_sign_row['weight']." ".$vital_sign_row['weight_type'];
				$req_Vital_sign .= '</td>';
				$req_col_span++;
			}
			if(($vital_sign_row['current_bp'] != "")||($vital_sign_row['normal_bp'] != "")){
				$req_Vital_sign .= '<td width="60" align="center">';
				if(($vital_sign_row['current_bp'] != "")||($vital_sign_row['normal_bp'] != "")) $req_Vital_sign .= $vital_sign_row['current_bp']."/".$vital_sign_row['normal_bp'];
				$req_Vital_sign .= '</td>';
				$req_col_span++;
			}
			if($vital_sign_row['HCin'] > 0){
				$req_Vital_sign .= '<td width="60" align="center">';
				if($vital_sign_row['HCin'] != "")	$req_Vital_sign .= $vital_sign_row['HCin']." In ";
				$req_Vital_sign .= '</td>';
				$req_col_span++;
			}
			if($vital_sign_row['temp'] != ""){
				$req_Vital_sign .= '<td width="60" align="center">';
				if($vital_sign_row['temp']) $req_Vital_sign .= $vital_sign_row['temp'];
				$req_Vital_sign .= '</td>';
				$req_col_span++;
			}
			if($vital_sign_row['pulse'] > 0){
				$req_Vital_sign .= '<td width="60" align="center">';
				if($vital_sign_row['pulse']) $req_Vital_sign .= $vital_sign_row['pulse'];
				$req_Vital_sign .= '</td>';
				$req_col_span++;
			}
			if($vital_sign_row['rr'] > 0){
				$req_Vital_sign .= '<td width="60" align="center">';
				if($vital_sign_row['rr']) $req_Vital_sign .= $vital_sign_row['rr'];
				$req_Vital_sign .= '</td>';
				$req_col_span++;
			}
			if($vital_sign_row['bmi'] > 0){
				$req_Vital_sign .= '<td width="60" align="center">';
				if($vital_sign_row['bmi']) $req_Vital_sign .= $vital_sign_row['bmi'];
				$req_Vital_sign .= '</td>';
				$req_col_span++;
			}
			$req_Vital_sign .= '</tr>';
			if($vital_sign_row['physical_exam_note'] != ""){
				$req_Vital_sign .= '<tr><td width="60"><strong>VS  Note</strong> </td><td colspan="'.($req_col_span-1).'">'.str_replace("\n", "<BR>", $vital_sign_row['physical_exam_note']);
				$req_Vital_sign .= '</td></tr>';
			}
			$req_Vital_sign .= '</table>';
		}
	}
	return $req_Vital_sign;
}
function pdf_visit_case($Patient_ID,$cc,$req_FromDate,$req_ToDate,$Pat_Diag_ID){
	require_once("includes/pdf_Visit_case_Report_Function.php");
	$patient_diagnosis_sql = "select * from patient_diagnosis where patient_id = $Patient_ID ";
	$tmpblnWhere=1	;
	if($Pat_Diag_ID != ''){
		if ($tmpblnWhere==0){ $patient_diagnosis_sql .=  "where";$tmpblnWhere=1;  }else{	$patient_diagnosis_sql .= " and ";}
		$patient_diagnosis_sql .= " Pat_Diag_ID = $Pat_Diag_ID ";	
	}
	if($cc != ''){
		if ($tmpblnWhere==0){ $patient_diagnosis_sql .=  "where";$tmpblnWhere=1;  }else{	$patient_diagnosis_sql .= " and ";}
		$patient_diagnosis_sql .= " Chief_Complaint like '%$cc%'";	
	}
	if(($req_FromDate != '')&&($req_ToDate != '')){
		if ($tmpblnWhere==0){ $patient_diagnosis_sql .=  "where";$tmpblnWhere=1;  }else{	$patient_diagnosis_sql .= " and ";}
		$patient_diagnosis_sql .= " (visit_date >= '$req_FromDate' and visit_date <= '$req_ToDate') ";
	}elseif($req_FromDate != ''){
		if ($tmpblnWhere==0){ $patient_diagnosis_sql .=  "where";$tmpblnWhere=1;  }else{	$patient_diagnosis_sql .= " and ";}
		$patient_diagnosis_sql .= " visit_date >= $req_FromDate";
	}elseif($req_ToDate != ''){
		if ($tmpblnWhere==0){ $patient_diagnosis_sql .=  "where";$tmpblnWhere=1;  }else{	$patient_diagnosis_sql .= " and ";}
		$patient_diagnosis_sql .= " visit_date <= $req_ToDate";
	}
	$req_SH 		= "";		$req_Allergies 	= "";		$req_Prev_Medn 	= "";
	$patient_diagnosis_sql .= " order by Pat_Diag_ID desc";
	$result = mysql_query($patient_diagnosis_sql) or die($patient_diagnosis_sql); 
	$progres_result = mysql_query($patient_diagnosis_sql)	or die("Couldn't execute query. progress note query");
	$totalreturned = mysql_num_rows($progres_result);
	while ($row = mysql_fetch_array($progres_result)){
		$req_visit_date 			= $row['visit_date'];
		$req_Chief_Complaint		= stripslashes($row['Chief_Complaint']);
		$req_Past_Medical_History	= stripslashes($row['Past_Medical_History']);
		$req_family_history			= stripslashes($row['family_history']);
		$req_social_history			= stripslashes($row['social_history']);
		$req_Allergies				= trim(stripslashes($row['Allergies']));
		$req_Review_Of_System		= stripslashes($row['Review_Of_System']);
		$Assessment			 		= stripslashes($row['Assessment']);
		$Left_Eye_Assessment 		= stripslashes($row['Left_Eye_Assessment']);
		$next_visit 				= stripslashes($row['Next_Visit']);
		$next_visit_days 			= stripslashes($row['Next_Visit_Day']);
		$next_visit_for 			= stripslashes($row['Next_Visit_For_Note']);
		$next_visit_for_to 			= fnFull_Name_By_User_ID($row['Next_Visit_For']);
		$req_Abandon_Note 			= stripslashes($row2['Abandon_Note']);
		if($req_Allergies == "") 			$req_Allergies 				= "patient has no knwon allergies";
		if($req_Past_Medical_History == "") $req_Past_Medical_History 	= "no medical history";
		if($req_family_history == "") 		$req_family_history 		= "no family history";
		if($req_social_history == "") 		$req_social_history 		= "no social history";
		$Pat_Diag_ID = $row['Pat_Diag_ID'];
	}
	$reqVisit_ID 			= $Pat_Diag_ID;
	$select_sql = "select Name,Value from tbl_app_category where Section_Name = 'SNH' order by name desc";
	$select_result = mysql_query($select_sql) or die($select_sql);
	while($select_row = mysql_fetch_array($select_result)){ 
		switch( $select_row['Name']){
			case 'CC' 			: $req_CC 		 = $select_row['Value']; 	break;
			case 'PMH' 			: $req_PMH 		 = $select_row['Value']; 	break;
			case 'FH' 			: $req_FH 		 = $select_row['Value']; 	break;
			case 'SH' 			: $req_SH 		 = $select_row['Value']; 	break;
			case 'Allergies' 	: $req_All		 = $select_row['Value']; 	break;
			case 'Prev_Med' 	: $req_Prev_Medn = $select_row['Value']; 	break;
		}
	}
	if($totalreturned >0){
		$soap_rowspan  =0 ;
		if($req_Past_Medical_History != "")		$soap_rowspan++;
		if($req_social_history != "")			$soap_rowspan++;
		if($req_family_history != "")			$soap_rowspan++;
		if($req_Allergies != "")				$soap_rowspan++;
		if($req_Details != "")					$soap_rowspan++;
 		$select_medication = "select * from medication where Visit_ID = $Pat_Diag_ID and Current_Medication_Plan = 0 order by Med_Id asc";
		$select_medication_result = mysql_query($select_medication)	OR die($select_medication);
		while($select_medication_row = mysql_fetch_array($select_medication_result)){
			$db_medication_Name  		= $select_medication_row['Med_Name'];
			$db_medication_Signature  	= $select_medication_row['Signature'];
			$db_medication_days  		= $select_medication_row['days'];
			$db_medication_No_of_Days  	= $select_medication_row['No_of_Days'];
			$db_medication_eye  		= $select_medication_row['eye'];
			$db_Act_Inact 				= $select_medication_row['Act_Inact'];
			$req_Details .= $db_medication_Name  ." ".$db_medication_Signature  ." ".$db_medication_No_of_Days  ." ".$db_medication_days  ." ".$db_medication_eye."<br />";
		} 
		$htmlcontent 	.= "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td width='40'></td><td>
		<table border=\"0\" cellpadding=\"1\" cellspacing=\"0\"><tr><td colspan=\"3\"><strong>Visit Date : </strong><font size='9'>".sqldateout_Time($req_visit_date)."</font></td></tr>";
		$htmlcontent 	.= '<tr><td  ><strong>'.$req_CC.'</strong></td><td >&nbsp;</td></tr>';
		$htmlcontent 	.= '<tr><td width="30">&nbsp;</td><td width="400"><font size="9">'.str_replace("\n","<br />",$req_Chief_Complaint).'</font></td><td >&nbsp;</td></tr>';
		if($req_Past_Medical_History != ""){
			$htmlcontent 	.= '<tr><td ><strong>'.$req_PMH.'</strong></td></tr>';
			$htmlcontent 	.= '<tr><td width="30">&nbsp;</td><td width="400"><font size="9">'.str_replace("\n","<br />",$req_Past_Medical_History).'</font></td></tr>';
		}
		if($req_social_history != ""){
			$htmlcontent 	.= '<tr><td ><strong>'.$req_SH.'</strong></td></tr>';
			$htmlcontent 	.= '<tr><td width="30">&nbsp;</td><td width="400"><font size="9">'.str_replace("\n","<br />",$req_social_history).'</font></td></tr>';
		}
		if($req_family_history != ""){
			$htmlcontent 	.= '<tr><td ><strong>'.$req_FH.'</strong></td></tr>';
			$htmlcontent 	.= '<tr><td width="30">&nbsp;</td><td width="400"><font size="9">'.str_replace("\n","<br />",$req_family_history).'</font></td></tr>';
		}
		if($req_Allergies != ""){
			$htmlcontent 	.= '<tr><td ><strong>'.$req_All.'</strong></td></tr>';
			$htmlcontent 	.= '<tr><td width="30">&nbsp;</td><td width="400"><font size="9">'.str_replace("\n","<br />",$req_Allergies).'</font></td></tr>';
		}
		if($req_Details != ""){
			$htmlcontent 	.= '<tr><td ><strong>'.$req_Prev_Medn.'</strong></td></tr>';
			$htmlcontent 	.= '<tr><td height="21" width="30">&nbsp;</td><td width="400" colspan="3"><font size="9">'.str_replace("\n","<br />",$req_Details).'</font></td></tr>';
		}
		
		$req_Vital_sign = pdf_vital_sign($reqVisit_ID,$cc,$req_FromDate,$req_ToDate);
		
		if($req_Vital_sign != ""){
			$htmlcontent 	.= '<tr><td ><strong>Vital sign</strong></td></tr>';
			$htmlcontent 	.= '<tr><td width="30">&nbsp;</td><td width="400" colspan="3"><font size="9">'.$req_Vital_sign.'</font></td></tr>';
		}
		
		if($req_Review_Of_System != ""){
			$htmlcontent 	.= '<tr><td ><strong>Observation</strong></td></tr>';
			$htmlcontent 	.= '<tr><td width="30">&nbsp;</td><td width="400" colspan="3"><font size="9">'.str_replace("\n","<br />",$req_Review_Of_System).'</font></td></tr>';
		}
		$req_Intravite_injection 	= fnIntravite_injection_short($reqVisit_ID);
		$req_office_leaser 		 	= fnIn_Office_Laser_Short_Details($reqVisit_ID);
		$req_FFA 					= fnFFA_Short_Details($reqVisit_ID);
		if($req_Intravite_injection != ""){
			$htmlcontent 	.= '<tr><td ><strong>Intravit Injection</strong></td></tr>';
			$htmlcontent 	.= '<tr><td width="30">&nbsp;</td><td width="400" colspan="3"><font size="9">'.$req_Intravite_injection.'</font></td></tr>';
		}
		if($req_office_leaser != ""){
			$htmlcontent 	.= '<tr><td ><strong>In-Office Laser Operative Report</strong></td></tr>';
			$htmlcontent 	.= '<tr><td width="30">&nbsp;</td><td width="400" colspan="3"><font size="9">'.$req_office_leaser.'</font></td></tr>';
		}
		if($req_FFA != ""){
			$htmlcontent 	.= '<tr><td ><strong>Angiographic Report (FFA)</strong></td></tr>';
			$htmlcontent 	.= '<tr><td width="30">&nbsp;</td><td width="400" colspan="3"><font size="9">'.$req_FFA.'</font></td></tr>';
		}
		$req_refraction_str = "";
		$refraction_card_sql 	= 'select `page_no` from tbl_refraction_card, patient_diagnosis where tbl_refraction_card.Visit_ID=patient_diagnosis.Pat_Diag_ID and Visit_ID = '."'$reqVisit_ID'";
		$refraction_card_result = mysql_query($refraction_card_sql) or die($refraction_card_sql);	
		while ($refraction_card_row = mysql_fetch_array($refraction_card_result)){
			$refraction_card_no = $refraction_card_row['page_no'];
			$req_refraction_str .= fnRefractionCard_report($reqVisit_ID,$refraction_card_no,0);
		}
		if($req_refraction_str != ""){
			$htmlcontent 	.= '<tr><td ><strong>Spectacle Refraction</strong></td></tr>';
			$htmlcontent 	.= '<tr><td width="30">&nbsp;</td><td width="400" colspan="3"><font size="9">'.$req_refraction_str.'</font></td></tr>';
		}
		$req_Vision = fnVision($reqVisit_ID);
		if($req_Vision != ""){
			$htmlcontent 	.= '<tr><td ><strong>Vision</strong></td></tr>';
			$htmlcontent 	.= '<tr><td width="30">&nbsp;</td><td width="400" colspan="3"><font size="9">'.$req_Vision.'</font></td></tr>';
		}
		$req_Retinoscopy_str = fnRetinoscopy_print($reqVisit_ID);
		if($req_Retinoscopy_str != ""){
			$htmlcontent 	.= '<tr><td ><strong>Retinoscopy</strong></td></tr>';
			$htmlcontent 	.= '<tr><td width="30">&nbsp;</td><td width="400" colspan="3"><font size="9">'.$req_Retinoscopy_str.'</font></td></tr>';
		}
		$sel_table_sql 		= "select * from `tbl_test_sub_category`,tbl_test_main_category where `tbl_test_sub_category`.Test_Main_Cat_ID = tbl_test_main_category.Test_Main_Cat_ID  and tbl_test_sub_category.`Test_Main_Cat_ID` != 25 and tbl_test_sub_category.`Approve` = 1 and (`Table_name` != 'tbl_concen_optho' || `Table_name` = ''|| `Table_name` = NULL)";
		$report_result 		= mysql_query($sel_table_sql) or die($sel_table_sql);
		while($report_row 	= mysql_fetch_array($report_result)){
			$req_sub_cat_id 	= $report_row['Test_Sub_Cat_ID'];
			$list_page_name = $report_row['Test_Sub_Cat_Note'];
			if ($view[$req_sub_cat_id] == 1)    { 
				$dbTable_Name 	= $report_row['Table_name'];
				$page_name 		= $report_row['Test_Sub_Cat_Name'];
				$tmpVisit_ID = $Pat_Diag_ID;
				if($dbTable_Name != ""){
					if($dbTable_Name == "tbl_angiography_report"){
						$req_right_Eye_Summery .= fnReport_Occular_Test_Summery($page_name,$tmpVisit_ID,$tempVisit_ID,$cc,$req_FromDate,$req_ToDate,"right");
						$req_left_Eye_Summery  .= fnReport_Occular_Test_Summery($page_name,$tmpVisit_ID,$tempVisit_ID,$cc,$req_FromDate,$req_ToDate,"left");
					}else{
						switch($page_name){
							case "Ocular Examination" : 
								$req_right_Eye_Summery .= fnReport_Occular_Summery($tmpVisit_ID,$tempVisit_ID,$cc,$req_FromDate,$req_ToDate,"right");
								$req_left_Eye_Summery  .= fnReport_Occular_Summery($tmpVisit_ID,$tempVisit_ID,$cc,$req_FromDate,$req_ToDate,"left");
								break;
							case "A Scan" : 
								$req_right_Eye_Summery .= fnReport_A_Scan_Summery($tmpVisit_ID,$tempVisit_ID,$cc,$req_FromDate,$req_ToDate,"right");
								$req_left_Eye_Summery  .= fnReport_A_Scan_Summery($tmpVisit_ID,$tempVisit_ID,$cc,$req_FromDate,$req_ToDate,"left");
								break;
							case "OCT" : 
								$req_right_Eye_Summery .= fnReport_OCT_Summery($tmpVisit_ID,$tempVisit_ID,$cc,$req_FromDate,$req_ToDate,"right");
								$req_left_Eye_Summery  .= fnReport_OCT_Summery($tmpVisit_ID,$tempVisit_ID,$cc,$req_FromDate,$req_ToDate,"left");
								break;
						}
					}
				}
			}
		}
		if(($req_right_Eye_Summery != "")||($req_left_Eye_Summery != "")){
			$htmlcontent 	.= '<tr><td ><strong>'.$page_name.'</strong></td></tr>';
			$htmlcontent 	.= '<tr><td width="30">&nbsp;</td><td colspan="3"><font size="9">';
			$htmlcontent 	.= "<table border='0' align='right' cellpadding='0' cellspacing='0'><tr><td width='50'></td>";
			$htmlcontent 	.= "<td colspan='2' align='left'  width='80' valign='top' class='BoldBlack'>RIGHT-EYE</td></tr><tr>";
			$htmlcontent 	.= "<td width='3%' align='left' valign='top' class='BoldBlack'>&nbsp;</td>";
			$htmlcontent 	.= "<td width='87%' align='left'  width='300' valign='top' class='BoldBlack'>".$req_right_Eye_Summery."</td></tr>";
			$htmlcontent 	.= "<tr><td align='left' valign='top' width='50'>&nbsp;</td>";
			$htmlcontent 	.= "<td align='left' valign='top' class='BoldBlack'>&nbsp;</td></tr><tr><td width='7%'></td>";
			$htmlcontent 	.= "<td colspan='2' align='left' valign='top' class='BoldBlack'>LEFT-EYE</td></tr><tr><td width='7%'></td>";
			$htmlcontent 	.= "<td align='left' valign='top'  width='80' class='BoldBlack'>&nbsp;</td>";
			$htmlcontent 	.= "<td align='left' valign='top'  width='300' class='BoldBlack'>".$req_left_Eye_Summery."</td></tr></table>";
			$htmlcontent 	.= '</font></td></tr>';
		}		
		$htmlcontent 	.= "</table></td></tr></table>";
	}
	return $htmlcontent;
}
function fnRefractionCard_report($reqVisit_ID,$refraction_card_no,$req_Show_print_btn){
	$dbTable_Name 	=  'tbl_refraction_card'; 
	$dbTable_ID_Name = 'refra_card_ID';

	$dbField_Name_1  = 'sph_d1';				$dbField_Name_2  = 'sph_n1';
	$dbField_Name_3  = 'cyl_d1';				$dbField_Name_4  = 'cyl_n1';
	$dbField_Name_5  = 'axis_d1';				$dbField_Name_6  = 'axis_n1';
	$dbField_Name_7  = 'sph_d2';				$dbField_Name_8  = 'sph_n2';
	$dbField_Name_9  = 'cyl_d2';				$dbField_Name_10 = 'cyl_n2';
	$dbField_Name_11 = 'axis_d2';				$dbField_Name_12 = 'axis_n2';
	$dbField_Name_13 = 'constant';				$dbField_Name_14 = 'comp_use';
	$dbField_Name_15 = 'Con_same_glasses';		$dbField_Name_16 = 'new_glasses';
	$dbField_Name_17 = 'temp_glasses';			$dbField_Name_18 = 'Patient_ID';
	$dbField_Name_19 = 'Visit_ID';				$dbField_Name_20 = 'TmpVisitID';
	$dbField_Name_21 = 'Refraction_Near';		$dbField_Name_22 = 'Refraction_Bifocal';
	$dbField_Name_23 = 'Refraction_Distance';	$dbField_Name_24 = 'R_V_D'; 
	$dbField_Name_25 = 'R_V_N';					$dbField_Name_26 = 'R_V_D_P';
	$dbField_Name_27 = 'R_V_N_P';				$dbField_Name_28 = 'L_V_D'; 
	$dbField_Name_29 = 'L_V_N'; 				$dbField_Name_30 = 'L_V_D_P';
	$dbField_Name_31 = 'L_V_N_P';				$dbField_Name_33 = 'Note';
	$dbField_Name_34 = 'Print_Card';	
	
	$dbField_Name_35 = 'Add_RE';				$dbField_Name_36 = 'Add_LE';
	$dbField_Name_37 = 'Dist_PD_RE';			$dbField_Name_38 = 'Dist_PD_LE';
	$dbField_Name_39 = 'Near_PD_RE';			$dbField_Name_40 = 'Near_PD_LE';
	$dbField_Name_41 = 'Glass_Resilens';		$dbField_Name_42 = 'White';
	$dbField_Name_43 = 'Polarized';			$dbField_Name_44 = 'sp2';
	$dbField_Name_45 = 'Photo_Brown';		$dbField_Name_46 = 'Trilogy';
	$dbField_Name_47 = 'Pink';				$dbField_Name_48 = 'Photo_Gray';
	$dbField_Name_49 = 'Office_lens';		$dbField_Name_50 = 'AI';
	$dbField_Name_51 = 'ARC';				$dbField_Name_52 = 'Hi_Index';
	$dbField_Name_53 = 'Grey';				$dbField_Name_54 = 'Polycarbonate';
	$dbField_Name_55 = 'Aspheric';			$dbField_Name_56 = 'Brown';
	$dbField_Name_57 = 'Multifocal';$dbRefraction_Field_Name_58 = "Old_New_Card";
	
	$sql = 'select * from '.$dbTable_Name.', patient_diagnosis where '.$dbTable_Name.'.Visit_ID=patient_diagnosis.Pat_Diag_ID and Visit_ID = '."'$reqVisit_ID'";
	if($refraction_card_no != 0)	$sql .=  " and page_no = $refraction_card_no";
	$result = mysql_query($sql) or die($sql);	
	while ($row = mysql_fetch_array($result)){
		$show_refraction = 0;
    	$dbField_Value_1 = trim($row[$dbField_Name_1]);			$dbField_Value_2 = trim($row[$dbField_Name_2]);
		$dbField_Value_3 = trim($row[$dbField_Name_3]);			$dbField_Value_4 = trim($row[$dbField_Name_4]);
		$dbField_Value_5 = trim($row[$dbField_Name_5]);			$dbField_Value_6 = trim($row[$dbField_Name_6]);
		$dbField_Value_7 = trim($row[$dbField_Name_7]);			$dbField_Value_8 = trim($row[$dbField_Name_8]);
		$dbField_Value_9 = trim($row[$dbField_Name_9]);			$dbField_Value_10 = trim($row[$dbField_Name_10]);
		$dbField_Value_11 = trim($row[$dbField_Name_11]);		$dbField_Value_12 = trim($row[$dbField_Name_12]);
		$dbField_Value_13 = trim($row[$dbField_Name_13]);		$dbField_Value_14 = trim($row[$dbField_Name_14]);
		$dbField_Value_15 = trim($row[$dbField_Name_15]);		$dbField_Value_16 = trim($row[$dbField_Name_16]);
		$dbField_Value_17 = trim($row[$dbField_Name_17]);		
		$dbField_Value_21 = trim($row[$dbField_Name_21]);		$dbField_Value_22 = trim($row[$dbField_Name_22]);
		$dbField_Value_23 = trim($row[$dbField_Name_23]);		$dbField_Value_24 = trim($row[$dbField_Name_24]);
		$dbField_Value_25 = trim($row[$dbField_Name_25]);		$dbField_Value_26 = trim($row[$dbField_Name_26]);
		$dbField_Value_27 = trim($row[$dbField_Name_27]);		$dbField_Value_28 = trim($row[$dbField_Name_28]);
		$dbField_Value_29 = trim($row[$dbField_Name_29]);		$dbField_Value_30 = trim($row[$dbField_Name_30]);
		$dbField_Value_31 = trim($row[$dbField_Name_31]);		$dbField_Value_33 = trim($row[$dbField_Name_33]);
		$dbField_Value_34 = trim($row[$dbField_Name_34]);
		$dbField_Value_35 = trim($row[$dbField_Name_35]);			$dbField_Value_36 = trim($row[$dbField_Name_36]);
		$dbField_Value_37 = trim($row[$dbField_Name_37]);			$dbField_Value_38 = trim($row[$dbField_Name_38]);
		$dbField_Value_39 = trim($row[$dbField_Name_39]);			$dbField_Value_40 = trim($row[$dbField_Name_40]);
		$dbField_Value_41 = trim($row[$dbField_Name_41]);			$dbField_Value_42 = trim($row[$dbField_Name_42]);
		$dbField_Value_43 = trim($row[$dbField_Name_43]);			$dbField_Value_44 = trim($row[$dbField_Name_44]);
		$dbField_Value_45 = trim($row[$dbField_Name_45]);			$dbField_Value_46 = trim($row[$dbField_Name_46]);
		$dbField_Value_47 = trim($row[$dbField_Name_47]);			$dbField_Value_48 = trim($row[$dbField_Name_48]);
		$dbField_Value_49 = trim($row[$dbField_Name_49]);			$dbField_Value_50 = trim($row[$dbField_Name_50]);
		$dbField_Value_51 = trim($row[$dbField_Name_51]);			$dbField_Value_52 = trim($row[$dbField_Name_52]);
		$dbField_Value_53 = trim($row[$dbField_Name_53]);			$dbField_Value_54 = trim($row[$dbField_Name_54]);
		$dbField_Value_55 = trim($row[$dbField_Name_55]);			$dbField_Value_56 = trim($row[$dbField_Name_56]);
		$dbField_Value_57 = $row["Multifocal"];
		
		if($dbField_Value_1 != "") $show_refraction = 1;		if($dbField_Value_2 != "") $show_refraction = 1;
		if($dbField_Value_3 != "") $show_refraction = 1;		if($dbField_Value_4 != "") $show_refraction = 1;
		if($dbField_Value_5 != "") $show_refraction = 1;		if($dbField_Value_6 != "") $show_refraction = 1;
		if($dbField_Value_7 != "") $show_refraction = 1;		if($dbField_Value_8 != "") $show_refraction = 1;
		if($dbField_Value_9 != "") $show_refraction = 1;		if($dbField_Value_10 != "") $show_refraction = 1;
		if($dbField_Value_11 != "") $show_refraction = 1;		if($dbField_Value_12 != "") $show_refraction = 1;
		if($dbField_Value_13 != "") $show_refraction = 1;		if($dbField_Value_14 != "") $show_refraction = 1;
		if($dbField_Value_15 != "") $show_refraction = 1;		if($dbField_Value_16 != "") $show_refraction = 1;
		if($dbField_Value_17 != "") $show_refraction = 1;		if($dbField_Value_18 != "") $show_refraction = 1;

		if($dbField_Value_19 != "") $show_refraction = 1;		if($dbField_Value_20 != "") $show_refraction = 1;
		if($dbField_Value_21 != "") $show_refraction = 1;		if($dbField_Value_22 != "") $show_refraction = 1;
		if($dbField_Value_23 != "") $show_refraction = 1;		if($dbField_Value_24 != "") $show_refraction = 1;
		if($dbField_Value_25 != "") $show_refraction = 1;		if($dbField_Value_26 != "") $show_refraction = 1;
		if($dbField_Value_27 != "") $show_refraction = 1;		if($dbField_Value_28 != "") $show_refraction = 1;
		if($dbField_Value_29 != "") $show_refraction = 1;		if($dbField_Value_30 != "") $show_refraction = 1;
		if($dbField_Value_31 != "") $show_refraction = 1;		if($dbField_Value_32 != "") $show_refraction = 1;
		if($dbField_Value_33 != "") $show_refraction = 1;	
		if($req_Old_New != "Old"){
			//if($_SESSION["Short_Refraction_Card"] == 1){
				if($dbField_Value_35 != ""){
					$dbField_Value_2 = $row[$dbField_Name_1] + $dbField_Value_35;
					$req_Array_value_1 =  0;
					$req_Array_value_2 =  0;
					$req_Array_value_3 =  0;
					$req_point_present =  0;
					if($dbField_Value_1 != "")	$req_Array_value_1 = count(split("\.",$dbField_Value_1));
					if($dbField_Value_2 != "")	$req_Array_value_2 = count(split("\.",$dbField_Value_2));
					if($dbField_Value_35 != "")	$req_Array_value_3 = count(split("\.",$dbField_Value_35));
					if(($req_Array_value_1 > 1) && ($req_Array_value_2 < 2)){
						$dbField_Value_2 = $dbField_Value_2 . ".00";
						$req_point_present = 1;
					}
					if(($req_point_present == 0)&&($req_Array_value_3 > 1)&&($req_Array_value_2 < 2)&&($dbField_Value_2 > 0))	$dbField_Value_2 = $dbField_Value_2 . ".00";
					if ($dbField_Value_2 > 0) $dbField_Value_2 = "+".$dbField_Value_2;
					$dbField_Value_10 = $row[$dbField_Name_9];
					$dbField_Value_4 = $row[$dbField_Name_3];
				}
				if($dbField_Value_36 != ""){
					$dbField_Value_6 = $row[$dbField_Name_5];
					$dbField_Value_8 = $row[$dbField_Name_7] + $dbField_Value_36;
					$req_Array_value_1 =  0;
					$req_Array_value_2 =  0;
					$req_Array_value_3 =  0;
					$req_point_present =  0;
					if($dbField_Value_7 != "")	$req_Array_value_1 = count(split("\.",$dbField_Value_7));
					if($dbField_Value_8 != "")	$req_Array_value_2 = count(split("\.",$dbField_Value_8));
					if($dbField_Value_36 != "")	$req_Array_value_3 = count(split("\.",$dbField_Value_36));
					if(($req_Array_value_1 > 1 )&&($req_Array_value_2 < 2)){
						$dbField_Value_8 = $dbField_Value_8 . ".00";
						$req_point_present = 1;
					}
					if(($req_point_present == 0)&&($req_Array_value_3 > 1)&&($req_Array_value_2 < 2)&&($dbField_Value_8 > 0))	$dbField_Value_8 = $dbField_Value_8 . ".00";
					if ($dbField_Value_8 > 0) $dbField_Value_8 = "+".$dbField_Value_8;
					$dbField_Value_12 = $row[$dbField_Name_11];
				}
			//}	
		}
		$req_Old_New = 	$row[$dbRefraction_Field_Name_58];
		if($show_refraction == 1){
			if($req_Old_New != "Old"){
				$req_str .= '<table cellpadding="0" cellspacing="0"><tr><td align="center" valign="top">
					<table border="0" align="left" cellpadding="0" cellspacing="0">';
				$req_str .= '<tr><td rowspan="2" align="center"  width="40" class="WhiteBG">&nbsp;</td>
					<td colspan="5" width="200" align="center" >RE</td>
					<td width="20" >&nbsp;</td>
					<td colspan="5" width="200" align="center" >LE</td></tr><tr>
					<td width="40" align="center" >Sph</td>
					<td width="40" align="center" >Cyl</td>
					<td width="40" align="center" >Axis</td>
					<td width="80"  align="center" >Vision</td>
					<td width="20" >&nbsp;</td>
					<td width="40" align="center" >Sph</td>
					<td width="40" align="center" >Cyl</td>
					<td width="40" align="center" >Axis</td>
					<td  width="80" align="center" >Vision</td></tr>';
				$req_str .= '<tr align="center" class="WhiteBG"><td width="40"><strong>D</strong></td>';
				$req_str .= '<td width="40">'.$dbField_Value_1.'</td><td width="40">'.$dbField_Value_3.'</td><td width="40">'.$dbField_Value_5.'</td>';
				$req_str .= '<td width="60">'.$dbField_Value_24.'</td>';
				$req_str .= '<td width="20">'.$dbField_Value_26.'</td><td width="20">&nbsp;</td>';
				$req_str .= '<td width="40">'.$dbField_Value_7.'</td><td width="40"> '.$dbField_Value_9.'</td><td width="40">'.$dbField_Value_11.'</td>';
				$req_str .= '<td width="60">'.$dbField_Value_28.'</td><td width="20">'.$dbField_Value_30.'</td>';
				$req_str .= '</tr><tr align="center" class="WhiteBG">';
				$req_str .= '<td width="40" ><strong>N</strong></td>';
				$req_str .= '<td width="40">'.$dbField_Value_2.'</td>';
				$req_str .= '<td width="40">'.$dbField_Value_4.'</td><td width="40">'.$dbField_Value_6.'</td><td width="60">'.$dbField_Value_25.'</td>';
				$req_str .= '<td width="20">'.$dbField_Value_27.'</td><td width="20">&nbsp;</td>';
				$req_str .= '<td width="40">'.$dbField_Value_8.'</td>';
				$req_str .= '<td width="40">'.$dbField_Value_10.'</td><td width="40">'.$dbField_Value_12.'</td><td width="60">'.$dbField_Value_29.'</td>';
				$req_str .= '<td width="20">'.$dbField_Value_31.'</td>';
				$req_str .= '</tr>';
				if(($dbField_Value_37 != "")||($dbField_Value_38 != "")||($dbField_Value_39 != "")||($dbField_Value_40 != "")){
					$req_str .= '<tr>
					<td class="WhiteBG" colspan="12" >
						<table border="0" cellspacing="0" cellpadding="0" ><tr>';
					$req_str .= '<td width="60"><strong>Dist PD </strong></td>';
					$req_str .= '<td width="20">RE</td>';
					$req_str .= '<td width="50"> :  '.$dbField_Value_37.'</td>';
					$req_str .= '<td width="20">LE </td>';
					$req_str .= '<td width="50">: '.$dbField_Value_38.'</td><td>&nbsp;</td>';
					$req_str .= '<td class="WhiteBG" width="60"><strong>Near PD </strong></td>';
					$req_str .= '<td width="20">  RE  </td>';
					$req_str .= '<td width="50"> :  '.$dbField_Value_39.'</td>';
					$req_str .= '<td width="20"> LE </td>';
					$req_str .= '<td width="50"> :  '.$dbField_Value_40.'</td><td>&nbsp;</td>';
					$req_str .= '</tr></table></td></tr>';
				}
				if (($dbField_Value_13 == 'CONSTANT')||($dbField_Value_21 == 'NEAR')||($dbField_Value_22 == 'BIFOCAL')||($dbField_Value_23 == 'DISTANCE')) {
					$req_str .= '<tr><td colspan="12" align="center" class="WhiteBG"><table border="0" cellpadding="0" cellspacing="0"><tr>';
					if ($dbField_Value_13 =='CONSTANT') $req_str .= '<td width="91">CONSTANT</td>';
					if ($dbField_Value_21 =='NEAR') 	$req_str .= '<td width="108">NEAR </td>';
					if ($dbField_Value_22 =='BIFOCAL') 	$req_str .= '<td width="95">BIFOCAL</td>';
					if ($dbField_Value_23 =='DISTANCE') $req_str .= '<td width="214">DISTANCE</td>';
					$req_str .= '</tr></table></td></tr>';	
				}	
				if($dbField_Value_57 != ""){
					$req_str .= '<tr><td class="WhiteBG" width="60"><strong>Multifocals</strong></td><td colspan="11" class="WhiteBG"><strong> :</strong> '.$dbField_Value_57.'</td></tr>';
				}
				if (($dbField_Value_41 =='1')||($dbField_Value_42 =='1')||($dbField_Value_43 =='1')||($dbField_Value_54 =='1')||($dbField_Value_42 =='1')||($dbField_Value_45 =='1')||($dbField_Value_46 =='1')||($dbField_Value_55 =='1')||($dbField_Value_57 =='1')||($dbField_Value_48 =='1')||($dbField_Value_49 =='1')||($dbField_Value_56 =='1')||($dbField_Value_50 =='1')||($dbField_Value_51 =='1')||($dbField_Value_52 =='1')||($dbField_Value_53 =='1')){
					$req_str .= '<tr><td class="WhiteBG" width="60"><strong>'.$dbField_Value_41.'</strong></td>
					<td colspan="11" class="WhiteBG"><strong>:</strong> ';
					if ($dbField_Value_42 =='1') $req_str .= "WHITE, ";
					if ($dbField_Value_43 =='1') $req_str .=  "POLARIZED, ";
					if ($dbField_Value_54 =='1') $req_str .=  "POLYCARBONATE, ";
					if ($dbField_Value_42 =='1') $req_str .=  "SP2, ";
					if ($dbField_Value_45 =='1') $req_str .=  "PHOTO BROWN, ";
					if ($dbField_Value_46 =='1') $req_str .=  "TRILOGY, ";
					if ($dbField_Value_55 =='1') $req_str .=  "ASPHERIC, ";
					if ($dbField_Value_57 =='1') $req_str .=  "PINK, ";
					if ($dbField_Value_48 =='1') $req_str .=  "PHOTO GRAY, ";
					if ($dbField_Value_49 =='1') $req_str .=  "OFFICE LENS, ";
					if ($dbField_Value_56 =='1') $req_str .=  "BROWN, ";
					if ($dbField_Value_50 =='1') $req_str .=  "AI, ";
					if ($dbField_Value_51 =='1') $req_str .=  "ARC, ";
					if ($dbField_Value_52 =='1') $req_str .=  "HI-INDEX, ";
					if ($dbField_Value_53 =='1') $req_str .=  "GREY, ";
					$req_str .= '</td></tr>';
				}
				if($dbField_Value_33 != ""){
					$req_str .= '<tr class="WhiteBG"><td class="WhiteBG"  width="60"><strong>Instructions </strong></td><td colspan="11"><strong>: </strong>'.str_replace("\n","<BR>",$dbField_Value_33).'</td></tr>';
				}
				$req_str .= '</table></td></tr></table>';	
			}else{
				$req_str .= '<table width="100%" cellpadding="0" cellspacing="0"><tr><td align="center" valign="top">';
				$req_str .= '<table width="80%" border="1" align="left" cellpadding="2" cellspacing="0" bordercolor="#666666" style="border-collapse:collapse">';				
				$req_str .= '<tr>';
				$req_str .= '<td width="5%" rowspan="2" align="center" class="WhiteBG">&nbsp;</td>';
				$req_str .= '<td height="20" colspan="5" align="center" ><strong>RE</strong></td>';
				$req_str .= '<td colspan="5" align="center" ><strong>LE</strong></td>';
				$req_str .= '</tr><tr>';
				$req_str .= '<td width="9%" align="center" class="comments2"><strong>Sph</strong></td>';
				$req_str .= '<td width="9%" align="center" class="comments2"><strong>Cyl</strong></td>';
				$req_str .= '<td width="9%" align="center" class="comments2"><strong>Axis</strong></td>';
				$req_str .= '<td width="9%"  align="center" class="comments2"><strong>Vision</strong></td>';
				$req_str .= '<td width="9%" align="center" class="comments2"><strong>Sph</strong></td>';
				$req_str .= '<td width="9%" align="center" class="comments2"><strong>Cyl</strong></td>';
				$req_str .= '<td width="9%" align="center" class="comments2"><strong>Axis</strong></td>';
				$req_str .= '<td width="9%"  align="center" class="comments2"><strong>Vision</strong></td>';
				$req_str .= '</tr><tr align="center" class="WhiteBG">';
				$req_str .= '<td height="22" class="comments2"><strong>D</strong></td>';
				$req_str .= '<td><?=$dbField_Value_1?></td><td><?=$dbField_Value_3?></td><td><?=$dbField_Value_5?></td>';
				$req_str .= '<td width="9%"><?=$dbField_Value_24?></td>';
				$req_str .= '<td width="9%"><?=$dbField_Value_26?></td>';
				$req_str .= '<td><?=$dbField_Value_7?></td><td><?=$dbField_Value_9?></td><td><?=$dbField_Value_11?></td>';
				$req_str .= '<td><?=$dbField_Value_28?></td><td><?=$dbField_Value_30?></td>';
				$req_str .= '</tr><tr align="center" class="WhiteBG">';
				$req_str .= '<td height="22" class="comments2"><strong>N</strong></td>';
				$req_str .= '<td>'; 
				if ($dbField_Value_2<>""){ 
					$req_str .= (($dbField_Value_22=="BIFOCAL")?"Add ":""); 
				} 
				$req_str .= $dbField_Value_2;
				$req_str .= '</td>';
				$req_str .= '<td>'.$dbField_Value_4.'</td><td>'.$dbField_Value_6.'</td><td>'.$dbField_Value_25.'</td>';
				$req_str .= '<td>'.$dbField_Value_27.'</td>';
				$req_str .= '<td>';
				if ($dbField_Value_8<>""){ 
					$req_str .= (($dbField_Value_22=="BIFOCAL")?"Add ":"");
				}
				$req_str .= $dbField_Value_8;
				$req_str .= '</td>';
				$req_str .= '<td>'.$dbField_Value_10.'</td><td>'.$dbField_Value_12.'</td><td>'.$dbField_Value_29.'</td>';
				$req_str .= '<td width="9%">'.$dbField_Value_31.'</td></tr>'; 
				if (($dbField_Value_13 == 'CONSTANT')||($dbField_Value_21 == 'NEAR')||($dbField_Value_22 == 'BIFOCAL')||($dbField_Value_23 == 'DISTANCE')) {
					$req_str .= '<tr><td colspan="10" ><table width="100%" border="0" cellpadding="0" cellspacing="1"><tr>'; 
					if ($dbField_Value_13 =='CONSTANT') {
						$req_str .= '<td width="91"  >CONSTANT</td>';
					}
					if ($dbField_Value_21 =='NEAR') {
						$req_str .= '<td width="108" >NEAR</td>';
					}
					if ($dbField_Value_22 =='BIFOCAL') {
						$req_str .= '<td width="95" >BIFOCAL</td>';
					}
					if ($dbField_Value_23 =='DISTANCE') {
						$req_str .= '<td width="214" >DISTANCE</td>';
					}
					$req_str .= '</tr></table></td></tr>';
				}	
				if(($dbField_Value_14 != "")||($dbField_Value_15 != "")||($dbField_Value_16 != "")||($dbField_Value_17 != "")){ 
					$req_str .= '<tr><td colspan="10" align="center" class="WhiteBG"><table width="100%" border="0" cellpadding="0" cellspacing="0">';
					if($dbField_Value_14=="Computer use"){
						$req_str .= '<tr><td width="72%" >&nbsp;</td><td width="28%" >'.$dbField_Value_14.'</td></tr>';
					}
					if($dbField_Value_15=="Continue same glasses"){
						$req_str .= '<tr><td >&nbsp;</td><td >'.$dbField_Value_15.'</td></tr>';
					}	
					if($dbField_Value_16=="New glasses"){
						$req_str .= '<tr><td >&nbsp;</td><td >'.$dbField_Value_16.'</td></tr>'; 
					}
					if($dbField_Value_17=="Temporary glasses"){
						$req_str .= '<tr><td >&nbsp;</td><td >'.$dbField_Value_17.'</td></tr>'; 
					}
					$req_str .= '</table></td></tr>';
				}
				$req_str .= '<tr class="WhiteBG"><td colspan="3" align="center"><strong>Note : </strong></td><td colspan="9">'.str_replace("\n","<BR>",$dbField_Value_33).'</td></tr></table></td></tr></table>';	
			}
		}
	}
	return $req_str;
}
function fnVision($reqVisit_ID){
	$db_Vision_Table_Name    = 'tbl_e_ocular_vision'; 
	$db_Vision_Table_ID_Name = 'OpthoRpt1_ID';	
	
	$db_Vision_Field_Name_1  = "R_Vision_Dist_With_Glass";			$db_Vision_Field_Name_2  = "R_Vision_Near_With_Glass";
	$db_Vision_Field_Name_3  = "L_Vision_Dist_With_Glass";			$db_Vision_Field_Name_4  = "L_Vision_Near_With_Glass";
	$db_Vision_Field_Name_5  = "R_Vision_Dist_Without_Glass";		$db_Vision_Field_Name_6  = "R_Vision_Near_Without_Glass";
	$db_Vision_Field_Name_7  = "L_Vision_Dist_Without_Glass";		$db_Vision_Field_Name_8  = "L_Vision_Near_Without_Glass";
	$db_Vision_Field_Name_9  = "R_Vision_Pin_Hole";					$db_Vision_Field_Name_10 = "L_Vision_Pin_Hole";
	$db_Vision_Field_Name_11 = "R_Vision_IOP";						$db_Vision_Field_Name_12 = "L_Vision_IOP";
	
	$db_Vision_Field_Name_13 = "R_Vision_d_p_gl";					$db_Vision_Field_Name_14 = "R_Vision_d_p_wogl";
	$db_Vision_Field_Name_15 = "R_Vision_N_p_gl";					$db_Vision_Field_Name_16 = "R_Vision_N_p_wgl";
	$db_Vision_Field_Name_17 = "L_Vision_d_p_gl";					$db_Vision_Field_Name_18 = "L_Vision_d_p_wogl";
	$db_Vision_Field_Name_19 = "L_Vision_N_p_gl";					$db_Vision_Field_Name_20 = "L_Vision_N_p_wogl";
	
	$db_Vision_Field_Name_21 = "Patient_ID";						$db_Vision_Field_Name_22 = "Visit_ID";
	$db_Vision_Field_Name_23 = "TmpVisitID"; 
	
	$db_Vision_Field_Name_24  = "R_Vision_Dist_Aftr_dila";			$db_Vision_Field_Name_25  = "R_Vision_Near_Aftr_dila";
	$db_Vision_Field_Name_26  = "L_Vision_Dist_Aftr_dila";			$db_Vision_Field_Name_27  = "L_Vision_Near_Aftr_dila";
	$db_Vision_Field_Name_28  = "R_Vision_d_p_Aftr_dila";			$db_Vision_Field_Name_29  = "R_Vision_N_p_Aftr_dila";
	$db_Vision_Field_Name_30  = "L_Vision_d_p_Aftr_dila";			$db_Vision_Field_Name_31  = "L_Vision_N_p_Aftr_dila";
	$db_Vision_Field_Name_32  = "remark";							$db_Vision_Field_Name_33 = 'L_remark';
	$db_Vision_Field_Name_34 = 'R_SPH';								$db_Vision_Field_Name_35 = 'L_SPH';			
	$db_Vision_Field_Name_36 = 'R_CYL';								$db_Vision_Field_Name_37 = 'L_CYL';			
	$db_Vision_Field_Name_38 = 'R_AXIS';							$db_Vision_Field_Name_39 = 'L_AXIS';		
	$db_Vision_Field_Name_40 = 'R_ADD';								$db_Vision_Field_Name_41 = 'L_ADD';
	$db_Vision_Field_Name_42 = 'Old_New_Card';
	$db_Vision_Field_Name_43 = 'R_Vision_IOP_Applanation';			$db_Vision_Field_Name_44 = 'L_Vision_IOP_Applanation';
	$returnstr="";
	$sql = 'select * from '.$db_Vision_Table_Name.', patient_diagnosis where '.$db_Vision_Table_Name.'.Visit_ID=patient_diagnosis.Pat_Diag_ID and Visit_ID = '."'$reqVisit_ID'";
	$result = mysql_query($sql) or die('Couldnt execute query.<br>'.$sql."<br>".mysql_error());
	while ($vision_row = mysql_fetch_array($result)){
		$db_Vision_Field_Value_1 	= $vision_row["$db_Vision_Field_Name_1"];		$db_Vision_Field_Value_2 	= $vision_row["$db_Vision_Field_Name_2"];
		$db_Vision_Field_Value_3 	= $vision_row["$db_Vision_Field_Name_3"];		$db_Vision_Field_Value_4 	= $vision_row["$db_Vision_Field_Name_4"];
		$db_Vision_Field_Value_5 	= $vision_row["$db_Vision_Field_Name_5"];		$db_Vision_Field_Value_6 	= $vision_row["$db_Vision_Field_Name_6"];
		$db_Vision_Field_Value_7 	= $vision_row["$db_Vision_Field_Name_7"];		$db_Vision_Field_Value_8 	= $vision_row["$db_Vision_Field_Name_8"];
		$db_Vision_Field_Value_9 	= $vision_row["$db_Vision_Field_Name_9"];		$db_Vision_Field_Value_10 	= $vision_row["$db_Vision_Field_Name_10"];
		$db_Vision_Field_Value_11 	= $vision_row["$db_Vision_Field_Name_11"];		$db_Vision_Field_Value_12 	= $vision_row["$db_Vision_Field_Name_12"];
		$db_Vision_Field_Value_13 	= $vision_row["$db_Vision_Field_Name_13"];		$db_Vision_Field_Value_14 	= $vision_row["$db_Vision_Field_Name_14"];
		$db_Vision_Field_Value_15 	= $vision_row["$db_Vision_Field_Name_15"];		$db_Vision_Field_Value_16 	= $vision_row["$db_Vision_Field_Name_16"];
		$db_Vision_Field_Value_17 	= $vision_row["$db_Vision_Field_Name_17"];		$db_Vision_Field_Value_18 	= $vision_row["$db_Vision_Field_Name_18"];
		$db_Vision_Field_Value_19 	= $vision_row["$db_Vision_Field_Name_19"];		$db_Vision_Field_Value_20 	= $vision_row["$db_Vision_Field_Name_20"];
		$db_Vision_Field_Value_21 	= $vision_row["$db_Vision_Field_Name_21"];		$db_Vision_Field_Value_24 	= $vision_row["$db_Vision_Field_Name_24"];
		$db_Vision_Field_Value_25 	= $vision_row["$db_Vision_Field_Name_25"];		$db_Vision_Field_Value_26 	= $vision_row["$db_Vision_Field_Name_26"];
		$db_Vision_Field_Value_27 	= $vision_row["$db_Vision_Field_Name_27"];		$db_Vision_Field_Value_28 	= $vision_row["$db_Vision_Field_Name_28"];
		$db_Vision_Field_Value_29 	= $vision_row["$db_Vision_Field_Name_29"];		$db_Vision_Field_Value_30 	= $vision_row["$db_Vision_Field_Name_30"];
		$db_Vision_Field_Value_31 	= $vision_row["$db_Vision_Field_Name_31"];		$db_Vision_Field_Value_32 	= $vision_row["$db_Vision_Field_Name_32"];
		$db_Vision_Field_Value_33 	= $vision_row["$db_Vision_Field_Name_33"];		$db_Vision_Field_Value_34 	= $vision_row["$db_Vision_Field_Name_34"];
		$db_Vision_Field_Value_35 	= $vision_row["$db_Vision_Field_Name_35"];		$db_Vision_Field_Value_36 	= $vision_row["$db_Vision_Field_Name_36"];
		$db_Vision_Field_Value_37 	= $vision_row["$db_Vision_Field_Name_37"];		$db_Vision_Field_Value_38 	= $vision_row["$db_Vision_Field_Name_38"];
		$db_Vision_Field_Value_39 	= $vision_row["$db_Vision_Field_Name_39"];		$db_Vision_Field_Value_40 	= $vision_row["$db_Vision_Field_Name_40"];
		$db_Vision_Field_Value_41 	= $vision_row["$db_Vision_Field_Name_41"];		$db_Vision_Field_Value_42 	= $vision_row["$db_Vision_Field_Name_42"];
		$db_Vision_Field_Value_43 	= $vision_row["$db_Vision_Field_Name_43"];		$db_Vision_Field_Value_44 	= $vision_row["$db_Vision_Field_Name_44"];
		

		if((trim($db_Vision_Field_Value_1) != "")||(trim($db_Vision_Field_Value_2) != "")||(trim($db_Vision_Field_Value_3) != "")||
		(trim($db_Vision_Field_Value_4) != "")||(trim($db_Vision_Field_Value_5) != "")||(trim($db_Vision_Field_Value_6) != "")||(trim($db_Vision_Field_Value_7) != "")||
		(trim($db_Vision_Field_Value_8) != "")||(trim($db_Vision_Field_Value_9) != "")||(trim($db_Vision_Field_Value_10)!= "")||(trim($db_Vision_Field_Value_11)!= "")||
		(trim($db_Vision_Field_Value_12)!= "")||(trim($db_Vision_Field_Value_13)!= "")||(trim($db_Vision_Field_Value_14)!= "")||(trim($db_Vision_Field_Value_15)!= "")||
		(trim($db_Vision_Field_Value_16)!= "")||(trim($db_Vision_Field_Value_17)!= "")||(trim($db_Vision_Field_Value_19)!= "")||(trim($db_Vision_Field_Value_20)!= "")||
		(trim($db_Vision_Field_Value_34)!= "")||(trim($db_Vision_Field_Value_35)!= "")||(trim($db_Vision_Field_Value_36)!= "")||(trim($db_Vision_Field_Value_37)!= "")||
		(trim($db_Vision_Field_Value_38)!= "")||(trim($db_Vision_Field_Value_39)!= "")||(trim($db_Vision_Field_Value_40)!= "")||(trim($db_Vision_Field_Value_41)!= "")||
		(trim($db_Vision_Field_Value_43)!= "")||(trim($db_Vision_Field_Value_44)!= "")){	
		
		  	$returnstr="<table border='0' align='left' cellpadding='1' cellspacing='1'>
			<tr><td align='left' valign='top' ><table border='0' cellpadding='1' cellspacing='1'>"; 
			$returnstr .="<tr>
			<td width='65' valign='top'><strong></strong></td>
			<td colspan='2' width='165' align='center' valign='top'>RE</td>
			<td width='20'> &nbsp;</td>
			<td colspan='2' width='140' align='center' valign='top'>LE</td></tr>";
			if($db_Vision_Field_Value_42 != "Old"){
				if((trim($db_Vision_Field_Value_1)!= "")||(trim($db_Vision_Field_Value_2)!= "")||(trim($db_Vision_Field_Value_13)!= "")||(trim($db_Vision_Field_Value_15)!= "")||(trim($db_Vision_Field_Value_34)!= "")||(trim($db_Vision_Field_Value_36)!= "")||(trim($db_Vision_Field_Value_38)!= "")||(trim($db_Vision_Field_Value_40)!= "")||(trim($db_Vision_Field_Value_35)!= "")||(trim($db_Vision_Field_Value_37)!= "")||(trim($db_Vision_Field_Value_39)!= "")||(trim($db_Vision_Field_Value_3)!= "")||(trim($db_Vision_Field_Value_4)!= "")||(trim($db_Vision_Field_Value_17)!= "")||(trim($db_Vision_Field_Value_41)!= "")||(trim($db_Vision_Field_Value_19)!= "")){
					$req_add_value_1 = $db_Vision_Field_Value_34 + $db_Vision_Field_Value_40;
					$req_Array_value_1 =  0;
					$req_Array_value_2 =  0;
					$req_Array_value_3 =  0;
					$req_point_present =  0;
					if($db_Vision_Field_Value_34 != "")	$req_Array_value_1 = count(split("\.",$db_Vision_Field_Value_34));
					if($req_add_value_1 != "")			$req_Array_value_2 = count(split("\.",$req_add_value_1));
					if($db_Vision_Field_Value_40 != "")	$req_Array_value_3 = count(split("\.",$db_Vision_Field_Value_40));
	
					if(($req_Array_value_1 > 1 )&&($req_Array_value_2 < 2)){
						$req_add_value_1 = $req_add_value_1 . ".00";
						$req_point_present =  1;
					}
	
					if(($req_point_present == 0)&&($req_Array_value_3 > 1)&&($req_Array_value_2 < 2)&&($req_add_value_1 > 0))	$req_add_value_1 = $req_add_value_1 . ".00";
					if($req_add_value_1 > 0) $req_add_value_1 = "+".$req_add_value_1;
					
					$req_add_value_2 = $db_Vision_Field_Value_35 + $db_Vision_Field_Value_41;
					$req_Array_value_1 =  0;
					$req_Array_value_2 =  0;
					if($db_Vision_Field_Value_35 != "")	$req_Array_value_1 = count(split("\.",$db_Vision_Field_Value_35));
					if($req_add_value_2 != "")			$req_Array_value_2 = count(split("\.",$req_add_value_2));
					if($db_Vision_Field_Value_41 != "")	$req_Array_value_3 = count(split("\.",$db_Vision_Field_Value_41));
					if(($req_Array_value_1 > 1 )&&($req_Array_value_2 < 2)){	
						$req_add_value_2 = $req_add_value_2 . ".00";
						$req_point_present =  1;
					}
					if(($req_point_present == 0)&&($req_Array_value_3 > 1)&&($req_Array_value_2 < 2)&&($req_add_value_2 > 0))	$req_add_value_2 = $req_add_value_2 . ".00";
					if($req_add_value_2 > 0) $req_add_value_2 = "+".$req_add_value_2;
					
					$returnstr .= "<tr><td width='65'><strong>With gl	</strong></td>";
					$returnstr .= "<td height='14' width='165' align='center' colspan='2'  >";
					if((trim($db_Vision_Field_Value_1)!= "")||(trim($db_Vision_Field_Value_2)!= "")||(trim($db_Vision_Field_Value_13)!= "")||(trim($db_Vision_Field_Value_15)!= "")||(trim($db_Vision_Field_Value_34)!= "")||(trim($db_Vision_Field_Value_36)!= "")||(trim($db_Vision_Field_Value_38)!= "")||(trim($db_Vision_Field_Value_40)!= "")){
						$returnstr .= "<table border='0' cellspacing='1' cellpadding='2' ><tr align='center'>
						<td align='center' width='25'>&nbsp;</td>
						<td align='center' width='30'>Sph</td>
						<td align='center' width='30'>Cyl</td>
						<td align='center' width='30'>Axis</td>
						<td align='center' colspan='2' width='50'>Vision</td></tr>";
						$returnstr .= "<tr align='center' width='25'><td align='center' width='25'>Dist</td>
						<td align='center' width='30'>".$db_Vision_Field_Value_34."</td>
						<td align='center' width='30'>".$db_Vision_Field_Value_36."</td>
						<td align='center' width='30'>".$db_Vision_Field_Value_38."</td>
						<td align='center' width='30'>".$db_Vision_Field_Value_1."</td>
						<td align='center' width='20'>".$db_Vision_Field_Value_13."</td></tr>";
						if(($db_Vision_Field_Value_40 != "")||(trim($db_Vision_Field_Value_2) != "")){ 
							$returnstr .= "<tr>
							<td align='center' width='25'>Near</td>
							<td align='center' width='30'>".$req_add_value_1."</td>
							<td align='center' width='30'>".$db_Vision_Field_Value_36."</td>
							<td align='center' width='30'>".$db_Vision_Field_Value_38."</td>
							<td align='center' width='30'>".$db_Vision_Field_Value_2."</td>
							<td align='center' width='20'>".$db_Vision_Field_Value_15."</td></tr>";
						}
						$returnstr .= "</table>";
					}
					$returnstr .= "</td><td width='20'>&nbsp;</td><td width='140' height='14' align='center'  colspan='2'  >";
					if((trim($db_Vision_Field_Value_35)!= "")||(trim($db_Vision_Field_Value_37)!= "")||(trim($db_Vision_Field_Value_39)!= "")||(trim($db_Vision_Field_Value_3)!= "")||(trim($db_Vision_Field_Value_4)!= "")||(trim($db_Vision_Field_Value_17)!= "")||(trim($db_Vision_Field_Value_41)!= "")||(trim($db_Vision_Field_Value_19)!= "")){
						$returnstr .= "<table border='0' cellspacing='1' cellpadding='1' ><tr align='center'>";
						$returnstr .= "<td width='30'>Sph</td>
							<td width='30'>Cyl</td>
							<td width='30'>Axis</td>
							<td colspan='2' width='50'>Vision</td></tr>";
						$returnstr .= "<tr align='center'>
										<td width='30'>".$db_Vision_Field_Value_35."</td>
										<td width='30'>".$db_Vision_Field_Value_37."</td>
										<td width='30'>".$db_Vision_Field_Value_39."</td>
										<td width='30'>".$db_Vision_Field_Value_3."</td>
										<td width='20'>".$db_Vision_Field_Value_17."</td></tr>";
						if(($db_Vision_Field_Value_41 != "")||(trim($db_Vision_Field_Value_4) != "")){ 
									$returnstr .= "<tr align='center'>
									<td width='30'>".$req_add_value_2."</td>
									<td width='30'>".$db_Vision_Field_Value_37."</td>
									<td width='30'>".$db_Vision_Field_Value_39."</td>
									<td width='30'>".$db_Vision_Field_Value_4."</td>
									<td width='20'>".$db_Vision_Field_Value_19."</td></tr>";
						}
						$returnstr .= "</table></td>";
					}
					$returnstr .= "</tr>";
				}
			 }else{
				if((trim($db_Vision_Field_Value_1)!= '')||(trim($db_Vision_Field_Value_2)!= '')||(trim($db_Vision_Field_Value_3)!= '')||(trim($db_Vision_Field_Value_4)!= '')||(trim($db_Vision_Field_Value_13)!= '')||(trim($db_Vision_Field_Value_15)!= '')||(trim($db_Vision_Field_Value_17)!= '')||(trim($db_Vision_Field_Value_19)!= '')){	 
					$returnstr .="<tr><td valign='top'><strong>With glass</strong></td>";
					if((trim($db_Vision_Field_Value_1) == '')&&(trim($db_Vision_Field_Value_2) == '')){
						$returnstr .="<td align='left' colspan='2' valign='top'>&nbsp;</td>";
					}else{						
						$returnstr .="<td width='20%' align='left' valign='top'>";
						if(trim($db_Vision_Field_Value_1)!= '')$returnstr .=' '.$db_Vision_Field_Value_1.' ' .$db_Vision_Field_Value_13;
						$returnstr .="</td>";
						$returnstr .="<td width='20%' align='center' valign='top'>";
						if(trim($db_Vision_Field_Value_2)!= '') $returnstr .=' '.$db_Vision_Field_Value_2.' ' .$db_Vision_Field_Value_15;
						$returnstr .="</td>";
					}
					if((trim($db_Vision_Field_Value_3) == '')&&(trim($db_Vision_Field_Value_4) == '')){
						$returnstr .="<td align='left' colspan='2' valign='top'>&nbsp;</td>";
					}else{	
						$returnstr .="<td width='20%' valign='top'>";
						if(trim($db_Vision_Field_Value_3)!= '') $returnstr .= '  '.$db_Vision_Field_Value_3.' ' .$db_Vision_Field_Value_17;
						$returnstr .="</td>";
						$returnstr .="<td width='20%' align='center' valign='top'>";
						if(trim($db_Vision_Field_Value_4)!= '') $returnstr .= '  '.$db_Vision_Field_Value_4.' ' .$db_Vision_Field_Value_19;
						$returnstr .="</td>";
					}
					$returnstr .= "</tr>";	
				}
			}
			if((trim($db_Vision_Field_Value_5)!= '')||(trim($db_Vision_Field_Value_14)!= '')||(trim($db_Vision_Field_Value_7)!= '')||(trim($db_Vision_Field_Value_16)!= '')||(trim($db_Vision_Field_Value_6)!= '')||(trim($db_Vision_Field_Value_16)!= '')||(trim($db_Vision_Field_Value_8)!= '')||(trim($db_Vision_Field_Value_20)!= '')){	 
				$returnstr .="<tr><td valign='top' width='65'><strong>Without glass</strong></td>";
				if((trim($db_Vision_Field_Value_5) == '')&&(trim($db_Vision_Field_Value_6) == '')){
					$returnstr .="<td align='left' colspan='2' valign='top'>&nbsp;</td><td>&nbsp;</td>";
				}else{
					$returnstr .="<td width='85' align='center' valign='top'>";
					if(trim($db_Vision_Field_Value_5)!= '') $returnstr .=' '.$db_Vision_Field_Value_5.' ' .$db_Vision_Field_Value_14."";
					$returnstr .="</td>";
					$returnstr .="<td width='80' align='center' valign='top'>";
					if(trim($db_Vision_Field_Value_6)!= '') $returnstr .=' '.$db_Vision_Field_Value_6.' ' .$db_Vision_Field_Value_16;
					$returnstr .="</td><td width='20'>&nbsp;</td>";
				}
				if((trim($db_Vision_Field_Value_7) == '')&&(trim($db_Vision_Field_Value_8) == '')){
					$returnstr .="<td align='left' colspan='2' valign='top'>&nbsp;</td>";
				}else{
					$returnstr .="<td width='70' valign='top'>";
					if(trim($db_Vision_Field_Value_7)!= '') $returnstr .= '  '.$db_Vision_Field_Value_7.' ' .$db_Vision_Field_Value_18;
					$returnstr .="</td>";
					$returnstr .="<td width='70' align='center' valign='top'>";
					if(trim($db_Vision_Field_Value_8)!= '') $returnstr .='  '.$db_Vision_Field_Value_8.' ' .$db_Vision_Field_Value_20;
					$returnstr .="</td>";
				}
				$returnstr .="</tr>";	
			}
			if((trim($db_Vision_Field_Value_24)!= '')||(trim($db_Vision_Field_Value_25)!= '')||(trim($db_Vision_Field_Value_28)!= '')||(trim($db_Vision_Field_Value_29)!= '')||(trim($db_Vision_Field_Value_26)!= '')||(trim($db_Vision_Field_Value_27)!= '')||(trim($db_Vision_Field_Value_30)!= '')||(trim($db_Vision_Field_Value_31)!= '')){	 
				$returnstr .="<tr><td valign='top' width='65'><strong>After Dilation</strong></td>";
				if((trim($db_Vision_Field_Value_24) == '')&&(trim($db_Vision_Field_Value_25) == '')){
					$returnstr .="<td align='left' colspan='2' valign='top'></td><td>&nbsp;</td>";
				}else{
					$returnstr .="<td width='85' align='center' valign='top'>";
					if(trim($db_Vision_Field_Value_24)!= '') $returnstr .=' '.$db_Vision_Field_Value_24.' ' .$db_Vision_Field_Value_28; 
					$returnstr .="</td><td width='20'>&nbsp;</td>";
					$returnstr .="<td width='80' align='center' valign='top'>";
					if(trim($db_Vision_Field_Value_25)!= '') $returnstr .= ' '.$db_Vision_Field_Value_25.' ' .$db_Vision_Field_Value_29; 
					$returnstr .="</td>";
				}
				if((trim($db_Vision_Field_Value_26) == '')&&(trim($db_Vision_Field_Value_27) == '')){
					$returnstr .="<td  align='left' colspan='2' valign='top'></td>";
				}else{				
					$returnstr .="<td width='70' align='center' valign='top'>";
					if(trim($db_Vision_Field_Value_26)!= '') $returnstr .='  '.$db_Vision_Field_Value_26.' ' .$db_Vision_Field_Value_30;
					$returnstr .="</td>";
					$returnstr .="<td width='70' align='center' valign='top'>";
					if(trim($db_Vision_Field_Value_27)!= '') $returnstr .= '  '.$db_Vision_Field_Value_27.' ' .$db_Vision_Field_Value_31;
					$returnstr .="</td>";
				}
				$returnstr .="</tr>";	
			}
			if((trim($db_Vision_Field_Value_9) != '')||(trim($db_Vision_Field_Value_10) != '')){	
			  	$returnstr .="<tr><td valign='top' width='65'><strong>Pinhole</strong></td>
				<td valign='top' width='165' align='center' colspan='2'>";
				if(trim($db_Vision_Field_Value_9)!= '') 	$returnstr .= ' '.$db_Vision_Field_Value_9;
				$returnstr .="</td><td width='20'>&nbsp;</td>	
				<td valign='top' width='140' align='center' colspan='2'>";
				if(trim($db_Vision_Field_Value_10)!= '') $returnstr .='  '.$db_Vision_Field_Value_10;
				$returnstr .="</td></tr>";	
			}
			if((trim($db_Vision_Field_Value_11) != '')||(trim($db_Vision_Field_Value_12) != '')){	
				$returnstr .="<tr><td valign='top' width='65'><strong>IOP (NCT)</strong></td>
				<td valign='top' width='165' align='center' colspan='2'>";
				if(trim($db_Vision_Field_Value_11)!= '') $returnstr .=' '.$db_Vision_Field_Value_11." MM HG ";
				$returnstr .="</td><td width='20'>&nbsp;</td>
				<td valign='top' width='140' align='center' colspan='2'>";
				if(trim($db_Vision_Field_Value_12)!= '') $returnstr .= '  '.$db_Vision_Field_Value_12." MM HG";
				$returnstr .="</td></tr>";	
			}
			if((trim($db_Vision_Field_Value_43) != '')||(trim($db_Vision_Field_Value_44) != '')){	
				$returnstr .="<tr><td valign='top' width='65'><strong>IOP: (Applanation)</strong></td>
				<td valign='top' width='165' align='center' colspan='2'>";
				if(trim($db_Vision_Field_Value_43)!= '') $returnstr .=' '.$db_Vision_Field_Value_43." MM HG ";
				$returnstr .="</td><td width='20'>&nbsp;</td>
				<td valign='top' width='140' align='center' colspan='2'>";
				if(trim($db_Vision_Field_Value_44)!= '') $returnstr .= '  '.$db_Vision_Field_Value_44." MM HG";
				$returnstr .="</td></tr>";	
			}		
			if((trim($db_Vision_Field_Value_32) != '')||(trim($db_Vision_Field_Value_33) != '')) {
				$returnstr .="<tr><td height='15' valign='top' width='65'><strong>Remark</strong></td>
				<td valign='top' width='165' align='center' colspan='2'>";
				if(trim($db_Vision_Field_Value_32)!= '') $returnstr .=' '.$db_Vision_Field_Value_32;
				$returnstr .="</td><td width='20'>&nbsp;</td>
				<td valign='top' width='140' align='center' colspan='2'>";
				if(trim($db_Vision_Field_Value_33)!= '') $returnstr .= ' '.$db_Vision_Field_Value_33;
				$returnstr .="</td></tr>";
			}
			$returnstr .="</table></td></tr></table>";
		} 
	} //while	
	return $returnstr; 
}
function fnRetinoscopy_print($reqVisit_ID){
	$db_Retinoscopy_Table_Name 	=  'tbl_Retinoscopy'; 
	$db_Retinoscopy_Table_ID_Name = 'Retinoscopy_ID';

	$db_Retinoscopy_Field_Name_1  = "Retinoscopy_DRY_RE_Sph";
	$db_Retinoscopy_Field_Name_2  = "Retinoscopy_DRY_RE_Cyl";
	$db_Retinoscopy_Field_Name_3  = "Retinoscopy_DRY_RE_Axis";
	$db_Retinoscopy_Field_Name_4  = "Retinoscopy_DRY_LE_Sph";
	$db_Retinoscopy_Field_Name_5  = "Retinoscopy_DRY_LE_Cyl";
	$db_Retinoscopy_Field_Name_6  = "Retinoscopy_DRY_LE_Axis";
	$db_Retinoscopy_Field_Name_7  = "Retinoscopy_CYCLOPEGIC_RE_Sph";
	$db_Retinoscopy_Field_Name_8  = "Retinoscopy_CYCLOPEGIC_RE_Cyl";
	$db_Retinoscopy_Field_Name_9  = "Retinoscopy_CYCLOPEGIC_RE_Axis";
	$db_Retinoscopy_Field_Name_10  = "Retinoscopy_CYCLOPEGIC_LE_Sph";
	$db_Retinoscopy_Field_Name_11  = "Retinoscopy_CYCLOPEGIC_LE_Cyl";
	$db_Retinoscopy_Field_Name_12  = "Retinoscopy_CYCLOPEGIC_LE_Axis";
	$db_Retinoscopy_Field_Name_13  = "Patient_ID";
	$db_Retinoscopy_Field_Name_14  = "Visit_ID";
	$db_Retinoscopy_Field_Name_15  = "TmpVisitID";
	$db_Retinoscopy_Field_Name_16  = "Retinoscopy_Dry_Acceptance_RE_Sph";
	$db_Retinoscopy_Field_Name_17  = "Retinoscopy_Dry_Acceptance_RE_Cyl";
	$db_Retinoscopy_Field_Name_18  = "Retinoscopy_Dry_Acceptance_RE_Axis";
	$db_Retinoscopy_Field_Name_19  = "Retinoscopy_Dry_Acceptance_LE_Sph";
	$db_Retinoscopy_Field_Name_20  = "Retinoscopy_Dry_Acceptance_LE_Cyl";
	$db_Retinoscopy_Field_Name_21  = "Retinoscopy_Dry_Acceptance_LE_Axis";
	$db_Retinoscopy_Field_Name_22  = "Retinoscopy_Dilated_Refraction_RE_Sph";
	$db_Retinoscopy_Field_Name_23  = "Retinoscopy_Dilated_Refraction_RE_Cyl";
	$db_Retinoscopy_Field_Name_24  = "Retinoscopy_Dilated_Refraction_RE_Axis";
	$db_Retinoscopy_Field_Name_25  = "Retinoscopy_Dilated_Refraction_LE_Sph";
	$db_Retinoscopy_Field_Name_26  = "Retinoscopy_Dilated_Refraction_LE_Cyl";
	$db_Retinoscopy_Field_Name_27  = "Retinoscopy_Dilated_Refraction_LE_Axis";
	$db_Retinoscopy_Field_Name_28  = "Retinoscopy_Dilated_Acceptance_RE_Sph";
	$db_Retinoscopy_Field_Name_29  = "Retinoscopy_Dilated_Acceptance_RE_Cyl";
	$db_Retinoscopy_Field_Name_30  = "Retinoscopy_Dilated_Acceptance_RE_Axis";
	$db_Retinoscopy_Field_Name_31  = "Retinoscopy_Dilated_Acceptance_LE_Sph";
	$db_Retinoscopy_Field_Name_32  = "Retinoscopy_Dilated_Acceptance_LE_Cyl";
	$db_Retinoscopy_Field_Name_33  = "Retinoscopy_Dilated_Acceptance_LE_Axis";
	
	$db_Retinoscopy_Field_Name_34  = "Retinoscopy_CYCLOPEGIC_RE_Vision_D";
	$db_Retinoscopy_Field_Name_35  = "Retinoscopy_CYCLOPEGIC_RE_Vision_D_P";
	$db_Retinoscopy_Field_Name_36  = "Retinoscopy_CYCLOPEGIC_RE_Vision_N";
	$db_Retinoscopy_Field_Name_37  = "Retinoscopy_CYCLOPEGIC_RE_Vision_N_P";
	$db_Retinoscopy_Field_Name_38  = "Retinoscopy_CYCLOPEGIC_LE_Vision_D";
	$db_Retinoscopy_Field_Name_39  = "Retinoscopy_CYCLOPEGIC_LE_Vision_D_P";
	$db_Retinoscopy_Field_Name_40  = "Retinoscopy_CYCLOPEGIC_LE_Vision_N";
	$db_Retinoscopy_Field_Name_41  = "Retinoscopy_CYCLOPEGIC_LE_Vision_N_P";
	$db_Retinoscopy_Field_Name_42  = "Retinoscopy_DRY_RE_Vision_D";
	$db_Retinoscopy_Field_Name_43  = "Retinoscopy_DRY_RE_Vision_D_P";
	$db_Retinoscopy_Field_Name_44  = "Retinoscopy_DRY_RE_Vision_N";
	$db_Retinoscopy_Field_Name_45  = "Retinoscopy_DRY_RE_Vision_N_P";
	$db_Retinoscopy_Field_Name_46  = "Retinoscopy_DRY_LE_Vision_D";
	$db_Retinoscopy_Field_Name_47  = "Retinoscopy_DRY_LE_Vision_D_P";
	$db_Retinoscopy_Field_Name_48  = "Retinoscopy_DRY_LE_Vision_N";
	$db_Retinoscopy_Field_Name_49  = "Retinoscopy_DRY_LE_Vision_N_P";
	$db_Retinoscopy_Field_Name_50  = "Retinoscopy_DRY_Acceptance_RE_Vision_D";
	$db_Retinoscopy_Field_Name_51  = "Retinoscopy_DRY_Acceptance_RE_Vision_D_P";
	$db_Retinoscopy_Field_Name_52  = "Retinoscopy_DRY_Acceptance_RE_Vision_N";
	$db_Retinoscopy_Field_Name_53  = "Retinoscopy_DRY_Acceptance_RE_Vision_N_P";
	$db_Retinoscopy_Field_Name_54  = "Retinoscopy_DRY_Acceptance_LE_Vision_D";
	$db_Retinoscopy_Field_Name_55  = "Retinoscopy_DRY_Acceptance_LE_Vision_D_P";
	$db_Retinoscopy_Field_Name_56  = "Retinoscopy_DRY_Acceptance_LE_Vision_N";
	$db_Retinoscopy_Field_Name_57  = "Retinoscopy_DRY_Acceptance_LE_Vision_N_P";
	$db_Retinoscopy_Field_Name_58  = "Retinoscopy_Dilated_Refraction_RE_Vision_D";
	$db_Retinoscopy_Field_Name_59  = "Retinoscopy_Dilated_Refraction_RE_Vision_D_P";
	$db_Retinoscopy_Field_Name_60  = "Retinoscopy_Dilated_Refraction_RE_Vision_N";
	$db_Retinoscopy_Field_Name_61  = "Retinoscopy_Dilated_Refraction_RE_Vision_N_P";
	$db_Retinoscopy_Field_Name_62  = "Retinoscopy_Dilated_Refraction_LE_Vision_D";
	$db_Retinoscopy_Field_Name_63  = "Retinoscopy_Dilated_Refraction_LE_Vision_D_P";
	$db_Retinoscopy_Field_Name_64  = "Retinoscopy_Dilated_Refraction_LE_Vision_N";
	$db_Retinoscopy_Field_Name_65  = "Retinoscopy_Dilated_Refraction_LE_Vision_N_P";
	$db_Retinoscopy_Field_Name_66  = "Retinoscopy_Dilated_Acceptance_RE_Vision_D";
	$db_Retinoscopy_Field_Name_67  = "Retinoscopy_Dilated_Acceptance_RE_Vision_D_P";
	$db_Retinoscopy_Field_Name_68  = "Retinoscopy_Dilated_Acceptance_RE_Vision_N";
	$db_Retinoscopy_Field_Name_69  = "Retinoscopy_Dilated_Acceptance_RE_Vision_N_P";
	$db_Retinoscopy_Field_Name_70  = "Retinoscopy_Dilated_Acceptance_LE_Vision_D";
	$db_Retinoscopy_Field_Name_71  = "Retinoscopy_Dilated_Acceptance_LE_Vision_D_P";
	$db_Retinoscopy_Field_Name_72  = "Retinoscopy_Dilated_Acceptance_LE_Vision_N";
	$db_Retinoscopy_Field_Name_73  = "Retinoscopy_Dilated_Acceptance_LE_Vision_N_P";
	$db_Retinoscopy_Field_Name_74  = "retinoscopy_Note";
	

	$sql = 'select * from '.$db_Retinoscopy_Table_Name.', patient_diagnosis where `Visit_ID` = '.$reqVisit_ID;
	$result = mysql_query($sql) or die($sql);	
	while ($row = mysql_fetch_array($result)){
		$show_refraction = 0;
    	$db_Retinoscopy_Field_Value_1 = trim($row[$db_Retinoscopy_Field_Name_1]);		$db_Retinoscopy_Field_Value_2 = trim($row[$db_Retinoscopy_Field_Name_2]);
		$db_Retinoscopy_Field_Value_3 = trim($row[$db_Retinoscopy_Field_Name_3]);		$db_Retinoscopy_Field_Value_4 = trim($row[$db_Retinoscopy_Field_Name_4]);
		$db_Retinoscopy_Field_Value_5 = trim($row[$db_Retinoscopy_Field_Name_5]);		$db_Retinoscopy_Field_Value_6 = trim($row[$db_Retinoscopy_Field_Name_6]);
		$db_Retinoscopy_Field_Value_7 = trim($row[$db_Retinoscopy_Field_Name_7]);		$db_Retinoscopy_Field_Value_8 = trim($row[$db_Retinoscopy_Field_Name_8]);
		$db_Retinoscopy_Field_Value_9 = trim($row[$db_Retinoscopy_Field_Name_9]);		$db_Retinoscopy_Field_Value_10 = trim($row[$db_Retinoscopy_Field_Name_10]);
		$db_Retinoscopy_Field_Value_11 = trim($row[$db_Retinoscopy_Field_Name_11]);		$db_Retinoscopy_Field_Value_12 = trim($row[$db_Retinoscopy_Field_Name_12]);
		$db_Retinoscopy_Field_Value_13 = trim($row[$db_Retinoscopy_Field_Name_13]);		$db_Retinoscopy_Field_Value_14 = trim($row[$db_Retinoscopy_Field_Name_14]);
		$db_Retinoscopy_Field_Value_15 = trim($row[$db_Retinoscopy_Field_Name_15]);		$db_Retinoscopy_Field_Value_16 = trim($row[$db_Retinoscopy_Field_Name_16]);
		$db_Retinoscopy_Field_Value_17 = trim($row[$db_Retinoscopy_Field_Name_17]);		$db_Retinoscopy_Field_Value_18 = trim($row[$db_Retinoscopy_Field_Name_18]);
		$db_Retinoscopy_Field_Value_19 = trim($row[$db_Retinoscopy_Field_Name_19]);		$db_Retinoscopy_Field_Value_20 = trim($row[$db_Retinoscopy_Field_Name_20]);
    	$db_Retinoscopy_Field_Value_21 = trim($row[$db_Retinoscopy_Field_Name_21]);		$db_Retinoscopy_Field_Value_22 = trim($row[$db_Retinoscopy_Field_Name_22]);
		$db_Retinoscopy_Field_Value_23 = trim($row[$db_Retinoscopy_Field_Name_23]);		$db_Retinoscopy_Field_Value_24 = trim($row[$db_Retinoscopy_Field_Name_24]);
		$db_Retinoscopy_Field_Value_25 = trim($row[$db_Retinoscopy_Field_Name_25]);		$db_Retinoscopy_Field_Value_26 = trim($row[$db_Retinoscopy_Field_Name_26]);
		$db_Retinoscopy_Field_Value_27 = trim($row[$db_Retinoscopy_Field_Name_27]);		$db_Retinoscopy_Field_Value_28 = trim($row[$db_Retinoscopy_Field_Name_28]);
		$db_Retinoscopy_Field_Value_29 = trim($row[$db_Retinoscopy_Field_Name_29]);		$db_Retinoscopy_Field_Value_30 = trim($row[$db_Retinoscopy_Field_Name_30]);
    	$db_Retinoscopy_Field_Value_31 = trim($row[$db_Retinoscopy_Field_Name_31]);		$db_Retinoscopy_Field_Value_32 = trim($row[$db_Retinoscopy_Field_Name_32]);
		$db_Retinoscopy_Field_Value_33 = trim($row[$db_Retinoscopy_Field_Name_33]);		$db_Retinoscopy_Field_Value_34 = trim($row[$db_Retinoscopy_Field_Name_34]);
		$db_Retinoscopy_Field_Value_35 = trim($row[$db_Retinoscopy_Field_Name_35]);		$db_Retinoscopy_Field_Value_36 = trim($row[$db_Retinoscopy_Field_Name_36]);
		$db_Retinoscopy_Field_Value_37 = trim($row[$db_Retinoscopy_Field_Name_37]);		$db_Retinoscopy_Field_Value_38 = trim($row[$db_Retinoscopy_Field_Name_38]);
		$db_Retinoscopy_Field_Value_39 = trim($row[$db_Retinoscopy_Field_Name_39]);		$db_Retinoscopy_Field_Value_40 = trim($row[$db_Retinoscopy_Field_Name_40]);	
    	$db_Retinoscopy_Field_Value_41 = trim($row[$db_Retinoscopy_Field_Name_41]);		$db_Retinoscopy_Field_Value_42 = trim($row[$db_Retinoscopy_Field_Name_42]);
		$db_Retinoscopy_Field_Value_43 = trim($row[$db_Retinoscopy_Field_Name_43]);		$db_Retinoscopy_Field_Value_44 = trim($row[$db_Retinoscopy_Field_Name_44]);
		$db_Retinoscopy_Field_Value_45 = trim($row[$db_Retinoscopy_Field_Name_45]);		$db_Retinoscopy_Field_Value_46 = trim($row[$db_Retinoscopy_Field_Name_46]);
		$db_Retinoscopy_Field_Value_47 = trim($row[$db_Retinoscopy_Field_Name_47]);		$db_Retinoscopy_Field_Value_48 = trim($row[$db_Retinoscopy_Field_Name_48]);
		$db_Retinoscopy_Field_Value_49 = trim($row[$db_Retinoscopy_Field_Name_49]);		$db_Retinoscopy_Field_Value_50 = trim($row[$db_Retinoscopy_Field_Name_50]);
		$db_Retinoscopy_Field_Value_51 = trim($row[$db_Retinoscopy_Field_Name_51]);		$db_Retinoscopy_Field_Value_52 = trim($row[$db_Retinoscopy_Field_Name_52]);
		$db_Retinoscopy_Field_Value_53 = trim($row[$db_Retinoscopy_Field_Name_53]);		$db_Retinoscopy_Field_Value_54 = trim($row[$db_Retinoscopy_Field_Name_54]);
		$db_Retinoscopy_Field_Value_55 = trim($row[$db_Retinoscopy_Field_Name_55]);		$db_Retinoscopy_Field_Value_56 = trim($row[$db_Retinoscopy_Field_Name_56]);
		$db_Retinoscopy_Field_Value_57 = trim($row[$db_Retinoscopy_Field_Name_57]);		$db_Retinoscopy_Field_Value_58 = trim($row[$db_Retinoscopy_Field_Name_58]);
		$db_Retinoscopy_Field_Value_59 = trim($row[$db_Retinoscopy_Field_Name_59]);		$db_Retinoscopy_Field_Value_60 = trim($row[$db_Retinoscopy_Field_Name_60]);		
    	$db_Retinoscopy_Field_Value_61 = trim($row[$db_Retinoscopy_Field_Name_61]);		$db_Retinoscopy_Field_Value_62 = trim($row[$db_Retinoscopy_Field_Name_62]);
		$db_Retinoscopy_Field_Value_63 = trim($row[$db_Retinoscopy_Field_Name_63]);		$db_Retinoscopy_Field_Value_64 = trim($row[$db_Retinoscopy_Field_Name_64]);
		$db_Retinoscopy_Field_Value_65 = trim($row[$db_Retinoscopy_Field_Name_65]);		$db_Retinoscopy_Field_Value_66 = trim($row[$db_Retinoscopy_Field_Name_66]);
		$db_Retinoscopy_Field_Value_67 = trim($row[$db_Retinoscopy_Field_Name_67]);		$db_Retinoscopy_Field_Value_68 = trim($row[$db_Retinoscopy_Field_Name_68]);
		$db_Retinoscopy_Field_Value_69 = trim($row[$db_Retinoscopy_Field_Name_69]);		$db_Retinoscopy_Field_Value_70 = trim($row[$db_Retinoscopy_Field_Name_70]);
		$db_Retinoscopy_Field_Value_71 = trim($row[$db_Retinoscopy_Field_Name_71]);		$db_Retinoscopy_Field_Value_72 = trim($row[$db_Retinoscopy_Field_Name_72]);
		$db_Retinoscopy_Field_Value_73 = trim($row[$db_Retinoscopy_Field_Name_73]);		$db_Retinoscopy_Field_Value_74 = trim($row[$db_Retinoscopy_Field_Name_74]);
	}
$req_str .= '<table border="0" cellspacing="0" cellpadding="1">
		<tr class="comments2">
	<td  width="65">&nbsp;</td>
	<td colspan="7" width="180" align="center">RE</td>
	<td width="20" >&nbsp;</td>
	<td colspan="7" width="180" align="center" >LE</td>
	</tr>
		<tr height="24">
	<td width="65">&nbsp;</td>
	<td width="30" align="center">Sph</td>
	<td width="30" align="center">Cyl</td>
	<td width="30" align="center">Axis</td>
	<td width="45" align="center">VA/D</td>
	<td width="45" align="center">VA/N</td>
	<td width="20" align="center">&nbsp;</td>
	<td width="30" align="center">Sph</td>
	<td width="30" align="center">Cyl</td>
	<td width="30" align="center">Axis</td>
	<td width="45" align="center">VA/D</td>
	<td width="45" align="center">VA/N</td>
	</tr>';
if(($db_Retinoscopy_Field_Value_7 != "")||($db_Retinoscopy_Field_Value_8 != "")||($db_Retinoscopy_Field_Value_9 != "")||
	($db_Retinoscopy_Field_Value_10 != "")||($db_Retinoscopy_Field_Value_11 != "")||($db_Retinoscopy_Field_Value_12 != "")||
	($db_Retinoscopy_Field_Value_34 != "")||($db_Retinoscopy_Field_Value_35 != "")||($db_Retinoscopy_Field_Value_36 != "")||
	($db_Retinoscopy_Field_Value_37 != "")||($db_Retinoscopy_Field_Value_38 != "")||($db_Retinoscopy_Field_Value_39 != "")||
	($db_Retinoscopy_Field_Value_40 != "")||($db_Retinoscopy_Field_Value_41 != "")){
	$req_str .= '<tr><td width="65"><strong>CYCLOPEGIC</strong></td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_7.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_8.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_9.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_34.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_35.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_36.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_37.'</td>';
	$req_str .= '<td width="20" align="center">&nbsp;</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_10.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_11.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_12.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_38.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_39.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_40.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_41.'</td>';
	$req_str .= '</tr>';
}
if(($db_Retinoscopy_Field_Value_1 != "")||($db_Retinoscopy_Field_Value_2 != "")||($db_Retinoscopy_Field_Value_3 != "")||
	($db_Retinoscopy_Field_Value_4 != "")||($db_Retinoscopy_Field_Value_5 != "")||($db_Retinoscopy_Field_Value_6 != "")||
	($db_Retinoscopy_Field_Value_42 != "")||($db_Retinoscopy_Field_Value_43 != "")||($db_Retinoscopy_Field_Value_44 != "")||
	($db_Retinoscopy_Field_Value_45 != "")||($db_Retinoscopy_Field_Value_46 != "")||($db_Retinoscopy_Field_Value_47 != "")||
	($db_Retinoscopy_Field_Value_48 != "")||($db_Retinoscopy_Field_Value_49 != "")){
	$req_str .= '<tr><td width="65"><strong>Dry Refraction</strong></td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_1.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_2.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_3.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_42.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_43.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_44.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_45.'</td>';
	$req_str .= '<td width="20" align="center">&nbsp;</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_4.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_5.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_6.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_46.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_47.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_48.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_49.'</td>';
	$req_str .= '</tr>';
}
if(($db_Retinoscopy_Field_Value_16 != "")||($db_Retinoscopy_Field_Value_17 != "")||($db_Retinoscopy_Field_Value_18 != "")||
	($db_Retinoscopy_Field_Value_19 != "")||($db_Retinoscopy_Field_Value_20 != "")||($db_Retinoscopy_Field_Value_21 != "")||
	($db_Retinoscopy_Field_Value_50 != "")||($db_Retinoscopy_Field_Value_51 != "")||($db_Retinoscopy_Field_Value_52 != "")||
	($db_Retinoscopy_Field_Value_53 != "")||($db_Retinoscopy_Field_Value_54 != "")||($db_Retinoscopy_Field_Value_55 != "")||
	($db_Retinoscopy_Field_Value_56 != "")||($db_Retinoscopy_Field_Value_57 != "")){
	$req_str .= '<tr><td width="65"><strong>Dry Acceptance</strong></td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_16.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_17.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_18.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_50.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_51.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_52.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_53.'</td>';
	$req_str .= '<td width="20" align="center">&nbsp;</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_19.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_20.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_21.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_54.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_55.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_56.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_57.'</td>';
	$req_str .= '</tr>';
}
if(($db_Retinoscopy_Field_Value_22 != "")||($db_Retinoscopy_Field_Value_23 != "")||($db_Retinoscopy_Field_Value_24 != "")||
	($db_Retinoscopy_Field_Value_25 != "")||($db_Retinoscopy_Field_Value_26 != "")||($db_Retinoscopy_Field_Value_27 != "")||
	($db_Retinoscopy_Field_Value_58 != "")||($db_Retinoscopy_Field_Value_59 != "")||($db_Retinoscopy_Field_Value_60 != "")||
	($db_Retinoscopy_Field_Value_61 != "")||($db_Retinoscopy_Field_Value_62 != "")||($db_Retinoscopy_Field_Value_63 != "")||
	($db_Retinoscopy_Field_Value_64 != "")||($db_Retinoscopy_Field_Value_65 != "")){
	$req_str .= '<tr><td width="65"><strong>Dilated Refraction</strong></td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_22.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_23.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_24.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_58.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_59.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_60.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_61.'</td>';
	$req_str .= '<td width="20" align="center">&nbsp;</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_25.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_26.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_27.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_62.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_63.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_64.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_65.'</td>';
	$req_str .= '</tr>';
}
if(($db_Retinoscopy_Field_Value_28 != "")||($db_Retinoscopy_Field_Value_29 != "")||($db_Retinoscopy_Field_Value_30 != "")||
	($db_Retinoscopy_Field_Value_31 != "")||($db_Retinoscopy_Field_Value_32 != "")||($db_Retinoscopy_Field_Value_33 != "")||
	($db_Retinoscopy_Field_Value_66 != "")||($db_Retinoscopy_Field_Value_67 != "")||($db_Retinoscopy_Field_Value_68 != "")||
	($db_Retinoscopy_Field_Value_69 != "")||($db_Retinoscopy_Field_Value_70 != "")||($db_Retinoscopy_Field_Value_71 != "")||
	($db_Retinoscopy_Field_Value_72 != "")||($db_Retinoscopy_Field_Value_73 != "")){
	$req_str .= '<tr><td width="65"><strong>Dilated  Acceptance</strong></td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_28.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_29.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_30.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_66.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_67.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_68.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_69.'</td>';
	$req_str .= '<td width="20" align="center">&nbsp;</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_31.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_32.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_33.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_70.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_71.'</td>';
	$req_str .= '<td width="30" align="center">'.$db_Retinoscopy_Field_Value_72.'</td>';
	$req_str .= '<td width="15" align="center">'.$db_Retinoscopy_Field_Value_73.'</td>';
	$req_str .= '</tr>';
}
$req_str .= '</table>';
return $req_str;
}
?>