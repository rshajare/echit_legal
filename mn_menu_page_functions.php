<? 

// +----------------------------------------------------------------------------------------------------------------------------+
// |  File Name : nm_menu_lhf_banner_functions.php
// |  Version : 2.0 
// |  Create-date : 29, March, 2010
// |  Modify-date : 22, April, 2010
// |  Authors : Niteen Borate                        
// | 
// |  Desciption : functions for header footer banner menu header
// |  How to use : to use it just use appropriate string in required template file
// |
// |  {$mn_usr_menu_header_str} use in html for user menu
// |  {$mn_adm_menu_header_str} use for admin menu
// |
// |  {$mn_lhf_header} = header string 
// |  {$mn_lhf_left} = left string
// |  {$mn_lhf_footer} = footer string
// |
// |  {$mn_banner_image1} = banner image 1 
// |  {$mn_banner_url1} = url for banner 1
// |  {$mn_banner_image2} = banner image 2 
// |  {$mn_banner_url2} = url for banner 2
// |   ......................
// |  {$mn_banner_image11} = banner image 1 
// |  {$mn_banner_url1} = url for banner 1
// |  
// +------------------------------------------------------------------------------------------------------------------------------
//

 require_once( dirname(__FILE__).'/sur_privi_access_functions.php' );




//======================================Start Banner=======================================
/*//For banner images at header and left page
$banner_sql = "Select * from tbl_mn_banner where banner_id = 'banner_id'";
$banner_temp = $db->getall($banner_sql);
foreach($banner_temp as $banner_row){
	$banner_image1 = $banner_row['banner1'];   
	$banner_image2 = $banner_row['banner2'];
	$banner_image3 = $banner_row['banner3'];
	$banner_image4 = $banner_row['banner4'];
	$banner_image5 = $banner_row['banner5'];
	$banner_image6 = $banner_row['banner6'];
	$banner_url1 = $banner_row['url1'];
	$banner_url2 = $banner_row['url2'];
	$banner_url3 = $banner_row['url3'];
	$banner_url4 = $banner_row['url4'];
	$banner_url5 = $banner_row['url5'];
	$banner_url6 = $banner_row['url6'];
	
	$t->assign ( 'mn_banner_url1', $banner_url1 );
	$t->assign ( 'mn_banner_url2', $banner_url2 );
	$t->assign ( 'mn_banner_url3', $banner_url3 );
	$t->assign ( 'mn_banner_url4', $banner_url4 );
	$t->assign ( 'mn_banner_url5', $banner_url5 );
	$t->assign ( 'mn_banner_url6', $banner_url6 );
	$t->assign ( 'mn_banner_image1', $banner_image1 );
	$t->assign ( 'mn_banner_image2', $banner_image2 );
	$t->assign ( 'mn_banner_image3', $banner_image3 );
	$t->assign ( 'mn_banner_image4', $banner_image4 );
	$t->assign ( 'mn_banner_image5', $banner_image5 );
	$t->assign ( 'mn_banner_image6', $banner_image6 );
}
//======================================End Banner================*/


//================================Get Left Header Footer Content
$tmplaguage = $_SESSION['opt_lang'];
$tmpsql = "SELECT name, category_tag FROM tbl_mn_lhf where lang = '$tmplaguage' and type = 'lhf' and order_no=1 and 
	name in ('report_header','report_header_1','header','web_sur_header','web_sur_footer','shop_name')";
$tmpresult = mysql_query($tmpsql) or die(mysql_error(). "<br>Couldn't execute query3.");
while ($tmprow = mysql_fetch_array($tmpresult)) {        
    //echo "mn_lhf_".$tmprow['name']."<BR>";
	$t->assign("mn_lhf_".$tmprow['name'],$tmprow["category_tag"]);
}		
$tmpsql = "SELECT name, category_tag FROM tbl_mn_lhf where lang = '$tmplaguage' and type = 'lhf' and order_no=1 and 
	name in ('news_alert') and trim(category_tag) != ''";
$tmpresult = mysql_query($tmpsql) or die(mysql_error(). "<br>Couldn't execute query3.");
while ($tmprow = mysql_fetch_array($tmpresult)) {        
	$t->assign("mn_lhf_".$tmprow['name'],$tmprow["category_tag"]);
}		
//================================End Get Left Header Footer Content
//report_header version



//

function heire_cat($main_id,$selected,$lang){
	if($main_id == 0){
		$reqmaincat = "Select * from tbl_category where hierno = 0 and lang = '$lang'";
	}else{
		$reqmaincat = "Select * from tbl_category where main_id = $main_id and lang = '$lang'";
	}
	//echo $reqmaincat;
	$main_cat_temp = mysql_query( $reqmaincat ) or die(mysql_error().$reqmaincat);
	$space = '';
	while($row = mysql_fetch_array($main_cat_temp)){
		$space = ' ';
		$cat_id = $row['main_cat_id'];
		$req_present = fngetcount("tbl_category","main_id = $cat_id");
		$level = $row['hierno'];
		for($i=1;$i<=$level;$i++){
			$space = $space."--";
		}
		if($req_present > 0){
			//$req_option_string .= "<optgroup label='$space".$row['name']."'></optgroup>";
			if($selected == $cat_id) $sel = "selected='selected'"; else $sel = '';
			$req_option_string .= "<option value='".$cat_id."' ".$sel.">"."<strong>".$space.$row['name']."</strong>"."</option>";
			$req_option_string .=  heire_cat($cat_id,$selected,$lang);
		}else{
			if($selected == $cat_id) $sel = "selected='selected'"; else $sel = '';
			$req_option_string .= "<option value='".$cat_id."' ".$sel.">$space".$row['name']."</option>";			
		}
	}			
	return $req_option_string;
}



?>