function fnloading_window(){
	var divTag 			= document.createElement("div");
	divTag.id 			= "Loading_Div";
	divTag.setAttribute("align","center");
	divTag.setAttribute('style', 'display:none');
	divTag.innerHTML 	= "<div align='center' id='div_shadow_layer'><br><br><table width='100%' height='100%'><tr><td align='center' VALIGN='MIDDLE' class='NormalBR'><img src='images/progress.gif' border='0' alt='Loading, please wait...' /></td></tr></table><br><br> Loading......</div>";
	document.body.appendChild(divTag);
	showLayer('Message_Box','Loading_Div','center=1,height=100,width=200');
	return divTag;
}
function fnCloseloading_window(divTag){
	document.body.removeChild(divTag);
	closewindow();
}