<?php include( 'sur_adm_permission.php' );
if ( !defined( 'SMARTY_DIR' ) ){include_once( 'init.php' );}
header('Content-Type: text/html; charset=utf-8');

$ses_lang = 'english';
$req_tmp_laguage = $_SESSION['opt_lang'];

if (isset($_GET["ref_user_code"])){
	$ref_user_code = $_GET[ 'ref_user_code' ];
	$t->assign ( 'ref_user_code', $ref_user_code );
}

if ( ( isset( $_REQUEST['ref_user_type'] ) ) && ($_REQUEST['ref_user_type']<>'') ) {
	$ref_user_type=$_REQUEST['ref_user_type'] ;
	$_SESSION['ref_user_type'] = $ref_user_type;
}elseif ( (isset( $_SESSION['ref_user_type'] ) ) && ($_SESSION['ref_user_type']<>'') ){
	$ref_user_type= $_SESSION['ref_user_type'];
}else {
	$ref_user_type="user" ;
	$_SESSION['ref_user_type'] = $ref_user_type;
}
$t->assign ( 'ref_user_type', $ref_user_type);

$login_to_introducer= fnGetValue("tbl_mn_app_setting","field_value","field_name='login_to_introducer'");
$t->assign ( 'login_to_introducer', $login_to_introducer );	

if (($ref_user_type =='staff')|| ($ref_user_type =='system_user')){  // for staff & employee
	$user_html_page 		= "sur_adm_user_list.htm";	
}elseif ( ($ref_user_type =='introducer') ){  // for customer 
	$user_html_page 		= "sur_adm_user_list_intro.htm";	
}else{
	$user_html_page 		= "sur_adm_user_list.htm";	
}	


$_SESSION["SubMen"] = 1;


// Bank deposit
$Surveillance  = array();

$reqCurrPage = "Surveillance";
$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
$Surveillance['Surveillance'] = $reqIsAccess ;

$reqCurrPage = "Surveillance_other";
$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
$Surveillance['other'] = $reqIsAccess ;

$reqCurrPage = "Surveillance_staff";
$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
$Surveillance['staff'] = $reqIsAccess ;

$reqCurrPage = "Surveillance_staff_modify";
$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
//$reqIsAccess = "YES";
$Surveillance['staff_modify'] = $reqIsAccess ;
$t->assign ('Surveillance', $Surveillance);
	
// brancharray 	
// table name = tbl_gen_branch in gold // tbl_branch chit
$arrbranch_code = array();
$arrbranch_code1 = array();
$branchtemp = $db->getAll( "SELECT main_branch_id,branch_name FROM tbl_branch where lang = '$ses_lang' order by `branch_name` asc" );
foreach( $branchtemp as $index => $branchrow ) {
	$arrbranch_code1['main_branch_id'][] = $branchrow['main_branch_id'];
	$arrbranch_code1['branch_name'][] = $branchrow['branch_name'];
}
$t->assign ( 'arrbranch_code', $arrbranch_code1 );


$ref_user_type_arr = array();
$temp = $db->getAll( "SELECT distinct(ref_user_type) FROM tbl_sur_sign_up order by `ref_user_type`" );
foreach( $temp as $index => $row ) {
	$ref_user_type_arr['ref_user_type'][] = $row['ref_user_type'];
}
$t->assign ( 'ref_user_type_arr', $ref_user_type_arr );

//== end of variable declaration




///====================================================================
// Action part
if (  (isset($_POST["txtcaloption"])) && ($_POST["txtcaloption"]<>'') ){  //block/unblock user email id and approve/bolck to user

	$tmp_caloption = $_POST["txtcaloption"];
	$reqfrom_personnel_id    = $_SESSION['personnel_id'];
	$sql="delete from tbl_msg_tmp_personnel where from_personnel_id = '$reqfrom_personnel_id' ";
	$db->query( $sql);
	
	if ( ($tmp_caloption == 'send_message_toall') || ($tmp_caloption == 'send_sms_toall') ) {  // send to all
		$block_usr_sql = stripcslashes($_POST['past_sql']);
		//echo $block_usr_sql;
		$temp_block_usr = $db->getall($block_usr_sql);
		foreach($temp_block_usr as $blk_row){
			$reqpersonnel_id = $blk_row["personnel_id"];
			$insert_sql= "insert IGNORE into tbl_msg_tmp_personnel values ('$reqpersonnel_id','$reqfrom_personnel_id')";
			$db->query($insert_sql);	
		}
	}else{
		$chk_arr = $_POST['chk'];
		$Count = count($chk_arr); 
		for ($c=0; $c<$Count; $c++){
			$req_to_personnel_id = $chk_arr[$c];
			$insert_sql= "insert into tbl_msg_tmp_personnel values ('$req_to_personnel_id','$reqfrom_personnel_id')";
			$db->query($insert_sql);	
		}	
	}	
				
				
	switch ($tmp_caloption) {
	
	case "send_message_toall":
		$to = "msg_send_message.php?no_action=1"; // pass no_action flag to next page so he will not perform temp perssonel add 
		break;
	
	case "send_sms_toall":
		$to = "msg_send_sms.php?no_action=1"; // pass no_action flag to next page so he will not perform temp perssonel add 
		break;
				
	case "send_message":
		$to = "msg_send_message.php?no_action=1"; // pass no_action flag to next page so he will not perform temp perssonel add 
		break;	
	
	case "send_sms":
		$to = "msg_send_sms.php?no_action=1";   // pass no_action flag to next page so he will not perform temp perssonel add 
		break;	
		
	case "apply_for_course":
		$_SESSION['assign_cr_flag']="YES";
		$t->assign ( 'assign_cr_flag', 'YES');
		$to = "usercource_add.php?no_action=1";   // pass no_action flag to next page so he will not perform temp perssonel add 
		break;	
		
	case "add_discount":	
		$to = "sur_add_discount.php?op=1";   // pass no_action flag to next page so he will not perform temp perssonel add 
		break;	
	
	case "del_select":
		foreach ($chk_arr as $tmpval_personnel_id) {
			fnDelUser($tmpval_personnel_id);
		}
		$to = "sur_adm_user_list.php?ref_user_type=$ref_user_type";
		break;
	
	case "activate":
		foreach ($chk_arr as $tmpval_personnel_id) {
			fnActivateUser($tmpval_personnel_id,1);
		}
		$to = "sur_adm_user_list.php?ref_user_type=$ref_user_type";
		break;	
		
	case "deactive":
		foreach ($chk_arr as $tmpval_personnel_id) {
			fnActivateUser($tmpval_personnel_id,0);
		}
		$to = "sur_adm_user_list.php?ref_user_type=$ref_user_type";
		break;
		
	case "blk_email":
		foreach ($chk_arr as $tmpval_personnel_id) {
			fnBlkEmailUser($tmpval_personnel_id,1);
		}
		$to = "sur_adm_user_list.php?ref_user_type=$ref_user_type";
		break;		
	
	case "blk_usr":
		foreach ($chk_arr as $tmpval_personnel_id) {
			fnBlkUser($tmpval_personnel_id);
			
		}
		$to = "sur_adm_user_list.php?ref_user_type=$ref_user_type";
		break;	
			
	}
	header("Location:". $to); 
	exit();

}//end of block/unblock user email id and approve/bolck to user

/// End of action part



// ==================================OP Part =============================================
if (isset( $_GET['op'] ) ){
	$op = $_GET['op'];
	if ($op==2){
		$reqpersonnel_id = $_GET[ 'personnel_id' ];
		fnUserPassReset($reqpersonnel_id);
		$t->assign ( 'resultstr', "Password reset Successfully");	
		
	}elseif ($op==3){
		$reqpersonnel_id = $_GET[ 'personnel_id' ];
		$tmpResult = fnDelUser($reqpersonnel_id);
		$t->assign ( 'resultstr', $tmpResult);	
		
	}elseif ($op==4){
		$reqpersonnel_id = $_GET[ 'personnel_id' ];
		$act = $_GET[ 'act' ];
		$tmpResult = fnActivateUser ($reqpersonnel_id,$act);
		$t->assign ( 'resultstr', $tmpResult);	

		
	}elseif ($op==5){
		$tmpval_personnel_id = $_GET[ 'personnel_id' ];
		$act = $_GET[ 'act' ];
		fnBlkEmailUser($tmpval_personnel_id,$act);
	
	}elseif ($op==6){
		$reqpersonnel_id = $_GET[ 'personnel_id' ];
		$act = $_GET[ 'act' ];
		fnBlkUser($tmpval_personnel_id);
		
	}elseif ($op==8){	
		$act = $_REQUEST["act"];
		$reqpersonnel_id = $_GET[ 'personnel_id' ];
		fnSMS_FlagUser($reqpersonnel_id,$act);
	}
	
	$t->assign ( 'result', 4);
}
// end of op part





//=============Display part ================================
//$reqpersonnel_id	= $_SESSION["personnel_id"];



$sql = "SELECT CONCAT(first_name,' ',last_name) as full_name,tbl_sur_sign_up.* 
	    FROM tbl_sur_sign_up left join tbl_sur_sign_up_branches on tbl_sur_sign_up.`personnel_id`=tbl_sur_sign_up_branches.`personnel_id` ";


if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort'];	else $reqsort='user_code';
if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";

//---------------------------------Paging--------------------------------------------------
if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
$t->assign('reqord',$reqord);
//-------------------------------end  Paging------------------------------------------------------



$tmpblnWhere=0;

//WHERE main_branch_id in  ".$UsrBranchIdlist;


if (strcmp($ref_user_type,"")<>0){				
	if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
	$sql = $sql . " tbl_sur_sign_up.ref_user_type = '$ref_user_type'";
	$PageString = $PageString  .'&ref_user_type='.$ref_user_type;
}	

//if ($ref_user_type<>'staff' and $ref_user_type <> 'system_user'){                
		//if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
		//$sql = $sql . " main_branch_id in  ".$UsrBranchIdlist;
//}

 
if ( isset( $_REQUEST['txtkeyword'] ) ){
	$reqkeyword=$_REQUEST['txtkeyword'] ;
	$t->assign ( 'reqkeyword', $reqkeyword);
	if (strcmp($reqkeyword,"")<>0){				
			if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
			$sql = $sql . " tbl_sur_sign_up.privilege_type = '$reqkeyword'";
			$PageString = $PageString  .'&txtkeyword='.$reqkeyword;
	 }
}

if ( isset( $_REQUEST['privilege_type'] ) ){
	$privilege_type=$_REQUEST['privilege_type'] ;
	$t->assign ( 'privilege_type', $privilege_type);
	if (strcmp($privilege_type,"")<>0){				
			if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
			$sql = $sql . " tbl_sur_sign_up.privilege_type = '$privilege_type'";
			$PageString = $PageString  .'&privilege_type='.$privilege_type;
	 }
}

if (( isset( $_REQUEST['assign_flag'] ) ) && ($ref_user_type=='subscriber') ){
	$assign_flag=$_REQUEST['assign_flag'] ;
	$t->assign ( 'assign_flag', $assign_flag);
	if (strcmp($assign_flag,"")<>0){				
			if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
			if ($assign_flag ==1)
				$sql = $sql . "tbl_sur_sign_up.ref_user_code <> ''";
			else 
				$sql = $sql . "tbl_sur_sign_up.ref_user_code = ''";
			$PageString = $PageString  .'&assign_flag='.$assign_flag;
	 }
}




if ( isset( $_REQUEST['txtkeyword1'] ) ){
	$reqkeyword1=$_REQUEST['txtkeyword1'] ;
	$t->assign ( 'reqkeyword1', $reqkeyword1);
	if (strcmp($reqkeyword1,"")<>0){				
			if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
			$sql = $sql . " tbl_sur_sign_up.email like '%$reqkeyword1%'";
			$PageString = $PageString  .'&txtkeyword1='.$reqkeyword1;
	 }
}


if ( isset( $_REQUEST['txtempcode'] ) ){
	$reqempcode=$_REQUEST['txtempcode'] ;
	$t->assign ( 'reqempcode', $reqempcode);
	if (strcmp($reqempcode,"")<>0){				
			if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
			$sql = $sql . " tbl_sur_sign_up.user_code like '%$reqempcode%'";
			$PageString = $PageString  .'&txtkeyword1='.$reqempcode;
	 }
}





if ( isset( $_REQUEST['txtkeyword3'] ) ){
	$reqkeyword3=$_REQUEST['txtkeyword3'] ;
	$t->assign ( 'reqkeyword3', $reqkeyword3);
	if (strcmp($reqkeyword3,"")<>0){				
			if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
			if($reqkeyword3 == '1')
				$sql = $sql . " tbl_sur_sign_up.approved = '1'";
			if($reqkeyword3 == '2')
				$sql = $sql . " tbl_sur_sign_up.approved = '0'";
			if($reqkeyword3 == '3')
				$sql = $sql . " tbl_sur_sign_up.blk_email = '1'";	
			$PageString = $PageString  .'&txtkeyword3='.$reqkeyword3;
	 }
}


if ( isset( $_REQUEST['txtkeyword4'] ) ){
	$reqkeyword4=$_REQUEST['txtkeyword4'] ;
	$t->assign ( 'reqkeyword4', $reqkeyword4);
	if (strcmp($reqkeyword4,"")<>0){				
		    if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
			$sql = $sql . " member_type = '$reqkeyword4'";
			$PageString = $PageString  .'&txtkeyword4='.$reqkeyword4;
	 }
}

if ( isset( $_REQUEST['txtkeyword51'] ) && isset( $_REQUEST['txtkeyword52'] ) ){
	$reqkeyword51=$_REQUEST['txtkeyword51'] ;
	$reqkeyword52=$_REQUEST['txtkeyword52'] ;
	$t->assign ( 'reqkeyword51', $reqkeyword51);
	$t->assign ( 'reqkeyword52', $reqkeyword52);
	if ((strcmp($reqkeyword51,"")<>0) &&(strcmp($reqkeyword52,"")<> 0)) {				
			if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
			$sql = $sql . " (tbl_sur_sign_up.join_date >= '". SqlDateIn($reqkeyword51). "' and tbl_sur_sign_up.join_date <= '". SqlDateIn($reqkeyword52) ."')";
			$PageString = $PageString  .'&txtkeyword51='.$reqkeyword51;
			$PageString = $PageString  .'&txtkeyword52='.$reqkeyword52;
	 }
}


if (strcmp($ref_user_code,"")<>0){				
	    if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
		$sql = $sql . " ref_user_code = 0 ";
		$PageString = $PageString  .'&ref_user_code='.$ref_user_code;
}
$sql = $sql . " group by tbl_sur_sign_up.`personnel_id` ";

$tmpblnhaving=0;
if ( isset( $_REQUEST['txtkeyword9'] ) ){
		$reqkeyword9 = $_REQUEST['txtkeyword9'];
	}else{
		$reqkeyword9 = DEFAULT_BRANCH_ID;
	}
//if ( isset( $_REQUEST['txtkeyword9'] ) ){
	//$reqkeyword9=$_REQUEST['txtkeyword9'] ;
	$t->assign ( 'reqkeyword9', $reqkeyword9);
	$PageString = $PageString  .'&txtkeyword9='.$reqkeyword9;
	if (strcmp($reqkeyword9,"")<>0){			
		if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnhaving=1; } else	$sql = $sql . " and ";
		$sql = $sql . " '$reqkeyword9' in (group_concat(main_branch_id)) ";
	}
//}
if ( isset( $_REQUEST['txtkeyword2'] ) ){
	$reqkeyword2=$_REQUEST['txtkeyword2'] ;
	$t->assign ( 'reqkeyword2', $reqkeyword2);
	if (strcmp($reqkeyword2,"")<>0){				
			if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnhaving=1; } else	$sql = $sql . " and ";
			$sql = $sql . " full_name like '%$reqkeyword2%'";
			$PageString = $PageString  .'&txtkeyword2='.$reqkeyword2;
	 }
}

if (strcmp($reqsort,"")<>0)  $sql = $sql . ' order by ' . $reqsort ;
	$sql = $sql . ' ' . $reqord ;

// ---------------- call paging function---------------------------------------------------
   $strfnPagingSql = $sql; include 'includes/callpaging.php'; 
//-----------------------------------------------------------------------------------   

//echo $sql;
$t->assign('sql',$sql);
//$t->assign('PageString',$PageString);
$data = array();
$temp = $db->getAll( $sql );
foreach ($temp as $index=> $row){

	// Thumb Code Function         ###createThumbnail($imageDirectory, $imageName, $thumbDirectory, $ratioFlag, $thumbWidth, $thumbHeight ){
	//if ($row['personnel_logo'] <> '') createThumbnail("doc", $row['personnel_logo'], "doc/thumbs", 0, 80,70);
	$row['join_date'] = sqldateout($row['join_date'] );
	$row['join_time'] = ($row['join_time'] );
	$row['privilege_type_name'] = FnGetValue("tbl_sur_privilege_types","privilege_type_name","lang = 'english' and privilege_type_id = '".$row['privilege_type']."'");
	$row['del_flag'] = FnGetValue("tbl_sur_privilege_types","del_flag","lang = 'english' and privilege_type_id = ".$row['privilege_type']);
	$row['last_login'] = SqlDateOut(FnGetValue("tbl_sur_last_login_details","max(Last_Date_Time)","User_ID =".$row['personnel_id']));
	$row['ref_user_code'] = $row['ref_user_code'];
    $data[] = $row;
}
$t->assign ( 'data', $data );
$t->assign ( 'reqord', $reqord);
$t->assign ( 'PageString', $PageString);

$t->display($user_html_page);

?>