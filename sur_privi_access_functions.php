<? 

// +----------------------------------------------------------------------------------------------------------------------------+
// |  File Name : sur_privilage_access_rights_functions.php
// |  Version : 2.0 
// |  Create-date : 29, March, 2010
// |  Modify-date : 22, April, 2010
// |  Authors : Niteen Borate                        
// | 
// |  Desciption : functions tp check if xyz user has prvilage to access particluar page or not
// |  How to use : 

// |  function FnDelete($dbTable_Name,$dbTable_ID_Name,$dbTable_ID_Value,$flList_Page,$PHP_SELF,$TemplatePage)

// |  function fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID)

// | $t->assign ('enrollment', $enrollment1);
// | {$enrollment.Group_Management} == "YES"
// |
// |  
// +------------------------------------------------------------------------------------------------------------------------------
//


function FnDelete($dbTable_Name,$dbTable_ID_Name,$dbTable_ID_Value,$flList_Page,$PHP_SELF,$TemplatePage){
	$nTemplatePer3 = $TemplatePage;
	if ($nTemplatePer3=="nTemplateBlank.php"){	
		$nTemplatePerFooter ="nTemplateBlank_Footer.php";
		$DispMsg=0;
	}else{										
		$nTemplatePerFooter ="nTemplate2.php";
		$DispMsg=1;
	}
	
	$DeleteAutoCnt = FnGetCount("tbl_sur_p_field_links","Table1='$dbTable_Name'");
	if ($DeleteAutoCnt > 0){
		if (isset($_GET["DelYes"])){
			if (($_GET["DelYes"]) == "Yes"){
				$sql = "select * from tbl_sur_p_field_links where Table1 = '$dbTable_Name' ";		
				$result = mysql_query($sql)	or die("Couldn't execute query FnDelete1.");
				while ($row = mysql_fetch_array($result)){
					$Table1 = $row['Table1'];		
					$Field1 = $row['Field1'];	
					$Table2 = $row['Table2'];	
					$Field2 = $row['Field2'];	
					$Field1Value = FnGetValue($Table1,$Field1,"$dbTable_ID_Name = $dbTable_ID_Value");	
					$sql1 = "delete from $Table2 where $Field2 = '$Field1Value'";
					$result1 = mysql_query($sql1) or die($sql1."Couldn't execute FnDelete2.");
				}
				$sql = "delete from $dbTable_Name where $dbTable_ID_Name = $dbTable_ID_Value";
				$result = mysql_query($sql) or die($sql."Couldn't execute query FnDelete3.");
				$MsgStr = "<tr><td align=center>Records Deleted Successfully !! </td></tr><br><br><tr><td align=center><a href='$flList_Page'> click here to go to list.. </a><td></tr>";
			}else{
				$MsgStr = "<tr><td align=center> Action canceled successfully!! </td></tr><br><br><tr><td align=center><a href='$flList_Page'> click here to go to list.. </a><td></tr>";
			}
			if ($DispMsg==1){
				include("includes/$nTemplatePer3");	
				echo "<table align=top width='100%'><tr><td align=center> $MsgStr </td></tr><br><br></table>";
				include("includes/$nTemplatePerFooter");
			}else{ header("location:  $flList_Page");}
			
		}else{
			$DeleteAutoCnt = FnGetCount("tbl_sur_p_field_links","Table1='$dbTable_Name' and Delete_Auto='No'");
			if ($DeleteAutoCnt > 0){
				$MsgStr = "<tr><td align=center>Impossible to Delete Current Record. There are Related Entries in Other Tables !!! </td></tr><br><br>";
				$MsgStr = $MsgStr . "<tr><td align=center><a href='$flList_Page'> click here to go to list.. </a><td></tr>";
			
				if ($DispMsg==1){
					 include("includes/$nTemplatePer3");	
					 echo "<table align=top width='100%'><tr><td align=center> $MsgStr </td></tr><br><br></table>";
					 include("includes/$nTemplatePerFooter");
				}else{ header("location:  $flList_Page");}				 
			
			}else{
				$DeleteAutoCnt = FnGetCount("tbl_sur_p_field_links","Table1='$dbTable_Name' and Delete_Auto='Ask'");
				if ($DeleteAutoCnt > 0){
					$MsgStr = "<tr><td align=center><b><font color='#FF0000'> ATTENTION!</font></b> There Are Related Entries In Other Tables. <BR> Do You Want To Proceed With Deleting all the Related Entries Also..</td></tr><br><br>";
					$MsgStr = $MsgStr . "<tr><td align=center><B> <a href='$PHP_SELF?DelYes=Yes&$dbTable_ID_Name=$dbTable_ID_Value'><b><font color='#FF0000'> YES. </font></b></a>&nbsp;&nbsp;&nbsp;&nbsp;  <a href='$PHP_SELF?DelYes=No&$dbTable_ID_Name=$dbTable_ID_Value'> NO. </a></b> <td></tr>";
				if ($DispMsg==1){
					include("includes/$nTemplatePer3");	
					echo "<table align=top width='100%'><tr><td align=center> $MsgStr </td></tr><br><br></table>";
					include("includes/nTemplate2.php");
				}else{ header("location:  $flList_Page");}	
			
				}else{
					$sql = "select * from tbl_sur_p_field_links where Table1 = '$dbTable_Name' ";		
					$result = mysql_query($sql)	or die("Couldn't execute query FnDelete4.");

					while ($row = mysql_fetch_array($result)){
						$Table1 = $row['Table1'];		
						$Field1 = $row['Field1'];	
						$Table2 = $row['Table2'];	
						$Field2 = $row['Field2'];	
						$Field1Value = FnGetValue($Table1,$Field1,"$dbTable_ID_Name = $dbTable_ID_Value");	
						$sql1 = "delete from $Table2 where $Field2 = '$Field1Value'";
						$result1 = mysql_query($sql1) or die($sql1."Couldn't execute query FnDelete5.");
					}
					$sql = "delete from $dbTable_Name where $dbTable_ID_Name = $dbTable_ID_Value";
					$result = mysql_query($sql) or die($sql."Couldn't execute query FnDelete6.");
					$MsgStr = "<tr><td align=center>Records Deleted Successfully !! </td></tr><br><br>";
					$MsgStr = $MsgStr . "<tr><td align=center><a href='$flList_Page'> click here to go to list.. </a><td></tr>";

					if ($DispMsg==1){
						include("includes/$nTemplatePer3");	
						echo "<table align=top width='100%'><tr><td align=center> $MsgStr </td></tr><br><br></table>";
						include("includes/$nTemplatePerFooter");
					}else{ header("location:  $flList_Page");}	
				 }
			}
		}  
	}else{
		$sql = "delete from $dbTable_Name where $dbTable_ID_Name = $dbTable_ID_Value";
		$result = mysql_query($sql) or die($sql."Couldn't execute query FnDelete7.");
		$MsgStr = "Record Deleted Successfuly...<br>";
		
		
		if ($DispMsg==1){
		//echo $TemplatePage."====".$nTemplatePerFooter;
		//include("includes/$TemplatePage");
		echo "$MsgStr ";
		//include("includes/$nTemplatePerFooter");
		}else{ header("location:  $flList_Page");}	
	}
}


function fnChkInUpdate($reqTable,$reqField)
{
	$sql = "select * from tbl_sur_p_field_links where Table1  ='$reqTable' and Field1 = '$reqField' ";
	$result = mysql_query($sql)	or die("Couldn't execute query Chk In Update.");
	$reqUpdate="No";
	while ($row = mysql_fetch_array($result)){ 
		 	return "Yes";
	}
} 



function FnUpdate2($dbTable_Name,$dbTable_ID_Name,$dbTable_ID_Value,$AllVar,$AllVar1)
{

 $flList_Page = $_SESSION["flList_Page"];
 $DeleteAutoCnt = FnGetCount("tbl_sur_p_field_links","Table1='$dbTable_Name'");
 if ($DeleteAutoCnt > 0)
 {
	if (isset($_GET["UpdateYes"]))
	{
	 	if (($_GET["UpdateYes"]) == "Yes")
		{  
		 	fnUpdateAll($dbTable_Name,$dbTable_ID_Name,$dbTable_ID_Value,$AllVar,$AllVar1);
			//$MsgStr="Updated";
			//$MsgStr = "<tr><td align=center>Records Updated Successfuly !! </td></tr><br><br>";
			//$MsgStr = $MsgStr . "<tr><td align=center><a href='$flList_Page'> click here to go to list.. </a><td></tr>";
	    }
	    else
	    {
	 		$MsgStr = "<tr><td align=center> Action canceled successfully!! </td></tr><br><br>";
	  	    $MsgStr = $MsgStr . "<tr><td align=center><a href='$flList_Page'> click here to go to list.. </a><td></tr>";
 	    }
  	}		 
    else
    {	
		 $i = 0;
		 $DeleteAutoCnt = 0;
		 $cntAllVar = sizeof($AllVar);
 		 $cntAllVar1 = sizeof($AllVar1);
		 while ($i < $cntAllVar)
		 {
			 $DeleteAutoCnt = $DeleteAutoCnt + FnGetCount("tbl_sur_p_field_links","Table1='$dbTable_Name' and Field1='$cntAllVar[$i]' and Update_Auto='No'");
			 $i = $i + 1;	
		 }	 
		 if ($DeleteAutoCnt > 0)
		 {
				$MsgStr = "<tr><td align=center>Impossible to Update Current Record. There are Related Entries in Other Tables !!! </td></tr><br><br>";
		  	    $MsgStr = $MsgStr . "<tr><td align=center><a href='$flList_Page'> click here to go to list.. </a><td></tr>";
		 }
		 else
		 {
				 $i = 0;
				 $DeleteAutoCnt = 0;
				 $cntAllVar = sizeof($AllVar);
		 		 $cntAllVar1 = sizeof($AllVar1);
				 
				 while ($i < $cntAllVar)
				 {
					 $DeleteAutoCnt = $DeleteAutoCnt + FnGetCount("tbl_sur_p_field_links","Table1='$dbTable_Name' and Field1='$AllVar[$i]' and Update_Auto='Ask'");
		 			 $i = $i + 1;	
				 }	 
				 if ($DeleteAutoCnt > 0)
				 {
					  $MsgStr = "<tr><td align=center><b><font color='#FF0000'> ATTENTION!</font></b> There Are Related Entries In Other Tables. <BR> Do You Want To Proceed With Updating all the Related Entries Also..</td></tr><br><br>";
				      $MsgStr = $MsgStr . "<tr><td align=center><B> <a href='$PHP_SELF?submit=1&submitUp=1&UpdateYes=Yes&$dbTable_ID_Name=$dbTable_ID_Value'><b><font color='#FF0000'> YES. </font></b></a>&nbsp;&nbsp;&nbsp;&nbsp;  <a href='$PHP_SELF?submit=1&UpdateYes=No&$dbTable_ID_Name=$dbTable_ID_Value'> NO. </a></b> <td></tr>";
			 	 }
				 else
				 {
					 $i = 0;
					 $DeleteAutoCnt = 0;
					 $cntAllVar = sizeof($AllVar);
		 			 $cntAllVar1 = sizeof($AllVar1);
				 
					 while ($i < $cntAllVar)
					 {
						 $DeleteAutoCnt = $DeleteAutoCnt + FnGetCount("tbl_sur_p_field_links","Table1='$dbTable_Name' and Field1='$AllVar[$i]' and Update_Auto='Yes'");
			 			 $i = $i + 1;	
					 }	 
					 if ($DeleteAutoCnt > 0){
					 	fnUpdateAll($dbTable_Name,$dbTable_ID_Name,$dbTable_ID_Value,$AllVar,$AllVar1);
						//$MsgStr = "<tr><td align=center>Records Updated Successfuly !! </td></tr><br><br>";
						//$MsgStr = $MsgStr . "<tr><td align=center><a href='$flList_Page'> click here to go to list.. </a><td></tr>";
						return; 
						}else { return; }
				 }
	       }
      }  
 } 
 else
 {
 	return;
 }
 
 return $MsgStr; 
}// end of fn delete function



function fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID){

	//return "YES";
	if ($_SESSION["privilege_type"] == "sadmin")  {
	
		return "YES";
		
	}else{
		$sql = "select * from tbl_sur_p_privileges where page ='$reqCurrPage' and Privilege_Type_ID = '$tmpPrivilege_Type_ID'";		
		$result = mysql_query($sql) or die(mysql_error())	;

		if($result == ""){ ?>
			<script language="javascript"> window.location.href="Session_expires.php?pageurl='<?=$PHP_SELF?>'"; </script><?
		}else{
			$reqPrivilage="NO";
			while ($row = mysql_fetch_array($result)){ 
				 $reqPrivilage = $row["Privileges1"]; 
				 if ($reqPrivilage == "FILTERED") { 
					$reqFilter  = $row["Filter"]; 
					$reqTables  = $row["Tables"];
				 }
			}
			
			if ($reqPrivilage == "NO"){ return "NO";} 
			if ($reqPrivilage == "YES"){ return "YES";} 
			
			if ($reqPrivilage == "FILTERED"){ /*	$curUserID   		= $_SESSION["User_ID"];
				$reqFilter=str_replace("curUser_ID",$curUserID,$reqFilter);		
				if (isset($_SESSION["Doctor_ID_Fillter"])){			
					$curDoctor_ID		= $_SESSION["Doctor_ID_Fillter"];
					$reqFilter=str_replace("curDoctor_ID",$curDoctor_ID,$reqFilter);
					}elseif (isset($_SESSION["Doctor_ID"])){			
					$curDoctor_ID		= $_SESSION["Doctor_ID"];
					$reqFilter=str_replace("curDoctor_ID",$curDoctor_ID,$reqFilter);
					}
				if (isset($_SESSION["Patient_ID_Fillter"])){
					$curPatient_ID    = $_SESSION["Patient_ID_Fillter"];
					$reqFilter=str_replace("curPatient_ID",$curPatient_ID,$reqFilter);
					}elseif (isset($_SESSION["Patient_ID"])){
					$curPatient_ID    = $_SESSION["Patient_ID"];
					$reqFilter=str_replace("curPatient_ID",$curPatient_ID,$reqFilter);
					}
				if (isset($_SESSION["Staff_ID_Fillter"])){
					$curStaff_ID    = $_SESSION["Staff_ID_Fillter"];
					$reqFilter=str_replace("curStaff_ID",$curStaff_ID,$reqFilter);
					}elseif (isset($_SESSION["Staff_ID"])){
					$curStaff_ID    = $_SESSION["Staff_ID"];
					$reqFilter=str_replace("curStaff_ID",$curStaff_ID,$reqFilter);
					}
				$sql = " select * from $reqTables where " . $reqFilter;
				$resultFilter = mysql_query($sql) or die($sql."<br> Couldn't execute query Page Access Qry.");
				$totalreturned = mysql_num_rows($resultFilter); 
				$totalreturned = fnGetCount($reqTables,$reqFilter);
				if ($totalreturned == 0){
					return "NO";
				}  */	} 
				
			return "YES";
		}
	}
} // end of function




// Access Menu
/*	$access_prev_arr = array();
	//$access_prev_arr["adv_prev_sales_order"] 	= fnPageAccess("adv_prev_sales_order",$_SESSION["privilege_type_id"]);
	$access_prev_arr["all_edit_delete"] 	= fnPageAccess("all_edit_delete",$_SESSION["privilege_type_id"]);
	$t->assign ( 'access_prev_arr', $access_prev_arr);*/

	/*$masters = array();
	$masters1 = array();
	
	$reqCurrPage = "Master";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$masters1['Master'] = $reqIsAccess ;	

	$reqCurrPage = "Company_Profile";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$masters1['Company_Profile'] = $reqIsAccess ;	
	
	$reqCurrPage = "Director_ShareHolder_Member";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$masters1['Director_ShareHolder_Member'] = $reqIsAccess ;	
	
	$reqCurrPage = "Branch_List";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$masters1['Branch_List'] = $reqIsAccess ;
	
	$reqCurrPage = "Area_List";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$masters1['Area_List'] = $reqIsAccess ;	
	
	$reqCurrPage = "Collection_Code_List";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$masters1['Collection_Code_List'] = $reqIsAccess ;
	
	$reqCurrPage = "Bank_List";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$masters1['Bank_List'] = $reqIsAccess ;	
	
	$reqCurrPage = "Master_Code_List";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$masters1['Master_Code_List'] = $reqIsAccess ;	
	
	$reqCurrPage = "Introducer_List";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$masters1['Introducer_List'] = $reqIsAccess ;	
	
	$reqCurrPage = "Introducer_Type_List";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$masters1['Introducer_Type_List'] = $reqIsAccess ;
	
	$reqCurrPage = "Cordinator_Code_List";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$masters1['Cordinator_Code_List'] = $reqIsAccess ;	
	
	$t->assign ('masters', $masters1);
	
	$enrollment  = array();
	$enrollment1 = array();
	
	$reqCurrPage = "Enrollment";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$enrollment1['Enrollment'] = $reqIsAccess ;
	
	$reqCurrPage = "Group_Management";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$enrollment1['Group_Management'] = $reqIsAccess ;
	
	$reqCurrPage = "Group_Forming";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$enrollment1['Group_Forming'] = $reqIsAccess ;	
	
	$reqCurrPage = "Group_Enrollment";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$enrollment1['Group_Enrollment'] = $reqIsAccess ;
	
	$reqCurrPage = "Applicant_Substitution";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$enrollment1['Applicant_Substitution'] = $reqIsAccess ;
		
	$t->assign ('enrollment', $enrollment1);
	
	$entry  = array();
	$entry1 = array();
	
	$reqCurrPage = "Entry";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$entry1['Entry'] = $reqIsAccess ;
	
	$reqCurrPage = "Priza_Entry";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$entry1['Priza_Entry'] = $reqIsAccess ;
	
	$reqCurrPage = "Intimation_Entry";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$entry1['Intimation_Entry'] = $reqIsAccess ;
	
	$reqCurrPage = "Subscription_Entry";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$entry1['Subscription_Entry'] = $reqIsAccess ;
	
	$reqCurrPage = "Recipt_List";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$entry1['Recipt_List'] = $reqIsAccess ;
	
	$reqCurrPage = "Intimation_Print";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$entry1['Intimation_Print'] = $reqIsAccess ;
	$t->assign ('entry', $entry1);

    $bank_deposite  = array();
	$bank_deposite1 = array();
	
	$reqCurrPage = "Bank_Deposit_Header";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$bank_deposite1['Bank_Deposit_Header'] = $reqIsAccess ;
	
	$reqCurrPage = "Bank_Deposite";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$bank_deposite1['Bank_Deposite'] = $reqIsAccess ;
	
	$reqCurrPage = "Bank_Deposite_Print";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$bank_deposite1['Bank_Deposite_Print'] = $reqIsAccess ;

    $t->assign ('bank_deposite', $bank_deposite1);
	
	$print  = array();
	$print1 = array();
	
	$reqCurrPage = "Print";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$print1['Print'] = $reqIsAccess ;
	
	$reqCurrPage = "Groupwise_List";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$print1['Groupwise_List'] = $reqIsAccess ;
	
	$reqCurrPage = "Outstading_Report";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$print1['Outstading_Report'] = $reqIsAccess ;
	
	$reqCurrPage = "Outstading_Report_GroupWise";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$print1['Outstading_Report_GroupWise'] = $reqIsAccess ;
	
	$reqCurrPage = "Outstading_Report_BranchWise";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$print1['Outstading_Report_BranchWise'] = $reqIsAccess ;
	
	$reqCurrPage = "Incentive_Report";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$print1['Incentive_Report'] = $reqIsAccess ;
	
	$reqCurrPage = "Minutes_Report";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$print1['Minutes_Report'] = $reqIsAccess ;
	
	$reqCurrPage = "Agreement_Ledger_Report";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$print1['Agreement_Ledger_Report'] = $reqIsAccess ;
	
	$reqCurrPage = "Master_Code_Report";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$print1['Master_Code_Report'] = $reqIsAccess ;	
	
	$reqCurrPage = "No_Of_Collection";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$print1['No_Of_Collection'] = $reqIsAccess ;
	
	$reqCurrPage = "Month_Wise";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$print1['Month_Wise'] = $reqIsAccess ;	
	
	$reqCurrPage = "chit_payment_slip";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$print1['chit_payment_slip'] = $reqIsAccess ;		
	
	$t->assign ('print', $print1);
	
	$option  = array();
	$option1 = array();
	
	$reqCurrPage = "Option";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$option1['Option'] = $reqIsAccess ;
	
	$reqCurrPage = "Section_List";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$option1['Section_List'] = $reqIsAccess ;
	
	$reqCurrPage = "Application_Pages";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$option1['Application_Pages'] = $reqIsAccess ;
	
	$reqCurrPage = "Privilege_Type_List";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$option1['Privilege_Type_List'] = $reqIsAccess ;
	
	$reqCurrPage = "View_Assigned_Privileges";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$option1['View_Assigned_Privileges'] = $reqIsAccess ;
	
	$reqCurrPage = "Make_copy_of_privilege";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$option1['Make_copy_of_privilege'] = $reqIsAccess ;	
	
	$t->assign ('option', $option1);
		
	$Company_Profile = array();
	$Company_Profile1 = array();
	
	$reqCurrPage = "Company_Profile_Add";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$Company_Profile1['Company_Profile_Add'] = $reqIsAccess ;
	
	$reqCurrPage = "Company_Profile_Edit";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$Company_Profile1['Company_Profile_Edit'] = $reqIsAccess ;
	
	$reqCurrPage = "Company_Profile_Del";
	$tmpPrivilege_Type_ID =  $_SESSION["privilege_type_id"];
	$reqIsAccess = fnPageAccess($reqCurrPage,$tmpPrivilege_Type_ID);
	$Company_Profile1['Company_Profile_Del'] = $reqIsAccess ;	
	
	$t->assign ('Company_Profile', $Company_Profile1); 	*/


?>