<?php

include_once ("init.php");
include_once( 'sur_adm_permission.php' );
include_once( 'controllers/lgl_report_draft/classreport_draft.php' );

header('Content-Type: text/html; charset=utf-8');  
$report_draft = new report_draft();
$StatusMessage = "";
switch($_GET['action']){
	case '1' :		
					$report_draft->Addreport_draft();							
					break;	
	case '2' :		
					$report_draft->Editreport_draft();					
					break;	
					
	case '3' :		
					$report_draft->store();					
					break;	
	
	case '4':	
	case 'destroy':	
					$report_draft->destroy($_REQUEST['report_draft_id']);	
					$_SESSION['AlertMessage'] = "Record Deleted Sucessfully";	
					header('Location: lgl_report_draft.php');
					break;	
					
	case '5' :		
					$report_draft->update();	
					break;	
	default:		
					$report_draft->index();
					break;
}

//==========================#  End of Getting records in list page===================================================================
?>