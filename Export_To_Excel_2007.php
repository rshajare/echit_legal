<? 
set_time_limit(0);
ini_set('memory_limit','200M');
include( 'sur_adm_permission.php' );
//include_once("includes/common.php");
//include_once("includes/other_common_function.php");
require_once 'libs/PHPExcel/PHPExcel.php';

$req_Col_array = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
//print_r($_SESSION['xls_report']);

// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");


$req_no_cols = count($_SESSION['xls_report']['Col']);

$req_heading 	= $_SESSION['xls_report']['Heading'];
$reqFromDate 	= $_SESSION['xls_report']['Date']['From'] ;
$reqToDate 		= $_SESSION['xls_report']['Date']['To'] ;

$req_Header = "";
for( $cnt = 0; $cnt < count($_SESSION['xls_report']['Search']); $cnt++ ){
	$req_Header .= $_SESSION['xls_report']['Search'][$cnt];
}
$req_Header_date = 'For the period :  From '.$reqFromDate.' To '.$reqToDate;


// Add some data
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setCellValue('A1', $req_heading);
$objPHPExcel->getActiveSheet()->setCellValue('A2', $req_Header);
$objPHPExcel->getActiveSheet()->setCellValue('A3', $req_Header_date );

$objPHPExcel->getActiveSheet()->mergeCells('A1:'.$req_Col_array[$req_no_cols].'1');
$objPHPExcel->getActiveSheet()->mergeCells('A2:'.$req_Col_array[$req_no_cols].'2');
$objPHPExcel->getActiveSheet()->mergeCells('A3:'.$req_Col_array[$req_no_cols].'3');

$row_cnt = 5;
for( $cnt = 0; $cnt < count($_SESSION['xls_report']['Col']); $cnt++ ){
	$col_cnt = $req_Col_array[$cnt].$row_cnt;
	$objPHPExcel->getActiveSheet()->setCellValue($col_cnt,$_SESSION['xls_report']['Col'][$cnt]);
	$objPHPExcel->getActiveSheet()->getStyle($col_cnt)->getFont()->setBold(true);	 
	$objPHPExcel->getActiveSheet()->getColumnDimension($req_Col_array[$cnt])->setAutoSize(true);
}

$objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 5);
$row_cnt = 6;

for( $header_cnt = 0; $header_cnt < count($_SESSION['xls_report']['Data_row']); $header_cnt++ ){
	for( $cnt = 0; $cnt < count($_SESSION['xls_report']['Col']); $cnt++ ){
		$col_cnt = $req_Col_array[$cnt].$row_cnt;
		$objPHPExcel->getActiveSheet()->setCellValue($col_cnt,$_SESSION['xls_report']['Data_row'][$header_cnt][$cnt]);	 
	}
	$row_cnt++;
}



$objPHPExcel->getActiveSheet()->setTitle('Sheet1');

if(PDF != 1){ 
	// Redirect output to a client�s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.str_replace(' ','_',trim($req_heading)).date("dmYhis").'.xlsx"');
	header('Cache-Control: max-age=0');
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
}
?>