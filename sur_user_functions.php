<?php
if ( !defined( 'SMARTY_DIR' ) ){include_once( 'init.php' );}

// +----------------------------------------------------------------------------------------------------------------------------+
// |  File Name : sur_user_functions.php
// |  Version : 3.0 
// |  Create-date : 29, May, 2013
// |  Modify-date : 
// |  Authors : Niteen Borate                        
// | 
// |  Desciption : functions for internal message comminication
// |  How to use : 
// |
// |  function fnUsrPassReset($reqpersonnel_id)
// |  function fnAddEditUser($ref_user_code,$ref_user_type,$name,$email,$mobile,$login='',$password=''){  //ref_user_type = subscriber, ref_user_code = subid
// |  function fnDelUser($tmpPersonnel_ID){
// |  function fnActivateUser($tmpPersonnel_ID,$act){
// |  function fnUserSignUp($reqpersonnel_id,$new_pass){  signup email 
// +------------------------------------------------------------------------------------------------------------------------------
//


 /**********************************/
// Set user details to user arry
/**********************************/
/*$user_details = array();
$user = array();
if($_SESSION["personnel_id"]<>''){
    $reqPersonnel_ID =	$_SESSION["personnel_id"];
	$user_details['personnel_id'] = $reqPersonnel_ID;
	$user_details['personnel_logo'] = FnGetValue("tbl_sur_sign_up","personnel_logo","personnel_id= $reqPersonnel_ID"); 
	$user_details['mr_mrs'] = FnGetValue("tbl_sur_sign_up","mr_mrs","personnel_id= $reqPersonnel_ID");  
	$pri_type = FnGetValue("tbl_sur_sign_up","privilege_type","personnel_id= $reqPersonnel_ID");
	$user_details['type'] = FnGetValue("tbl_sur_privilege_types","privilege_type_name","privilege_type_id = ".$pri_type);
	
	$_SESSION["privilege_type"] = $user_details['type'];	
}else{
   $reqPersonnel_ID=0;
}

$t->assign ( 'personnel_id', $reqPersonnel_ID );
$t->assign ('user', $user_details);

*/

 /**********************************/
// Set user details to user arry
/**********************************/
$user_details = array();
if($_SESSION["personnel_id"]<>''){

	$user_details['personnel_id']   = $_SESSION["personnel_id"];
	$user_details['type']		    = $_SESSION["privilege_type"];
	$user_details['type_id'] 		= $_SESSION["privilege_type_id"]; 	
	$user_details['personnel_id'] 	= $_SESSION["personnel_id"]; 		
	$user_details['full_name']  	= $_SESSION["first_name"]; 			
	$join_date 						= isset($_SESSION["join_date"])? $_SESSION["join_date"]:'';	
	$user_details['join_date'] 	 	= SQLDateOut($join_date);
		
}else{
   $reqPersonnel_ID=0;
}

$t->assign ('personnel_id', isset($reqPersonnel_ID)? $reqPersonnel_ID: 0 );
$t->assign ('user', $user_details);

//echo $_SESSION["privilege_type"];

// delete user
function fnDelUser($tmpPersonnel_ID){
	if ($tmpPersonnel_ID <> ''){
		$privilege_type_id = FnGetValue("tbl_sur_sign_up","privilege_type","personnel_id = $tmpPersonnel_ID");
		$del_flag = FnGetValue("tbl_sur_privilege_types","del_flag","privilege_type_id = $privilege_type_id");
		if( ($_SESSION['db_admin'] == 1) && ($del_flag == 1)) {
				$sqlins = "delete from tbl_sur_sign_up where personnel_id=$tmpPersonnel_ID";
				$res_sql = mysql_query($sqlins) or die(mysql_error().$sqlins);
				
				$sqlins = "delete from tbl_sur_sign_up_other WHERE personnel_id= $tmpPersonnel_ID";
				$res_sql = mysql_query($sqlins) or die(mysql_error().$sqlins);
				return "User Deleted Sucessfully";
			}{
				return "Need super admin access rights to delete user";
			}	
		 }	
}// delete user



// function to add/update new user
function fnAddEditUser($ref_user_code,$ref_user_type,$name,$email,$mobile,$main_branch_id='',$login='',$password=''){  //ref_user_type = subscriber, ref_user_code = subid

	$reqprivilege_type  = FnGetValue("tbl_sur_privilege_types","main_privilege_type_id","privilege_type_name = 'subscriber'");
	$tmpUserExist 		= fnGetCount("tbl_sur_sign_up","ref_user_code='$ref_user_code' and ref_user_type='$ref_user_type'");
	
	//echo $tmpUserExist;
	//exit;
	if ($tmpUserExist == 1){ // if user exist update
		if ($password<>'') { $sqlpass = ",password=MD5('$password')";}else{$sqlpass = "";}
		if ($login<>'') { $sqllogin = ",login='$login'";}else{$sqllogin = "";}
		$sqlup = "UPDATE tbl_sur_sign_up SET first_name= '$name', email = '$email', mobile_phone_no = '$mobile' , approved='1' $sqllogin $sqlpass  WHERE ref_user_code= '$ref_user_code' and ref_user_type = '$ref_user_type'"; 
	    $result = mysql_query($sqlup) or die($sqlup.mysql_error().'can not execute');

		$reqpersonnel_id = fnGetValue("tbl_sur_sign_up","personnel_id","ref_user_code='$ref_user_code'");		
		$tmpUserBranchExist = fnGetCount("tbl_sur_sign_up_branches","personnel_id='$reqpersonnel_id' and main_branch_id='$main_branch_id'");
		if ($tmpUserBranchExist ==0){
				$sql_insert_branch="INSERT INTO `tbl_sur_sign_up_branches` (`sur_sign_up_branches_id` ,`personnel_id` ,`main_branch_id`)	VALUES (		
				NULL,'".$reqpersonnel_id."','".$main_branch_id."')";
				$result = mysql_query($sql_insert_branch) or die($sql_insert_branch.mysql_error().'can not execute');	
		}	
		//echo $sql_insert_branch;
		//exit;
		
	}if ($tmpUserExist == 0){ // if user not exist add new one
		
		if ($password == '') $password = rand (100, 1000);
		$sqlins = "insert into tbl_sur_sign_up (personnel_id,ref_user_code,ref_user_type, first_name,email,mobile_phone_no,approved,join_date,login,password,privilege_type,sms_flag) 
									values (NULL,'$ref_user_code','$ref_user_type', '$name','$email','$mobile','1',CURDATE(),'$login',MD5($password),$reqprivilege_type,1)"; 
		$result = mysql_query($sqlins) or die($sqlins.mysql_error().'can not execute');
		$reqpersonnel_id = mysql_insert_id();  // Get last inserted id
		
		$sql_insert_branch="INSERT INTO `tbl_sur_sign_up_branches` (`sur_sign_up_branches_id` ,`personnel_id` ,`main_branch_id`)	VALUES (		
			NULL,'".$reqpersonnel_id."','".$main_branch_id."')";
		$result = mysql_query($sql_insert_branch) or die($sql_insert_branch.mysql_error().'can not execute');	
		
		
		
		//echo $sqlins;
		//exit;
	}	
	
		
}	

// fnActivateUser user
function fnActivateUser($tmpPersonnel_ID,$act){

	$privilege_type_id = FnGetValue("tbl_sur_sign_up","privilege_type","personnel_id = $tmpPersonnel_ID");
	$del_flag = FnGetValue("tbl_sur_privilege_types","del_flag","privilege_type_id = $privilege_type_id");
	$goFlag=1;
	//echo $privilege_type_id .$del_flag;
	//exit;
	
	if($del_flag == 0) {
		if ($_SESSION['db_admin'] == 1) 
			$goFlag=1;
		else 
			$goFlag=0;
	}
	
	if ( ($tmpPersonnel_ID <> '') && ($goFlag == 1)) {
			$sql = "Update tbl_sur_sign_up set approved = $act where personnel_id=$tmpPersonnel_ID";
			$result = mysql_query($sql)	or die("Couldn't execute query.");
			
			global $lang; global $t;
			
			if ($act==1){
				 $account_status =  "Activated Successfully now you can login to system ";
			}else{
				 $account_status =  "DeActivated";
			} 
			
			$t->assign('act', $act);
		
			//send email
			$from_user_id = $_SESSION['personnel_id'];
			$to_user_id = $tmpPersonnel_ID;  //change only this value
			$firstname = FnGetValue("tbl_sur_sign_up","first_name","personnel_id = " . $to_user_id);
			$lastname = FnGetValue("tbl_sur_sign_up","last_name","personnel_id = " . $to_user_id);
			$to = fnGetValue("tbl_sur_sign_up","email","personnel_id = ".$to_user_id);
			$def_lang = fnGetValue("tbl_sur_sign_up","def_lang","personnel_id = ".$to_user_id);
			
			if ($def_lang =='') $def_lang='english';
			$t->assign('firstname', $firstname); $t->assign('lastname', $lastname); $t->assign('email', $to);
			$t->assign('account_status', $account_status);
			
			
			$msg = $t->fetch('emailtemplates/account_activate_'.$def_lang.'.html');
			if (eregi ("<title>(.*)</title>", $msg, $out)) { $subject = $out[1];  }
			fnSend_Email_Pear($to,$from,$subject,$msg,$attch_id);
			
			if ($act==1) return "User Activated Successfully";
			else	     return "User DeActivated Successfully";
		}else{
			return "Need super admin access rights to update user";
		
		}
		
		//end email
	
}// delete user



// fnActivateUser user
function fnSMS_FlagUser($tmpPersonnel_ID,$act){
	if ($tmpPersonnel_ID <> ''){
				$sql = "Update tbl_sur_sign_up set sms_flag = $act where personnel_id=$tmpPersonnel_ID";
				$result = mysql_query($sql)	or die($sql."Couldn't execute query.");

		}
}// delete user



// fnBlkEmailUser user
function fnBlkEmailUser($tmpPersonnel_ID,$act){
	if ($tmpPersonnel_ID <> ''){
			$blkemail = FnGetValue("tbl_sur_sign_up","email","personnel_id = $tmpPersonnel_ID");
			$sql = "Update tbl_sur_sign_up set blk_email = $act where email = '$blkemail'";
			$result = mysql_query($sql)	or die("Couldn't execute query.");
			//echo $sql;
	}		
}// delete user



// delete user
function fnBlkUser($tmpPersonnel_ID,$act){
	if ($tmpPersonnel_ID <> ''){
		$sql = "Update tbl_sur_sign_up set blk_usr = $act where personnel_id=$tmpval_personnel_id";
		$result = mysql_query($sql)	or die("Couldn't execute query.");
	}	
}// delete user




// reset pass
function fnUserPassReset($reqpersonnel_id){

		global $lang; global $t;
		$reqlogin 	  = FnGetValue("tbl_sur_sign_up","login","personnel_id=$reqpersonnel_id");
		$reqpassword  = FnGetValue("tbl_sur_sign_up","password","personnel_id=$reqpersonnel_id");
		$new_pass  = substr($reqpassword,1,5);
		$t->assign('new_pass', $new_pass);
		$t->assign('reqlogin', $reqlogin);
		
		//send email
		$from_user_id = $_SESSION['personnel_id'];
		$to_user_id = $reqpersonnel_id;  // change only this value
		$firstname = FnGetValue("tbl_sur_sign_up","first_name","personnel_id = " . $to_user_id);
		$lastname = FnGetValue("tbl_sur_sign_up","last_name","personnel_id = " . $to_user_id);
		$to = fnGetValue("tbl_sur_sign_up","email","personnel_id = ".$to_user_id);
		$def_lang = fnGetValue("tbl_sur_sign_up","def_lang","personnel_id = ".$to_user_id);
		if ($def_lang =='') $def_lang='english';
		$t->assign('firstname', $firstname); $t->assign('lastname', $lastname); $t->assign('email', $to);
		
		
		$msg = $t->fetch('emailtemplates/pass_reset_'.$def_lang.'.html');
		if (eregi ("<title>(.*)</title>", $msg, $out)) { $subject = $out[1];  }
		fnSend_Email_Pear($to,$from,$subject,$msg,$attch_id);
		
		// end email
		
		$sqlins = "UPDATE tbl_sur_sign_up SET password= MD5('$new_pass') WHERE personnel_id= $reqpersonnel_id"; 
	    $result = mysql_query($sqlins) or die(mysql_error().'can not execute');
	
}// end of reset pass



// change pass

function fnUserPassChange($reqpersonnel_id,$new_pass){

		global $lang; global $t;
		$reqlogin 	  = FnGetValue("tbl_sur_sign_up","login","personnel_id=$reqpersonnel_id");
		$t->assign('new_pass', $new_pass);
		$t->assign('reqlogin', $reqlogin);
		
		//send email
		$from_user_id = $_SESSION['personnel_id'];
		$to_user_id = $reqpersonnel_id;  // change only this value
		$firstname = FnGetValue("tbl_sur_sign_up","first_name","personnel_id = " . $to_user_id);
		$lastname = FnGetValue("tbl_sur_sign_up","last_name","personnel_id = " . $to_user_id);
		$to = fnGetValue("tbl_sur_sign_up","email","personnel_id = ".$to_user_id);
		$def_lang = fnGetValue("tbl_sur_sign_up","def_lang","personnel_id = ".$to_user_id);
		if ($def_lang =='') $def_lang='english';
		$t->assign('firstname', $firstname); $t->assign('lastname', $lastname); $t->assign('email', $to);
		
		$msg = $t->fetch('emailtemplates/pass_reset_'.$def_lang.'.html');
		if (eregi ("<title>(.*)</title>", $msg, $out)) { $subject = $out[1];  }
		fnSend_Email_Pear($to,$from,$subject,$msg,$attch_id);
		// end email
		
		$sqlins = "UPDATE tbl_sur_sign_up SET password= MD5('$new_pass') WHERE personnel_id= $reqpersonnel_id"; 
	    $result = mysql_query($sqlins) or die(mysql_error().'can not execute');
	
}// end of change pass


// new gistration email
function fnUserSignUp($reqpersonnel_id,$new_pass){

		global $lang; global $t;
		$reqlogin 	  = FnGetValue("tbl_sur_sign_up","login","personnel_id=$reqpersonnel_id");
		$req_Session_id = FnGetValue("tbl_sur_sign_up","unique_id","personnel_id = " . $reqpersonnel_id);
		
		$t->assign('req_Session_id', $req_Session_id);
		$t->assign('new_pass', $new_pass);
		$t->assign('reqlogin', $reqlogin);
		
		$t->assign('reqpersonnel_id', $reqpersonnel_id);
		
		//send email
		$from_user_id = $_SESSION['personnel_id'];
		$to_user_id = $reqpersonnel_id;  // change only this value
		$firstname = FnGetValue("tbl_sur_sign_up","first_name","personnel_id = " . $to_user_id);
		$lastname = FnGetValue("tbl_sur_sign_up","last_name","personnel_id = " . $to_user_id);
		$to = fnGetValue("tbl_sur_sign_up","email","personnel_id = ".$to_user_id);
		$def_lang = fnGetValue("tbl_sur_sign_up","def_lang","personnel_id = ".$to_user_id);
		if ($def_lang =='') $def_lang='english';
		$t->assign('firstname', $firstname); $t->assign('lastname', $lastname); $t->assign('email', $to);
		$t->assign('to', $to);
		
		// send to user
		$msg = $t->fetch('emailtemplates/user_signup_'.$def_lang.'.html');
		if (eregi ("<title>(.*)</title>", $msg, $out)) { $subject = $out[1];  }
		fnSend_Email_Pear($to,$from,$subject,$msg,$attch_id);
		
		// send to admin	
		$to_admin    = FnGetValue("tbl_mn_app_setting","field_value","field_name='admin_email_id'");
		$msg = $t->fetch('emailtemplates/user_signup_admin_'.$def_lang.'.html');
		if (eregi ("<title>(.*)</title>", $msg, $out)) { $subject = $out[1];  }
		fnSend_Email_Pear($to_admin,$from,$subject,$msg,$attch_id);

		// end email
		
	
}// end of reset pass




//===========Send birthday reminder to studetn===================
function send_birthday_reminder_email($reqpersonnelid){

	
		global $lang; global $t;
		$t->assign('act', $act);
	
		//send email
		$from_user_id = $_SESSION['personnel_id'];
		$to_user_id = $reqpersonnelid;  //change only this value
		$firstname = FnGetValue("tbl_sur_sign_up","first_name","personnel_id = " . $to_user_id);
		$lastname = FnGetValue("tbl_sur_sign_up","last_name","personnel_id = " . $to_user_id);
		$to = fnGetValue("tbl_sur_sign_up","email","personnel_id = ".$to_user_id);
		$def_lang = fnGetValue("tbl_sur_sign_up","def_lang","personnel_id = ".$to_user_id);
		if ($def_lang =='') $def_lang='english';
		$t->assign('firstname', $firstname); $t->assign('lastname', $lastname); $t->assign('email', $to);
		
		$msg = $t->fetch('emailtemplates/birth_day_reminder_'.$def_lang.'.html');
		if (eregi ("<title>(.*)</title>", $msg, $out)) { $subject = $out[1];  }
		fnSend_Email_Pear($to,$from,$subject,$msg,$attch_id);
}		
		

function send_birthday_reminder($flag){
	$tmplocal = $flag;
	$reqcur_month_day = date("m-d");
	$reqfrm_email  = FnGetValue("tbl_app_setting","field_value","field_name='admin_email_id'");
	$sql_first  = "select * from tbl_sign_up where RIGHT(`birth_date`,5) = '$reqcur_month_day'";
	$temp_first = mysql_query($sql_first) or die(mysql_error().$sql_first);
	while($first_row = mysql_fetch_array($temp_first)){
		$reqpersonnelid   = $first_row['personnel_id'];
		$reqemail 	  = $first_row['email'];
		$reqbirthdate = SqlDateOut($first_row['birth_date']);
		$reqfull_name = $first_row['first_name']." ".$first_row['last_name'];
		send_birthday_reminder_email($reqpersonnelid);		
	}
}
//===========End of Send birthday reminder to studetn===================

?>