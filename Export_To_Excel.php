<? 
set_time_limit(0);
ini_set('memory_limit','200M');
include( 'sur_adm_permission.php' );

set_include_path(dirname(__FILE__).'/libs/Pear/');
require_once 'Spreadsheet/Excel/Writer.php';

$workbook = new Spreadsheet_Excel_Writer();
$worksheet =& $workbook->addWorksheet();

//$format_bold =& $workbook->addFormat();
$format_bold =& $workbook->addFormat(array('fgcolor' => 'cyan', 'pattern' => 1, 'size' => 13 ,'align' => 'center'));
$format_bold->setBold();

$req_heading 	= $_SESSION['xls_report']['Heading'];
$reqFromDate 	= $_SESSION['xls_report']['Date']['From'] ;
$reqToDate 		= $_SESSION['xls_report']['Date']['To'] ;

$req_Header = "";
for( $cnt = 0; $cnt < count($_SESSION['xls_report']['Search']); $cnt++ ){
	$req_Header .= $_SESSION['xls_report']['Search'][$cnt];
}

$req_Header_date = 'For the period :  From '.$reqFromDate.' To '.$reqToDate;

$format_bold =& $workbook->addFormat(array('fgcolor' => 'white', 'pattern' => 1, 'size' => 12 ,'align' => 'center'));
$format_bold->setBold();

//$worksheet->setMerge ( 0,0,0,9);
$worksheet->write(0, 0, $req_heading, $format_bold);
//$worksheet->setMerge ( 2,0,2,9);
$worksheet->write(1, 0, $req_Header_date, $format_bold);
//$worksheet->setMerge ( 3,0,3,9);
$worksheet->write(2, 0, $req_Header, $format_bold);
$header_cnt = 4;

/*$header_cnt=0;
$margin = 0.2;
$print_header = "&11 &B ".$req_heading."\n  &10 ".$req_Header_date."\n  &10 ".$req_Header."\n \n ";
$worksheet->setHeader ($print_header ,$margin);

$worksheet->setFooter ("Page &P of &P"  ,$margin);
$worksheet->repeatColumns ($header_cnt,9);*/

$format_bold =& $workbook->addFormat(array('fgcolor' => 'cyan', 'pattern' => 1, 'size' => 12 ,'align' => 'center'));
$format_bold->setBold();
for( $cnt = 0; $cnt < count($_SESSION['xls_report']['Col']); $cnt++ ){
	$worksheet->write($header_cnt, $cnt , $_SESSION['xls_report']['Col'][$cnt], $format_bold);
}
$header_cnt1 = $header_cnt + 1;
$format_bold =& $workbook->addFormat(array('fgcolor' => 'white', 'pattern' => 1, 'size' => 10));
for( $header_cnt = 0; $header_cnt < count($_SESSION['xls_report']['Data_row']); $header_cnt++ ){
	for( $cnt = 0; $cnt < count($_SESSION['xls_report']['Col']); $cnt++ ){
		$worksheet->write($header_cnt1, $cnt , $_SESSION['xls_report']['Data_row'][$header_cnt][$cnt], $format_bold);
	}
	$header_cnt1++;
}


$worksheet->hideGridLines();
$req_heading 	= $_SESSION['xls_report']['Heading'];
$workbook->send(str_replace(' ','_',trim($req_heading)).date("dmYhis").'.xls');
$workbook->close();


?>
