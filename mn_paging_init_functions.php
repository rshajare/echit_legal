<?
$RecordsPerPage  = $_SESSION["RecordsPerPage"];
$_SESSION["Currency_Sign"] 	= "$";  
$fixed_currency_sign 		= $_SESSION["Currency_Sign"];


function paging($PageString,$page,$iPageMin,$iPageMax,$pagecount,$totalreturned,$perpage,$recordstart){
	echo "<table cellpadding='0' cellspacing='0' border='0'><tr valign='top'><td class='BoldBlack'><table class='BoldBlack' cellpadding='0' cellspacing='0' border='0'>	<tr>";	
	paging_pages($PageString,$page,$iPageMin,$iPageMax,$pagecount,$totalreturned,$perpage,$recordstart);
	echo "</tr></table></td></tr><tr valign='top'><td class='BlackNormalFont' align='left' width='50%'>";
	paging_total_found($PageString,$page,$iPageMin,$iPageMax,$pagecount,$totalreturned,$perpage,$recordstart);	  
	echo "</td></tr></table>";
}
function paging_one_line($PageString,$page,$iPageMin,$iPageMax,$pagecount,$totalreturned,$perpage,$recordstart){
	echo "<table class='BoldBlack' width='100%' cellpadding='0' cellspacing='0' border='0'><tr>";	
	paging_pages($PageString,$page,$iPageMin,$iPageMax,$pagecount,$totalreturned,$perpage,$recordstart);
	echo  "<td class='BlackNormalFont' align='right' width='50%'>";
	paging_total_found($PageString,$page,$iPageMin,$iPageMax,$pagecount,$totalreturned,$perpage,$recordstart);	  
	echo "</td></tr></table>";
}

function paging_pages($PageString,$page,$iPageMin,$iPageMax,$pagecount,$totalreturned,$perpage,$recordstart){
	echo "<td width='50%'>";
	if(($page>=1)and ($totalreturned > $perpage)){
		if($page>1){
			echo"<a href=\"$PHP_SELF?page=1&$PageString\">&laquo;First&raquo;</a> "; 
			$pageprev = $page-1;
			echo"<a href=\"$PHP_SELF?page=$pageprev&$PageString\">&laquo;Prev</a> "; 
		}
		for ($i=$iPageMin; $i <= $iPageMax;$i++){ 
			if($i != $page)	echo"<a href=\"$PHP_SELF?page=$i&$PageString\">$i</a> "; 
			else			echo " $i "; 
		} //end for 
		if(($page<$pagecount)){
			$pagenext = $page+1;
			echo"<a href=\"$PHP_SELF?page=$pagenext&$PageString\"> Next&raquo;</a> "; 
			echo"<a href=\"$PHP_SELF?page=$pagecount&$PageString\">&laquo;Last&raquo;</a> "; 
		}
	}
	echo "</td>";			
}

function paging_total_found($PageString,$page,$iPageMin,$iPageMax,$pagecount,$totalreturned,$perpage,$recordstart){
	if ($totalreturned == 0) 	$tmprecordstart = 0; 
	else 						$tmprecordstart = $recordstart+1;
	$tmpViewString = $tmprecordstart ;
	if ($page < $pagecount)			$tmpViewString = $tmpViewString . " - " . ($recordstart + $perpage);
	else if ($totalreturned<>0)	   	$tmpViewString = $recordstart + 1 . " - ". ($totalreturned);
	else							$tmpViewString = 0 . " - ". ($totalreturned);
	echo "Total Found : $totalreturned &nbsp;&nbsp;  Displaying : $tmpViewString";

}

//fuction to used for paging (getting max and min value)
function pageminmax($page,$pagecount){
 $result = array();
 $iPageOffSet = 5;
		if ($page < 5 )	
			$iPageOffSet = 5 + (5-$page);

		if (($page + $iPageOffSet) > $pagecount) 
			$iPageMax = $page + ($pagecount - $page);
		else	
			$iPageMax = $page + $iPageOffSet;

		if (($page - $iPageOffSet) < 1) 
				$iPageMin = 1;
		else
		{
			if (($page + $iPageOffSet) > $pagecount) 
				$iPageMin = (($pagecount - ($iPageOffSet + $iPageOffSet )) + 1);
			else
				$iPageMin = ($page - $iPageOffSet) + 1;
		}
		
		if ($iPageMin < 1)  
			$iPageMin = 1;
		if ($iPageMax > $pagecount) 
			$iPageMax = $pagecount;  
			
 $result[0] = $iPageMin;
 $result[1] = $iPageMax;
 return $result;
}
?>