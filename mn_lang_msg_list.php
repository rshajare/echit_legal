<? if ( !defined( 'SMARTY_DIR' ) ) { include_once( 'init.php' );}
$t->assign ('unid',md5(uniqid(rand(), true)));
header('Content-Type: text/html; charset=utf-8');

$newtxtmainkey = $_REQUEST['txtmainkey'];
$txtnew_msg    = $_REQUEST['new_msg'];
$reqmsgimport  = $_GET['msgimport'];
$result_str = "";

//===================== Select appropriate language =======================
$lang_pri = 'english';

if (isset($_POST["txt_lang_sec"])) 	$lang_sec = $_POST["txt_lang_sec"];	elseif (isset($_GET["lang_sec"])) $lang_sec = $_GET["lang_sec"];	
else{ if ($_SESSION["tmp_opt_lang_sec"] <> "") $lang_sec= $_SESSION["tmp_opt_lang_sec"]; else $lang_sec = FnGetValue("tbl_lang","language","language<>'$lang_pri'"); }

$_SESSION["tmp_opt_lang_sec"] = $lang_sec;
$tmp_opt_lang_sec_key = FnGetValue("tbl_lang","google_key","language = '$lang_sec'"); 
$t-> assign('tmp_opt_lang_sec_key',$tmp_opt_lang_sec_key);

$t-> assign('lang_sec',$lang_sec);
$t-> assign('lang_pri',$lang_pri);

// list of other languages except lang_pri i.e english code decalared in init 
//$t->assign ( 'lang_sec_list', $lang_sec_list );
//===================end Select appropriate language=================================

// Code to delelte all blank entry
if  ( isset( $_GET['dell_blank']) ){
	$sqldel =  "DELETE  FROM `tbl_lang_msg`  WHERE `descr` =  '' AND `lang` = 'english'";
	$result = mysql_query($sqlins) or die(mysql_error().$sqlins);
}

//$sqldel =  "DELETE  FROM `tbl_lang_msg` ";
//$result = mysql_query($sqldel) or die(mysql_error().$sqldel);

// =======================  Add New Data  =======================
if  ( isset( $_POST['txtmainkey']) ){//add new main/sub category

		$newtxtmainkey = trim($_POST[ 'txtmainkey' ]);
		$newdescr      = trim($_POST[ 'txtdescr' ]);
		$lang = trim($_POST['txt_lang_pri_add']);

		//adding english message
		$chkgrekmsg = FnGetCount("tbl_lang_msg", "mainkey = '$newtxtmainkey' and lang = 'english'");
		if ($chkgrekmsg > 0){
			$olddescr = fnGetValue("tbl_lang_msg","descr","mainkey = '$newtxtmainkey' and lang = 'english'");
			if ($olddescr ==''){ 
				//$db->query( "delete from tbl_lang_msg where mainkey = '$newtxtmainkey' and lang = 'english'" );
				$chkgrekmsg = 0;
			}
		}
		
		if ( ($chkgrekmsg == 0) && ($newtxtmainkey <>'') && ($newdescr <> '') ) {
			$sqlins = "Insert into tbl_lang_msg values(NULL,'english','$newtxtmainkey','','$newdescr')";
			$result = mysql_query($sqlins) or die(mysql_error().$sqlins);
			
			$result_str = "<BR><BR> Result : " .  $result . "- Query". $sqlins .  "";
			$t-> assign('result_str',$result_str);
			
		}else
		{
			$result_str = "<BR><BR>Cannot be added Key : " .  $newtxtmainkey .  "already exist or blank entry";
			$t-> assign('result_str',$result_str);
		}
		
		$t-> assign('op',1);
		$t->display('mn_lang_msg_list.htm');
		exit;
}
//==========================end of ading main and sub category=======================



//----------------------------------------------------------------------------------------
//===================End of adding new multilanguage message =============================
//----------------------------------------------------------------------------------------

$op = $_GET['op'];
if($op<>''){
	if ($op==1){//showing new message adding form
		$reqkeyword1  = $_REQUEST['txtsearch'] ;
		$t-> assign('reqkeyword1',$reqkeyword1);

		$reqfind = $_REQUEST['find'] ;
		$t-> assign('reqfind',$reqfind);

		$reqlangfind = $_REQUEST['langfind'] ;
		$t-> assign('reqfind',$reqfind);
		
		$reqlastup = $_REQUEST['lastup'] ;
		$t-> assign('reqlastup',$reqlastup);
		
		$t-> assign('op',$op);
		
		$t->display('mn_lang_msg_list.htm');
		exit;
	}elseif($op=='3'){//deleting multilanguage message
	
		if ( $_SESSION['db_admin']==1 ){	
			$reqlang_msg_id = $_GET['lang_msg_id'];
			$reqMainKey = FnGetValue("tbl_lang_msg","mainkey","id = $reqlang_msg_id");
			$chkgrekmsg = FnGetCount1("tbl_lang_msg","id","mainkey = '$reqMainKey' and lang = '$lang_sec'");
		
			if($chkgrekmsg > 1){
				$delete_greek_sql = "Delete from tbl_lang_msg where mainkey = '$reqMainKey' and lang = '$lang_sec' and descr = ''";
				$db->query( $delete_greek_res);
			}else{
				$delete_greek_sql = "Delete from tbl_lang_msg where mainkey = '$reqMainKey' and lang = '$lang_sec'";
				$db->query( $delete_greek_sql);
			}
			
			$delete_sql = "Delete from tbl_lang_msg where id = $reqlang_msg_id"; 
			$db->query( $delete_sql);
		}// end of pass check for delete	
	}// end of op==3
	//}
}//end of shwoing new message form and end of deleting multilanguage message



//=================== Edit Existing data & Translating the message for both language =========================	
if (isset($_POST["MAIN_CAT_ID_ARR"])){
	
  $MAIN_CAT_ID_ARR = $_POST["MAIN_CAT_ID_ARR"]; 
  $FIELD_1_PRI_ARR = $_POST["FIELD_1_PRI_ARR"];
  $FIELD_1_SEC_ARR = $_POST["FIELD_1_SEC_ARR"];

  
  $TMP_CNT=count($FIELD_1_PRI_ARR);
  for ($i=0;$i<$TMP_CNT;$i++){
  
		$MAIN_CAT_ID_VAL = $MAIN_CAT_ID_ARR[$i];
		$FIELD_1_PRI_VAL = trim($FIELD_1_PRI_ARR[$i]);
		$FIELD_1_SEC_VAL   = trim($FIELD_1_SEC_ARR[$i]);		
		
		if ($FIELD_1_PRI_VAL <>''){		
			$upmsg = "Update tbl_lang_msg SET descr = '$FIELD_1_PRI_VAL' where mainkey = '$MAIN_CAT_ID_VAL' and lang = '$lang_pri'";
			$db->query( $upmsg);
			
		}	
		//echo $upmsg;
		
		
		//Add/update other lang data
		if ( ($FIELD_1_SEC_VAL <>'') && ($MAIN_CAT_ID_VAL <> '') ){		
			$chkgrekmsg = FnGetCount("tbl_lang_msg", "mainkey = '$MAIN_CAT_ID_VAL' and lang = '$lang_sec'");
			if ($chkgrekmsg > 0) 
				$upmsg_gr  = "Update tbl_lang_msg SET descr = '$FIELD_1_SEC_VAL' where mainkey = '$MAIN_CAT_ID_VAL' and lang = '$lang_sec'";
			else
				$upmsg_gr = "Insert into tbl_lang_msg values(NULL, '$lang_sec','$MAIN_CAT_ID_VAL','','$FIELD_1_SEC_VAL')";

			$db->query( $upmsg_gr);
		}
		//echo $upmsg_gr;
		
  }// end of for loop
			
  $t->assign ( 'result', 1);

} // end of submit
//================End for updating the message for both language =========================



//----------------------------------------------------------------------------------------
//================showing lang key message ===============================================
//----------------------------------------------------------------------------------------
$sql_lang_msg_list="";
$reqkeyword  = $_REQUEST['txtsearch'] ;
if($reqkeyword<>''){
	$sql_lang_msg_list = "select * from tbl_lang_msg where lang = '$lang_pri' and descr like '%$reqkeyword%'";	
	$t-> assign('reqkeyword',$reqkeyword);
}

$reqkeyword1  = $_REQUEST['txtsearch1'] ;
if($reqkeyword1<>''){
	$sql_lang_msg_list = "select * from tbl_lang_msg where lang = '$lang_pri' and mainkey like '%$reqkeyword1%'";	
	$t-> assign('reqkeyword1',$reqkeyword1);
}

$reqfind = $_REQUEST['find'] ;
if($reqfind<>''){
	$sql_lang_msg_list = "select * from tbl_lang_msg where lang = '$lang_pri' and descr like '$reqfind%'";	
	$t-> assign('reqfind',$reqfind);
}	

$reqlangfind = $_REQUEST['langfind'] ;
if($reqlangfind<>''){
	$sql_lang_msg_list = "select * from tbl_lang_msg where lang = '$lang_pri' and mainkey like '$reqlangfind%'";	
	$t-> assign('reqlangfind',$reqlangfind);
}	

$reqlastup = $_REQUEST['lastup'] ;
if($reqlastup<>''){
	$sql_lang_msg_list = "select * from tbl_lang_msg where lang = '$lang_pri' order by `id` desc limit 0,100";	
	$t-> assign('reqlastup',$reqlastup);
}	

if($sql_lang_msg_list == ''){
	$sql_lang_msg_list = "select * from tbl_lang_msg where lang = '$lang_pri' ";
	$t-> assign('reqfind',A);
}	

//echo $sql;
//echo $sql_lang_msg_list;
$main_key = array();
$main_key_list = $db->getAll( $sql_lang_msg_list );
foreach($main_key_list as $index=>$row){

	$mainkey = $row['mainkey'];
	$row['descr_pri'] = FnGetValue("tbl_lang_msg","descr","mainkey = '$mainkey' and lang = '$lang_pri'"); 
	$row['descr_sec'] = FnGetValue("tbl_lang_msg","descr","mainkey = '$mainkey' and lang = '$lang_sec'"); 	

	$main_key[] = $row;
}
$t-> assign('main_key_list',$main_key);


$t->display('mn_lang_msg_list.htm');
?>