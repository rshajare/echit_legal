<?php
if ( !defined( 'SMARTY_DIR' ) ){include_once( 'init.php' );}
include( 'sur_adm_permission.php' );
header('Content-Type: text/html; charset=utf-8');
$tmpsubmit = $_GET['submit'];
$tmplaguage =  $_SESSION['opt_lang'];//session language


//=================Refresh Value For Add Product Page================================
if($tmpsubmit==1){
	$main_category_id = $_POST['txtmain_category_id'];
	$temp = $db->getAll( "SELECT * FROM tbl_category where main_id = $main_category_id order by `cat_id` desc");
	$t->assign ( 'tmp_subcat', $temp );
	$t->assign ( 'main_category_id', $main_category_id );
}

$main_id = $_POST['txtmain_category_id'];
$sub_id = $_POST['txtsub_category_id'];

$s_data = array();
$t->assign ( 'select_temp', $sub_id );

$cout = count($sub_id);

$sql = "SELECT concat(first_name,' ',last_name) as full_name,tbl_sur_sign_up.* FROM tbl_sur_sign_up";

if (isset($_GET['sort'])) $reqsort = $_GET['sort'];	else $reqsort='personnel_id';
if (isset($_GET['ord'])) $reqord = $_GET['ord'];  else  $reqord = "desc";
if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";	 
//---------------------------------Paging--------------------------------------------------
if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
$t->assign('reqord',$reqord);
//-------------------------------end  Paging------------------------------------------------------

$tmpblnWhere=0;
if ( isset( $_REQUEST['txtkeyword'] ) ){
	$reqkeyword=$_REQUEST['txtkeyword'] ;
	$t->assign ( 'reqkeyword', $reqkeyword);
	if (strcmp($reqkeyword,"")<>0){				
		if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
		$sql = $sql . "tbl_sur_sign_up.email like '%$reqkeyword%'";
		$PageString = $PageString  .'&txtkeyword='.$reqkeyword;
	 }
}

if ( isset( $_REQUEST['txtkeyword1'] ) ){
	$reqkeyword1=$_REQUEST['txtkeyword1'] ;
	$t->assign ( 'reqkeyword1', $reqkeyword1);
	if (strcmp($reqkeyword1,"")<>0){				
		if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
		$sql = $sql . "tbl_sur_sign_up.privilege_type = '$reqkeyword1'";
		$PageString = $PageString  .'&txtkeyword1='.$reqkeyword1;
	 }
}

if ( isset( $_REQUEST['txtkeyword51'] ) && isset( $_REQUEST['txtkeyword52'] ) ){
	$reqkeyword51=$_REQUEST['txtkeyword51'] ;
	$reqkeyword52=$_REQUEST['txtkeyword52'] ;
	$t->assign ( 'reqkeyword51', $reqkeyword51);
	$t->assign ( 'reqkeyword52', $reqkeyword52);
	if ((strcmp($reqkeyword51,"")<>0) &&(strcmp($reqkeyword52,"")<> 0)) {				
		if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
		$sql = $sql . " (tbl_sur_sign_up.join_date >= '". SqlDateIn($reqkeyword51). "' and tbl_sur_sign_up.join_date <= '". SqlDateIn($reqkeyword52) ."')";
		$PageString = $PageString  .'&txtkeyword51='.$reqkeyword51;
		$PageString = $PageString  .'&txtkeyword52='.$reqkeyword52;
	 }
}

$tmppri_type = FnGetValue("tbl_sur_privilege_types","privilege_type_id","privilege_type_name = 'admin'");
$tmppri_type1 = FnGetValue("tbl_sur_privilege_types","privilege_type_id","privilege_type_name = 'ip'");
if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
	$sql = $sql . " privilege_type <> $tmppri_type and privilege_type <> $tmppri_type1";

if ( isset( $_REQUEST['txtkeyword2'] ) ){
	$reqkeyword2=$_REQUEST['txtkeyword2'] ;
	$t->assign ( 'reqkeyword2', $reqkeyword2);
	if (strcmp($reqkeyword2,"")<>0){				
		$sql = $sql . " having full_name like '%$reqkeyword2%'";
		$PageString = $PageString  .'&txtkeyword2='.$reqkeyword2;
	 }
}

if (strcmp($reqsort,"")<>0)  $sql = $sql . ' order by ' . $reqsort ;
	$sql = $sql . ' ' . $reqord ;

//echo $sql;
// ---------------- call paging function---------------------------------------------------
   $strfnPagingSql = $sql; include 'includes/callpaging.php'; 
//-----------------------------------------------------------------------------------   
$data = array();
$tmpcnt = 0;
$temp = $db->getAll( $sql );
foreach ($temp as $index=> $row){
	$row['join_date'] = sqldateout($row['join_date'] );
	$row['tmpcnt'] = $tmpcnt;
	$tmpcnt = $tmpcnt + 1;
	$data[] = $row;
}
$t->assign ( 'data', $data );

$usrgrp = array();
$usrgrptemp = $db->getAll( "SELECT * FROM tbl_sur_privilege_types where lang = '$tmplaguage' and privilege_type_name <> 'admin' and privilege_type_name <> 'ip' order by `privilege_type_name` asc" );
foreach( $usrgrptemp as $index => $grprow ) {
		$usrgrp['privilege_type_id'][]   = $grprow['privilege_type_id'];
		$usrgrp['gr_privilege_type_name'][]   = $grprow['gr_privilege_type_name'];
		$usrgrp['privilege_type_id'][]   = $grprow['privilege_type_id'];
		$usrgrp['privilege_type_name'][] = $grprow['privilege_type_name'];
	}
$t->assign ( 'usrgrp', $usrgrp );

$t->assign ( 'tmpcnt_total', $tmpcnt );
$t->assign ( 'reqord', $reqord);
$t->display('msg_sel_users_list.htm');
?>