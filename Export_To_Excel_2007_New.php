<? 
set_time_limit(0);
ini_set('memory_limit','200M');
include( 'sur_adm_permission.php' );
require_once 'libs/PHPExcel/PHPExcel.php';

$req_Col_array = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();


// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");


$req_no_cols = count($_SESSION['xls_report']['Col']);
$req_heading 	= $_SESSION['xls_report']['Heading'];
$req_Header = "";

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->setCellValue('B1', $req_heading);
$objPHPExcel->getActiveSheet()->mergeCells('B1:'.$req_Col_array[$_SESSION['xls_report']['header_col_cnt']].'1');

$objPHPExcel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);

$row_cnt = 3;



for( $main_header_cnt = 0; $main_header_cnt < $_SESSION['xls_report']['header_row_cnt']; $main_header_cnt++ ){	
	for( $main_header_det_cnt = 0; $main_header_det_cnt < count($_SESSION['xls_report']['Header_row_0']); $main_header_det_cnt++ ){
		$col_first = $req_Col_array[1].$row_cnt;
		for( $cnt = 1; $cnt <= $_SESSION['xls_report']['header_col_cnt']; $cnt++ ){
			$col = $req_Col_array[$cnt].$row_cnt;
			$objPHPExcel->getActiveSheet()->setCellValue($col,$_SESSION['xls_report']['Header_row_0'][$main_header_det_cnt][$cnt]);	 
			$objPHPExcel->getActiveSheet()->getStyle($col,$_SESSION['xls_report']['Header_row_0'][$main_header_det_cnt][$cnt])->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle($col,$_SESSION['xls_report']['Header_row_0'][$main_header_det_cnt][$cnt])->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle($col,$_SESSION['xls_report']['Header_row_0'][$main_header_det_cnt][$cnt])->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle($col,$_SESSION['xls_report']['Header_row_0'][$main_header_det_cnt][$cnt])->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);						
			$col_last =$col;
		}		
		$row_cnt++;	
	}
     
	for( $main_header_det_cnt = 0; $main_header_det_cnt < count($_SESSION['xls_report']['Header_row']); $main_header_det_cnt++ ){
		for( $cnt = 1; $cnt <= $_SESSION['xls_report']['header_col_cnt']; $cnt++ ){
			$col = $req_Col_array[$cnt].$row_cnt;
			$objPHPExcel->getActiveSheet()->setCellValue($col,$_SESSION['xls_report']['Header_row'][$main_header_det_cnt][$cnt]);	 
			$objPHPExcel->getActiveSheet()->getStyle($col)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle($col)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle($col)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle($col)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);						
			$objPHPExcel->getActiveSheet()->getStyle($col)->getFont()->setBold(true);
		}
		$row_cnt++;	
	}
	for( $header_cnt = 0; $header_cnt < count($_SESSION['xls_report']['Data_row'][$main_header_cnt]); $header_cnt++ ){
		for( $cnt = 1; $cnt <= $_SESSION['xls_report']['header_col_cnt']; $cnt++ ){
			$col = $req_Col_array[$cnt].$row_cnt;
			$objPHPExcel->getActiveSheet()->setCellValue($col,$_SESSION['xls_report']['Data_row'][$main_header_cnt][$header_cnt][$cnt]['val']);	 			
			if ($_SESSION['xls_report']['Data_row'][$main_header_cnt][$header_cnt][$cnt]['align'] !=""){
				if ($_SESSION['xls_report']['Data_row'][$main_header_cnt][$header_cnt][$cnt]['align'] =='right'){
					$objPHPExcel->getActiveSheet()->getStyle($col)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				}else				if ($_SESSION['xls_report']['Data_row'][$main_header_cnt][$header_cnt][$cnt]['align'] =='left'){
					$objPHPExcel->getActiveSheet()->getStyle($col)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
				}else				if ($_SESSION['xls_report']['Data_row'][$main_header_cnt][$header_cnt][$cnt]['align'] =='center'){
					$objPHPExcel->getActiveSheet()->getStyle($col)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				}
			}
			$objPHPExcel->getActiveSheet()->getStyle($col)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle($col)->getBorders()->getBottom()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle($col)->getBorders()->getLeft()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$objPHPExcel->getActiveSheet()->getStyle($col)->getBorders()->getRight()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);						

		}
		$row_cnt++;
	}
}
$objPHPExcel->getActiveSheet()->mergeCells('B3:I3');		
$objPHPExcel->getActiveSheet()->mergeCells('B4:I4');		
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(8);			
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(35);			
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);			
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);			
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(35);			

$objPHPExcel->getActiveSheet()->setTitle('Sheet1');

if(PDF != 1){ 
	// Redirect output to a client�s web browser (Excel2007)
	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.str_replace(' ','_',trim($req_heading)).date("dmYhis").'.xlsx"');
	header('Cache-Control: max-age=0');
	
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save('php://output');
	exit;
}
?>