<? 
// +----------------------------------------------------------------------------------------------------------------------------+
// |  File Name : install.php
// |  Version : 2.0 
// |  Create-date : 20, March, 2010
// |  Modify-date : 29, April, 2010
// |  Authors : Niteen Borate                        
// | 
// |  Desciption : install script on server
// |  How to use : to use it just use appropriate string in required template file
// |
// |  [ "sql_back_up/install_default_db.bat" ]:  	default data file windows [sql_back_up] is folder
// |  [ "sql_back_up/install_default_db.doc.gz" ]:   default data file linux [sql_back_up] is folder
// |
// |  
// +------------------------------------------------------------------------------------------------------------------------------
//

set_time_limit(0);
header('Content-Type: text/html; charset=utf-8');
?>
 <table width="90%" border="0" align="center" cellpadding="2" cellspacing="2">
 
    <tr>
      <td>
		  <? if (isset($_POST["Submit"])) { 
		  
		  $dbhost = $_POST["dbhost"];
		  $dbuname =  $_POST["dbuname"];
		  $dbname =  $_POST["dbname"];
		  $dbpass = $_POST["dbpass"];
  		  $webpath = $_POST["webpath"];
		  $dbrest_flag = $_POST["dbrest_flag"];
		  $server_type = $_POST["server_type"];
		  
		  $string = '<?php 
					define( "DB_USER", "'. $dbuname. '");
					define( "DB_NAME", "'. $dbname. '" );
					define( "DB_HOST", "'. $dbhost. '" );
					define( "DB_PASS", "'. $dbpass. '" );
					define( "WEBSITE_PATH", "'. $webpath. '" );
					define( "DB_TYPE", "mysql" );
					?>'; 
		  $string = trim($string);			
				$fp = fopen("config_server.php", "w");
				fwrite($fp, $string);
				fclose($fp); 

			if ($dbrest_flag=='1'){
				// Install/Run Data Base File 
				$savepath = dirname(__FILE__) . '/' ; // Full path to this directory. Do not use trailing slash!	
				
				if ($server_type == 'win' ){ 
					$restore_filename = "install_default_db.doc";
					$your_data = "set PATH=%PATH%;C:\\PROGRA~1\\mysql\mysql server 5.0\\bin\\\r\n";
					$your_data .= "mysql -u ".$dbuname." -p".$dbpass." ".$dbname." < ".$savepath."sql_back_up/".$restore_filename;
					$myFile = "install_default_db.bat";
					$handle = fopen($myFile, 'w');
					fwrite($handle,$your_data);
					fclose($handle);
					exec($savepath . "install_default_db.bat");
				}else{
					$restore_filename = "install_default_db.doc.gz";
					if ($dbpass<>"") $pstring =  " -p".$dbpass; else $pstring =  " ";
					$your_data .= " gunzip < ".$savepath."sql_back_up/".$restore_filename ." | mysql -u ".$dbuname." ". $pstring . " -D ". $dbname;
					//$your_data .= " gunzip < ".FULL_PATH."sql_back_up/".$restore_filename ." | mysql -u ".$dbuser." ". $pstring . " -D ". $dbname;
					
					$return1 = exec($your_data);
					//echo $your_data."===".$return1;
					}
				}

		  ?>
		  <table width="100%" border="0" cellpadding="6" cellspacing="1" bgcolor="#999999">
			<tr>
			  <td height="81" bgcolor="#FFFFFF"><div align="center">
				<h2>Script installed Successfully !!</h2>
			  </div></td>
			</tr>
			<tr>
			  <td bgcolor="#FFFFFF"><div align="center"><a href="<?=$webpath?>">
			  plz click here to contine................</a></div></td>
			</tr>
		  </table>
		 <?  
		 exit();
		 } ?>
      </td>
    </tr>
	
    <tr>
      <td>
	
	  
 <form action="" method="post" name="install" id="install"> 
  <table border="0" align="center" cellpadding="6" cellspacing="1" bgcolor="#666666">
    <tr bgcolor="Efefef">
      <td ><div align="center"><h3><strong>Script Configuration</strong></h3></div></td>
    </tr>
    <tr bgcolor="#FFFFFF">
      <td width="183"><div align="right">DB Host </div></td>
      <td width="660"><input name="dbhost" type="text" id="dbhost" value="localhost">(e.g : localhost) </td>
    </tr>
    <tr bgcolor="#FFFFFF">
      <td><div align="right">DB Name </div></td>
      <td><input name="dbname" type="text" id="dbname">(e.g : projects_shop)</td>
    </tr>
    <tr bgcolor="#FFFFFF">
      <td><div align="right">DB Username</div></td>
      <td><input name="dbuname" type="text" id="dbuname">(e.g: niteen241)</td>
    </tr>
    <tr bgcolor="#FFFFFF">
      <td><div align="right">DB Pass </div></td>
      <td><input name="dbpass" type="password" id="dbpass">(e.g : XXXX)</td>
    </tr>
    <tr bgcolor="#FFFFFF">
      <td><div align="right">Website Path </div></td>
      <td><input name="webpath" type="text" id="webpath" size="60">
        <br>
        (e.g : http://www.projects.netforce.gr/new_shop_prj/)</td>
    </tr>
    <tr bgcolor="#FFFFFF">
      <td><div align="right">Server Type</div></td>
      <td><input type="radio" name="server_type" value="win">
        Windows 
          <input name="server_type" type="radio" value="lin" checked>
          Linux</td>
    </tr>
    <tr bgcolor="#FFFFFF">
      <td><div align="right"></div></td>
      <td><input type="checkbox" name="dbrest_flag" value="1">        
         Install with Default DB. <br>
         This will install script with default database &quot;default_install_db_bck.doc&quot;
         which is stored in &quot;sql_back_up&quot; folder of script. <br>         
         <br>         <br>
      </td>
    </tr>
    <tr bgcolor="#FFFFFF">
      <td ><div align="center">
        <input type="submit" name="Submit" value="Install Script">
      </div></td>
    </tr>
  </table>
  </form>
  
  
	  </td>
    </tr>
</table>