<? if ( !defined( 'SMARTY_DIR' ) ) { include_once( 'init.php' );}

// +----------------------------------------------------------------------------------------------------------------------------+
// |  File Name : nm_menu_lhf_banner_functions.php
// |  Version : 2.0 
// |  Create-date : 29, March, 2010 
// |  Modify-date : 27, Dec, 2013
// |  Authors : Niteen Borate                        
// | 
// |  Desciption : functions to create .js file for drowndown menu.
// |  e.g [mn_file_sadmin.js] for [sadmin] previlage type.
// |  All functions used in this page are independent they are used only for this page.
// +------------------------------------------------------------------------------------------------------------------------------
//


// We used seprate function just to genrate menu, to avoide conflicts with existing system.
function fnPageAccess_ToGenMenu($reqCurrPage,$tmpPrivilege_Type_ID){
    $tmpPrivilege_Type_Name = fnGetValue("tbl_sur_privilege_types","privilege_type_name","privilege_type_id = '$tmpPrivilege_Type_ID'");
    if ($tmpPrivilege_Type_Name =='sadmin'){
        return "YES";
    }
    $sql = "select Privileges1,Filter,Tables from tbl_sur_p_privileges where page ='$reqCurrPage' and Privilege_Type_ID = '$tmpPrivilege_Type_ID'";
    $result = mysql_query($sql) or die(mysql_error())	;
    if($result == ""){
        ?><script language="javascript"> window.location.href="Session_expires.php?pageurl='<?=$PHP_SELF?>'"; </script><?
    }else{
        $reqPrivilage="NO";
        while ($row = mysql_fetch_array($result)){
             $reqPrivilage = $row["Privileges1"];
             if ($reqPrivilage == "FILTERED") {
                $reqFilter  = $row["Filter"];
                $reqTables  = $row["Tables"];
             }
        }

        if ($reqPrivilage == "NO"){ return "NO";}
        if ($reqPrivilage == "YES"){ return "YES";}
        if ($reqPrivilage == "FILTERED"){
            //
        }
        return "YES";
    }
} // end of function

// Function to get multilevel CMS menu
function fnShowMenu($hirno,$cntsub,$main_cat_id,$tmplaguage,$privilege_type_id){
    $returnstr = '';
    $tmprecnt1 = 0;
	if($cntsub > 0){
		$catsql = "select * from tbl_mn_menu_page where main_id = " . $main_cat_id . " and lang='$tmplaguage' and hierno = " . $hirno . " order by order_no ";
		$temp_menu = mysql_query($catsql) or die($catsql .mysql_error());
		$tmprecnt = count($temp_menu);
		While($row = mysql_fetch_array($temp_menu)){
			$login_req = $row['login_req'];
            $tmpGO = 0;
            if (($login_req == 1)){
                $reqIsAccess = fnPageAccess_ToGenMenu($row['page_code'],$privilege_type_id);
                if ($reqIsAccess =='YES')
                    $tmpGO = 1;
            }elseif ($login_req == 0)
                $tmpGO = 1;
			
			if	($tmpGO ==1){

                $reqmain_id     = $row['main_cat_id'];
                $reqmain_name   = $row['name'];
                $tmpMain_ID     = $row['main_cat_id'];
                $tmpherno       = $row['hierno'];
                $tmpherno       = $tmpherno + 1;
                $cntCatSub      = FnGetCount("tbl_mn_menu_page","main_id = ".$tmpMain_ID);

                if(($tmprecnt >= 1) && ($tmprecnt1==0) && ($cntCatSub > 0)){
                    $returnstr .= '<li class="dropdown-submenu"><a href="#" data-toggle="dropdown" class="dropdown-toggle">'.$reqmain_name.'</a>';
                    $returnstr .= '<ul class="dropdown-menu">';
                    $returnstr .= fnShowMenu($tmpherno,$cntCatSub,$tmpMain_ID,$tmplaguage,$privilege_type_id);
                    $returnstr .= '</ul>';
                }else{
                    $returnstr .= '<li><a href="'.$row['category_tag'].'">'.$reqmain_name.'</a>';
                }
                $returnstr .= '</li>';
			}// end of login req			
	    }// end of while
	}
	return $returnstr;
}
// End of function


//user header
if (($_REQUEST['privilege_type_id']!="") && ($_REQUEST['privilege_type_id']!="0")){	
	$tmplaguage = $_SESSION['opt_lang'];
	$sel_privilege_type_id = $_REQUEST['privilege_type_id'];//$_SESSION['privilege_type_id'];
	$privilege_type_name = fnGetValue("tbl_sur_privilege_types","privilege_type_name","privilege_type_id=$sel_privilege_type_id");
	//Assign admin header
	$tmplaguage = $_SESSION['opt_lang'];
	$mn_menu_access_arr  = array();
	$str = '';
/*	$sql = "SELECT main_cat_id,name,login_req,category_tag,page_code,page_type FROM tbl_mn_menu_page where hierno = 0 and lang='$tmplaguage' and type = 'adm'" . " order by order_no";
	$result = mysql_query($sql);
	While($row = mysql_fetch_array($result)){ 
		$menu_id        = $row['main_cat_id'];
		$name			= $row['name'];
		$login_req 		= $row['login_req'];
		$reqCurrPage	= $row['category_tag'];
        $tmpGO = 0;
		if ( $login_req == 1) {
			$reqIsAccess = fnPageAccess_ToGenMenu($row['page_code'],$sel_privilege_type_id);
			$mn_menu_access_arr[$reqCurrPage] = $reqIsAccess ;
			if ($reqIsAccess =='YES')
			    $tmpGO = 1;
			
		}elseif ($login_req == 0)
            $tmpGO = 1;
		
		if	($tmpGO ==1){
            $cntCatSub = FnGetCount("tbl_mn_menu_page","main_id = ".$menu_id);
            if($cntCatSub > 0){
                $str .= '<li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">'.$name.'<b class="caret"></b></a>';
                $str .= '<ul class="dropdown-menu" role="menu">';
                $retrunstr = fnShowMenu(1,1,$menu_id,$tmplaguage,$sel_privilege_type_id);
                $str .= $retrunstr . "</ul></li>";
            }else{
                if ($row['page_type'] =='link')
                    $str .= '<li><a href="'.$row["category_tag"].'">'.$name.'</a></li>';
                else
                    $str .= '<li><a href="mn_page_detail.php?page_id='.$menu_id.'">'.$name.'</a></li>';
            }

		}// end if login req
	 } // end of while
	 


	$myFile = "templates/mn_file_".$privilege_type_name.".htm";
	$fh = fopen($myFile, 'w') or die("can't open file");
	fwrite($fh, $str);
	fclose($fh);
	echo "File ".$myFile." Created Successfully";
*/	
	$sql = "SELECT main_cat_id,name,login_req,category_tag,page_code,page_type FROM tbl_mn_menu_page where hierno = 0 and lang='$tmplaguage' and type = 'lgl'" . " order by order_no";
	$result = mysql_query($sql);
	While($row = mysql_fetch_array($result)){ 
		$menu_id        = $row['main_cat_id'];
		$name			= $row['name'];
		$login_req 		= $row['login_req'];
		$reqCurrPage	= $row['category_tag'];
        $tmpGO = 0;
		if ( $login_req == 1) {
			$reqIsAccess = fnPageAccess_ToGenMenu($row['page_code'],$sel_privilege_type_id);
			$mn_menu_access_arr[$reqCurrPage] = $reqIsAccess ;
			if ($reqIsAccess =='YES')
			    $tmpGO = 1;
			
		}elseif ($login_req == 0)
            $tmpGO = 1;
		
		if	($tmpGO ==1){
            $cntCatSub = FnGetCount("tbl_mn_menu_page","main_id = ".$menu_id);
            if($cntCatSub > 0){
                $str .= '<li class="dropdown"><a href="#" data-toggle="dropdown" class="dropdown-toggle">'.$name.'<b class="caret"></b></a>';
                $str .= '<ul class="dropdown-menu" role="menu">';
                $retrunstr = fnShowMenu(1,1,$menu_id,$tmplaguage,$sel_privilege_type_id);
                $str .= $retrunstr . "</ul></li>";
            }else{
                if ($row['page_type'] =='link')
                    $str .= '<li><a href="'.$row["category_tag"].'">'.$name.'</a></li>';
                else
                    $str .= '<li><a href="mn_page_detail.php?page_id='.$menu_id.'">'.$name.'</a></li>';
            }

		}// end if login req
	 } // end of while
	 


	$myFile = "templates/mn_file_lgl_".$privilege_type_name.".htm";
	$fh = fopen($myFile, 'w') or die("can't open file");
	fwrite($fh, $str);
	fclose($fh);
	echo "File ".$myFile." Created Successfully";
	
}
?>