var intH = 0;
var intW = 0;
var shadow;
var attr;
var layer_data ;
var frame_width,frame_height,frame_left,frame_top,frame_attr;
var contenttype,contentsource;
var req_top;
var center_align;
var req_left;
var req_width;
var req_height;
var req_maximum;

function check_control(contenttype,contentsource) {
	if (!document.getElementById("shadow")) {
        newDiv2 	= document.createElement("div");
        newDiv2.id 	= "shadow";
		if (contenttype=="Url_Dialog_trans"){
			newDiv2.className = 'shadowlayer_transparent'; 
		}else{	newDiv2.className = 'shadowlayer'; }
        document.body.appendChild(newDiv2);
    }
	shadow 	= document.getElementById("shadow");
	var bws = getBrowserHeight();
	shadow.style.width  = bws.width 	+ "px";
	shadow.style.height = bws.height 	+ "px";
	var html_window_data 	= '';
	
/*	if ((contenttype=="Two_Buttono_Dialog")||(contenttype=="One_Buttono_Dialog")){
		html_window_data	 	+= '';
	}else{
		html_window_data	 	+= '<div class="html_window_control" align="right"><img src="images/del.gif" class="html_window_control_del"lass="html_window_control" title="Close" onclick="javascript: closewindow();" /></div>';
	}
*/
	if ((contenttype=="Two_Buttono_Dialog")||(contenttype=="One_Buttono_Dialog")){
		html_window_data	 	+= '';
	}else if ((contenttype=="Url_Dialog")||(contenttype=="Url_Dialog_trans")){	
		html_window_data += '<div class="html_window_control1" align="right"><img src="images/del.gif" class="html_window_control_del" border=0 height="14" weight="14" title="Close" onclick="javascript: closewindow();" /> &nbsp;</div>';	
	}else{
		html_window_data += '<div class="html_window_control" align="right"><a HREF="javascript: closewindow();" style="text-decoration:none; color:#FFF; font-size:11px">Close</a></div>';
	}
	
	if (!document.getElementById("html_window")) {
		html_window 	= document.createElement("div");
		html_window.id 	= "html_window";
		html_window.className = 'html_window'; 
	
		html_window.innerHTML = html_window_data
		
		document.body.appendChild(html_window);
	}else{
		document.getElementById("html_window").innerHTML = html_window_data
	}
	switch(contenttype){
		case 'iframe':
			if (!document.getElementById("layer_frame")) {
				newDiv2 	= document.createElement("iframe");
				newDiv2.id 	= "layer_frame";
				newDiv2.className = 'html_window_data'; 
				newDiv2.src = contentsource;
				document.getElementById("html_window").appendChild(newDiv2)
			}
			break;
		case 'div' :
			if (!document.getElementById("layer_frame")) {
				newDiv2 	= document.createElement("div");
				newDiv2.id 	= "layer_frame";
				newDiv2.className = 'html_window_data'; 
				if(document.getElementById(contentsource)){
					newDiv2.innerHTML = document.getElementById(contentsource).innerHTML;
				}
				document.getElementById("html_window").appendChild(newDiv2);			
			}
			break;
		case 'Two_Buttono_Dialog':	
				newDiv2 	= document.createElement("div");
				newDiv2.id 	= "layer_frame";
				newDiv2.className = 'html_window_data'; 			
				newDiv2.innerHTML = contentsource;
				document.getElementById("html_window").appendChild(newDiv2);			
				break;
		case 'One_Buttono_Dialog':	
				newDiv2 	= document.createElement("div");
				newDiv2.id 	= "layer_frame";
				newDiv2.className = 'html_window_data'; 			
				newDiv2.innerHTML = contentsource;
				document.getElementById("html_window").appendChild(newDiv2);			
				break;
		case 'Url_Dialog_trans':	
				newDiv2 	= document.createElement("div");
				newDiv2.id 	= "layer_frame";
				newDiv2.className = 'html_url_window_data'; 			
				newDiv2.innerHTML = contentsource;
				document.getElementById("html_window").appendChild(newDiv2);			
				break;
		case 'Url_Dialog':
				newDiv2 	= document.createElement("div");
				newDiv2.id 	= "layer_frame";
				newDiv2.className = 'html_window_data'; 			
				newDiv2.innerHTML = contentsource;
				document.getElementById("html_window").appendChild(newDiv2);			
				break;
	}	
	
}
function getBrowserHeight() {
	if(document.body && (document.body.clientWidth || document.body.clientHeight)) {
		if (document.body.clientHeight < document.body.scrollHeight) {
    	    intH = document.body.scrollHeight;
    	}
		intW = document.body.scrollWidth;
	}
	return { width: parseInt(intW), height: parseInt(intH) };
} 

function getValue(Name){
	var config=new RegExp(Name+"=([^,]+)", "i");
	return (config.test(frame_attr))? parseInt(RegExp.$1) : 0;
}
function find_positions(){
	center_align = parseInt(getValue("center"));
	req_left 	 = parseInt(getValue("left"));
	req_top  	 = parseInt(getValue("top"));
	req_width 	 = parseInt(getValue("width"));
	req_height   = parseInt(getValue("height"));
	req_maximum  = parseInt(getValue("maximum"));

	if (window.innerHeight){
		cHeight 	= window.innerHeight;
		cWidth 		= window.innerWidth;
		frame_left 	= window.pageXOffset;
		frame_top 	= window.pageYOffset;
	}else if (document.documentElement && document.documentElement.clientHeight){
		cHeight 	= document.documentElement.clientHeight;
		cWidth 		= document.documentElement.clientWidth;
		frame_left 	= document.documentElement.scrollLeft;
		frame_top 	= document.documentElement.scrollTop;
	}else if (document.body){
		cHeight			= document.body.clientHeight;
		cWidth 			= document.body.clientWidth;
		frame_left 		= document.body.scrollLeft;
		frame_top 		= document.body.scrollTop;
	}
	if(req_maximum == 1){
		frame_width  = cWidth - 40;
		frame_height = cHeight - 40;
	}else{
		frame_width  = req_width  ? req_width  : 350;
		frame_height = req_height ? req_height : 230;
	}
	if((center_align)||((req_left == 0)&&(req_top == 0))){
		frame_left = parseInt((cWidth  - frame_width) / 2);
		frame_top  = parseInt(frame_top + (cHeight - frame_height)/ 2);
	}else{
		frame_left = req_left;
		frame_top  = parseInt(frame_top + req_top);
	}
	if(frame_top < 0) frame_top = 0;
	if(frame_left < 0) frame_left = 0;
}
function setLayerPosition(contenttype,contentsource) {
	check_control(contenttype,contentsource);
	find_positions();
	layer_data	= document.getElementById("html_window");
	layer_data.style.left 	= frame_left;
	layer_data.style.top 	= frame_top;
	layer_data.style.width 	= frame_width;
	layer_data.style.height	= frame_height;
}
function showLayer(contenttype,contentsource,attr) {
	frame_attr = attr;
	setLayerPosition(contenttype,contentsource);
	shadow.style.display 	 = "block"; 
	layer_data.style.display = "block";
}

function Show_Two_Button_Dialog(But1Caption,Action_On_But1,But2Caption,Action_On_But2,MsgToDisplay,attr) {
	if (attr!="")	frame_attr = attr; else frame_attr="height=150,width=550";	
	contentsource="<link REL='stylesheet' HREF='styles.css'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	contentsource=contentsource+"<tr><td>&nbsp;</td></tr><tr> <td align='center' class'NormalBR'><strong>"+MsgToDisplay+"</strong></td> </tr>";
	contentsource=contentsource+"<tr><td align='center'>&nbsp;</td></tr>";
	contentsource=contentsource+"<tr><td align='center'><input name='But1' type='button' id='But1' value='"+But1Caption+"' onclick='"+Action_On_But1+"' />&nbsp;";
	contentsource=contentsource+"<input name='But2' type='button' id='But2' value='"+But2Caption+"' onclick='"+Action_On_But2+"' /></td>";
	contentsource=contentsource+"</tr></table>";
	setLayerPosition('Two_Buttono_Dialog',contentsource);
	shadow.style.display 	 = "block"; 
	layer_data.style.display = "block";
}
function Show_One_Button_Dialog(But1Caption,Action_On_But1,MsgToDisplay,attr) {
	if (attr!="")	frame_attr = attr; else frame_attr="height=150,width=550";	
	contentsource="<link REL='stylesheet' HREF='styles.css'><table width='100%' border='0' cellspacing='0' cellpadding='0'>";
	contentsource=contentsource+"<tr><td>&nbsp;</td></tr><tr> <td align='center' class'NormalBR'><strong>"+MsgToDisplay+"</strong></td> </tr>";
	contentsource=contentsource+"<tr><td align='center'>&nbsp;</td></tr>";
	contentsource=contentsource+"<tr><td align='center'><input name='But1' type='button' id='But1' value='"+But1Caption+"' onclick='"+Action_On_But1+"' />&nbsp;</td>";
	contentsource=contentsource+"</tr></table>";
	setLayerPosition('One_Buttono_Dialog',contentsource);
	shadow.style.display 	 = "block"; 
	layer_data.style.display = "block";
}
function Show_Dialog_with_url(Url,attr) {
	if (attr!="")	frame_attr = attr; else frame_attr="height=500,width=850";	
	contentsource="<iframe SRC="+Url+" NAME='right' ID='linkform' WIDTH='100%' FRAMEBORDER='0' HEIGHT='100%' vspace='0' hspace='0' align='center' SCROLLING='auto'></iframe>";
	setLayerPosition('Url_Dialog',contentsource);
	shadow.style.display 	 = "none"; 
	layer_data.style.display = "block";

}
function Show_Dialog_with_url_trans(Url,attr) {
	if (attr!="")	frame_attr = attr; else frame_attr="height=500,width=850";	
	contentsource="<iframe SRC="+Url+" NAME='right' ID='linkform' WIDTH='100%' FRAMEBORDER='0' HEIGHT='100%' SCROLLING='auto'></iframe>";
	setLayerPosition('Url_Dialog_trans',contentsource);
	shadow.style.display 	 = "block"; 
	layer_data.style.display = "block";
}
function closewindow() {
	shadow.style.display 	 = "none"; 
	layer_data.style.display = "none";
}
function addEvent(elm, evType, fn, useCapture){
	if (elm.addEventListener){
		elm.addEventListener(evType, fn, useCapture);
		return true;
	}else if (elm.attachEvent){
		var r = elm.attachEvent("on"+evType, fn);
		return r;
	}else{
		alert("Handler could not be removed");
	}
}
function onchange_call(){
	if (window.innerHeight){
		cHeight 	= window.innerHeight;
		cWidth 		= window.innerWidth;
		frame_left 	= window.pageXOffset;
		frame_top 	= window.pageYOffset;
	}else if (document.documentElement && document.documentElement.clientHeight){
		cHeight 	= document.documentElement.clientHeight;
		cWidth 		= document.documentElement.clientWidth;
		frame_left 	= document.documentElement.scrollLeft;
		frame_top 	= document.documentElement.scrollTop;
	}else if (document.body){
		cHeight			= document.body.clientHeight;
		cWidth 			= document.body.clientWidth;
		frame_left 		= document.body.scrollLeft;
		frame_top 		= document.body.scrollTop;
	}	
	if(req_maximum == 1){
		frame_width  = cWidth - 40;
		frame_height = cHeight - 40;
	}else{
		frame_width  = req_width  ? req_width  : 350;
		frame_height = req_height ? req_height : 230;
	}
	if((center_align)||((req_left == 0)&&(req_top == 0))){
		frame_left = parseInt((cWidth  - frame_width) / 2);
		frame_top  = parseInt(frame_top + (cHeight - frame_height)/ 2);
	}else{
		frame_left = req_left;
		frame_top  = parseInt(frame_top + req_top);
	}
	layer_data	= document.getElementById("html_window");
	if (frame_left){	layer_data.style.left 	= frame_left;}
	if (frame_top){	layer_data.style.top 	= frame_top;}
}

addEvent(window, "resize", onchange_call);
addEvent(window, "scroll", onchange_call);