<?php

/**
Generic PHP functions. Please only put general-use PHP functions
in this file. Feature-specific functions should be kept local to
the particular feature.
*/


/**
 * Splits set of sql queries into an array
 */
function splitSql($sql)
{
    $sql = preg_replace("/\r/s", "\n", $sql);
    $sql = preg_replace("/[\n]{2,}/s", "\n", $sql);
    $lines = explode("\n", $sql);
    $queries = array();
    $inQuery = 0;
    $i = 0;

    foreach ($lines as $line) {
        $line = trim($line);

        if (!$inQuery) {
            if (preg_match("/^CREATE/i", $line)) {
                $inQuery = 1;
                $queries[$i] = $line;
            }
            elseif (!empty($line) && $line[0] != "#") {
                $queries[$i] = preg_replace("/;$/i", "", $line);
                $i++;
            }
        }
        elseif ($inQuery) {
            if (preg_match("/^[\)]/", $line)) {
                $inQuery = 0;
                $queries[$i] .= preg_replace("/;$/i", "", $line);
                $i++;
            }
            elseif (!empty($line) && $line[0] != "#") {
                $queries[$i] .= $line;
            }
        }
    }

    return $queries;
}


function parseInsertSql( $sql, $table, &$fields, &$values ) {

      $sql = trim( $sql );

      if ( empty( $sql ) || $sql[0] == '#' )
          return false;

      $sql = str_replace( '[prefix]_', DB_PREFIX.'_', $sql );

      $pattern = '/insert\s+into\s+`?'.$table.'`?\s+\((.*?)\)\s+values\s+\((.*?)\)\s*$/i';

      if ( !preg_match( $pattern, $sql, $matches ) )
          return false;

      $tempFields = split( ',', trim( $matches[1], ' ' ) );

      foreach ( $tempFields as $field ) {
          $fields[] = trim( $field, ' `' );
      }

      $tempValues = split( ',', trim( $matches[2], ' ' ) );
      $value = '';

      for ( $i=0, $n=count( $tempValues ); $i<$n; $i++ ) {

          $value .= $tempValues[$i];

          if ( (substr_count( $value, '\'' ) - substr_count( $value, '\\\'' )) % 2 == 0 ) {
              $values[] = trim( $value, " '" );
              $value = '';
          } else {
              $value .= ',';
          }
      }

      return true;


}

/**
 * retrieves the user's browser type
 */
/*function getUserBrowser()
{
    global $HTTP_USER_AGENT, $_SERVER;
    if (!empty($_SERVER['HTTP_USER_AGENT'])) {
        $HTTP_USER_AGENT = $_SERVER['HTTP_USER_AGENT'];
    }
    elseif (getenv("HTTP_USER_AGENT")) {
        $HTTP_USER_AGENT = getenv("HTTP_USER_AGENT");
    }
    elseif (empty($HTTP_USER_AGENT)) {
        $HTTP_USER_AGENT = "";
    }

    if (eregi("MSIE ([0-9].[0-9]{1,2})", $HTTP_USER_AGENT, $regs)) {
        $browser['agent'] = 'MSIE';
        $browser['version'] = $regs[1];
    }
    elseif (eregi("Mozilla/([0-9].[0-9]{1,2})", $HTTP_USER_AGENT, $regs)) {
        $browser['agent'] = 'MOZILLA';
        $browser['version'] = $regs[1];
    }
    elseif (eregi("Opera(/| )([0-9].[0-9]{1,2})", $HTTP_USER_AGENT, $regs)) {
        $browser['agent'] = 'OPERA';
        $browser['version'] = $regs[2];
    }
    else {
        $browser['agent'] = 'OTHER';
        $browser['version'] = 0;
    }

    return $browser['agent'];
}
*/
// possibly eliminate?
function getNewWindowHref( $href, $width, $height ) {
    $uw = $width + 10;
    $uh = $height + 20;
    return "javascript:launchCentered( '$href', $uw, $uh, 'resizable=no,scrollbars=no' );";
}

/**
 * Appends array $source to the end of array $dest
 */
function array_append( $dest, $source ) {
    $n = count( $dest );
    $n1 = count( $source );
    for ( $index=$n; $index<$n+$n1; $index++ ) {
        $dest[$index] = $source[$index-$n];
    }
    return $dest;
}

/**
 * Determines if GD library is installed
 */
function gdInstalled() {
    return function_exists( 'gd_info' );
}

function getSetting( $name, $default=NULL ) {
    global $db, $site, $moduleKey;

    $value = $db->getOne( 'select value from '.MODULESETTINGS_TABLE." where name='$name' and site_key='$site' and module_key='$moduleKey'" );

    return isset( $value ) ? $value : $default;
}

function cut( $value, $len ) {
    return ( strlen($value) > $len ) ? substr( $value, 0, $len - 1) .'...' : $value;
}


/**
* This will remove HTML tags, javascript sections
* and white space. It will also convert some
* common HTML entities to their text equivalent.
* $conent should contain an HTML document.
* From PHP Manual.
*/
function stripHTMLTags( $content ) {

    $search = array ("'<script[^>]*?>.*?</script>'si",  // Strip out javascript
                     "'<\s*br\s*(\/)?>'i",              // Replace brs to spaces
                     "'<[\/\!]*?[^<>]*?>'si",           // Strip out html tags
                     "'([\r\n])[\s]+'",                 // Strip out white space
                     "'&(quot|#34);'i",                 // Replace html entities
                     "'&(amp|#38);'i",
                     "'&(lt|#60);'i",
                     "'&(gt|#62);'i",
                     "'&(nbsp|#160);'i",
                     "'&(iexcl|#161);'i",
                     "'&(cent|#162);'i",
                     "'&(pound|#163);'i",
                     "'&(copy|#169);'i",
                     "'&#(\d+);'");

    $replace = array ("",
                      " ",
                      "",
                      "\\1",
                      "\"",
                      "&",
                      "<",
                      ">",
                      " ",
                      chr(161),
                      chr(162),
                      chr(163),
                      chr(169),
                      "chr(\\1)");

    $content = preg_replace ($search, $replace, $content);

    return $content;
}

if (!function_exists(CopyFiles)) {
	function CopyFiles($fromdir, $todir, $recursed = 1 ) {
		if ($fromdir == "" or !is_dir($fromdir)) {
			echo ('Invalid directory');
			return false;
		}
		if ($todir == "") {
			echo('Output Directory name is missing');
			return false;
		}

		if (!file_exists($todir)) {
			mkdir($todir);
		}

		$filelist = array();
		$dir = opendir($fromdir);

		while($file = readdir($dir)) {
			if($file == "." || $file == ".." || $file == 'Thumbs.db' || $file == 'readme.txt' || $file == 'index.html' || $file == 'index.htm') {
				continue;
			} elseif (is_dir($fromdir."/".$file)) {
				if ($recursed == 1) {
					$temp = CopyFiles($fromdir."/".$file, $todir."/".$file, $recursed);
				}
			} elseif (file_exists($fromdir."/".$file)) {
				/* copy($fromdir."/".$file, $todir."/".$file); */
				/* Trying to overcome the issue of installation on some
				   systems where copy command may be issuing some unwanted checks.  */
				$data = file_get_contents($fromdir."/".$file);
				$fout = fopen($todir."/".$file,'wb');
				$fx = fwrite($fout, $data);
				fclose($fout);
			}
		}

		closedir($dir);
		return true;
	}
}

function getStates ($countrycode='US', $all='Y', $order='name') {

	$states = array();

	global $db;

	$sql = 'select code, name  from ! where countrycode = ? order by !';

	$recs = $db->getAll($sql, array( STATES_TABLE, $countrycode, $order ) );

	if (count($recs) <= 0) return $states;

	foreach ($recs as $rec) {

		$states[$rec['code']] = $rec['name'];

	}

	$recs = $states;

	$states=array();
	if ($all == 'Y') {

		$states['AA'] = ($recs['AA']!='')?$recs['AA']:'All States';

	}
	foreach ($recs as $key => $val) {

		if ($key != 'AA') {

			$states[$key] = $val;

		}
	}

	return $states;

}

function getCounties ($countrycode='US', $statecode = 'AA', $all='Y', $order='name') {

	$counties = array();

	global $db;

	if ($statecode == 'AA') {
		$sql = 'select code, name from ! where countrycode = ? and statecode <> ? order by !';
	} else {
		$sql = 'select code, name from ! where countrycode = ? and statecode = ? order by !';
	}

	$recs = $db->getAll($sql, array( COUNTIES_TABLE, $countrycode, $statecode, $order ) );

	if (count($recs) <= 0) return $counties;

	foreach ($recs as $rec) {

		$counties[$rec['code']] = $rec['name'];

	}

	$recs = $counties;

	$counties=array();
	if ($all == 'Y') {

		$counties['AA'] = ($recs['AA']!='')?$recs['AA']:'All Counties/Districts';

	}
	foreach ($recs as $key => $val) {

		if ($key != 'AA') {

			$counties[$key] = $val;

		}
	}

	return $counties;

}

function getCities ($countrycode='US', $statecode = 'AA', $countycode = 'AA', $all='Y', $order='name') {

	$cities = array();

	global $db;

	if ($countycode == 'AA') {
	 	$sql = 'select code, name from ! where countrycode = ? and statecode = ? and countycode <> ? order by !';
	} else {
	 	$sql = 'select code, name from ! where countrycode = ? and statecode = ? and countycode = ? order by !';
	}
	$recs = $db->getAll($sql, array( CITIES_TABLE, $countrycode, $statecode, $countycode, $order ) );

	if (count($recs) <= 0) return $cities;

	foreach ($recs as $rec) {

		$cities[$rec['code']] = $rec['name'];

	}

	$recs = $cities;

	$cities=array();
	if ($all == 'Y') {

		$cities['AA'] = ($recs['AA']!='')?$recs['AA']:'All Cities/Towns';

	}
	foreach ($recs as $key => $val) {

		if ($key != 'AA') {

			$cities[$key] = $val;

		}
	}

	return $cities;

}


function getZipcodes ($countrycode='US', $statecode = 'AA', $countycode = 'AA', $citycode = 'AA', $all='Y', $order='code') {

	$zipcodes = array();

	global $db;

	if ($citycode == 'AA') {
		$sql = 'select code, code as cd1 from ! where countrycode = ? and statecode = ? and countycode = ? and citycode <> ? order by code';
	} else {
		$sql = 'select code, code as cd1  from ! where countrycode = ? and statecode = ? and countycode = ? and citycode = ? order by code';
	}
	$recs = $db->getAll($sql, array( ZIPCODES_TABLE, $countrycode, $statecode, $countycode, $citycode) );

	if (count($recs) <= 0) return $zipcodes;

	foreach ($recs as $rec) {

		$zipcodes[$rec['code']] = $rec['cd1'];

	}

	$recs = $zipcodes;

	$zipcodes=array();
	if ($all == 'Y') {

		$zipcodes['AA'] = ($recs['AA']!='')?$recs['AA']:'All Zip/Pin Codes';

	}
	foreach ($recs as $key => $val) {

		if ($key != 'AA') {

			$zipcodes[$key] = $val;

		}
	}

	return $zipcodes;

}

function zipsAvailable($cntry_code) {
	/* Function to check if zip code data is available for this country */

	global $db;

	$ret = $db->getOne('select 1 from ! where countrycode = ? limit 1,1', array( ZIPCODES_TABLE, $cntry_code) );

	if (isset($ret) && $ret > 0) return $ret;

	return 0;

}

function mailSender ($hdr_from, $hdr_to, $email, $subject, $body, $attachment='') {
/*
This is a wrapper function for sending emails
	$hdr_from  - THe from address to be kept in the header
	$hdr_to    - The to name and address to be kept in the Header
	$email     - Email address to which the mail to be sent
	$subject   - Subject of the email
	$body      - The body of the email
	$attachment - Mail Attachment
*/

	/* Construct the header portion */

	include_once (PEAR_DIR.'Mail/mime.php');

	$crlf = chr(13);

	$headers = array (
		'From'    	=> $hdr_from,
		'To' 		=> $hdr_to,
		'Subject' 	=> stripslashes($subject)
	);

	$mime = new Mail_mime($crlf);

	if (MAIL_FORMAT == 'text') {

		$body = str_replace('<br>',$crlf,$body);
		$body = str_replace('<br />',$crlf,$body);
		$body = str_replace('<br/>',$crlf,$body);

		// replace site link with full URL

		global $config;

/*		$site_name = $config['site_name'];
		$site_url = 'http://' . $_SERVER['SERVER_NAME'] . DOC_ROOT;
		$site_link = '<a href="' . $site_url . '">' . $site_name . '</a>';

		$body = str_replace( $site_link, $site_url, $body );
*/
		// remove any final tags
		$body = strip_tags( $body );

		$mime->setTXTBody($body);

	} else {

		$body = nl2br($body);
		$mime->setHTMLBody($body);

	}


	if (!is_array($attachment) ) {
		$attach_files = explode(',',$attachment);
	} else {
		$attach_files = $attachment;
	}
	if (count($attach_files) > 0) {
		foreach ($attach_files as $file) {
			if ($file != '') {
				$mime->addAttachment("../emailimages/".$file);
			}
		}
	}

	$body = $mime->get();

	$hdrs = $mime->headers($headers);

	if (MAIL_TYPE == 'smtp') {

		$params['host'] = SMTP_HOST;
		$params['port'] = SMTP_PORT;
		$params['auth'] = (SMTP_AUTH=='1') ? true : false ;
		$params['username'] = SMTP_USER;
		$params['password'] = SMTP_PASS;
	}

	if ( trim( MAIL_TYPE ) == '' ) {
		$mail_type = 'mail';
	}
	else {
		$mail_type = MAIL_TYPE;
	}

	$mail_object =& Mail::factory( $mail_type, $params );

	return($mail_object->send( $email, $hdrs, $body ) );
}

function process_payment_info($params) {

	global $db;

	// get the user information for this transaction

	$trnrec=$db->getRow('select * from ! where id = ?', array(TRANSACTIONS_TABLE, $params['pay_txn_id']) );

	$user_id = $trnrec['user_id'];

	$user_level		= $trnrec['to_membership'];

	$levelvars = $db->getRow( 'select activedays, name from ! where roleid = ?', array( MEMBERSHIP_TABLE, $user_level ) );

	$activedays		= $levelvars['activedays'];
	$level_name		= $levelvars['name'];

	if ( $params['valid'] ) {

		$sql = 'update ! set payment_email = ?, amount_paid = ?, txn_id = ?, txn_date = ?, payment_vars = ?, payment_status=? where id = ?';

		$db->query( $sql, array( TRANSACTIONS_TABLE, $params['email'], $params['amount'], $params['txn_id'], date('Y-m-d'), $params['vars'], $params['payment_status'], $params['pay_txn_id'] ) );

		// determine when this user's membership was to expire, then extend it by $activedays days

		if ($params['payment_status'] == 'Completed') {
			$curlevel = $db->getRow( 'select levelend, level from ! where id = ?', array( USER_TABLE, $user_id ) );

			$levelend = $curlevel['levelend'];

			if ( $levelend < time() ) {
				$levelend = time();
			}

			// new expiration date for this member

			if ($curlevel['level'] != $user_level) {

				$levelend = strtotime( "+$activedays day", time() );

			} else {

				$levelend = strtotime( "+$activedays day", $levelend );

			}

			$sql = 'UPDATE ! SET level = ?, levelend = ? WHERE id = ?';

			$db->query( $sql, array( USER_TABLE, $user_level, $levelend, $user_id ) );
		}
	}

	return $level_name;
}

function getTplFile ($resource_type, $resource_name, &$template_source, &$template_timestamp, &$smarty_obj)
{
	global $skin_name;
	if ( !is_readable ( $resource_name )) {
		// create the template file, return contents.
		$new_resource = str_replace('/templates/','/templates/',$smarty_obj->template_dir).$resource_name;
		$template_timestamp = filemtime($new_resource);
		$template_source = file_get_contents($new_resource);
		return true;
	}

}

?>