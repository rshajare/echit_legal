/**
 * Created by swara on 11/09/2017.
 */
// Chheck branch
$(function() {
	$( "#txtSearchMemberName" ).autocomplete({
        source: function( request, response ) {
            selectedBranch = $("#txtmain_branch_id").val();
            if(selectedBranch > 0 ){
                $.ajax({
                    url: "lgl_customer.php?action=search&branch="+selectedBranch,
                    dataType: "json",
                    data: {
                        searchName: request.term
                    },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                id: item.id,
                                label: item.label,
                                value: item.value
                            };
                        }));
                    }
                });
            }else {
                alert("select Branch to Search");
            }
        },
        minLength: 2,
        select: function( event, ui ) {
            $( "#txtcustomer_id" ).val(ui.item.id);
           // memberinfo(1,ui.item.id);
        }
    } );
	$( "#txtcustomer_name" ).autocomplete({
        source: function( request, response ) {
            selectedBranch = $("#txtmain_branch_id").val();
            if(selectedBranch > 0 ){
                $.ajax({
                    url: "lgl_customer.php?action=search&type=lgl&branch="+selectedBranch,
                    dataType: "json",
                    data: {
                        searchName: request.term
                    },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                id: item.id,
                                label: item.label,
                                value: item.value
                            };
                        }));
                    }
                });
            }else {
                alert("select Branch to Search");
            }
        },
        minLength: 2,
        select: function( event, ui ) {
            $( "#txtcustomer_id" ).val(ui.item.id);
           fncust_change();
        }
    } );
	$( "#txtkeyword" ).autocomplete({
        source: function( request, response ) {
            selectedBranch = $("#txtkeyword5").val();
            if(selectedBranch > 0 ){
                $.ajax({
                    url: "lgl_customer.php?action=listsearch&branch="+selectedBranch,
                    dataType: "json",
                    data: {
                        searchName: request.term
                    },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                id: item.id,
                                label: item.label,
                                value: item.value
                            };
                        }));
                    }
                });
            }else {
                alert("select Branch to Search");
            }
        },
        minLength: 2,
        select: function( event, ui ) {
            $( "#txtcustomer_id" ).val(ui.item.id);
          // fncust_change();
        }
    } );
	$( "#txtSearchCustomerName" ).autocomplete({
        source: function( request, response ) {
                $.ajax({
                    url: "lgl_customer.php?action=listcustsearch",
                    dataType: "json",
                    data: {
                        searchName: request.term
                    },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                id: item.id,
                                label: item.label,
                                value: item.value
                            };
                        }));
                    }
                });
        },
        minLength: 2,
        select: function( event, ui ) {
            $( "#txtcustomer_id" ).val(ui.item.id);
          // fncust_change();
        }
    } );
});

