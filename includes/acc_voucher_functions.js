function validate(form) { 
	// Checking if at least one period button is selected. Or not. 

	var Privilege_Type_ID = document.getElementById("Privilege_Type_ID").value;
	
	if ((document.getElementById("total_db_cr").value )!=0){
		alert("Balance is not correct"); 
		return false;	
	}
	if ((document.frm_new.txt_company_branch.value.length) ==0 ){ 
		alert("Please select branch"); 
		return false;
	}	
	if ((document.frm_new.txtvoucher_type.value.length) ==0 ){ 
		alert("Please select voucher type"); 
		return false; 
	}
	if ((document.frm_new.txttrans_date.value.length) ==0 ){ 
		alert("Please select voucher date"); 
		return false;
	}
	if(document.frm_new.txttrans_date.value.length>0){
		if(ValidateDate(document.frm_new.txttrans_date.value)!=""){
			alert("Voucher Date : "+ValidateDate(document.frm_new.txttrans_date.value));	 
		}
	}
	var str1 = document.getElementById("txttrans_date").value;
  	var str2 = document.getElementById("date_close").value;
	var dt1  = parseInt(str1.substring(0,2),10);
    var mon1 = parseInt(str1.substring(3,5),10);
    var yr1  = parseInt(str1.substring(6,10),10);
    var dt2  = parseInt(str2.substring(0,2),10);
    var mon2 = parseInt(str2.substring(3,5),10);
    var yr2  = parseInt(str2.substring(6,10),10);
    var date1 = new Date(yr1, mon1, dt1);
    var date2 = new Date(yr2, mon2, dt2); 
	
  	var fn_from = document.getElementById("fn_from").value;
  	var fn_to = document.getElementById("fn_to").value;		
	var fn_dt1  = parseInt(fn_from.substring(0,2),10);
    var fn_mon1 = parseInt(fn_from.substring(3,5),10);
    var fn_yr1  = parseInt(fn_from.substring(6,10),10);
    var fn_dt2  = parseInt(fn_to.substring(0,2),10);
    var fn_mon2 = parseInt(fn_to.substring(3,5),10);
    var fn_yr2  = parseInt(fn_to.substring(6,10),10);
    var fn_date1 = new Date(fn_yr1, fn_mon1, fn_dt1);
    var fn_date2 = new Date(fn_yr2, fn_mon2, fn_dt2); 
	
/*	if (Privilege_Type_ID !=31){
		if(date1.getTime()<date2.getTime()){
			alert("Please Select Date Greater Than Closing Date");
			return false;
		}
	}*/
	
	if((date1.getTime()<fn_date1.getTime()) || (date1.getTime()>fn_date2.getTime())){
		alert("Please Select Date from current fiancial year");
		return false;
	}
	
	var vocher=document.getElementById("txtvoucher_type").options[document.getElementById("txtvoucher_type").selectedIndex].text;
	
	/*
	if(vocher!="Journal voucher"){			
		if(document.getElementById("paymentBy").value==""){
			alert("please select paymentBy");
			document.getElementById("paymentBy").focus();
			return false;
		}
	}
	if(document.getElementById("paymentBy").value=="cheque"){
		if(document.getElementById("txtchequNo").value==""){
			alert("please enter cheque no");
			document.getElementById("txtchequNo").focus();
			return false;
		}
		if(document.getElementById("txtchequeDate").value==""){
			alert("please enter cheque date");
			document.getElementById("txtchequeDate").focus();
			return false;
		}
		if(document.getElementById("txtbankName").value==""){
			alert("please enter Bank name");
			document.getElementById("txtbankName").focus();
			return false;
		}
	}
	*/
	var acount_sel_flag = 0;
	var total_cnt =document.getElementById("total_no_accounts").value;
	for(var cnt=0;cnt<total_cnt;cnt++){
		txt_acc = document.getElementById("txt_acc"+cnt).value;		
		by_to = document.getElementById("txt_by_to"+cnt).value;
		if(txt_acc != ""){
			acount_sel_flag = 1;
			if (by_to == "by"){
				db_amt 		= document.getElementById("txt_db_amt"+cnt).value;
				if(isNaN(db_amt)) db_amt = 0;
				if(db_amt == '') db_amt = 0;
				db_amt		= parseFloat(db_amt);
				if(db_amt <= 0){
					alert("Please check Debit Amount.Debit Amount should be greater than Zero")	;
					document.getElementById("txt_db_amt"+cnt).focus();
					return false;
				}
			}else{
				cr_amt = document.getElementById("txt_cr_amt"+cnt).value;
				if(isNaN(cr_amt)) cr_amt = 0;
				if(cr_amt == '') cr_amt = 0;
				cr_amt	= parseFloat(cr_amt);
				if(cr_amt <= 0){
					alert("Please check Credit Amount.Credit Amount should be greater than Zero")	;
					document.getElementById("txt_cr_amt"+cnt).focus();
					return false;
				}
			}	
		
		}else{
			//acount_sel_flag = 1;
			if (by_to == "by"){
				db_amt 		= document.getElementById("txt_db_amt"+cnt).value;
				if(isNaN(db_amt)) db_amt = 0;
				if(db_amt == '') db_amt = 0;
				db_amt		= parseFloat(db_amt);
				if(db_amt > 0){
					alert("Please Select Account")	;
					document.getElementById("txt_acc"+cnt).focus();
					return false;
				}
			}else{
				cr_amt = document.getElementById("txt_cr_amt"+cnt).value;
				if(isNaN(cr_amt)) cr_amt = 0;
				if(cr_amt == '') cr_amt = 0;
				cr_amt	= parseFloat(cr_amt);
				if(cr_amt > 0){
					alert("Please Select Account")	;
					document.getElementById("txt_acc"+cnt).focus();
					return false;
				}
			}	
			
		}
	}
	if(acount_sel_flag == 0){
		alert("please Select Account ");
		return false;
	}
	
	//return true;
}
function hidedetails(){
	if(document.getElementById("paymentBy").value=="cheque"){
		document.getElementById("txtchequNo").disabled=false;
		document.getElementById("txtchequeDate").disabled=false;
		document.getElementById("txtbankName").disabled=false;
	}else{
		document.getElementById("txtchequNo").disabled=true;
		document.getElementById("txtchequeDate").disabled=true;
		document.getElementById("txtbankName").disabled=true;
	
	}
}
function getCloseDate(txt_company_branch){
	ajax_server_check();
	document.getElementById("div_vouchert_type").style.display = 'none';
	document.getElementById("div_vouchert_note").style.display = 'none';
	document.getElementById("div_vouchert_list").style.display = 'none';
	var branch_id 	= document.getElementById("txt_company_branch").value;
	if(branch_id != ""){
		document.getElementById("div_vouchert_type").style.display = 'block';
		//document.getElementById("div_vouchert_note").style.display = 'block';
		//document.getElementById("div_vouchert_list").style.display = 'block';
		var total_cnt =document.getElementById("total_no_accounts").value;
		for(var cnt=0;cnt<total_cnt;cnt++){
			document.getElementById("txt_acc"+cnt).value="";
		}
		var voucher_type 	= document.getElementById("txtvoucher_type").value;
		xmlHttp.onreadystatechange=function(){
			if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
					var xmlDoc = xmlHttp.responseXML;
					document.getElementById("date_close").value =xmlDoc.getElementsByTagName("close_date")[0].firstChild.nodeValue; 
					document.getElementById("fn_from").value =xmlDoc.getElementsByTagName("fn_from_date")[0].firstChild.nodeValue;
					document.getElementById("fn_to").value =xmlDoc.getElementsByTagName("fn_to_date")[0].firstChild.nodeValue;
					
					if(voucher_type != ""){
						fnvchtype_change();
						//fnfillAccounts('txt_company_branch','div_accounts','','fnshow_acc_balance','','BlackNormalFont',voucher_type); 
						//fnbytochange();
					}
				}
			}
		xmlHttp.open("GET",'acc_ajax_function.php?validate_date_close=1&brach_id='+branch_id,false);
		xmlHttp.send(null);	
	}
}
function fnEdit_transaction(cnt){
		amount=document.getElementById("txt_list_trans_amt"+cnt).value;
		var amt_td=document.getElementById("td_amt_"+cnt);
		amt_td.innerHTML="";
		var  textbox= document.createElement("input");
		textbox.setAttribute("type","text"); 
		textbox.setAttribute("name","txt_list_trans_amt"+cnt);
		textbox.setAttribute("id","txt_list_trans_amt"+cnt);
		textbox.setAttribute("value",amount);
		amt_td.appendChild(textbox);
		
		document.getElementById("option_update_id_"+cnt).style.display='block';
		document.getElementById("option_id_"+cnt).style.display='none';
		
}
function fnupdate_transaction(cnt){
		amount=document.getElementById("txt_list_trans_amt"+cnt).value;
		var amt_td=document.getElementById("td_amt_"+cnt);
		amt_td.innerHTML=amount;
		var  textbox= document.createElement("input");
		textbox.setAttribute("type","hidden"); 
		textbox.setAttribute("name","txt_list_trans_amt"+cnt);
		textbox.setAttribute("id","txt_list_trans_amt"+cnt);
		textbox.setAttribute("value",amount);
		amt_td.appendChild(textbox);
		document.getElementById("option_update_id_"+cnt).style.display='none';
		document.getElementById("option_id_"+cnt).style.display='block';
		 cal_tot();
}

function fnbytochange(cnt){
	var total_cnt =document.getElementById("total_no_accounts").value;
	var by_to = document.getElementById("txt_by_to"+cnt).value;
	document.getElementById("div_acc_balance"+cnt).innerHTML	= "";
	if (by_to == "by"){
		document.getElementById("div_by_accounts").style.display = 'block';
		fnCreateOtherCombo('txt_by_acc','txt_acc'+cnt);
		document.getElementById("div_by_accounts").style.display = 'none';
	}else if (by_to == "to"){
		document.getElementById("div_to_accounts").style.display = 'block';
		fnCreateOtherCombo('txt_to_acc','txt_acc'+cnt);
		document.getElementById("div_to_accounts").style.display = 'none';
	}
}
function fnbytochangeall(cnt){
	var total_cnt =document.getElementById("total_no_accounts").value;
	for(var cnt=0;cnt<total_cnt;cnt++){
		var by_to = document.getElementById("txt_by_to"+cnt).value;
		document.getElementById("div_acc_balance"+cnt).innerHTML	= "";
		if (by_to == "by"){
			document.getElementById("div_by_accounts").style.display = 'block';
			fnCreateOtherCombo('txt_by_acc','txt_acc'+cnt);
			document.getElementById("div_by_accounts").style.display = 'none';
		}else if (by_to == "to"){
			document.getElementById("div_to_accounts").style.display = 'block';
			fnCreateOtherCombo('txt_to_acc','txt_acc'+cnt);
			document.getElementById("div_to_accounts").style.display = 'none';
		}
	}
}

function fnvchtype_change(){
	var branch_id 		= document.getElementById("txt_company_branch").value;
	if(branch_id != ""){
		var voucher_type 	= document.getElementById("txtvoucher_type").value;
		document.getElementById("div_vouchert_note").style.display = 'none';
		document.getElementById("div_vouchert_list").style.display = 'none';
		if(voucher_type != ""){
			document.getElementById("div_vouchert_note").style.display = 'block';
			document.getElementById("div_vouchert_list").style.display = 'block';
			fnfillAccounts('txt_company_branch','div_accounts','','fnshow_acc_balance','','BlackNormalFont',voucher_type); 	
			var transaction_id 	= document.getElementById("transaction_id").value;
			var total_cnt 	= document.getElementById("total_no_accounts").value;
			if(transaction_id != ""){
				var debit_total = 0;
				var credit_total = 0;
				for(var cnt=0;cnt<total_cnt;cnt++){
					document.getElementById("txt_acc"+cnt).value 	= document.getElementById("txt_acc_selected"+cnt).value;
					enable_diable(cnt);
					fnshow_acc_balanceNew(cnt); 
				}
				fnAmountTotal();
			}else{
				for(var cnt=0;cnt<total_cnt;cnt++){
					fnbytochange(cnt);
				}				
			}
			hidedetails();
		}
	}
}

function fnCreateOtherCombo(combobox_from_id,combobox_to_id){
 	var cmb		= document.getElementById(combobox_to_id);
	var sel		= document.getElementById(combobox_from_id);
	cmb.innerHTML = sel.innerHTML;
}
function enable_diable(cnt){
	var by_to 	= document.getElementById("txt_by_to"+cnt).value;
	var txt_acc = document.getElementById("txt_acc"+cnt).value;
	if(txt_acc != ""){
		document.getElementById("txt_trans_note"+cnt).style.display = 'block';
		document.getElementById("txt_trans_note"+cnt).disabled 		= false;
		if (by_to == "by"){
			document.getElementById("txt_db_amt"+cnt).style.display = 'block';
			document.getElementById("txt_db_amt"+cnt).disabled 		= false;
			document.getElementById("txt_cr_amt"+cnt).style.display = 'none';
		}else{
			document.getElementById("txt_db_amt"+cnt).style.display = 'none';
			document.getElementById("txt_cr_amt"+cnt).disabled 		= false;
			document.getElementById("txt_cr_amt"+cnt).style.display = 'block';
		}			
	}else{
		document.getElementById("txt_trans_note"+cnt).disabled 		= true;
		document.getElementById("txt_db_amt"+cnt).disabled 			= true;
		document.getElementById("txt_cr_amt"+cnt).disabled 			= true;
		document.getElementById("txt_db_amt"+cnt).style.display 	= 'none';
		document.getElementById("txt_cr_amt"+cnt).style.display 	= 'none';
		document.getElementById("txt_trans_note"+cnt).style.display = 'none';
	}	
}
function fnshow_acc_balanceNew(cnt){
/*	ajax_server_check();
	var from_acc_id;
	from_acc_id = document.getElementById("txt_acc"+cnt).value;
	document.getElementById("div_acc_balance"+cnt).innerHTML ="";
	if(from_acc_id != ""){
		xmlHttp.onreadystatechange=function(){
			if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
				document.getElementById("div_acc_balance"+cnt).innerHTML = xmlHttp.responseText;
				document.getElementById('txt_trans_note'+cnt).focus();
			}
		}
		var url_1;
		url_1 = "acc_ajax_function.php?show_acc_balance=1&from_acc_id="+from_acc_id;
		xmlHttp.open("GET",url_1,false);
		xmlHttp.send(null);
	}else{
		document.getElementById("div_acc_balance"+cnt).innerHTML = "";	
	}*/
}
function fnAmountTotal(){
	var total_cnt 	= document.getElementById("total_no_accounts").value;
	var debit_total = 0;
	var credit_total = 0;
	for(var cnt=0;cnt<total_cnt;cnt++){
		var by_to = document.getElementById("txt_by_to"+cnt).value;
		if (by_to == "by"){
			db_amt 		= document.getElementById("txt_db_amt"+cnt).value;
			if(isNaN(db_amt)) db_amt = 0;
			if(db_amt == '') db_amt = 0;
			db_amt		= parseFloat(db_amt);
			debit_total	= parseFloat(debit_total);
			debit_total += db_amt;
		}else{
			cr_amt = document.getElementById("txt_cr_amt"+cnt).value;
			if(isNaN(cr_amt)) cr_amt = 0;
			if(cr_amt == '') cr_amt = 0;
			cr_amt	= parseFloat(cr_amt);
			credit_total	= parseFloat(credit_total);
			credit_total += cr_amt;
		}	
	}
	document.getElementById("div_debit_amount_total").innerHTML  = debit_total;
	document.getElementById("div_credit_amount_total").innerHTML = credit_total;
	document.getElementById("txtdebit_amount_total").value 		 = debit_total;
	document.getElementById("txtcredit_amount_total").value		 = credit_total;
	document.getElementById("div_total_db_cr").innerHTML		 = debit_total - credit_total;
	document.getElementById("total_db_cr").value		 		 = debit_total - credit_total;
}
function fnOnload(){
	getCloseDate('txt_company_branch','txt_acc');
	//fnvchtype_change();	
	/*hidedetails();
	var total_cnt 	= document.getElementById("total_no_accounts").value;
	var debit_total = 0;
	var credit_total = 0;
	for(var cnt=0;cnt<total_cnt;cnt++){
		document.getElementById("txt_acc"+cnt).value 	= document.getElementById("txt_acc_selected"+cnt).value;
		enable_diable(cnt);
		fnshow_acc_balanceNew(cnt); 
	}
	fnAmountTotal();*/
}