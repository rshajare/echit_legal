function fnAdd_transaction(op,req_no){
	var passData = "";
	req_acc_id 		 	 = document.getElementById("txt_acc").value;
	passData 			+= "&acc_id="+req_acc_id;
	req_trans_note 		 = document.getElementById("txt_trans_note").value;
	passData 			+= "&trans_note="+req_trans_note;
	req_by_to	 	 = document.getElementById("txt_by_to").value;
	passData 			+= "&by_to="+req_by_to;
	req_amt 	 	 = document.getElementById("txt_amt").value;
	if (req_amt == ""){ req_amt=0;}
	passData 		 	+= "&amt="+req_amt;
	req_debit_credit  = document.getElementById("txt_debit_credit").value;
	passData 			+= "&debit_credit="+req_debit_credit;
	var num_field = document.getElementById("txt_hid_num_trans").value;
	var by_cnt=0;
	if(num_field > 0){
		for(var i =1; i <= num_field; i++){
			//passData += "&list_trans%5B%5D=" 				+ document.getElementById("list_trans"+i).value;
			passData += "&list_trans_acc%5B%5D=" 			+ document.getElementById("txt_list_trans_acc"+i).value;
			passData += "&list_trans_note%5B%5D="		+ document.getElementById("txt_list_trans_note"+i).value;
			passData += "&list_trans_by_to%5B%5D=" 			+ document.getElementById("txt_list_trans_by_to"+i).value;
			if (document.getElementById("txt_list_trans_by_to"+i).value == "by"){by_cnt=by_cnt+1;}				
			passData += "&list_trans_amt%5B%5D=" 			+ document.getElementById("txt_list_trans_amt"+i).value;
			passData += "&list_trans_db_cr%5B%5D=" 			+ document.getElementById("txt_list_trans_db_cr"+i).value;
		}
	}
	if (op==1){
		if (req_acc_id == ""){ 
			alert("Please select Account"); 
			return false;
		}
		
		if ((by_cnt > 0)&&(req_by_to=='by')){ 
			alert("By Account can be only one"); 
			return false;
		}
	}
	document.getElementById("txt_acc").value 			= "";
	document.getElementById("txt_trans_note").value 	= "";
	document.getElementById("txt_by_to").value 			= "";
	document.getElementById("txt_amt").value 		 	= "";
	document.getElementById("txt_debit_credit").value 	= "";

	ajax_server_check();
	document.getElementById("list_par").innerHTML = "";
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
			document.getElementById("list_par").innerHTML = xmlHttp.responseText;
		}
	}
	var url_1;
	url_1 = "acc_ajax_function.php?add_voucher_trans=1&op="+op+"&no="+req_no;
	xmlHttp.open("POST",url_1,true);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  
	xmlHttp.send(passData);

}
function fnshow_acc_balance(ctrl_name){
	ajax_server_check();
	from_acc_id = document.getElementById(ctrl_name).value;
	if(from_acc_id != ""){
		xmlHttp.onreadystatechange=function(){
			if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
				document.getElementById("div_acc_balance").innerHTML = xmlHttp.responseText;
			}
		}
		var url_1;
		url_1 = "acc_ajax_function.php?show_acc_balance=1&from_acc_id="+from_acc_id;
		xmlHttp.open("GET",url_1,true);
		xmlHttp.send(null);
	}else{
		document.getElementById("div_acc_balance").innerHTML = "";	
	}
}