<?php 
	ob_start ("ob_gzhandler");
	header("Content-type: text/javascript; charset: UTF-8");
	header("Cache-Control: must-revalidate");
	$offset = 60 * 60 * 24 * 3;
	$ExpStr = "Expires: " . gmdate("D, d M Y H:i:s",time() + $offset) . " GMT";
	header($ExpStr);
?>var calender={
	disappeardelay: 250,
	disablemenuclick: true,
	enableswipe: 1,
	enableiframeshim: 1,
	dropmenuobj: null, ie: document.all, firefox: document.getElementById&&!document.all, swipetimer: undefined, bottomclip:0,
	getposOffset:function(what, offsettype){
		var totaloffset=(offsettype=="left")? what.offsetLeft : what.offsetTop;
		var parentEl=what.offsetParent;
		while (parentEl!=null){
			if(this.ie){
				totaloffset = (offsettype=="left") ? totaloffset + parentEl.offsetLeft  : totaloffset + parentEl.offsetTop;
			}else{
				totaloffset = (offsettype=="left") ? totaloffset + parentEl.offsetLeft  : totaloffset + parentEl.offsetTop;
			}
			parentEl=parentEl.offsetParent;
		}
		if(offsettype=="left"){
			totaloffset = totaloffset - 150;
		}
		return totaloffset;
	},
	iecompattest:function(){
		return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body	
	},
	clearbrowseredge:function(obj, whichedge){
		var edgeoffset = 0
		if (whichedge == "rightedge"){
			var windowedge	=	this.ie && !window.opera ? this.iecompattest().scrollLeft+this.iecompattest().clientWidth-15 : window.pageXOffset+window.innerWidth-15
			this.dropmenuobj.contentmeasure=this.dropmenuobj.offsetWidth
			if (windowedge - this.dropmenuobj.x < this.dropmenuobj.contentmeasure)  //move menu to the left?
				edgeoffset	= this.dropmenuobj.contentmeasure - obj.offsetWidth
		}else{
			var topedge 	= this.ie && !window.opera? this.iecompattest().scrollTop : window.pageYOffset
			var windowedge 	= this.ie && !window.opera? this.iecompattest().scrollTop+this.iecompattest().clientHeight-15 : window.pageYOffset+window.innerHeight-18
			this.dropmenuobj.contentmeasure = this.dropmenuobj.offsetHeight
			if (windowedge - this.dropmenuobj.y < this.dropmenuobj.contentmeasure){ //move up?
				edgeoffset = this.dropmenuobj.contentmeasure+obj.offsetHeight
				if ((this.dropmenuobj.y-topedge) < this.dropmenuobj.contentmeasure) //up no good either?
					edgeoffset = this.dropmenuobj.y + obj.offsetHeight - topedge
			}
		}
		return edgeoffset
	},
	dropit:function(dropmenuID){
		this.dropmenuobj		= document.getElementById(dropmenuID)
		this.dropmenuobj.x		= this.getposOffset(this.dropmenuobj, "left")
		this.dropmenuobj.y		= this.getposOffset(this.dropmenuobj, "top")
		this.dropmenuobj.left 	= this.dropmenuobj.x  - this.clearbrowseredge(this, "rightedge")  + "px"
		this.dropmenuobj.top 	= this.dropmenuobj.y  - this.clearbrowseredge(this, "bottomedge") +  this.dropmenuobj.offsetHeight + 1 +"px"
		return [this.dropmenuobj.left , this.dropmenuobj.top];
	}
}