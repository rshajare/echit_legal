//ajax funxction
var xmlHttp;
var msie = (window.ActiveXObject) ? true : false;
var moz = (document.implementation && document.implementation.createDocument) ? true : false;
function ajax_server_check()
{
	try{xmlHttp=new XMLHttpRequest();}catch (e){try{xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");}catch (e){try {xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");}catch (e){alert("Your browser does not support AJAX!"); return false;}}}
}
function fnChange_customer(){
	if(document.getElementById("txtcustomer_name_id").value != ""){
		customer_id = document.getElementById("txtcustomer_name_id").value;
		document.getElementById("txtcustomer_first_name").disabled 		= true;
		document.getElementById("txtcustomer_last_name").disabled 		= true;
		if(document.getElementById("txtcustomer_contact_name")) document.getElementById("txtcustomer_contact_name").disabled	= true;
		document.getElementById("txtcustomer_address").disabled 		= true;
		document.getElementById("txtcustomer_contact_no").disabled 		= true;
		document.getElementById("txtcustomer_mobile_no").disabled 		= true;
		document.getElementById("txtcustomer_fax").disabled 			= true;
		document.getElementById("txtcustomer_email").disabled 			= true;
		ajax_server_check();
		xmlHttp.onreadystatechange=function(){
			if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
				var xmlDoc  		 = xmlHttp.responseXML;
				cust_first_name		 = xmlDoc.getElementsByTagName("cust_first_name")[0].childNodes[0].nodeValue;
				cust_last_name		 = xmlDoc.getElementsByTagName("cust_last_name")[0].childNodes[0].nodeValue;
				cust_contact_name 	 = xmlDoc.getElementsByTagName("cust_contact_name")[0].childNodes[0].nodeValue;
				cust_address 		 = xmlDoc.getElementsByTagName("cust_address")[0].childNodes[0].nodeValue;
				cust_telephone_no 	 = xmlDoc.getElementsByTagName("cust_telephone_no")[0].childNodes[0].nodeValue;
				cust_mobile_phone_no = xmlDoc.getElementsByTagName("cust_mobile_phone_no")[0].childNodes[0].nodeValue;
				cust_fax 			 = xmlDoc.getElementsByTagName("cust_fax")[0].childNodes[0].nodeValue;
				cust_email 			 = xmlDoc.getElementsByTagName("cust_email")[0].childNodes[0].nodeValue;
				
				bill_name 			 = xmlDoc.getElementsByTagName("bill_name")[0].childNodes[0].nodeValue;
				bill_address 		 = xmlDoc.getElementsByTagName("bill_address")[0].childNodes[0].nodeValue;
				bill_city 			 = xmlDoc.getElementsByTagName("bill_city")[0].childNodes[0].nodeValue;
				bill_state 			 = xmlDoc.getElementsByTagName("bill_state")[0].childNodes[0].nodeValue;
				bill_country 		 = xmlDoc.getElementsByTagName("bill_country")[0].childNodes[0].nodeValue;
				bill_pin_code 		 = xmlDoc.getElementsByTagName("bill_pin_code")[0].childNodes[0].nodeValue;
				bill_phone 		 	 = xmlDoc.getElementsByTagName("bill_phone")[0].childNodes[0].nodeValue;
				bill_fax 		 	 = xmlDoc.getElementsByTagName("bill_fax")[0].childNodes[0].nodeValue;
				bill_email 		 	 = xmlDoc.getElementsByTagName("bill_email")[0].childNodes[0].nodeValue;
				pay_term 		 	 = xmlDoc.getElementsByTagName("pay_term")[0].childNodes[0].nodeValue;
				delivery_term 	 = xmlDoc.getElementsByTagName("delivery_term")[0].childNodes[0].nodeValue;
										
				document.getElementById("txtcustomer_first_name").value  = cust_first_name;
				document.getElementById("txtcustomer_last_name").value 	 = cust_last_name;
				if(document.getElementById("txtcustomer_contact_name")) document.getElementById("txtcustomer_contact_name").value= cust_contact_name;
				document.getElementById("txtcustomer_address").value 	 = cust_address;
				document.getElementById("txtcustomer_contact_no").value  = cust_telephone_no;
				document.getElementById("txtcustomer_mobile_no").value 	 = cust_mobile_phone_no;
				document.getElementById("txtcustomer_fax").value 		 = cust_fax;
				document.getElementById("txtcustomer_email").value 		 = cust_email;
				
				document.getElementById("txtpay_term").value 		= pay_term;
				document.getElementById("txtdelivery_term").value 		= delivery_term;
				
				document.getElementById("txt_bill_name").value 		= bill_name;
				document.getElementById("txt_bill_address").value 	= bill_address;
				if(document.getElementById("txt_bill_city"))document.getElementById("txt_bill_city").value 		= bill_city;
				if(document.getElementById("txt_bill_state"))document.getElementById("txt_bill_state").value 	= bill_state;
				if(document.getElementById("txt_bill_country"))document.getElementById("txt_bill_country").value 	= bill_country;
				if(document.getElementById("txt_bill_pin"))document.getElementById("txt_bill_pin").value 		= bill_pin_code;
				if(document.getElementById("txt_bill_phone"))document.getElementById("txt_bill_phone").value 	= bill_phone;
				if(document.getElementById("txt_bill_fax"))document.getElementById("txt_bill_fax").value 		= bill_fax;
				if(document.getElementById("txt_bill_email"))document.getElementById("txt_bill_email").value 	= bill_email;
			}
		}
		var url_1;
		url_1 = "inventory_ajax_function.php?custmer_details=1&customer_id="+customer_id;
		xmlHttp.open("GET",url_1,true);
		xmlHttp.send(null);
	}else{
		document.getElementById("txtcustomer_first_name").disabled 		= false;
		document.getElementById("txtcustomer_last_name").disabled 		= false;
		if(document.getElementById("txtcustomer_contact_name")) document.getElementById("txtcustomer_contact_name").disabled= false;
		document.getElementById("txtcustomer_address").disabled 	= false;
		document.getElementById("txtcustomer_contact_no").disabled 	= false;
		document.getElementById("txtcustomer_mobile_no").disabled 	= false;
		document.getElementById("txtcustomer_fax").disabled 		= false;
		document.getElementById("txtcustomer_email").disabled 		= false;
				
		document.getElementById("txtcustomer_first_name").value 		 = "";
		document.getElementById("txtcustomer_last_name").value 		 = "";
		if(document.getElementById("txtcustomer_contact_name")) document.getElementById("txtcustomer_contact_name").value= "";
		document.getElementById("txtcustomer_address").value 	 = "";
		document.getElementById("txtcustomer_contact_no").value  = "";
		document.getElementById("txtcustomer_mobile_no").value 	 = "";
		document.getElementById("txtcustomer_fax").value 		 = "";
		document.getElementById("txtcustomer_email").value 		 = "";
		document.getElementById("txtpay_term").value 		= "";
		document.getElementById("txtdelivery_term").value 		= "";
	}
}
function signout(usrid) {
	ajax_server_check();
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
			res = xmlHttp.responseText;
		}
	}
	var url_1;
	url_1 = "ajax_function.php?op=2&usrid="+usrid;
	xmlHttp.open("GET",url_1,true);
	xmlHttp.send(null);
}
function find_shipping_cost() {
	var shipping_id = document.getElementById("txt_ship_by").value;
	ajax_server_check();
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
			res = xmlHttp.responseText;
			document.getElementById("txt_ship_cost").value = res;
		}
	}
	var url_1;
	url_1 = "inventory_ajax_function.php?find_shipping_cost=1&shipping_id="+shipping_id;
	xmlHttp.open("GET",url_1,true);
	xmlHttp.send(null);
}

//end of ajax function
function fnAdd_product(op,req_no){
	var passData = "";

	product_id = document.getElementById("txtproduct_id").value;
	passData += "&product_id="+product_id;
	product_qty = document.getElementById("txt_product_qty").value;
	passData += "&product_qty="+product_qty;
	product_price 	 = document.getElementById("txt_product_price").value;
	passData 			+= "&product_price="+product_price;

    if (product_id == "")
	   {alert("Plese select product!");
			return false;
		}
	if (product_qty == "")
	   {alert("Plese enter Quantity!");
			return false;
		}
		
	if (product_price == "")
	   {alert("Plese enter Price!");
			return false;
		}	

	var num_field = document.getElementById("txt_hid_num_par").value;
	document.getElementById("txtproduct_id").value = "";
	document.getElementById("txt_product_qty").value = "";
	document.getElementById("txt_product_price").value 		= "";
	if(num_field > 0){
		for(var i =1; i <= num_field; i++){
			passData += "&list_product%5B%5D=" 				+ document.getElementById("txt_list_product_"+i).value;
			passData += "&list_product_qty%5B%5D=" 			+ document.getElementById("txt_list_product_qty"+i).value;
			passData += "&list_product_price%5B%5D=" 	+ document.getElementById("txt_list_product_price"+i).value;
		}
	}
	ajax_server_check();
	document.getElementById("list_par").innerHTML = "";
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
			document.getElementById("list_par").innerHTML = xmlHttp.responseText;
		}
	}
	var url_1;
	url_1 = "inventory_ajax_function.php?add_product=1&op="+op+"&no="+req_no;
	xmlHttp.open("POST",url_1,true);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  
	xmlHttp.send(passData);

}
function fnAdd_quotation_particular(op,req_no){

	var passData = "";
	if(document.getElementById("txtproduct_id")){
		product_id 		 	 = document.getElementById("txtproduct_id").value;
		passData 			+= "&product_id="+product_id;
	}
	if(document.getElementById("txt_product_qty")){
		product_qty 	 	 = document.getElementById("txt_product_qty").value;
		passData 		 	+= "&product_qty="+product_qty;
	}
	if(document.getElementById("txt_product_measure")){
		product_measure 	 	 = document.getElementById("txt_product_measure").value;
		passData 		 	+= "&product_measure="+product_measure;
	}
	
	var multiple_order_one_invoice 	 	 = document.getElementById("multiple_order_one_invoice").value;

	if(document.getElementById("txt_product_price")){
		product_price 	 = document.getElementById("txt_product_price").value;
		passData 			+= "&product_price="+product_price;
	}
	var num_field = document.getElementById("txt_hid_num_par").value;
	if(document.getElementById("txtproduct_id"))		document.getElementById("txtproduct_id").value		= "";
	if(document.getElementById("txt_product_qty"))		document.getElementById("txt_product_qty").value	= "";
	if(document.getElementById("txt_product_measure"))		document.getElementById("txt_product_measure").value	= "";
	if(document.getElementById("txt_product_price"))	document.getElementById("txt_product_price").value	= "";
	if(num_field > 0){
		for(var i =1; i <= num_field; i++){
			passData += "&list_product%5B%5D=" 				+ document.getElementById("txt_list_product_"+i).value;
			passData += "&list_product_qty%5B%5D=" 			+ document.getElementById("txt_list_product_qty"+i).value;	
			passData += "&list_product_measure%5B%5D=" 			+ document.getElementById("txt_list_product_measure"+i).value;	
			passData += "&list_product_price%5B%5D=" 		+ document.getElementById("txt_list_product_price"+i).value;
		}
	}
	ajax_server_check();
	document.getElementById("list_par").innerHTML = "";
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
			document.getElementById("list_par").innerHTML = xmlHttp.responseText;
		}
	}
	var url_1;
	url_1 = "inventory_ajax_function.php?add_quotation_item=1&op="+op+"&no="+req_no;
	xmlHttp.open("POST",url_1,true);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  
	xmlHttp.send(passData);
}
function fnAdd_particular(op,req_no){
	var passData = "";
	
	product_id = document.getElementById("txt_product").value;
	passData += "&product_id="+product_id;
	product_qty = document.getElementById("txt_product_qty").value;
	passData += "&product_qty="+product_qty;
	product_qty_measure = document.getElementById("txt_list_product_qty_measure").value;
	passData += "&product_qty_measure="+product_qty_measure;
/*	if(document.getElementById("txt_product_tax")){
			product_tax = document.getElementById("txt_product_tax").value;
			passData += "&product_tax="+product_tax;
	}
*/	product_adj_price 	 = document.getElementById("txt_product_adj_price").value;
	passData 			+= "&product_adj_price="+product_adj_price;
    if (product_id == "")
	   {alert("Plese select product!");
			return false;
		}
	if (product_qty == "")
	   {alert("Plese enter Quantity!");
			return false;
		}
		
/*		if (product_tax == "")
	   {alert("Plese enter Tax!");
			return false;
		}
*/		
	if (product_adj_price == "")
	   {alert("Plese enter Price!");
			return false;
		}	
	var num_field = document.getElementById("txt_hid_num_par").value;
	document.getElementById("txt_product").value = "";
	document.getElementById("txt_product_qty").value = "";
	document.getElementById("txt_list_product_qty_measure").value = "";
	//document.getElementById("txt_product_tax").value = "";
	document.getElementById("txt_product_adj_price").value 		= "";
	if(num_field > 0){
		for(var i =1; i <= num_field; i++){
			passData += "&list_product%5B%5D=" 				+ document.getElementById("txt_list_product"+i).value;
			passData += "&list_product_qty%5B%5D=" 			+ document.getElementById("txt_list_product_qty"+i).value;
			passData += "&list_product_qty_measure%5B%5D=" 			+ document.getElementById("txt_list_product_qty_measure"+i).value;
			//passData += "&list_product_tax%5B%5D=" 			+ document.getElementById("txt_list_product_tax"+i).value;
			
			passData += "&list_product_adj_price%5B%5D=" 	+ document.getElementById("txt_list_product_adj_price"+i).value;
		}
	}
	ajax_server_check();
	document.getElementById("list_par").innerHTML = "";
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
			document.getElementById("list_par").innerHTML = xmlHttp.responseText;
		}
	}
	var url_1;
	url_1 = "inventory_ajax_function.php?add_item=1&op="+op+"&no="+req_no;
	xmlHttp.open("POST",url_1,true);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  
	xmlHttp.send(passData);
}

function fnAdd_invoice_particular(op,req_no){
	var passData = "";
	if(document.getElementById("txt_product")){
		product_id 		 	 = document.getElementById("txt_product").value;
		passData 			+= "&product_id="+product_id;
	}
	if(document.getElementById("txt_product_qty")){
		product_qty 	 	 = document.getElementById("txt_product_qty").value;
		passData 		 	+= "&product_qty="+product_qty;
	}
	if(document.getElementById("txt_product_tax")){
		product_tax 	 	 = document.getElementById("txt_product_tax").value;
		passData 		 	+= "&product_tax="+product_tax;
	}

	var multiple_order_one_invoice 	 	 = document.getElementById("multiple_order_one_invoice").value;
	if(document.getElementById("txt_product_adj_price")){
		product_adj_price 	 = document.getElementById("txt_product_adj_price").value;
		passData 			+= "&product_adj_price="+product_adj_price;
	}
	var num_field = document.getElementById("txt_hid_num_par").value;
	if(document.getElementById("txt_product"))			document.getElementById("txt_product").value 				= "";
	if(document.getElementById("txt_product_qty"))		document.getElementById("txt_product_qty").value 		 	= "";
	if(document.getElementById("txt_product_tax"))		document.getElementById("txt_product_tax").value 		 	= "";
	//document.getElementById("txt_product_unit_price").value 	= "";
	if(document.getElementById("txt_product_adj_price"))document.getElementById("txt_product_adj_price").value 		= "";
	if(num_field > 0){
		for(var i =1; i <= num_field; i++){
			if (multiple_order_one_invoice==1){
				passData += "&sales_order_id%5B%5D=" 				+ document.getElementById("txt_sales_order_id_"+i).value;
				passData += "&list_product_other_charges%5B%5D=" 	+ document.getElementById("txt_list_product_other_charges"+i).value;
			}
			passData += "&list_product_stock_id%5B%5D=" 		+ document.getElementById("txt_list_product_stock_id"+i).value;
			passData += "&list_product%5B%5D=" 				+ document.getElementById("txt_list_product_"+i).value;
			passData += "&list_product_qty%5B%5D=" 			+ document.getElementById("txt_list_product_qty"+i).value;		
			passData += "&list_product_tax%5B%5D=" 			+ document.getElementById("txt_list_product_tax"+i).value;	
			passData += "&list_product_unit_price%5B%5D=" 	+ document.getElementById("txt_list_product_unit_price"+i).value;
			
			passData += "&list_product_adj_price%5B%5D=" 	+ document.getElementById("txt_list_product_adj_price"+i).value;
		}
	}
	ajax_server_check();
	document.getElementById("list_par").innerHTML = "";
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
			document.getElementById("list_par").innerHTML = xmlHttp.responseText;
			calculate_main_total();
		}
	}
	var url_1;
	url_1 = "inventory_ajax_function.php?add_invoice_item=1&op="+op+"&no="+req_no;
	xmlHttp.open("POST",url_1,true);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  
	xmlHttp.send(passData);
}	
function fnAdd_sales_order_particular(op,req_no){
	var passData = "";
	var product_id = "";
	var stock_id = "";
	if(document.frm_new.txtstock_id){
		var keyword_len1 = document.frm_new.txtstock_id.length;
		if(keyword_len1 > 0){
			for(var i=0;i<keyword_len1;i++){
				if(document.frm_new.txtstock_id[i].checked){
					stock_id 	= 	document.frm_new.txtstock_id[i].value;
				}
			}
		}else{
			stock_id 	= 	document.frm_new.txtstock_id.value;	
		}
	}
	if(stock_id != ""){
		passData 		 	+= "&stock_id="+stock_id;
		product_id 	 	 	= document.getElementById("txt_product_"+stock_id).value;
		passData 		 	+= "&product_id="+product_id;
		product_qty 	 	 = document.getElementById("txt_product_qty_"+stock_id).value;
		passData 		 	+= "&product_qty="+product_qty;
		if(document.getElementById("txt_product_tax_"+stock_id)){
			product_tax 	 	 = document.getElementById("txt_product_tax_"+stock_id).value;
			passData 		 	+= "&product_qty="+product_tax;
		}
		product_unit_price   = document.getElementById("txt_product_unit_price_"+stock_id).value;
		passData 			+= "&product_unit_price="+product_unit_price;
	}
	var num_field = document.getElementById("txt_hid_num_par").value;
	document.getElementById("div_product_list").innerHTML = "";
	if(num_field > 0){
		for(var i =1; i <= num_field; i++){
			passData += "&list_product%5B%5D=" 				+ document.getElementById("txt_list_product_"+i).value;
			passData += "&list_product_qty%5B%5D=" 			+ document.getElementById("txt_list_product_qty"+i).value;
			passData += "&list_product_tax%5B%5D=" 			+ document.getElementById("txt_list_product_tax"+i).value;
			passData += "&list_product_unit_price%5B%5D=" 	+ document.getElementById("txt_list_product_unit_price"+i).value;
			passData += "&list_product_stock_id%5B%5D=" 	+ document.getElementById("txt_list_product_stock_id"+i).value;
		}
	}
	ajax_server_check();
	document.getElementById("list_par").innerHTML = "";
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
			document.getElementById("list_par").innerHTML = xmlHttp.responseText;
		}
	}
	var url_1;
	url_1 = "inventory_ajax_function.php?add_sales_order_item=1&op="+op+"&no="+req_no;
	xmlHttp.open("POST",url_1,true);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  
	xmlHttp.send(passData);
}
function fnAdd_sales_invoice_particular(op,req_no){
	var passData = "";
	var product_id = "";
	var stock_id = "";
	branch_id 	= document.getElementById("txtbranch_id").value;
	passData 	+= "&branch_id="+branch_id;

	if(document.frm_new.txtstock_id){
		var keyword_len1 = document.frm_new.txtstock_id.length;
		if(keyword_len1 > 0){
			for(var i=0;i<keyword_len1;i++){
				if(document.frm_new.txtstock_id[i].checked){
					stock_id 	= 	document.frm_new.txtstock_id[i].value;
				}
			}
		}else{
			stock_id 	= 	document.frm_new.txtstock_id.value;	
		}
	}
	if(stock_id != ""){
		passData 		 	+= "&stock_id="+stock_id;
		product_id 	 	 	= document.getElementById("txt_product_"+stock_id).value;
		passData 		 	+= "&product_id="+product_id;
		product_qty 	 	 = document.getElementById("txt_product_qty_"+stock_id).value;
		passData 		 	+= "&product_qty="+product_qty;
		
		if(document.getElementById("txt_product_tax_"+stock_id)){
			product_tax 	 	 = document.getElementById("txt_product_tax_"+stock_id).value;
			passData 		 	+= "&product_tax="+product_tax;
		}
		product_unit_price   = document.getElementById("txt_product_unit_price_"+stock_id).value;
		passData 			+= "&product_unit_price="+product_unit_price;
		product_adj_price 	 = document.getElementById("txt_product_adj_price_"+stock_id).value;
		passData 			+= "&product_adj_price="+product_adj_price;
	}
	var num_field = document.getElementById("txt_hid_num_par").value;
	document.getElementById("div_product_list").innerHTML = "";
	if(num_field > 0){
		for(var i =1; i <= num_field; i++){
			passData += "&list_product%5B%5D=" 				+ document.getElementById("txt_list_product_"+i).value;
			passData += "&list_product_qty%5B%5D=" 			+ document.getElementById("txt_list_product_qty"+i).value;
			passData += "&list_product_tax%5B%5D=" 			+ document.getElementById("txt_list_product_tax"+i).value;
			passData += "&list_product_unit_price%5B%5D=" 	+ document.getElementById("txt_list_product_unit_price"+i).value;
			passData += "&list_product_adj_price%5B%5D=" 	+ document.getElementById("txt_list_product_adj_price"+i).value;
		}
	}
	ajax_server_check();
	document.getElementById("list_par").innerHTML = "";
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
			document.getElementById("list_par").innerHTML = xmlHttp.responseText;
		}
	}
	var url_1;
	url_1 = "inventory_ajax_function.php?add_sales_invoice_item=1&op="+op+"&no="+req_no;
	xmlHttp.open("POST",url_1,true);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  
	xmlHttp.send(passData);
}
function fnAdd_sales_order_particular(op,req_no){
	var passData = "";
	var product_id = "";
	var stock_id = "";
	if(document.frm_new.txtstock_id){
		var keyword_len1 = document.frm_new.txtstock_id.length;
		if(keyword_len1 > 0){
			for(var i=0;i<keyword_len1;i++){
				if(document.frm_new.txtstock_id[i].checked){
					stock_id 	= 	document.frm_new.txtstock_id[i].value;
				}
			}
		}else{
			stock_id 	= 	document.frm_new.txtstock_id.value;	
		}
	}
	if(stock_id != ""){
		passData 		 	+= "&stock_id="+stock_id;
		product_id 	 	 	= document.getElementById("txt_product_"+stock_id).value;
		passData 		 	+= "&product_id="+product_id;
		product_qty 	 	 = document.getElementById("txt_product_qty_"+stock_id).value;
		if(product_qty==""){
			document.getElementById("txt_product_qty_"+stock_id).focus();
			alert("please enter quantity");
			return false;
		}

		passData 		 	+= "&product_qty="+product_qty;
		if(document.getElementById("txt_product_tax_"+stock_id)){
			product_tax 	 	 = document.getElementById("txt_product_tax_"+stock_id).value;
			passData 		 	+= "&product_tax="+product_tax;
		}
	}
	var num_field = document.getElementById("txt_hid_num_par").value;
	
	document.getElementById("div_product_list").innerHTML = "";
	if(num_field > 0){
		for(var i =1; i <= num_field; i++){
			passData += "&list_product%5B%5D=" 				+ document.getElementById("txt_list_product_"+i).value;
			passData += "&list_product_qty%5B%5D=" 			+ document.getElementById("txt_list_product_qty"+i).value;
			if(document.getElementById("txt_list_product_tax"+i))
				passData += "&list_product_tax%5B%5D=" 		+ document.getElementById("txt_list_product_tax"+i).value;
			passData += "&list_product_stock_id%5B%5D=" 	+ document.getElementById("txt_list_product_stock_id"+i).value;
		}
	}
	ajax_server_check();
	document.getElementById("list_par").innerHTML = "";
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
			document.getElementById("list_par").innerHTML = xmlHttp.responseText;
		}
	}
	var url_1;
	url_1 = "inventory_ajax_function.php?add_sales_order_item=1&op="+op+"&no="+req_no;
	xmlHttp.open("POST",url_1,true);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  
	xmlHttp.send(passData);
}
function fnProduct_sales_order_Stock(req_page_no){
	var passData = "";
	if(req_page_no == "") req_page_no = 1;
	product_code 		 = document.getElementById("txt_search_product_code").value;
	passData 			+= "&product_code="+product_code;
	product_desc 	 	 = document.getElementById("txt_search_product_desc").value;
	passData 		 	+= "&product_desc="+product_desc;
	product_type 	 	 = document.getElementById("txt_search_product_type").value;
	passData 		 	+= "&product_type="+product_type;
	ajax_server_check();
	document.getElementById("div_product_list").innerHTML = "";
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
			var req_response = "";
			req_response = xmlHttp.responseText;
			if(req_response != ""){
				document.getElementById("div_product_list").innerHTML = req_response;
				//fnfind_stock();
			}
		}
	}
	var url_1;
	url_1 = "inventory_ajax_function.php?product_sales_stock_list=1&page="+req_page_no;
	xmlHttp.open("POST",url_1,true);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  
	xmlHttp.send(passData);
}
function fnProduct_Stock(req_page_no){
	var passData = "";
	if(req_page_no == "") req_page_no = 1;
	product_code 		 = document.getElementById("txt_search_product_code").value;
	passData 			+= "&product_code="+product_code;
	product_desc 	 	 = document.getElementById("txt_search_product_desc").value;
	passData 		 	+= "&product_desc="+product_desc;
	product_type 	 	 = document.getElementById("txt_search_product_type").value;
	passData 		 	+= "&product_type="+product_type;
	ajax_server_check();
	document.getElementById("div_product_list").innerHTML = "";
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
			var req_response = "";
			req_response = xmlHttp.responseText;
			if(req_response != ""){
				document.getElementById("div_product_list").innerHTML = req_response;
				//fnfind_stock();
			}
		}
	}
	var url_1;
	url_1 = "inventory_ajax_function.php?product_stock_list=1&page="+req_page_no;
	xmlHttp.open("POST",url_1,true);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  
	xmlHttp.send(passData);
}
function fnAdd_sales_order_expense_particular(op,req_no){
	var passData = "";
	//alert("HERE");
	customer_name_id 	= document.getElementById("txtcustomer_name_id").value;
	passData 			+= "&customer_name_id="+customer_name_id;
	
	payment_by 			= document.getElementById("txtpayment_by").value;
	passData			+= "&payment_by="+payment_by;
	
	amount 				= document.getElementById("txtamount").value;
	passData 			+= "&amount="+amount;
	
	neft_bank 	 		= document.getElementById("txtneft_bank").value;
	passData 			+= "&neft_bank="+neft_bank;
	
	remark 	 			= document.getElementById("txtremark").value;
	passData 			+= "&remark="+remark;
	
	
 /*	   if (neft_bank == "")
	   {alert("Plese select product!");
			return false;
		}*/

	
	
	if((payment_by=='CARD/NEFT')&&(neft_bank=="")){
		alert("Please select bank name");return false;
		}
		
	var num_field = document.getElementById("txt_hid_num_par").value;
	document.getElementById("txtcustomer_name_id").value = "";
	document.getElementById("txtpayment_by").value = "";
	document.getElementById("txtamount").value = "";
	document.getElementById("txtneft_bank").value = "";
	document.getElementById("txtremark").value 		= "";
	
	if(num_field > 0){
		for(var i =1; i <= num_field; i++){
			passData += "&list_customer_name_id%5B%5D="	+ document.getElementById("txt_list_customer_name_id"+i).value;
			passData += "&list_payment_by%5B%5D="		+ document.getElementById("txt_list_payment_by"+i).value;
			passData += "&list_amount%5B%5D=" 			+ document.getElementById("txt_list_amount"+i).value;
			passData += "&list_neft_bank%5B%5D="		+ document.getElementById("txt_list_neft_bank"+i).value;
			passData += "&list_remark%5B%5D="			+ document.getElementById("txt_list_remark"+i).value;
			passData += "&list_transaction_id%5B%5D="	+ document.getElementById("txt_list_transaction_id"+i).value;
		}
	}
	ajax_server_check();
	document.getElementById("list_par").innerHTML = "";
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
			document.getElementById("list_par").innerHTML = xmlHttp.responseText;
		}
	}
	var url_1;
	url_1 = "inventory_ajax_function.php?add_sales_order_expense_item=1&op="+op+"&no="+req_no;
	xmlHttp.open("POST",url_1,true);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  
	xmlHttp.send(passData);
}
function fn_make_present(id,date,flag){
	var url =	"daily_present.php?action=7&emp_id="+id+"&present_date="+date+"&flag="+flag;
	var url1 =	"daily_present.php?action=10&emp_id="+id+"&present_date="+date+"&flag="+flag;;
	
	if (flag==1){
		window.open(url1, '', 'hight=10,width=10');					
	}else{
		window.open(url, '', 'hight=10,width=10');				
	}
}
function fn_change_date(date,flag){
	
	var passData = "";
	passData 			+= "&present_date="+date;
    //alert("HI");
	ajax_server_check();
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
			alert(xmlHttp.responseText);
		}
	}
	var url_1;
	url_1 = "inventory_ajax_function.php?change_date=1&flag="+flag;
	xmlHttp.open("POST",url_1,true);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  
	xmlHttp.send(passData);
}
function fnchange_emp() {
	var passData = "";
	var emp_id = document.getElementById("txtemp_id").value;
	ajax_server_check();
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
			 document.getElementById("txtbasic_pay").value=xmlHttp.responseText;
			fnchange_month();
		}
	}
	var url_1;
	url_1 = "inventory_ajax_function.php?change_emp=1&emp_id="+emp_id;
	xmlHttp.open("POST",url_1,true);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  
	xmlHttp.send(passData);
}
function fnchange_sal_adv_amt() {
	var passData = "";
	var sal_adv_cut = document.getElementById("txtsal_adv_cut").value;
 //   alert("HI");
	ajax_server_check();
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
			//alert(xmlHttp.responseText);
			fnchange_month();
		}
	}
	var url_1;
	url_1 = "inventory_ajax_function.php?change_sal_adv_cut=1&sal_adv_cut="+sal_adv_cut;
	xmlHttp.open("POST",url_1,true);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  
	xmlHttp.send(passData);
}
function fnchange_oth_adv_amt() {
	var passData = "";
	var oth_adv_cut = document.getElementById("txtoth_adv_cut").value;
  //  alert("HI");
	ajax_server_check();
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
			//alert(xmlHttp.responseText);
			fnchange_month();
		}
	}
	var url_1;
	url_1 = "inventory_ajax_function.php?change_oth_adv_cut=1&oth_adv_cut="+oth_adv_cut;
	xmlHttp.open("POST",url_1,true);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  
	xmlHttp.send(passData);
}
function fnchange_month() {
	var passData = "";
	var month = document.getElementById("txtmonth").value;
	var year = document.getElementById("txtyear").value;
	var emp_id = document.getElementById("txtemp_id").value;
	var sal_adv_cut = document.getElementById("txtsal_adv_cut").value;
	var oth_adv_cut = document.getElementById("txtoth_adv_cut").value;
	
	ajax_server_check();
	xmlHttp.onreadystatechange=function(){
		if(xmlHttp.readyState==4 || xmlHttp.readyState=="complete"){
				var xmlDoc			= xmlHttp.responseXML;
				var days_present	= xmlDoc.getElementsByTagName("total_days_present")[0].childNodes[0].nodeValue;
				var total_sal_adv			= xmlDoc.getElementsByTagName("total_sal_adv")[0].childNodes[0].nodeValue;
				var total_oth_adv			= xmlDoc.getElementsByTagName("total_oth_adv")[0].childNodes[0].nodeValue;
				var total_pay_for_month	= xmlDoc.getElementsByTagName("total_pay_for_month")[0].childNodes[0].nodeValue;
				var actual_pay		= xmlDoc.getElementsByTagName("actual_amount_to_pay")[0].childNodes[0].nodeValue;
				var sal_adv_cut		= xmlDoc.getElementsByTagName("sal_adv_cut")[0].childNodes[0].nodeValue;
				var oth_adv_cut		= xmlDoc.getElementsByTagName("oth_adv_cut")[0].childNodes[0].nodeValue;

				document.getElementById("txtdays_present").value  = days_present;
				document.getElementById("txtsalary_adv").value  = total_sal_adv;
				document.getElementById("txtother_adv").value  = total_oth_adv;
				document.getElementById("txtsalary_of_month").value  = total_pay_for_month;
				document.getElementById("txtactual_pay").value  = actual_pay;
				document.getElementById("txtsal_adv_cut").value  = sal_adv_cut;
				document.getElementById("txtoth_adv_cut").value  = oth_adv_cut;
		}
	}
	var url_1;
	url_1 = "inventory_ajax_function.php?change_month=1&month="+month+"&year="+year+"&emp_id="+emp_id+"&sal_adv_cut="+sal_adv_cut+"&oth_adv_cut="+oth_adv_cut;
	xmlHttp.open("POST",url_1,true);
	xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");  
	xmlHttp.send(passData);
}
