//Chrome Drop Down Menu v2.01- Author: Dynamic Drive (http://www.dynamicdrive.com)
//Last updated: November 14th 06- added iframe shim technique

var calender={
	disappeardelay: 250, //set delay in miliseconds before menu disappears onmouseout
	disablemenuclick: true, //when user clicks on a menu item with a drop down menu, disable menu item's link?
	enableswipe: 1, //enable swipe effect? 1 for yes, 0 for no
	enableiframeshim: 1, //enable "iframe shim" technique to get drop down menus to correctly appear on top of controls such as form objects in IE5.5/IE6? 1 for yes, 0 for no

	//No need to edit beyond here////////////////////////
	dropmenuobj: null, ie: document.all, firefox: document.getElementById&&!document.all, swipetimer: undefined, bottomclip:0,
	getposOffset:function(what, offsettype){
		var totaloffset=(offsettype=="left")? what.offsetLeft : what.offsetTop;
		var parentEl=what.offsetParent;
		while (parentEl!=null){
			if(this.ie){
				totaloffset = (offsettype=="left") ? totaloffset + parentEl.offsetLeft  : totaloffset + parentEl.offsetTop;
			}else{
				totaloffset = (offsettype=="left") ? totaloffset + parentEl.offsetLeft  : totaloffset + parentEl.offsetTop;
			}
			parentEl=parentEl.offsetParent;
		}
		if(offsettype=="left"){
			totaloffset = totaloffset - 150;
		}
		return totaloffset;
	},
	iecompattest:function(){
		return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body	
	},

	clearbrowseredge:function(obj, whichedge){
		var edgeoffset = 0
		if (whichedge == "rightedge"){
			var windowedge	=	this.ie && !window.opera ? this.iecompattest().scrollLeft+this.iecompattest().clientWidth-15 : window.pageXOffset+window.innerWidth-15
			this.dropmenuobj.contentmeasure=this.dropmenuobj.offsetWidth
			if (windowedge - this.dropmenuobj.x < this.dropmenuobj.contentmeasure)  //move menu to the left?
				edgeoffset	= this.dropmenuobj.contentmeasure - obj.offsetWidth
		}else{
			var topedge 	= this.ie && !window.opera? this.iecompattest().scrollTop : window.pageYOffset
			var windowedge 	= this.ie && !window.opera? this.iecompattest().scrollTop+this.iecompattest().clientHeight-15 : window.pageYOffset+window.innerHeight-18
			this.dropmenuobj.contentmeasure = this.dropmenuobj.offsetHeight
			if (windowedge - this.dropmenuobj.y < this.dropmenuobj.contentmeasure){ //move up?
				edgeoffset = this.dropmenuobj.contentmeasure+obj.offsetHeight
				if ((this.dropmenuobj.y-topedge) < this.dropmenuobj.contentmeasure) //up no good either?
					edgeoffset = this.dropmenuobj.y + obj.offsetHeight - topedge
			}
		}
		return edgeoffset
	},
	dropit:function(dropmenuID){
		this.dropmenuobj		= document.getElementById(dropmenuID)
		this.dropmenuobj.x		= this.getposOffset(this.dropmenuobj, "left")
		this.dropmenuobj.y		= this.getposOffset(this.dropmenuobj, "top")
		this.dropmenuobj.left 	= this.dropmenuobj.x  - this.clearbrowseredge(this, "rightedge") 
		if(this.dropmenuobj.left < 0) this.dropmenuobj.left = 3
		this.dropmenuobj.left   = this.dropmenuobj.left + "px"
		cHeight	= document.body.clientHeight;
		this.dropmenuobj.top 	= this.dropmenuobj.y  - this.clearbrowseredge(this, "bottomedge") +  this.dropmenuobj.offsetHeight 
		req_height = parseInt(this.dropmenuobj.top) + 160;
		if(req_height > cHeight) 
			req_height = parseInt(this.dropmenuobj.top) - 160;
		else
			req_height = this.dropmenuobj.top;
		   
		this.dropmenuobj.top 	= req_height + "px"
		return [this.dropmenuobj.left , this.dropmenuobj.top];
	}
}