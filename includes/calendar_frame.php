<?
$field= $_GET["field"];
$form= $_GET["form"];
$dateType = $_GET["dateType"];
$functionname = $_GET["funcname"];
$functarg1 = $_GET["functarg1"];
$functarg2 = $_GET["functarg2"];
$cal_argument = $_GET["cal_arg"];

$dateField = "window.parent.document.".$form.".".$field;

?>
<html>
<head>
<title>Calendar Control</title>
<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=utf-8" />
<script TYPE="text/javascript" LANGUAGE="javascript">
/* $id: tbl_change.js,v 1.1 2005/11/23 19:10:30 nijel Exp $ */


/**
 * Modify from controls when the "NULL" checkbox is selected
 *
 * @param   string   the MySQL field type
 * @param   string   the urlencoded field name
 * @param   string   the md5 hashed field name
 * @return  boolean  always true
 */
function nullify(theType, urlField, md5Field, multi_edit)
{
    var rowForm = document.forms['insertForm'];

    if (typeof(rowForm.elements['funcs' + multi_edit + '[' + urlField + ']']) != 'undefined') {
        rowForm.elements['funcs' + multi_edit + '[' + urlField + ']'].selectedIndex = -1;
    }

    // "SET" field , "ENUM" field with more than 20 characters
    // or foreign key field
    if (theType == 1 || theType == 3 || theType == 4) {
        rowForm.elements['field_' + md5Field + multi_edit + '[]'].selectedIndex = -1;
    }
    // Other "ENUM" field
    else if (theType == 2) {
        var elts     = rowForm.elements['field_' + md5Field + multi_edit + '[]'];
        // when there is just one option in ENUM:
        if (elts.checked) {
            elts.checked = false;
        } else {
            var elts_cnt = elts.length;
            for (var i = 0; i < elts_cnt; i++ ) {
                elts[i].checked = false;
            } // end for

        } // end if
    }
    // Other field types
    else /*if (theType == 5)*/ {
        rowForm.elements['fields' + multi_edit + '[' + urlField + ']'].value = '';
    } // end if... else if... else

    return true;
} // end of the 'nullify()' function


/**
 * Unchecks the "NULL" control when a function has been selected or a value
 * entered
 *
 * @param   string   the urlencoded field name
 *
 * @return  boolean  always true
 */
function unNullify(urlField, multi_edit)
{
    var rowForm = document.forms['insertForm'];

    if (typeof(rowForm.elements['fields_null[multi_edit][' + multi_edit + '][' + urlField + ']']) != 'undefined') {
        rowForm.elements['fields_null[multi_edit][' + multi_edit + '][' + urlField + ']'].checked = false
    } // end if

    if (typeof(rowForm.elements['insert_ignore_' + multi_edit]) != 'undefined') {
        rowForm.elements['insert_ignore_' + multi_edit].checked = false
    } // end if

    return true;
} // end of the 'unNullify()' function

var day;
var month;
var year;
var hour;
var minute;
var second;
var clock_set = 0;


/**
 * Formats number to two digits.
 *
 * @param   int number to format.
 * @param   string type of number
 */
function formatNum2(i, valtype) {
    f = (i < 10 ? '0' : '') + i;
    if (valtype && valtype != '') {
        switch(valtype) {
            case 'month':
                f = (f > 12 ? 12 : f);
                break;

            case 'day':
                f = (f > 31 ? 31 : f);
                break;

            case 'hour':
                f = (f > 24 ? 24 : f);
                break;

            default:
            case 'second':
            case 'minute':
                f = (f > 59 ? 59 : f);
                break;
        }
    }

    return f;
}

/**
 * Formats number to two digits.
 *
 * @param   int number to format.
 * @param   int default value
 * @param   string type of number
 */
function formatNum2d(i, default_v, valtype) {
    i = parseInt(i, 10);
    if (isNaN(i)) return default_v;
    return formatNum2(i, valtype)
}

/**
 * Formats number to four digits.
 *
 * @param   int number to format.
 */
function formatNum4(i) {
    i = parseInt(i, 10)
    return (i < 1000 ? i < 100 ? i < 10 ? '000' : '00' : '0' : '') + i;
}

/**
 * Initializes calendar 
 */
function initCalendar() {
  var dateField = <?=$dateField?>;
  var dateType = '<?=$dateType?>';
    if (!year && !month && !day) {	
        /* Called for first time */
        if (dateField.value) {
	       value = dateField.value;
            if (dateType == 'datetime' || dateType == 'date') {
                if (dateType == 'datetime') {
                    parts   = value.split(' ');
                    value   = parts[0];
                    if (parts[1]) {
                        time    = parts[1].split(':');
                        hour    = parseInt(time[0],10);
                        minute  = parseInt(time[1],10);
                        second  = parseInt(time[2],10);
                    }
                }
                date        = value.split("-");
                day         = parseInt(date[2],10);
                month       = parseInt(date[1],10) - 1;
                year        = parseInt(date[0],10);
            } else if (dateType == 'date1'){
				  
					date        = value.split("-");
					day         = parseInt(date[1],10);
					month       = parseInt(date[2],10) - 1;
					year        = parseInt(date[0],10);
			} else {
                year        = parseInt(value.substr(0,4),10);
                month       = parseInt(value.substr(4,2),10) - 1;
                day         = parseInt(value.substr(6,2),10);
                hour        = parseInt(value.substr(8,2),10);
                minute      = parseInt(value.substr(10,2),10);
                second      = parseInt(value.substr(12,2),10);
            }
        }
        if (isNaN(year) || isNaN(month) || isNaN(day) || day == 0) { dt = new Date(); year = dt.getFullYear(); month = dt.getMonth(); day = dt.getDate(); }
        if (isNaN(hour) || isNaN(minute) || isNaN(second)) { dt = new Date(); hour = dt.getHours();  minute  = dt.getMinutes(); second = dt.getSeconds();}
    } else {
        /* Moving in calendar */
        if (month > 11) { month = 0;  year++; }
        if (month < 0) {  month = 11; year--; }
    }

    if (document.getElementById) {  cnt = document.getElementById("calendar_data");
    } else if (document.all) {  cnt = document.all["calendar_data"]; }
    cnt.innerHTML = "";
    str = ""
    //heading table
    str += '<table height="25" valign="top" class="calendar" border=0 CELLPADDING="0" CELLSPACING="0" STYLE="border-collapse:collapse" width="100%" ><tr style="background-color: #EDF2FE"><td align="Center">';
    str += '<a href="javascript:month--; initCalendar();" STYLE="text-decoration:none"><strong>&laquo;</strong></a> ';
    str += '<select class="txtBox" id="select_month" name="monthsel" onchange="month = parseInt(document.getElementById(\'select_month\').value); initCalendar();">';
    for (i =0; i < 12; i++) {
        if (i == month) selected = ' selected="selected"';    else selected = '';
        str += '<option value="' + i + '" ' + selected + '>' + month_names[i] + '</option>';
    }
    str += '</select>';
    str += ' <a href="javascript:month++; initCalendar();" STYLE="text-decoration:none"><strong>&raquo;</strong></a>';
    str += '</td><td width="50%" align="Center">';
    str += '<a href="javascript:year--; initCalendar();" STYLE="text-decoration:none"><strong>&laquo;</strong></a> ';
    str += '<select class="txtBox" id="select_year" name="yearsel" onchange="year = parseInt(document.getElementById(\'select_year\').value); initCalendar();">';
    for (i = year - 100; i < year + 100; i++) {
        if (i == year) selected = ' selected="selected"';  else selected = '';
        str += '<option value="' + i + '" ' + selected + '>' + i + '</option>';
    }
    str += '</select>';
    str += ' <a href="javascript:year++; initCalendar();" STYLE="text-decoration:none"><strong>&raquo;</strong></a>';
    str += '</td></tr></table>';
    str += '<table class="calendar" border=1 BORDERCOLOR="#FFFFFF" STYLE="border-collapse:collapse"><tr class="NormalBR" style="background-color: #A5B9E2">';
    for (i = 0; i < 7; i++) { str += "<th width='14%'>" + day_names[i] + "</th>";  }
    str += "</tr>";
    var firstDay = new Date(year, month, 1).getDay();
    var lastDay = new Date(year, month + 1, 0).getDate();
    str += "<tr class='NormalBR'>";
    dayInWeek = 0;
    for (i = 0; i < firstDay; i++) { str += "<td style=\"background-color: #EDF2FE\">&nbsp;</td>";     dayInWeek++; }
    for (i = 1; i <= lastDay; i++) {
        if (dayInWeek == 7) {  str += "</tr><tr class='NormalBR'>";   dayInWeek = 0; }
        dispmonth = 1 + month;
        if (dateType == 'datetime' || dateType == 'date') {  actVal = "" + formatNum2(dispmonth, 'month') + "/" + formatNum2(i, 'day') + "/" + formatNum4(year) ;
        }else if ( dateType == 'date1') {	actVal = "" + formatNum2(i, 'day') + "/" + formatNum2(dispmonth, 'month') + "/" + formatNum4(year) ;
		} else {  actVal = "" + formatNum4(year) + formatNum2(dispmonth, 'month') + formatNum2(i, 'day');  }
        if (i == day) { style = ' class="selected"';  current_date = actVal;
        } else {   style = ''; }
        str += "<td" + style + " align='center' style=\"cursor:pointer; background-color: #D1DBF1\" onclick=\"javascript:returnDate('" + actVal + "','"+dateField+"');\"><a href=\"javascript:returnDate('" + actVal + "','"+dateField+"');\" STYLE=\"text-decoration:none\">" + i + "</a></td>"
        dayInWeek++;
    }
    for (i = dayInWeek; i < 7; i++) { str += "<td style=\"background-color: #EDF2FE\">&nbsp;</td>";   }
    str += "</tr></table>";
    cnt.innerHTML = str;
    // Should we handle time also?
    if (dateType != 'date' && dateType != 'date1' && !clock_set) {
        if (document.getElementById) {       cnt = document.getElementById("clock_data");
        } else if (document.all) {           cnt = document.all["clock_data"];      }
        str = '';
        init_hour = hour;
        init_minute = minute;
        init_second = second;
        str += '<form method="NONE" class="clock" onsubmit="returnDate(\'' + current_date + '\')">';
        str += '<input id="hour"    type="text" size="2" maxlength="2" onblur="this.value=formatNum2d(this.value, init_hour, \'hour\'); init_hour = this.value;" value="' + formatNum2(hour, 'hour') + '" />:';
        str += '<input id="minute"  type="text" size="2" maxlength="2" onblur="this.value=formatNum2d(this.value, init_minute, \'minute\'); init_minute = this.value;" value="' + formatNum2(minute, 'minute') + '" />:';
        str += '<input id="second"  type="text" size="2" maxlength="2" onblur="this.value=formatNum2d(this.value, init_second, \'second\'); init_second = this.value;" value="' + formatNum2(second, 'second') + '" />';
        str += '<br />';
        str += '<input type="submit" value="' + submit_text + '"/>';
        str += '</form>';
        cnt.innerHTML = str;
        clock_set = 1;
    }
}

/** Returns date from calendar.  @param   string     date text */
function returnDate(d,dateField) {
	txt = d;
	//alert(txt);	
	window.parent.calender_field_name.value = txt;
	 if(window.parent.calender_field_name1){ window.parent.calender_field_name1.value = txt; }
	 eval("window.parent.closemypage()"); 
	<? if ($functionname<>""){ ?>
		eval("window.parent.<?=$functionname?>(txt)");	
	<? }?>
}
</script>
<script TYPE="text/javascript" LANGUAGE="javascript">
var month_names = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
var day_names = new Array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");
var submit_text = "Go";
</script>
<style>
.NormalBR {
	color: #000000;
	background-color: #FFFFFF;
	font-weight: normal;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8pt;
}.txtBox{
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 8.5pt;
	border: 1px solid;
	border-color: #666666;	
}
</style>
</head>
<body onLoad="initCalendar();" LEFTMARGIN="1" RIGHTMARGIN="1" TOPMARGIN="1" BOTTOMMARGIN="1"><div ID="calendar_data" ></div><div ID="clock_data"></div>
</body>
</html>