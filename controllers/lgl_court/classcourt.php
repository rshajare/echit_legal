<?
include_once ("classes/classdatabase.php");

class court extends Database
{
    private $TableName;
    private $TableID;
    public $TableIDVal;

 
    private $SessionLanguage;
    private $TableFieldArray;
    public $TableFieldValArray;


    public function __construct()
    {
        global $t;
        $this->TableName = 'tbl_lgl_court';
        $this->TableID = 'court_id';
        $this->TableFieldArray = array(
            array('court_name', 1, 1, PDO::PARAM_STR),
            array('court_address', 1, 1, PDO::PARAM_STR),
			array('main_branch_id', 1, 1, PDO::PARAM_INT)
        );
        $this->SessionLanguage = $_SESSION['opt_lang'];
        $this->DBConnect();


        $branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch order by `branch_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);

    }


    public function Addcourt()
    {
        global $t;
        $t->display('master_lookups/lgl_court/court_add.htm');
    }

    public function Editcourt()
    {
        global $t;
        $main = array();
        $court_id = $_GET['court_id'];
        $sql = "SELECT * FROM tbl_lgl_court where court_id=$court_id";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $data[] = $row;
        }
        $t->assign('data', $data);

        $t->display('master_lookups/lgl_court/court_edit.htm');

    }

    public function destroy($court_id)
    {
        $del_id_val = $court_id;
        $this->DeleteRecord($this->TableName, $this->TableID, $del_id_val);
    }


    public function store()
    {
        $court_id = $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
        header("Location: lgl_court.php");
    }

    public function update()
    {
        $court_id = $this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
        header("Location: lgl_court.php");
    }

    public function index()
    {

        global $t;
		$sql= "	SELECT * from tbl_lgl_court
				LEFT JOIN tbl_lgl_branch on tbl_lgl_branch.branch_id = tbl_lgl_court.main_branch_id";

		
        $tmpblnWhere = 0;
        		if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort']; else $reqsort = 'court_id';
				if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
				if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";	 
				//---------------------------------Paging--------------------------------------------------
				if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
				if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
				$t->assign('reqord',$reqord);
				$t->assign('perpage',$perpage);
				
				//-------------------------------end  Paging------------------------------------------------------


        $tmpblnWhere = 1;
        			if ( isset( $_REQUEST['txtkeyword'] ) ){
                    $reqkeyword = $_REQUEST['txtkeyword'] ;
                    $t->assign ( 'reqkeyword', $reqkeyword);
                    if (strcmp($reqkeyword,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_court.court_name like '%$reqkeyword%' ";
                        $PageString = $PageString  .'&txtkeyword='.$reqkeyword;
                    }
                }
        			if ( isset( $_REQUEST['txtkeyword3'] ) ){
                    $reqkeyword3 = $_REQUEST['txtkeyword3'] ;
                    $t->assign ( 'reqkeyword3', $reqkeyword3);
                    if (strcmp($reqkeyword3,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_branch.branch_id like '%$reqkeyword3%' ";
                        $PageString = $PageString  .'&txtkeyword3='.$reqkeyword3;
                    }
                }
        if (strcmp($reqsort, "") <> 0) $sql = $sql . ' order by ' . $reqsort;
        $sql = $sql . ' ' . $reqord;


// ---------------- call paging function---------------------------------------------------
$strfnPagingSql = $sql; include 'includes/callpaging.php'; 
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        $cnt = 1;

     // echo $sql;
        foreach ($temp as $index => $row) {
            $row["cnt"] = $cnt++;
			$row["branch_det"]	= fngetValue("tbl_lgl_branch","branch_name","branch_id='".$row["main_branch_id"]."'");
			
            $courtResult[] = $row;
        }
        $t->assign('court', $courtResult);
        $t->display('master_lookups/lgl_court/court_list.htm');

    }

}

?>
