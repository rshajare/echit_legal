<?
include_once ("classes/classdatabase.php");

class company extends Database
{
    private $TableName;
    private $TableID;
    public $TableIDVal;


    private $SessionLanguage;
    private $TableFieldArray;
    public $TableFieldValArray;


    public function __construct()
    {
        global $t;
        $this->TableName = 'tbl_lgl_company';
        $this->TableID = 'company_id';
        $this->TableFieldArray = array(
            array('company_name', 1, 1, PDO::PARAM_STR),
            array('company_address', 1, 1, PDO::PARAM_STR),
            array('contact_number', 1, 1, PDO::PARAM_INT)
        );
        $this->SessionLanguage = $_SESSION['opt_lang'];
        $this->DBConnect();
		
        $comp_arr = array();
        $sql = "SELECT company_name,company_id FROM tbl_lgl_company order by `company_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $comp_arr['company_id'][] = $row['company_id'];
            $comp_arr['company_name'][] = $row['company_name'];
        }
        $t->assign('comp_arr', $comp_arr);
    }


    public function Addcompany()
    {
        global $t;
        $t->display('master_lookups/lgl_company/company_add.htm');
    }

    public function Editcompany()
    {
        global $t;
        $main = array();
        $company_id = $_GET['company_id'];
        $sql = "SELECT * FROM tbl_lgl_company where company_id=$company_id";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $data[] = $row;
        }
        $t->assign('data', $data);

        $t->display('master_lookups/lgl_company/company_edit.htm');

    }

    public function destroy($company_id)
    {
        $del_id_val = $company_id;
        $this->DeleteRecord($this->TableName, $this->TableID, $del_id_val);
    }


    public function store()
    {
        $company_id = $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
        header("Location: lgl_company.php");
    }

    public function update()
    {
        $company_id = $this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
        header("Location: lgl_company.php");
    }

    public function index()
    {

        global $t;
        $sql = "SELECT * from tbl_lgl_company ";
        $tmpblnWhere = 0;
        		if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort']; else $reqsort = 'company_id';
				if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
				if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";	 
				//---------------------------------Paging--------------------------------------------------
				if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
				if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
				$t->assign('reqord',$reqord);
				$t->assign('perpage',$perpage);
				
				//-------------------------------end  Paging------------------------------------------------------


        $tmpblnWhere = 1;
        			if ( isset( $_REQUEST['txtkeyword'] ) ){
                    $reqkeyword = $_REQUEST['txtkeyword'] ;
                    $t->assign ( 'reqkeyword', $reqkeyword);
                    if (strcmp($reqkeyword,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "company_name like '%$reqkeyword%' ";
                        $PageString = $PageString  .'&txtkeyword='.$reqkeyword;
                    }
                }
                if ( isset( $_REQUEST['txtkeyword1'] ) ){
                    $reqkeyword1 = $_REQUEST['txtkeyword1'] ;
                    $t->assign ( 'reqkeyword1', $reqkeyword1);
                    if (strcmp($reqkeyword1,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "company_address like '%$reqkeyword1%' ";
                        $PageString = $PageString  .'&txtkeyword1='.$reqkeyword1;
                    }
                }
        			if ( isset( $_REQUEST['txtkeyword2'] ) ){
                    $reqkeyword2 = $_REQUEST['txtkeyword2'] ;
                    $t->assign ( 'reqkeyword2', $reqkeyword2);
                    if (strcmp($reqkeyword2,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "contact_number like '%$reqkeyword2%' ";
                        $PageString = $PageString  .'&txtkeyword2='.$reqkeyword2;
                    }
                }
				
        
        if (strcmp($reqsort, "") <> 0) $sql = $sql . ' order by ' . $reqsort;
        $sql = $sql . ' ' . $reqord;


// ---------------- call paging function---------------------------------------------------
$strfnPagingSql = $sql; include 'includes/callpaging.php'; 
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        $cnt = 1;

      // echo $sql;
        foreach ($temp as $index => $row) {
            $row["cnt"] = $cnt++;
            $companyResult[] = $row;
        }
        $t->assign('company', $companyResult);
        $t->display('master_lookups/lgl_company/company_list.htm');

    }

}

?>
