<?
include_once ("classes/classdatabase.php");

class agent extends Database
{
    private $TableName;
    private $TableID;
    public $TableIDVal;

 
    private $SessionLanguage;
    private $TableFieldArray;
    public $TableFieldValArray;


    public function __construct()
    {
        global $t;
        $this->TableName = 'tbl_lgl_agent';
        $this->TableID = 'agent_id';
        $this->TableFieldArray = array(
            array('agent_name', 1, 1, PDO::PARAM_STR),
            array('agent_address', 1, 1, PDO::PARAM_STR),
            array('contact_no', 1, 1, PDO::PARAM_INT),
			array('main_branch_id', 1, 1, PDO::PARAM_INT),
			array('company_id', 1, 1, PDO::PARAM_INT)
        );
        $this->SessionLanguage = $_SESSION['opt_lang'];
        $this->DBConnect();

        $comp_arr = array();
        $sql = "SELECT company_name,company_id FROM tbl_lgl_company order by `company_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $comp_arr['company_id'][] = $row['company_id'];
            $comp_arr['company_name'][] = $row['company_name'];
        }
        $t->assign('comp_arr', $comp_arr);


/*        $branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch order by `branch_id` asc";
		//echo $sql;
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);
*///print_r($branch_arr);
    }


    public function Addagent()
    {
        global $t;
        $t->display('master_lookups/lgl_agent/agent_add.htm');
    }

    public function Editagent()
    {
        global $t;
        $main = array();
        $agent_id = $_GET['agent_id'];
        $sql = "SELECT * FROM tbl_lgl_agent where agent_id=$agent_id";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $data[] = $row;
			$this->all_lk($row["company_id"],$row["main_branch_id"]);

        }
        $t->assign('data', $data);

        $t->display('master_lookups/lgl_agent/agent_edit.htm');

    }
		public function all_lk($req_company_id,$req_branch_id){
		global $t;

		$company_id=$req_company_id;
		$branch_id=$req_branch_id;
		
		
		$branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$company_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);


  }


    public function destroy($agent_id)
    {
        $del_id_val = $agent_id;
        $this->DeleteRecord($this->TableName, $this->TableID, $del_id_val);
    }


    public function store()
    {
        $agent_id = $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
        header("Location: lgl_agent.php");
    }

    public function update()
    {
        $agent_id = $this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
        header("Location: lgl_agent.php");
    }

    public function index()
    {

        global $t;
		$sql= "	SELECT * from tbl_lgl_agent
				LEFT JOIN tbl_lgl_branch on tbl_lgl_agent.main_branch_id = tbl_lgl_branch.branch_id
				LEFT JOIN tbl_lgl_company on tbl_lgl_agent.company_id = tbl_lgl_company.company_id";

		
        //$tmpblnWhere = 0;
        if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort']; else $reqsort = 'agent_id';
				if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
				if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";	 
				//---------------------------------Paging--------------------------------------------------
				if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
				if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
				$t->assign('reqord',$reqord);
				$t->assign('perpage',$perpage);
				
				//-------------------------------end  Paging------------------------------------------------------


        			if ( isset( $_REQUEST['txtkeyword'] ) ){
                    $reqkeyword = $_REQUEST['txtkeyword'] ;
                    $t->assign ( 'reqkeyword', $reqkeyword);
                    if (strcmp($reqkeyword,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_agent.agent_name like '%$reqkeyword%' ";
                        $PageString = $PageString  .'&txtkeyword='.$reqkeyword;
                    	}
                	}
					if ( isset( $_REQUEST['txtkeyword4'] ) ){
						$reqkeyword4 = $_REQUEST['txtkeyword4'] ;
						$t->assign ( 'reqkeyword4', $reqkeyword4);
						if (strcmp($reqkeyword4,"")<>0){
							if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
							$sql = $sql . "tbl_lgl_company.company_id = '$reqkeyword4' ";
							$PageString = $PageString  .'&txtkeyword4='.$reqkeyword4;
						}
					}
				
					if ( isset( $_REQUEST['txtkeyword5'] ) ){
						$reqkeyword5 = $_REQUEST['txtkeyword5'] ;
						$t->assign ( 'reqkeyword5', $reqkeyword5);
						if (strcmp($reqkeyword5,"")<>0){
							if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
							$sql = $sql . "tbl_lgl_branch.branch_id = '$reqkeyword5' ";
							$PageString = $PageString  .'&txtkeyword5='.$reqkeyword5;
						}
					}
        
        if (strcmp($reqsort, "") <> 0) $sql = $sql . ' order by ' . $reqsort;
        $sql = $sql . ' ' . $reqord;


// ---------------- call paging function---------------------------------------------------
$strfnPagingSql = $sql; include 'includes/callpaging.php'; 
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        $cnt = 1;

      //echo $sql;
        foreach ($temp as $index => $row) {
            $row["cnt"] = $cnt++;
			$row["branch_det"]	= fngetValue("tbl_lgl_branch","branch_name","branch_id='".$row["main_branch_id"]."'");
			
            $agentResult[] = $row;
        }
		
        $branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$reqkeyword4' order by `branch_name` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);
		
        $t->assign('agent', $agentResult);
        $t->display('master_lookups/lgl_agent/agent_list.htm');

    }

}

?>
