<?
include_once ("classes/classdatabase.php");

class complainant extends Database
{
    private $TableName;
    private $TableID;
    public $TableIDVal;

 
    private $SessionLanguage;
    private $TableFieldArray;
    public $TableFieldValArray;


    public function __construct()
    {
        global $t;
        $this->TableName = 'tbl_lgl_complainant';
        $this->TableID = 'complainant_id';
        $this->TableFieldArray = array(
            array('complainant_name', 1, 1, PDO::PARAM_STR),
            array('complainant_dob', 1, 1, PDO::PARAM_STR),
            array('designation', 1, 1, PDO::PARAM_STR),
		    array('son_of', 1, 1, PDO::PARAM_STR),
            array('residing_at', 1, 1, PDO::PARAM_STR),
			array('main_branch_id', 1, 1, PDO::PARAM_INT),
			array('occupation', 1, 1, PDO::PARAM_STR),
			array('off_address', 1, 1, PDO::PARAM_STR),
            array('age', 1, 1, PDO::PARAM_INT),
            array('pan_card_no', 1, 1, PDO::PARAM_STR),
            array('joining_on', 1, 1, PDO::PARAM_STR),
            array('emp_id_no', 1, 1, PDO::PARAM_STR),
			array('board_resolution_date', 1, 1, PDO::PARAM_STR)
			
        );
        $this->SessionLanguage = $_SESSION['opt_lang'];
        $this->DBConnect();


        $branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch order by `branch_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);

    }


    public function Addcomplainant()
    {
        global $t;
        $t->display('master_lookups/lgl_complainant/complainant_add.htm');
    }

    public function Editcomplainant()
    {
        global $t;
        $main = array();
        $complainant_id = $_GET['complainant_id'];
        $sql = "SELECT * FROM tbl_lgl_complainant where complainant_id=$complainant_id";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $data[] = $row;
        }
        $t->assign('data', $data);

        $t->display('master_lookups/lgl_complainant/complainant_edit.htm');

    }

    public function destroy($complainant_id)
    {
        $del_id_val = $complainant_id;
        $this->DeleteRecord($this->TableName, $this->TableID, $del_id_val);
    }


    public function store()
    {
        $complainant_id = $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
        header("Location: lgl_complainant.php");
    }

    public function update()
    {
        $complainant_id = $this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
        header("Location: lgl_complainant.php");
    }

    public function index()
    {

        global $t;
		$sql= "	SELECT * from tbl_lgl_complainant
		 LEFT JOIN tbl_lgl_branch on tbl_lgl_branch.branch_id = tbl_lgl_complainant.main_branch_id";
		
        $tmpblnWhere = 0;
       			if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort']; else $reqsort = 'complainant_id';
				if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
				if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";	 
				//---------------------------------Paging--------------------------------------------------
				if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
				if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
				$t->assign('reqord',$reqord);
				$t->assign('perpage',$perpage);
				
				//-------------------------------end  Paging------------------------------------------------------
        $tmpblnWhere = 1;
        			if ( isset( $_REQUEST['txtkeyword'] ) ){
                    $reqkeyword = $_REQUEST['txtkeyword'] ;
                    $t->assign ( 'reqkeyword', $reqkeyword);
                    if (strcmp($reqkeyword,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_complainant.complainant_name like '%$reqkeyword%' ";
                        $PageString = $PageString  .'&txtkeyword='.$reqkeyword;
                    }
                }
        			if ( isset( $_REQUEST['txtkeyword2'] ) ){
                    $reqkeyword2 = $_REQUEST['txtkeyword2'] ;
                    $t->assign ( 'reqkeyword2', $reqkeyword2);
                    if (strcmp($reqkeyword2,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_complainant.designation like '%$reqkeyword2%' ";
                        $PageString = $PageString  .'&txtkeyword2='.$reqkeyword2;
                    }
                }
        			if ( isset( $_REQUEST['txtkeyword3'] ) ){
                    $reqkeyword3 = $_REQUEST['txtkeyword3'] ;
                    $t->assign ( 'reqkeyword3', $reqkeyword3);
                    if (strcmp($reqkeyword3,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_branch.branch_id like '%$reqkeyword3%' ";
                        $PageString = $PageString  .'&txtkeyword3='.$reqkeyword3;
                    }
                }
        
        if (strcmp($reqsort, "") <> 0) $sql = $sql . ' order by ' . $reqsort;
        $sql = $sql . ' ' . $reqord;


// ---------------- call paging function---------------------------------------------------
$strfnPagingSql = $sql; include 'includes/callpaging.php'; 
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        $cnt = 1;

     //echo $sql;
        foreach ($temp as $index => $row) {
            $row["cnt"] = $cnt++;
			$row["branch_det"]	= fngetValue("tbl_lgl_branch","branch_name","branch_id='".$row["main_branch_id"]."'");
            $complainantResult[] = $row;
        }
        $t->assign('complainant', $complainantResult);
        $t->display('master_lookups/lgl_complainant/complainant_list.htm');

    }

}

?>
