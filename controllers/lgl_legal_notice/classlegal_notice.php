<?
include_once ("classes/classdatabase.php");

class legal_notice extends Database
{
    private $TableName;
    private $TableID;
    public $TableIDVal;

 
    private $SessionLanguage;
    private $TableFieldArray;
    public $TableFieldValArray;


    public function __construct()
    {
        global $t;
        $this->TableName = 'tbl_lgl_legal_notice';
        $this->TableID = 'legal_notice_id';
        $this->TableFieldArray = array(
            array('customer_id', 1, 1, PDO::PARAM_INT),
            array('legal_notice_date', 1, 1, PDO::PARAM_STR),
            array('send_on', 1, 1, PDO::PARAM_STR),
            array('received_on', 1, 1, PDO::PARAM_STR),
			array('ack_received_on', 1, 1, PDO::PARAM_STR),
            array('status_postal_ack', 1, 1, PDO::PARAM_STR),
			array('cause_of_action', 1, 1, PDO::PARAM_STR),
			array('main_branch_id', 1, 1, PDO::PARAM_INT),
			array('company_id', 1, 1, PDO::PARAM_INT),
            array('notice_status', 1, 1, PDO::PARAM_STR),
			array('guarantor_id', 1, 1, PDO::PARAM_INT),
			array('category_id', 1, 1, PDO::PARAM_INT)
        );
        $this->SessionLanguage = $_SESSION['opt_lang'];
        $this->DBConnect();

		
        $comp_arr = array();
        $sql = "SELECT company_name,company_id FROM tbl_lgl_company order by `company_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $comp_arr['company_id'][] = $row['company_id'];
            $comp_arr['company_name'][] = $row['company_name'];
        }
        $t->assign('comp_arr', $comp_arr);
	    
		$category_arr = array();
        $sql = "SELECT category_id,category_name FROM tbl_lgl_customer_report_category order by `category_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $category_arr['category_id'][] = $row['category_id'];
            $category_arr['category_name'][] = $row['category_name'];
        }
        $t->assign('category_arr', $category_arr);

    }


    public function Addlegal_notice(){
        global $t;
		$customer_id = $_GET["customer_id"];	
		$company_id  = $_GET["company_id"];
		$main_branch_id  = $_GET["main_branch_id"];
		$category_id  = $_GET["category_id"];

		$isadded= fngetcount("tbl_lgl_legal_notice","customer_id='$customer_id' and category_id='$category_id'");
		if (($isadded!=0)) {
			$message = "Legal Notice entry for the customer For this Category is already Added.Plz select another customer.";
			echo "<script type='text/javascript'>alert('$message');</script>";
		} 

		if($customer_id != ""){
			$data = array();
			$sql 	= "select guarantor_name,guarantor_id from tbl_lgl_guarantor where customer_id = $customer_id";
			$temp = $this->db_pdo->prepare($sql);
			$temp->execute();
       		$cnt = 1;
        	foreach ($temp as $index => $row) {
            	$row["cnt"] = $cnt++;
				$data[] = $row;
			}
			$t->assign( 'num_of_par', $row["cnt"] );
			
			$t->assign( 'data', $data );
        
            $company_id 		= fnGetValue("tbl_lgl_customer","company_id","customer_id='".$customer_id."'");
			$t->assign('company_id', $company_id);
            $main_branch_id 		= fnGetValue("tbl_lgl_customer","main_branch_id","customer_id='".$customer_id."'");
			$t->assign('main_branch_id', $main_branch_id);
			
			$branch_arr = array();
			$sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$company_id'";
			$temp = $this->db_pdo->prepare($sql);
			$temp->execute();
			foreach ($temp as $index => $row) {
				$branch_arr['branch_id'][] = $row['branch_id'];
				$branch_arr['branch_name'][] = $row['branch_name'];
			}
			$t->assign('branch_arr', $branch_arr);
	
			$customer_arr = array();
			$sql = "SELECT customer_name,customer_id,chit_group_no,ticket_no FROM tbl_lgl_customer where company_id='$company_id' and main_branch_id='$main_branch_id' order by customer_id asc";
			$temp = $this->db_pdo->prepare($sql);
			$temp->execute();
			foreach ($temp as $index => $row) {
				$customer_arr['customer_id'][] = $row['customer_id'];
				$customer_arr['customer_name'][] = $row['customer_name']. ' ( ' . $row['chit_group_no']. ' | ' . $row['ticket_no'] . ' ) ' ;
			}
			$t->assign('customer_arr', $customer_arr);
		}
			
			$t->assign( 'company_id', $company_id );
			$t->assign( 'main_branch_id', $main_branch_id );
			$t->assign( 'customer_id', $customer_id );
			$t->assign( 'category_id', $category_id );
			$t->display('master_lookups/lgl_legal_notice/legal_notice_add.htm');
    }

    public function Editlegal_notice()
    {
        global $t;
        $main = array();
        $legal_notice_id = $_GET['legal_notice_id'];
        $sql = "SELECT * FROM tbl_lgl_legal_notice where legal_notice_id=$legal_notice_id and tbl_lgl_legal_notice.guarantor_id IS NULL";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
			$this->all_lk($row["company_id"],$row["main_branch_id"]);
			$data[] = $row;
		}
		
		$data1 	= array();
		

		$sql1= "SELECT * from tbl_lgl_guarantor left join tbl_lgl_legal_notice on (tbl_lgl_guarantor.guarantor_id = tbl_lgl_legal_notice.guarantor_id) where tbl_lgl_guarantor.customer_id=".$row["customer_id"]." ";
		//echo $sql1;
		$temp1 = $this->db_pdo->prepare($sql1);
		$temp1->execute();
		$cnt = 1;
		
		foreach ($temp1 as $index => $row1) {
			$row1["cnt"] = $cnt++;
			//$row1["guarantor_name"]=fngetvalue("tbl_lgl_guarantor","guarantor_name","guarantor_id=".$row1["guarantor_id"]."");
			$data1[] = $row1;		

		}

		$t->assign( 'num_of_par', $row1["cnt"] );
		$t->assign( 'data1', $data1 );		
			
		
        $t->assign('data', $data);

        $t->display('master_lookups/lgl_legal_notice/legal_notice_edit.htm');

    }
	public function all_lk($req_company_id,$req_branch_id){
		global $t;

		$company_id=$req_company_id;
		$branch_id=$req_branch_id;
		$customer_id=$req_customer_id;
		
		
		$branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$company_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);

        $customer_arr = array();
        $sql = "SELECT customer_name,customer_id,chit_group_no,ticket_no FROM tbl_lgl_customer where main_branch_id='$branch_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $customer_arr['customer_id'][] = $row['customer_id'];
            $customer_arr['customer_name'][] = $row['customer_name']. ' ( ' . $row['chit_group_no']. ' | ' . $row['ticket_no'] . ' ) ' ;
        }
        $t->assign('customer_arr', $customer_arr);

	  	$bank_arr = array();
        $sql = "SELECT BankID,Bankname FROM tbl_lgl_banks where main_branch_id='$branch_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $bank_arr['BankID'][] = $row['BankID'];
            $bank_arr['Bankname'][] = $row['Bankname'];
        }
        $t->assign('bank_arr', $bank_arr);

  }

    public function destroy($legal_notice_id)
    {
        $del_id_val = $legal_notice_id;
        $this->DeleteRecord($this->TableName, $this->TableID, $del_id_val);
    }


    public function store(){
	    $legal_notice_id 		= $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
		$req_total_guarantor 	= $_POST["txt_hid_num_par"];
		$req_legal_notice_id	= $legal_notice_id;
		$req_customer_id		= $_POST["txtcustomer_id"];
		$req_company_id			= $_POST["txtcompany_id"];
		$req_main_branch_id		= $_POST["txtmain_branch_id"];
		$req_cause_of_action	= $_POST["txtcause_of_action"];
		$req_notice_status		= $_POST["txtnotice_status"];

		for($i=1;$i<=$req_total_guarantor;$i++){
			$req_guarantor_id		= $_POST["txtguarantor_id".$i];
			$req_legal_notice_date	= $_POST["txtlegal_notice_date".$i];
			$req_send_on			= $_POST["txtsend_on".$i];
			$req_received_on		= $_POST["txtreceived_on".$i];
			$req_ack_received_on	= $_POST["txtack_received_on".$i];
			$req_status_postal_ack	= $_POST["txtstatus_postal_ack".$i];
			$req_category_id		= $_POST["txtcategory_id".$i];
		  	$this->TableFieldValArray=array(array("$req_customer_id"),array("$req_legal_notice_date"),array("$req_send_on"),array("$req_received_on"),array("$req_ack_received_on"),array("$req_status_postal_ack"),array("$req_cause_of_action"),array("$req_main_branch_id"),array("$req_company_id"),array("$req_notice_status"),array("$req_guarantor_id"),array("$req_category_id"));
			$this->InsertRecordValOnly($this->TableName,$this->TableFieldArray,$this->TableFieldValArray);
		}
        header("Location: lgl_legal_notice.php");
    }

    public function update(){
		$legal_notice_id = $this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
		$req_total_rows 	= $_POST["txt_hid_num_par"];
		for($i=1;$i<=$req_total_rows;$i++){
			$req_legal_notice_id	= $_POST["txtlegal_notice_id".$i];
			$req_guarantor_id		= $_POST["txtguarantor_id".$i];
			$req_legal_notice_date	= $_POST["txtlegal_notice_date".$i];
			$req_send_on			= $_POST["txtsend_on".$i];
			$req_received_on		= $_POST["txtreceived_on".$i];
			$req_ack_received_on	= $_POST["txtack_received_on".$i];
			$req_status_postal_ack	= $_POST["txtstatus_postal_ack".$i];
			$req_category_id		= $_POST["txtcategory_id".$i];
		  	$this->TableFieldValArray=array(array("$req_customer_id"),array("$req_legal_notice_date"),array("$req_send_on"),array("$req_received_on"),array("$req_ack_received_on"),array("$req_status_postal_ack"),array("$req_cause_of_action"),array("$req_main_branch_id"),array("$req_company_id"),array("$req_notice_status"),array("$req_guarantor_id"),array("$req_category_id"));			
			$this->UpdateRecordValOnly($this->TableName,$this->TableFieldArray,$this->TableFieldValArray,$this->TableID,$req_legal_notice_id);
		}
		header("Location: lgl_legal_notice.php");
    }

    public function index(){
        global $t;
		$sql= "	SELECT * from tbl_lgl_legal_notice 
				LEFT JOIN tbl_lgl_customer on tbl_lgl_legal_notice.customer_id = tbl_lgl_customer.customer_id
				LEFT JOIN tbl_lgl_branch on tbl_lgl_legal_notice.main_branch_id = tbl_lgl_branch.branch_id
				LEFT JOIN tbl_lgl_company on tbl_lgl_legal_notice.company_id = tbl_lgl_company.company_id 
				WHERE guarantor_id IS NULL";

        		if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort']; else $reqsort = 'legal_notice_id';
				if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
				if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";	 
				//---------------------------------Paging--------------------------------------------------
				if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
				if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
				$t->assign('reqord',$reqord);
				$t->assign('perpage',$perpage);
				
				//-------------------------------end  Paging------------------------------------------------------
				$tmpblnWhere = 1;

                if ( isset( $_REQUEST['txtkeyword'] ) ){
                    $reqkeyword = $_REQUEST['txtkeyword'] ;
                    $t->assign ( 'reqkeyword', $reqkeyword);
                    if (strcmp($reqkeyword,"")<>0){
                        if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_customer.customer_name like '%$reqkeyword%' ";
                        $PageString = $PageString  .'&txtkeyword='.$reqkeyword;
                    }
                }
                if ( isset( $_REQUEST['txtkeyword4'] ) ){
                    $reqkeyword4 = $_REQUEST['txtkeyword4'] ;
                    $t->assign ( 'reqkeyword4', $reqkeyword4);
                    if (strcmp($reqkeyword4,"")<>0){
                        if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_company.company_id = '$reqkeyword4' ";
                        $PageString = $PageString  .'&txtkeyword4='.$reqkeyword4;
                    }
                }
                if ( isset( $_REQUEST['txtkeyword5'] ) ){
                    $reqkeyword5 = $_REQUEST['txtkeyword5'] ;
                    $t->assign ( 'reqkeyword5', $reqkeyword5);
                    if (strcmp($reqkeyword5,"")<>0){
                        if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_branch.branch_id = '$reqkeyword5' ";
                        $PageString = $PageString  .'&txtkeyword5='.$reqkeyword5;
                    }
                }
				
        		if (strcmp($reqsort, "") <> 0) $sql = $sql . ' order by ' . $reqsort;
        		$sql = $sql . ' ' . $reqord;


// ---------------- call paging function---------------------------------------------------
		$strfnPagingSql = $sql; include 'includes/callpaging.php'; 
        echo $sql;
		$temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        $cnt = 1;

        
        foreach ($temp as $index => $row) {
            $row["cnt"] = $cnt++;
			$row["category_name"]=fngetvalue("tbl_lgl_customer_report_category","category_name","category_id='".$row["category_id"]."'");
			$sql_guarantor= "SELECT * from tbl_lgl_legal_notice where guarantor_id IS NOT NULL  and customer_id='".$row["customer_id"]."' ";
			$temp = $this->db_pdo->prepare($sql_guarantor);
        	$temp->execute();
			$guarantorResult=array();
			foreach ($temp as $index => $row_guarantor) {
				
				$row_guarantor["guarantor_name"]=fngetvalue("tbl_lgl_guarantor","guarantor_name","guarantor_id=".$row_guarantor["guarantor_id"]."");
				$guarantorResult[] = $row_guarantor;
			}
			
			$row['guarantor_details'] = $guarantorResult;
			$legal_noticeResult[] = $row;

        }
        $branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$reqkeyword4' order by `branch_name` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);

		$custmor_arr = array();
        $sql = "SELECT customer_name,customer_id FROM tbl_lgl_customer where main_branch_id='$reqkeyword5' and company_id='$reqkeyword4' order by `customer_name`   asc";
//        echo $sql;
		$temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $custmor_arr['customer_id'][] = $row['customer_id'];
            $custmor_arr['customer_name'][] = $row['customer_name'];
        }
        $t->assign('custmor_arr', $custmor_arr);
		
        $t->assign('legal_notice', $legal_noticeResult);
        $t->display('master_lookups/lgl_legal_notice/legal_notice_list.htm');

    }

}

?>
