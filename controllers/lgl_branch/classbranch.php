<?
include_once ("classes/classdatabase.php");

class branch extends Database
{
    private $TableName;
    private $TableID;
    public $TableIDVal;


    private $SessionLanguage;
    private $TableFieldArray;
    public $TableFieldValArray;


    public function __construct()
    {
        global $t;
        $this->TableName = 'tbl_lgl_branch';
        $this->TableID = 'branch_id';
        $this->TableFieldArray = array(
            array('branch_name', 1, 1, PDO::PARAM_STR),
            array('branch_address', 1, 1, PDO::PARAM_STR),
            array('chit_registrar_name', 1, 1, PDO::PARAM_STR),
		    array('branch_mgr_name', 1, 1, PDO::PARAM_STR),
            array('branch_contact_no', 1, 1, PDO::PARAM_INT),
            array('company_id', 1, 1, PDO::PARAM_INT),
        );
        $this->SessionLanguage = $_SESSION['opt_lang'];
        $this->DBConnect();


        $comp_arr = array();
        $sql = "SELECT company_name,company_id FROM tbl_lgl_company order by `company_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $comp_arr['company_id'][] = $row['company_id'];
            $comp_arr['company_name'][] = $row['company_name'];
        }
        $t->assign('comp_arr', $comp_arr);

    }


    public function Addbranch()
    {
        global $t;
        $t->display('master_lookups/lgl_branch/branch_add.htm');
    }

    public function Editbranch()
    {
        global $t;
        $main = array();
        $branch_id = $_GET['branch_id'];
        $sql = "SELECT * FROM tbl_lgl_branch where branch_id=$branch_id";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $data[] = $row;
        }
        $t->assign('data', $data);

        $t->display('master_lookups/lgl_branch/branch_edit.htm');

    }

    public function destroy($branch_id)
    {
        $del_id_val = $branch_id;
        $this->DeleteRecord($this->TableName, $this->TableID, $del_id_val);
    }


    public function store()
    {
        $branch_id = $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
        header("Location: lgl_branch.php");
    }

    public function update()
    {
        $branch_id = $this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
        header("Location: lgl_branch.php");
    }

    public function index()
    {

        global $t;
		$sql= "	SELECT * from tbl_lgl_branch 
				LEFT JOIN tbl_lgl_company on tbl_lgl_branch.company_id = tbl_lgl_company.company_id ";
		
        $tmpblnWhere = 0;
        		if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort']; else $reqsort = 'branch_id';
				if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
				if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";	 
				//---------------------------------Paging--------------------------------------------------
				if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
				if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
				$t->assign('reqord',$reqord);
				$t->assign('perpage',$perpage);
				
				//-------------------------------end  Paging------------------------------------------------------


        $tmpblnWhere = 1;
        			if ( isset( $_REQUEST['txtkeyword'] ) ){
                    $reqkeyword = $_REQUEST['txtkeyword'] ;
                    $t->assign ( 'reqkeyword', $reqkeyword);
                    if (strcmp($reqkeyword,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_branch.branch_name like '%$reqkeyword%' ";
                        $PageString = $PageString  .'&txtkeyword='.$reqkeyword;
                    }
                }
                if ( isset( $_REQUEST['txtkeyword1'] ) ){
                    $reqkeyword1 = $_REQUEST['txtkeyword1'] ;
                    $t->assign ( 'reqkeyword1', $reqkeyword1);
                    if (strcmp($reqkeyword1,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_branch.branch_address like '%$reqkeyword1%' ";
                        $PageString = $PageString  .'&txtkeyword1='.$reqkeyword1;
                    }
                }
        			if ( isset( $_REQUEST['txtkeyword2'] ) ){
                    $reqkeyword2 = $_REQUEST['txtkeyword2'] ;
                    $t->assign ( 'reqkeyword2', $reqkeyword2);
                    if (strcmp($reqkeyword2,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_branch.branch_contact_no like '%$reqkeyword2%' ";
                        $PageString = $PageString  .'&txtkeyword2='.$reqkeyword2;
                    }
                }
        			if ( isset( $_REQUEST['txtkeyword3'] ) ){
                    $reqkeyword3 = $_REQUEST['txtkeyword3'] ;
                    $t->assign ( 'reqkeyword3', $reqkeyword3);
                    if (strcmp($reqkeyword3,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_company.company_id like '%$reqkeyword3%' ";
                        $PageString = $PageString  .'&txtkeyword3='.$reqkeyword3;
                    }
                }
        
        if (strcmp($reqsort, "") <> 0) $sql = $sql . ' order by ' . $reqsort;
        $sql = $sql . ' ' . $reqord;


// ---------------- call paging function---------------------------------------------------
$strfnPagingSql = $sql; include 'includes/callpaging.php'; 
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        $cnt = 1;

     // echo $sql;
        foreach ($temp as $index => $row) {
            $row["cnt"] = $cnt++;
			$row["company_det"]	= fngetValue("tbl_lgl_company","company_name","company_id='".$row["company_id"]."'");
			//echo $company_det;
            $branchResult[] = $row;
        }
        $t->assign('branch', $branchResult);
        $t->display('master_lookups/lgl_branch/branch_list.htm');

    }

}

?>
