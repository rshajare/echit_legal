<?PHP
include_once("classes/classdatabase.php");

class report_draft_lk extends Database {
    private $TableName ;
    private $TableID ;
    public $TableIDVal ;

    private $SessionLanguage ;
    private $TableFieldArray;
    public $TableFieldValArray;
    public $PageTitle;

    public function __construct(){
        global $t;
        $this->TableName 			= 'tbl_report_draft_lk';
        $this->TableID 				= 'report_draft_id';
        $this->TableFieldArray 		= array(
            array('report_name', 1 , 1, PDO::PARAM_STR),
	  );

        $this->PageTitle = 'Report Draft List';

        $this->SessionLanguage 		= $_SESSION['opt_lang'];

        $this->DBConnect();
   }

    public function destroy($report_draft_id){
		$del_id_val	= $report_draft_id;	
		$this->DeleteRecord($this->TableName,$this->TableID,$del_id_val);

    }

    public function store(){
        $report_draft_id = $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
        header("Location: lgl_report_draft_lk.php");
}

    public function update(){
        $report_draft_id = $this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
        header("Location: lgl_report_draft_lk.php");
}

    public function index(){

        global $t;
        $Result = array();
        $EditResult = array();
        // Edit Data

        if(isset($_GET[$this->TableID])){
            $Editsql= "	SELECT * from  " . $this->TableName . ' where '. $this->TableID . ' = '. $_GET[$this->TableID];
            $Edittemp = $this->db_pdo->prepare($Editsql);
            $Edittemp->execute();

            foreach( $Edittemp as $index => $row )
            {
                $EditResult[]=$row;
            }
        }else{
            $EditResult[0]= array('report_draft_id' => '','report_name' => '');
        }
        $t->assign ( 'Editdata', $EditResult);

		$sql="SELECT * from  tbl_report_draft_lk "  ;
		//echo $sql;
		$tmpblnWhere = 1;
		
        			if ( isset( $_REQUEST['txtkeyword1'] ) ){
                    $reqkeyword1 = $_REQUEST['txtkeyword1'] ;
                    $t->assign ( 'reqkeyword1', $reqkeyword1);
                    if (strcmp($reqkeyword1,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_report_draft_lk.report_name like '%$reqkeyword1%' ";
                        $PageString = $PageString  .'&txtkeyword1='.$reqkeyword1;
                    }
                }
		
		
		if (strcmp($reqsort,"")<>0)  $sql = $sql . ' order by ' . $reqsort ;
		$sql = $sql . ' ' . $reqord ;


// ---------------- call paging function---------------------------------------------------
		$strfnPagingSql = $sql; include 'includes/callpaging.php'; 
		$temp = $this->db_pdo->prepare($sql);
		$temp->execute();
		$cnt=1;

		foreach( $temp as $index => $row ) 
		{
			$row["cnt"]	= $cnt++ ;
            $Report_DraftResult[] = $row;
		}
		$t->assign ( 'cnt', $cnt);
		$t->assign ( 'Report_List', $Report_DraftResult);
        $t->display('master_lookups/lgl_report_draft_lk/lgl_report_draft.htm');

    }

}

?>
