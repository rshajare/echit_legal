<?
include_once ("classes/classdatabase.php");

class status_change extends Database
{
    private $TableName;
    private $TableID;
    public $TableIDVal;

 
    private $SessionLanguage;
    private $TableFieldArray;
    public $TableFieldValArray;


    public function __construct()
    {
        global $t;
        $this->TableName = 'tbl_lgl_status_change';
        $this->TableID = 'status_change_id';
        $this->TableFieldArray = array(
			array('customer_id', 1, 1, PDO::PARAM_INT),
			array('status_date', 1, 1, PDO::PARAM_STR),
			array('status', 1, 1, PDO::PARAM_INT),
			array('main_branch_id', 1, 1, PDO::PARAM_INT),
			array('company_id', 1, 1, PDO::PARAM_INT)

        );
        $this->SessionLanguage = $_SESSION['opt_lang'];
        $this->DBConnect();

        $comp_arr = array();
        $sql = "SELECT company_name,company_id FROM tbl_lgl_company order by `company_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $comp_arr['company_id'][] = $row['company_id'];
            $comp_arr['company_name'][] = $row['company_name'];
        }
        $t->assign('comp_arr', $comp_arr);


        $status_arr = array();
        $sql = "SELECT status_name,status_id FROM tbl_lgl_status order by `status_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $status_arr['status_id'][] = $row['status_id'];
            $status_arr['status_name'][] = $row['status_name'];
        }
        $t->assign('status_arr', $status_arr);
    }


    public function Addstatus_change()
    {
        global $t;
		$customer_id = $_GET['customer_id'];	
        $t->assign('customer_id', $customer_id);

		if($customer_id!=""){
            $reqcompany_id 		= fnGetValue("tbl_lgl_customer","company_id","customer_id='".$customer_id."'");
			$t->assign('reqcompany_id', $reqcompany_id);
            $reqmain_branch_id 		= fnGetValue("tbl_lgl_customer","main_branch_id","customer_id='".$customer_id."'");
			$t->assign('reqmain_branch_id', $reqmain_branch_id);
			
		$branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$reqcompany_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);

        $customer_arr = array();
        $sql = "SELECT customer_name,customer_id,chit_group_no,ticket_no FROM tbl_lgl_customer where company_id='$reqcompany_id' and main_branch_id='$reqmain_branch_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $customer_arr['customer_id'][] = $row['customer_id'];
            $customer_arr['customer_name'][] = $row['customer_name']. ' ( ' . $row['chit_group_no']. ' | ' . $row['ticket_no'] . ' ) ' ;
        }
        $t->assign('customer_arr', $customer_arr);

					
		}
		$t->display('master_lookups/lgl_status_change/status_change_add.htm');
    }

    public function Editstatus_change()
    {
        global $t;
        $main = array();
        $status_change_id = $_GET['status_change_id'];
        $sql = "SELECT * FROM tbl_lgl_status_change where status_change_id=$status_change_id";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
			$this->all_lk($row["company_id"],$row["main_branch_id"]);
			$customer_name 		= fnGetValue("tbl_lgl_customer","customer_name","customer_id='".$row["customer_id"]."'");
            $data[] = $row;
        }
        $t->assign('data', $data);
        $t->assign('customer_name', $customer_name);
        $t->display('master_lookups/lgl_status_change/status_change_edit.htm');

    }
	public function all_lk($req_company_id,$req_branch_id){
		global $t;

		$company_id=$req_company_id;
		$branch_id=$req_branch_id;
		$customer_id=$req_customer_id;
		
		
		$branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$company_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);

        $customer_arr = array();
        $sql = "SELECT customer_name,customer_id,chit_group_no,ticket_no FROM tbl_lgl_customer where main_branch_id='$branch_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $customer_arr['customer_id'][] = $row['customer_id'];
            $customer_arr['customer_name'][] = $row['customer_name']. ' ( ' . $row['chit_group_no']. ' | ' . $row['ticket_no'] . ' ) ' ;
        }
        $t->assign('customer_arr', $customer_arr);

	  	$bank_arr = array();
        $sql = "SELECT BankID,Bankname FROM tbl_lgl_banks where main_branch_id='$branch_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $bank_arr['BankID'][] = $row['BankID'];
            $bank_arr['Bankname'][] = $row['Bankname'];
        }
        $t->assign('bank_arr', $bank_arr);

  }
    public function destroy($status_change_id)
    {
        $del_id_val = $status_change_id;
        $this->DeleteRecord($this->TableName, $this->TableID, $del_id_val);
    }


    public function store()
    {
        $status_change_id = $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
        header("Location: lgl_status_change.php");
    }

    public function update()
    {
        $status_change_id = $this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
        header("Location: lgl_status_change.php");
    }

    public function index()
    {

        global $t;
		$sql= "	SELECT * from tbl_lgl_status_change 
				LEFT JOIN tbl_lgl_customer on tbl_lgl_status_change.customer_id = tbl_lgl_customer.customer_id
				LEFT JOIN tbl_lgl_branch on tbl_lgl_status_change.main_branch_id = tbl_lgl_branch.branch_id
				LEFT JOIN tbl_lgl_company on tbl_lgl_status_change.company_id = tbl_lgl_company.company_id";


				//$tmpblnWhere = 0;
        		if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort']; else $reqsort = 'status_change_id';
				if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
				if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";	 
				//---------------------------------Paging--------------------------------------------------
				if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
				if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
				$t->assign('reqord',$reqord);
				$t->assign('perpage',$perpage);
				
				//-------------------------------end  Paging------------------------------------------------------
				//$tmpblnWhere = 1;
        			if ( isset( $_REQUEST['txtkeyword'] ) ){
                    $reqkeyword = $_REQUEST['txtkeyword'] ;
                    $t->assign ( 'reqkeyword', $reqkeyword);
                    if (strcmp($reqkeyword,"")<>0){
                        if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_customer.customer_name like '%$reqkeyword%' ";
                        $PageString = $PageString  .'&txtkeyword='.$reqkeyword;
                    }
                }
        			if ( isset( $_REQUEST['txtkeyword4'] ) ){
                    $reqkeyword4 = $_REQUEST['txtkeyword4'] ;
                    $t->assign ( 'reqkeyword4', $reqkeyword4);
                    if (strcmp($reqkeyword4,"")<>0){
                        if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_company.company_id = '$reqkeyword4' ";
                        $PageString = $PageString  .'&txtkeyword4='.$reqkeyword4;
                    }
                }
        			if ( isset( $_REQUEST['txtkeyword5'] ) ){
                    $reqkeyword5 = $_REQUEST['txtkeyword5'] ;
                    $t->assign ( 'reqkeyword5', $reqkeyword5);
                    if (strcmp($reqkeyword5,"")<>0){
                        if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_branch.branch_id = '$reqkeyword5' ";
                        $PageString = $PageString  .'&txtkeyword5='.$reqkeyword5;
                    }
                }
				
        
        if (strcmp($reqsort, "") <> 0) $sql = $sql . ' order by ' . $reqsort;
        $sql = $sql . ' ' . $reqord;


// ---------------- call paging function---------------------------------------------------
$strfnPagingSql = $sql; include 'includes/callpaging.php'; 
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        $cnt = 1;

        //echo $sql;
        foreach ($temp as $index => $row) {
            $row["cnt"] = $cnt++;
            $row["status_name"] = fnGetValue("tbl_lgl_status","status_name","status_id='".$row["status"]."'");
			$status_changeResult[] = $row;
        }
        $branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$reqkeyword4' order by `branch_name` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);

		$custmor_arr = array();
        $sql = "SELECT customer_name,customer_id FROM tbl_lgl_customer where main_branch_id='$reqkeyword5' and company_id='$reqkeyword4' order by `customer_name`   asc";
//        echo $sql;
		$temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $custmor_arr['customer_id'][] = $row['customer_id'];
            $custmor_arr['customer_name'][] = $row['customer_name'];
        }
        $t->assign('custmor_arr', $custmor_arr);
		
        $t->assign('status_change', $status_changeResult);
        $t->display('master_lookups/lgl_status_change/status_change_list.htm');

    }

}

?>
