<?
include_once ("classes/classdatabase.php");

class status extends Database
{
    private $TableName;
    private $TableID;
    public $TableIDVal;

 
    private $SessionLanguage;
    private $TableFieldArray;
    public $TableFieldValArray;


    public function __construct()
    {
        global $t;
        $this->TableName = 'tbl_lgl_status';
        $this->TableID = 'status_id';
        $this->TableFieldArray = array(
            array('status_name', 1, 1, PDO::PARAM_STR),
			array('main_branch_id', 1, 1, PDO::PARAM_INT)
        );
        $this->SessionLanguage = $_SESSION['opt_lang'];
        $this->DBConnect();


        $branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch order by `branch_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);

    }


    public function Addstatus()
    {
        global $t;
        $t->display('master_lookups/lgl_status/status_add.htm');
    }

    public function Editstatus()
    {
        global $t;
        $main = array();
        $status_id = $_GET['status_id'];
        $sql = "SELECT * FROM tbl_lgl_status where status_id=$status_id";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $data[] = $row;
        }
        $t->assign('data', $data);

        $t->display('master_lookups/lgl_status/status_edit.htm');

    }

    public function destroy($status_id)
    {
        $del_id_val = $status_id;
        $this->DeleteRecord($this->TableName, $this->TableID, $del_id_val);
    }


    public function store()
    {
        $status_id = $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
        header("Location: lgl_status.php");
    }

    public function update()
    {
        $status_id = $this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
        header("Location: lgl_status.php");
    }

    public function index()
    {

        global $t;
		$sql= "	SELECT * from tbl_lgl_status";
		
       // $tmpblnWhere = 0;
        		if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort']; else $reqsort = 'status_id';
				if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
				if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";	 
				//---------------------------------Paging--------------------------------------------------
				if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
				if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
				$t->assign('reqord',$reqord);
				$t->assign('perpage',$perpage);
				
				//-------------------------------end  Paging------------------------------------------------------
      //  $tmpblnWhere = 1;
        			if ( isset( $_REQUEST['txtkeyword'] ) ){
                    $reqkeyword = $_REQUEST['txtkeyword'] ;
                    $t->assign ( 'reqkeyword', $reqkeyword);
                    if (strcmp($reqkeyword,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_status.status_name like '%$reqkeyword%' ";
                        $PageString = $PageString  .'&txtkeyword='.$reqkeyword;
                    }
                }
        if (strcmp($reqsort, "") <> 0) $sql = $sql . ' order by ' . $reqsort;
        $sql = $sql . ' ' . $reqord;


// ---------------- call paging function---------------------------------------------------
        $strfnPagingSql = $sql; include 'includes/callpaging.php';
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        $cnt = 1;

     // echo $sql;
        foreach ($temp as $index => $row) {
            $row["cnt"] = $cnt++;
            $statusResult[] = $row;
        }
        $t->assign('status', $statusResult);
        $t->display('master_lookups/lgl_status/status_list.htm');

    }

}

?>
