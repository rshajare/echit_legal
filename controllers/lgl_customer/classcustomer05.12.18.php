<?
include_once ("classes/classdatabase.php");

class customer extends Database
{
    private $TableName;
    private $TableID;
    public $TableIDVal;

 
    private $SessionLanguage;
    private $TableFieldArray;
    public $TableFieldValArray;


    public function __construct()
    {
        global $t;
        $this->TableName = 'tbl_lgl_customer';
        $this->TableID = 'customer_id';
        $this->TableFieldArray = array(
            array('customer_name', 1, 0, PDO::PARAM_STR),//0
            array('designation', 1, 0, PDO::PARAM_STR),//1
            array('address', 1, 0, PDO::PARAM_STR),//2
		    array('chit_group_no', 1, 0, PDO::PARAM_STR),//3
            array('ticket_no', 1, 0, PDO::PARAM_INT),//4
            array('main_branch_id', 1, 1, PDO::PARAM_INT),//5
			
            array('chit_value', 1, 0, PDO::PARAM_INT),//6
            array('agreement_date', 1, 0, PDO::PARAM_STR),//7
            array('chit_period', 1, 0, PDO::PARAM_INT),//8
		    array('commencinf_from', 1, 0, PDO::PARAM_STR),//9
            array('terminating_on', 1, 0, PDO::PARAM_STR),//10
            array('monthly_sub', 1, 0, PDO::PARAM_INT),//11
			
            array('chit_reg_no', 1, 0, PDO::PARAM_STR),//12
            array('auction_amount', 1, 0, PDO::PARAM_INT),//13
            array('auction_date', 1, 0, PDO::PARAM_STR),//14
		    array('payment_amount', 1, 0, PDO::PARAM_INT),//15
            array('deduction_amount', 1, 0, PDO::PARAM_INT),//16
            array('cust_cheque_no', 1, 1, PDO::PARAM_INT),//17

            array('cust_cheque_date', 1, 1, PDO::PARAM_STR),//18
            array('cust_drawn_on', 1, 1, PDO::PARAM_STR),//19
            array('installments_cCleared', 1, 0, PDO::PARAM_INT),//20
		    array('pending_amount', 1, 0, PDO::PARAM_INT),//21
            array('collection_agent', 1, 1, PDO::PARAM_STR),//22
            array('company_id', 1, 1, PDO::PARAM_INT),//23
            array('custmor_age', 1, 0, PDO::PARAM_INT),//24
            array('custmor_pan_card_no', 1, 1, PDO::PARAM_STR),//25
            array('future_liability', 1, 0, PDO::PARAM_INT)//26
			
        );
		
		$this->SubTableName 			= 'tbl_lgl_customer_details';
		$this->SubTableID 				= 'customer_details_id';
		$this->SubTableFieldArray 		= array(			
			array('customer_details_name', 1 , 0, PDO::PARAM_STR),
            array('customer_details_designation', 1, 0, PDO::PARAM_STR),
            array('cusromer_details_address', 1, 0, PDO::PARAM_STR),
            array('custmor_details_age', 1, 0, PDO::PARAM_INT),
            array('custmor_id', 1, 0, PDO::PARAM_INT),
		);

		$this->SubSubTableName 				= 'tbl_lgl_chit_details';
		$this->SubSubTableID 				= 'gw_chit_details_id';
		$this->SubSubTableFieldArray 		= array(			
			array('gw_chit_group_no', 1 , 0, PDO::PARAM_STR),//0
            array('gw_ticket_no', 1, 0, PDO::PARAM_INT),//1
			array('gw_chit_value', 1 , 0, PDO::PARAM_INT),//2
            array('gw_chit_period', 1, 0, PDO::PARAM_INT),//3
			array('gw_chit_reg_no', 1 , 0, PDO::PARAM_STR),//4
            array('gw_monthly_sub', 1, 0, PDO::PARAM_INT),//5
			array('gw_commencinf_from', 1 , 0, PDO::PARAM_STR),//6
            array('gw_terminating_on', 1, 0, PDO::PARAM_STR),//7
			array('gw_auction_amount', 1 , 0, PDO::PARAM_INT),//8
            array('gw_auction_date', 1, 0, PDO::PARAM_STR),//9
			array('gw_payment_amount', 1 , 0, PDO::PARAM_INT),//10
            array('gw_deduction_amount', 1, 0, PDO::PARAM_INT),//11
			array('gw_agreement_date', 1 , 0, PDO::PARAM_STR),//12
            array('gw_installments_cCleared', 1, 0, PDO::PARAM_INT),//13
			array('gw_pending_amount', 1 , 0, PDO::PARAM_INT),//14
            array('gw_future_liability', 1, 0, PDO::PARAM_INT),//15
            array('custmor_id', 1, 0, PDO::PARAM_INT),//16
		);

        $this->SessionLanguage = $_SESSION['opt_lang'];
        $this->DBConnect();

        $comp_arr = array();
        $sql = "SELECT company_name,company_id FROM tbl_lgl_company order by `company_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $comp_arr['company_id'][] = $row['company_id'];
            $comp_arr['company_name'][] = $row['company_name'];
        }
        $t->assign('comp_arr', $comp_arr);

        $advocate_arr = array();
        $sql = "SELECT advocate_id,advocate_name FROM tbl_lgl_advocate order by `advocate_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $advocate_arr['advocate_id'][] = $row['advocate_id'];
            $advocate_arr['advocate_name'][] = $row['advocate_name'];
        }
        $t->assign('advocate_arr', $advocate_arr);
		
	  	$def_adv_arr = array();
        $sql = "SELECT def_adv_id,def_adv_name FROM  tbl_lgl_def_adv order by `def_adv_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $def_adv_arr['def_adv_id'][] = $row['def_adv_id'];
            $def_adv_arr['def_adv_name'][] = $row['def_adv_name'];
        }
        $t->assign('def_adv_arr', $def_adv_arr);
		
		
	    $court_arr = array();
        $sql = "SELECT court_id,court_name FROM tbl_lgl_court order by `court_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $court_arr['court_id'][] = $row['court_id'];
            $court_arr['court_name'][] = $row['court_name'];
        }
        $t->assign('court_arr', $court_arr);
		
	    $complainant_arr = array();
        $sql = "SELECT complainant_id,complainant_name FROM tbl_lgl_complainant order by `complainant_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $complainant_arr['complainant_id'][] = $row['complainant_id'];
            $complainant_arr['complainant_name'][] = $row['complainant_name'];
        }
        $t->assign('complainant_arr', $complainant_arr);

	    $agent_arr = array();
        $sql = "SELECT agent_id,agent_name FROM tbl_lgl_agent order by `agent_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $agent_arr['agent_id'][] = $row['agent_id'];
            $agent_arr['agent_name'][] = $row['agent_name'];
        }
        $t->assign('agent_arr', $agent_arr);
		
	    $Bankname_arr = array();
        $sql = "SELECT BankID,Bankname FROM tbl_lgl_banks order by `BankID` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $Bankname_arr['BankID'][] = $row['BankID'];
            $Bankname_arr['Bankname'][] = $row['Bankname'];
        }
        $t->assign('Bankname_arr', $Bankname_arr);
		
        $custmor_arr = array();
        $sql = "SELECT customer_name,customer_id FROM tbl_lgl_customer order by `customer_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $custmor_arr['customer_id'][] = $row['customer_id'];
            $custmor_arr['customer_name'][] = $row['customer_name'];
        }
        $t->assign('custmor_arr', $custmor_arr);
		
    }


    public function Addcustomer(){
        global $t;
        $t->display('master_lookups/lgl_customer/customer_add.htm');
    }
	public function addcustomerbyvalue($customerarr){
		global $t;
		for($i=0;$i<=count($customerarr);$i++){
			$this->TableFieldValArray[$i][0] 	= $customerarr[$i];
		}
		$customer_id=$this->InsertRecordValOnly($this->TableName,$this->TableFieldArray,$this->TableFieldValArray);//die();
	}
    public function Editcustomer(){
        global $t;
        $main = array();
        $customer_id = $_GET['customer_id'];

        $sql = "SELECT * FROM tbl_lgl_customer where customer_id=$customer_id";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
			$detailsql = "SELECT * FROM tbl_lgl_customer_details where custmor_id=$customer_id";
			$detailtemp = $this->db_pdo->prepare($detailsql);
			$detailtemp->execute();
			foreach ($detailtemp as $index => $detailrow) {
				$detaildata[] = $detailrow;
			}
			$row["details"]=$detaildata;

			$qw_detailsql = "SELECT * FROM tbl_lgl_chit_details where custmor_id=$customer_id";
			$qw_detailtemp = $this->db_pdo->prepare($qw_detailsql);
			$qw_detailtemp->execute();
			foreach ($qw_detailtemp as $index => $qw_detailrow) {
				$qw_detaildata[] = $qw_detailrow;
			}
			$row["qw_details"]=$qw_detaildata;
			$total=count($row["details"]);
			$qw_total=count($row["qw_details"]);
			
            $data[] = $row;
        	$t->assign('array', $array);
			
			//print_r($row["details"]);
		}
		$this->all_lk($row["company_id"]);
        $t->assign('total', $total);
        $t->assign('qw_total', $qw_total);
        $t->assign('data', $data);
        $t->display('master_lookups/lgl_customer/customer_edit.htm');

    }
	public function all_lk($req_company_id){
		global $t;

		$company_id=$req_company_id;
		//$branch_id=$req_branch_id;
		
		$branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$company_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);


        $customer_arr = array();
        $sql = "SELECT customer_name,customer_id FROM tbl_lgl_customer ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $customer_arr['customer_id'][] = $row['customer_id'];
            $customer_arr['customer_name'][] = $row['customer_name'];
        }
        $t->assign('customer_arr', $customer_arr);
		
        $complainant_arr = array();
        $sql = "SELECT complainant_id,complainant_name FROM tbl_lgl_complainant ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $complainant_arr['complainant_id'][] = $row['complainant_id'];
            $complainant_arr['complainant_name'][] = $row['complainant_name'];
        }
        $t->assign('complainant_arr', $complainant_arr);
		
        $advocate_arr = array();
        $sql = "SELECT advocate_id,advocate_name FROM tbl_lgl_advocate ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $advocate_arr['advocate_id'][] = $row['advocate_id'];
            $advocate_arr['advocate_name'][] = $row['advocate_name'];
        }
        $t->assign('advocate_arr', $advocate_arr);
       
	    $agent_arr = array();
        $sql = "SELECT agent_id,agent_name FROM tbl_lgl_agent ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $agent_arr['agent_id'][] = $row['agent_id'];
            $agent_arr['agent_name'][] = $row['agent_name'];
        }
        $t->assign('agent_arr', $agent_arr);
      
	    $court_arr = array();
        $sql = "SELECT court_id,court_name FROM tbl_lgl_court ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $court_arr['court_id'][] = $row['court_id'];
            $court_arr['court_name'][] = $row['court_name'];
        }
        $t->assign('court_arr', $court_arr);
       
	    $user_arr = array();
        $sql = "SELECT user_id,user_name FROM tbl_lgl_user ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $user_arr['user_id'][] = $row['user_id'];
            $user_arr['user_name'][] = $row['user_name'];
        }
        $t->assign('user_arr', $user_arr);
      
	  	$def_adv_arr = array();
        $sql = "SELECT def_adv_id,def_adv_name FROM  tbl_lgl_def_adv ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $def_adv_arr['def_adv_id'][] = $row['def_adv_id'];
            $def_adv_arr['def_adv_name'][] = $row['def_adv_name'];
        }
        $t->assign('def_adv_arr', $def_adv_arr);

	    $guarantor_arr = array();
        $sql = "SELECT guarantor_id,guarantor_name FROM tbl_lgl_guarantor ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $guarantor_arr['guarantor_id'][] = $row['guarantor_id'];
            $guarantor_arr['guarantor_name'][] = $row['guarantor_name'];
        }
        $t->assign('guarantor_arr', $guarantor_arr);
      
	  	$bank_arr = array();
        $sql = "SELECT BankID,Bankname FROM tbl_lgl_banks ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $bank_arr['BankID'][] = $row['BankID'];
            $bank_arr['Bankname'][] = $row['Bankname'];
        }
        $t->assign('bank_arr', $bank_arr);
   }
    public function destroy($customer_id){
        $del_id_val = $customer_id;
        $this->DeleteRecord($this->TableName, $this->TableID, $del_id_val);
    }
    public function store(){
		$this->TableFieldValArray[0][0]		=$_POST["txtcustomer_name1"];
		$this->TableFieldValArray[1][0]		=$_POST["txtdesignation1"];
		$this->TableFieldValArray[2][0]		=$_POST["txtaddress1"];
		$this->TableFieldValArray[3][0]		=$_POST["txtchit_group_no1"];
		$this->TableFieldValArray[4][0]		=$_POST["txtticket_no1"];
		$this->TableFieldValArray[6][0]		=$_POST["txtchit_value1"];
		$this->TableFieldValArray[7][0]		=$_POST["txtagreement_date1"];
		$this->TableFieldValArray[8][0]		=$_POST["txtchit_period1"];
		$this->TableFieldValArray[9][0]		=$_POST["txtcommencinf_from1"];
		$this->TableFieldValArray[10][0]	=$_POST["txtterminating_on1"];
		$this->TableFieldValArray[11][0]	=$_POST["txtmonthly_sub1"];
		$this->TableFieldValArray[12][0]	=$_POST["txtchit_reg_no1"];
		$this->TableFieldValArray[13][0]	=$_POST["txtauction_amount1"];
		$this->TableFieldValArray[14][0]	=$_POST["txtauction_date1"];
		$this->TableFieldValArray[15][0]	=$_POST["txtpayment_amount1"];
		$this->TableFieldValArray[16][0]	=$_POST["txtdeduction_amount1"];
		$this->TableFieldValArray[20][0]	=$_POST["txtinstallments_cCleared1"];
		$this->TableFieldValArray[21][0]	=$_POST["txtpending_amount1"];
		$this->TableFieldValArray[24][0]	=$_POST["txtfuture_liability1"];
		
        $customer_id = $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
		for($i=1;$i<=3;$i++)
		{
			$this->SubTableFieldValArray[0][0]=$_POST["txtcustomer_name$i"];
			$this->SubTableFieldValArray[1][0]=$_POST["txtdesignation$i"];
			$this->SubTableFieldValArray[2][0]=$_POST["txtaddress$i"];
			$this->SubTableFieldValArray[3][0]=$_POST["txtcustmor_age$i"];
			$this->SubTableFieldValArray[4][0]=$customer_id;
			if($_POST["txtcustomer_name$i"]!=""){
				$customer_details_id=$this->InsertRecord($this->SubTableName,$this->SubTableFieldArray,$this->SubTableFieldValArray);
			}
        }
		
		for($i=1;$i<=5;$i++)
		{
			$this->SubSubTableFieldValArray[0][0]	=$_POST["txtchit_group_no$i"];
			$this->SubSubTableFieldValArray[1][0]	=$_POST["txtticket_no$i"];
			$this->SubSubTableFieldValArray[2][0]	=$_POST["txtchit_value$i"];
			$this->SubSubTableFieldValArray[3][0]	=$_POST["txtchit_period$i"];
			$this->SubSubTableFieldValArray[4][0]	=$_POST["txtchit_reg_no$i"];
			$this->SubSubTableFieldValArray[5][0]	=$_POST["txtmonthly_sub$i"];
			$this->SubSubTableFieldValArray[6][0]	=$_POST["txtcommencinf_from$i"];
			$this->SubSubTableFieldValArray[7][0]	=$_POST["txtterminating_on$i"];
			$this->SubSubTableFieldValArray[8][0]	=$_POST["txtauction_amount$i"];
			$this->SubSubTableFieldValArray[9][0]	=$_POST["txtauction_date$i"];
			$this->SubSubTableFieldValArray[10][0]	=$_POST["txtpayment_amount$i"];
			$this->SubSubTableFieldValArray[11][0]	=$_POST["txtdeduction_amount$i"];
			$this->SubSubTableFieldValArray[12][0]	=$_POST["txtagreement_date$i"];
			$this->SubSubTableFieldValArray[13][0]	=$_POST["txtinstallments_cCleared$i"];
			$this->SubSubTableFieldValArray[14][0]	=$_POST["txtpending_amount$i"];
			$this->SubSubTableFieldValArray[15][0]	=$_POST["txtfuture_liability$i"];
			$this->SubSubTableFieldValArray[16][0]	=$customer_id;
			if($_POST["txtchit_group_no$i"]!=""){
				$gw_chit_details_id=$this->InsertRecord($this->SubSubTableName,$this->SubSubTableFieldArray,$this->SubSubTableFieldValArray);
			}
        }
		
        header("Location: lgl_customer.php");
    }
    public function update(){
		$this->TableFieldValArray[0][0]		=$_POST["txtcustomer_details_name1"];
		$this->TableFieldValArray[1][0]		=$_POST["txtcustomer_details_designation1"];
		$this->TableFieldValArray[2][0]		=$_POST["txtcusromer_details_address1"];
		$this->TableFieldValArray[3][0]		=$_POST["txtchit_group_no1"];
		$this->TableFieldValArray[4][0]		=$_POST["txtticket_no1"];
		$this->TableFieldValArray[6][0]		=$_POST["txtchit_value1"];
		$this->TableFieldValArray[7][0]		=$_POST["txtagreement_date1"];
		$this->TableFieldValArray[8][0]		=$_POST["txtchit_period1"];
		$this->TableFieldValArray[9][0]		=$_POST["txtcommencinf_from1"];
		$this->TableFieldValArray[10][0]	=$_POST["txtterminating_on1"];
		$this->TableFieldValArray[11][0]	=$_POST["txtmonthly_sub1"];
		$this->TableFieldValArray[12][0]	=$_POST["txtchit_reg_no1"];
		$this->TableFieldValArray[13][0]	=$_POST["txtauction_amount1"];
		$this->TableFieldValArray[14][0]	=$_POST["txtauction_date1"];
		$this->TableFieldValArray[15][0]	=$_POST["txtpayment_amount1"];
		$this->TableFieldValArray[16][0]	=$_POST["txtdeduction_amount1"];
		$this->TableFieldValArray[20][0]	=$_POST["txtinstallments_cCleared1"];
		$this->TableFieldValArray[21][0]	=$_POST["txtpending_amount1"];
		$this->TableFieldValArray[24][0]	=$_POST["txtcustmor_details_age1"];
		$this->TableFieldValArray[26][0]	=$_POST["txtfuture_liability1"];

        $customer_id=$this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
        
		for($i=1;$i<=3;$i++)
		{
			$req_customer_details_id			=$_POST["txtcustomer_details_id$i"];
			$req_customer_name					=$_POST["txtcustomer_details_name$i"];
			$req_designation					=$_POST["txtcustomer_details_designation$i"];
  			$req_address						=$_POST["txtcusromer_details_address$i"];
			$req_custmor_age					=$_POST["txtcustmor_details_age$i"];
			$req_customer_id					=$customer_id;
			if($_POST["txtcustomer_details_name$i"]!=""){
				
				if($req_customer_details_id!=""){
					
					echo $update_sql = "update `tbl_lgl_customer_details` set `customer_details_name`= '$req_customer_name',  
									   `customer_details_designation`= '$req_designation',`cusromer_details_address`= '$req_address',	
									   `custmor_details_age`= '$req_custmor_age',`custmor_id`= '$req_customer_id'
									    where `customer_details_id` = ".$req_customer_details_id;
					
					$update_result 	= mysql_query($update_sql) or die(mysql_error().$update_sql);
					}
				else{
					$insq_sql = "INSERT INTO `tbl_lgl_customer_details` (`customer_details_id`,`customer_details_name`,`customer_details_designation`,`cusromer_details_address`,`custmor_details_age`,`custmor_id`) 
															 VALUES (NULL,'$req_customer_name','$req_designation','$req_address','$req_custmor_age', '$req_customer_id')";
					
				$ins_result = mysql_query($insq_sql) or die(mysql_error().$insq_sql);
				}
			}
      	}
		for($i=1;$i<=5;$i++)
		{
			$req_gw_chit_details_id			=$_POST["txtgw_chit_details_id$i"];
			$req_chit_group_no				=$_POST["txtchit_group_no$i"];
			$req_ticket_no					=$_POST["txtticket_no$i"];
  			$req_chit_value					=$_POST["txtchit_value$i"];
			$req_chit_period				=$_POST["txtchit_period$i"];
			
			$req_chit_reg_no				=$_POST["txtchit_reg_no$i"];
			$req_monthly_sub				=$_POST["txtmonthly_sub$i"];
  			$req_commencinf_from			=$_POST["txtcommencinf_from$i"];
			$req_terminating_on				=$_POST["txtterminating_on$i"];
			
			$req_auction_amount				=$_POST["txtauction_amount$i"];
			$req_auction_date				=$_POST["txtauction_date$i"];
  			$req_payment_amount				=$_POST["txtpayment_amount$i"];
			$req_deduction_amount			=$_POST["txtdeduction_amount$i"];

			$req_agreement_date				=$_POST["txtagreement_date$i"];
			$req_installments_cCleared		=$_POST["txtinstallments_cCleared$i"];
  			$req_pending_amount				=$_POST["txtpending_amount$i"];
			$req_future_liability			=$_POST["txtfuture_liability$i"];
			$req_customer_id				=$customer_id;
			if($_POST["txtchit_group_no$i"]!=""){
				if($req_gw_chit_details_id!=""){
					$update_sql 	= "update `tbl_lgl_chit_details` set 
									  `gw_chit_group_no`= '$req_chit_group_no',`gw_ticket_no`= '$req_ticket_no',`gw_chit_value`= '$req_chit_value',`gw_chit_period`= '$req_chit_period',
									  `gw_chit_reg_no`= '$req_chit_reg_no',`gw_monthly_sub`= '$req_monthly_sub',`gw_commencinf_from`= '$req_commencinf_from',`gw_terminating_on`= '$req_terminating_on',
									  `gw_auction_amount`= '$req_auction_amount',`gw_auction_date`= '$req_auction_date',`gw_payment_amount`= '$req_payment_amount',`gw_deduction_amount`= '$req_deduction_amount',
									  `gw_agreement_date`= '$req_agreement_date',`gw_installments_cCleared`= '$req_installments_cCleared',`gw_pending_amount`= '$req_pending_amount',`gw_future_liability`= '$req_future_liability',
									  `custmor_id` 	= '$req_customer_id'
									   where `gw_chit_details_id` = ".$req_gw_chit_details_id;
					$update_result 	= mysql_query($update_sql) or die(mysql_error().$update_sql);
					}
				else{
				$insq_sql = "INSERT INTO `tbl_lgl_chit_details` (`gw_chit_details_id`,`gw_chit_group_no`,`gw_ticket_no`,`gw_chit_value`,`gw_chit_period`,
																`gw_chit_reg_no`,`gw_monthly_sub`,`gw_commencinf_from`,`gw_terminating_on`,`gw_auction_amount`,
																`gw_auction_date`,`gw_payment_amount`,`gw_deduction_amount`,`gw_agreement_date`,gw_installments_cCleared,
																`gw_pending_amount`,`gw_future_liability`,`custmor_id`) 
														VALUES  (NULL,'$req_chit_group_no','$req_ticket_no','$req_chit_value','$req_chit_period',
																'$req_chit_reg_no','$req_monthly_sub','$req_commencinf_from','$req_terminating_on',
																'$req_auction_amount','$req_auction_date','$req_payment_amount','$req_deduction_amount',
																'$req_agreement_date','$req_installments_cCleared','$req_pending_amount','$req_future_liability', '$req_customer_id')";
				$ins_result = mysql_query($insq_sql) or die(mysql_error().$insq_sql);
				}
			}
      	}
	  
	header("Location: lgl_customer.php");
    }
	public function index(){
  
		  global $t;
		  $sql= "	SELECT * from tbl_lgl_customer
				  LEFT JOIN tbl_lgl_branch on tbl_lgl_customer.main_branch_id = tbl_lgl_branch.branch_id
				  LEFT JOIN tbl_lgl_company on tbl_lgl_customer.company_id = tbl_lgl_company.company_id";
  
				  if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort'];	else $reqsort='customer_id';
				  if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
				  if ($reqord == "asc") $reqord = "asc"; elseif ($reqord == "desc") $reqord = "asc";	 
				  //---------------------------------Paging--------------------------------------------------
				  if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
				  if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
				  $t->assign('reqord',$reqord);
				  $t->assign('perpage',$perpage);
				  
				  //-------------------------------end  Paging------------------------------------------------------
				  //$tmpblnWhere = 1;
					  if ( isset( $_REQUEST['txtkeyword'] ) ){
						  $reqkeyword = $_REQUEST['txtkeyword'] ;
						  $t->assign ( 'reqkeyword', $reqkeyword);
						  if (strcmp($reqkeyword,"")<>0){
							  if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
							  $sql = $sql . "tbl_lgl_customer.customer_name LIKE '%$reqkeyword%' ";
							  $PageString = $PageString  .'&txtkeyword='.$reqkeyword;
						  }
					  }
					  if ( isset( $_REQUEST['txtkeyword4'] ) ){
						  $reqkeyword4 = $_REQUEST['txtkeyword4'] ;
						  $t->assign ( 'reqkeyword4', $reqkeyword4);
						  if (strcmp($reqkeyword4,"")<>0){
							  if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
							  $sql = $sql . "tbl_lgl_company.company_id = '$reqkeyword4' ";
							  $PageString = $PageString  .'&txtkeyword4='.$reqkeyword4;
						  }
					  }
					  if ( isset( $_REQUEST['txtkeyword5'] ) ){
						  $reqkeyword5 = $_REQUEST['txtkeyword5'] ;
						  $t->assign ( 'reqkeyword5', $reqkeyword5);
						  if (strcmp($reqkeyword5,"")<>0){
							  if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
							  $sql = $sql . "tbl_lgl_branch.branch_id = '$reqkeyword5' ";
							  $PageString = $PageString  .'&txtkeyword5='.$reqkeyword5;
						  }
					  }
				  
		  
  if (strcmp($reqsort,"")<>0)  $sql = $sql . ' order by ' . $reqsort ;
  $sql = $sql . ' ' . $reqord ;
  
  //echo $sql;
  // ---------------- call paging function---------------------------------------------------
  $strfnPagingSql = $sql; include 'includes/callpaging.php'; 
  //----------------------------------------------------------------------------------- 
		  $temp = $this->db_pdo->prepare($sql);
		  $temp->execute();
		  $cnt = 1;
  
		 //echo $sql;
		  foreach ($temp as $index => $row) {
			  $row["cnt"] = $cnt++;
			  $row["status"]	= fngetValue("tbl_lgl_status_change","status","customer_id='".$row["customer_id"]."'");
			  $row["status_name"]	= fngetValue("tbl_lgl_status","status_name","status_id='".$row["status"]."'");
			  $row["company_det"]	= fngetValue("tbl_lgl_company","company_name","company_id='".$row["company_id"]."'");
			  $row["branch_det"]	= fngetValue("tbl_lgl_branch","branch_name","branch_id='".$row["main_branch_id"]."'");
			  $customerResult[] = $row;
		  }
  
		  $branch_arr = array();
		  $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$reqkeyword4' order by `branch_name` asc";
		  $temp = $this->db_pdo->prepare($sql);
		  $temp->execute();
		  foreach ($temp as $index => $row) {
			  $branch_arr['branch_id'][] = $row['branch_id'];
			  $branch_arr['branch_name'][] = $row['branch_name'];
		  }
		  $t->assign('branch_arr', $branch_arr);
  
		  $custmor_arr = array();
		  $sql = "SELECT customer_name,customer_id FROM tbl_lgl_customer where main_branch_id='$reqkeyword5' and company_id='$reqkeyword4' order by `customer_name`   asc";
  //        echo $sql;
		  $temp = $this->db_pdo->prepare($sql);
		  $temp->execute();
		  foreach ($temp as $index => $row) {
			  $custmor_arr['customer_id'][] = $row['customer_id'];
			  $custmor_arr['customer_name'][] = $row['customer_name'];
		  }
		  $t->assign('custmor_arr', $custmor_arr);
		  
		  $t->assign('customer', $customerResult);
		  $t->display('master_lookups/lgl_customer/customer_list.htm');
  
	  }
    public function customerfullinfo() 	
    {
        global $t;
        if ($_GET['customer_id'] != "") {
			
		$sql= "	SELECT * from tbl_lgl_customer
				LEFT JOIN tbl_lgl_branch on tbl_lgl_customer.main_branch_id = tbl_lgl_branch.branch_id
				LEFT JOIN tbl_lgl_company on tbl_lgl_customer.company_id = tbl_lgl_company.company_id
				WHERE customer_id = " . $_GET['customer_id'];
			
            $temp = $this->db_pdo->prepare($sql);
            $temp->execute();
            foreach ($temp as $index => $row) {
            	$row['customer_name'] = $row['customer_name']. ' ( ' . $row['chit_group_no']. ' | ' . $row['ticket_no'] . ' ) ' ;
				$row["case_no"]		= fngetValue("tbl_lgl_case_entry","case_no","customer_id='".$row["customer_id"]."'");
				$row["case_category_id"]	= fngetValue("tbl_lgl_case_entry","case_category_id","customer_id='".$row["customer_id"]."'");
				$row["category_name"]	= fngetValue("tbl_lgl_customer_report_category","category_name","category_id='".$row["case_category_id"]."'");
				$row["status"]	= fngetValue("tbl_lgl_status_change","status","customer_id='".$row["customer_id"]."'");
				$row["status_name"]	= fngetValue("tbl_lgl_status","status_name","status_id='".$row["status"]."'");
				$data[] = $row;
            }
            // Legal Notice Entry List
            $legalnoticedata = array();
            $legalnoticesql = "select * from tbl_lgl_legal_notice where `customer_id` = " . $_GET['customer_id']." and guarantor_id IS NULL";
			$legalnoticetemp = $this->db_pdo->prepare($legalnoticesql);
            $legalnoticetemp->execute();
            foreach ($legalnoticetemp as $index => $legalnoticerow) {
				
				$sql_guarantor= "SELECT * from tbl_lgl_legal_notice where customer_id=".$_GET['customer_id']." and guarantor_id IS NOT NULL ";
				$temp = $this->db_pdo->prepare($sql_guarantor);
				$temp->execute();
				$guarantorResult=array();
				foreach ($temp as $index => $row_guarantor) {
					$row_guarantor['legal_notice_date'] 	= sqldateout($row_guarantor['legal_notice_date']);
					$row_guarantor['send_on'] 				= sqldateout($row_guarantor['send_on']);
					$row_guarantor['received_on'] 			= sqldateout($row_guarantor['received_on']);
					$row_guarantor['ack_received_on']		= sqldateout($row_guarantor['ack_received_on']);
					$row_guarantor["guarantor_name"]		=fngetvalue("tbl_lgl_guarantor","guarantor_name","guarantor_id=".$row_guarantor["guarantor_id"]."");
					$guarantorResult[] = $row_guarantor;
				}
				
				$legalnoticerow['guarantor_details'] = $guarantorResult;
				
				$legalnoticerow['legal_notice_date'] 	= sqldateout($legalnoticerow['legal_notice_date']);
				$legalnoticerow['send_on'] 				= sqldateout($legalnoticerow['send_on']);
				$legalnoticerow['received_on'] 			= sqldateout($legalnoticerow['received_on']);
				$legalnoticerow['ack_received_on']		= sqldateout($legalnoticerow['ack_received_on']);
                $legalnoticedata[] = $legalnoticerow;
            }
			
			
            // Guarantor List
            $guarantordata = array();
            $guarantorsql = "select * from tbl_lgl_guarantor where `customer_id` = " . $_GET['customer_id']." ";
			$guarantortemp = $this->db_pdo->prepare($guarantorsql);
            $guarantortemp->execute();
            foreach ($guarantortemp as $index => $guarantorrow) {
                $guarantorrow['guarantor_dob'] = sqldateout($guarantorrow['guarantor_dob']);
                $guarantordata[] = $guarantorrow;
            }
            
			
			// Cheque Return List
            $cheque_returndata = array();
            $cheque_returnsql = "select * from tbl_lgl_cheque_return where `customer_id` = " . $_GET['customer_id']." ";
			$cheque_returntemp = $this->db_pdo->prepare($cheque_returnsql);
            $cheque_returntemp->execute();
            foreach ($cheque_returntemp as $index => $cheque_returnrow) {
				$cheque_returnrow["drawn_onBankname"]			= fngetValue("tbl_lgl_banks","Bankname","BankID='".$cheque_returnrow["drawn_on"]."'");
				$cheque_returnrow["ch_deposited_atBankname"]	= fngetValue("tbl_lgl_banks","Bankname","BankID='".$cheque_returnrow["ch_deposited_at"]."'");
				$cheque_returnrow['cheque_date'] 				= sqldateout($cheque_returnrow['cheque_date']);
				$cheque_returnrow['ch_received_date'] 			= sqldateout($cheque_returnrow['ch_received_date']);
				$cheque_returnrow['ch_deposited_on'] 			= sqldateout($cheque_returnrow['ch_deposited_on']);
				$cheque_returnrow['cheque_return_date']			= sqldateout($cheque_returnrow['cheque_return_date']);
				$cheque_returnrow["comp_bank"]					= fngetValue("tbl_lgl_banks","Bankname","BankID='".$cheque_returnrow["comp_bank"]."'");
				$cheque_returndata[] = $cheque_returnrow;
            }
			

			// Payment List
            $paymentdata = array();
            $paymentsql = "select * from tbl_lgl_payment where `customer_id` = " . $_GET['customer_id']." ";
			$paymenttemp = $this->db_pdo->prepare($paymentsql);
            $paymenttemp->execute();
            foreach ($paymenttemp as $index => $paymentrow) {
				$paymentrow["pay_cheque_bankBankname"]			= fngetValue("tbl_lgl_banks","Bankname","BankID='".$paymentrow["pay_cheque_bank"]."'");
				$paymentrow['pay_date']			 				= sqldateout($paymentrow['pay_date']);
				$paymentrow['pay_cheque_date']			 		= sqldateout($paymentrow['pay_cheque_date']);
				$paymentdata[] = $paymentrow;
            }
			
			// Status Change List
            $status_changedata = array();
            $status_changesql = "select * from tbl_lgl_status_change where `customer_id` = " . $_GET['customer_id']." ";
			$status_changetemp = $this->db_pdo->prepare($status_changesql);
            $status_changetemp->execute();
            foreach ($status_changetemp as $index => $status_changerow) {
				$status_changerow["status_name"]	= fngetValue("tbl_lgl_status","status_name","status_id='".$status_changerow["status"]."'");
				$status_changerow['status_date']	= sqldateout($status_changerow['status_date']);
				$status_changedata[] = $status_changerow;
            }
			
			// Case Entry List
            $case_entrydata = array();
            $case_entrysql = "select * from tbl_lgl_case_entry where `customer_id` = " . $_GET['customer_id']." ";
			$case_entrytemp = $this->db_pdo->prepare($case_entrysql);
            $case_entrytemp->execute();
            foreach ($case_entrytemp as $index => $case_entryrow) {
				$case_entry							= $case_entryrow['case_id'];
				$case_entryrow["case_category"]		= fngetValue("tbl_lgl_customer_report_category","category_name","category_id='".$case_entryrow["case_category_id"]."'");
				$case_entryrow["court"]				= fngetValue("tbl_lgl_court","court_name","court_id='".$case_entryrow["court"]."'");
				$case_entryrow["advocate"]			= fngetValue("tbl_lgl_advocate","advocate_name","advocate_id='".$case_entryrow["advocate"]."'");
				$case_entryrow["opp_advocate"]		= fngetValue("tbl_lgl_def_adv","def_adv_name","def_adv_id='".$case_entryrow["opp_advocate"]."'");
				$case_entryrow["complainant"]		= fngetValue("tbl_lgl_complainant","complainant_name","complainant_id='".$case_entryrow["complainant"]."'");
				$case_entryrow['case_date']			= sqldateout($case_entryrow['case_date']);
				$case_entrydata[] = $case_entryrow;
            }
			
			// Internal Note List
            $internal_notedata = array();
            $internal_notesql = "select * from tbl_lgl_internal_note where `customer_id` = " . $_GET['customer_id']." ";
			$internal_notetemp = $this->db_pdo->prepare($internal_notesql);
            $internal_notetemp->execute();
            foreach ($internal_notetemp as $index => $internal_noterow) {
				$internal_noterow['note_date'] = sqldateout($internal_noterow['note_date']);
				$internal_noterow['nxt_hearing_date'] = sqldateout($internal_noterow['nxt_hearing_date']);
				$internal_notedata[] = $internal_noterow;
            }
			
			
			

			// 138 Report List
            $customer_138_reportdata = array();
			$customer_138_reportsql=" SELECT tbl_lgl_report_draft.report_draft_id as draft_report_draft_id,report_details.report_draft_id,
									  tbl_lgl_report_draft.report_name,report_details.customer_id ,report_details.customer_report_id
									  FROM tbl_lgl_report_draft
									  LEFT JOIN tbl_lgl_customer_report_category on tbl_lgl_report_draft.category_id = tbl_lgl_customer_report_category.category_id
									  LEFT JOIN (select * from tbl_lgl_customer_report where customer_id= " . $_GET['customer_id'].") as report_details
									  on (tbl_lgl_report_draft.report_draft_id = report_details.report_draft_id)
									  WHERE tbl_lgl_report_draft.category_id='3' 
									  ORDER BY tbl_lgl_report_draft.report_name asc"; 
									  
			//echo $customer_138_reportsql;						  
  			$customer_138_reporttemp = $this->db_pdo->prepare($customer_138_reportsql);
            $customer_138_reporttemp->execute();
            foreach ($customer_138_reporttemp as $index => $customer_138_reportrow) {
                $customer_138_reportrow['report_date'] = sqldateout(fngetvalue("tbl_lgl_customer_report","report_date","customer_report_id='" . $customer_138_reportrow['customer_report_id']." '"));
				$customer_138_reportdata[] = $customer_138_reportrow;
            }

			// Arbitration Report List
            $customer_arb_reportdata = array();
			
            $customer_arb_reportsql = "SELECT tbl_lgl_report_draft.report_draft_id as draft_report_draft_id,report_details.report_draft_id,
									  tbl_lgl_report_draft.report_name,report_details.customer_id ,report_details.customer_report_id
									  FROM tbl_lgl_report_draft
									  LEFT JOIN tbl_lgl_customer_report_category on tbl_lgl_report_draft.category_id = tbl_lgl_customer_report_category.category_id
									  LEFT JOIN (select * from tbl_lgl_customer_report where customer_id= " . $_GET['customer_id'].") as report_details
									  on (tbl_lgl_report_draft.report_draft_id = report_details.report_draft_id)
									  WHERE tbl_lgl_report_draft.category_id='4' 
									  ORDER BY tbl_lgl_report_draft.report_name asc"; 
			$customer_arb_reporttemp = $this->db_pdo->prepare($customer_arb_reportsql);
            $customer_arb_reporttemp->execute();
            foreach ($customer_arb_reporttemp as $index => $customer_arb_reportrow) {
                $customer_arb_reportrow['report_date'] = sqldateout(fngetvalue("tbl_lgl_customer_report","report_date","customer_report_id='" . $customer_arb_reportrow['customer_report_id']." '"));
				$customer_arb_reportdata[] = $customer_arb_reportrow;
            }

			// Legal Notice Report List
            $customer_lgl_reportdata = array();
			
            $customer_lgl_reportsql = "SELECT tbl_lgl_report_draft.report_draft_id as draft_report_draft_id,report_details.report_draft_id,
									  tbl_lgl_report_draft.report_name,report_details.customer_id ,report_details.customer_report_id
									  FROM tbl_lgl_report_draft
									  LEFT JOIN tbl_lgl_customer_report_category on tbl_lgl_report_draft.category_id = tbl_lgl_customer_report_category.category_id
									  LEFT JOIN (select * from tbl_lgl_customer_report where customer_id= " . $_GET['customer_id'].") as report_details
									  on (tbl_lgl_report_draft.report_draft_id = report_details.report_draft_id)
									  WHERE tbl_lgl_report_draft.category_id='5' 
									  ORDER BY tbl_lgl_report_draft.report_name asc"; 
			$customer_lgl_reporttemp = $this->db_pdo->prepare($customer_lgl_reportsql);
            $customer_lgl_reporttemp->execute();
            foreach ($customer_lgl_reporttemp as $index => $customer_lgl_reportrow) {
                $customer_lgl_reportrow['report_date'] = sqldateout(fngetvalue("tbl_lgl_customer_report","report_date","customer_report_id='" . $customer_lgl_reportrow['customer_report_id']." '"));
				$customer_lgl_reportdata[] = $customer_lgl_reportrow;
            }

            $t->assign('data', $data);
            $t->assign('legalnoticedata', $legalnoticedata);
            $t->assign('guarantordata', $guarantordata);
            $t->assign('cheque_returndata', $cheque_returndata);
            $t->assign('periodical_updationdata', $periodical_updationdata);
            $t->assign('paymentdata', $paymentdata);
            $t->assign('status_changedata', $status_changedata);
            $t->assign('internal_notedata', $internal_notedata);
            $t->assign('customer_138_reportdata', $customer_138_reportdata);
            $t->assign('customer_arb_reportdata', $customer_arb_reportdata);
            $t->assign('customer_lgl_reportdata', $customer_lgl_reportdata);
            $t->assign('case_entrydata', $case_entrydata);
            $t->assign('case_entry', $case_entry);
            $t->assign('customer_id', $_GET['customer_id']);
			
     }
        $t->display('master_lookups/lgl_customer/customer_details.htm');
        die();
    }                                    
}

?>
