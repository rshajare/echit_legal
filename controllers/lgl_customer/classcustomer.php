<?
include_once ("classes/classdatabase.php");

class customer extends Database
{
    private $TableName;
    private $TableID;
    public $TableIDVal;

 
    private $SessionLanguage;
    private $TableFieldArray;
    public $TableFieldValArray;


    public function __construct()
    {
        global $t;
        $this->TableName = 'tbl_lgl_customer';
        $this->TableID = 'customer_id';
        $this->TableFieldArray = array(
            array('customer_name', 1, 1, PDO::PARAM_STR),
            array('designation', 1, 1, PDO::PARAM_STR),
            array('address', 1, 1, PDO::PARAM_STR),
		    array('chit_group_no', 1, 1, PDO::PARAM_STR),
            array('ticket_no', 1, 1, PDO::PARAM_INT),
            array('main_branch_id', 1, 1, PDO::PARAM_INT), //5
			
            array('chit_value', 1, 1, PDO::PARAM_INT),
            array('agreement_date', 1, 1, PDO::PARAM_STR),
            array('chit_period', 1, 1, PDO::PARAM_INT),
		    array('commencinf_from', 1, 1, PDO::PARAM_STR),
            array('terminating_on', 1, 1, PDO::PARAM_STR),
            array('monthly_sub', 1, 1, PDO::PARAM_INT), //11
			
            array('chit_reg_no', 1, 1, PDO::PARAM_STR), //12
            array('auction_amount', 1, 1, PDO::PARAM_INT),//13
            array('auction_date', 1, 1, PDO::PARAM_STR),//14
		    array('payment_amount', 1, 1, PDO::PARAM_INT),//15
            array('deduction_amount', 1, 1, PDO::PARAM_INT),//16
            array('cust_cheque_no', 1, 1, PDO::PARAM_INT),//17

            array('cust_cheque_date', 1, 1, PDO::PARAM_STR),//18
            array('cust_drawn_on', 1, 1, PDO::PARAM_STR),
            array('installments_cCleared', 1, 1, PDO::PARAM_INT),
		    array('pending_amount', 1, 1, PDO::PARAM_INT),
            //array('complainant', 1, 1, PDO::PARAM_STR),
            array('collection_agent', 1, 1, PDO::PARAM_STR),//22

           // array('court', 1, 1, PDO::PARAM_STR),
           // array('judge', 1, 1, PDO::PARAM_STR),
           // array('advocate', 1, 1, PDO::PARAM_STR),
		    //array('opp_advocate', 1, 1, PDO::PARAM_STR),
            array('company_id', 1, 1, PDO::PARAM_INT), //23
            array('custmor_age', 1, 1, PDO::PARAM_INT),
            array('custmor_pan_card_no', 1, 1, PDO::PARAM_STR),
           // array('joining_on', 1, 1, PDO::PARAM_STR),
            array('future_liability', 1, 1, PDO::PARAM_INT), //26
			
            array('gst', 1, 1, PDO::PARAM_INT), //27
            array('oth_charges', 1, 1, PDO::PARAM_INT),
            array('adv_payment', 1, 1, PDO::PARAM_INT),
            array('sub_due', 1, 1, PDO::PARAM_INT),
            array('sec_deposit', 1, 1, PDO::PARAM_INT),
            array('last_pay_date', 1, 1, PDO::PARAM_STR),//32
            array('TR_no', 1, 1, PDO::PARAM_INT),
            array('div_paid', 1, 1, PDO::PARAM_INT),
            array('sub_paid', 1, 1, PDO::PARAM_INT),
            array('chit_reg_date', 1, 1, PDO::PARAM_INT),
            array('pend_amt_arbitration', 1, 1, PDO::PARAM_INT),
            array('pend_amt_legal_notice', 1, 1, PDO::PARAM_INT)
			
        );
        $this->SessionLanguage = $_SESSION['opt_lang'];
        $this->DBConnect();

        $comp_arr = array();
        $sql = "SELECT company_name,company_id FROM tbl_lgl_company order by `company_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $comp_arr['company_id'][] = $row['company_id'];
            $comp_arr['company_name'][] = $row['company_name'];
        }
        $t->assign('comp_arr', $comp_arr);

        $advocate_arr = array();
        $sql = "SELECT advocate_id,advocate_name FROM tbl_lgl_advocate order by `advocate_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $advocate_arr['advocate_id'][] = $row['advocate_id'];
            $advocate_arr['advocate_name'][] = $row['advocate_name'];
        }
        $t->assign('advocate_arr', $advocate_arr);
		
	  	$def_adv_arr = array();
        $sql = "SELECT def_adv_id,def_adv_name FROM  tbl_lgl_def_adv order by `def_adv_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $def_adv_arr['def_adv_id'][] = $row['def_adv_id'];
            $def_adv_arr['def_adv_name'][] = $row['def_adv_name'];
        }
        $t->assign('def_adv_arr', $def_adv_arr);
		
		
	    $court_arr = array();
        $sql = "SELECT court_id,court_name FROM tbl_lgl_court order by `court_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $court_arr['court_id'][] = $row['court_id'];
            $court_arr['court_name'][] = $row['court_name'];
        }
        $t->assign('court_arr', $court_arr);
		
	    $complainant_arr = array();
        $sql = "SELECT complainant_id,complainant_name FROM tbl_lgl_complainant order by `complainant_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $complainant_arr['complainant_id'][] = $row['complainant_id'];
            $complainant_arr['complainant_name'][] = $row['complainant_name'];
        }
        $t->assign('complainant_arr', $complainant_arr);

	    $agent_arr = array();
        $sql = "SELECT agent_id,agent_name FROM tbl_lgl_agent order by `agent_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $agent_arr['agent_id'][] = $row['agent_id'];
            $agent_arr['agent_name'][] = $row['agent_name'];
        }
        $t->assign('agent_arr', $agent_arr);
		
	    $Bankname_arr = array();
        $sql = "SELECT BankID,Bankname FROM tbl_lgl_banks order by `BankID` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $Bankname_arr['BankID'][] = $row['BankID'];
            $Bankname_arr['Bankname'][] = $row['Bankname'];
        }
        $t->assign('Bankname_arr', $Bankname_arr);
		
        $custmor_arr = array();
        $sql = "SELECT customer_name,customer_id FROM tbl_lgl_customer order by `customer_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $custmor_arr['customer_id'][] = $row['customer_id'];
            $custmor_arr['customer_name'][] = $row['customer_name'];
        }
        $t->assign('custmor_arr', $custmor_arr);
		
    }


    public function Addcustomer(){
        global $t;
        $t->display('master_lookups/lgl_customer/customer_add.htm');
    }
	public function addcustomerbyvalue($customerarr){
		global $t;
		for($i=0;$i<=count($customerarr);$i++){
			$this->TableFieldValArray[$i][0] 	= $customerarr[$i];
		}
		$customer_id=$this->InsertRecordValOnly($this->TableName,$this->TableFieldArray,$this->TableFieldValArray);//die();
	}
    public function Editcustomer(){
        global $t;
        $main = array();
        $customer_id = $_GET['customer_id'];
        $sql = "SELECT * FROM tbl_lgl_customer where customer_id=$customer_id";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $data[] = $row;
		}
		$this->all_lk($row["company_id"]);

        $t->assign('data', $data);

        $t->display('master_lookups/lgl_customer/customer_edit.htm');

    }
	public function all_lk($req_company_id){
		global $t;

		$company_id=$req_company_id;
		//$branch_id=$req_branch_id;
		
		$branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$company_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);


        $customer_arr = array();
        $sql = "SELECT customer_name,customer_id FROM tbl_lgl_customer ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $customer_arr['customer_id'][] = $row['customer_id'];
            $customer_arr['customer_name'][] = $row['customer_name'];
        }
        $t->assign('customer_arr', $customer_arr);
		
        $complainant_arr = array();
        $sql = "SELECT complainant_id,complainant_name FROM tbl_lgl_complainant ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $complainant_arr['complainant_id'][] = $row['complainant_id'];
            $complainant_arr['complainant_name'][] = $row['complainant_name'];
        }
        $t->assign('complainant_arr', $complainant_arr);
		
        $advocate_arr = array();
        $sql = "SELECT advocate_id,advocate_name FROM tbl_lgl_advocate ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $advocate_arr['advocate_id'][] = $row['advocate_id'];
            $advocate_arr['advocate_name'][] = $row['advocate_name'];
        }
        $t->assign('advocate_arr', $advocate_arr);
       
	    $agent_arr = array();
        $sql = "SELECT agent_id,agent_name FROM tbl_lgl_agent ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $agent_arr['agent_id'][] = $row['agent_id'];
            $agent_arr['agent_name'][] = $row['agent_name'];
        }
        $t->assign('agent_arr', $agent_arr);
      
	    $court_arr = array();
        $sql = "SELECT court_id,court_name FROM tbl_lgl_court ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $court_arr['court_id'][] = $row['court_id'];
            $court_arr['court_name'][] = $row['court_name'];
        }
        $t->assign('court_arr', $court_arr);
       
	    $user_arr = array();
        $sql = "SELECT user_id,user_name FROM tbl_lgl_user ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $user_arr['user_id'][] = $row['user_id'];
            $user_arr['user_name'][] = $row['user_name'];
        }
        $t->assign('user_arr', $user_arr);
      
	  	$def_adv_arr = array();
        $sql = "SELECT def_adv_id,def_adv_name FROM  tbl_lgl_def_adv ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $def_adv_arr['def_adv_id'][] = $row['def_adv_id'];
            $def_adv_arr['def_adv_name'][] = $row['def_adv_name'];
        }
        $t->assign('def_adv_arr', $def_adv_arr);

	    $guarantor_arr = array();
        $sql = "SELECT guarantor_id,guarantor_name FROM tbl_lgl_guarantor ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $guarantor_arr['guarantor_id'][] = $row['guarantor_id'];
            $guarantor_arr['guarantor_name'][] = $row['guarantor_name'];
        }
        $t->assign('guarantor_arr', $guarantor_arr);
      
	  	$bank_arr = array();
        $sql = "SELECT BankID,Bankname FROM tbl_lgl_banks ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $bank_arr['BankID'][] = $row['BankID'];
            $bank_arr['Bankname'][] = $row['Bankname'];
        }
        $t->assign('bank_arr', $bank_arr);
   }
    public function destroy($customer_id){
        $del_id_val = $customer_id;
        $this->DeleteRecord($this->TableName, $this->TableID, $del_id_val);
    }
    public function store(){
        $customer_id = $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
        header("Location: lgl_customer.php");
    }
    public function update(){
		//echo "here";
		//echo $_POST['txtlast_pay_date'];
		
        $customer_id = $this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
        header("Location: lgl_customer.php");
    }
    public function index(){

        global $t;
		$sql= "	SELECT * from tbl_lgl_customer
				LEFT JOIN tbl_lgl_branch on tbl_lgl_customer.main_branch_id = tbl_lgl_branch.branch_id
				LEFT JOIN tbl_lgl_company on tbl_lgl_customer.company_id = tbl_lgl_company.company_id";

				if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort'];	else $reqsort='customer_id';
				if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
				if ($reqord == "asc") $reqord = "asc"; elseif ($reqord == "desc") $reqord = "asc";	 
				//---------------------------------Paging--------------------------------------------------
				if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
				if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
				$t->assign('reqord',$reqord);
				$t->assign('perpage',$perpage);
				
				//-------------------------------end  Paging------------------------------------------------------
				//$tmpblnWhere = 1;
					if ( isset( $_REQUEST['txtkeyword'] ) ){
						$reqkeyword = $_REQUEST['txtkeyword'] ;
						$t->assign ( 'reqkeyword', $reqkeyword);
						if (strcmp($reqkeyword,"")<>0){
							if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
							$sql = $sql . "tbl_lgl_customer.customer_name LIKE '%$reqkeyword%' ";
							$PageString = $PageString  .'&txtkeyword='.$reqkeyword;
						}
					}
					if ( isset( $_REQUEST['txtkeyword4'] ) ){
						$reqkeyword4 = $_REQUEST['txtkeyword4'] ;
						$t->assign ( 'reqkeyword4', $reqkeyword4);
						if (strcmp($reqkeyword4,"")<>0){
							if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
							$sql = $sql . "tbl_lgl_company.company_id = '$reqkeyword4' ";
							$PageString = $PageString  .'&txtkeyword4='.$reqkeyword4;
						}
					}
					if ( isset( $_REQUEST['txtkeyword5'] ) ){
						$reqkeyword5 = $_REQUEST['txtkeyword5'] ;
						$t->assign ( 'reqkeyword5', $reqkeyword5);
						if (strcmp($reqkeyword5,"")<>0){
							if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
							$sql = $sql . "tbl_lgl_branch.branch_id = '$reqkeyword5' ";
							$PageString = $PageString  .'&txtkeyword5='.$reqkeyword5;
						}
					}
				
        
if (strcmp($reqsort,"")<>0)  $sql = $sql . ' order by ' . $reqsort ;
$sql = $sql . ' ' . $reqord ;

//echo $sql;
// ---------------- call paging function---------------------------------------------------
$strfnPagingSql = $sql; include 'includes/callpaging.php'; 
//----------------------------------------------------------------------------------- 
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        $cnt = 1;

       //echo $sql;
        foreach ($temp as $index => $row) {
            $row["cnt"] = $cnt++;
			$row["status"]	= fngetValue("tbl_lgl_status_change","status","customer_id='".$row["customer_id"]."'");
			$row["status_name"]	= fngetValue("tbl_lgl_status","status_name","status_id='".$row["status"]."'");
			$row["company_det"]	= fngetValue("tbl_lgl_company","company_name","company_id='".$row["company_id"]."'");
			$row["branch_det"]	= fngetValue("tbl_lgl_branch","branch_name","branch_id='".$row["main_branch_id"]."'");
			$customerResult[] = $row;
        }

        $branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$reqkeyword4' order by `branch_name` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);

		$custmor_arr = array();
        $sql = "SELECT customer_name,customer_id FROM tbl_lgl_customer where main_branch_id='$reqkeyword5' and company_id='$reqkeyword4' order by `customer_name`   asc";
//        echo $sql;
		$temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $custmor_arr['customer_id'][] = $row['customer_id'];
            $custmor_arr['customer_name'][] = $row['customer_name'];
        }
        $t->assign('custmor_arr', $custmor_arr);
		
        $t->assign('customer', $customerResult);
        $t->display('master_lookups/lgl_customer/customer_list.htm');

    }
    public function customerfullinfo()
    {
        global $t;
        if ($_GET['customer_id'] != "") {
			
		$sql= "	SELECT * from tbl_lgl_customer
				LEFT JOIN tbl_lgl_branch on tbl_lgl_customer.main_branch_id = tbl_lgl_branch.branch_id
				LEFT JOIN tbl_lgl_company on tbl_lgl_customer.company_id = tbl_lgl_company.company_id
				WHERE customer_id = " . $_GET['customer_id'];
			
            $temp = $this->db_pdo->prepare($sql);
            $temp->execute();
            foreach ($temp as $index => $row) {
            	$row['customer_name'] = $row['customer_name']. ' ( ' . $row['chit_group_no']. ' | ' . $row['ticket_no'] . ' ) ' ;
				$row["case_no"]		= fngetValue("tbl_lgl_case_entry","case_no","customer_id='".$row["customer_id"]."'");
				$row["case_category_id"]	= fngetValue("tbl_lgl_case_entry","case_category_id","customer_id='".$row["customer_id"]."'");
				$row["category_name"]	= fngetValue("tbl_lgl_customer_report_category","category_name","category_id='".$row["case_category_id"]."'");
				$row["status"]	= fngetValue("tbl_lgl_status_change","status","customer_id='".$row["customer_id"]."'");
				$row["status_name"]	= fngetValue("tbl_lgl_status","status_name","status_id='".$row["status"]."'");
				$data[] = $row;
            }
            // Legal Notice Entry List
            $legalnoticedata = array();
            $legalnoticesql = "select * from tbl_lgl_legal_notice where `customer_id` = " . $_GET['customer_id']." and guarantor_id IS NULL";
			$legalnoticetemp = $this->db_pdo->prepare($legalnoticesql);
            $legalnoticetemp->execute();
            foreach ($legalnoticetemp as $index => $legalnoticerow) {
				
				$sql_guarantor= "SELECT * from tbl_lgl_legal_notice where customer_id=".$_GET['customer_id']." and guarantor_id IS NOT NULL ";
				$temp = $this->db_pdo->prepare($sql_guarantor);
				$temp->execute();
				$guarantorResult=array();
				foreach ($temp as $index => $row_guarantor) {
					$row_guarantor['legal_notice_date'] 	= sqldateout($row_guarantor['legal_notice_date']);
					$row_guarantor['send_on'] 				= sqldateout($row_guarantor['send_on']);
					$row_guarantor['received_on'] 			= sqldateout($row_guarantor['received_on']);
					$row_guarantor['ack_received_on']		= sqldateout($row_guarantor['ack_received_on']);
					$row_guarantor["guarantor_name"]		=fngetvalue("tbl_lgl_guarantor","guarantor_name","guarantor_id=".$row_guarantor["guarantor_id"]."");
					$guarantorResult[] = $row_guarantor;
				}
				
				$legalnoticerow['guarantor_details'] = $guarantorResult;
				
				$legalnoticerow['legal_notice_date'] 	= sqldateout($legalnoticerow['legal_notice_date']);
				$legalnoticerow['send_on'] 				= sqldateout($legalnoticerow['send_on']);
				$legalnoticerow['received_on'] 			= sqldateout($legalnoticerow['received_on']);
				$legalnoticerow['ack_received_on']		= sqldateout($legalnoticerow['ack_received_on']);
                $legalnoticedata[] = $legalnoticerow;
            }
			
			
            // Guarantor List
            $guarantordata = array();
            $guarantorsql = "select * from tbl_lgl_guarantor where `customer_id` = " . $_GET['customer_id']." ";
			$guarantortemp = $this->db_pdo->prepare($guarantorsql);
            $guarantortemp->execute();
            foreach ($guarantortemp as $index => $guarantorrow) {
                $guarantorrow['guarantor_dob'] = sqldateout($guarantorrow['guarantor_dob']);
                $guarantordata[] = $guarantorrow;
            }
            
			
			// Cheque Return List
            $cheque_returndata = array();
            $cheque_returnsql = "select * from tbl_lgl_cheque_return where `customer_id` = " . $_GET['customer_id']." ";
			$cheque_returntemp = $this->db_pdo->prepare($cheque_returnsql);
            $cheque_returntemp->execute();
            foreach ($cheque_returntemp as $index => $cheque_returnrow) {
				$cheque_returnrow["drawn_onBankname"]			= fngetValue("tbl_lgl_banks","Bankname","BankID='".$cheque_returnrow["drawn_on"]."'");
				$cheque_returnrow["ch_deposited_atBankname"]	= fngetValue("tbl_lgl_banks","Bankname","BankID='".$cheque_returnrow["ch_deposited_at"]."'");
				$cheque_returnrow['cheque_date'] 				= sqldateout($cheque_returnrow['cheque_date']);
				$cheque_returnrow['ch_received_date'] 			= sqldateout($cheque_returnrow['ch_received_date']);
				$cheque_returnrow['ch_deposited_on'] 			= sqldateout($cheque_returnrow['ch_deposited_on']);
				$cheque_returnrow['cheque_return_date']			= sqldateout($cheque_returnrow['cheque_return_date']);
				$cheque_returnrow["comp_bank"]					= fngetValue("tbl_lgl_banks","Bankname","BankID='".$cheque_returnrow["comp_bank"]."'");
				$cheque_returndata[] = $cheque_returnrow;
            }
			

			// Payment List
            $paymentdata = array();
            $paymentsql = "select * from tbl_lgl_payment where `customer_id` = " . $_GET['customer_id']." ";
			$paymenttemp = $this->db_pdo->prepare($paymentsql);
            $paymenttemp->execute();
            foreach ($paymenttemp as $index => $paymentrow) {
				$paymentrow["pay_cheque_bankBankname"]			= fngetValue("tbl_lgl_banks","Bankname","BankID='".$paymentrow["pay_cheque_bank"]."'");
				$paymentrow['pay_date']			 				= sqldateout($paymentrow['pay_date']);
				$paymentrow['pay_cheque_date']			 		= sqldateout($paymentrow['pay_cheque_date']);
				$paymentdata[] = $paymentrow;
            }
			
			// Status Change List
            $status_changedata = array();
            $status_changesql = "select * from tbl_lgl_status_change where `customer_id` = " . $_GET['customer_id']." ";
			$status_changetemp = $this->db_pdo->prepare($status_changesql);
            $status_changetemp->execute();
            foreach ($status_changetemp as $index => $status_changerow) {
				$status_changerow["status_name"]	= fngetValue("tbl_lgl_status","status_name","status_id='".$status_changerow["status"]."'");
				$status_changerow['status_date']	= sqldateout($status_changerow['status_date']);
				$status_changedata[] = $status_changerow;
            }
			
			// Case Entry List
            $case_entrydata = array();
            $case_entrysql = "select * from tbl_lgl_case_entry where `customer_id` = " . $_GET['customer_id']." ";
			$case_entrytemp = $this->db_pdo->prepare($case_entrysql);
            $case_entrytemp->execute();
            foreach ($case_entrytemp as $index => $case_entryrow) {
				$case_entryrow["case_category"]		= fngetValue("tbl_lgl_customer_report_category","category_name","category_id='".$case_entryrow["case_category_id"]."'");
				$case_entryrow["court"]				= fngetValue("tbl_lgl_court","court_name","court_id='".$case_entryrow["court"]."'");
				$case_entryrow["advocate"]			= fngetValue("tbl_lgl_advocate","advocate_name","advocate_id='".$case_entryrow["advocate"]."'");
				$case_entryrow["opp_advocate"]		= fngetValue("tbl_lgl_def_adv","def_adv_name","def_adv_id='".$case_entryrow["opp_advocate"]."'");
				$case_entryrow["complainant"]		= fngetValue("tbl_lgl_complainant","complainant_name","complainant_id='".$case_entryrow["complainant"]."'");
				$case_entryrow['case_date']			= sqldateout($case_entryrow['case_date']);
				$case_entrydata[] = $case_entryrow;
            }
			
			// Internal Note List
            $internal_notedata = array();
            $internal_notesql = "select * from tbl_lgl_internal_note where `customer_id` = " . $_GET['customer_id']." ";
			$internal_notetemp = $this->db_pdo->prepare($internal_notesql);
            $internal_notetemp->execute();
            foreach ($internal_notetemp as $index => $internal_noterow) {
				$internal_noterow['note_date'] = sqldateout($internal_noterow['note_date']);
				$internal_noterow['nxt_hearing_date'] = sqldateout($internal_noterow['nxt_hearing_date']);
				$internal_notedata[] = $internal_noterow;
            }
			
			
			

			// 138 Report List
            $customer_138_reportdata = array();
			$customer_138_reportsql=" SELECT tbl_lgl_report_draft.report_draft_id as draft_report_draft_id,report_details.report_draft_id,
									  tbl_lgl_report_draft.report_name,report_details.customer_id ,report_details.customer_report_id
									  FROM tbl_lgl_report_draft
									  LEFT JOIN tbl_lgl_customer_report_category on tbl_lgl_report_draft.category_id = tbl_lgl_customer_report_category.category_id
									  LEFT JOIN (select * from tbl_lgl_customer_report where customer_id= " . $_GET['customer_id'].") as report_details
									  on (tbl_lgl_report_draft.report_draft_id = report_details.report_draft_id)
									  WHERE tbl_lgl_report_draft.category_id='3' 
									  ORDER BY tbl_lgl_report_draft.report_name asc"; 
									  
			//echo $customer_138_reportsql;						  
  			$customer_138_reporttemp = $this->db_pdo->prepare($customer_138_reportsql);
            $customer_138_reporttemp->execute();
            foreach ($customer_138_reporttemp as $index => $customer_138_reportrow) {
                $customer_138_reportrow['report_date'] = sqldateout(fngetvalue("tbl_lgl_customer_report","report_date","customer_report_id='" . $customer_138_reportrow['customer_report_id']." '"));
				$customer_138_reportdata[] = $customer_138_reportrow;
            }

			// Arbitration Report List
            $customer_arb_reportdata = array();
			
            $customer_arb_reportsql = "SELECT tbl_lgl_report_draft.report_draft_id as draft_report_draft_id,report_details.report_draft_id,
									  tbl_lgl_report_draft.report_name,report_details.customer_id ,report_details.customer_report_id
									  FROM tbl_lgl_report_draft
									  LEFT JOIN tbl_lgl_customer_report_category on tbl_lgl_report_draft.category_id = tbl_lgl_customer_report_category.category_id
									  LEFT JOIN (select * from tbl_lgl_customer_report where customer_id= " . $_GET['customer_id'].") as report_details
									  on (tbl_lgl_report_draft.report_draft_id = report_details.report_draft_id)
									  WHERE tbl_lgl_report_draft.category_id='4' 
									  ORDER BY tbl_lgl_report_draft.report_name asc"; 
			$customer_arb_reporttemp = $this->db_pdo->prepare($customer_arb_reportsql);
            $customer_arb_reporttemp->execute();
            foreach ($customer_arb_reporttemp as $index => $customer_arb_reportrow) {
                $customer_arb_reportrow['report_date'] = sqldateout(fngetvalue("tbl_lgl_customer_report","report_date","customer_report_id='" . $customer_arb_reportrow['customer_report_id']." '"));
				$customer_arb_reportdata[] = $customer_arb_reportrow;
            }

			// Legal Notice Report List
            $customer_lgl_reportdata = array();
			
            $customer_lgl_reportsql = "SELECT tbl_lgl_report_draft.report_draft_id as draft_report_draft_id,report_details.report_draft_id,
									  tbl_lgl_report_draft.report_name,report_details.customer_id ,report_details.customer_report_id
									  FROM tbl_lgl_report_draft
									  LEFT JOIN tbl_lgl_customer_report_category on tbl_lgl_report_draft.category_id = tbl_lgl_customer_report_category.category_id
									  LEFT JOIN (select * from tbl_lgl_customer_report where customer_id= " . $_GET['customer_id'].") as report_details
									  on (tbl_lgl_report_draft.report_draft_id = report_details.report_draft_id)
									  WHERE tbl_lgl_report_draft.category_id='5' 
									  ORDER BY tbl_lgl_report_draft.report_name asc"; 
			$customer_lgl_reporttemp = $this->db_pdo->prepare($customer_lgl_reportsql);
            $customer_lgl_reporttemp->execute();
            foreach ($customer_lgl_reporttemp as $index => $customer_lgl_reportrow) {
                $customer_lgl_reportrow['report_date'] = sqldateout(fngetvalue("tbl_lgl_customer_report","report_date","customer_report_id='" . $customer_lgl_reportrow['customer_report_id']." '"));
				$customer_lgl_reportdata[] = $customer_lgl_reportrow;
            }

            $t->assign('data', $data);
            $t->assign('legalnoticedata', $legalnoticedata);
            $t->assign('guarantordata', $guarantordata);
            $t->assign('cheque_returndata', $cheque_returndata);
            $t->assign('periodical_updationdata', $periodical_updationdata);
            $t->assign('paymentdata', $paymentdata);
            $t->assign('status_changedata', $status_changedata);
            $t->assign('internal_notedata', $internal_notedata);
            $t->assign('customer_138_reportdata', $customer_138_reportdata);
            $t->assign('customer_arb_reportdata', $customer_arb_reportdata);
            $t->assign('customer_lgl_reportdata', $customer_lgl_reportdata);
            $t->assign('case_entrydata', $case_entrydata);

            $t->assign('customer_id', $_GET['customer_id']);
			
     }
        $t->display('master_lookups/lgl_customer/customer_details.htm');
        die();
    }
}

?>
