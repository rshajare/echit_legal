<?
include_once ("classes/classdatabase.php");

class customer_report_category extends Database
{
	private $TableName ;
	private $TableID ;
	public $TableIDVal ;

	
	private $SessionLanguage ;
	
	private $TableFieldArray;
	public $TableFieldValArray;
	public $printreport;

    public function __construct()
    {
        global $t;
        $this->TableName = 'tbl_lgl_customer_report_category';
        $this->TableID = 'category_id';
        $this->TableFieldArray = array(
            array('category_name', 1, 1, PDO::PARAM_INT),
            array('main_category_id', 1, 1, PDO::PARAM_STR)
        );

        $this->SessionLanguage = $_SESSION['opt_lang'];
        $this->DBConnect();


 
   
 }
    public function Addcustomer_report_category()
    {
		global $t;
		  

        $t->display('master_lookups/lgl_customer_report_category/rep_category_add.htm');
		//$t->display('lgl_reports.htm');	
    }

    public function Editcustomer_report_category()
    {global $t;
        $main = array();

        $sql = "SELECT * FROM ".$this->TableName." WHERE ".$this->TableID."='".$this->TableIDVal."' ";
		$temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $data[] = $row;
        }
        $t->assign('data', $data);

        $t->display('master_lookups/lgl_customer_report_category/rep_category_edit.htm');

    }

    public function destroy($customer_report_category_id)
    {
        $this->DeleteRecord($this->TableName, $this->TableID, $customer_report_category_id);
    }


    public function store()
    {
        $customer_report_category_id = $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
    	header("Location: lgl_report_category.php");
    }

    public function update()
    {
		$customer_report_category_id = $this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
    	header("Location: lgl_report_category.php");
    }
 
    public function index()
    {

        global $t;
		$sql= "	SELECT * from tbl_lgl_customer_report_category  ";
		
        $tmpblnWhere = 0;
        		if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort']; else $reqsort = 'category_id';
				if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
				if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";	 
				//---------------------------------Paging--------------------------------------------------
				if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
				if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
				$t->assign('reqord',$reqord);
				$t->assign('perpage',$perpage);
				
				//-------------------------------end  Paging------------------------------------------------------


        $tmpblnWhere = 1;
        			if ( isset( $_REQUEST['txtkeyword'] ) ){
                    $reqkeyword = $_REQUEST['txtkeyword'] ;
                    $t->assign ( 'reqkeyword', $reqkeyword);
                    if (strcmp($reqkeyword,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "category_name like '%$reqkeyword%' ";
                        $PageString = $PageString  .'&txtkeyword='.$reqkeyword;
                    }
                }
        if (strcmp($reqsort, "") <> 0) $sql = $sql . ' order by ' . $reqsort;
        $sql = $sql . ' ' . $reqord;


// ---------------- call paging function---------------------------------------------------
		$strfnPagingSql = $sql; include 'includes/callpaging.php'; 
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        $cnt = 1;

     // echo $sql;
        foreach ($temp as $index => $category_row) {
				$category_row["cnt"] = $cnt++;
				$category_data[] = $category_row;
        }
			$t->assign('category_data', $category_data);
       		$t->display('master_lookups/lgl_customer_report_category/rep_category_list.htm');

    }

}
?>
