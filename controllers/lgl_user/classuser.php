<?
include_once ("classes/classdatabase.php");

class user extends Database
{
    private $TableName;
    private $TableID;
    public $TableIDVal;

 
    private $SessionLanguage;
    private $TableFieldArray;
    public $TableFieldValArray;


    public function __construct()
    {
        global $t;
        $this->TableName = 'tbl_lgl_user';
        $this->TableID = 'user_id';
        $this->TableFieldArray = array(
            array('login', 1, 1, PDO::PARAM_STR),
            array('password', 1, 1, PDO::PARAM_STR),
            array('mr_mrs', 1, 1, PDO::PARAM_STR),
			
			array('user_name', 1, 1, PDO::PARAM_STR),
            array('birth_date', 1, 1, PDO::PARAM_STR),
            array('company_id', 1, 1, PDO::PARAM_INT),

            array('user_address', 1, 1, PDO::PARAM_STR),
            array('office_address', 1, 1, PDO::PARAM_STR),
            array('city', 1, 1, PDO::PARAM_STR),

            array('zip_code', 1, 1, PDO::PARAM_STR),
            array('country', 1, 1, PDO::PARAM_STR),
            array('state', 1, 1, PDO::PARAM_STR),

            array('telephone_no', 1, 1, PDO::PARAM_STR),
            array('mobile_phone_no', 1, 1, PDO::PARAM_STR),
            array('add_info', 1, 1, PDO::PARAM_STR),

			array('main_branch_id', 1, 1, PDO::PARAM_INT)
        );
        $this->SessionLanguage = $_SESSION['opt_lang'];
        $this->DBConnect();


        $comp_arr = array();
        $sql = "SELECT company_name,company_id FROM tbl_lgl_company order by `company_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $comp_arr['company_id'][] = $row['company_id'];
            $comp_arr['company_name'][] = $row['company_name'];
        }
        $t->assign('comp_arr', $comp_arr);

        $branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch order by `branch_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);

    }


    public function Adduser()
    {
        global $t;
        $t->display('master_lookups/lgl_user/user_add.htm');
    }

    public function Edituser()
    {
        global $t;
        $main = array();
        $user_id = $_GET['user_id'];
        $sql = "SELECT * FROM tbl_lgl_user where user_id=$user_id";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $data[] = $row;
        }
        $t->assign('data', $data);

        $t->display('master_lookups/lgl_user/user_edit.htm');

    }

    public function destroy($user_id)
    {
        $del_id_val = $user_id;
        $this->DeleteRecord($this->TableName, $this->TableID, $del_id_val);
    }


    public function store()
    {
        $user_id = $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
        header("Location: lgl_user.php");
    }

    public function update()
    {
        $user_id = $this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
        header("Location: lgl_user.php");
    }

    public function index()
    {

        global $t;
		$sql= "	SELECT * from tbl_lgl_user
				LEFT JOIN tbl_lgl_branch on tbl_lgl_user.main_branch_id = tbl_lgl_branch.branch_id
				LEFT JOIN tbl_lgl_company on tbl_lgl_user.company_id = tbl_lgl_company.company_id";
		
        //$tmpblnWhere = 0;
        		if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort']; else $reqsort = 'user_id';
				if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
				if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";	 
				//---------------------------------Paging--------------------------------------------------
				if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
				if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
				$t->assign('reqord',$reqord);
				$t->assign('perpage',$perpage);
				
				//-------------------------------end  Paging------------------------------------------------------
       // $tmpblnWhere = 1;
        			if ( isset( $_REQUEST['txtkeyword'] ) ){
                    $reqkeyword = $_REQUEST['txtkeyword'] ;
                    $t->assign ( 'reqkeyword', $reqkeyword);
                    if (strcmp($reqkeyword,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_user.user_name like '%$reqkeyword%' ";
                        $PageString = $PageString  .'&txtkeyword='.$reqkeyword;
                    }
                }
        			if ( isset( $_REQUEST['txtkeyword2'] ) ){
                    $reqkeyword2 = $_REQUEST['txtkeyword2'] ;
                    $t->assign ( 'reqkeyword2', $reqkeyword2);
                    if (strcmp($reqkeyword2,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_user.company_id like '%$reqkeyword2%' ";
                        $PageString = $PageString  .'&txtkeyword2='.$reqkeyword2;
                    }
                }
        			if ( isset( $_REQUEST['txtkeyword3'] ) ){
                    $reqkeyword3 = $_REQUEST['txtkeyword3'] ;
                    $t->assign ( 'reqkeyword3', $reqkeyword3);
                    if (strcmp($reqkeyword3,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_user.mobile_phone_no like '%$reqkeyword3%' ";
                        $PageString = $PageString  .'&txtkeyword3='.$reqkeyword3;
                    }
                }
        			if ( isset( $_REQUEST['txtkeyword5'] ) ){
                    $reqkeyword5 = $_REQUEST['txtkeyword5'] ;
                    $t->assign ( 'reqkeyword5', $reqkeyword5);
                    if (strcmp($reqkeyword5,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_branch.branch_id like '%$reqkeyword5%' ";
                        $PageString = $PageString  .'&txtkeyword5='.$reqkeyword5;
                    }
                }
				
        
        if (strcmp($reqsort, "") <> 0) $sql = $sql . ' order by ' . $reqsort;
        $sql = $sql . ' ' . $reqord;


// ---------------- call paging function---------------------------------------------------
        $strfnPagingSql = $sql; include 'includes/callpaging.php';
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        $cnt = 1;

     // echo $sql;
        foreach ($temp as $index => $row) {
            $row["cnt"] = $cnt++;
            $userResult[] = $row;
        }
        $t->assign('userdet', $userResult);
        $t->display('master_lookups/lgl_user/user_list.htm');

    }

}

?>
