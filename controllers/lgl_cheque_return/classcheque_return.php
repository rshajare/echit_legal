<?
include_once ("classes/classdatabase.php");

class cheque_return extends Database
{
    private $TableName;
    private $TableID;
    public $TableIDVal;

 
    private $SessionLanguage;
    private $TableFieldArray;
    public $TableFieldValArray;


    public function __construct()
    {
        global $t;
        $this->TableName = 'tbl_lgl_cheque_return';
        $this->TableID = 'cheque_return_id';
        $this->TableFieldArray = array(
            array('customer_id', 1, 1, PDO::PARAM_INT),
            array('cheque_no', 1, 1, PDO::PARAM_INT),
            array('cheque_date', 1, 1, PDO::PARAM_STR),
		    array('cheque_return_date', 1, 1, PDO::PARAM_STR),
            array('cheque_return_reason', 1, 1, PDO::PARAM_STR),
            array('main_branch_id', 1, 1, PDO::PARAM_INT),
			array('company_id', 1, 1, PDO::PARAM_INT),
            array('drawn_on', 1, 1, PDO::PARAM_INT),
		    array('ch_amount', 1, 1, PDO::PARAM_INT),
            array('ch_deposited_on', 1, 1, PDO::PARAM_STR),
            array('ch_deposited_at', 1, 1, PDO::PARAM_INT),
			array('ch_received_date', 1, 1, PDO::PARAM_STR),
            array('comp_bank', 1, 1, PDO::PARAM_INT),
			array('comp_bank_branch', 1, 1, PDO::PARAM_STR),
			array('drawn_on_branch', 1, 1, PDO::PARAM_STR),
            array('print_date', 1, 1, PDO::PARAM_STR)
        );
        $this->SessionLanguage = $_SESSION['opt_lang'];
        $this->DBConnect();

        $comp_arr = array();
        $sql = "SELECT company_name,company_id FROM tbl_lgl_company order by `company_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $comp_arr['company_id'][] = $row['company_id'];
            $comp_arr['company_name'][] = $row['company_name'];
        }
        $t->assign('comp_arr', $comp_arr);

		

	//	print_r($custmor_arr);
		

    }


    public function Addcheque_return()
    {
        global $t;
		$customer_id = $_GET['customer_id'];	
        $t->assign('customer_id', $customer_id);
		
		$customer_name 		= fnGetValue("tbl_lgl_customer","customer_name","customer_id='".$customer_id."'");
        $t->assign('customer_name', $customer_name);
		
		if($customer_id!=""){
            $reqcompany_id 		= fnGetValue("tbl_lgl_customer","company_id","customer_id='".$customer_id."'");
			$t->assign('reqcompany_id', $reqcompany_id);
            $reqmain_branch_id 		= fnGetValue("tbl_lgl_customer","main_branch_id","customer_id='".$customer_id."'");
			$t->assign('reqmain_branch_id', $reqmain_branch_id);
			
		$branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$reqcompany_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);

        $customer_arr = array();
        $sql = "SELECT customer_name,customer_id,chit_group_no,ticket_no FROM tbl_lgl_customer where company_id='$reqcompany_id' and main_branch_id='$reqmain_branch_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $customer_arr['customer_id'][] = $row['customer_id'];
            $customer_arr['customer_name'][] = $row['customer_name']. ' ( ' . $row['chit_group_no']. ' | ' . $row['ticket_no'] . ' ) ' ;
        }
        $t->assign('customer_arr', $customer_arr);

	  	$bank_arr = array();
        $sql = "SELECT BankID,Bankname FROM tbl_lgl_banks where main_branch_id='$reqmain_branch_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $bank_arr['BankID'][] = $row['BankID'];
            $bank_arr['Bankname'][] = $row['Bankname'];
        }
        $t->assign('bank_arr', $bank_arr);
		}
		
        $t->display('master_lookups/lgl_cheque_return/cheque_return_add.htm');
    }

    public function Editcheque_return()
    {
        global $t;
        $main = array();
        $cheque_return_id = $_GET['cheque_return_id'];
        $sql = "SELECT * FROM tbl_lgl_cheque_return where cheque_return_id=$cheque_return_id";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $data[] = $row;
			$row["cheque_return_date"]  =sqldatein($row["cheque_return_date"]);
		    $customer_name 		= fnGetValue("tbl_lgl_customer","customer_name","customer_id='".$row["customer_id"]."'");
			$this->all_lk($row["company_id"],$row["main_branch_id"]);
		}
		//print_r($data);
        $t->assign('customer_name', $customer_name);
        $t->assign('data', $data);
        $t->display('master_lookups/lgl_cheque_return/cheque_return_edit.htm');

    }
	public function all_lk($req_company_id,$req_branch_id){
		global $t;
		$company_id=$req_company_id;
		$branch_id=$req_branch_id;
		$customer_id=$req_customer_id;
		
		
		$branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$company_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);

        $customer_arr = array();
        $sql = "SELECT customer_name,customer_id,chit_group_no,ticket_no FROM tbl_lgl_customer where main_branch_id='$branch_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $customer_arr['customer_id'][] = $row['customer_id'];
            $customer_arr['customer_name'][] = $row['customer_name']. ' ( ' . $row['chit_group_no']. ' | ' . $row['ticket_no'] . ' ) ' ;
        }
        $t->assign('customer_arr', $customer_arr);

	  	$bank_arr = array();
        $sql = "SELECT BankID,Bankname FROM tbl_lgl_banks where main_branch_id='$branch_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $bank_arr['BankID'][] = $row['BankID'];
            $bank_arr['Bankname'][] = $row['Bankname'];
        }
        $t->assign('bank_arr', $bank_arr);

  }

    public function destroy($cheque_return_id)
    {
        $del_id_val = $cheque_return_id;
        $this->DeleteRecord($this->TableName, $this->TableID, $del_id_val);
    }


    public function store()
    {
        $cheque_return_id = $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
        header("Location: lgl_cheque_return.php");
    }

    public function update()
    {
        $cheque_return_id = $this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
        header("Location: lgl_cheque_return.php");
    }

    public function index()
    {

        global $t;
		$sql= "	SELECT * from tbl_lgl_cheque_return 
				LEFT JOIN tbl_lgl_customer on tbl_lgl_cheque_return.customer_id = tbl_lgl_customer.customer_id
				LEFT JOIN tbl_lgl_branch on tbl_lgl_cheque_return.main_branch_id = tbl_lgl_branch.branch_id
				LEFT JOIN tbl_lgl_company on tbl_lgl_cheque_return.company_id = tbl_lgl_company.company_id";
		
				//$tmpblnWhere = 0;
        		if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort']; else $reqsort = 'cheque_return_id';
				if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
				if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";	 
				//---------------------------------Paging--------------------------------------------------
				if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
				if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
				$t->assign('reqord',$reqord);
				$t->assign('perpage',$perpage);
				
				//-------------------------------end  Paging------------------------------------------------------
				//$tmpblnWhere = 1;
        			if ( isset( $_REQUEST['txtkeyword'] ) ){
                    $reqkeyword = $_REQUEST['txtkeyword'] ;
                    $t->assign ( 'reqkeyword', $reqkeyword);
                    if (strcmp($reqkeyword,"")<>0){
                        if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_customer.customer_name LIKE '%$reqkeyword%' ";
                        $PageString = $PageString  .'&txtkeyword='.$reqkeyword;
                    }
                }
        			if ( isset( $_REQUEST['txtkeyword2'] ) ){
                    $reqkeyword2 = $_REQUEST['txtkeyword2'] ;
                    $t->assign ( 'reqkeyword2', $reqkeyword2);
                    if (strcmp($reqkeyword2,"")<>0){
                        if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_cheque_return.cheque_no = '$reqkeyword2' ";
                        $PageString = $PageString  .'&txtkeyword2='.$reqkeyword2;
                    }
                }
        			if ( isset( $_REQUEST['txtkeyword4'] ) ){
                    $reqkeyword4 = $_REQUEST['txtkeyword4'] ;
                    $t->assign ( 'reqkeyword4', $reqkeyword4);
                    if (strcmp($reqkeyword4,"")<>0){
                        if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_company.company_id = '$reqkeyword4' ";
                        $PageString = $PageString  .'&txtkeyword4='.$reqkeyword4;
                    }
                }
        			if ( isset( $_REQUEST['txtkeyword5'] ) ){
                    $reqkeyword5 = $_REQUEST['txtkeyword5'] ;
                    $t->assign ( 'reqkeyword5', $reqkeyword5);
                    if (strcmp($reqkeyword5,"")<>0){
                        if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_branch.branch_id = '$reqkeyword5' ";
                        $PageString = $PageString  .'&txtkeyword5='.$reqkeyword5;
                    }
                }
				
        
        if (strcmp($reqsort, "") <> 0) $sql = $sql . ' order by ' . $reqsort;
        $sql = $sql . ' ' . $reqord;


// ---------------- call paging function---------------------------------------------------
		$strfnPagingSql = $sql; include 'includes/callpaging.php'; 
		$temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        $cnt = 1;

        //echo $sql;
        foreach ($temp as $index => $row) {
            $row["cnt"] = $cnt++;
			$cheque_returnResult[] = $row;
        }
        $branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$reqkeyword4' order by `branch_name` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);

		$custmor_arr = array();
        $sql = "SELECT customer_name,customer_id FROM tbl_lgl_customer where main_branch_id='$reqkeyword5' and company_id='$reqkeyword4' order by `customer_name`   asc";
//        echo $sql;
		$temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $custmor_arr['customer_id'][] = $row['customer_id'];
            $custmor_arr['customer_name'][] = $row['customer_name'];
        }
        $t->assign('custmor_arr', $custmor_arr);
		
        $t->assign('cheque_return', $cheque_returnResult);
        $t->display('master_lookups/lgl_cheque_return/cheque_return_list.htm');

    }

}

?>
