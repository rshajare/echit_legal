<?
include_once ("classes/classdatabase.php");

class report_draft extends Database
{
	private $TableName ;
	private $TableID ;
	public $TableIDVal ;

	private $SubTableName ;
	private $SubTableID ;
	public $SubTableIDVal ;
	
	private $SessionLanguage ;
	
	private $TableFieldArray;
	public $TableFieldValArray;
	private $SubTableFieldArray;
	public $SubTableFieldValArray;

    public function __construct()
    {
        global $t;
        $this->TableName = 'tbl_lgl_report_draft';
        $this->TableID = 'report_draft_id';
        $this->TableFieldArray = array(
            array('report_name', 1, 1, PDO::PARAM_STR),
            array('report_draft', 1, 1, PDO::PARAM_STR),
            array('category_id', 1, 1, PDO::PARAM_INT)
        );
		

        $this->SessionLanguage = $_SESSION['opt_lang'];
        $this->DBConnect();


        $report_arr = array();
        $sql = "SELECT report_name,report_draft_id FROM tbl_lgl_report_draft order by `report_draft_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $report_arr['report_draft_id'][] = $row['report_draft_id'];
            $report_arr['report_name'][] = $row['report_name'];
        }
        $t->assign('report_arr', $report_arr);
		
        $category_arr = array();
        $sql = "SELECT category_name,category_id FROM tbl_lgl_customer_report_category order by `category_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $category_arr['category_id'][] = $row['category_id'];
            $category_arr['category_name'][] = $row['category_name'];
        }
        $t->assign('category_arr', $category_arr);
		
//print_r($report_arr);

    }


    public function Addreport_draft()
    {
        global $t;
        $t->display('master_lookups/lgl_report_draft/report_draft_add.htm');
    }

    public function Editreport_draft()
    {
        global $t;
        $main = array();
        $report_draft_id = $_GET['report_draft_id'];
        $sql = "SELECT * FROM tbl_lgl_report_draft where report_draft_id=$report_draft_id";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
		//echo $sql; 
        foreach ($temp as $index => $row) {
			$row[report_draft] = stripslashes($row[report_draft]);
            $data[] = $row;
        }
        $t->assign('data', $data);
        $t->display('master_lookups/lgl_report_draft/report_draft_edit.htm');

    }

    public function destroy($report_draft_id)
    {
        $del_id_val = $report_draft_id;
        $this->DeleteRecord($this->TableName, $this->TableID, $del_id_val);
    }


    public function store()
    {
		
        $report_draft_id = $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
        header("Location: lgl_report_draft.php");
    }

    public function update()
    {
		$this->TableFieldValArray[1][0]=$_POST["txtreport_name"];
		$this->TableFieldValArray[3][0]=$_POST["txtcategory_id"];
		
		$str=$_POST["txtreport_draft"];
		$a = htmlentities($str);
		$b = html_entity_decode($a);
		$this->TableFieldValArray[2][0]=$b;
		
        $report_draft_id = $this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
        header("Location: lgl_report_draft.php");
    }

    public function index()
    {

        global $t;
		$sql= "	SELECT * from tbl_lgl_report_draft 
				LEFT JOIN tbl_lgl_customer_report_category on tbl_lgl_report_draft.category_id = tbl_lgl_customer_report_category.category_id";
		
        //$tmpblnWhere = 0;
        		if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort']; else $reqsort = 'report_name';
				if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
				if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";	 
				//---------------------------------Paging--------------------------------------------------
				if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
				if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
				$t->assign('reqord',$reqord);
				$t->assign('perpage',$perpage);
				
				//-------------------------------end  Paging------------------------------------------------------
        //$tmpblnWhere = 1;
        			if ( isset( $_REQUEST['txtkeyword'] ) ){
                    $reqkeyword = $_REQUEST['txtkeyword'] ;
                    $t->assign ( 'reqkeyword', $reqkeyword);
                    if (strcmp($reqkeyword,"")<>0){
                        if ($tmpblnhaving==0){ $sql = $sql . " having "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
                        $sql = $sql . "tbl_lgl_report_draft.report_name LIKE '%$reqkeyword%' ";
                        $PageString = $PageString  .'&txtkeyword='.$reqkeyword;
                    }
                }
					if ( isset( $_REQUEST['txtkeyword1'] ) ){
						$reqkeyword1 = $_REQUEST['txtkeyword1'] ;
						$t->assign ( 'reqkeyword1', $reqkeyword1);
						if (strcmp($reqkeyword1,"")<>0){
							if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
							$sql = $sql . "tbl_lgl_customer_report_category.category_id = '$reqkeyword1' ";
							$PageString = $PageString  .'&txtkeyword1='.$reqkeyword1;
						}
					}
				
        
        if (strcmp($reqsort, "") <> 0) $sql = $sql . ' order by ' . $reqsort;
        $sql = $sql . ' ' . $reqord;


// ---------------- call paging function---------------------------------------------------
        $strfnPagingSql = $sql; include 'includes/callpaging.php';
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        $cnt = 1;

     // echo $sql;
        foreach ($temp as $index => $row) {
            $row["cnt"] = $cnt++;
           // $row["report_name"] = fnGetVAlue("tbl_report_draft_lk","report_name","report_draft_id='".$row["report_name"]."' ");
            $report_draftResult[] = $row;
        }
        $t->assign('report_draft', $report_draftResult);
        $t->display('master_lookups/lgl_report_draft/report_draft_list.htm');

    }

}

?>
