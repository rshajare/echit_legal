<?
include_once ("classes/classdatabase.php");

class case_entry extends Database
{
    private $TableName;
    private $TableID;
    public $TableIDVal;

 
    private $SessionLanguage;
    private $TableFieldArray;
    public $TableFieldValArray;


    public function __construct()
    {
        global $t;
        $this->TableName = 'tbl_lgl_case_entry';
        $this->TableID = 'case_id';
        $this->TableFieldArray = array(
            array('company_id', 1, 1, PDO::PARAM_INT),
            array('main_branch_id', 1, 1, PDO::PARAM_INT),
            array('customer_id', 1, 1, PDO::PARAM_INT),
			array('case_no', 1, 1, PDO::PARAM_STR),			
			array('court', 1, 1, PDO::PARAM_INT),
			array('judge', 1, 1, PDO::PARAM_INT),
			array('advocate', 1, 1, PDO::PARAM_INT),
			array('opp_advocate', 1, 1, PDO::PARAM_INT),
			array('complainant', 1, 1, PDO::PARAM_INT),
            array('case_date', 1, 1, PDO::PARAM_STR),
			array('case_category_id', 1, 1, PDO::PARAM_INT),
        );
        $this->SessionLanguage = $_SESSION['opt_lang'];
        $this->DBConnect();

        $comp_arr = array();
        $sql = "SELECT company_name,company_id FROM tbl_lgl_company order by `company_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $comp_arr['company_id'][] = $row['company_id'];
            $comp_arr['company_name'][] = $row['company_name'];
        }
        $t->assign('comp_arr', $comp_arr);

        $advocate_arr = array();
        $sql = "SELECT advocate_id,advocate_name FROM tbl_lgl_advocate order by `advocate_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $advocate_arr['advocate_id'][] = $row['advocate_id'];
            $advocate_arr['advocate_name'][] = $row['advocate_name'];
        }
        $t->assign('advocate_arr', $advocate_arr);
		
	  	$def_adv_arr = array();
        $sql = "SELECT def_adv_id,def_adv_name FROM  tbl_lgl_def_adv order by `def_adv_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $def_adv_arr['def_adv_id'][] = $row['def_adv_id'];
            $def_adv_arr['def_adv_name'][] = $row['def_adv_name'];
        }
        $t->assign('def_adv_arr', $def_adv_arr);
		
		
	    $court_arr = array();
        $sql = "SELECT court_id,court_name FROM tbl_lgl_court order by `court_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $court_arr['court_id'][] = $row['court_id'];
            $court_arr['court_name'][] = $row['court_name'];
        }
        $t->assign('court_arr', $court_arr);
		
	    $complainant_arr = array();
        $sql = "SELECT complainant_id,complainant_name FROM tbl_lgl_complainant order by `complainant_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $complainant_arr['complainant_id'][] = $row['complainant_id'];
            $complainant_arr['complainant_name'][] = $row['complainant_name'];
        }
        $t->assign('complainant_arr', $complainant_arr);

	    $category_arr = array();
        $sql = "SELECT category_id,category_name FROM tbl_lgl_customer_report_category order by `category_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $category_arr['category_id'][] = $row['category_id'];
            $category_arr['category_name'][] = $row['category_name'];
        }
        $t->assign('category_arr', $category_arr);
		
    }


    public function Addcase_entry()
    {
        global $t;
		$customer_id = $_GET['customer_id'];	
        $t->assign('customer_id', $customer_id);
		
		if($customer_id!=""){
            $reqcompany_id 		= fnGetValue("tbl_lgl_customer","company_id","customer_id='".$customer_id."'");
			$t->assign('reqcompany_id', $reqcompany_id);
            $reqmain_branch_id 		= fnGetValue("tbl_lgl_customer","main_branch_id","customer_id='".$customer_id."'");
			$t->assign('reqmain_branch_id', $reqmain_branch_id);
			
		$branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$reqcompany_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);

        $customer_arr = array();
        $sql = "SELECT customer_name,customer_id,chit_group_no,ticket_no FROM tbl_lgl_customer where company_id='$reqcompany_id' and main_branch_id='$reqmain_branch_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $customer_arr['customer_id'][] = $row['customer_id'];
            $customer_arr['customer_name'][] = $row['customer_name']. ' ( ' . $row['chit_group_no']. ' | ' . $row['ticket_no'] . ' ) ' ;
        }
        $t->assign('customer_arr', $customer_arr);

					
		}
		$t->display('master_lookups/lgl_case_entry/case_entry_add.htm');
    }


    public function Editcase_entry()
    {
        global $t;
		
        $main = array();
        $case_id = $_GET['case_id'];
        $sql = "SELECT * FROM tbl_lgl_case_entry where case_id=$case_id";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $data[] = $row;
			$this->all_lk($row["company_id"],$row["main_branch_id"]);
			$customer_name 		= fnGetValue("tbl_lgl_customer","customer_name","customer_id='".$row["customer_id"]."'");
        }
        $t->assign('data', $data);
        $t->assign('customer_name', $customer_name);
        $t->display('master_lookups/lgl_case_entry/case_entry_edit.htm');

    }
	
	public function all_lk($req_company_id,$req_branch_id){
		global $t;

		$company_id=$req_company_id;
		$branch_id=$req_branch_id;
		
		$branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$company_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);

        $customer_arr = array();
        $sql = "SELECT customer_name,customer_id,chit_group_no,ticket_no FROM tbl_lgl_customer where main_branch_id='$branch_id'";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $customer_arr['customer_id'][] = $row['customer_id'];
            $customer_arr['customer_name'][] = $row['customer_name']. ' ( ' . $row['chit_group_no']. ' | ' . $row['ticket_no'] . ' ) ' ;
        }
        $t->assign('customer_arr', $customer_arr);
		
        $complainant_arr = array();
        $sql = "SELECT complainant_id,complainant_name FROM tbl_lgl_complainant ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $complainant_arr['complainant_id'][] = $row['complainant_id'];
            $complainant_arr['complainant_name'][] = $row['complainant_name'];
        }
        $t->assign('complainant_arr', $complainant_arr);
		
        $advocate_arr = array();
        $sql = "SELECT advocate_id,advocate_name FROM tbl_lgl_advocate ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $advocate_arr['advocate_id'][] = $row['advocate_id'];
            $advocate_arr['advocate_name'][] = $row['advocate_name'];
        }
        $t->assign('advocate_arr', $advocate_arr);
       
      
	    $court_arr = array();
        $sql = "SELECT court_id,court_name FROM tbl_lgl_court ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $court_arr['court_id'][] = $row['court_id'];
            $court_arr['court_name'][] = $row['court_name'];
        }
        $t->assign('court_arr', $court_arr);
       
      
	  	$def_adv_arr = array();
        $sql = "SELECT def_adv_id,def_adv_name FROM  tbl_lgl_def_adv ";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $def_adv_arr['def_adv_id'][] = $row['def_adv_id'];
            $def_adv_arr['def_adv_name'][] = $row['def_adv_name'];
        }
        $t->assign('def_adv_arr', $def_adv_arr);

   }

    public function destroy($case_id)
    {
        $del_id_val = $case_id;
        $this->DeleteRecord($this->TableName, $this->TableID, $del_id_val);
    }


    public function store()
    {
        $case_id = $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
        header("Location: lgl_case_entry.php");
    }

    public function update()
    {
        $case_id = $this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
        header("Location: lgl_case_entry.php");
    }

    public function index()
    {

        global $t;
		$sql= "	SELECT * from tbl_lgl_case_entry";

		$sql= "	SELECT *,tbl_lgl_case_entry.judge as judge_name
				FROM `tbl_lgl_case_entry` 
				LEFT JOIN tbl_lgl_company on tbl_lgl_case_entry.company_id  = tbl_lgl_company.company_id
				LEFT JOIN tbl_lgl_branch on tbl_lgl_case_entry.main_branch_id = tbl_lgl_branch.branch_id
				LEFT JOIN tbl_lgl_customer on tbl_lgl_case_entry.customer_id = tbl_lgl_customer.customer_id
				LEFT JOIN tbl_lgl_court on tbl_lgl_case_entry.court  = tbl_lgl_court.court_id
				LEFT JOIN tbl_lgl_complainant on tbl_lgl_case_entry.complainant = tbl_lgl_complainant.complainant_id
				LEFT JOIN tbl_lgl_advocate on tbl_lgl_case_entry.advocate = tbl_lgl_advocate.advocate_id
				LEFT JOIN tbl_lgl_def_adv on tbl_lgl_case_entry.opp_advocate = tbl_lgl_def_adv.def_adv_id
				LEFT JOIN tbl_lgl_customer_report_category on tbl_lgl_case_entry.case_category_id = tbl_lgl_customer_report_category.category_id";
				
        //$tmpblnWhere = 0;
				if (isset($_REQUEST['sort'])) $reqsort = $_REQUEST['sort']; else $reqsort = 'case_id';
				if (isset($_REQUEST['ord'])) $reqord = $_REQUEST['ord'];  else  $reqord = "asc";
				if ($reqord == "asc") $reqord = "desc"; elseif ($reqord == "desc") $reqord = "asc";	 
				//---------------------------------Paging--------------------------------------------------
				if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
				if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
				$t->assign('reqord',$reqord);
				$t->assign('perpage',$perpage);
				
				//-------------------------------end  Paging------------------------------------------------------
				//$tmpblnWhere = 1;
					if ( isset( $_REQUEST['txtkeyword'] ) ){
						$reqkeyword = $_REQUEST['txtkeyword'] ;
						$t->assign ( 'reqkeyword', $reqkeyword);
						if (strcmp($reqkeyword,"")<>0){
							if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
							$sql = $sql . "tbl_lgl_customer.customer_name LIKE '%$reqkeyword%' ";
							$PageString = $PageString  .'&txtkeyword='.$reqkeyword;
						}
					}
					if ( isset( $_REQUEST['txtkeyword4'] ) ){
						$reqkeyword4 = $_REQUEST['txtkeyword4'] ;
						$t->assign ( 'reqkeyword4', $reqkeyword4);
						if (strcmp($reqkeyword4,"")<>0){
							if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
							$sql = $sql . "tbl_lgl_company.company_id = '$reqkeyword4' ";
							$PageString = $PageString  .'&txtkeyword4='.$reqkeyword4;
						}
					}
					if ( isset( $_REQUEST['txtkeyword5'] ) ){
						$reqkeyword5 = $_REQUEST['txtkeyword5'] ;
						$t->assign ( 'reqkeyword5', $reqkeyword5);
						if (strcmp($reqkeyword5,"")<>0){
							if ($tmpblnWhere==0){ $sql = $sql . " where "; $tmpblnWhere=1; } else	$sql = $sql . " and ";
							$sql = $sql . "tbl_lgl_branch.branch_id = '$reqkeyword5' ";
							$PageString = $PageString  .'&txtkeyword5='.$reqkeyword5;
						}
					}
				
        
        if (strcmp($reqsort, "") <> 0) $sql = $sql . ' order by ' . $reqsort;
        $sql = $sql . ' ' . $reqord;


// ---------------- call paging function---------------------------------------------------
$strfnPagingSql = $sql; include 'includes/callpaging.php'; 
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        $cnt = 1;

       //echo $sql;
        foreach ($temp as $index => $row) {
            $row["cnt"] = $cnt++;
			$row['case_date'] = sqldateout($row['case_date']);
			
			$case_entryResult[] = $row;
        }

        $branch_arr = array();
        $sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$reqkeyword4' order by `branch_name` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $branch_arr['branch_id'][] = $row['branch_id'];
            $branch_arr['branch_name'][] = $row['branch_name'];
        }
        $t->assign('branch_arr', $branch_arr);

		$custmor_arr = array();
        $sql = "SELECT customer_name,customer_id FROM tbl_lgl_customer where main_branch_id='$reqkeyword5' and company_id='$reqkeyword4' order by `customer_name`   asc";
//        echo $sql;
		$temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $custmor_arr['customer_id'][] = $row['customer_id'];
            $custmor_arr['customer_name'][] = $row['customer_name'];
        }
        $t->assign('custmor_arr', $custmor_arr);
		
        $t->assign('case_entry', $case_entryResult);
        $t->display('master_lookups/lgl_case_entry/case_entry_list.htm');

    }

}

?>
