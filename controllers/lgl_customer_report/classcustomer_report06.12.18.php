<?
include_once ("classes/classdatabase.php");

class customer_report extends Database
{
	private $TableName ;
	private $TableID ;
	public $TableIDVal ;

	
	private $SessionLanguage ;
	
	private $TableFieldArray;
	public $TableFieldValArray;
	public $printreport;

    public function __construct()
    {
        global $t;
        $this->TableName = 'tbl_lgl_customer_report';
        $this->TableID = 'customer_report_id';
        $this->TableFieldArray = array(
            array('customer_id', 1, 1, PDO::PARAM_INT),
            array('report_draft_id', 1, 1, PDO::PARAM_INT),
            array('report_draft', 1, 1, PDO::PARAM_STR),
            array('report_date', 1, 1, PDO::PARAM_STR)
        );

        $this->SessionLanguage = $_SESSION['opt_lang'];
        $this->DBConnect();

        $category_arr = array();
        $sql = "SELECT category_name,category_id FROM tbl_lgl_customer_report_category order by `category_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $category_arr['category_id'][] = $row['category_id'];
            $category_arr['category_name'][] = $row['category_name'];
        }
        $t->assign('category_arr', $category_arr);

        $report_arr = array();
        $sql = "SELECT report_name,report_draft_id FROM tbl_lgl_report_draft order by `report_draft_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $report_arr['report_draft_id'][] = $row['report_draft_id'];
            $report_arr['report_name'][] = $row['report_name'];
        }
        $t->assign('report_arr', $report_arr);
		
		$comp_arr = array();
		$sql = "SELECT company_name,company_id FROM tbl_lgl_company order by `company_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
		foreach ($temp as $index => $row) {
			$comp_arr['company_id'][] = $row['company_id'];
			$comp_arr['company_name'][] = $row['company_name'];
		}
		$t->assign('comp_arr', $comp_arr);
		
		$this->printreport=0;
   
 }
    public function Addcustomer_report()
    {
        global $t;
		
		  $Report_draft_id = $_GET['Report_draft_id'];
		  $t->assign ( 'Report_draft_id', $Report_draft_id );
		 
		  $reqcompany_id = $_GET['reqcompany_id'];
		  $t-> assign('reqcompany_id',$reqcompany_id);

		  $reqmain_branch_id= $_GET['reqmain_branch_id'];
		  $t-> assign('reqmain_branch_id',$reqmain_branch_id);

		  $req_branch_name		= fnGetValue("tbl_lgl_branch","branch_name","branch_id=$reqmain_branch_id");
		  $t-> assign('req_branch_name',$req_branch_name);

		  $reqcustomer_id = $_GET['reqcustomer_id'];
		  $t-> assign('reqcustomer_id',$reqcustomer_id);
	      
		  $customer_name 		= fnGetValue("tbl_lgl_customer","customer_name","customer_id='".$reqcustomer_id."'");
		  $t-> assign('customer_name',$customer_name);
		  
		  $year=date("Y");
		  $t-> assign('year',$year);
		  
		  $day=date("d");
		  $t-> assign('day',$day);

		  $month=date("m");
		  $t-> assign('month',$month);
		  

		  $date=date("d/m/Y");
		  $t-> assign('date',$date);
		  
		  $reqdate=date("Y-m-d");
		  $t-> assign('reqdate',$reqdate);
	
		  $main_data = array();
		  $main_sql = "SELECT * FROM tbl_lgl_report_draft where report_draft_id='$Report_draft_id' order by report_draft_id  asc";
		  $temp = $this->db_pdo->prepare($main_sql);
		  $temp->execute();
		  foreach ($temp as $index => $main_row) {
				  
				  $data = array();
				  $sql = "SELECT *,tbl_lgl_complainant.designation as complainant_designation,tbl_lgl_customer.designation as designation 
				  		  FROM tbl_lgl_customer 
						  LEFT JOIN tbl_lgl_case_entry on (tbl_lgl_customer.customer_id = tbl_lgl_case_entry.customer_id )
				          LEFT JOIN tbl_lgl_company on tbl_lgl_customer.company_id = tbl_lgl_company.company_id
				          LEFT JOIN tbl_lgl_complainant on tbl_lgl_case_entry.complainant = tbl_lgl_complainant.complainant_id
				          LEFT JOIN tbl_lgl_advocate on tbl_lgl_case_entry.advocate = tbl_lgl_advocate.advocate_id
				          LEFT JOIN tbl_lgl_cheque_return on tbl_lgl_customer.customer_id = tbl_lgl_cheque_return.customer_id
						  LEFT JOIN tbl_lgl_court on tbl_lgl_case_entry.court = tbl_lgl_court.court_id
						  LEFT JOIN tbl_lgl_legal_notice on tbl_lgl_customer.customer_id = tbl_lgl_legal_notice.customer_id
						  where tbl_lgl_customer.company_id='$reqcompany_id' and tbl_lgl_customer.main_branch_id='$reqmain_branch_id' and tbl_lgl_customer.customer_id='$reqcustomer_id'";
				         // echo $sql;
				  $temp = $this->db_pdo->prepare($sql);
				  $temp->execute();
						  
				  foreach ($temp as $index => $row) {
					 	$row['cust_drawn_on']			= fnGetValue("tbl_lgl_banks","Bankname","BankID='".$row["cust_drawn_on"]."'");
					 	$row['drawn_on']				= fnGetValue("tbl_lgl_banks","Bankname","BankID='".$row["drawn_on"]."'");
					 	$row['ch_deposited_at']			= fnGetValue("tbl_lgl_banks","Bankname","BankID='".$row["ch_deposited_at"]."'");
					 	$row['comp_bank']				= fnGetValue("tbl_lgl_banks","Bankname","BankID='".$row["comp_bank"]."'");
						$row['customer_name']			= nl2br($row['customer_name']);
						$row['address']					= nl2br($row['address']);
						$row['off_address_with_lines']	= nl2br($row['off_address']);
						
						
						$row['board_resolution_date']	= sqldateout($row['board_resolution_date']);	
						$row['agreement_date']			= sqldateout($row['agreement_date']);	
					 	$row['commencinf_from']			= sqldateout($row['commencinf_from']);	
						$row['terminating_on']			= sqldateout($row['terminating_on']);
					 	$row['auction_date']			= sqldateout($row['auction_date']);	
					 	$row['cheque_date']				= sqldateout($row['cheque_date']);	
					 	$row['cheque_return_date']		= sqldateout($row['cheque_return_date']);	
					 	$row['ch_deposited_on']			= sqldateout($row['ch_deposited_on']);	
					 	$row['ch_received_date']		= sqldateout($row['ch_received_date']);	
					 	$row['cust_cheque_date']		= sqldateout($row['cust_cheque_date']);	
					 	$row['joining_on']				= sqldateout($row['joining_on']);	
					 	$row['send_on']					= sqldateout($row['send_on']);	
					 	$row['ack_received_on']			= sqldateout($row['ack_received_on']);	
					 	$row['legal_notice_date']		= sqldateout($row['legal_notice_date']);	
					 	$row['received_on']				= sqldateout($row['received_on']);	
						

						$row['textchit_value']			= NumtoWord($row['chit_value']);	
						$row['textpayment_amount'] 		= NumtoWord($row['payment_amount']);
						$row['textfuture_liability'] 	= NumtoWord($row['future_liability']);
						$row['complainant_age']			= (date('Y')-date('Y',strtotime($row['complainant_dob'])));
						$row['complainant_designation']	= $row['complainant_designation'];

						$row['textauction_amount'] 		= NumtoWord($row['auction_amount']);
						$row['prize_amount'] 			= (($row['chit_value'])-($row['auction_amount']));
						$row['textprize_amount'] 		= NumtoWord($row['prize_amount']);
						
						$row['liability_amount'] 		= (($row['chit_period'])-($row['installments_cCleared']))*($row['monthly_sub']);
						$row['liability_installments'] 		= ($row['liability_amount']/$row['monthly_sub']);
						
						$row['future_liability_installments'] 	= ($row['future_liability']/$row['monthly_sub']);

						$row['textliability_amount']	= NumtoWord($row['liability_amount']);
						$row['textpending_amount'] 		= NumtoWord($row['pending_amount']);
						$row['textmonthly_sub'] 		= NumtoWord($row['monthly_sub']);
						$row['textauction_amount'] 		= NumtoWord($row['auction_amount']);
						$row['textpending_amount'] 		= NumtoWord($row['pending_amount']);
						$row['textch_amount'] 			= NumtoWord($row['ch_amount']);
						
						$row['chit_value']			= numberToCurrency($row['chit_value']); ;
						$row['monthly_sub']			= numberToCurrency($row['monthly_sub']); ;
						$row['auction_amount']		= numberToCurrency($row['auction_amount']); ;
						$row['payment_amount']		= numberToCurrency($row['payment_amount']); ;
						$row['ch_amount']			= numberToCurrency($row['ch_amount']); ;
						$row['custch_amount']		= numberToCurrency($row['custch_amount']); ;						
						$row['pending_amount']		= numberToCurrency($row['pending_amount']); ;						
						$row['prize_amount']		= numberToCurrency($row['prize_amount']); ;						
						$row['future_liability'] 	= numberToCurrency($row['future_liability']);

						
						$reqinstallments_cCleared=$row['installments_cCleared'];
						$reqcommencinf_from=sqldatein($row['commencinf_from']);
						$subscribtion_stop = FnGetDateValue("DATE_ADD('$reqcommencinf_from', INTERVAL $reqinstallments_cCleared month)");
						$t->assign('subscribtion_stop', $subscribtion_stop);

						$subscribtion_stop_time=strtotime(sqldatein($subscribtion_stop));
						$row['month']=date("F",$subscribtion_stop_time);
						$row['year']=date("Y",$subscribtion_stop_time);

						$time=strtotime(sqldatein($row['commencinf_from']));
						$row['start_date']=date("d",$time);
						$row['start_month']=date("F",$time);
						$row['start_year']=date("Y",$time);
						
						$time=strtotime(sqldatein($row['terminating_on']));
						$row['end_date']=date("d",$time);
						$row['end_month']=date("F",$time);
						$row['end_year']=date("Y",$time);
						
						$garantor_sql="SELECT * FROM tbl_lgl_guarantor where customer_id='".$reqcustomer_id."'"; 
						$garantor_temp = $this->db_pdo->prepare($garantor_sql);
						$garantor_temp->execute();
						$str_guarantor_names ="<ul>";
						$str_guarantor_names_withcnt="<table border='1' >
          <tr>
            <td>1.</td>
            <td><strong>".$row["customer_name"]."</strong></td>
          </tr>
          <tr>
            <td></td>
            <td>ADDRESS :".$row["address"]."</td>
          </tr>
      ";	
						$cnt=2;		  
						foreach ($garantor_temp as $index => $garantor_row) {
							$str_guarantor_names=$str_guarantor_names."<li><strong> C C to  ".$garantor_row["guarantor_name"]."</strong></li>";
							$str_guarantor=$str_guarantor."<strong>"."$cnt"."</strong>"."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"."<strong>".$garantor_row["guarantor_name"]."</strong>"."<br>";
							
							$str_guarantor_names_withcnt=$str_guarantor_names_withcnt."
							<tr><td>".$cnt++."</td><td><strong>".$garantor_row["guarantor_name"]."</strong></td></tr>
							<tr><td></td><td>ADDRESS: ".$garantor_row["guarantor_address"]."</td></tr>	";
							
							$row['gar_arr'][]=$garantor_row;
						}
						$row['guarantor_names'] = $str_guarantor_names;
						$row['guarantor'] = $str_guarantor;
						$row['guarantor_names_withcnt'] = $str_guarantor_names_withcnt."</table>";
						
						
					$data[] = $row;
					  
				  } 
				  $t->assign('data', $data);
				$draft			= stripslashes($main_row['report_draft']);	
				$main_data[] = $main_row;
		  } 
		  

			$customer_arr = array();
			$sql = "SELECT customer_name,customer_id,chit_group_no,ticket_no FROM tbl_lgl_customer where main_branch_id='$reqmain_branch_id' and company_id='$reqcompany_id' order by `customer_name` asc";
			$temp = $this->db_pdo->prepare($sql);
			$temp->execute();
			foreach ($temp as $index => $row) {
				$customer_arr['customer_id'][] = $row['customer_id'];
            $customer_arr['customer_name'][] = $row['customer_name']. ' ( ' . $row['chit_group_no']. ' | ' . $row['ticket_no'] . ' ) ' ;
			}
			$t->assign('customer_arr', $customer_arr);
		
			////////////////////new code added end /////////////////
			
			
			$branch_arr = array();
			$sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$reqcompany_id' order by `branch_name` asc";
			$temp = $this->db_pdo->prepare($sql);
			$temp->execute();
			foreach ($temp as $index => $row) {
				$branch_arr['branch_id'][] = $row['branch_id'];
				$branch_arr['branch_name'][] = $row['branch_name'];
			}
			$t->assign('branch_arr', $branch_arr);
		  
		  $t->assign('draft', $draft);
		  $t->assign('main_data', $main_data);

        $t->display('master_lookups/lgl_customer_report/lgl_reports.htm');
    }

    public function Editcustomer_report()
    {
        global $t;
        $main = array();
		  $Report_draft_id = $_GET['Report_draft_id'];
		  $t->assign ( 'Report_draft_id', $Report_draft_id );
		 
		  $reqcompany_id = $_GET['reqcompany_id'];
		  $t-> assign('reqcompany_id',$reqcompany_id);

		  $reqmain_branch_id= $_GET['reqmain_branch_id'];
		  $t-> assign('reqmain_branch_id',$reqmain_branch_id);

		  $reqcustomer_id = $_GET['reqcustomer_id'];
		  $t-> assign('reqcustomer_id',$reqcustomer_id);

        $sql = "SELECT * FROM tbl_lgl_customer_report where report_draft_id='$Report_draft_id' and customer_id='$reqcustomer_id'";
        //echo $sql;
		$temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
			$row[report_draft] = stripslashes($row[report_draft]);
            $data[] = $row;
        }
        $t->assign('data', $data);

        $t->display('master_lookups/lgl_customer_report/customer_report_edit.htm');

    }

    public function destroy($customer_report_id)
    {
        $del_id_val = $customer_report_id;
        $this->DeleteRecord($this->TableName, $this->TableID, $del_id_val);
    }


    public function store()
    {
		$this->TableFieldValArray[2][1]=$_POST['txtreport_draft_id'];
		$this->TableFieldValArray[3][1]=$_POST['txtreport_draft'];
		$this->TableFieldValArray[4][1]=sqldateout($_POST['txtreport_date']);
        $customer_report_id = $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
       	if ($this->printreport==1){
			$this->print_report($customer_report_id);
		}else{
	    	header("Location: lgl_customer_report.php");
		}
    }

    public function update()
    {
		$this->TableFieldValArray[2][1]=$_POST['txtreport_draft_id'];
		$this->TableFieldValArray[3][1]=$_POST['txtreport_draft'];
		$this->TableFieldValArray[4][1]=sqldateout($_POST['txtreport_date']);
		$customer_report_id = $this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
		//echo "customer_report_id=".$customer_report_id;
		if ($this->printreport==1){
			$this->print_report($customer_report_id);
		}else{
	    	header("Location: lgl_customer_report.php");
		}

    }
    public function print_report_from_list()
    {
		//echo "HI";
		//echo "customer_report_id=".$customer_report_id;
        $report_draft_id = $_GET['Report_draft_id'];
        $customer_id = $_GET['reqcustomer_id'];
		//echo $report_draft_id;
		        global $t;
        $main = array();

        $sql = "SELECT * FROM tbl_lgl_customer_report where report_draft_id='$report_draft_id' and customer_id='$customer_id'";
		//echo $sql;
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
			$row[report_draft] = stripslashes($row[report_draft]);
            $data[] = $row;
        }
        $t->assign('data', $data);

        $t->display('master_lookups/lgl_customer_report/customer_report_print.htm');

    }
    public function print_report($customer_report_id)
    {
		//echo "customer_report_id=".$customer_report_id;
        global $t;
        $main = array();
        //$customer_report_id = $_GET['customer_report_id'];
        $sql = "SELECT * FROM tbl_lgl_customer_report where customer_report_id=$customer_report_id";
		//echo $sql;
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
			$row[report_draft] = stripslashes($row[report_draft]);
            $data[] = $row;
        }
        $t->assign('data', $data);

        $t->display('master_lookups/lgl_customer_report/customer_report_print.htm');

    }
    public function index()
    {
		global $t;

        $main = array();
		
		$reqcategory_id = $_GET['reqcategory_id'];
		$t-> assign('reqcategory_id',$reqcategory_id);

		$reqcompany_id = $_GET['reqcompany_id'];
		$t-> assign('reqcompany_id',$reqcompany_id);
  
		$reqmain_branch_id= $_GET['reqmain_branch_id'];
		$t-> assign('reqmain_branch_id',$reqmain_branch_id);
		
		
        $reqcustomer_id = $_GET['reqcustomer_id'];
		$t-> assign('reqcustomer_id',$reqcustomer_id);
		$customer_name 		= fnGetValue("tbl_lgl_customer","customer_name","customer_id='".$reqcustomer_id."'");

		if($reqcustomer_id!="" OR $reqcompany_id!="" OR $reqmain_branch_id!=""){


		$customer_sql=" SELECT tbl_lgl_report_draft.report_draft_id as draft_report_draft_id,report_details.report_draft_id,
						tbl_lgl_report_draft.report_name,report_details.customer_id ,report_details.customer_report_id
						FROM tbl_lgl_report_draft
						LEFT JOIN tbl_lgl_customer_report_category on tbl_lgl_report_draft.category_id = tbl_lgl_customer_report_category.category_id
						LEFT JOIN (select * from tbl_lgl_customer_report where customer_id=$reqcustomer_id) as report_details
						on (tbl_lgl_report_draft.report_draft_id = report_details.report_draft_id)"; 
						if($reqcategory_id!="")
						{
						 $customer_sql.=" where tbl_lgl_report_draft.category_id=$reqcategory_id "; 
						}
						 $customer_sql.=" ORDER BY report_name asc ";
						//---------------------------------Paging--------------------------------------------------
						if (!(isset($_REQUEST['perpage']))){ $perpage= $_SESSION['RecordsPerPage'];}else{ $perpage = $_REQUEST['perpage'];}
						if (!(isset($_REQUEST['page']))){$page = 1;}else{$page = $_REQUEST['page'];}$PageString= "&perpage=".$perpage;
						$t->assign('reqord',$reqord);
						$t->assign('perpage',$perpage);
						
						//-------------------------------end  Paging------------------------------------------------------
			$strfnPagingSql = $customer_sql; include 'includes/callpaging.php'; 
			$temp = $this->db_pdo->prepare($customer_sql);
			//echo $customer_sql;
			$temp->execute();$cnt = 1;
			foreach ($temp as $index => $customer_row) {
				$customer_row["customer_id"] = $reqcustomer_id;
				$customer_row["cnt"] = $cnt++;
				$customer_data[] = $customer_row;
			}
			
			////////////////////new code added/////////////////
			
			$newcompany_id = $_POST['txtcompany_id'];
			$newmain_branch_id= $_POST['txtmain_branch_id'];
		
			$customer_arr = array();
			if($reqcompany_id=="" && $reqmain_branch_id==""){
				$sql = "SELECT customer_name,customer_id FROM tbl_lgl_customer where main_branch_id='$newmain_branch_id' and company_id='$newcompany_id' order by `customer_name` asc";
			}
			else{
				$sql = "SELECT customer_name,customer_id FROM tbl_lgl_customer where main_branch_id='$reqmain_branch_id' and company_id='$reqcompany_id' order by `customer_name` asc";
			}
			$temp = $this->db_pdo->prepare($sql);
			$temp->execute();
			foreach ($temp as $index => $row) {
				$customer_arr['customer_id'][] = $row['customer_id'];
				$customer_arr['customer_name'][] = $row['customer_name'];
			}
			$t->assign('customer_arr', $customer_arr);
		
			////////////////////new code added end /////////////////
			
			
			$branch_arr = array();
			if($reqcompany_id==""){
				$sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$newcompany_id' order by `branch_name` asc";
			}else{
				$sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch where company_id='$reqcompany_id' order by `branch_name` asc";
			}
			$temp = $this->db_pdo->prepare($sql);
			$temp->execute();
			//echo $sql;
			foreach ($temp as $index => $row) {
				$branch_arr['branch_id'][] = $row['branch_id'];
				$branch_arr['branch_name'][] = $row['branch_name'];
			}
			$t->assign('branch_arr', $branch_arr);

			
			$t->assign('customer_data', $customer_data);
			$t->assign('company_name', $company_name);
			$t->assign('branch_name', $branch_name);
			$t->assign('customer_name', $customer_name);
			
			}
       		$t->display('master_lookups/lgl_customer_report/customers_all_reports.htm');
	        }
}
?>
