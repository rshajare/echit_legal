<?
include_once ("classes/classdatabase.php");

class customer_report extends Database
{
	private $TableName ;
	private $TableID ;
	public $TableIDVal ;

	
	private $SessionLanguage ;
	
	private $TableFieldArray;
	public $TableFieldValArray;
	public $printreport;

    public function __construct()
    {
        global $t;
        $this->TableName = 'tbl_lgl_customer_report';
        $this->TableID = 'customer_report_id';
        $this->TableFieldArray = array(
            array('customer_id', 1, 1, PDO::PARAM_INT),
            array('report_draft_id', 1, 1, PDO::PARAM_INT),
            array('report_draft', 1, 1, PDO::PARAM_STR)
        );

        $this->SessionLanguage = $_SESSION['opt_lang'];
        $this->DBConnect();


        $report_arr = array();
        $sql = "SELECT report_name,report_draft_id FROM tbl_lgl_report_draft order by `report_draft_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
            $report_arr['report_draft_id'][] = $row['report_draft_id'];
            $report_arr['report_name'][] = $row['report_name'];
        }
        $t->assign('report_arr', $report_arr);
		
		$comp_arr = array();
		$sql = "SELECT company_name,company_id FROM tbl_lgl_company order by `company_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
		foreach ($temp as $index => $row) {
			$comp_arr['company_id'][] = $row['company_id'];
			$comp_arr['company_name'][] = $row['company_name'];
		}
		$t->assign('comp_arr', $comp_arr);
		
		$customer_arr = array();
		$sql = "SELECT customer_name,customer_id FROM tbl_lgl_customer order by `customer_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
		foreach ($temp as $index => $row) {
			$customer_arr['customer_id'][] = $row['customer_id'];
			$customer_arr['customer_name'][] = $row['customer_name'];
		}
		$t->assign('customer_arr', $customer_arr);
		
		$branch_arr = array();
		$sql = "SELECT branch_name,branch_id FROM tbl_lgl_branch order by `branch_id` asc";
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
		foreach ($temp as $index => $row) {
			$branch_arr['branch_id'][] = $row['branch_id'];
			$branch_arr['branch_name'][] = $row['branch_name'];
		}
		$t->assign('branch_arr', $branch_arr);
		$this->printreport=0;
   
 }
    public function Addcustomer_report()
    {
        global $t;
		
		  $Report_draft_id = $_GET['Report_draft_id'];
		  $t->assign ( 'Report_draft_id', $Report_draft_id );
		 
		  $reqcompany_id = $_GET['reqcompany_id'];
		  $t-> assign('reqcompany_id',$reqcompany_id);

		  $reqmain_branch_id= $_GET['reqmain_branch_id'];
		  $t-> assign('reqmain_branch_id',$reqmain_branch_id);

		  $req_branch_name		= fnGetValue("tbl_lgl_branch","branch_name","branch_id=$reqmain_branch_id");
		  $t-> assign('req_branch_name',$req_branch_name);

		  $reqcustomer_id = $_GET['reqcustomer_id'];
		  $t-> assign('reqcustomer_id',$reqcustomer_id);
		  
		  $year=date("Y");
		  $t-> assign('year',$year);
		  
		  $day=date("d");
		  $t-> assign('day',$day);

		  $month=date("m");
		  $t-> assign('month',$month);
		  

		  $date=date("d/m/Y");
		  $t-> assign('date',$date);
	
		  $main_data = array();
		  $main_sql = "SELECT * FROM tbl_lgl_report_draft where report_draft_id='$Report_draft_id' order by report_draft_id  asc";
		  //echo $main_sql;
		  $temp = $this->db_pdo->prepare($main_sql);
		  $temp->execute();
		  foreach ($temp as $index => $main_row) {
				  $report_category_id = $main_row['category_id'];
				  $data = array();
				  $sql = "SELECT *,tbl_lgl_complainant.designation as complainant_designation,
				  				tbl_lgl_customer.designation as designation 
				  		  FROM tbl_lgl_customer 
				          LEFT JOIN tbl_lgl_company on tbl_lgl_customer.company_id = tbl_lgl_company.company_id
						  LEFT JOIN (SELECT * FROM tbl_lgl_case_entry WHERE tbl_lgl_case_entry.case_category_id=' $report_category_id') as case_det on (tbl_lgl_customer.customer_id = case_det.customer_id)
				          LEFT JOIN tbl_lgl_complainant on case_det.complainant = tbl_lgl_complainant.complainant_id
				          LEFT JOIN tbl_lgl_advocate on case_det.advocate = tbl_lgl_advocate.advocate_id
						  LEFT JOIN tbl_lgl_court on case_det.court = tbl_lgl_court.court_id
				          LEFT JOIN tbl_lgl_cheque_return on tbl_lgl_customer.customer_id = tbl_lgl_cheque_return.customer_id
						  LEFT JOIN (SELECT * FROM tbl_lgl_legal_notice WHERE tbl_lgl_legal_notice.category_id=' $report_category_id' and tbl_lgl_legal_notice.guarantor_id is null) as legal_notice_det on (tbl_lgl_customer.customer_id = legal_notice_det.customer_id)
						  WHERE tbl_lgl_customer.company_id='$reqcompany_id' and tbl_lgl_customer.main_branch_id='$reqmain_branch_id' and 
						  		tbl_lgl_customer.customer_id='$reqcustomer_id'";
				          //echo $sql;
				  $temp = $this->db_pdo->prepare($sql);
				  $temp->execute();
						  
				  foreach ($temp as $index => $row) {
					 	$row['cust_drawn_on']			= fnGetValue("tbl_lgl_banks","Bankname","BankID='".$row["cust_drawn_on"]."'");
					 	$row['drawn_on']				= fnGetValue("tbl_lgl_banks","Bankname","BankID='".$row["drawn_on"]."'");
					 	$row['ch_deposited_at']			= fnGetValue("tbl_lgl_banks","Bankname","BankID='".$row["ch_deposited_at"]."'");
					 	$row['comp_bank']				= fnGetValue("tbl_lgl_banks","Bankname","BankID='".$row["comp_bank"]."'");

/*					 	$row['drawn_on_branch']			= fnGetValue("tbl_lgl_banks","Bankname","BankID='".$row["ch_deposited_at"]."'");
					 	$row['ch_deposited_at_branch']			= fnGetValue("tbl_lgl_banks","Bankname","BankID='".$row["ch_deposited_at"]."'");
*/						

						$row['customer_name']			= nl2br($row['customer_name']);
						$row['address']					= nl2br($row['address']);
						$row['off_address_with_lines']	= nl2br($row['off_address']);
						
						
						
						$row['agreement_date']			= sqldateout($row['agreement_date']);	
					 	$row['commencinf_from']			= sqldateout($row['commencinf_from']);	
					 	//echo	$row['terminating_on'];
						$row['terminating_on']			= sqldateout($row['terminating_on']);
					//	echo	$row['terminating_on'];
					 	$row['auction_date']			= sqldateout($row['auction_date']);	
					 	$row['cheque_date']				= sqldateout($row['cheque_date']);	
					 	$row['cheque_return_date']		= sqldateout($row['cheque_return_date']);	
					 	$row['ch_deposited_on']			= sqldateout($row['ch_deposited_on']);	
					 	$row['ch_received_date']		= sqldateout($row['ch_received_date']);	
					 	$row['cust_cheque_date']		= sqldateout($row['cust_cheque_date']);	
					 	$row['joining_on']				= sqldateout($row['joining_on']);	
					 	$row['send_on']					= sqldateout($row['send_on']);	
					 	$row['ack_received_on']			= sqldateout($row['ack_received_on']);	
					 	$row['legal_notice_date']		= sqldateout($row['legal_notice_date']);	
					 	$row['received_on']				= sqldateout($row['received_on']);	
					 	$row['case_date']				= sqldateout($row['case_date']);	

					 	$row['chit_reg_date']			= sqldateout($row['chit_reg_date']);	
					 	$row['last_pay_date']			= sqldateout($row['last_pay_date']);	

						$row['textchit_value']			= NumtoWord($row['chit_value']);	
						$row['textpayment_amount'] 		= NumtoWord($row['payment_amount']);
						$row['textfuture_liability'] 	= NumtoWord($row['future_liability']);
						$row['complainant_age']			= (date('Y')-date('Y',strtotime($row['complainant_dob'])));
						$row['complainant_designation']	= $row['complainant_designation'];

						$row['textauction_amount'] 		= NumtoWord($row['auction_amount']);
						$row['prize_amount'] 			= (($row['chit_value'])-($row['auction_amount']));
						$row['textprize_amount'] 		= NumtoWord($row['prize_amount']);
						
						$row['liability_amount'] 		= (($row['chit_period'])-($row['installments_cCleared']))*($row['monthly_sub']);
						$row['liability_installments'] 		= ($row['liability_amount']/$row['monthly_sub']);

						$row['paid_amount'] 		= ($row['installments_cCleared'] * $row['monthly_sub']);
						
						$row['future_liability_installments'] 	= ($row['future_liability']/$row['monthly_sub']);

						switch ($report_category_id) {
							case 4: //Arbitration
								$row['pending_amount']=$row['pend_amt_arbitration'];
								break;
							case 5: //legal Notice
								$row['pending_amount']=$row['pend_amt_legal_notice'];
								break;
							default: //138
								$row['pending_amount']=$row['pending_amount'];							
						}
						$row['textliability_amount']	= NumtoWord($row['liability_amount']);
						$row['textpending_amount'] 		= NumtoWord($row['pending_amount']);
						$row['textmonthly_sub'] 		= NumtoWord($row['monthly_sub']);
						$row['textauction_amount'] 		= NumtoWord($row['auction_amount']);
						$row['textpending_amount'] 		= NumtoWord($row['pending_amount']);
						$row['textch_amount'] 			= NumtoWord($row['ch_amount']);
						$row['textchit_value'] 			= NumtoWord($row['chit_value']);
			
						
						$reqinstallments_cCleared=$row['installments_cCleared'];
						$reqcommencinf_from=sqldatein($row['commencinf_from']);
						$subscribtion_stop = FnGetDateValue("DATE_ADD('$reqcommencinf_from', INTERVAL $reqinstallments_cCleared month)");
						$t->assign('subscribtion_stop', $subscribtion_stop);
						
						$reqterminating_on=sqldatein($row['terminating_on']);
						

						$terminating_on_plus_three_yr = FnGetDateValue("DATE_ADD('$reqterminating_on', INTERVAL 3 year)");
						$int_no_of_months_frm_term_pls_three=FnGetDateValue("TIMESTAMPDIFF(MONTH,'".$terminating_on_plus_three_yr."','".'2019-02-12'."' )");
						$row['int_no_of_months_frm_term_pls_three'] = $int_no_of_months_frm_term_pls_three;
						$row['terminating_on_plus_three_yr'] = sqldateout($terminating_on_plus_three_yr);
						$t->assign('terminating_on_plus_three_yr', $terminating_on_plus_three_yr);

						//echo $subscribtion_stop;
						$int_no_of_days=FnGetDateValue("DATEDIFF('".sqldatein($row['case_date'])."','".sqldatein($row['terminating_on'])."' )");
						//echo "DATEDIFF('".sqldatein($row['case_date'])."','".$subscribtion_stop."' )"."--((".$row['pending_amount']." * 0.18) / 365) * ".$int_no_of_days;
						$intrest_total =  (($row['pending_amount'] * 0.18) / 365) * $int_no_of_days;	
						
						$row['intrest_total']=sprintf("%0.2f",$intrest_total);
	 					
						$row['total_pending']=sprintf("%0.2f",($intrest_total+$row['pending_amount']));
						$row['subscribtion_stop']=sqldateout($subscribtion_stop);;

						$subscribtion_stop_time=strtotime(sqldatein($subscribtion_stop));
						$row['month']=date("F",$subscribtion_stop_time);
						$row['year']=date("Y",$subscribtion_stop_time);

						$case_datetime=strtotime(sqldatein($row['case_date']));
						$row['case_date_day']=date("d",$case_datetime);
						$row['case_date_month']=date("F",$case_datetime);
						$row['case_date_year']=date("Y",$case_datetime);

						$time=strtotime(sqldatein($row['commencinf_from']));
						$row['start_date']=date("d",$time);
						$row['start_month']=date("F",$time);
						$row['start_year']=date("Y",$time);
						
						$time=strtotime(sqldatein($row['terminating_on']));
						$row['end_date']=date("d",$time);
						$row['end_month']=date("F",$time);
						$row['end_year']=date("Y",$time);
						
						$garantor_sql="SELECT * FROM tbl_lgl_guarantor where customer_id='".$reqcustomer_id."'"; 
						$garantor_temp = $this->db_pdo->prepare($garantor_sql);
						$garantor_temp->execute();
						$str_guarantor_names ="<ul>";
						$str_guarantor_names_withcnt="<table border='0' >
						<tr>
						 <td>1.</td>
						 <td><strong>".$row["customer_name"]."</strong></td>
						</tr>
						<tr>
						 <td></td>
						 <td>ADDRESS :".$row["address"]."</td>
						</tr>
						";	
						$str_guarantor_names_withcnt_woaddr="<table border='0' >
						<tr>
						 <td>1.</td>
						 <td><strong>".$row["customer_name"]."</strong></td>
						</tr>
						
						";							
						$cnt=2;	 
						foreach ($garantor_temp as $index => $garantor_row) {
							$str_guarantor_names=$str_guarantor_names."<li><strong>  ".$garantor_row["guarantor_name"]."</strong></li>";
							$str_guarantor=$str_guarantor."<strong>"."$cnt"."</strong>"."&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"."<strong>".$garantor_row["guarantor_name"]."</strong>"."<br>";
							$str_guarantor_names_withcnt=$str_guarantor_names_withcnt."
								<tr><td>".$cnt++."</td><td><strong>".$garantor_row["guarantor_name"]."</strong></td></tr>
								<tr><td></td><td>ADDRESS: ".$garantor_row["guarantor_address"]."</td></tr>	";
							$str_guarantor_names_withcnt_woaddr=$str_guarantor_names_withcnt_woaddr."
								<tr><td>".$cnt++."</td><td><strong>".$garantor_row["guarantor_name"]."</strong></td></tr>";							
							$row['gar_arr'][]=$garantor_row;
						}
						$str_guarantor_names_withcnt=$str_guarantor_names_withcnt."</table>";		
						$str_guarantor_names_withcnt_woaddr=$str_guarantor_names_withcnt_woaddr."</table>";	
							
						$str_guarantor_names_with_registrar=$str_guarantor_names."<li><strong> The Joint Registrar of Chits </strong></li>";

						$cnt--;
						$row['guarantor_cnt']=$cnt;
						if ($cnt>3){
							$row['guarantor_cnt_str'] = "2 to ".$cnt;
						}else{
							$row['guarantor_cnt_str'] = "2 and ".$cnt;
						}
						$row['guarantor_names'] 		= $str_guarantor_names;
						$row['guarantor_names_with_registrar'] 		= $str_guarantor_names_with_registrar;
						$row['guarantor'] 				= $str_guarantor;
						$row['guarantor_names_withcnt'] = $str_guarantor_names_withcnt;						
						$row['str_guarantor_names_withcnt_woaddr'] = $str_guarantor_names_withcnt_woaddr;						

						$row['chit_value']			= numberToCurrency($row['chit_value']); ;
						$row['monthly_sub']			= numberToCurrency($row['monthly_sub']); ;
						$row['auction_amount']		= numberToCurrency($row['auction_amount']); ;
						$row['payment_amount']		= numberToCurrency($row['payment_amount']); ;
						$row['ch_amount']			= numberToCurrency($row['ch_amount']); ;
						$row['custch_amount']		= numberToCurrency($row['custch_amount']); ;						
						$row['pending_amount']		= numberToCurrency($row['pending_amount']); ;						
						$row['prize_amount']		= numberToCurrency($row['prize_amount']); ;						
						$row['future_liability'] 	= numberToCurrency($row['future_liability']);
						$row['paid_amount'] 		= numberToCurrency($row['paid_amount']);
						$row['deduction_amount']    = numberToCurrency($row['deduction_amount']);
						$row['intrest_total']    	= numberToCurrency($row['intrest_total']);
						$row['total_pending']    	= numberToCurrency($row['total_pending']);
						$row['sub_due']    			= numberToCurrency($row['sub_due']);
						$row['gst']    				= numberToCurrency($row['gst']);
						$row['pend_amt_arbitration']	= numberToCurrency($row['pend_amt_arbitration']);
						$row['pend_amt_legal_notice']	= numberToCurrency($row['pend_amt_legal_notice']);
						$row['div_paid']				= numberToCurrency($row['div_paid']);
						$row['sub_paid']				= numberToCurrency($row['sub_paid']);
						$row['oth_charges']				= numberToCurrency($row['oth_charges']);
						$row['adv_payment']				= numberToCurrency($row['adv_payment']);
						$row['sec_deposit']				= numberToCurrency($row['sec_deposit']);
						$data[] = $row;
				  } 
				  $t->assign('data', $data);
						  
				$draft			= stripslashes($main_row['report_draft']);	
				$main_data[] = $main_row;
		  } 
		  $t->assign('draft', $draft);
		  $t->assign('main_data', $main_data);

        $t->display('master_lookups/lgl_customer_report/lgl_reports.htm');
		//$t->display('lgl_reports.htm');	
    }

    public function Editcustomer_report()
    {
        global $t;
        $main = array();
		  $Report_draft_id = $_GET['Report_draft_id'];
		  $t->assign ( 'Report_draft_id', $Report_draft_id );
		 
		  $reqcompany_id = $_GET['reqcompany_id'];
		  $t-> assign('reqcompany_id',$reqcompany_id);

		  $reqmain_branch_id= $_GET['reqmain_branch_id'];
		  $t-> assign('reqmain_branch_id',$reqmain_branch_id);

		  $reqcustomer_id = $_GET['reqcustomer_id'];
		  $t-> assign('reqcustomer_id',$reqcustomer_id);

        $sql = "SELECT * FROM tbl_lgl_customer_report where report_draft_id='$Report_draft_id' and customer_id='$reqcustomer_id'";
        //echo $sql;
		$temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
			$row[report_draft] = stripslashes($row[report_draft]);
            $data[] = $row;
        }
        $t->assign('data', $data);

        $t->display('master_lookups/lgl_customer_report/customer_report_edit.htm');

    }

    public function destroy($customer_report_id)
    {
        $del_id_val = $customer_report_id;
        $this->DeleteRecord($this->TableName, $this->TableID, $del_id_val);
    }


    public function store()
    {
		$this->TableFieldValArray[2][1]=$_POST['txtreport_draft_id'];
		$this->TableFieldValArray[3][1]=$_POST['txtreport_draft'];

        $customer_report_id = $this->InsertRecord($this->TableName, $this->TableFieldArray, $this->TableFieldValArray);
       	if ($this->printreport==1){
			$this->print_report($customer_report_id);
		}else{
	    	header("Location: lgl_customer_report.php");
		}
    }

    public function update()
    {
		$this->TableFieldValArray[2][1]=$_POST['txtreport_draft_id'];
		$this->TableFieldValArray[3][1]=$_POST['txtreport_draft'];

		$customer_report_id = $this->UpdateRecord($this->TableName, $this->TableFieldArray, $this->TableID, $this->TableFieldValArray);
		//echo "customer_report_id=".$customer_report_id;
		if ($this->printreport==1){
			$this->print_report($customer_report_id);
		}else{
	    	header("Location: lgl_customer_report.php");
		}

    }
    public function print_report_from_list()
    {
		//echo "HI";
		//echo "customer_report_id=".$customer_report_id;
        $report_draft_id = $_GET['Report_draft_id'];
        $customer_id = $_GET['reqcustomer_id'];
		//echo $report_draft_id;
		        global $t;
        $main = array();

        $sql = "SELECT * FROM tbl_lgl_customer_report where report_draft_id='$report_draft_id' and customer_id='$customer_id'";
		//echo $sql;
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
			$row[report_draft] = stripslashes($row[report_draft]);
            $data[] = $row;
        }
        $t->assign('data', $data);

        $t->display('master_lookups/lgl_customer_report/customer_report_print.htm');

    }
    public function print_report($customer_report_id)
    {
		//echo "customer_report_id=".$customer_report_id;
        global $t;
        $main = array();
        //$customer_report_id = $_GET['customer_report_id'];
        $sql = "SELECT * FROM tbl_lgl_customer_report where customer_report_id=$customer_report_id";
		//echo $sql;
        $temp = $this->db_pdo->prepare($sql);
        $temp->execute();
        foreach ($temp as $index => $row) {
			$row[report_draft] = stripslashes($row[report_draft]);
            $data[] = $row;
        }
        $t->assign('data', $data);

        $t->display('master_lookups/lgl_customer_report/customer_report_print.htm');

    }
	
	
 /*   public function report_list()
    {
		global $t;
  
        $main = array();
		$reqcompany_id = $_GET['reqcompany_id'];
		$t-> assign('reqcompany_id',$reqcompany_id);
  
		$reqmain_branch_id= $_GET['reqmain_branch_id'];
		$t-> assign('reqmain_branch_id',$reqmain_branch_id);
		
        $customer_id = $_GET['customer_id'];
		$t-> assign('customer_id',$customer_id);

        $company_name = fnGetVAlue("tbl_lgl_company","company_name","company_id=$reqcompany_id");
        $branch_name = fnGetVAlue("tbl_lgl_branch","branch_name","branch_id=$reqmain_branch_id");
        $customer_name = fnGetVAlue("tbl_lgl_customer","customer_name","customer_id=$customer_id");

		$customer_sql="SELECT tbl_lgl_customer_report.report_draft_id,tbl_lgl_report_draft.report_name,tbl_lgl_customer_report.customer_id FROM tbl_lgl_report_draft left join tbl_lgl_customer_report
					   on (tbl_lgl_report_draft.report_name = tbl_lgl_customer_report.report_draft_id) 
					   WHERE customer_id=$customer_id or customer_id is null
					   ORDER BY report_draft_id";
        $temp = $this->db_pdo->prepare($customer_sql);
		//echo $customer_sql;
        $temp->execute();$cnt = 1;
        foreach ($temp as $index => $customer_row) {
			$customer_row["customer_id"] = $customer_id;
			$customer_row["cnt"] = $cnt++;
			$customer_data[] = $customer_row;
		}
        $t->assign('customer_data', $customer_data);
        $t->assign('company_name', $company_name);
        $t->assign('branch_name', $branch_name);
        $t->assign('customer_name', $customer_name);

        $t->display('master_lookups/lgl_customer_report/customers_all_reports.htm');
    }*/
    public function index()
    {
		global $t;

        $main = array();
		$reqcompany_id = $_GET['reqcompany_id'];
		$t-> assign('reqcompany_id',$reqcompany_id);
  
		$reqmain_branch_id= $_GET['reqmain_branch_id'];
		$t-> assign('reqmain_branch_id',$reqmain_branch_id);
		
		
        $reqcustomer_id = $_GET['reqcustomer_id'];
		$t-> assign('reqcustomer_id',$reqcustomer_id);
		
		
		
		if($reqcustomer_id!="" OR $reqcompany_id!="" OR $reqmain_branch_id!=""){


			$company_name = fnGetVAlue("tbl_lgl_company","company_name","company_id=$reqcompany_id");
			$branch_name = fnGetVAlue("tbl_lgl_branch","branch_name","branch_id=$reqmain_branch_id");
			$customer_name = fnGetVAlue("tbl_lgl_customer","customer_name","customer_id=$reqcustomer_id");
	
			$customer_sql="SELECT tbl_lgl_report_draft.report_draft_id as draft_report_draft_id,report_details.report_draft_id,
								tbl_lgl_report_draft.report_name,report_details.customer_id ,report_details.customer_report_id
						   FROM tbl_lgl_report_draft left join (select * from tbl_lgl_customer_report where customer_id=$reqcustomer_id) as report_details
						   on (tbl_lgl_report_draft.report_draft_id = report_details.report_draft_id) 
						   ORDER BY report_draft_id";
			
			$temp = $this->db_pdo->prepare($customer_sql);
			//echo $customer_sql;
			$temp->execute();$cnt = 1;
			foreach ($temp as $index => $customer_row) {
				$customer_row["customer_id"] = $reqcustomer_id;
				$customer_row["cnt"] = $cnt++;
				$customer_data[] = $customer_row;
			}
			$t->assign('customer_data', $customer_data);
			$t->assign('company_name', $company_name);
			$t->assign('branch_name', $branch_name);
			$t->assign('customer_name', $customer_name);
			
			}
       		$t->display('master_lookups/lgl_customer_report/customers_all_reports.htm');
	        }
}
?>
