<?php

// +----------------------------------------------------------------------------------------------------------------------------+
// |  File Name : nit_text_finder.php
// |  Version : 1.0 
// |  Create-date : 29, March, 2010
// |  Modify-date : 12, April, 2010
// |  authors : Niteen Borate                        
// | 
// |  Desciption : this program is to search unused multilanguage messages from html 
// |  pages of template folder.
// | 
// |  How it works :  first looks over all .html files from template folder 
// |  and search for {lang mkey=''} string/expession by using preg_match() function if founds add such entry in [tbl_find] table 
// |  used IGNORE to avoide dublicate entry then take backup of existing table and use delete query to delete message which 
// |  are not in tbl_find table
// |  
// +------------------------------------------------------------------------------------------------------------------------------
//

set_time_limit(0);

// Use this connection setting when u have diff database then config file
//--------------------------------------------------------------------------------------- 
/* 
$dbhost = "localhost";
$dbuname = "root";
$dbpassword= "";
$dbname = "shop";

$connection = mysql_connect($dbhost,$dbuname) or die("Could not connect to MySQL server!");
$db 		= mysql_select_db($dbname, $connection) or die ("Could not select data base!");   
*/
//--------------------------------------------------------------------------------------- 


// include databae connection file
if ( !defined( 'SMARTY_DIR' ) ){include_once( 'init.php' );}
header('Content-Type: text/html; charset=utf-8');



//create tbl_find table if not exist
$sql_create = "CREATE TABLE IF NOT EXISTS `tbl_find` ( `find_key` varchar(200) NOT NULL,  UNIQUE KEY `find_key` (`find_key`))" ;
$result=mysql_query($sql_create) or die("erro to create table"); 



// Function to copy table
function copy_table($from, $to) {

    if(table_exists($to)) {
        $success = false;
    }
    else {
        mysql_query("CREATE TABLE $to LIKE $from");
        mysql_query("INSERT INTO $to SELECT * FROM $from");
        $success = true;
    }
   
    return $success;
   
}


// Function to check if table exist
function table_exists($tablename, $database = false) {

    if(!$database) {
        $res = mysql_query("SELECT DATABASE()");
        $database = mysql_result($res, 0);
    }
   
    $res = mysql_query("
        SELECT COUNT(*) AS count
        FROM information_schema.tables
        WHERE table_schema = '$database'
        AND table_name = '$tablename'
    ");
   
    return mysql_result($res, 0) == 1;
}



?>
  <table border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#666666">
  
<? if (isset($_POST["Submit"])) {  ?>
  <tr bgcolor="Efefef">
      <td width="100%"><div align="center"> Resulted Messages per page
	  
	  <table width="100%" border="1" cellspacing="1" cellpadding="1">
	  <tr>
 	    <td width="9%">SR</td>
	    <td width="9%">File</td>
	    <td width="82%">Messages</td>
	    <td width="9%">Total</td>
	    </tr>
	<? 
				define("SLASH", stristr($_SERVER[SERVER_SOFTWARE], "win") ? "\\" : "/");
				$path = dirname(__FILE__). "/templates";
				$q = "mkey";
				$fp = opendir($path);
				
				$cnt_file = 0;  // for each file msg count
				$cnt_file_msg_total = 0; // for total msg count
				
				echo "<TR>";
				while($f = readdir($fp)){
					if( preg_match("#^\.+$#", $f) ) continue; // ignore symbolic links
					$file_full_path = $path.SLASH.$f;
					if(is_dir($file_full_path)) {
						$ret .= php_grep($q, $file_full_path);
					} else {
						$cnt_file_msg=0;
						$cnt_file = $cnt_file + 1;
						echo  "<TD>$cnt_file</td><TD>$f</td>";
						$content = file_get_contents($file_full_path);
						$lines = preg_split("/\r?\n|\r/", $content); // turn the content in rows
						$is_title = false;
						echo  "<TD>";
						foreach ($lines as $val) {
							//preg_match($pattern, $string, $matches);
							if (preg_match("{lang\s+mkey=\'(.*?)\'}", $val, $title)) {
									$cnt_file_msg = $cnt_file_msg+1;
									$find_key = $title[1];
									$sql = "insert IGNORE  into tbl_find values('$find_key')";
									$result=mysql_query($sql); 
									echo $title[1] . ",    ";
							}
						}
						echo  "</TD><td>" . $cnt_file_msg . "</td>";
						$cnt_file_msg_total = $cnt_file_msg_total + $cnt_file_msg;
					}// end of else
					echo "</tr>";
				} // end of while 	?>	
		</table>
		</td>
    </tr>
	
	<form action="" method="post" name="install" id="install"> 
    <tr bgcolor="Efefef">
      <td width="843"><div align="center"> 
	  <strong>Remove unused <?=$cnt_file_msg_total?> multilanguage messages from database <font color="#FF0000" size="4">[ <?=$dbname?>.tbl_lang_msg ]</font>  
        </strong></div></td>
    </tr>
    <tr bgcolor="#FFFFFF">
      <td><div align="center">
        <input type="submit" name="Delete" value="Delete Unused Messages----->">
      </div></td>
    </tr>
	</form>
	
	
<? 
}elseif (isset($_POST["Delete"])) {   // delete searched data/records

?>
 <tr bgcolor="Efefef">
      <td width="843"><div align="center"> 
 <strong> 
 <? 
	if(copy_table('tbl_lang_msg', 'tbl_lang_msg_bak')) {
 	   echo "<BR><Br> Backup Done :   tbl_lang_msg_bak backup table created successfully <BR><BR><BR>\n";
	}
	else {  
   	    echo "<Br><Br><Br><Br><font color='#FF0000' size='5'>Wait   !!!!!!<br> <BR>XXXXXXXXXXXXX Danger XXXXXXXXXXXXXX <br>";
    	echo "Problem to create backup table failure !!!!!!!!!!!!!<br> <BR></font><font color='#FF0000'></font><BR>";
		exit();
	}
	
	$sql_delete_rec = "delete from tbl_lang_msg where mainkey not in (select find_key from `tbl_find`)";
	$result=mysql_query($sql_delete_rec) or die("erro to delete unused messages from tbl_lang_msg table"); 
	?>
		<font color="#0000FF" size="5">Congratulation!!! [<?=mysql_affected_rows();?> ] unused messages deleted successfully !!!!!!!!</font> </font>        </strong></div>
  </td>
 </tr>
 
<? 	
}else {  ?>   
   <form action="" method="post" name="install" id="install"> 
    <tr bgcolor="Efefef">
      <td width="843" height="82"><div align="center"> <strong>Remove unused multilanguage messages from <font color="#FF0000" size="4">[
          <?= dirname(__FILE__)?>
          \templates\*.htm]</font>  template files
        </strong></div></td>
    </tr>
    <tr bgcolor="#FFFFFF">
      <td height="43"><div align="center"><font size="5">
        <input type="submit" name="Submit" value="Search----->">
      </font></div></td>
    </tr>
	</form>
<? } ?>	

  </table>	